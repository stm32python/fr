---
title: L'Internet Des Objets
description: Présentation des technologies des objets connectés
---

# L'Internet Des Objets

L'Internet des Objets (IoT) est un réseau de dispositifs physiques, tels que des capteurs, des caméras, des voitures, des appareils électroménagers, des montres intelligentes, etc. qui sont connectés à Internet et peuvent échanger des données entre eux et avec des systèmes informatiques.

Ces objets peuvent être contrôlés et surveillés à distance, ce qui permet une automatisation et une optimisation des processus dans de nombreux domaines, tels que la santé, l'agriculture, la logistique, l'énergie, la sécurité, etc. L'IoT est considéré comme une technologie clé pour la transformation numérique de l'industrie et de la société dans son ensemble.

## Préambule

Cette section est une synthèse sur l'IoT mise à jour au mois d'août 2024.<br>
Le sujet de l'IoT étant en ébullition permanente, mais également très vaste, il est impossible de le couvrir entièrement sans un certain nombre d'omissions et d'incohérences apparentes, pour plusieurs raisons.

D'une part, le nombre de technologies mises en œuvre est très important, chacune étant spécialisée pour une classe d'applications - voire pour **une** application donnée. Et ces technologies évoluent **très rapidement** au fil des normes (voir par exemple le Wi-Fi qui, depuis 2014 a connu 4 normes, la dernière en date de 2024).

Une autre difficulté provient d'un certain "enthousiasme" de la part des fournisseurs de solutions (hardware, software ...) qui prêtent souvent à leurs produits des caractéristiques "idéales" pour des cas d'usage que l'on ne rencontre qu'en laboratoire de test. Par exemple, on lit régulièrement que des objets connectés auraient une autonomie de plusieurs années sur piles. C'est rarement le cas en pratique. Un an d'autonomie sur des piles de qualité non industrielle (donc, peu onéreuses) est déjà une belle performance, en sachant que cette durée est influencée par de très nombreux paramètres plus ou moins faciles à anticiper (qualité de l'électronique, fréquence d'interrogation ou d'émission de l'objet, qualité de son firmware, historique des températures à son emplacement, qualité de ses piles, etc.).

Une source de complexité supplémentaire, liée à la précédente et nuisible à la croissance du marché de l'IoT, **est l'absence de normes pour rendre les objets connectés interopérables**. En domotique, par exemple, on trouve des technologies très similaires (ex : Zigbee, Z-Wave) qui ne sont pas interopérables. Une tentative pour adresser ce problème est le [**protocole Matter**](https://fr.wikipedia.org/wiki/Matter_(standard)). Mais sera-t-il adopté par les très nombreux fabricants qui se livrent une concurrence féroce ?

Ces réflexions sur le manque cruel de standards nous amènent finalement aux thématiques critiques de **la qualité** (robustesse, fiabilité), de **l'obsolescence** (intentionnelle ou pas), de la **réparabilité** (écoconception), de **la sûreté** (cybersécurité), de **la collecte et de l'utilisation des données personnelles** (RGPD), de **l'impact environnemental**, etc.<br>
Par exemple, si je déploie un réseau de caméras connectées conçues (hardware + software) par une société donnée ...
* Est-ce que celle-ci existera encore dans 3 ans pour assurer la maintenance ou le SAV ?
* Sinon, la solution est-elle conçue et documentée pour que je puisse aisément la réparer moi-même plutôt que jeter, et donc gaspiller, des centaines (voire des milliers) d'euros de matériel ?
* Est-ce que les mises-à-jour logicielles seront assurées, pour ne pas rendre le matériel prématurément obsolète ? 
* Suis-je certain que les images prises par mes caméras et envoyées sur Internet ne seront pas exploitées par une tierce partie, à mon insu ? 
* Etc.

## Sommaire

1. [Introduction](introduction)
2. [Les éléments d’un système IoT](architecture)
3. [IoT et cybersécurité](cybersecurite)
4. [Les technologies et stratégies de communication pour l'IoT](protocole)
    * [Liaisons et protocoles filaires](protocole_wire)
    * [Liaisons et protocoles sans fil](protocole_rf)
5. [IIoT et industrie 4.0](industrie)
6. [Les plateformes industrielles pour l'IoT](plateforme)
7. [Les défis de l'IoT](defis)
8. [Conclusion et perspectives](conclusion)

## Remerciements

*Je tiens à remercier Pascal Bouscasse et Johann Gilbert pour la mise à disposition de leur cours "Initiation à l'IoT", certains chapitres de ce cours en ligne en sont largement inspirés.*

Cette section cite de très nombreuses références au fil du texte et des illustrations, pour la plupart empruntées à d'autres auteurs. Aussi nous espérons qu'aucun d'entre eux ne s'offusquera si nous avions accidentellement omis de le citer. Il s'agirait bien évidemment d'une maladresse, certainement pas d'une volonté de nous approprier indûment son travail !

## Références

Parmi les ressources en ligne qui ont retenu notre attention citons :

- [Le Site de Orange sur l'IoT, faq](https://iotjourney.orange.com/fr-FR/support/faq)
- [Le Site de Orange sur l'IoT, connectivité](https://iotjourney.orange.com/fr-FR/connectivite)
- [Une introduction à Sigfox sur Linux Embedded](https://www.linuxembedded.fr/2020/03/introduction-a-sigfox)
- [Présentation IoT de Amarjeetsingh Thakur](https://fr.slideshare.net/slideshow/introduction-to-iot-234762568/234762568)
- [Le guide Internet de Steeve sur MQTT](http://www.steves-internet-guide.com/)
- [L'article de Digikey sur le bus SPI](https://www.digikey.fr/fr/articles/why-how-to-use-serial-peripheral-interface-simplify-connections-between-multiple-devices)
- [Le cours « Sécurité et systèmes embarqués » de Pablo Rauzy (Université Paris 8)](https://pablo.rauzy.name/teaching/2016-2020.html#sese)
- [Site de ma CNIL : « Comprendre les grands principes de la cryptologie et du chiffrement »](https://www.cnil.fr/fr/cybersecurite/comprendre-les-grands-principes-de-la-cryptologie-et-du-chiffrement)
- [Site de SYNOX](https://www.synox.io/ressources/)

De très nombreuses autres références sont disponibles au fil du texte des différentes pages.