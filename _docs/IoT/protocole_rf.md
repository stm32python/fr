---
title: Quelques liaisons et protocoles radiofréquence
description:
---

# Quelques liaisons et protocoles radiofréquence

Alors que l'IoT a hérité des technologies de communication câblées qui lui préexistaient, il a motivé un développement rapide et une diversification des technologies de **communication sans fil**. Le concept de réseaux d'objets intelligents interconnectés au déploiement facile et peu coûteux est indissociable de celles-ci.

La communication sans fil repose (essentiellement) sur diverses technologies radiofréquence (RF) telles que le Wi-Fi (un LAN pour "Local Area Network"), le Bluetooth (un PAN pour "Personal Area Network"), LoRaWAN / Sigfox (des LPWAN pour "Low Power Wide Area Networks") et les réseaux cellulaires (les WAN pour "Wide Area Networks").

## Les critères clef : portée et débit

Chaque réseau présente des points forts et des points faibles, le choix de la technologie pour une application donnée se fera en compromis de plusieurs critères :

- <span style="font-size: 15px;">Consommation d’énergie (combien d’énergie consommée pour envoyer 1 Mo ?)</span>
- <span style="font-size: 15px;">Bande passante (combien de temps nécessaire pour envoyer 1 Mo ?)</span>
- <span style="font-size: 15px;">Portée (sur combien de mètres ou kilomètres la connexion reste-t-elle fiable ?)</span>
- <span style="font-size: 15px;">Fréquence à laquelle vous devez récupérer les données captées par votre objet (en temps réel, une fois par heure ou par jour, etc.)</span>

Pour résumer, **il s'agit donc d'un arbitrage entre autonomie de l'objet, portée des échanges RF et débit des échanges RF avec l'objet**, illustré par la figure qui suit :

<div align="left">
<img alt="Portée et débit" src="images/debit_portee.jpg" width="600px">
</div>

> Source image originale : [matooma.com](https://www.matooma.com/fr/s-informer/actualites-iot-m2m/m2m-comment-connecter-vos-objets)

La portée d’émission d’un signal, c’est-à-dire la distance maximale à laquelle un récepteur est capable de décoder le signal, peut être découpée en trois catégories :

- <span style="font-size: 15px;">Courte : Quelques mètres à une centaine de mètres</span>
- <span style="font-size: 15px;">Moyenne : De quelques centaines de mètres à quelques kilomètres</span>
- <span style="font-size: 15px;">Longue : Jusqu’à plusieurs dizaines de kilomètres</span>

Le débit est principalement limité par la technologie adoptée pour la **modulation du signal** et la **largeur de la bande de fréquences** autrement appelée **bande passante** [^1] ; plus celle-ci est large et plus le débit sera important :

- <span style="font-size: 15px;">Bas débit : Jusqu’à plusieurs dizaines de bit/s ;</span>
- <span style="font-size: 15px;">Moyen débit : Jusqu’à plusieurs centaines de Kbit/s ;</span>
- <span style="font-size: 15px;">Haut débit : Plusieurs centaines de Kbit/s et jusqu’à plusieurs dizaines de Mbit/s ;</span>
- <span style="font-size: 15px;">Très haut débit : Plusieurs centaines de Mbit/s voire plusieurs Gbit/s.</span>

Les meilleures portées sont réalisées par des modulations à basse fréquence (BF), mais elles impliquent de faibles débits de données. A l'inverse, les débits les plus élevés nécessitent de hautes fréquences (HF) qui sont rapidement atténuées car absorbées par l'environnement, ce qui limite la portée des signaux. Et, bien sûr, plus la fréquence du signal augmente, plus son émission nécessite de l'énergie !

Notons enfin la nécessité de longues études d'impact sur l'environnement avant de valider un nouveau protocole HF, susceptible d'endommager les tissus vivants si les puissances d'émissions sont trop élevées. En fait, la réglementation impose une limite supérieure à la puissance d'émission et les concepteurs de matériels améliorent en conséquence la sensibilité des récepteurs.

## Principes de base de la radiocommunication

A toutes fins utiles, nous rappelons brièvement ici les principes de physique qui sont à la base des radiocommunications et qui justifient les orientations technologiques des protocoles et liaisons radiofréquence que nous énumèrerons plus loin.

### Ondes électromagnétiques et antennes

Les technologies de communication RF exploitent toutes le même phénomène physique : lorsque **des électrons oscillent dans un matériau conducteur**, celui-ci émet des "paquets" [**d'ondes électromagnétiques** (OEM)](https://fr.wikipedia.org/wiki/Onde_%C3%A9lectromagn%C3%A9tique) qui se propagent ensuite à 300 millions de mètres par seconde dans le vide (et légèrement moins dans l'air). Une OEM "théorique" est constituée d'une oscillation couplée d'un champ électrique et d'un champ magnétique [^2]. Contrairement à une onde sonore, elle n'a pas besoin de s'appuyer sur un milieu matériel tel que l'air pour se propager. 

|Modélisation d'une onde électromagnétique|Diagramme de rayonnement d'une antenne 5G|
|:-:|:-:|
|<img alt="Onde électromagnétique" src="images/onde_electromagnetique.jpg" width="300px" width="350">|<img alt="Rayonnement d'une antenne 5G" src="images/Antenne adaptative rayonnement vertical .jpg" width="350px">|
|[Source : Abeilles en liberté](https://www.abeillesenliberte.fr/labeille-est-elle-electrosensible-partie-2/)|Source : [OFEV](https://www.bafu.admin.ch/bafu/fr/home.html)|

Si le paquet d'OEM est produit intentionnellement, l'objet conducteur source dans lequel oscillent les électrons est appelé [**antenne**](https://fr.wikipedia.org/wiki/Antenne_radio%C3%A9lectrique) émettrice. La fréquence d'oscillation des électrons dans l'antenne va déterminer **l'énergie transportée par l'onde**, les deux sont proportionnelles. On utilise aussi souvent la **longueur d'onde**, inversement proportionnelle à la fréquence.

Dans les cas qui nous intéressent on souhaite produire [des ondes radio](https://fr.wikipedia.org/wiki/Onde_radio), ce qui signifie qu'elles auront des fréquences "basses" (moins de 300 GHz) [^3], qu'elles transporteront peu d'énergie, et **qu'elles se propageront loin** en contrepartie [^4]. C'est l'une des contraintes fondamentales en radiocommunication, qui justifie la multiplication des solutions techniques selon les usages : **la puissance transmise (et donc in fine le débit d'informations) est élevée à haute fréquence tandis que la portée de la communication est élevée à basse fréquence.** Il faudra faire des compromis ...

Lorsqu'une antenne émet des ondes radio, celles-ci **se propagent dans des  directions déterminées par sa géométrie** [^5]. L'émission des antennes constitue naturellement **une vulnérabilité de sécurité** : tous les messages émis peuvent être écoutés par quiconque dispose d'un récepteur. Tout comme les transmissions câblées [^6], les transmissions radio devront donc **être chiffrées** pour garantir que seuls leurs destinataires légitimes pourront accéder à leur contenu, c'est l'un des enjeux [de la cybersécurité](cybersecurite).

Une antenne peut aussi fonctionner en **réception**. Les OEM reçues font osciller les électrons qu'elle contient ce qui y crée un courant mesurable contenant toutes les fréquences émises (enfin, si l'antenne est correctement conçue !).

|Emission d'une antenne dipolaire|Réception par une antenne dipolaire|
|:-:|:-:|
|<img alt="Emission dipôle" src="images/Dipole_xmting_antenna_animation_4_408x318x150ms.gif" width="400px">|<img alt="Réception dipôle" src="images/Dipole_receiving_antenna_animation_6_800x394x150ms.gif" width="400px">|

Source : [Wikipédia](https://fr.wikipedia.org/wiki/Onde_radio)

### Emission et réception d'un signal

Le principe de la radiocommunication exploite directement le rayonnement des antennes. Si on est capable de produire dans une antenne, avec un émetteur, un message encodé via un ensemble de fréquences électriques, des ondes radio seront rayonnées. Une autre antenne, connectée à un module [récepteur](https://fr.wikipedia.org/wiki/R%C3%A9ception_des_ondes_radio%C3%A9lectriques), pourra capter ces ondes, les convertir en courants électriques et extraire de ceux-ci le message de l'émetteur.

Le schéma fonctionnel général de tout système de communication numérique est donné ci-dessous. Dans le cas des communications radio, le canal de transmission est tout simplement l'espace entre l'antenne de l'émetteur et l'antenne du récepteur.

<div align="left">
<img alt="Systèmes de communication" src="images/syscomm.jpg" width="650px">
</div>

> Source image :  [Cours "Propagation" de Christophe Roblin, Télécom Paris](https://www.researchgate.net/publication/323199059_C-Roblin_Cours-Propagation-Antennes_2017)

Il existe [**de nombreuses façons d'encoder / de décoder un message dans une onde radio, c'est ce que l'on appelle la modulation / démodulation**](https://fr.wikipedia.org/wiki/Modulation_du_signal). Le choix de la modulation sert bien évidemment les objectifs de la technologie considérée (priorité à la résistance aux interférences ? à la portée ? à l'économie d'énergie ? au débit ? Etc.).

Une antenne en réception collecte en fait toutes les OEM de son environnement dans une plage de fréquences qui dépend principalement de sa géométrie. Charge alors au circuit récepteur de filtrer les fréquences qui ne correspondent pas aux signaux intéressants (considérées comme "du bruit") avant de démoduler ces derniers. Les circuits d'émission et de réception sont complexes, à la fois analogiques et numériques. 

Le schéma ci-dessous est un exemple d'architecture radio IEEE 802.11a (Wi-Fi) :

<br>
<div align="left">
<img alt="Portée et débit" src="images/radio-wi-fi.jpg" width="900px">
</div>
<br>

> Source :  [Ozone connect](https://www.ozoneconnect.io/ressources/technologies-sans-fil-industrielles-le-guide-de-choix-complet/)

Ce type d’architecture permet une communication **« full duplex »**, c’est-à-dire fonctionnant aussi bien en émission qu’en réception. Une même antenne est utilisée pour l’émission et la réception grâce à un **duplexeur** permettant d’isoler les deux voies. Certains systèmes utilisent deux antennes distinctes, notamment lorsque l’isolation entre les voies d’émission et de réception est soumise à une contrainte sévère (source : [Christophe Roblin
Télécom Paris, cours "Propagation"](https://www.researchgate.net/publication/323199059_C-Roblin_Cours-Propagation-Antennes_2017)).

### Propagation et bilan de liaison, puissance du signal reçu (RSSI)

Au cours d'une [transmission radio](https://fr.wikipedia.org/wiki/Propagation_des_ondes_radio), il se passe beaucoup de phénomènes entre l'émetteur et le récepteur (rélexion, réfraction, diffraction, diffusion, interférences,etc.) qui imposent des contraintes fortes sur la conception des antennes, des émetteurs et des récepteurs.

Un paragraphe succinct sur ce sujet est disponible [ici, dans le tutoriel sur LoRa / LoRaWAN](/../Embedded/lora_rf). Vous y trouverez notamment une explication de termes qui sont systématiquement présents dans les fiches techniques ou les applications des protocoles sans fil : RSSI, SNR, dBm.

## Les bandes et la régulation ISM

D'après [LearningLab](https://learninglab.gitlabpages.inria.fr/mooc-iot/mooc-iot-ressources/Module1/S02/C034AA-M01-S02-part1-cours_FR.html) et [l'ACERP  (Autorité de Régulation des Communications Electroniques et des Postes)](https://www.arcep.fr/la-regulation/grands-dossiers-reseaux-mobiles/le-guichet-start-up-et-innovation/le-portail-bandes-libres.html), le spectre radio est régulé et la plupart des plages de fréquences sont réservées (police, armée, aéronautique, météorologie, etc.) et sous licence (téléphonie mobile, etc.). Ces attributions changent d’un pays à l’autre, en France, c’est l’ARCEP qui est en charge de cette régulation.Vous pouvez consulter la répartition du spectre des fréquences en France sur le site de [l’Agence nationale des fréquences (ANFR)](https://www.anfr.fr/accueil).

Pour garantir qu’une communication ne sera pas brouillée par des interférences, il faut pouvoir contrôler les entités autorisées à communiquer sur les mêmes fréquences et, pour cela, utiliser **des bandes de fréquences sous licence**. C’est ce que font les opérateurs téléphoniques lorsqu'ils achètent des licences, autrement dit, des droits d’utilisation exclusifs d’une largeur du spectre radio. Ainsi, seuls leurs abonnés peuvent utiliser ces fréquences (via la carte SIM qui contrôle l’usage de telle ou telle fréquence).

Ce processus d’autorisation individuelle n’est cependant pas toujours adapté à la multiplication des objets communicants. C’est pourquoi l’ACERP propose **des bandes de fréquences libres d’utilisation : [les bandes ISM (Industrie, Science et Médical)](https://fr.wikipedia.org/wiki/Bande_industrielle,_scientifique_et_m%C3%A9dicale)**.

La contrepartie de la simplicité (et de la gratuité) d’usage des bandes libres est l’absence de garantie contre le risque de brouillage, en raison du nombre et de l’emplacement indéfini des acteurs qui peuvent les utiliser. Pour limiter ce risque, **chaque bande libre est dédiée à certaines catégories de dispositifs**, qui doivent respecter des limites de puissance et de taux d’utilisation ainsi que d’éventuelles autres restrictions. 

**Quelques usages notables de bandes de fréquences ISM :**

|Plage de fréquences|Technologie|
|:-:|:-:|
|13 553 – 13 576 kHz|RFID, NFC|
|169.4 – 169.8125 MHz|Wize (LPWAN lancé par GRDF pour ses compteurs connectés)|
|433.05 – 434.79 MHz|Talkies-walkies, télécommandes, LoRa|
|863 – 868.6 MHz|z-Wave, Sigfox, LoRa, RFID UHF, Zigbee|
|2400 – 2483.5 MHz|Wi-Fi, Bluetooth, Zigbee, Thread|
|5150 – 5350 MHz|Wi-Fi|

> Source : [ACERP](https://www.arcep.fr/la-regulation/grands-dossiers-reseaux-mobiles/le-guichet-start-up-et-innovation/le-portail-bandes-libres.html)

**Les bandes ISM et leurs limitations d'usage en puissance selon le cadre :**

<div align="left">
<img alt="Bandes ISM et puissances autorisées" src="images/bandes_ISM_puissance_autorisees.jpg" width="600px">
</div>

> Source image originale : [EBDS Wireless & Antennas](http://www.ebds.eu)

Ces règles techniques ne visent pas à exclure tout risque de brouillage, mais à le contenir dans des limites compatibles avec la coexistence de l’ensemble des utilisateurs. En pratique, ces conditions techniques d’utilisation consistent à **limiter la portée et le temps d’utilisation de la bande** afin de réduire le risque de brouillage mutuel. Par exemple, l’utilisation des systèmes Wi-Fi dans la bande 2,4 GHz est limitée à une puissance d’émission de 100 mW (à comparer aux fours à micro-onde qui utilisent une puissance 100 000 fois supérieure). De même, pour les bandes ISM basses, le temps d’émission (duty cycle) est limité à 1% du temps par heure, soit 36s.

## Les communications courte distance

Ces bus, protocoles, liaisons et/ou réseaux sont souvent utilisés dans des environnements locaux, tels que des maisons, des bureaux ou des espaces industriels. 

### RFID et NFC

La [**Radio Frequency Identification (RFID)**](https://iotjourney.orange.com/fr-FR/support/faq/quelle-difference-entre-nfc-et-rfid) est une technologie permettant des échanges de données à **courte distance**, au plus une centaine de mètres dans le cas d'objets  alimentés (appelés **tags actifs**) mais en général **quelques centimètres** pour les **tags passifs** (voir plus loin). C'est aussi un mode de connexion particulier, dédié à l’identification que l'on pourrait surnommer "code-barre 2.0". Une puce RFID permet d’identifier à distance, comme on le ferait en scannant un code-barre.

D'après [EYEBYTE](https://www.fr-ebyte.com/news/525) :

La RFID est principalement utilisée pour l'identification et le suivi automatiques des objets, la logistique, la gestion des stocks, le suivi des articles, le contrôle d'accès, les systèmes de paiement.

Un système RFID se compose généralement d'une étiquette (ou tag) et d'un lecteur. L'étiquette contient une puce et une antenne capables de recevoir et de transmettre des signaux radiofréquence. 

Les étiquettes RFID disponibles sont de trois types : 

1. **Passives** : aucune batterie intégrée requise pour le tag, c'est le signal de requête RF du lecteur qui procure l'énergie.<br>
Il existe plusieurs fréquences RFID passives. La basse fréquence (BF – 125 kHz ou 134 kHz), l'ultra haute fréquence (UHF soit 850-960 MHz) ou Rain RFID et la haute fréquence (HF soit 13,56MHz).

2. **Semi-passives** : batterie intégrée dans le tag. Le tag ne répond que pour donner suite à la réception d’une requête provenant d’un lecteur.

3. **Actives** : batterie intégrée dans le tag. Le Tag se comporte comme une balise autonome et envoie ses messages sans aucune corrélation avec un éventuel lecteur.

La **Near Field Communication (NFC)** est en fait une technologie de type RFID dont la portée est limitée à quelques centimètres pour des raisons de sécurité, qui communique en conséquence à haute fréquence (13.56 MHz). Elle est essentiellement utilisée pour le paiement sans contact.

|Logo RFID|Architecture d'un tag RFID **passif**|Logo NFC|Paiement sans contact avec NFC|
|:-:|:-:|:-:|:-:|
|<img alt="Logo RFID" src="images/rfid.jpg" width="100px">|<img alt="Un tag RFID" src="images/RFID tag.jpg" width="170px">|<img alt="logo NFC" src="images/logo NFC.png" width="70px">|<img alt="application NFC" src="images/nfc.png" width="300px">|
|Source : [Creative Fabrica](https://www.creativefabrica.com/fr/product/rfid-radio-frequency-identification-7/)|Source : [Medium](https://medium.com/insa-tc/tag-rfid-et-ses-failles-de-s%C3%A9curit%C3%A9-6dcd90047393)|Source : [Wikimedia](https://commons.wikimedia.org/wiki/File:NFC_logo.svg)|Source : [Finom.co](https://finom.co/fr-fr/blog/what-are-contactless-payments-and-how-they-work/)|


**Exemple d'infrastructure NFC :**

<div align="left">
<img alt="Infrastructure NFC" src="images/IOT_31.png" width="480px">
</div>

Source : [st.com](https://www.st.com/content/st_com/en.html)

### Wi-Fi

Le [réseau Wi-Fi](https://fr.wikipedia.org/wiki/Wi-Fi) permet aux dispositifs électroniques de se connecter entre eux et à Internet. Le Wi-Fi permet un débit important, de manière fiable et sécurisée. Il est régi par les normes du groupe IEEE 802.11, datant de 1997, qui évoluent très rapidement afin de réduire la latence et d'augmenter le débit de données. Par exemple, la norme Wi-Fi 7, lancée en janvier 2024, promet jusqu'à 46 Gb/s !

En contreparties le Wi-Fi consomme beaucoup d'énergie (ce qui compromet son intégration dans des objets connectés alimentés sur batteries) et sa portée est limitée à quelques dizaines de mètres, nettement moins au travers d’obstacles comme des murs épais. Il est en revanche largement adopté pour les applications de [**l'IIoT**](industrie) (voir [cette référence](https://www.ozoneconnect.io/ressources/technologies-sans-fil-industrielles-le-guide-de-choix-complet/) ou [celle-ci](https://www.intel.com/content/dam/www/central-libraries/us/en/documents/2022-06/wi-fi-tutorial-long.pdf)).

|Wi-Fi|Exemple d'infrastructure Wi-Fi|
|:-:|:-:|
|<img alt="Logo Wi-Fi" src="images/IOT_32.png" width="250">|<img alt="Infrastructure Wi-Fi" src="images/IOT_33.png" width="450px">|
||Source : [sunix.com](https://www.sunix.com/en/deviceserver.php)|

**WiFi HaLow : la norme 802.11ah pour concurrencer les LPWAN ?**

A noter l’existence d’une norme [**Wi-Fi IEEE 802.11ah**](https://fr.wikipedia.org/wiki/IEEE_802.11ah) dite **« Wi-Fi HaLOW »** qui a pour objectif de fournir une grande portée et d’être économe en énergie pour les objets connectés.

- La bande de fréquence est celle de 900 MHz ;
- Le data-rate utilisable en HaLow varie entre 150 kb/s et 86.7 Mb/s ;
- La largeur de canal utilisée est de 1,2,4 et 8 MHz ;
- La portée théorique de HaLow est de plusieurs kilomètres.

Par exemple, en septembre 2024 :  D'après [Génération NT](https://www.generation-nt.com/actualites/wifi-portee-16-km-2050616), un module de référence MM6108-MF08651 Wi-Fi HaLow émettant avec une puissance de 21 dBm depuis une antenne dipôle standard de 1 dBi pour une puissance rayonnée mesurée à 22 dBm a permis un débit de 2 mégabits par seconde à une distance de 15,9 km.


### Bluetooth et Bluetooth Low Energy (BLE)

**Bluetooth** est une norme de télécommunications permettant l'échange bidirectionnel de données à courte distance en utilisant des ondes radio UHF sur la bande de fréquence de 2,4 GHz. Son but est de simplifier les connexions entre les appareils électroniques à proximité en supprimant des liaisons filaires (source : [Wikipédia](https://fr.wikipedia.org/wiki/Bluetooth)). Sa consommation d'énergie est relativement faible ainsi que sa portée (de 20 à 50 mètres en intérieur, selon l’environnement) et son débit maximum est de 3 Mbit/s. Le but du BLE est d'assurer des communications courte portée et faible débit entre des objets connectés.


[**Bluetooth à basse consommation**](../Embedded/ble) ou **Bluetooth à basse énergie** (en anglais : [**Bluetooth Low Energy — BLE**](https://www.argenox.com/library/bluetooth-low-energy/introduction-to-bluetooth-low-energy-v4-0/) ou **BTLE**) ou encore **Bluetooth Smart** est une technique de transmission sans fil créée par Nokia en 2006 sous la forme d’un standard ouvert basé sur le Bluetooth, qu'il complète mais sans le remplacer.<br>
Comparé au Bluetooth, le BLE permet un débit du même ordre de grandeur (1 Mbit/s) pour **une consommation d'énergie réduite** (2 à 10 fois moins) en prenant le parti d'éteindre la fonction radio des objets entre deux échanges. La technologie permet aux appareils de se connecter dans un rayon de 20 mètres au plus.

|Bluetooth|Exemple d'infrastructure BLE|
|:-:|:-:|
|<img alt="Logo Bluetooth" src="images/IOT_34.png" width="180">|<img alt="Infrastructure BLE" src="images/IOT_35.png" width="600px">|
||Source : [Moko Blue](https://www.mokoblue.com/fr/gateway/)|

### ZigBee

D'après [Wikipédia](https://fr.wikipedia.org/wiki/ZigBee) **ZigBee** est un protocole de haut niveau permettant la communication d'équipements personnels ou domestiques équipés de petits émetteurs radios à faible consommation.Cette technologie a principalement pour but la communication à courtes distances dans un cadre « domotique ».


|ZigBee|Exemple d'infrastructure ZigBee|
|:-:|:-:|
|<img alt="Logo ZigBee" src="images/logo_zigbee.png" width="150">|<img alt="Infrastructure ZigBee" src="images/Zigbee_SmartHome.png" width="400px">|
||Source : [mokoblue.com](https://www.mokoblue.com/fr/gateway/)|

ZigBee permet plusieurs topologies de réseaux : étoile, maillage, arborescente (voir [cette source](https://connect.ed-diamond.com/MISC/misc-086/tout-tout-tout-vous-saurez-tout-sur-le-zigbee)).

### Z-Wave

D'après [Wikipédia](https://fr.wikipedia.org/wiki/Z-Wave) **Z-Wave** est un protocole radio conçu pour la domotique (éclairage, chauffage…). Z-Wave communique en utilisant une technologie radio de faible puissance dans la bande de fréquence de 868 MHz.<br>
La portée directe est d'environ 50 m (davantage en extérieur, moins en intérieur) et exploite la technologie du maillage (mesh) pour augmenter cette distance ainsi que la fiabilité.

Z-Wave est conçue pour être facilement intégrée dans les produits électroniques de consommation, y compris les appareils à piles tels que les télécommandes, les détecteurs de fumée et capteurs de sécurité.

|Z-Wave|Exemple d'infrastructure Z-Wave|
|:-:|:-:|
|<img alt="Logo Z-Wave" src="images/Logo Z-Wave.png" width="150">|<img alt="Infrastructure Z-Wave" src="images/infra_Z-Wave.jpg" width="400px">|
||Source : [keysecurity.com.tw](https://www.keysecurity.com.tw/comparaison-protocole-zigbee-vs-z-wave.html?lang=FR)|

Un réseau Z-Wave peut être constitué de 1 jusqu’à 232 périphériques, avec la possibilité de réaliser un pont entre deux réseaux si plus de périphériques sont nécessaires. Par comparaison avec Zigbee, la portée du Z-Wave est légèrement supérieure et son débit divisé par six (100 kbits/s au lieu de 250 kbits/s).

## Les communications longue distance

Ces liaisons et/ou réseaux à longue distance sont conçues pour permettre la connectivité entre des objets intelligents sur des distances étendues par exemple : la surveillance agricole, le suivi de véhicules, la gestion des réseaux de capteurs dans des zones géographiquement dispersées. Un autre de leurs avantages, et pas des moindres, **est leur bonne pénétration à travers les murs des bâtiments**.

Parmi ces réseaux on distingue deux familles :
1. **Ceux qui n'utilisent pas une connectivité cellulaire et font appel à leurs propres passerelles, à savoir LoRaWAN et SigFox**. Ils sont adapté à des applications qui envoient très peu de données à une très faible fréquence et sont par conséquent les champions de la frugalité énergétique. 
2. **Ceux qui utilisent une connectivité cellulaire, à savoir LTE-M et NB-IoT**, pour des applications qui envoient plus fréquemment des données et qui nécessitent une latence raisonnablement faible, au prix d'une consommation significativement plus élevée. Ces réseaux bénéficient de services clef en main via un abonnement chez le bon opérateur téléphonique.

Passons brièvement en revue ces solutions ...

### LoRaWAN et LoRa (LPWAN non cellulaire)

La spécification [LoRaWAN](../Embedded/lora) pour **L**ong-**Ra**nge **W**ide **A**rea **N**etwork est un protocole de communication radiofréquence grande portée et faible consommation développé pour les objets connectés, reposant sur une technologie de modulation à très large bande (UWB) **LoRa**, créé et breveté par la start-up française Cycleo en 2009.

LoRaWAN cible les exigences clés de l'IoT telles que la communication bidirectionnelle, la sécurité de bout en bout, la mobilité et les services de localisation et appartient à la famille plus large des technologies *Low Power Wide Area Networks* (LPWAN ou *réseau étendu à basse consommation* en français) qui ne reposent pas sur le réseau de téléphonie cellulaire.

Ce type de réseau est déployé dans certaines bandes de fréquences ISM, disponibles mondialement sans licence. 

LoRaWAN est l'un des deux LPWAN non cellulaire les plus populaires, avec Sigfox.

|LoRaWAN|Exemple d'infrastructure LoRaWAN|
|:-:|:-:|
|<img alt="Logo LoRaWAN" src="images/IOT_36.png" width="500px">|<img alt="Infrastructure LoRaWAN" src="images/IOT_37.png" width="550px">|
|Source : [alicdn.com](https://sc04.alicdn.com/)|Source : [digikey.fr](https://www.digikey.fr/fr/articles/develop-lora-for-low-rate-long-range-iot-applications)|


### Sigfox (LPWAN non cellulaire)

[Sigfox](https://www.sigfox.com/) désigne un opérateur de télécommunications français créé en 2009 par Christophe Fourtet et Ludovic Le Moan et implanté à Labège, commune de la banlieue toulousaine. Sigfox est racheté en 2022 par la société taïwanaise Unabiz.

Sigfox est spécialisé dans l'IoT grâce à un réseau bas débit dit "0G". Il contribue à l'IoT en permettant l'interconnexion via une passerelle. Sa technologie radio UNB (« Ultra Narrow Band ») lui permet de bâtir un réseau cellulaire bas-débit, économe en énergie. 
Ce type de réseau est déployé dans certaines bandes de fréquences ISM, disponibles mondialement sans licence (Source texte : [Wikipédia](https://fr.wikipedia.org/wiki/Sigfox)).

Sigfox est l'un des deux LPWAN non cellulaire les plus populaires, avec *LoRaWAN*.


|Sigfox|Exemple d'infrastructure Sigfox|
|:-:|:-:|
|<img alt="Logo Sigfox" src="images/IOT_43.png" width="250px">|<img alt="Infrastructure Sigfox" src="images/IOT_44.png" width="600px">|
||Source : [libelium.com](https://development.libelium.com/sigfox_networking_guide/introduction)|


### NB-IoT ou LTE Cat-NB (LPWAN cellulaire)

[NB-IoT](https://en.wikipedia.org/wiki/Narrowband_IoT) pour pour "Narrow Band Internet of Things" couvre les mêmes cas d'usage que LoRaWAN et SigFox, pour des applications supportant une latence élevée (1 à 10 secondes) et de faibles débits (5,5 Kbit/s en downlink et jusqu’à 62,5 Kbit/s en uplink). Bien que ce réseau repose sur les infrastructures 4G, celle-ci doivent être mises à jour matériellement pour son bon fonctionnement, ce qui coûte environs 15000 $ par station de base. 

|NB-IoT|Exemple d'infrastructure NB-IoT|
|:-:|:-:|
|<img alt="Logo NB-IoT" src="images/IOT_38.png" width="300px">|<img alt="Infrastructure NB-IoT" src="images/IOT_39.png" width="600px">|
||Source : [thefastmode.com](https://www.thefastmode.com/technology-solutions/13542-softbank-claims-worlds-first-connection-test-of-non-ip-data-delivery-nidd-for-nb-iot)|

Le NB-IoT n'est pas adapté à des objets qui se déplacent, la connexion est interrompue lorsqu'un objet quitte le périmètre d'une tour cellulaire (Source : [mattoma](https://www.matooma.com/fr/s-informer/actualites-iot-m2m/ltem-avantages-specificites-techniques)).

Quelques applications clef du NB-IoT :

<div align="left">
<img alt="Applications du NB-IoT" src="images/applis_nb_iot.png" width="600px">
</div>

> Source image : Présentation "La 5G, nouvel Eldorado ?", Hakim JAAFAR, STMicroelectronics (02/2022)

### LTE-M ou LTE Cat-M1/M2 (LPWAN cellulaire)

Les réseaux [LTE-M](https://en.wikipedia.org/wiki/LTE-M) pour "Long-Term Evolution Machine Type Communication" sont entièrement compatibles avec les réseaux LTE (2G) et sont une extension de la 4G. Contrairement au NB-IoT, leur déploiement ne nécessite aucune modification de l'infrastructure cellulaire, une mise à jour logicielle des stations de base est suffisante et les smartphones peuvent s'y connecter. 


|LTE-M|Exemple d'infrastructure LTE-M|
|:-:|:-:|
|<img alt="Logo LTE" src="images/IOT_40.png" width="300px">|<img alt="Infrastructure LTE" src="images/IOT_41.png" width="600px">|
||Source : [murata.com](https://www.murata.com/en-eu/products/connectivitymodule/cat-m1)|

Le LTE-M offre des avantages compétitifs uniques au sein des technologies LPWAN, notamment : 
Le LTE-M offre des avantages compétitifs uniques au sein des technologies LPWAN, notamment : 

* Il permet une communication bi-directionnelle en temps réel (débits montants et descendants), à la différence du Nb-IoT et LoraWAN qui ne sont pas en temps réel.
* Il offre des débits variables pouvant aller jusqu’à 7 Mbit/s en uplink et 4 Mbits/s en downlink (pour les modules LTE Cat-M2).
* Sa latence est paramétrable entre 1 ms et quelques secondes 
* Il supporte des communications vocales (à la différence du NB-IoT).
* Il supporte le handover permettant ainsi aux dispositifs mobiles de changer de tour de communication sans perte de session même à des vitesses pouvant dépasser les 100 km/h (Source : [mattoma](https://www.matooma.com/fr/s-informer/actualites-iot-m2m/ltem-avantages-specificites-techniques)).

En contrepartie de ses caractéristiques débit et latence meilleures que celles du NB-IoT, LTE-M consomme bien plus d'énergie.

Quelques applications du LTE-M :

<div align="left">
<img alt="Applications du LTE-M" src="images/applis_lte_m.png" width="600px">
</div>

> Source image : Présentation "La 5G, nouvel Eldorado ?", Hakim JAAFAR, STMicroelectronics (02/2022)

## Les réseaux cellulaires "longue distance"

Ces réseaux "historiques", initialement développés pour la téléphonie mobile, sont antérieurs à l'IoT dont ils ont progressivement intégré les besoins au fil de leur évolutions, en proposant des normes moins énergivores pour les objets sur batteries (voir NB-IoT et LTE-M ci-avant, et leur chronologie juste après).<br>

Telles qu'elles, la 3G, la 4G, la 5G (au sens "téléphonie") **ne sont pas du tout adaptées aux applications IoT lorsque les objets ne peuvent pas être alimentés sur secteur**. Mais elles ont d'indéniables atouts en contrepartie de leur forte consommation et de leur coût : 

- Elles permettent **une couverture internationale** grâce à l'interopérabilité des **réseaux GSM** (pour "Global System for Mobile"). Pour des applications de suivi de biens ("assets tracking") c'est une caractéristique essentielle ;
- Leur mise en œuvre est facilitée puisque le réseau cellulaire est déployé et administré par l'opérateur chez lequel vous vous abonnez.  

## Principe de fonctionnement

D'après [Orange](https://radio-waves.orange.com/fr/un-reseau-mobile-comment-ca-marche/) :

Un réseau mobile est composé **d’un réseau d’antennes-relais** (ou stations de base), couvrant chacune une portion de territoire délimité **(cellule)** et acheminant les communications sous-forme d’ondes radio vers et depuis les terminaux des utilisateurs.

Les communications mobiles suivent le principe général de la téléphonie : relier deux utilisateurs distants en passant par l’équipement réseau d’un opérateur chargé de gérer le service. Mais à la différence du fixe, dans le réseau mobile, ce ne sont pas des fils de cuivre ou de fibre optique qui assurent la liaison finale mais des transmissions radio.

Le téléphone mobile d’un utilisateur communique par la voie des airs avec une antenne-relais, qui elle-même communique avec le commutateur centralisé de l’opérateur : un ordinateur. Celui-ci achemine la communication vers le correspondant sur le réseau fixe ou via d’autres antennes relais.

Pour communiquer, un utilisateur mobile doit donc être à portée d’une antenne-relais. Celle-ci a une portée limitée, et ne couvre qu’un territoire restreint autour d’elle, appelé **la « cellule »** (d’où l’autre nom de **« réseaux cellulaires »** souvent utilisé pour désigner les réseaux mobiles). Pour couvrir un maximum de territoire et faire en sorte que les utilisateurs soient toujours en mesure de téléphoner, les opérateurs déploient des milliers de cellules, chacune d’elles étant équipée d’antennes en faisant en sorte que leurs cellules se chevauchent, de façon à ne jamais perdre le positionnement des utilisateurs.

**Principe d'un réseau cellulaire :**

<div align="left">
<img alt="Principe d'un réseau cellulaire" src="images/reseau_cellulaire.jpg" width="400px">
</div>

**Source image** :  [Orange](https://radio-waves.orange.com/fr/un-reseau-mobile-comment-ca-marche/)

### Chronologie des réseaux cellulaires (et autres)

La frise chronologique ci-dessous donne les dates approximatives de disponibilité des différentes technologies de communication radiofréquence supportées par les réseaux de téléphonie mobile :

<div align="left">
<img alt="Chronologie des réseaux RF" src="images/chronologie_rf.png" width="900px">
</div>

> Source image : Présentation "La 5G, nouvel Eldorado ?", Hakim JAAFAR, STMicroelectronics (02/2022)

On constate que les nouvelles technologies sont apparues de façon progressive, sous forme de "demi-standards" pourrait-on dire, avant d'être intégrées dans les évolutions majeures du réseau cellulaire. Ainsi, la 5G n'apporte pas à ses début une révolution pour les technologies WAN et LPWAN, mais elle intègre et standardise celles qui lui préexistaient, généralement inventées et mise en œuvre par des alliances d'entreprises et d'acteurs de l'électronique, des télécommunication, des services ...

### LTE Cat 0, Cat 1, Cat 4 …

D'après [Orange](https://iotjourney.orange.com/fr-FR/connectivite/l-essentiel-a-savoir-sur-la-connectivite-4g-lte-cat-1) :

La connectivité **4G LTE Cat 1**, abréviation pour *Long Term Evolution Category 1*, est une variante de la technologie 4G LTE, la quatrième génération de réseau cellulaire pour la téléphonie mobile. La 4G LTE Cat 1 a été normalisée en 2008 en tant que première déclinaison du LTE spécialement conçue pour l’IoT, l’Internet des Objets. Bien qu’elle soit la plus ancienne version de la technologie 4G LTE, ses capacités en débit de données restent largement au-dessus d’autres connectivités dédiées à l’IoT comme LoRA et NB-IoT.


|LTE|Positionnement du LTE Cat 1/4|
|:-:|:-:|
|<img alt="Logo LTE" src="images/Logo LTE.png" width="100px">|<img alt="Infrastructure LTE" src="images/Orange reseaux RF compares.png" width="600px">|
||Source : [Orange.com](https://iotjourney.orange.com/fr-FR/connectivite/l-essentiel-a-savoir-sur-la-connectivite-4g-lte-cat-1)|


En termes de fonctionnement, la 4G LTE Cat 1 utilise une bande de fréquence d’une largeur allant de 800 à 2600 MHz lui permettant d’offrir des débits de 10 Mbit/s en réception (descendant) et 5 Mbit/s en émission (ascendant), en moyenne. De ce fait, par rapport à d’autres technologies spécifiques à l’IoT comme la connectivité 4G LTE Cat M 1 ou la connectivité NB-IoT, la 4G LTE Cat 1 prend en charge, par exemple :

* La voix sur LTE (VoLTE)
* Le data streaming (données transmises en continu)
* Les applications à faible latence (50 - 100 ms)
* La couverture internationale

La 4G LTE Cat 1 est donc un choix pertinent pour de nombreuses applications M2M (Machine-to-Machine) et bien adapté pour les entreprises de services.

Exemples d'applications :
* Surveillance et contrôle des processus à distance (IoT industriel)
* Suivi des actifs
* Gestion des flottes d’appareils connectés
* Systèmes de contrôle d’accès par la voix
* Services d’alarme.

La 4G LTE Cat 4 est une évolution de la 4G LTE Cat 1. Ses taux de transmissions sont plus élevés : 150 Mbit/s (descendant) et 50 Mbit/s (ascendant). Cependant, cette technologie de communication coûte plus cher.

### Zoom sur la 5G

Déployée depuis les années 2020, la 5G définit et normalise trois familles de technologies de communication RF :

- **EMBB** pour "Enhanced Mobile Broad Band". L'objectif est d'augmenter le débit de la téléphonie mobile en choisissant la bonne fréquence parmi celles disponibles :

    <div align="left">
    <img alt="Bandes <EMBB de la 5G" src="images/bandes-frequences-5G-ra2022.png" width="
    600px">
    </div>
    
    > Source image : [arcep.fr](https://www.arcep.fr/la-regulation/grands-dossiers-reseaux-mobiles/la-5g.html)

<br>

- **MMTC** pour "Massive Machine Type Communication". On est **dans le cœur de notre sujet**, il s'agit de construire un réseau capable de supporter la croissance de l'IoT en offrant des connexions M2M (pour "Machine-to-Machine") bien plus performantes que celles du "vieux" standard 2G, redimensionnées pour gérer des centaines de millions d'objets potentiels. Les deux réseaux IoT mobiles (LPWAN) cellulaires couverts par MMTC sont ceux que nous avons déjà évoqué : LTE-M et NB-IoT. 

<br>

- **URLLC** pour "Ultra-Reliability and Low Latency Communication", qui permettra le développement de solutions connectées à très basse latence et très haut débit en phase avec les besoins d'applications pour lesquelles la fiabilité et la réactivité du réseau sont essentielles telles que l'automobile autonome, la chirurgie à distance, le pilotage à distance de machines industrielles, etc.<br>

### Les bandes de  fréquence de la 5G en France

D'après [Ariase](https://www.ariase.com/mobile/dossiers/5g-frequences), les fréquences 5G en France sont (ou seront) : 

- **La bande des 3.5 GHz** est celle qui a été attribuée en exclusivité à la 5G, à l'issue d'un long processus et d'enchères. De toutes les fréquences  utilisées par la 5G, la bande des 3,5 GHz est celle qui offre le meilleur compromis bande passante - débits - portée. Les fréquences 3,5 GHz ont une portée moyenne de 400 mètres en zone urbaine et 1,2 km en zone rurale.<br>
Notons qu'aux USA, les fréquences porteuses choisies (3.7 à 3.98GHz) suscitent des réserves de la part de la Federal Aviation Administration (FAA) du fait de leurs potentielles interférences avec les altimètres radar des avions (qui opèrent entre 4.2 et 4.4GHz).

- **Les fréquences de la 2G, 3G, 4G (700 Mhz, 800 Mhz, 900 Mhz, 1.8 GHz, 2.1 GHz, 2.6 GHz)**, que les opérateurs peuvent basculer en 5G. Ces fréquences basses ont une grande portée, 2 km en zone urbaine et 8 Km en zone rurale, elles permettent d'assurer une meilleure couverture du territoire, y compris en zone rurale. Autre avantage : une bonne pénétration à l'intérieur des bâtiments. Inconvénient principal : les débits sont limités.

- **La bande des 26 GHz** est celle qui permettra à la 5G d'exprimer tout son potentiel en offrant des débits comparables à ceux de la fibre.
Mais les fréquences très hautes ont aussi leur lot d'inconvénients. D'abord, elles ont une faible pénétration dans les bâtiments. Ensuite, elles ont une portée limitée, de l'ordre de 150 mètres en zone urbaine, ce qui nécessitera le déploiement d'un grand nombre d'antennes.<br>
Par ailleurs les fréquences millimétriques choisies interfèrent avec les radars météo et suscitent aussi auprès de la population des inquiétudes sanitaires et environnementales (probablement injustifiées).


<br>Pour finir, le déploiement des nouveaux services de la 5G est finalement **plus compliqué qu'anticipé** car les marchés qui en ont besoin ne sont pas assez matures au regard des investissements "délirants" nécessaires, d'autant que le gain pour son principal usage actuel, la téléphonie mobile, est anecdotique par comparaison avec la 4G.<br>La sphère publique se pose également la question de la pertinence à "forcer" ces nouveaux marchés à une époque où les télécommunications, l'IA générative, les cryptomonnaies, etc. consomment énormément d'énergie en contrepartie de services parfois très utiles, parfois complètement dispensables voire "socialement toxiques". Cette inquiétude est d'autant plus vive qu'un usage immodéré du numérique participe à l'accélération du dérèglement climatique.

## Conclusion : Sélection d'une solution radiofréquence

Pour conclure, le graphique qui suit donne un organigramme décisionnel pour la mise en œuvre d'une solution connectée compte tenu des contraintes de son environnement. Ce n'est qu'en évaluant avec attention les possibilités offertes par les différentes technologie et leurs plages de fonctionnement que l'on pourra construire un objet fiable et facile d'utilisation dans le cadre du modèle économique choisi.

<div align="left">
<img alt="Choix d'une solution RF embarquée" src="images/choix_solution_rf.jpg" width="800px">
</div>

> Source image d'origine : **Rosmianto** (page Linkedin inaccessible)

On notera que cet organigramme ne tient pas compte d'un autre critère très important pour certaines applications : **la latence** de la communication. On s'intéresse ici essentiellement au compromis bande passante / portée / consommation.

Le tableau ci-dessous est une synthèse des caractéristiques comparées des différents protocoles que nous avons passés en revue, d'après une liste conséquente de sites Internet ayant servi à construire ce tutoriel. **Vous y trouverez des ordres de grandeur** mais ne prenez pas les chiffres "au pied de la lettre", les performances peuvent varier significativement d'un produit à une autre et selon les conditions dans lesquelles on les mesure, qui ne sont pas précisées ici. Les portées notamment sont très difficiles à évaluer car elle dépendent totalement de l'environnement (intérieur/extérieur, champ libre/obstacles).

<div align="left">
<img alt="Caractéristiques des solutions RF" src="images/rf_syhthesis.jpg" width="1000px">
</div>

## Références

- Article de [Wikipédia sur le Wi-Fi](https://fr.wikipedia.org/wiki/Wi-Fi)
- Guide (eBook) ["Technologies sans fil industrielles" de Ozone connect](https://www.ozoneconnect.io/ressources/technologies-sans-fil-industrielles-le-guide-de-choix-complet/).
- Présentation ["Wi-Fi Unleashed: Wi-Fi 7, 6 GHz, and Beyond" de Intel](https://www.intel.com/content/dam/www/central-libraries/us/en/documents/2022-06/wi-fi-tutorial-long.pdf)
- Le site [LearningLab](https://learninglab.gitlabpages.inria.fr/mooc-iot/mooc-iot-ressources/Module1/S02/C034AA-M01-S02-part1-cours_FR.html)
- Le site de [l'ACERP  (Autorité de Régulation des Communications Electroniques et des Postes)](https://www.arcep.fr/la-regulation/grands-dossiers-reseaux-mobiles/le-guichet-start-up-et-innovation/le-portail-bandes-libres.html)
- Un très bon article sur [Linux Embedded](https://www.linuxembedded.fr/2016/03/protocoles-de-communication-frameworks-et-systemes-dexploitation-pour-les-objets-connectes), certes un peu ancien.
- [Cours "Propagation" de Christophe Roblin, Télécom Paris](https://www.researchgate.net/publication/323199059_C-Roblin_Cours-Propagation-Antennes_2017)
- [Article de Argenox, Introduction to Bluetooth Low Energy](https://www.argenox.com/library/bluetooth-low-energy/introduction-to-bluetooth-low-energy-v4-0/)
- [Article de Argenox, Maximizing BLE Range](https://www.argenox.com/library/bluetooth-low-energy/maximizing-bluetooth-low-energy-ble-range/)
-[Article du blog BoucheCousue sur le Wi-Fi HaLow](https://bouchecousue.com/blog/wifi-halow-la-norme-802-11ah-porte-loin/)

## Notes au fil du texte

[^1]: En électronique, la bande passante d'un système est l'intervalle de fréquences dans lequel l'affaiblissement du signal est inférieur à une valeur spécifiée. C'est une façon sommaire de caractériser la fonction de transfert d'un système, pour indiquer la gamme de fréquences qu'il peut raisonnablement traiter (source : [Wikipédia]( https://fr.wikipedia.org/wiki/Bande_passante)).

[^2]: En fait, il s'agit [d'une seule et même entité](https://fr.wikipedia.org/wiki/Tenseur_%C3%A9lectromagn%C3%A9tique) selon la théorie de la relativité restreinte. 

[^3]: Pour en savoir plus, vous pouvez consulter l'article de Wikipédia sur le [**spectre électromagnétique**](https://fr.wikipedia.org/wiki/Spectre_%C3%A9lectromagn%C3%A9tique).

[^4]: Lorsque leur fréquence augmente, les ondes électromagnétiques transportent de plus en plus d'énergie et se propagent de moins en moins loin car elles sont significativement absorbées par les molécules de l'air ou des objets qu'elles rencontrent.

[^5]: En général, les antennes ont un rayonnement **anisotrope**, càd qu'elles focalisent leur signal dans des directions spécifiques. Elles émettent plus de puissance dans certaines directions, c'est le cas par exemple des cellules pour la téléphonie mobile.

[^6]: Lorsque les communications sont filaires les câbles se comportent également comme des antennes et rayonnent un signal qui peut être écouté à distance. Ainsi les câbles coaxiaux sont blindés pour éviter la déperdition d'énergie liée au rayonnement et à l'interaction avec d'autres ondes électromagnétiques qui bruiteraient la communication. Plus la fréquence du signal augmente, plus le fil rayonne. Ceci est aussi l'une des difficultés qui se présentent lors de la conception des circuits intégrés cadencés à haute fréquence.