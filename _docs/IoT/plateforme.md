---
title: Les plateformes industrielles pour l'IoT
description: Présentation des principales plateformes industrielles pour l'IoT
---

# Les plateformes industrielles pour l'IoT

Les plateformes IoT sont des solutions logicielles de  services. Elles centralisent les données et les communications des objets connectés.

Elles s'exécutent sur des serveurs, hébergés dans des infrastructures cloud ([AWS](https://aws.amazon.com/), [Google Cloud](https://cloud.google.com/), [Microsoft Azure](https://azure.microsoft.com/), etc.) ou dans l'infrastructure de l'entreprise.

**Leur objectif est de faciliter la récolte, le stockage et le traitement des indicateurs pour mieux piloter l'environnement connecté, voire automatiser son fonctionnement.**

Ces solutions doivent pouvoir s'adapter aux passages à l'échelle aussi bien au niveau horizontal (nombre d'objets connectés) qu'au niveau vertical (variété des solutions apportées).

Les plateformes IoT permettent une interconnexion avec le système d'information existant de l'entreprise. De ce fait, [**la cybersécurité**](../IoT/cybersecurite) est l'un des enjeux principaux des environnements informatiques. L'acquisition, le stockage et le transfert de données doivent faire l’objet de mesures de sécurisation de bout en bout.<br> 
De nombreuses entreprises ont recours à des plateformes cloud pour cette raison : la cybersécurité est un métier complexe et une lourde responsabilité qui est facile à confier à des prestataires tels que Microsoft, Amazon ou Google mais très difficile à développer et maintenir en interne.


<br>
<div align="left">
<img alt="Plateform IoT" src="images/IOT_79.png" width="500px">
</div>
<br>

> Source image : [digora.com](https://www.digora.com/fr/blog/quest-ce-quune-plateforme-iot-et-comment-choisir-la-bonne)

## Les fonctions des plateformes IoT

On peut diviser les fonctions d’une plateforme IoT en deux catégories : **Métier** et **Technique**.

1 . **Les fonctions « métier »**

  Raisons pour lesquelles est mise en œuvre la solution connectée. Elles sont propres à chaque projet et répondent à des usages et besoins spécifiques : 
  - <span style="font-size: 15px;">Collecte des données ;</span>
  - <span style="font-size: 15px;">Traitement des données en fonction du besoin exprimé ;</span>
  - <span style="font-size: 15px;">Exposition des API pour la connexion à d’autres applications.</span>

<br>
<div align="left">
<img alt="Metier" src="images/IOT_81.png" width="500px">
</div>
<br>

> Source image : [iotjourney.orange.com](https://iotjourney.orange.com/fr-FR/partenaires/partenaires/fusion-as-la-plateforme-iot-e-novact-1618931037544)

2 . **Les fonctions « techniques »**

  Elles s’assurent du bon fonctionnement de la solution : 
  - <span style="font-size: 15px;">Suivi de l’état de communication de chaque équipement, afin de détecter tout comportement anormal ;</span>
  - <span style="font-size: 15px;">Suivi du niveau de batterie, afin de planifier les interventions de remplacement ou recharge ;</span>
  - <span style="font-size: 15px;">Détection des tentatives de piratage ;</span>
  - <span style="font-size: 15px;">Gestion de la mise à jour des objets avec un bootloader, afin de propager les nouveaux firmwares sur le terrain ;</span>
  - <span style="font-size: 15px;">Prise de contrôle et réalisation de diagnostics détaillés afin de repérer et corriger des comportements anormaux ;</span>
  - <span style="font-size: 15px;">Suivi des consommations en données et ressources du parc afin d’optimiser les abonnements (et, encore, de détecter les piratages) ;</span>
  - <span style="font-size: 15px;">Enregistrement et suppression des objets.</span>

<br>
<div align="left">
<img alt="Technique" src="images/IOT_82.png" width="500px">
</div>
<br>

> Source image : [iotjourney.orange.com](https://iotjourney.orange.com/fr-FR/partenaires/partenaires/fusion-as-la-plateforme-iot-e-novact-1618931037544)

# Les 4 plateformes IoT les plus populaires

Gartner définit le marché mondial des plateformes de l'Internet industriel des objets (IIoT) comme *"un ensemble de capacités logicielles middleware intégrées, avec un marché multivendeur, afin de faciliter et d'automatiser la prise de décision en matière de gestion des actifs de manière durable et dans le cadre d'un déploiement à l'échelle mondiale au sein des industries à forte intensité d'actifs"*.

Les plateformes IIoT offrent également une visibilité et un contrôle opérationnels pour les usines, les infrastructures et les équipements. Les plateformes IIoT mondiales sont déployées pour résoudre les défis liés aux opérations industrielles, tels que : la durabilité, l'automatisation, les opérations à distance, la transformation des technologies opérationnelles (OT) et des applications industrielles, l'évolutivité à l'échelle mondiale.

> Source : [gartner.com](https://www.gartner.com/reviews/market/global-industrial-iot-platforms)

## AWS IoT Core
[AWS IoT Core](https://aws.amazon.com/fr/iot-core/) est un service cloud proposé par Amazon Web Services (AWS) qui facilite la gestion et la communication des appareils connectés à l'Internet des objets (IoT). Il offre une plateforme robuste pour connecter, gérer et échanger des données entre des appareils IoT de manière sécurisée et évolutive.

Les principales fonctionnalités d'AWS IoT Core comprennent la gestion des appareils, la sécurité des communications, la mise à l'échelle automatique, la gestion des identités, et la possibilité de traiter et d'analyser les flux de données générés par les appareils connectés. Cela permet aux développeurs de créer des applications IoT fiables et sécurisées tout en bénéficiant de la flexibilité et de la puissance du cloud computing d'AWS.

<br>
<div align="left">
<img alt="Amazon" src="images/IOT_83.png" width="500px">
</div>
<br>

> Source image : [docs.aws.amazon.com](https://docs.aws.amazon.com/fr_fr/iot/latest/developerguide/what-is-aws-iot.html)

## Microsoft Azure IoT

[Microsoft Azure IoT](https://azure.microsoft.com/en-us/free/iot/) est la solution cloud d'IoT proposée par Microsoft. Elle offre une gamme de services et de fonctionnalités pour connecter, surveiller, et gérer des dispositifs IoT à l'échelle mondiale.

Les principaux composants d'Azure IoT incluent Azure IoT Hub, Azure IoT Central, Azure Sphere, Azure IoT Edge, et d'autres services complémentaires. Azure IoT Hub, par exemple, est un service de passerelle de messages qui facilite la communication bidirectionnelle entre les applications cloud et les dispositifs IoT. Azure IoT Edge permet de déployer des charges de travail d'analyse et de traitement des données directement sur les appareils, offrant ainsi une architecture de traitement distribué.

<br>
<div align="left">
<img alt="Microsoft" src="images/IOT_84.png" width="700px">
</div>
<br>

> Source image : [learn.microsoft.com](https://learn.microsoft.com/fr-fr/azure/architecture/reference-architectures/iot)

## IBM Watson IoT

[IBM Watson IoT](https://internetofthings.ibmcloud.com/) est une offre de services et de plateformes d'IoT proposée par IBM. Elle intègre des capacités avancées d'analyse, d'IA (intelligence artificielle), et de gestion des appareils IoT pour aider les entreprises à collecter, gérer et analyser des données provenant d'une multitude d'appareils connectés.

On y retrouve les mêmes types de services que ceux proposés par Microsoft Azure IoT. 

<br>
<div align="left">
<img alt="IBM" src="images/IOT_85.png" width="500px">
</div>
<br>

> Source image : [cloud.ibm.com](https://cloud.ibm.com/catalog/services/internet-of-things-platform#about)

## PTC Kepware et ThingWorx

[PTC](https://www.ptc.com/fr) propose la plateforme [ThingWorx](https://www.ptc.com/fr/products/thingworx) pour l'IoT Industriel. ThingWorx offre des fonctionnalités pour la création d'applications IoT complexes, depuis la connexion sécurisée des appareils jusqu'à l'analyse avancée des données. La plateforme se compose de plusieurs composants clés, tels que ThingWorx Foundation, ThingWorx Analytics, ThingWorx Industrial Connectivity ... 

<br>
<div align="left">
<img alt="PTC" src="images/IOT_86.png" width="600px">
</div>
<br>

> Source image : [www.pngitem.com](https://www.pngitem.com/download/iTTRobh_transparent-fast-forward-button-png-machine-learning-big/)

Parmi ces composants [Kepware](https://www.kepware.fr/) est une plateforme de connectivité conçue pour faciliter l'intégration de divers équipements et systèmes hétérogène dans l'environnement de l'automatisation industrielle. Il permet aux entreprises d'optimiser leurs opérations en centralisant la collecte de données et en fournissant une interface standardisée pour les applications de supervision et de contrôle.

<br>
<div align="left">
<img alt="Kepware" src="images/IOT_88.jpg" width="450px">
</div>
<br>

> Source image : [catapultsoftware.com](https://www.catapultsoftware.com/products/kepware-kepserverex.html)

### Quelques compléments concernant Kepware

- Kepware est un serveur de communication OPC (OLE for Process Control) qui agit comme un intermédiaire entre les dispositifs de terrain (tels que des capteurs, actionneurs, et autres équipements industriels) et les applications de niveau supérieur, telles que des systèmes de contrôle, des logiciels SCADA (Supervisory Control and Data Acquisition), des systèmes MES (Manufacturing Execution System) et d'autres applications industrielles.

- Kepware offre la prise en charge de nombreux protocoles de communication industriels, la connectivité avec une large gamme d'équipements et de dispositifs, la possibilité de collecter et d'agréger des données provenant de sources diverses, ainsi que des fonctionnalités de sécurité pour assurer une communication fiable et sécurisée.

### Quelques compléments concernant ThingWorx

- **ThingWorx Foundation** : Il s'agit du cœur de la plateforme, fournissant des outils de développement, de connectivité, de gestion des appareils et de création d'interfaces utilisateur pour les applications IoT.

- **ThingWorx Analytics** : Cette composante propose des capacités d'analyse prédictive et prescriptive pour permettre aux entreprises de tirer des insights significatifs à partir des données IoT.

- **ThingWorx Industrial Connectivity** : Il offre des fonctionnalités pour la connectivité avec une variété d'appareils et de protocoles industriels, facilitant l'intégration de la plateforme avec des équipements existants.

<br>
<div align="left">
<img alt="ThingWorx" src="images/IOT_87.png" width="500px">
</div>
<br>

> Source image : [manufacturingtomorrow.com](https://www.manufacturingtomorrow.com/article/2017/07/the-importance-of-the-edge-for-the-industrial-internet-of-things-in-the-energy-industry/10056/)

# Quelques autres plateformes IoT

## TagoIO

[TagoIO](https://tago.io/) est une plateforme IoT basée sur le cloud qui permet aux développeurs et aux entreprises de créer, déployer et gérer des applications IoT de manière efficace. La plateforme TagoIO propose des fonctionnalités pour la collecte de données en temps réel, la visualisation, l'analyse et la gestion des appareils connectés.

TagoIO fournit une infrastructure cloud pour connecter des appareils IoT, collecter des données générées par ces appareils, et créer des tableaux de bord interactifs pour visualiser ces données. La plateforme offre également des outils de développement qui permettent aux utilisateurs de créer des applications personnalisées en utilisant des langages de programmation tels que JavaScript.

La flexibilité de TagoIO en termes de connectivité, de visualisation des données et de développement d'applications en fait une option populaire pour ceux qui souhaitent créer des solutions IoT sur mesure sans avoir à gérer l'infrastructure sous-jacente.

<br>
<div align="left">
<img alt="TagoIO" src="images/IOT_89.png" width="500px">
</div>
<br>

> Source image : [solutionbook.io](https://solutionbook.io/product/tagoio-iot-cloud-platform/)

**Principales caractéristiques de TagoIO :**

- **Connectivité IoT :** TagoIO facilite la connexion de divers types d'appareils IoT, qu'il s'agisse de capteurs, d'actionneurs ou d'autres dispositifs.

- **Collecte et stockage de données :** La plateforme permet de collecter des données en temps réel à partir des appareils connectés et de les stocker de manière sécurisée dans le cloud.

- **Visualisation des données :** TagoIO offre des outils de création de tableaux de bord personnalisés pour visualiser les données IoT de manière claire et interactive.

- **Analyse des données :** Les utilisateurs peuvent appliquer des analyses aux données collectées pour obtenir des informations exploitables.

- **Gestion des appareils :** TagoIO facilite la gestion à distance des appareils IoT, notamment la configuration, la mise à jour du firmware, et d'autres opérations.

## Thingsboard

[ThingsBoard](https://thingsboard.io/) est une plateforme IoT open source qui offre des fonctionnalités pour la gestion des appareils, la collecte de données, la visualisation et l'analyse dans le contexte de l'IoT. Contrairement à certaines plateformes IoT propriétaires, ThingsBoard est conçu pour être extensible et personnalisable, permettant aux développeurs de l'adapter à leurs besoins spécifiques.

ThingsBoard est souvent utilisé par les développeurs et les entreprises qui cherchent à déployer des solutions IoT personnalisées et à exploiter les avantages de l'open source pour répondre à des besoins spécifiques de connectivité et de gestion des données dans leurs projets IoT.

<br>
<div align="left">
<img alt="Thingsboard" src="images/IOT_91.png" width="500px">
</div>
<br>

> Source image : [thingsboard.io](https://thingsboard.io/docs/samples/exxn/exxn/)

**Principales caractéristiques de ThingsBoard :**

- **Gestion des appareils :** ThingsBoard permet la gestion centralisée des appareils IoT, facilitant l'ajout, la configuration et la surveillance des dispositifs connectés.

- **Collecte de données :** La plateforme prend en charge la collecte de données en temps réel provenant de divers capteurs et appareils IoT, permettant aux utilisateurs de stocker ces données de manière sécurisée.

- **Visualisation des données :** ThingsBoard propose des outils de visualisation des données sous forme de tableaux de bord interactifs, graphiques et widgets personnalisables, permettant aux utilisateurs de surveiller les données IoT de manière visuelle.

- **Analyse des données :** La plateforme offre des capacités d'analyse pour aider les utilisateurs à tirer des insights significatifs à partir des données IoT collectées.

- **Connectivité et Protocoles :** ThingsBoard prend en charge plusieurs protocoles de communication IoT tels que MQTT, CoAP, et HTTP, facilitant ainsi la connectivité avec une variété d'appareils.

- **Extensibilité :** ThingsBoard est extensible et permet aux développeurs d'ajouter des fonctionnalités personnalisées grâce à des plugins et des API.

- **Open Source :** En tant que plateforme open source, ThingsBoard offre une flexibilité totale aux utilisateurs pour personnaliser, étendre et contribuer au développement de la plateforme.

# Les bases de données IoT 

Cette section, loin d'être exhaustive, présente les bases de données IoT les plus utilisées.

## InfluxDB

[InfluxDB](https://www.influxdata.com/products/influxdb-overview/) est une base de données open source conçue spécifiquement pour le stockage et la récupération de données de séries temporelles. Les données de séries temporelles sont des enregistrements qui sont associés à des horodatages, tels que des mesures de capteurs IoT, des données météorologiques, des métriques de performance, etc.

<br>
<div align="left">
<img alt="InfluxDB" src="images/IOT_92.png" width="500px">
</div>
<br>

> Source image : [influxdata.com](https://www.influxdata.com/blog/influxdb-3-0-system-architecture/)

**Quelques pécisions supplémentaires concernant InfluxDB :**

- **Base de Données de Séries Temporelles :** InfluxDB est conçu spécifiquement pour stocker et interroger des données de séries temporelles. Il offre une structure optimisée pour gérer des enregistrements temporels massifs de manière efficace.

- **Horodatage :** Chaque point de données stocké dans InfluxDB est associé à un horodatage, ce qui facilite l'analyse des tendances temporelles.

- **SQL-Like Query Language :** InfluxDB utilise un langage de requête similaire au SQL, appelé InfluxQL, permettant aux utilisateurs d'interroger et d'analyser les données de manière intuitive.

- **Retention Policy :** InfluxDB offre la possibilité de définir des politiques de rétention pour gérer automatiquement la suppression des données obsolètes, aidant ainsi à gérer l'espace de stockage.

- **Support pour MQTT et d'autres Protocoles :** InfluxDB prend en charge des protocoles tels que MQTT, facilitant l'intégration avec des appareils IoT pour la collecte de données.

- **Telegraf :** [Telegraf](https://www.influxdata.com/time-series-platform/telegraf/) est un agent de collecte de données qui peut être utilisé avec InfluxDB pour collecter, agréger et envoyer des données provenant de diverses sources.

- **Open Source :** InfluxDB est open source, ce qui signifie que son code source est accessible et modifiable par la communauté, offrant une flexibilité totale aux utilisateurs.

## Elasticsearch

[Elasticsearch](https://www.elastic.co/fr/elasticsearch) est une base de données de recherche et d'analyse distribuée, open source, qui est souvent utilisée pour le stockage, la recherche et l'analyse de grands ensembles de données, y compris des données de séries temporelles dans le contexte de l'Internet des objets (IoT). Bien que la principale force d'Elasticsearch soit la recherche textuelle, elle est également capable de traiter des données structurées, par exemple de séries temporelles.

Elasticsearch est fréquemment utilisé dans des environnements où la recherche et l'analyse de données non structurées, textuelles ou de séries temporelles sont cruciales, comme dans le domaine de la surveillance IoT, des journaux (logs) d'applications, et d'autres cas d'utilisation nécessitant une recherche et une analyse rapides.

<br>
<div align="left">
<img alt="Elasticsearch" src="images/IOT_93.png" width="500px">
</div>
<br>

> Source image : [elastic.co](https://www.elastic.co/fr/blog/industrial-internet-of-things-iiot-with-the-elastic-stack)

**Quelques précisions supplémentaires concernant Elasticsearch :**

- **Moteur de Recherche :** Elasticsearch est basé sur Apache Lucene et est conçu comme un moteur de recherche distribué, capable de fournir des fonctionnalités de recherche textuelle puissantes et rapides.

- **Séries Temporelles :** Bien qu'Elasticsearch ne soit pas spécifiquement conçu pour les séries temporelles, il peut être utilisé pour stocker, indexer et interroger des données de séries temporelles grâce à son modèle de données flexible.

- **JSON (JavaScript Object Notation) :** Elasticsearch utilise des documents JSON pour stocker les données. Les données de séries temporelles peuvent être représentées sous forme de documents JSON avec des champs tels que l'horodatage et les valeurs associées.

- **Analyse en Temps Réel :** Elasticsearch prend en charge l'indexation et la recherche en temps réel, ce qui le rend adapté à des cas d'utilisation où les données sont continuellement mises à jour.

- **Agrégations :** Elasticsearch offre des fonctionnalités d'agrégation qui permettent d'analyser et de résumer les données de manière flexible.

- **Évolutivité et Distribution :** Elasticsearch est conçu pour être distribué, ce qui signifie qu'il peut être facilement mis à l'échelle horizontalement pour gérer de grandes quantités de données et de demandes de recherche.

- **Stack ELK :** Elasticsearch est souvent utilisé en conjonction avec Logstash et Kibana pour former le stack ELK (Elasticsearch, Logstash, Kibana), fournissant une solution complète pour la collecte, le traitement, la recherche et la visualisation de données.

## Références
