---
title: Quelques liaisons et protocoles filaires
description: Technologies de communication filaire couramment utilisées dans l'IoT
---

# Quelques liaisons et protocoles filaires

Les technologies de communication filaires préexistaient à l'IoT. Elles offrent **une connectivité plus fiable et plus stable et en général un meilleur débit** que celles reposant sur la radiofréquence car elles sont nettement moins sensibles au bruit, ce qui peut être essentiel pour certaines applications.

Bien évidemment, ces avantages proviennent du fait que les technologies de communication filaires ne sont pas soumises à la contrainte de frugalité énergétique qui est prioritaire pour [la plupart des technologies de communication radiofréquence](protocole_rf) plus récentes et pour la plupart conçues pour l'IoT. Un objet câblé sur un réseau devra généralement être alimenté sur secteur.

Nous allons passer brièvement en revue quelques unes de ces technologies parmi celles qui sont les plus utilisées dans le contexte de l'IoT.

## Le Courant Porteur en Ligne (CPL)

D'après [Wikipédia](https://fr.wikipedia.org/wiki/Courants_porteurs_en_ligne) :

La communication par **courants porteurs en ligne** (ou CPL) permet de construire un réseau informatique sur le réseau électrique d'une habitation ou d'un bureau, voire d'un quartier ou groupe de bureaux.
On peut ainsi étendre un réseau local existant ou partager un accès internet existant via les prises électriques grâce à la mise en place de boîtiers spécifiques. Dans l’état actuel de la technique, les débits atteints sont compris entre 14 et 1 200 Mbit/s.

Ce protocole constitue un pas de plus vers les réseaux électriques intelligents, et l'Internet de l'énergie. Il permet un meilleur auto-contrôle et monitoring du réseau de distribution électrique, et une gestion énergétique fine, y compris pour la gestion contrôlée de l'éclairage intérieur ou extérieur, la charge énergétique des véhicules électriques, et d'autres applications des « réseaux de demain » (gestion de production et microproduction décentralisées d'énergies irrégulières de type solaire/éolien), etc. Le CPL G3 est utilisé par Enedis pour la mise en place du compteur communicant Linky.

|Adaptateurs CPL - Wi-Fi Devolo|Mise en œuvre du CPL|
|:-:|:-:|
|<img alt="CPL Devolo" src="images/cpl devolo.jpg" width="350px">|<img alt="Réseau CPL" src="images/reseauCPL_thumb.jpg" width="350px">|
|Source image : [Devolo](https://www.devolo.fr/produits/magic-cpl)|Source image : [Le blog d'Eric](https://www.ericboisseau.com/a-la-maison-mieux-vaut-le-cpl-au-wifi-pour-le-reseau-informatique/)|

Le principe du CPL consiste à superposer au courant électrique alternatif de 50 ou 60 Hz un signal à plus haute fréquence et de faible énergie. Ce deuxième signal se propage sur l’installation électrique et peut être reçu et décodé à distance. Ainsi le signal CPL est reçu par tout récepteur CPL de même catégorie se trouvant sur le même réseau électrique. Cette façon de faire comporte cependant un inconvénient : le réseau électrique n'est pas adapté au transport de hautes fréquences car il n'est pas blindé. En conséquence, la plus grande partie de l'énergie injectée par le modem CPL est rayonnée sous forme d'onde radio.

## Les liaisons série asynchrones

Un **UART**, pour **Universal Asynchronous Receiver Transmitter**, est un émetteur-récepteur asynchrone universel. En langage courant, c'est le composant utilisé pour faire la liaison entre un ordinateur ou un microcontrôleur et un port série. L'ordinateur envoie les données en parallèle (autant de fils que de bits de données). Il faut donc transformer ces données pour les faire passer à travers une liaison série qui utilise un seul fil pour faire passer tous les bits de données (source : [Wikipedia](https://fr.wikipedia.org/wiki/UART)).

La liaison série asynchrone définit un **protocole** de transfert et différentes **normes** le mette en œuvre. Les normes précisent :
 - Des niveaux électriques ;
 - Des préconisations physiques (type connecteur, brochage, distance de communication, etc.) ; 
 - Des signaux de contrôle.

### Exemples de normes reposant sur UART : RS232/485 – Modbus/ASCII

Les PC de bureau sont tous équipés d’au moins un port série (COM1) qui est régi par la norme RS232C. Il est nécessaire d'avoir un boîtier d'adaptation (interface RS232/ RS485) des niveaux électriques. Dans certains cas, comme pour les PC dits "industriels", lorsqu'une carte supportant le standard [RS485](https://fr.wikipedia.org/wiki/EIA-485) peut être installée, le boîtier d'adaptation n'est pas nécessaire.<br>
Les données transmises via une interface RS485 utilisent généralement le protocole [MODBUS](https://fr.wikipedia.org/wiki/Modbus). Un périphérique [RS232](https://fr.wikipedia.org/wiki/RS-232) peut également utiliser les protocoles texte ([ASCII](https://fr.wikipedia.org/wiki/American_Standard_Code_for_Information_Interchange)).

<br>
<div align="left">
<img alt="Modbus" src="images/IOT_23.jpg" width="500px">
</div>
<br>

> Source image : [iotindustriel.com](https://www.vscom.de/brief_modgate.htm)

## Universal Serial Bus (USB)

Au cours de la dernière décennie, la norme du bus série universel ([USB](https://fr.wikipedia.org/wiki/USB)) a été adoptée par les concepteurs de dispositifs industriels et grand public comme interface de choix pour permettre la connectivité à d'autres applications, en raison de sa facilité d'utilisation, de sa fonctionnalité plug-and-play et de sa robustesse. Elle est aujourd'hui l'une des interfaces M2M les plus largement adoptées.

|USB|Les différents connecteurs USB|
|:-:|:-:|
|<img alt="USB" src="images/usb_logo.png" width="250px">|<img alt="Connecteurs USB" src="images/IOT_25.png" width="350px">|
|Source : [SOSPC](https://sospc.name/toutes-les-normes-usb/)|Source image : [digikey.fr](https://www.digikey.fr/fr/articles/get-started-with-usb-c-power-delivery)|

## Controller Area Network (CAN)

**[Le protocole](https://fr.wikipedia.org/wiki/Protocole_CAN) et [le bus](https://fr.wikipedia.org/wiki/Bus_de_donn%C3%A9es_CAN) CAN**, pour "Controller Area Network" ont été initialement développés dans le cadre de la conception automobile. Depuis 1960 le nombre de capteurs augmente sans cesse dans les véhicules et donc le câblage aussi (2000 m en 1995). Pour parer à cela, la société BOSCH invente en 1983 un bus série, asynchrone, multiplexé, sur lequel des messages circulent tous sur une même paire de fils. Moins de fils implique moins de maintenance, des coûts plus bas et une plus grande fiabilité. L’ampleur du marché automobile a fait baisser les coûts ce qui a permis la diffusion du bus CAN dans les autres industries (médical, aéronautique, machines industrielles …).

Son fonctionnement de type multi-maîtres apporte redondance et robustesse ; tous les nœuds du réseau ont les mêmes droits et s’occupent de la détection et de la gestion des erreurs. Les nœuds disposent chacun de leur propre horloge et le protocole CAN est conçu pour qu’ils ajustent celle-ci régulièrement de sorte à rester mutuellement synchrones. Par conséquent, des nœuds peuvent être ajoutés au réseau sans qu'il n'y ait rien à modifier tant au niveau logiciel que matériel et sans qu’il y ait besoin d’une unité centrale pour les coordonner. 

<br>
<div align="left">
<img alt="Bus CAN pour l'automobile" src="images/can_auto.jpg" width="800px">
</div>
<br>

> Source image : [Quora](https://www.quora.com/What-is-a-CAN-bus)

## Les bus et protocoles série courte portée de l'embarqué

Les objets de l'IoT sont avant tout des systèmes électroniques assemblés sur des circuits formés sur des plaques en résine époxy (en anglais "PCB" pour "Printed Circuit Board") comme avant eux les ordinateurs de bureau, les téléviseurs, les cartes contrôleurs de chaudières, etc. Les concepteurs des premiers systèmes électroniques complexes ont bien évidemment dû mettre au point des protocoles et bus de communications pour interconnecter des circuits intégrés à courtes distances sur leur PCB. Les plus utilisés sont des bus série : One-Wire, I2C (ou Two-Wire), I3C et SPI.

### Le bus One-Wire

[Le bus et le protocole **One-Wire**](https://fr.wikipedia.org/wiki/1-Wire) (littéralement "Un-câble") est également connu sous le nom de "bus Dallas" ou "1-Wire". One-Wire fut conçu par Dallas Semiconductor qui permet de connecter en série, mais aussi parallèle ou en étoile, des composants avec seulement deux fils (un fil de données et un fil de masse).<br> 
La communication sur le bus 1-Wire est caractérisée par un ensemble de « pulses » ou changements d’états. L’état par défaut de la ligne data est +5V, ce qui permet d’alimenter les différents composants à partir de la ligne data en mode parasite. Les périphériques que l'on y connecte ont tous une adresse unique et non modifiable, codée sur 64 bits.  Son débit maximum théorique est de 15.4 kbps sur quelques dizaines de centimètres.

Une architecture One-Wire : 

<br>
<div align="left">
<img alt="Architecture One-Wire" src="images/One-Wire_Architecture.png" width="600px">
</div>
<br>

> Source image : [Zephyr](https://docs.zephyrproject.org/latest/hardware/peripherals/w1.html)

Le fait de ne disposer que d'une seule ligne pour alimenter et communiquer est bien sûr problématique pour la résistance au bruit, ce qui limite la portée et/ou le débit de ce bus.

### Le bus I2C (ou Two-Wire)

[Le bus et le protocole **I2C ou IIC**](https://fr.wikipedia.org/wiki/I2C), pour "Inter-Integrated Circuit", ont été inventés par Philips. Tout comme le bus SPI, le bus série I2C a été créé pour fournir un moyen simple de transférer des informations numériques entre des capteurs, des actuateurs, des afficheurs et un microcontrôleur.<br>
Il nécessite deux lignes (c'est pourquoi on le trouve aussi parfois désigné par "Two-Wire"), SDA ("Serial Data", ligne de données) et SCL ("Serial Clock", ligne de synchronisation / horloge partagée). Les périphériques que l'on y connecte doivent tous avoir une adresse identifiante unique et statique (non modifiable, jusqu'à 2<sup>7</sup> = 128 adresses possibles sur un même bus). Son débit maximum théorique est de 3 Mbps sur quelques dizaines de centimètres.

Une architecture I2C : 

<br>
<div align="left">
<img alt="Architecture I2C" src="images/I2C_Architecture.png" width="600px">
</div>
<br>

> Source image : [Wikipédia](https://fr.wikipedia.org/wiki/I2C)

### Le bus I3C

[Le bus et le protocole **I3C**](https://en.wikipedia.org/wiki/I3C_(bus)), pour "Improved Inter-Integrated Circuit", succèdent à l'I2C. Les premiers microcontrôleurs intégrant des contrôleurs de bus série I3C datent de 2023. Entre autres améliorations, un bus I3C permet des débits théoriques de 30 Mbps (dix fois plus que l'I2C, mais avec une portée inchangée de quelques dizaines de centimètres) et est capable d'attribuer dynamiquement des adresses aux périphériques que l'on y connecte. Il  gère aussi les interruptions en provenance de ses périphériques esclaves sans monopoliser d'autres broches du microcontrôleur maître.<br>
Les constructeurs s'arrangent bien évidemment pour assurer une compatibilité ascendante entre I2C et I3C (un périphérique I2C pourra être connecté à un bus I3C et y fonctionner). Pour aller plus loin, voir [cet article.](https://www.totalphase.com/blog/2022/05/i2c-vs-i3c-what-are-the-differences/)

Comparaison entre les architectures I3C et I2C :

<br>
<div align="left">
<img alt="Comparaison I2C et I3C" src="images/I2C_versus_I3C.png" width="650px">
</div>
<br>

> Source image : [MIPI Alliance](https://www.mipi.org/)

### Le bus SPI

[Le bus **SPI**](https://fr.wikipedia.org/wiki/Serial_Peripheral_Interface), pour "Serial Peripheral Interface", a été inventés par Motorola.<br>
SPI n'utilise pas de protocole standard et transfère uniquement des paquets de données, ce qui la rend idéale pour le transfert de longs flux de données.

Un bus SPI permet la connexion en série entre un microcontrôleur maître et plusieurs circuits périphériques esclaves disposant d’interfaces compatibles avec quatre fils de liaison :
* **SCLK** pour "Serial Clock", une ligne d'horloge pour synchroniser maître et esclaves.
* **MOSI** pour "Master Out, Salve In", pour transmettre les signaux du maître vers l'esclave sélectionné.
* **MISO** pour "Master In, Salve Out", pour transmettre les signaux de l'esclave sélectionné vers le maître. Lorsqu'un périphérique SPI souhaite faire remonter une interruption au microcontrôleur maître, il utilise aussi cette ligne.
* **SS** pour "Slave Select", afin de l'adresser (un seul esclave est activé à chaque transaction sur le bus). de ce fait SPI n'utilise pas de protocole compliqué nécessitant un adressage et une vérification d'état. Il s'agit plutôt d'une interface de base pour un transfert de données rapide, sans le traitement de programmation associé à un bus d'interface plus sophistiqué.

Par comparaison avec l'I2C, le bus SPI offre des débits plus élevés (jusqu'à 60 Mbps) en contrepartie de sa connectique plus complexe, mais sa portée reste de quelques dizaines de centimètres. Des variantes du SPI, le **Dual SPI** et le **Quad SPI** ont également été développées ; elles consistent à doubler ou quadrupler les lignes MISO et MOSI pour augmenter d'autant le débit binaire. Elles servent à la gestion de périphériques tels que des puces de mémoire flash ou des afficheurs de haute résolution.  

Exemple d'une architecture SPI avec trois stations connectées :

<br>
<div align="left">
<img alt="Architecture SPI" src="images/SPI_Architecture.png" width="450px">
</div>
<br>

> Source image : [Wikipédia](https://fr.wikipedia.org/wiki/Serial_Peripheral_Interface)

## Références