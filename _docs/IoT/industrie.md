---
title: IIoT et industrie 4.0
description: Chapitre d'introduction à l'internet des objets industriel.
---

# Qu'est-ce que l'IoT industriel ?

D’une certaine manière, l’Internet des Objets donne « bras et jambes » à Internet pour évoluer dans le monde physique. Cela fait naître de nouvelles possibilités de communication, de détection (collecte de données), de raisonnement (utilisation de ces données), puis d’actions physiques au moyen d’actionneurs. Des boucles de contrôle d’un type nouveau, reposant sur le réseau, peuvent exploiter si nécessaire d’importantes ressources de calcul distantes. En rendant possibles ces nouvelles boucles de contrôle, puis en les mettant en œuvre, l’Internet des Objets transforme radicalement l’environnement notamment grâce aux :

- Capteurs ;
- Actionneurs ;
- Boucles de contrôle cyber-physiques ;
- Composants robotiques.

Le but de l'Internet des Objets Industriel ou **IIoT** est, par la transformation digitale, de développer des processus de production hautement automatisés. 

**L’industrie 4.0** permet d'améliorer les **processus** et **méthodes de maintenance** des équipements. Parmi les **indicateurs de performance**, la **maintenabilité** est essentielle. La maintenabilité d’un équipement est la capacité pour des composants ou des applications à être maintenus, de manière cohérente et à moindre coût, en état de fonctionnement non dégradé. Plus généralement, dans l'industrie, ce terme exprime la capacité d'un système à être simplement et rapidement réparé de sorte à diminuer les temps et les coûts d'intervention.

La **maintenance prédictive** améliore la maintenabilité. Il est devenu possible de détecter les problèmes potentiels pendant que les équipements fonctionnent pour la fabrication et avant avant leur défaillance, ce qui permet aux industriels de les corriger avant qu'une panne ne se produise. Ce changement de paradigme, qui consiste à résoudre les problèmes avant qu'ils ne surviennent, se répand rapidement et permet aux industriels de réduire leurs coûts et d'améliorer leurs relations client.

La mise en œuvre de l’IIoT, qui combine la connectivité et l'analyse prédictive des équipements de production, devient donc un enjeu majeur pour les industriels. Des déploiements d'applications de maintenance industrielle IIoT sont tout naturellement envisagés par la quasi-totalité des marchés et secteurs d’activité.

## La collecte de données : data-lake vs edge AI

Les dispositifs IIoT collectent une quantité importante de données provenant de diverses sources. Elles peuvent inclure des informations sur la température, la pression, l'humidité, la performance des machines, la consommation d'énergie, etc. Afin de stocker et d'agréger ces données les services informatiques des industriels doivent mettre en place une stratégie de [Data Management](https://www.ptc.com/fr/technologies/plm/product-data-management) et de stockage via un [data lake (littéralement "lac de données")](https://learn.microsoft.com/fr-fr/azure/architecture/data-guide/scenarios/data-lake).

<br>
<div align="left">
<img alt="Data Lake" src="images/IOT_52.jpg" width="500px">
</div>
<br>

> Source image : [learn.microsoft.com](https://learn.microsoft.com/fr-fr/azure/architecture/data-guide/scenarios/data-lake)

Le data lake est une infrastructure de base de donnée de (très) grande capacité qui peut mémoriser les mesures "brutes" issues des capteurs, sans aucun pré-traitement. Une autre approche se développe chez les industriels, l'[Edge AI](https://www.digikey.fr/en/maker/projects/what-is-edge-ai-machine-learning-iot/4f655838138941138aaad62c170827af) qui consiste à pré-traiter les données **au plus près du système qui les produit** plutôt que "remplir" un datalake en vue de leur post-traitement.


## La cryptographie pour l'IIoT

En raison de la nature critique des opérations industrielles, la sécurité est une préoccupation majeure dans l'IIoT. Des protocoles de sécurité tels que l'authentification, le chiffrement et la gestion des accès sont essentiels pour protéger les données et les systèmes.

Les appareil connectés sont potentiellement vulnérables et exposés à toutes sortes de risques, par exemple :

- Ils peuvent être volés, altérés, modifiés de manière malveillante ou remplacés car il s'agit souvent de dispositifs portables.
- Ils étendent la surface d'attaque du réseau en y ajoutant davantage de points d'entrées /sorties potentiels. Par exemple, ils peuvent avoir plusieurs interfaces réseau, dont une connectée à Internet.
- Ils peuvent introduire des malwares dans le réseau via le dispositif lui-même ou via les outils utilisés pour construire, développer ou surveiller le dispositif (IDE, logiciel ...).
- En cas de dysfonctionnement, ils peuvent perturber d'autres équipements entraînant un impact sur leurs performances ou même des arrêts de production.
- Ils peuvent contenir des vulnérabilités ciblées par des malwares conduisant à leur dysfonctionnement.
- Ils peuvent utiliser des protocoles peu sécurisés, permettant à un attaquant de prendre leur contrôle ou d'accéder à des données confidentielles.
- Etc.

<br>
<div align="left">
<img alt="Security" src="images/IOT_58.jpg" width="350px">
</div>
<br>

> Source image : [cheapsslsecurity.com](https://cheapsslsecurity.com/blog/iot-security-understanding-pki-role-in-securing-internet-of-things/)

La contre-mesure la plus élémentaire pour mieux sécuriser le réseau, qui date de bien avant l'IIOT, est **la mise en œuvre de [méthodes cryptographiques asymétriques](https://fr.wikipedia.org/wiki/Cryptographie_asym%C3%A9trique)** pour signer / authentifier les données sensibles et pour les crypter / décrypter afin qu'elles ne puissent pas être exploitées par de tierces personnes non autorisées à les consulter.
Dans ce contexte, les **Infrastructure à clés publiques (Public key infrastructure)** jouent un rôle de premier plan.

La **Public Key Infrastructure (PKI)**, en français "Infrastructure à Clé Publique", est un ensemble de technologies, de politiques et de procédures qui permettent la gestion sécurisée des clés cryptographiques (clés publiques et privées) ainsi que des certificats numériques. La PKI joue un rôle essentiel dans la sécurisation des communications électroniques, notamment sur Internet.

Les principaux éléments qui composent une PKI sont :

- Des **certificats numériques**, documents électroniques qui associent une clé publique à l'identité d'une entité (personne, serveur, dispositif). Ils sont émis par des autorités de certification (CA) de confiance.

- Des **autorités de certification (CA)**, entités de confiance qui émettent les certificats numériques. Elles valident l'identité des titulaires de certificats avant de les émettre, ce qui assure une certaine garantie de la fiabilité des informations.

- Des **répertoires**, bases de données qui stockent les certificats numériques et d'autres informations liées à la PKI. Ils peuvent être utilisés pour rechercher des certificats et vérifier leur validité.

- Des **autorités d'enregistrement (RA)** responsables de la vérification de l'identité des personnes ou des entités demandant un certificat. Elles agissent comme intermédiaires entre les utilisateurs finaux et les autorités de certification.

- Des **infrastructure de Gestion des Clés (KMI)** chargées de générer, stocker et distribuer les clés cryptographiques. Elles permettent également la révocation des clés en cas de besoin.

La PKI est largement utilisée pour sécuriser les transactions en ligne, les communications par e-mail, l'accès aux réseaux privés, et d'autres applications nécessitant une sécurité renforcée. Elle est essentielle pour la mise en œuvre de protocoles de sécurité tels que SSL/TLS (pour sécuriser les connexions HTTPS), S/MIME (pour sécuriser les e-mails), et d'autres applications utilisant la cryptographie à clé publique.

<br>
<div align="left">
<img alt="PKI" src="images/IOT_59.png" width="450px">
</div>
<br>

> Source image : [www.embedded.com](https://www.embedded.com/iot-security-hinges-on-effective-device-enrollment-with-public-key-infrastructure/)



### Exemple : le Wi-Fi Protected Access for Enterprise (WPA-E)
Le Wi-Fi est un protocole de communication largement utilisé chez les industriels notamment pour connecter le matériel informatique (ordinateur de bureau, ordinateur portable, imprimante, etc.) mais aussi les robots industriels. Le WPA-E (pour "Wi-Fi Protected Access for Enterprise") est le mode de connexion Wi-Fi requis pour répondre aux contraintes de cybersécurité évoquées dans le paragraphe précédent. Il utilise le protocole RADIUS (pour "Remote Authentication Dial-in User Service") pour gérer l'authentification des utilisateurs. Un serveur RADIUS respecte la norme IEEE 802.1x, par laquelle les utilisateurs sont authentifiés sur la base de leurs certificats de compte.

<br>
<div align="left">
<img alt="Radius" src="images/IOT_54.png" width="500px">
</div>
<br>

> Source image : [techbast.com](https://techbast.com/2015/05/setup-radius-server-2008-r2-for-wirelesswpawpa2-enterprise.html)

Le **Extensible Authentication Protocol** (EAP) est un protocole de communication réseau embarquant de multiples méthodes d'authentification.

En particulier, et parmi celles-ci, [**EAP-TLS (Extensible Authentication Protocol-Transport Layer Security)**](https://fr.wikipedia.org/wiki/Extensible_Authentication_Protocol) utilise deux certificats pour la création d'un tunnel sécurisé qui permet ensuite l'identification côté serveur et côté client.

<br>
<div align="left">
<img alt="EAP-TLS" src="images/IOT_55.png" width="400px">
</div>
<br>

> Source image : [wikipedia.org](https://fr.wikipedia.org/wiki/Extensible_Authentication_Protocol)

EAP-TLS est l'un des protocoles EAP que WPA-E peut utiliser pour établir une authentification sécurisée des clients dans un réseau sans fil. Le choix du protocole EAP dépend des politiques de sécurité de l'organisation et des exigences spécifiques du réseau. Mais il n'est pas le seul ; outre EAP-TLS, les autres protocoles **EAP** les plus utilisés sont :

- **EAP-PEAP (Protected Extensible Authentication Protocol) :** PEAP est un protocole qui encapsule d'autres protocoles EAP, généralement EAP-MSCHAPv2 ou EAP-GTC, dans un tunnel sécurisé TLS. Il offre une solution de protection contre les attaques de type homme du milieu.

- **EAP-TTLS (Tunneled Transport Layer Security) :** TTLS est un protocole similaire à PEAP dans le sens où il encapsule d'autres protocoles EAP, mais il utilise une méthode de tunneling différente. TTLS utilise TLS pour sécuriser les échanges entre le client et le serveur.

- **EAP-FAST (Flexible Authentication via Secure Tunneling) :** FAST est conçu pour simplifier le processus d'authentification en évitant la nécessité d'utiliser des certificats sur les clients. Il utilise un tunnel sécurisé pour protéger les échanges d'informations d'identification.

- **EAP-MD5 (Message Digest 5) :** Moins sécurisé que d'autres protocoles EAP, EAP-MD5 transmet les informations d'identification du client en utilisant le chiffrement MD5, mais ne fournit pas de protection forte contre les attaques.

- **EAP-SIM (Subscriber Identity Module) et EAP-AKA (Authentication and Key Agreement) :** Ces protocoles sont utilisés principalement dans les réseaux mobiles et tirent parti des cartes SIM pour l'authentification. 

## Intégration des systèmes

L'IIoT connecte les systèmes informatiques avec les systèmes de contrôle industriel [SCADA](https://fr.wikipedia.org/wiki/Syst%C3%A8me_de_contr%C3%B4le_et_d%27acquisition_de_donn%C3%A9es), les systèmes d'exécution de la fabrication [MES](https://www.sap.com/france/products/scm/execution-mes/what-is-mes.html) pour créer un environnement plus intelligent et plus cohérent. 

<br>
<div align="left">
<img alt="SCADA" src="images/IOT_56.png" width="400px">
</div>
<br>

> Source image : [sick.com](https://www.sick.com/fr/fr/sick-sensor-blog/la-data-integration-en-industrie-tout-comprendre/w/blog-data-integration-industry-40/)

## Automatisation et Contrôle

L'IIoT permet l'automatisation et le contrôle avancés des processus industriels. Les dispositifs intelligents peuvent prendre des décisions en temps réel basées sur les données collectées, optimisant ainsi les performances et la productivité.

Parmi les solutions on peut citer par exemple [Kepware - PTC](https://www.ptc.com/fr/products/kepware) qui est un portefeuille de solutions de connectivité industrielles qui aident les entreprises à connecter divers dispositifs d’automatisation et applications logicielles. Grâce à une architecture évolutive et unifiée, Kepware offre la flexibilité nécessaire pour combiner les pilotes et utiliser plusieurs protocoles sur un seul serveur tout en offrant une interface rationalisée pour une installation, une configuration, une maintenance et un dépannage simples.

<br>
<div align="left">
<img alt="Kepware" src="images/IOT_57.png" width="400px">
</div>
<br>

> Source image : [wiseit.com](https://wiseit.com.ua/en/services/software/graphic-programs-and-cad-design/kepware-software-ptc/)

## Maintenance prédictive et prescriptive

Grâce à la surveillance en temps réel des équipements, l'IIoT facilite la mise en œuvre de la [maintenance prédictive](https://fr.wikipedia.org/wiki/Maintenance_pr%C3%A9ventive). Les défaillances potentielles peuvent être identifiées avant qu'elles ne deviennent des problèmes majeurs, permettant une planification plus efficace de la maintenance.

La maintenance prédictive utilise **l'apprentissage automatique** (en anglais : **machine learning**) pour prédire le moment où l'équipement est susceptible de tomber en panne.

Parmi les technologies de maintenance prédictives dédiées à l'IIoT, on peut citer l'[edge AI](https://stm32ai.st.com/) en particulier dans sa version **embarquée sur [microcontrôleur](../Microcontrollers/microcontroleur)**.<br>
Par exemple les équipements industriels, tels que les pompes à eau, produisent différents signaux pendant leur fonctionnement. En y plaçant des objets dotés de capteurs et de microcontrôleurs il est possible d'utiliser l'apprentissage automatique pour reconnaître les modèles de comportement normaux et anormaux.

<br>
<div align="left">
<img alt="Maintenance predictive edge AI" src="images/IOT_61.jpg" width="450px">
</div>
<br>

> Source image : [stm32ai.st.com](https://stm32ai.st.com/use-case/pump-anomaly-detection-based-on-vibrations/)


Cette approche présente le double avantage de traiter le signal **en temps réel** et de réaliser l'inférence IA (voire son apprentissage) **directement dans le microcontrôleur de l'objet** plutôt que collecter des giga-octets de données pour les envoyer ensuite dans un data lake.

Mieux encore, l'edge AI permet de converger vers une **maintenance prescriptive** notamment grâce à des algorithmes de classification capables de désigner avec précision les comportements dégradés des machines au prix d'un apprentissage automatique nécessitant un peu plus de temps.

## Gouvernance

L'IIoT est un domaine relativement nouveau pour les industriels, il est donc impératif d'organiser les équipes afin de tendre vers l'excellence opérationnelle.

La matrice RACI appartient à la famille des outils de gestion des responsabilités et des rôles. RACI est un acronyme décrivant les différentes responsabilités que peuvent avoir les membres d'une équipe ou les parties prenantes dans un projet ou un processus :

**R - Responsible (Responsable)** : La personne ou le groupe responsable de la réalisation d'une tâche ou de l'activité. Il s'agit de la personne qui exécute l'action.

**A - Accountable (Aval)** : La personne ou le groupe qui est ultimement responsable du résultat de la tâche. Il s'agit souvent du responsable hiérarchique ou de la personne qui prend la décision finale.

**C - Consulted (Consulté)** : Les personnes ou groupes qui fournissent des informations ou des conseils pour la réalisation de la tâche. Ce sont généralement des experts ou des parties prenantes clés.

**I - Informed (Informé)** : Les personnes ou groupes qui doivent être tenus informés du progrès de la tâche ou de l'activité, mais qui ne sont pas directement impliqués dans sa réalisation.

La matrice RACI est souvent utilisée pour clarifier les rôles et les responsabilités au sein d'une équipe, en s'assurant que chacun comprend clairement ce qui est attendu de lui et des autres. Elle est largement utilisée dans le domaine de la gestion de projets et de la gouvernance d'entreprise pour améliorer la communication et la coordination.

<br>
<div align="left">
<img alt="RACI" src="images/IOT_60.png" width="550px">
</div>
<br>

> Source image : [pyx4.com](https://pyx4.com/blog/les-3-matrices-raci-pour-gerer-efficacement-votre-projet/)

## Références

- Guide (eBook) ["Acteurs & Plateformes IIoT" de Ozone connect](https://www.ozoneconnect.io/ressources/acteurs-plateformes-iiot-tout-ce-quil-faut-savoir-ebook/).
- Guide (eBook) ["Technologies sans fil industrielles" de Ozone connect](https://www.ozoneconnect.io/ressources/technologies-sans-fil-industrielles-le-guide-de-choix-complet/).