---
title: Les technologies et stratégies de communication pour l'IoT
description: Chapitre sur les bus, réseaux et protocoles de communication IoT.
---

# Les technologies et stratégies de communication pour l'IoT

L'objectif de cette section est de donner une vue d'ensemble, à cette date (janvier 2024), des principales technologies de communication utilisées dans l'univers du numérique en général et pour les applications IoT en particulier. Vous serez peut être dérangé par le fait que nous ne ferons pas au fil du texte (pour rester synthétiques) une distinction rigoureuse entre les notions de **bus**, de **réseaux**, de **protocoles** et de **normes**. Quelques rappels à ces sujets :

 - Un **bus informatique** est un dispositif de transmission de données partagé entre plusieurs composants d'un système numérique. Un bus informatique est constitué des **circuits d'interface** et **du protocole** qui définit la manière dont les signaux doivent se comporter pour réaliser les communications. Les caractéristiques du matériel conditionnent en partie le type de communication et le protocole, on est alors dans le cadre d'une **norme**.

 - On distingue généralement un bus d'une part d'une **liaison point à point**, qui ne concerne que deux composants qui en ont l'usage exclusif, et d'autre part, **d'un réseau**, qui implique des participants indépendants entre eux, c'est-à-dire pouvant fonctionner de manière autonome, et qui comprend plusieurs canaux permettant des communications simultanées.

> Source des textes : [Wikipédia](https://fr.wikipedia.org/wiki/Bus_informatique)

## Les différents types de réseaux

Comme illustré ci-dessous, on peut classer les stratégies de communication dans un "environnement connecté" (avec ou sans fil) en cinq catégories complémentaires :

 1. **[Les Personal Area Networks (PAN)](https://fr.wikipedia.org/wiki/R%C3%A9seau_personnel)**. Un **réseau personnel** désigne un type de réseau informatique restreint en matière d'équipements, généralement mis en œuvre dans un espace d'une dizaine de mètres. D'autres appellations pour ce type de réseau sont : réseau domestique ou réseau individuel. 
L'idée est d'envoyer des informations entre des périphériques proches. Un PAN est généralement utilisé pour créer un réseau d'appareils personnels (ordinateur portable, smartphone, tablette, smartwatch, casque de réalité virtuelle, imprimantes...). 

 2. **[Les Local Area Networks (LAN)](https://fr.wikipedia.org/wiki/R%C3%A9seau_local)**. Un **réseau local** est un réseau informatique où les terminaux qui y participent (ordinateurs, etc.) communiquent sans utiliser l’accès à Internet. Une autre approche consiste à définir le réseau local par sa taille physique. C'est généralement un réseau à une échelle géographique relativement restreinte, par exemple une salle informatique, une habitation particulière, un bâtiment ou un site d'entreprise.

 3. **[Les Wide Area Networks (WAN)](https://fr.wikipedia.org/wiki/R%C3%A9seau_%C3%A9tendu)**. Un **réseau étendu** est un réseau informatique ou un réseau de télécommunications couvrant une grande zone géographique, typiquement à l'échelle d'un pays, d'un continent, ou de la planète entière. Le plus grand WAN est le réseau Internet.

 4. **[Les Low Power Wide Area Networks (LPWAN)](https://fr.wikipedia.org/wiki/Low_Power_Wide_Area_Network)**. Un **réseau étendu à basse consommation** est un type de réseau essentiellement employé dans l'Internet des objets (Internet of Things ou IoT) et dans la communication intermachines (Machine to Machine ou M2M).<br>Apparus dans la décennie 2010, ils constituent un nouveau modèle de communication dans l'IoT, à côté des réseaux sans fil à faible portée et des réseaux cellulaires conventionnels. Ils proposent des compromis originaux répondant aux contraintes de contextes IoT de plus en plus répandus : un grand nombre d'objets, devant envoyer leurs mesures assez loin (quelques kilomètres) et traitant occasionnellement de petites quantités d'information, tout en maintenant une faible consommation électrique - l'alimentation s'effectuant le plus souvent par une pile devant durer plusieurs années. Les plus connus, qui nécessitent leurs propres passerelles, sont **LoRaWAN** et **Sigfox**. D'autres LPWAN reposent sur le réseau d'antennes cellulaires 2G/3G/4G/5G : **NB-IOT** et **LTE-M**.

 5. **Les liaisons satellitaires** sont des vecteurs de communication utilisés par les WAN et LPWAN, afin d'assurer une couverture réseau dans les zones où des infrastructures de télécommunication ne peuvent pas être installées (mers et océans, zones désertiques ...). Le développement récent des constellations de nanosatellites en orbite basse laisse penser que ces liaisons pourraient à terme remplacer une partie des réseaux câblés existants.

> Source des textes : Wikipédia

<br>
<div align="left">
<img alt="Protocols" src="images/IOT_20.png" width="600px">
</div>
<br>

> Source image : [matooma.com](https://www.matooma.com/fr/s-informer/actualites-iot-m2m/m2m-comment-connecter-vos-objets)

## Le modèle OSI

### Le modèle OSI en général

[Open Systems Interconnection (OSI)](https://aws.amazon.com/fr/what-is/osi-model/) explicite la stratégie de transformation et gestion des données qui transitent entre un émetteur et un récepteur interconnectés via un (ou des) réseau(x). Le modèle OSI fournit un langage universel pour la mise en réseau informatique, de sorte que diverses technologies puissent communiquer à l'aide de protocoles ou de règles de communication standard.

D'après [Cloudflare](https://www.cloudflare.com/fr-fr/learning/ddos/glossary/open-systems-interconnection-model-osi/), OSI divise un système de communication en sept couches abstraites, empilées. Parcourrons-le de la couche 1 à la couche 7 :

1. **Couche physique**. Cette couche inclut les équipements physiques impliqués dans le transfert de données, tels que les câbles et les commutateurs. C'est également la couche où les données sont converties en une **séquence binaire**.  Elle assemble les bits en **trames** pour la couche de liaison.

2. **Couche de liaison**.  Cette couche définit le format des données et gère l’intégrité et le flux de celles-ci. Elle assemble les trames en **paquets** pour la couche réseau.

3. **Couche réseau**. Cette couche est chargée du transfert des paquets entre deux réseaux différents (adressage) et elle détermine le meilleur chemin pour les acheminer (routage).  Elle assemble les paquets en **segments** pour la couche de transport.

4. **Couche de transport**. Cette couche est responsable de la communication de bout en bout entre les deux appareils, mais aussi du contrôle des flux (vitesse optimale pour une transmission fluide) et des erreurs (demande de retransmission si données incomplètes). Elle assemble les segments en **données** pour la couche de session.

5. **Couche de session**. Il s’agit de la couche responsable de l’ouverture et de la fermeture de la communication entre les deux appareils. L’intervalle entre les deux est appelé **"session"**. Elle laisse la session ouverte suffisamment longtemps pour transférer toutes les données, puis la ferme rapidement afin d’éviter le gaspillage de ressources.

6. **Couche de présentation**. Cette couche est responsable de la préparation des données afin qu’elles puissent être utilisées par la couche applicative. Elle est chargée de les **décompresser**, de les **déchiffrer** et de les **traduire**.

7. **Couche applicative**. Cette couche est chargée de livrer les données brutes et "lisibles" aux applications "utilisateur" telles que les navigateurs web et les clients e-mail. 

A l'occasion d'une communication entre deux appareils, les informations parcourent les sept couches du modèle OSI "dans un sens puis dans l'autre" :

* **Lors de l'émission, "descente" de la couche 7 à la couche 1** : Conversion des données brutes lisibles sur le poste émetteur en flux binaire dans le média de transmission puis ...

* **Lors de la réception, "remontée" de la couche 1 à la couche 7** : Conversion d'un flux binaire fourni par le média de transmission en données brutes lisibles sur le récepteur.

### Le modèle OSI pour l'IoT

La figure suivante illustre **le modèle OSI en positionnant les principaux protocoles IoT** : 

<br>
<div align="left">
<img alt="Le modèle OSI" src="images/modele_OSI.jpg" width="600px">
</div>
<br>

> Sources : [Wikipédia](https://fr.wikipedia.org/wiki/Mod%C3%A8le_OSI) et [digikey.ch](https://www.digikey.ch/fr/articles/application-layer-protocol-options-for-m2m-and-iot-functionality)|

<br>

Le modèle OSI s'applique chaque fois qu'il y a communication d'objet à objet, d'objet à passerelle, de passerelle à centre de données ou de passerelle au cloud.

**Couches 1 & 2, Physique & Liaison** : Comment la donnée est-elle physiquement transmise sur le réseau ? Ex : lignes cuivre, câble coaxial, ondes radio.

- <span style="font-size: 15px;">[BLE](../Embedded/ble)</span>
- <span style="font-size: 15px;">[Z-Wave](https://fr.wikipedia.org/wiki/Z-Wave)</span>
- <span style="font-size: 15px;">[LoRa ](../Embedded/lora)</span>
- <span style="font-size: 15px;">[Radio Frequency Identification (RFID)](https://fr.wikipedia.org/wiki/Radio-identification)</span>
- <span style="font-size: 15px;">[Zigbee](https://fr.wikipedia.org/wiki/ZigBee)</span>
- <span style="font-size: 15px;">[Near Field Communication (NFC)](https://fr.wikipedia.org/wiki/Near-field_communication)</span>
- <span style="font-size: 15px;">[Long Term Evolution (LTE)](https://fr.wikipedia.org/wiki/LTE_(r%C3%A9seaux_mobiles))</span>
- <span style="font-size: 15px;">[Courants porteurs en ligne (CPL)](https://fr.wikipedia.org/wiki/Courants_porteurs_en_ligne)</span>
- <span style="font-size: 15px;">[Wi-Fi/802.11](https://fr.wikipedia.org/wiki/Wi-Fi) </span>
- <span style="font-size: 15px;">[Ethernet/802.3](https://fr.wikipedia.org/wiki/Ethernet)</span>
- <span style="font-size: 15px;">[LoRaWAN](https://fr.wikipedia.org/wiki/LoRaWAN)</span>
- <span style="font-size: 15px;">[LRWPAN/802.15.1](https://fr.wikipedia.org/wiki/IEEE_802.15.4)</span>
- <span style="font-size: 15px;">[GSM/UMTS/GPRS](http://lewebdephilou.free.fr/RESEAUX-TELECOM/Cours-Telecom/Telephonie/GSM-GPRS-UMTS_Girodon.htm)</span>
- <span style="font-size: 15px;">...</span>

**Couche 3, Réseau** : Stratégie d’adressage et de routage des paquets entre deux réseaux différents.

- <span style="font-size: 15px;">[Internet Protocol (IP)](https://fr.wikipedia.org/wiki/Internet_Protocol)</span>
- <span style="font-size: 15px;">[IPv6 Low power Wireless Personal Area Networks (6LoWPAN)](https://fr.wikipedia.org/wiki/6LoWPAN)</span>
- <span style="font-size: 15px;">...</span>

**Couche 4, Transport** : Transmission, contrôle de flux et gestion des erreurs.

- <span style="font-size: 15px;">[Transmission Control Protocol (TCP)](https://fr.wikipedia.org/wiki/Suite_des_protocoles_Internet)</span>
- <span style="font-size: 15px;">[User Datagram Protocol (UDP)](https://fr.wikipedia.org/wiki/User_Datagram_Protocol) </span>
- <span style="font-size: 15px;">[Sockets](https://fr.wikipedia.org/wiki/Socket)</span>
- <span style="font-size: 15px;">...</span>

**Couches 5 à 7, Session, Présentation, Application** : Ouverture et fermeture de la communication entre deux appareils (session), traduction, chiffrement et compression des données (présentation), livraison des données pour l'utilisateur (application).

- <span style="font-size: 15px;">[Constrained Application Protocol (CoaP)](https://fr.wikipedia.org/wiki/CoAP)</span>
- <span style="font-size: 15px;">[Message Queuing Telemetry Transport (MQTT)](https://fr.wikipedia.org/wiki/MQTT)</span>
- <span style="font-size: 15px;">[Advanced Message Queuing Protocol (AMQP)](https://fr.wikipedia.org/wiki/Advanced_Message_Queuing_Protocol)</span>
- <span style="font-size: 15px;">[ OPC Unified Architecture (OPC-UA)](https://en.wikipedia.org/wiki/OPC_Unified_Architecture)</span>
- <span style="font-size: 15px;">[ HyperText Transfer Protocol (HTTP)](https://fr.wikipedia.org/wiki/Hypertext_Transfer_Protocol)</span>
- <span style="font-size: 15px;">...</span>

## Liaisons et protocoles filaires

Cette section présente quelques-unes des technologies de communication filaire couramment utilisées pour l'IoT.

[**Cliquez ici pour en savoir plus**](protocole_wire).

## Liaisons et protocoles sans fil

Cette section présente quelques-unes des technologies de communication radiofréquence couramment utilisées pour l'IoT.

[**Cliquez ici pour en savoir plus**](protocole_rf).


## Les protocoles réseau de l'IoT

Le tableau ci-dessous rappelle les couches du modèle OSI pour les principaux protocoles réseaux utilisés par les objets connectés.

<br>
<div align="left">
<img alt="Protocoles applicatifs IoT" src="images/protocoles_applicatifs_IoT.jpg" width="500px">
</div>
<br>

> Source image d'origine : [wikipedia.org](https://fr.m.wikipedia.org/wiki/Fichier:Osi-coap.png)

Nous allons les examiner tour à tour dans cette section.

### Couches physique & liaison

**Ethernet, fondation de l'IoT industriel**

[**D'après Wikipédia**](https://fr.wikipedia.org/wiki/Ethernet) **Ethernet** est un protocole de communication utilisé pour les réseaux informatiques, exploitant la commutation de paquets. Il réalise les fonctions de la couche physique et de la couche liaison de données (couches 1 et 2) du modèle OSI. C'est une norme internationale ISO/IEC/IEEE 8802-31.

Il a été conçu au début des années 1970 pour faire communiquer des ordinateurs rattachés à un même « éther », c'est-à-dire un milieu passif capable de transférer des données, comme un câble coaxial. À l'origine prévu pour des transmissions sur des réseaux locaux, Ethernet est aujourd'hui utilisé sur tout type de réseau (réseau étendu, dorsale Internet, automobile…) mais aussi pour des communications sur circuit imprimé.

Ethernet est fréquemment utilisé pour transmettre les télécommunications personnelles ou professionnelles et se combine facilement avec les technologies sans fil (protocoles Wi-Fi). Ethernet est standardisé jusqu'à 400 Gb/s et des débits plus rapides sont à l'étude.

Compte tenu de ses caractéristiques, Ethernet et ses variantes dominent totalement les applications **IoT industrielles** pour lesquelles la fiabilité et la réactivité sont prioritaires sur la consommation d'énergie. La figure ci-dessous montre l’estimation de l’utilisation des "familles" de protocoles Ethernet  pour l'IoT industriel entre 2021 et 2027 :

|Ethernet|Ethernet pour l'IoT industriel|
|:-:|:-:|
|<img alt="Logo Ethernet" src="images/ethernet.jpg" width="200px">|<img alt="Ethernet pour l'IoT industriel" src="images/IOT_22.png" width="650px">|
||Source :  [iotindustriel.com](https://iotindustriel.com/autres/guides-de-choix-et-definitions/ethernet-industriel-lessentiel-a-savoir/)|

A noter que le protocole EtherNet/IP devrait être le plus populaire pendant cette période.

**GSM, GPRS et UMTS, différentes générations de réseaux mobiles.**

[D'après Echos du Net](https://www.echosdunet.net/reseau-mobile/difference-gsm-umts), GSM, GPRS et UMTS correspondent chacun à différentes générations de réseaux mobiles. Le GSM remonte aux années 1980, lorsque les communications mobiles furent rendues possibles. Le standard de réseau mobile a évolué, de la 2G à la 3G (UMTS) au milieu des années 2000.

**GSM** signifie littéralement "Global System for Mobile Communication" et marque les débuts du numérique dans la téléphonie mobile (norme créée en 1982). Ce réseau ne permet que d'échanger par la voix. On parle aussi de 2G pour faire référence à la technologie GSM. Il s'agit donc d'un standard dit de "seconde génération" (d'où le terme 2G) car les communications fonctionnent selon un mode entièrement numérique, ce qui n'était pas le cas avec les téléphones portables de la première génération. En Europe, les bandes de fréquence utilisées par le GSM sont centrées sur 900 MHz et sur 1800 MHz.

**GPRS** pour "General Packet Radio Service" est une évolution du GSM qui fait la transition vers la troisième génération (3G ou UMTS). Pour parler du standard GPRS, on peut parler de 2.5G ou même de 2.75G que l'on appelle **EDGE**). EDGE signifie "Enhanced Data Rates for GSM Evolution" (débits trois fois supérieurs à ceux du GSM mais couverture réduite).

**UMTS** pour "Universal Mobile Telecommunications System" **ou 3G** est une évolution du standard GPRS et l'une des technologies mobiles de troisième génération. L'UMTS est bien plus rapide que le GPRS. Si l'EDGE propose un débit de 120 Kbit/s, la 3G culmine à 250 Kbit/s. Toutes ces raisons font que la 3G, aujourd'hui supplantée par la 4G, a longtemps été considérée comme LA technologie de pointe en ce qui concerne la vidéoconférence et l'accès à la télévision, par exemple. En 2024, la 4G et la 5G sont les réseaux mobiles dominants en France et leurs débits sont sans précédent par rapport à la 3G. Le débit maximum théorique de la 4G est de 150 Mb/s et jusqu'à 900 Mb/s en 4G+ (une évolution du réseau 4G qui permet d’agréger 2 ou 3 bandes de fréquence et ainsi d’améliorer les débits descendants), celui de la 5G est de 2 Gbits/s.


**La norme IEEE 802.11 et le Wi-Fi**

D'après [IONOS](https://www.ionos.fr/digitalguide/serveur/know-how/ieee-80211/), **La norme 802.11** a été publiée pour la première fois en 1997 par l’Institut des ingénieurs électriciens et électroniciens (IEEE) et a été rapidement intégrée au sein de divers appareils. Aujourd’hui, IEEE 802.11 est la norme la plus connue et la plus utilisée pour les réseaux sans fil. Il existe plusieurs générations de réseaux locaux sans fil.

Certes, 802.11 est parfois utilisé comme synonyme de « Wi-Fi », mais ce n’est pas tout à fait vrai. Le Wi-Fi est au départ un Ethernet sans fil, c’est-à-dire un réseau local qui fonctionne sans câble. Pour mettre en place un tel réseau, la norme 802.11 est nécessaire. Celle-ci définit la couche physique dans un réseau local sans fil et permet d’accéder à cette couche. En principe, il est également possible d’utiliser une autre technique pour mettre en place un tel réseau. Cependant, comme la norme IEEE 802.11 est très répandue, elle est souvent associée au terme Wi-Fi.

**La norme IEEE 802.15.4 pour les LR-WPAN (Low Rate Wireless Personal Area Network)**

D'après [Wikipédia](https://fr.wikipedia.org/wiki/IEEE_802.15.4), La norme  IEEE 802.15.4 est destinée aux réseaux sans fil de la famille des LR WPAN (Low Rate Wireless Personal Area Network) du fait de leur faible consommation, de leur faible portée et du faible débit des dispositifs utilisant ce protocole. IEEE 802.15.4 est utilisée par de nombreuses implémentations basées sur des protocoles propriétaires ou sur IP (Internet Protocol), comme le ZigBee et le 6LoWPAN.

### Couche réseau : IP et 6LoWPAN

[**IP** pour "Internet Protocol"](https://fr.wikipedia.org/wiki/Internet_Protocol) est une famille de protocoles de communication de réseaux informatiques conçus pour être utilisés sur Internet. Les protocoles IP assurent l'acheminement au mieux (best-effort delivery) des paquets. Ils ne se préoccupent pas du contenu des paquets, mais fournissent une méthode pour les mener à destination. La norme [**IPv4**](https://fr.wikipedia.org/wiki/IPv4) a récemment été révisée en version [**IPv6**](https://iotindustriel.com/glossaire-iiot/ipv6-cest-quoi/) pour offrir assez d'adresses pour accueillir tous les appareils connectés à l’internet.

[**6LoWPAN**](https://fr.wikipedia.org/wiki/6LoWPAN) pour "IPv6 Low power Wireless Personal Area Networks". D'après [OZONE CONNECT](https://iotindustriel.com/glossaire-iiot/6lowpan-cest-quoi/) : 
* La technologie est conçue spécifiquement pour les appareils à faible consommation, ce qui la rend beaucoup plus efficace en termes de consommation d’énergie que les protocoles traditionnels tels que l’IPv4.
* 6LoWPAN utilise une topologie de réseau maillé, ce qui permet une plus grande fiabilité et une plus grande flexibilité en termes de réseau
* 6LoWPAN permet à des dispositifs dont la puissance de traitement et la mémoire sont limitées de communiquer efficacement sur des réseaux sans fil

### Couche transport

D'après le site de l'éditeur d'antivirus [Avast](https://www.avast.com/fr-fr/c-tcp-vs-udp-difference) ...

**TCP**, pour  "Transmission Control Protocol", est un protocole de communication pour Internet orienté connexion qui permet aux appareils et applications informatiques d’envoyer des données et d’en vérifier la livraison. TCP est utilsié pour les e-mails, la navigation Web ou le transfert de fichiers.

**UDP**, pour "User Datagram Protocol" est un protocole de communication pour Internet orienté message qui permet aux appareils et applications informatiques d’envoyer des données, sans en vérifier la livraison. UDP est le mieux adapté à la communication en temps réel et aux systèmes de diffusion ("streaming").

Les deux principales différences entre TCP et UDP sont :
 - TCP exige une connexion fiable entre le serveur et le destinataire, ce qui peut ralentir le transfert de données. UDP est un protocole sans connexion, donc beaucoup plus rapide.
 - TCP garantit une transmission sans faille des données, même si les paquets perdus ou endommagés sont retransmis. UDP est un protocole « tire et oublie » qui ne vérifie pas les erreurs et ne renvoie pas les paquets de données perdus.

### Couche applicative

1. **HTTP, conçu pour les objets disposant de ressources conséquentes**

    Dans le modèle OSI, le protocole HTTP utilise la suite TCP - IPv4/IPv6 - Ethernet. Il hérite donc des contraintes d'Ethernet et impose aux objets des ressources conséquentes. 

2. **CoAP, conçu pour les applications de l'IoT contraint**

    L'inadéquation de HTTP pour les objets contraints a naturellement poussé à la mise au point de protocoles web de type client/serveur taillés exclusivement pour l'IoT. C'est le cas de [**CoAP**](https://fr.wikipedia.org/wiki/CoAP) pour "Constrained Application Protocol". Il a été développé avec l'API **REST** [^1] pour utiliser les adresses IPv6 (anticipant ainsi la prolifération des objets connectés) tout en restant très efficace en termes de bande passante et d’utilisation de l’énergie.

    D'après [iotindustriel.com](https://iotindustriel.com/glossaire-iiot/6lowpan-cest-quoi/), **6LoWPAN** est l'acronyme de "IPv6 Low power Wireless Personal Area Networks". C'est un protocole de communication conçu pour les réseaux à faible puissance et à pertes, généralement radiofréquence. Il fonctionne à la couche 3 du modèle OSI avec une topologie de réseau maillé. 

3. **MQTT, "s'abonner - publier"**

    [MQTT](https://fr.wikipedia.org/wiki/MQTT) pour "Message Queuing Telemetry Transport" est un protocole de communication léger, basé sur un système publisher/subscriber, une stratégie basée sur l'échange de messages via des "boites à lettres". Son principe de fonctionnement est illustré par la figure qui suit :

    |MQTT|Principe de MQTT|
    |:-:|:-:|
    |<img alt="Logo MQTT" src="images/mqtt.png" width="200px">|<img alt="Principe de MQTT" src="images/IOT_49.png" width="500px">|
    ||Source : [Medium](https://nitin-sharma.medium.com/getting-started-with-mqtt-part-1-a3c365e3a488)|

    Un serveur ou broker centralise les données regroupées en boites à lettres thématiques (par exemple : mesures de température). L'objet connecté (serveur) "poste" ses mesures dans des boites à lettres thématiques ("température du moteur 2", "alarme de mouvement dans le salon", etc.) et n'importe quel autre objet (client) pourra "s'abonner" à ces boîtes pour en consulter le contenu. Le broker gère la distribution des messages aux clients. [MQTT-SN](http://www.steves-internet-guide.com/mqtt-sn/) est une modification de MQTT optimisée pour l'IoT qui utilise UDP au lieu de TCP. 

    MQTT est devenu un standard international dans la communication entre les objets car il présente de très nombreux avantages face au protocole HTTP :

    - <span style="font-size: 15px;">Il est environ 90 fois plus rapide pour envoyer des informations</span>
    - <span style="font-size: 15px;">Il est environ 10 fois moins énergivore pour envoyer des messages</span>
    - <span style="font-size: 15px;">Il est environ 170 fois moins énergivore pour recevoir des messages</span>
    
    <br>

4. **AMQP, "produire et consommer"**

    Le protocole [AMQP](https://fr.wikipedia.org/wiki/Advanced_Message_Queuing_Protocol) pour "Advanced Message Queing Protocol" est basé sur le même principe que MQTT. Cependant la notion de publisher/subscriber est remplacée par celle de producer/consumer.

    <br>
    <div align="left">
    <img alt="AMQP" src="images/IOT_50.png" width="250px">
    </div>
    <br>
    
    > Source image : [esegece.com](https://www.esegece.com/community/blog/amqp-receive-messages)

    D'après [IONOS](https://www.ionos.fr/digitalguide/sites-internet/developpement-web/advanced-message-queuing-protocol-amqp/), ce protocole ouvert créé à l'instigation de la banque américaine JPMorgan Chase résout plusieurs problèmes en même temps : 

    - Il garantit une transmission des données fiable (à l’aide d’un message broker). 
    - Il envoie les messages dans des files d’attente, permettant ainsi une communication asynchrone : l’émetteur et le destinataire n’ont pas à agir au même rythme. Le destinataire (consommateur) du message n’est pas obligé d’accepter directement l’information, de la traiter et d’en accuser réception auprès de l’émetteur (producteur). À la place, il va récupérer le message dans la file d’attente lorsqu’il en a la capacité. Le producteur peut ainsi continuer à travailler sans créer de période d’inactivité.

## Références

- [Article de Cloudflare sur le modèle OSI](https://www.cloudflare.com/fr-fr/learning/ddos/glossary/open-systems-interconnection-model-osi/)
- [Article de Digikey sur l'IIOT](https://www.digikey.ch/fr/articles/application-layer-protocol-options-for-m2m-and-iot-functionality)

[^1]: [REST](https://fr.wikipedia.org/wiki/Representational_state_transfer) pour "REpresentational State Transfer" est un style d'architecture pour créer des services web. Dans un service web REST, les requêtes effectuées sur l'URI d'une ressource produisent une réponse dont le corps est généralement formaté en HTML, XML ou JSON. Lorsque le protocole HTTP est utilisé, comme c'est souvent le cas, les méthodes HTTP disponibles sont GET, HEAD, POST, PUT, PATCH, DELETE, CONNECT, OPTIONS et TRACE2. Dans le cas de CoAP qui nous intéresse ici le protocole est bien sûr 6LowPAN.


