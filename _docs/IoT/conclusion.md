---
title: Conclusion et perspectives
description: Chapitre de conclusion à l'internet des objets.
---

# Synthèse

**L'IOT consiste à connecter des objets, des choses, des capteurs ... n'importe quoi à Internet.**

**Anticipés en 2025 :**
- 55 milliards d'objets connectés
- 175 zêta (10<sup>21</sup>) octets échangés

**Architecture en 3 couches :**
- **Objets contraints** : Flotte d’objets en contact avec la physique mesurée.
- **Gateway** : Transit et traitement partiel des données
- **Cloud Platform** : Héberge, traite et met à disposition les données, gère la flotte d'objets.

<br>
<div align="left">
<img alt="Architecture" src="images/IOT_8.jpg" width="700px">
</div>
<br>

# Les défis

**Interopérabilité**. Les grands fournisseurs de "solutions" IoT les enferment dans des silos rendant complexe l’ouverture d’un système. Des normes existent aujourd’hui pour casser ces barrières, elles doivent être privilégiées.

**Autonomie**. Une fois diffusés, les objets sont autonomes. En énergie, parfois plusieurs années, et en gestion (update, réseau…). Un objet doit être fortement optimisé pour être robuste et économe en énergie.

**Volume de données**. L’enjeu des prochaines années est la limitation du volume de données. Le réduire, c’est économiser de l’infrastructure, de l’argent et de l’énergie. L’Edge Computing et l'Edge AI en particulier sont des éléments de réponse à ce défi.

**Sécurité**. Développer une flotte d'objets contraints c’est mettre à disposition de potentiels "hackers" un grand nombre de portes d’entrées dans le système. Toutes doivent être protégées. Penser que protéger la plateforme est suffisant est une erreur.

**Impact sociétal**. Beaucoup de questions sensibles encore sans réponses. Comment garantir la protection des données personnelles et l'anonymat des citoyens "cernés" par les objets indiscrets ? 

**Impacte environnemental**. Quelle stratégie de recyclage ou reconditionnement mettre en œuvre pour éviter de "jeter" des milliards d'objets connectés dont la conception, la fabrication et la distribution ont un impact significatif sur l'environnement ?

# Quelle solution IoT choisir ?

Les solutions IoT sont évidemment dictées par l'étude du problème qui appelle leur déploiement, par ses contraintes et ses objectifs.

**Réseau**
- Courte portée
  - Wi-Fi
  - Bluetooth / Zigbee / Z-Wave / BLE
- Longue portée
  - NB-IOT / LTE-M / Sigfox / LoRaWAN

**Gestion de l’énergie**
- Un bon objet connecté est un objet qui dort beaucoup.
- Low Energy vs calcul.
- Rechargeable vs jetable longue durée.

**Traitement de l’information**
- Dans l’objet vs Gateway vs Cloud

**Framework**
- AWS
- PTC
- Google Cloud Platform – IoT Framework
- MBED IoT Platform
- KAA IoT
- Zetta
- …

**Device management**
- LWM2M
- UpSwift
- …

<br>
<div align="left">
<img alt="IoT" src="images/IOT_71.jpg" width="500px">
</div>
<br>

> Source image : [theinternetofthings.report](https://theinternetofthings.report/trending-news/innophase-iot-selects-stmicroelectronics-to-deliver-industrys-lowest-power-sensor-to-cloud-iot-solution)

