---
title: Introduction
description: Chapitre d'introduction à l'internet des objets.
---

# Qu’est ce que l’Internet des Objets ?

**L'Internet des Objets** ou IoT est la capacité à connecter des objets, des choses, des capteurs, n'importe quoi à Internet.
Il représente une évolution naturelle des technologies, le lien entre le monde numérique et le monde physique.<br>
L'IoT est parmi nous et il évolue très vite : on estime à 150 milliards le nombre d'objets qui seront connectés en 2025 (source : [**Wikipédia**](https://fr.wikipedia.org/wiki/Internet_des_objets)).


# Les champs d’application

Les champs d’application de l'IoT sont nombreux ; presque tous les domaines de l'activité humaine sont concernés. Parmi les situations dans lesquelles la collecte de données sur le terrain et leur analyse permet d'améliorer les pratiques on peut citer :

<br>
<div align="left">
<img alt="Solutions et applications" src="images/Applications IoT.jpg" width="750px">
</div>
<br>

> Source image : [inria.fr](https://learninglab.gitlabpages.inria.fr/mooc-iot/mooc-iot-ressources/Module1/S01/C034AA-M01-S01-part1-cours_FR.html)


Les applications et objets de l'IoT sont bien sûr architecturés selon ces contextes et objectifs extrêmement variés :

- <span style="font-size: 15px;">Autonomie</span>
- <span style="font-size: 15px;">Taille des données acquises et envoyées</span>
- <span style="font-size: 15px;">Environnement (accès aux capteurs, milieu hostile …)</span>
- <span style="font-size: 15px;">Espacement entre les capteurs</span>
- <span style="font-size: 15px;">Etc.</span>


# Un secteur d’application : *Les "Smart Cities"*

D'après [la CNIL](https://www.cnil.fr/fr/definition/smart-city), la ville intelligente est un nouveau concept de développement urbain. Il s’agit d’améliorer la qualité de vie des citadins en rendant la ville plus adaptative et efficace, à l’aide de nouvelles technologies qui s’appuient sur un écosystème d’objets et de services. Le périmètre couvrant ce nouveau mode de gestion des villes inclut notamment : infrastructures publiques (bâtiments, mobiliers urbains, domotique, etc.), réseaux (eau, électricité, gaz, télécoms) ; transports (transports publics, routes et voitures intelligentes, covoiturage, mobilités dites douces - à vélo, à pied, etc.) ; les e-services et e-administrations.

<br>
<div align="left">
<img src="images/IOT_2.jpg" width="437px">
</div>
<br>

> Source image : [banquedesterritoires.fr](https://www.banquedesterritoires.fr/france-urbaine-dresse-le-portrait-dune-smart-city-a-la-francaise)

Par exemple, on les retrouve au quotidien pour ...
- <span style="font-size: 15px;">Identifier et signaler les places de parking</span>
- <span style="font-size: 15px;">Adapter l’éclairage public</span>
- <span style="font-size: 15px;">Détecter des fuites</span>
- <span style="font-size: 15px;">Optimiser le ramassage des déchets</span>
- <span style="font-size: 15px;">Relever les consommations d'eau, d'électricité ...</span>
- <span style="font-size: 15px;">Etc.</span>

Aujourd’hui, l’utilisation de l’IoT reste parcellaire mais les plus grande villes s’appuient sur l’IoT pour certaines applications ayant une plus-value économique, citoyenne ou environnementale évidente.

# Un secteur d’application : *La santé connectée*

Grâce à l'Internet des Objets les dispositifs médicaux peuvent récolter des données pour des diagnostics ou des soins à distance mais aussi permettre aux patients de mieux contrôler leur traitement. L'IoT apporte une plus grande réactivité et une meilleure compréhension des différents comportements via une captation locale et précise des données.

<br>
<div align="left">
<img src="images/IOT_3.jpg" width="413px">
</div>
<br>

> Source image : [santé.fr](https://www.mayotte.ars.sante.fr/le-panorama-sante-de-lars-mayotte?parent=15536)

Les solutions connectées les plus connues sont :
- <span style="font-size: 15px;">Pompes à insuline (Johnson & Johnson)</span>
- <span style="font-size: 15px;">Pacemakers (St Jude Medical)</span>
- <span style="font-size: 15px;">Respirateurs (Novartis, Propeller)</span>

Mais les applications possibles sont bien plus diversifiées que la création de nouveaux équipements pour les soins. On peut citer :
- La gestion intelligente des établissements de santé (ex : sécurité du bâtiment, surveillance des chambres froides, suivi de la qualité d'air ...)
- La gestion intelligente de la logistique (ex : traçabilité des produits de santé, suivi du transports d'organes, anticipation de la rupture de la chaîne du froid ...)
- La télésurveillance médicale (ex : détection des chutes, des accidents cardiaques, téléassistance, cabines de télédiagnostic ... )

# Un secteur d’application :	*Le "Smart Farming" ou "Agriculture 4.0"*

Un agriculteur doit mesurer les variations de différents paramètre sur la superficie d'un champ pour adapter en conséquence l'utilisation d'eau, de pesticides et d'engrais. Pour ce faire :

- <span style="font-size: 15px;">Des capteurs sont positionnés dans les cultures pour mesurer la température, la pression et l'humidité de l’air ou du sol ; </span>
- <span style="font-size: 15px;">Un réseau tel que [LoRaWAN](../Embedded/lora) ou [Sigfox](https://www.sigfox.com/) est déployé, dont la couverture répond aux besoins de l'application ; </span>
- <span style="font-size: 15px;">Les données sont centralisées sur un serveur dans le Cloud ; </span>
- <span style="font-size: 15px;">Une application propose et met en œuvre les actions (commutation de vanne, ouverture fermeture de panneaux de serre ...).</span>

<br>
<div align="left">
<img src="images/IOT_4.png" width="547px">
</div>
<br>

> Source image : [kvernelandgroup.com](https://fr.kvernelandgroup.com/Marques-et-Produits/iM-FARMING/The-Future-of-Smart-Farming )

Les élevages importants ont généralisé le suivi des animaux, qui sont équipés de badges. Les paramètres physiologiques des animaux tels que leur poids et leur alimentation sont surveillés et les pratiques des éleveurs s'adaptent en conséquence.

## Exemple : Service IoT de météo locale

Le réseau de la société [Sencrop](https://sencrop.com/fr/) maille la France avec des **stations météo autonomes pendant 3 ans** et **connectées au [réseau Sigfox](https://www.sigfox.com/)** (maillage France en bas débit).<br>
 Elles mesurent :

 - <span style="font-size: 13px; list-style-type: square;">L'hygrométrie</span>
 - <span style="font-size: 13px; list-style-type: square;">La pluviométrie</span>
 - <span style="font-size: 13px; list-style-type: square;">La vitesse et la direction du vent</span>
 - <span style="font-size: 13px; list-style-type: square;">L'humectation</span>
 - <span style="font-size: 13px; list-style-type: square;">La position GPS</span>
 - <span style="font-size: 13px; list-style-type: square;">La température</span>
 - <span style="font-size: 13px; list-style-type: square;">Le point de rosée</span>

<br>

<img src="images/IOT_5_1.jpg" width="400px"> <img src="images/IOT_5.png" width="200px">

> Source images : [Sencrop](https://sencrop.com/fr/)

Les données sont stockées et traitées dans le cloud.
Les abonnés sont alertés avec des messages adaptés à leur culture (mildiou, dartrose, gel, botritis, oïdium …).

# Un secteur d’application : *Les transports connectés*

L'Internet des Objets dans les transports et la logistique permet de gérer efficacement les chaînes d'approvisionnement, comme le suivi des véhicules connectés.

La plupart des sociétés de transport sont équipées de **suivi de flotte**, deux solutions sont principalement adoptées :

1. Un capteur GPS est installé dans chaque véhicule. Il envoie ses données régulièrement par réseau LTE-M ou NB-IOT.
2. Une application est installée sur le smartphone du transporteur, elle réalise elle-même le tracking.

Les données sont centralisées et l’application fournit, entre autres, une estimation fine de l’heure de livraison en fonction de la position du transport dans sa tournée. On peut ainsi optimiser les itinéraires de livraison, anticiper les retards, indiquer aux clients où se trouvent leurs colis en temps réel, etc.

## Exemple : Service IoT de suivi de flotte à l’échelle planétaire

L'entreprise [Traxens](https://www.traxens.com/) installe des capteurs sur les containers dans le monde entier. Un capteur donné est **autonome pendant 3 ans** et s’insère dans **un réseau maillé**.

<br>
<div align="left">
<img src="images/IOT_7.png" width="800px">
</div>
<br>

> Source images : [Traxens](https://www.traxens.com/) 

Un abonnement sur un container équipé :
- <span style="font-size: 15px; list-style-type: circle;">Informe sur la position et le parcours du container</span>
- <span style="font-size: 15px; list-style-type: circle;">Calcule les émissions de CO<sub>2</sub></span>
- <span style="font-size: 15px; list-style-type: circle;">Prédit l’heure d’arrivée</span>
- <span style="font-size: 15px; list-style-type: circle;">Détecte les chocs</span>
- <span style="font-size: 15px; list-style-type: circle;">Compte les ouvertures de portes</span>
- <span style="font-size: 15px; list-style-type: circle;">Mesure la température et l’hygrométrie</span>

## Références

- [Traxens](https://www.traxens.com/) 
- [Sencrop](https://sencrop.com/fr/)
- [CNIL](https://www.cnil.fr/fr/definition/smart-city)
- [Kverneland group](https://fr.kvernelandgroup.com/Marques-et-Produits/iM-FARMING/The-Future-of-Smart-Farming )
- [Site de SYNOX (livres blancs)](https://www.synox.io/ressources/iot-livres-blancs/)

