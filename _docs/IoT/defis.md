---
title: Les défis de l'IoT
description: Chapitre sur les défis de l'IoT.
---

# Les défis d’un système IoT

Il existe nombre d’écueils sur le chemin des systèmes IoT, preuve en est la lenteur de déploiement de solutions dans les domaines de la santé ou de la ville connectée.
Pour n'en citer que quelques-uns :
- **Interopérabilité des solutions IoT**. Comment passer d’une solution AWS à Google ou Azure ?
- **Contraintes matérielles**. Comment tout faire tenir dans 2 cm² et 100 g pendant 10 ans ?
- **Topologie dynamique**. Comment gérer le mouvement permanent des objets ?
- **Stockage, traitement et transport des données**. On parle de 175 zêta octets (10<sup>21</sup> octets) en 2025. Comment les gérer?
- **Sécurité des objets connectés**. Comment garantir la protection des données sur l’ensemble du parcours ?
- **Problématiques sociétales et environnementales**. Confidentialité des données, recyclage …

## Interopérabilité

Aujourd’hui, les gros acteurs du marché se partagent l’IoT :
- AWS IoT pour Amazon
- Azure IoT pour Microsoft
- Google Coud IoT pou Google
- ...

Leurs offres proposent des briques logicielles depuis l’Objet jusqu’au Cloud.
Cette situation engendre un **fonctionnement en silos**. Les utilisateurs se retrouvent **captifs** d’une solution qui a le monopole de la gestion des objets et des données produites.

Un autre problème est le manque de **standards de communication** entre différents systèmes IoT. 

<br>
<div align="left">
<img alt="Interopérabilité" src="images/IOT_62.jpg" width="500px">
</div>
<br>

> Source image : [www.researchgate.net](https://www.researchgate.net/figure/Vertical-silos-of-IoT-service-deployment_fig1_267157426)

L’enjeu consiste donc à imposer des standards de communication.

**L’IETF (Internet Engineering Task Force)** en a publié un certain nombre sur :
- La sécurité (RFC 8576)
- L’utilisation de headers IP avec les objets contraints (RFC 8724 & RFC 6550)
- L’utilisation de l’IPv6 à travers le Bluetooth Low Energy (BLE)
- [Et bien d’autres sujets](https://wayback.archive-it.org/20635/20230207053750/https://www.ietfjournal.org/internet-of-things-standards-and-guidance-from-the-ietf/)

Ces standards s’appliquent en particulier aux réseaux de capteurs : 

<br>
<div align="left">
<img alt="IETF" src="images/IOT_63.jpg" width="400px">
</div>
<br>

> Source image : [www.researchgate.net]()

L’IETF est un organisme de standardisation ouvert. A l’opposé, des spécifications sont poussées par des entreprises regroupées en alliances :
- LoRaWAN est proposé par la Lora Alliance (IBM, Cisco, Schneider, Mueller …)
- Thread est proposé par ThreadGroup (Apple, Google, Hager, Ikea …)

<br>
<div align="left">
<img alt="Alliance" src="images/IOT_64.png" width="500px">
</div>
<br>

> Sources images : [lora-alliance.org](https://lora-alliance.org/), [wikipedia.org](https://fr.wikipedia.org/wiki/Matter_(standard)), [openthread.io](https://openthread.io/guides/thread-primer?hl=fr)

## Contraintes matérielles

Un objet est très souvent architecturé autour d’un [**microcontrôleur**](../Microcontrollers/microcontroleur) qui dispose de ressources limitées :
- RAM : < 100kB
- Mémoire flash : < 1 MB
- CPU : ARM < 100 GHz

Ce support doit faire fonctionner une application complète, capable de :
- faire des calculs
- Assumer sa sécurité
- Se mettre à jour
- Communiquer

<br>
<div align="left">
<img alt="Electronique" src="images/IOT_65.jpg" width="400px">
</div>
<br>

> Source image : [rs-online.com](https://fr.rs-online.com/web/p/modules-de-developpement-de-communication-et-sans-fil/1345557)

Ce défi est résolu par l’utilisation de micro-noyaux, bibliothèques et autres outils logiciels optimisés pour la programmation embarquée, par exemple :
- Amazon FreeRTOS
- Azure RTOS
- Huawei LightOS
- Windows 10 IoT Edition
- [Et bien d'autres encore](https://www.ubuntupit.com/best-iot-operating-system-for-your-iot-devices/)

L’autre gros problème est la gestion de l'énergie. Il est souvent question de systèmes embarqués qui devront rester autonomes plusieurs années, comme par exemple les flotteurs - profileurs [du programme "Adopt a float"](https://adoptafloat.com/).

Pour ce faire, les microcontrôleurs sont capables de se mettre en sommeil plus ou moins profond pour économiser l'énergie puis de se réveiller ponctuellement pour exécuter leurs programmes avant de se rendormir.

Le système d’exploitation de l’objet et son programme vont utiliser au maximum cette fonctionnalité de "mise en veille". Plus généralement toute l’électronique utilisée sera conçue, fabriquée et programmée pour que l'énergie consommée par l'objet soit la plus faible possible.

Des plateformes existent ([FIT-IoT](https://www.iot-lab.info/) par exemple) pour tester le code embarqué sur différentes architectures électroniques en mesurant finement la consommation de courant.

## Topologie dynamique

La topologie d'un réseau IoT est très dynamique.
En effet, chaque objet est indépendant et peut s'allumer et s'éteindre à tout moment. De plus, les objets peuvent être mobiles et chaque mobilité est différente en trajectoire et vitesse.

Cette dynamique ajoute un défi à l'acheminement des données et au placement des services. En effet :
- Comment envoyer un message à un objet sans savoir où il se trouve précisément ?
- Comment s'assurer d'une route de transmission des données entre nœuds si à tout moment n'importe lequel d'entre eux peut entrer ou sortir du périmètre couvert par une passerelle ?

<br>
<div align="left">
<img alt="Topologie" src="images/IOT_66.jpg" width="500px">
</div>
<br>

> Source image : [healthcare-in-europe.com](https://healthcare-in-europe.com/en/news/the-iot-mesh-network.html)

Cet aspect est également un défi pour le placement de services dans le edge computing par lequel on cherche à déployer les services au plus près des objets, en périphérie de réseau.

Les services doivent donc dynamiquement suivre la mobilité des objets et leurs besoins sans connaissance a priori de leurs déplacements.

## Stockage, traitement et transport des données

Ce défi est à découper en trois points :

- **Stockage des données**. Il est géré par des bases de données et l’expérience accumulée depuis le début de l’informatique doit y répondre. Ces systèmes de données sont en constante évolution.

- **Accès rapide à l’information**. L'information n’est pas disponible immédiatement car il faut traiter et recouper les données avant. Ces traitements sont parfois complexes et nécessitent une grande capacité de calcul. Aujourd'hui, les offres liées aux plateformes de cloud computing donnent accès très facilement à des ressources de calcul (serveurs, cartes GPU, etc.) et doivent permettre de produire des résultats de traitement en temps réels.

- **Répartition de la charge**. Il est possible de mieux répartir les ressources de calcul entre les différents éléments de la chaîne IoT :
  - Les calculs les plus intensifs s'effectuent dans les clouds.
  - Les calculs plus légers peuvent s'effectuer à un niveau intermédiaire (par exemple, sur une station de base).
  - Les calculs les plus élémentaires s'effectuent sur les objets eux-mêmes.

Cette meilleure répartition permet de diminuer l'utilisation des infrastructures de transport des données (radios, filaires) et donc d'augmenter le nombre d'objets pouvant être connectés.

<br>
<div align="left">
<img alt="Stockage" src="images/IOT_67.png" width="700px">
</div>
<br>

> Source image : [intel.fr](https://www.intel.fr/content/www/fr/fr/transportation/iot-fleet-management.html)

## Sécurité des objets connectés

Tous ces capteurs déployés, dans un bâtiment intelligent, dans une ville, chez les particuliers, etc. fournissent des données de natures très variées (température, luminosité, pression, accélération, proximité, humidité) et parfois très sensibles (localisation d'individus, données biométriques ...).

Si ces données sont interceptées, elles peuvent être exploitées par un tiers malveillant et causer des problèmes de différentes natures : dysfonctionnement ou arrêt complet de l'application, atteinte physique sur une ou plusieurs personnes, espionnage industriel en vue d'obtenir un avantage concurrentiel, etc.

La protection des données est donc un enjeu majeur pour garantir le développement de l'IoT sur le long terme. Elle doit se faire à plusieurs niveaux :

- Au niveau des communications entre objets, entre objets et passerelles, entre objets et platformes ;
- Au niveau software des objets, des passerelles, des plateformes ;
- Au niveau hardware des objets, des passerelles, des plateformes.

En fonction du niveau (communication, logiciel, matériel), la difficulté des attaques et la complexité des protections varient (voir [cette section](cybersecurite)).

<br>
<div align="left">
<img alt="Sécurité" src="images/IOT_68.jpg" width="500px">
</div>
<br>

> Source image : [idemia.com](https://www.idemia.com/fr/solution-iot-safe)

## Problématiques sociétales et environnementales

Au delà des aspects techniques, l'Internet des Objets doit faire face à des problèmes d'ordres sociétaux et environnementaux. Le défi principal réside dans la conséquence sur la vie quotidienne qu'implique le déploiement des solutions IoT. En effet, les objets connectés gagnent de plus en plus d'espaces dans la vie quotidienne de la population et même dans tous les domaines d'activités. Par exemple :

- Il peut arriver que certains utilisateurs, par manque de connaissance sur le fonctionnement de ces objets (un assistant vocal, une borne domotique), ne réalisent pas qu'ils partagent des informations personnelles, parfois même intimes.

- Le déploiement de solutions IoT en zone urbaine dans le but de rendre les villes plus intelligentes peut remonter des données sur l'usage de certaines infrastructures par les habitants de la ville. Ces systèmes doivent permettre d'améliorer la gestion des transports, des infrastructures, de faire des économies, d'améliorer le vivre ensemble. Mais même si les objectifs initiaux sont louables et importants et que ces données sont supposées être anonymes, rien ne garantit que le recoupement d'informations ne permettra pas de désanonymiser ces données.

Il peut donc exister un (sérieux) problème de respect de la vie privée lié à certaines applications IoT. 

<br>
<div align="left">
<img alt="RGPD" src="images/IOT_69.png" width="500px">
</div>
<br>

> Source image : [numeriqueethique.fr](https://numeriqueethique.fr/ressources/articles/objets-connectes-amis-ou-ennemis-de-l-ecologie)

D'un point de vue environnemental, l’IoT pourrait avoir un impact négatif important.
En effet, comme mentionné précédemment, il est anticipé le déploiement de milliards d'objets connectés.

- Quel sera l'impact sur l'environnement de tous ces objets disséminés lorsqu'ils arriveront en fin de vie ?
- Sera-t-il possible de les reconditionner pour d'autres fonctions plutôt que d'en produire de nouveaux ?

## Références