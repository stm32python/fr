---
title: IoT et cybersécurité
description: Chapitre sur la cybersécurité et l'IoT.
---

# IoT et cybersécurité

L'IoT offre est particulièrement exposé aux cyberattaques compte tenu de sa complexité. Celles-ci peuvent cibler **les objets contraints** et/ou **les passerelles** et/ou **les plateformes**. La cybersécurité est donc indispensable pour réduire cette surface d'attaque.

## La cryptographie, base de la cybersécurité

Une bonne partie des mécanismes de cybersécurité repose sur **des méthodes logicielles cryptographiques** qui réalisent **des opérations mathématiques de [chiffrement](https://fr.wikipedia.org/wiki/Chiffrement)** (permutations, substitutions, exponentiation modulaire, etc.) des données au niveau binaire. 

Ces opérations fournissent les garanties suivantes :

- **Authenticité** : Garantie sur l'origine des données ;
- **Intégrité** : Garantie que les données originales n'ont pas été falsifiées ;
- **Confidentialité** : Garantie que les données sont intelligibles uniquement par les destinataires autorisés ;
- **Non-répudiation** : Garantie qu’une action sur la donnée réalisée au nom d’un utilisateur (après authentification) ne saurait être répudiée par ce dernier.

Parmi les outils cryptographiques fondamentaux on peut citer :

- Les [**fonctions de hachage** (ex : SHA-2)](https://fr.wikipedia.org/wiki/SHA-2) et codes d'authentification de messages ;
- Les [**systèmes de signature numérique**](https://fr.wikipedia.org/wiki/Signature_num%C3%A9rique) ;
- Le [**chiffrement à clef privée** (symétrique)](https://fr.wikipedia.org/wiki/Cryptographie_sym%C3%A9trique) ;
- Le [**chiffrement à clef publique** (asymétrique)](https://fr.wikipedia.org/wiki/Cryptographie_asym%C3%A9trique) ;
- Les [**stratégies d'échange de clés** (ex : Diffie-Hellman)](https://fr.wikipedia.org/wiki/%C3%89change_de_cl%C3%A9s_Diffie-Hellman).

Les microcontrôleurs qui équipent les systèmes IoT récents intègrent tous des circuits spécialisés pour l'accélération des calculs cryptographiques à moindre coût énergétique.

Une introduction à la cryptographie est disponible sur [le site de la CNIL : « Comprendre les grands principes de la cryptologie et du chiffrement »](https://www.cnil.fr/fr/cybersecurite/comprendre-les-grands-principes-de-la-cryptologie-et-du-chiffrement) :

<br>
<div align="left">
<img alt="Les usages de la cryptographie" src="images/CNIL_Usages_Cryptographie.png" width="550px">
</div>
<br>

> Source image : [CNIL](https://www.cnil.fr/fr/cybersecurite/comprendre-les-grands-principes-de-la-cryptologie-et-du-chiffrement)

Une des grandes faiblesses des objets connectés est **le laxisme de certains fabricants** qui proposent des firmwares contenant des vulnérabilités logicielles affectant directement les fonctions cryptographiques (mots de passe par défaut que l'utilisateur n'est pas obligé de changer, algorithmes contenant des failles, etc.) et qui n'assurent pas des mises à jour régulières pour les corriger.

## Les cyberattaques visant l'IoT

Le schéma qui suit liste des types de cyberattaques que peut subir un système IoT :

<div align="left">
<img alt="Types de menaces" src="images/IOT_72.jpg" width="500px">
</div>

> Source image : [RIOT-OS](https://riot-os.github.io/riot-course/slides/06-security-with-riot/#2)

### Niveau 1 : Attaques de communication

Les attaques de communication ciblent les échanges sur le réseau. Leur principe est qu'un intermédiaire malveillant s'insère quelque part sur le chemin pris par la communication et ce faisant :

- Perturbe la communication ;
- Analyse le trafic envoyé sur le réseau. Intercepter des éléments sur la communication entre ses cibles permet de déduire des indices sur leur activité.
- Leurre d'autres entités sur le réseau pour amplifier (à leur insu) une autre attaque de plus grande envergure.

Parmi les attaques de communication les plus fréquentes, citons :

* Les attaques [**Sinkhole**](https://www.sciencedirect.com/topics/computer-science/sinkhole-attack) qui induisent en erreur les éléments du réseau pour faire passer le trafic via un nœud malveillant, lequel peut couper ce trafic.
* Toutes les attaques connues des procédés cryptographiques, par exemple [**Man in the middle**](https://fr.wikipedia.org/wiki/Attaque_de_l%27homme_du_milieu) par laquelle un acteur malveillant intercepte et falsifie les échanges entre deux terminaux.

Des protocoles réseau spécifiques doivent être utilisés pour se défendre contre ces attaques.

### Niveau 2 : Attaques logicielles

Les attaques logicielles exploitent les vulnérabilités des micrologiciels embarqués sur les objets connectés. Le code malveillant est injecté dans le logiciel de l'objet et peut avoir les conséquences suivantes :

- L'objet divulgue des informations confidentielles ;
- L'attaquant prend (secrètement) le contrôle de l'objet.

Parmi les attaques logicielles les plus fréquentes, citons :

- Les [**buffer overflows**](https://resources.altium.com/p/internet-of-things-security-vulnerabilities-all-about-buffer-overflow). C'est un exemple typique **d'attaque par faute**. Elle consiste à pousser le système en dehors de sa plage de fonctionnement nominal, ce qui le fait fauter, fautes ensuite analysées pour en tirer des informations pertinentes.

- Les [**malwares**](https://fr.wikipedia.org/wiki/Logiciel_malveillant), installés furtivement sur l'appareil pour lancer des opérations malveillantes telles, par exemple, que l'identification et la divulgation des clefs cryptographiques chargées dans la mémoire des microcontrôleurs.

- La **falsification du firmware de l'objet** par un attaquant. Le firmware de substitution pourra usurper l'identité de l'objet et accéder aux clefs cryptographiques d'autres processus ou objets de l'infrastructure, envoyer de fausses informations, etc.

Des mécanismes "hardware" doivent être utilisés pour se défendre de ce type d'attaques qui peuvent, par exemple, viser à protéger les accès en lecture ou en écriture de certaines zones de la mémoire du microcontrôleur qui anime l'objet.

### Niveau 3 : Attaques matérielles

Les attaques matérielles exploitent des vulnérabilités via une interaction physique avec les objets. D'après [Pablo Rauzy (Université Paris 8, cours « Sécurité et systèmes embarqués »)](https://pablo.rauzy.name/teaching/2016-2020.html#sese), les systèmes embarqués et notamment les objets connectés de l'IoT, sont par définition **des cibles privilégiées** pour ces attaques car **ils peuvent très facilement se retrouver entre les mains de l’attaquant**.

Les **attaques matérielles en déni de service** consistent en **du sabotage** pur et simple. Un objet peut être mis en panne ou réduit au silence à l'aide d'interférences radio ou d’une coupure de câbles.

Plus subtiles, [**les attaques par canaux auxiliaires**](https://fr.wikipedia.org/wiki/Attaque_par_canal_auxiliaire) (en anglais **side-channel attacks**) exploitent des failles aussi bien au niveau logiciel que matériel. Il existe différentes catégories d’attaques par canaux auxiliaires ...

* Les attaques **passives** (non invasives) :

   - **Analyse de consommation de courant**. Une analyse détaillée de la consommation de courant pendant les opérations cryptographiques peut indirectement révéler tout ou partie d'une **clef cryptographique**. 

    - **Analyse de temps de calcul**. Cette attaque consiste en l'étude du temps mis pour effectuer certaines opérations, toujours pour déduire des informations sur les clefs cryptographiques.

    - **Analyse acoustique**. Cette attaque consiste en l'étude du bruit généré par un ordinateur ou une machine qui chiffre. En effet, le processeur émet du bruit qui varie en intensité et en nature selon sa consommation et les opérations effectuées (typiquement des condensateurs qui se chargent ou se déchargent émettent un claquement facilement mesurable).

    - **Analyse des émanations électromagnétiques**. Cette attaque est similaire à la cryptanalyse acoustique, mais utilise le rayonnement électromagnétique (émission d'ondes, analyse d'une image thermique, lumière émise par un écran, etc.).

    - Etc.
      
* Les attaques **actives semi-invasives** ou **attaques par injection de faute**. Elles consistent à provoquer une erreur pendant l'exécution du firmware, par exemple en envoyant sur le microcontrôleur une impulsion laser ou électromagnétique ou encore en perturbant ponctuellement son circuit d'alimentation puis en analysant les conséquences.

* Les attaques **actives invasives**. Elles consistent à modifier physiquement le microcontrôleur. On peut par exemple utiliser des acides ou des faisceaux d'ions focalisés pour mettre à nu le circuit électronique (deprocessing) puis appliquer des techniques de **rétroingénierie** ou encore installer **des sondes** dans ses microstructures (microprobing). Dans ce cas, l'attaque permet de voler l'information, mais le matériel est détruit. Les attaques invasives sont évidemment très complexes et coûteuses et restent hors de portée de la plupart des cybercriminels.

<br>**Attaque matérielle par injection de fautes à l'aide d'un laser (Mines Saint-Étienne) :**

<div align="left">
<img alt="IMT : attaque par injection de fautes laser" src="images/banc-laser.jpg" width="700px">
</div>

> Source image : [l'MTech](https://imtech.imt.fr/2018/05/28/attaques-materielles-objets-connectes/)

### Niveau 4 : Attaques cyber-physiques

En plus des attaques de communication, logicielles et matérielles, de nouveaux types d'attaques peuvent exploiter **les caractéristiques cyber-physiques** du système.

Un exemple concret d'une telle attaque serait la [**réaction en chaîne**](https://eprint.iacr.org/2016/1047.pdf) provoquée par un botnet de climatiseurs qui pourraient synchroniser leur demande maximale d’électricité pour provoquer la rupture du réseau électrique. 

[**Le ver Stuxnet**](https://fr.wikipedia.org/wiki/Stuxnet), responsable en 2009 de la destruction des centrifugeuses du programme militaire Iranien d'enrichissement d'uranium, est un exemple d'attaque cyber-physique. Remarquons que les centrifugeuses Iraniennes ne faisaient pas partie de l'IoT mais de systèmes d'automatisme industriel locaux qui ont été compromis via des clefs USB "infectées".

## Exemple : Les cartes à puce sécurisées

En 1974, [Roland Moréno](https://fr.wikipedia.org/wiki/Roland_Moreno), un ingénieur français, met au point « un objet portable à mémoire » qui ouvre la voie à la carte de paiement à puce à la fin des années 1970. La [carte à puce](https://fr.wikipedia.org/wiki/Roland_Moreno) est sans doute l'application embarquée et l'objet connecté le plus directement associé à la cybersécurité.

<div align="left">
<img alt="Types de menaces" src="images/cb.jpg" width="400px">
</div>

> Source image : [MoneyVox](https://www.moneyvox.fr/carte-bancaire/actualites/97042/carte-bancaire-a-puce-comment-cette-invention-francaise-a-conquis-le-monde)

Les **objectifs principaux** d'une carte à puce sont de **protéger les données qu'elle contient contre toute intrusion** et **d'exécuter des algorithmes cryptographiques dans un contexte extrêmement sécurisé**. 

Elle est désormais largement adoptée dans le monde entier pour des applications très sensibles telles que le paiement dématérialisé, la sécurisation des dépenses de santé (carte Vitale en France), la sécurisation des abonnements de téléphonie mobile (cartes SIM) ou encore les passeports et cartes d'identité biométriques.

Bien que ces fonctions soient progressivement intégrées dans les smartphones, les cartes à puces conservent de sérieux avantages (très faible coût, robustesse, grand recul sur l'efficacité de leur sécurité, utilisation simple ...) et ne sont pas prêtes de disparaître.

La technologie des cartes à puces repose sur **des microcontrôleurs dits "sécurisés"** (SMCU), intégrant toutes sortes de contremesures aux attaques invasives, semi-invasives et non invasives. Par exemple :

- Détection des fluctuations de tension d'alimentation, de fréquence d'horloge, de température.
- Des générateurs de bruit aléatoire fonctionnent en parallèle des unités de calcul cryptographiques, pour brouiller les signatures de consommation et électromagnétiques.

- Les temps de calcul sont également rendus fluctuants pour ne rien révéler sur les algorithmes en cours d'exécution.

- Des "fusibles" logiciels et matériels effacent les clefs cryptographiques et les données criques si une tentative d'intrusion est détectée. Par exemple, le SMCU intègre un maillage fin et complexe qui les protègent partiellement des ondes électromagnétiques et complique considérablement d'éventuelles opérations de deprocessing / reverse engineering.

- Une architecture des mémoires (ROM, RAM, SRAM) qui complique le rétro-engineering ou la capture de clefs cryptographiques pendant le fonctionnement.

- Etc.

Les générations les plus récentes de cartes à puces utilisent [la technologie radiofréquences RFID - NFC](protocole_rf) pour simlifier les opérations les plus courantes (ex : portes-monnaie électroniques), une innovation qui a soulevé pendant un temps de fortes réticences compte tenu des nouvelles vulnérabilités introduites par les communications sans fil.

## Références

- [L'article de Wikipedia sur les attaques par canaux axillaires](https://fr.wikipedia.org/wiki/Attaque_par_canal_auxiliaire)
- [Le cours « Sécurité et systèmes embarqués » de Pablo Rauzy (Université Paris 8)](https://pablo.rauzy.name/teaching/2016-2020.html#sese)
- [Site de ma CNIL : « Comprendre les grands principes de la cryptologie et du chiffrement »](https://www.cnil.fr/fr/cybersecurite/comprendre-les-grands-principes-de-la-cryptologie-et-du-chiffrement)
- [Le cours « Sécurité des systèmes d'information » - Chapitre 1 - Cryptographie de Tarek Hdiji](https://fr.slideshare.net/slideshow/ch1cryptographie-scurit-informatiquepptx/266997730)
- [Cyberphysical Security for the Masses: A Survey of the Internet Protocol Suite for Internet of Things Security](HAL-version.pdf)
- Article de Ronan Lashermes, chercheur en sécurité des systèmes embarqués, INRIA : [ « Attaques par faute  »](https://ronan.lashermes.0nline.fr/papers/MISC96.pdf)
- L'ouvrage *"La cryptographie décryptée"* de H.X. Mel & Doris Baker, éditions CampusPress