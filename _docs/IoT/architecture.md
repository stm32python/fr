---
title:  Les éléments d’un système IoT
description: Chapitre sur l'architecture IoT.
---

# Les éléments d’un système IoT

Dans leur mise en œuvre technique, la plupart des solutions IoT sont fondées sur trois éléments principaux : 

- <span style="font-size: 15px;">**Les objets contraints ou terminaux (devices)**, qui génèrent les données et les envoient à des passerelles ...</span>
- <span style="font-size: 15px;">**Les passerelles (Gateways)**, qui font transiter ces données à travers le réseau pour atteindre ...</span>
- <span style="font-size: 15px;">**Des serveurs dans le cloud** qui hébergent les **plateformes IoT** (IoT Cloud Platform).
</span>

<br>
<div align="left">
<img alt="architecture IoT" src="images/IOT_8.jpg" width="700px">
</div>
<br>

## Les objets contraints (Devices)

Les objets, qu'il s'agisse de capteurs pour remonter des données physiques ou d'actionneurs pour agir sur l'environnement physique, sont à la base des solutions IoT. **Ce sont d'eux que proviennent les données.**

Ces objets sont généralement contraints en taille et équipés de **batteries** leur conférant une **autonomie limitée**. Ils fonctionnent avec un microcontrôleur (MCU) ce qui veut aussi dire qu'ils ont des **capacités de traitement limitées** par comparaison avec des architectures à microprocesseurs conventionnelles (PC, serveurs ...).

Exemples de Devices :

- <span style="font-size: 15px;">Capteur de température connecté</span>
- <span style="font-size: 15px;">Montre connectée</span>
- <span style="font-size: 15px;">Tracker de positionnement pour le transport</span>
- <span style="font-size: 15px;">Capteur d’ouverture de porte connecté</span>
- <span style="font-size: 15px;">Lampe connectée</span>
- <span style="font-size: 15px;">Pompe à insuline connectée</span>

En fait, toutes sortes d'objets peuvent être connectés, les limites ne sont fixées que par l'imagination (et le bon sens ...).
Ces objets ont pour rôle de supporter des tâches spécifiques, ce qui correspond à la définition **d'applications embarquées**. La partie logicielle qu'ils exécutent pourra comporter :

- <span style="font-size: 15px;">Un micro-système d'exploitation tel que [FreeRTOS](https://www.freertos.org/) ou [RIOT OS](https://www.riot-os.org/) ;</span>
- <span style="font-size: 15px;">Une couche d'abstraction matérielle (Hardware Abstraction Layer - HAL), qui simplifie les accès aux fonctionnalités du [MCU](../Microcontrollers/microcontroleur) (mémoire flash, GPIOs, interface série, etc.) ; </span>
- <span style="font-size: 15px;">Une couche de communication, fournissant des solutions pour permettre à l'objet de communiquer via un protocole filaire ou sans fil comme [Bluetooth Low Energy (BLE)](../Embedded/ble), [Z-Wave](https://www.z-wave.com/), [Thread](https://www.threadgroup.org/), [CAN](https://www.can-cia.org/) bus, [MQTT](https://mqtt.org/), [CoAP](https://coap.technology/), etc. ;</span>
- <span style="font-size: 15px;">Une gestion à distance, permettant de les contrôler, de mettre à jour leurs firmwares, de surveiller leurs niveaux de batterie, etc.</span>

## Les passerelles (Gateway)

### Définition

Les passerelles (*gateways* en anglais) sont des points de relais entre un réseau local de capteurs et d'actionneurs et un réseau externe, en général Internet. Exemples de passerelles :

* Box ADSL ou fibre (Passerelle LAN -> Internet)
* Gateway LoRaWAN (Passerelle LoRA -> Internet)
* Gateway Sigfox  (Passerelle Sigfox -> Internet)

Le réseau de capteurs est souvent réalisé dans les [bandes ISM](../Embedded/lora_ism) (fréquences libres d’attribution). C’est par exemple le cas des réseaux [LoRaWAN](../Embedded/lora.md) et [Sigfox](https://www.sigfox.com/). La passerelle connecte ces réseaux à Internet.

### Edge computing

Une passerelle peut offrir des capacités de traitement des données et de stockage à la périphérie des objets contraints  : on parle alors [**d'edge Computing**](https://fr.wikipedia.org/wiki/Edge_computing). 
L'edge computing effectue un maximum de traitement le plus tôt possible, dès que les données et la puissance de calcul sont réunis. Cette approche permet de :

- <span style="font-size: 15px;">Réduire la quantité de données envoyées dans le cloud ;</span>
- <span style="font-size: 15px;">Réduire la latence ;</span>
- <span style="font-size: 15px;">Réduire le trafic.</span>

On peut également étendre la notion d'edge computing en **confiant une première étape de traitement des données aux microcontrôleurs (MCU) des objets contraints**. Bien qu'ils restent très peu puissants par comparaison avec des microprocesseurs (CPU), les MCU intègrent de plus en plus d'unités spécialisées qui leur permettent d'effectuer des calculs mathématiques complexes, tels que ceux de l'Intelligence Artificielle, pour une fraction de l'énergie consommée par un CPU et tout aussi rapidement que celui-ci.

## Les plateformes IoT – Définition

Les plateformes IoT représentent les solutions logicielles et les services requis pour mettre en œuvre les solutions IoT.
Elles s'exécutent sur des serveurs, hébergés dans des infrastructures cloud ([AWS](https://aws.amazon.com/), [Google Cloud](https://cloud.google.com/), [Microsoft Azure](https://azure.microsoft.com/), etc.) ou dans l'infrastructure de l'entreprise.<br>
Ces solutions doivent pouvoir s'adapter aux passages à l'échelle aussi bien au niveau horizontal (nombre d'objets connectés) qu'au niveau vertical (variété des solutions apportées). Les plateformes IoT permettent une interconnexion avec le système d'information existant de l'entreprise.

<br>
<div align="left">
<img alt="Plateforme IoT" src="images/IOT_12.png" width="714px">
</div>
<br>

> Source image : [MOOC IoT INRIA](https://learninglab.gitlabpages.inria.fr/mooc-iot/mooc-iot-ressources/)

# Problématique centrale de l'architecture IoT : sa consommation

Malgré la diversité des cas d'usages, l'architecture d'une application IoT sera généralement la suivante : 

<br>
<div align="left">
<img alt="Chaine IoT" src="images/IOT_13.png" width="713px">
</div>
<br>

Dans la majorité des cas, et selon les détails de l'application, il faut donc porter un soin particulier au choix du type de communication entre les objets et « Internet ». 
Pour commencer, il faut se poser une question primordiale : "*Est-ce que mon capteur doit être oublié ?"*. Si oui, je dois donc m’assurer que son autonomie est maximale, une faible consommation pour une alimentation sur batterie ou bien une alimentation sur secteur s'imposent.

Si nous mettons de côté les capteurs alimentés sur secteur, **les étapes déterminantes pour la consommation d'une infrastructure IoT utilisant des objets alimentés par des batteries** seront :

- <span style="font-size: 15px;"> L'acquisition de la donnée</span>
  - <span style="font-size: 14px;">La nature des capteurs</span>
  - <span style="font-size: 14px;">Le nombre de capteurs</span>
  - <span style="font-size: 14px;">Les fréquences d’acquisition</span>
- <span style="font-size: 15px;">Le traitement de la donnée </span>
  - <span style="font-size: 14px;">Les filtres, moyennes, l'edge AI ...</span>
  - <span style="font-size: 14px;">Le stockage</span>
- <span style="font-size: 15px;">L'envoi de la donnée</span>
  - <span style="font-size: 14px;">Les fréquences d’envoi</span>
  - <span style="font-size: 14px;">La communication est-elle bidirectionnelle ?</span>
  - <span style="font-size: 14px;">La technologie de communication (dont modulation)</span>
  - <span style="font-size: 14px;">La portée requise pour le signal</span>

Les deux dernières étapes, la supervision de l'application et l'analyse des données remontées, ne pèseront pas sur la consommation des objets puisqu'elles seront nécessairement réalisées dans une architecture "cloud" par des serveurs alimentés sur secteur.

## Acquisition : capteurs, nombre et fréquences

Le choix des capteurs est important. Certains capteurs nécessitent des sources d’alimentation plus conséquentes que d’autres.<br>
Plus le nombre de capteurs branchés est important, plus la consommation le sera également.<br>
Les fréquences d’acquisition influencent directement la consommation. Est-ce que je dois récupérer des données en continu ou ponctuellement ?<br>
Par exemple, [un accéléromètre MEMS]() dont l’acquisition se ferait à plusieurs kilohertz ne serait pas forcément compatible avec une alimentation de type pile bouton classique.

<br>
<div align="left">
<img alt="Accéléromètres IoT" src="images/IOT_14.png" width="400px">
</div>
<br>

> Source image : [secondelmb.free.fr](http://secondelmb.free.fr/edc2/activites/act3.html)

Un moyen très efficace pour économiser l'énergie consiste à mettre l’objet en veille entre ses phases d'activité. On peut également utiliser des fonctionnalités de détection de choc ou de chute permettant de « réveiller » l’objet, toujours grâce à un accéléromètre MEMS, via le mécanisme de gestion des interruptions de son MCU.

## Traitement de la donnée

Le stockage de la donnée peut engendrer l’intégration de composants supplémentaires. Il peut suivre trois stratégies :

- <span style="font-size: 15px;">**Edge computing** : Analyse et traitement au plus près des objets. Dès lors, la quantité́ de données à transférer et à stocker sur le cloud est considérablement réduite. Le fait de placer de l'intelligence dans les passerelles, voire **dans les objets** (ex. [Edge AI](https://stm32ai.st.com/)), permet une  diminution des coûts mais interdit à l'exploitant de corréler les données d'un point de vue global sur le cloud.</span>

- <span style="font-size: 15px;">**Cloud computing** : Aucun traitement en local. Toutes les données reçues par les objets sont transférées dans un "data lake" (une énorme base de données). Cela impacte directement le canal de transmission qui doit pouvoir supporter un débit et une charge plus importants.</span>

- <span style="font-size: 15px;">**Hybride** : Il est possible de réaliser des prétraitements en local pour pousser une donnée plus légère et de finir les traitements en fin de chaine (concentrateur, cloud …).</span>

<br>
<div align="left">
<img alt="Computing" src="images/IOT_15.png" width="504px">
</div>
<br>

> Source image : [cardinalPeak](https://www.cardinalpeak.com/blog/at-the-edge-vs-in-the-cloud-artificial-intelligence-and-machine-learning)

## Envoi de la donnée

**Fréquence d’envoi** : Tout comme les fréquences d’acquisition, les fréquences d’envoi font appel à des composants électroniques qui consomment du courant. Il est indispensable de bien régler ces fréquences si la consommation est un point un important du projet.

**Communication bidirectionnelle** : La consommation ne sera pas du tout la même si l’objet est capable de recevoir de l’information en plus d’avoir la capacité à l’émettre. Etre à l’écoute en permanence lui fera perdre une autonomie conséquente.

**Technologie de communication** : Le choix de la technologie impacte directement la consommation (par exemple, le Wi-Fi est beaucoup plus gourmand que le Bluetooth). La stratégie la plus efficace pour économiser de l'énergie consiste à synchroniser les communications entre les objets et à les mettre en "sommeil" entre deux échanges.

<br>
<div align="left">
<img alt="Computing" src="images/IOT_16.jpg" width="450px">
</div>
<br>

**Portée et topologie du réseau** : La portée est dépendante de différents facteurs parmi lesquels ...

  - <span style="font-size: 15px;">La technologie utilisée</span>
  - <span style="font-size: 15px;">Le débit attendu</span>
  - <span style="font-size: 15px;">L'environnement (topologie du terrain, conditions météo)</span>
  - <span style="font-size: 15px;">**La topologie du réseau**, qui correspond à son architecture logique, définissant les liaisons entre ses objets et une hiérarchie éventuelle entre eux. Les principales topologies utilisées dans les télécommunications sont illustrées ci-dessous.</span>

  <br>

**Les principales topologies des réseaux de communications**

|La topologie Mesh (maillée)|La Topologie en étoile|La Topologie cellulaire|
|:-:|:-:|:-:|
|<img alt="Mesh" src="images/IOT_17.png" width="350px">|<img alt="Etoile" src="images/IOT_18.png" width="250px">|<img alt="Cellulaire" src="images/IOT_19.png" width="250px">|


## Références
-  [Le MOOC IoT de l'INRIA](https://learninglab.gitlabpages.inria.fr/mooc-iot/mooc-iot-ressources/)
