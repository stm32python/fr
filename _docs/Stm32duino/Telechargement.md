---
title: Ressources et téléchargements
description: Liens de téléchargement des firmwares et tutoriels, liens vers des ressources sur des sites en ligne pour Arduino et STM32duino
---

# Ressources Arduino

Vous trouverez dans cette section des liens vers des ressources en ligne utiles (ou indispensables !) pour l'apprentissage d'Arduino en général, et d'Arduino pour les microcontrôleurs STM32 en particulier. De nombreuses sources ou ressources recensées ici ont servi à la construction de ce site et sont utilisées au fil des tutoriels.

## Environnements de programmation en ligne

 - [*STudio4Education* de Sébastien Canet (Blockly)](https://GitHub.com/A-S-T-U-C-E/STudio4Education)

## Documentation en ligne

**Tutoriels Arduino**

  - [Les tutos pour utiliser l'IDE Arduino version 2](https://docs.arduino.cc/software/ide-v2)
  - [Le génialissime blog d'Eskimon](https://eskimon.fr/faire-communiquer-son-arduino-avec-un-appareil-android)
  - Des milliers d'autres sites ...

 **Documentation technique des cartes NUCLEO et X-NUCLEO**

  - [NUCLEO-WB55](https://www.st.com/en/evaluation-tools/p-nucleo-wb55.html)
  - [NUCLEO-L476RG](https://www.st.com/en/evaluation-tools/nucleo-l476rg.html)
  - [X-NUCLEO IKS0143](https://www.st.com/en/ecosystems/x-nucleo-iks01a3.html)


## Téléchargements

### Editeurs de texte et de code

  - [L'IDE Arduino version 2](https://www.arduino.cc/en/software#future-version-of-the-arduino-ide)

### Applications pour smartphones

  - Pour créer facilement des applications sur smartphones Android ou Apple, l'environnement [MIT App inventor](../tools/AppInventor/)
  - Une application client BLE sur smartphone par STMicroelectronics : [STBLESensor](https://www.st.com/en/embedded-software/stblesensor.html)
  - Une application client BLE sur smartphone, [nRF Connect for Mobile (versions Android)](https://play.google.com/store/apps/details?id=no.nordicsemi.android.mcp&hl=fr&gl=US&pli=1) ou [nRF Connect for Mobile (versions Apple)](https://apps.apple.com/ch/app/nrf-connect-for-mobile/id1054362403?l=fr) de [Nordic Semiconductor ASA](https://infocenter.nordicsemi.com/index.jsp)

### Logiciels pour manipuler la mémoire flash des MCU STM32

 - Dans quelques circonstances (décrites au fil des tutoriels) vous pourrez être amené à effacer la mémoire flash du microcontrôleur STM32 sur votre carte NUCLEO, ou encore à mettre à jour les firmwares d'autres composants sur celle-ci. Pour ces opérations vous aurez besoin du logiciel [STM32CubeProgrammer](https://www.st.com/en/development-tools/stm32cubeprog.html) et de [ce tutoriel](./../tools/cubeprog/index).

### Sketchs Arduino pour les tutoriels de ce site

Vous trouverez l'ensemble des sketchs présentés dans la section [Tutoriels](../Stm32duino/exercices/) dans [cette archive ZIP](./../../assets/Sketch/TUTOS.zip).

## Autres ressources

### Protocoles IoT

 - [Le guide Internet de Steve sur le MQTT](http://www.steves-internet-guide.com/)
 - [Le site The Things Network](https://www.thethingsnetwork.org/)
 - [Un TP de M. Silanus sur LoRaWAN et TTN](http://silanus.fr/bts/formationIOT/LoRa/lora.pdf)
 - [La plateforme en ligne ThingsBoard](https://thingsboard.io/)




