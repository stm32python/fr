---
title: Exercice de communication avec Firmata
description: Exercice de communication avec Firmata
---

# Exercice de communication avec Firmata

[Firmata](http://firmata.org/wiki/Main_Page) est un protocole de communication entre un PC et une carte à micro-contrôleur comme l'Arduino. Ce protocole permet de configurer les GPIO en entrée ou en sortie pour que un programme sur le PC puisse lire les entrées et positionner des sorties de la carte.

Dans cet exemple, vous utiliserez Firmata avec votre carte STM32.

Fermez la console série.

Ouvrez l'exemple nommé `Firmata` en faisant : `Fichier > Exemples > Firmata > StandardFirmata`

Puis téléverser le programme comme réalisé précédemment.

![Image](images/2_firmata/1.png)

Télécharger l’exécutable du client Firmata (correspondant à votre système d'exploitation) à cette adresse :

http://firmata.org/wiki/Main_Page

Cliquez sur l’exécutable (pas d'installation requise), le client Firmata s'ouvre :

Cliquez sur `Port` et sélectionnez le port COM correspondant à votre périphérique.

![Image](images/2_firmata/2.png)

Redémarrer votre STM32 en appuyant sur le bouton poussoir de couleur noire.
Une LED devrait clignoter rapidement sur votre STM32 juste après l'avoir redémarré,
cela veut dire que Firmata est prêt a être utilisé !

Le client Firmata apparait et les entrées/sorties peuvent être configurées sur votre carte.

![Image](images/2_firmata/3.png)

