---
title: Exercice avec le module GPS SIM28 Grove en C/C++ avec Stm32duino
description: Exercice avec le module GPS SIM28 Grove en C/C++ avec Stm32duino
---

# Exercice avec le module GPS SIM28 Grove en C/C++ avec Stm32duino

<div align="left">
<img alt="Grove - GPS" src="images/grove-gps.jpg" width="400px">
</div>

Bientôt disponible

> Crédit image : [Seeedstudio](http://wiki.seeedstudio.com/Grove_System/)
