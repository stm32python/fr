---
title: Exercice avec l'adaptateur Nintendo Nunchuk Grove en C/C++ avec Stm32duino
description: Exercice avec l'adaptateur Nintendo Nunchuk Grove en C/C++ avec Stm32duino
---

# Exercice avec l'adaptateur Nintendo Nunchuk Grove en C/C++ avec Stm32duino

<h2>Description</h2>
Le Nunchuk est une manette supplémentaire venant se connecter à la WiiMote (une manette destinée à être utilisée pour la console Nintendo Wii).
Le Nunchuk est composé des caractéristiques suivantes :
 - Joystick 2 axes (x, y)
 - Accéléromètre 3 axes (x, y, z)
 - Boutons poussoirs (C et Z)

Elle communique en liaison [I2C](../../Kit/glossaire) avec la Wiimote. Un mode de communication ne necéssitant que 4 fils (5V, GND, SDA, SCL).


<h2>Montage</h2>

Afin d'utiliser la manette nous utilisons un connecteur UEXT vers Grove comme celui ci : 
<div align="left">
<img alt="Grove - Nintendo Nunchuk Adapter" src="images/grove-nunchuk.jpg" width="200px">
</div>

Il faut ensuite venir connecter la manette à un connecteur I2C au Nucleo en utilisant les ports Arduino. On vient se brancher de cette facon :

| Connecteur UEXT   | ST Nucleo         |
| :-------------:   | :---------------: |
|       pwr         |        3.3V       |
|       gnd         |        GND        |
|        d          |        D14        |
|        c          |        D15        |




<h2>Programme</h2>
Pour simplifier le code nous utilisons une bibliothèque externe. Pour cela il faut aller dans l'IDE Arduino, sélectionner le menu *Outils* puis cliquer sur le menu *Gérer les bibliothèques*. Une nouvelle fenêtre s'ouvre, dans la barre de recherche tapez : *nintendo extension ctrl* puis installez la librairie proposée (ici en version 0.8.1). Le fichier du projet Arduino est quant à lui disponible via ce [lien](https://gitlab.com/stm32python/fr/-/blob/master/assets/Script/Nunchuk/Nunchuk.ino).

**Etape 1 :** Pour faire fonctionner le programme nous devons dans un premier temps importer la bibliothèque téléchargée précédemment. Pour se faire, il faut l'importer au tout début de notre code de cette facon : 
```
#include <NintendoExtensionCtrl.h>
```

On vient également créer une variable *nchuck* qui récupéra les informations retournées par la bibliothèque.
```
Nunchuk nchuk;
```

**Etape 2 :** Ensuite on vient configurer la boucle *setup* en initialisant la liaison série afin d'avoir les informations dans le moniteur série et on vient aussi initialiser la librairie. 
```
void setup() {
  Serial.begin(115200);
  nchuk.begin();

  while (!nchuk.connect()) {
    Serial.println("Manette non detectée !");
    delay(1000);
  }
}
```

**Etape 3 :** Enfin nous mettons à jour les données acquises et les affichons lorsque le Nunchuk est connecté :

```
void loop() {
  boolean success = nchuk.update();     //Récuperation des données de la manette

  if (success == true) {                //Si la connection est réussie
    nchuk.printDebug();                 //Affichage des valeurs
    delay(500);
  }
  else {                                //Si la manette est deconnectée
    Serial.println("Manette deconnectée !");
    delay(1000);
    nchuk.connect();
  }
}
```


<h2>Résultat</h2>

Il ne vous reste plus qu'à appuyer sur le bouton *téléverser* pour transférer le programme puis aller dans le menu *Outils* puis *Moniteur Série* pour observer le résultat !

Nous pouvons à présent observer les données extraites du Nunchuk sous cette forme :


<div align="left">
<img alt="Affichage des données du Nunchuk" src="images/nunchuk-c.png" width="800px">
</div>


Faites un mouvement avec le Nunchuk et vous devriez voir les valeurs de l'accéléromètre varier selon l'axe sur lequel vous l'avez deplacé. Vous pouvez également faire bouger le joystick et voir où celui ci se situe. De plus vous avez un retour d'informations sur l'état des boutons poussoirs.

**Remarque :** En cas de problème vérifiez que vous vous êtes connecté au bon port COM dans l'IDE Arduino. Si le problème provient du moniteur série vérifiez que celui-ci est bien configuré à 115200 bauds sinon vérifiez la connection de la manette.


<h2>Pour aller plus loin</h2>

Dans cet exercice nous avons vu l'affichage des données du Nunchuk. Pour aller plus loin nous pouvons, par exemple, récupérer ces données et les traiter pour activer des servos moteurs en fonction de la direction du joystick.

(prochainement)


> Crédit image : [Seeedstudio](http://wiki.seeedstudio.com/Grove_System/)

