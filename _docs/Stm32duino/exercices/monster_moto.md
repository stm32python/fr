---
title: Exercice de pilotage de moteurs avec la carte SparkFun Monster Moto Shield en C/C++ avec Stm32duino
description: Exercice de pilotage de moteurs avec la carte SparkFun Monster Moto Shield en C/C++ avec Stm32duino
---

# Exercice de pilotage de moteurs avec la carte SparkFun Monster Moto Shield en C/C++ avec Stm32duino

## La carte SparkFun Monster Moto Shield
La carte [SparkFun Monster Moto Shield](https://www.sparkfun.com/products/retired/10182) est une carte permettant de piloter deux très puissants moteurs à courant continu. Elle est équipée de deux composants de pilotage de moteur [VNH2SP30](http://cdn.sparkfun.com/datasheets/Dev/Arduino/Shields/10832.pdf) de ST destinés à l'industrie automobile.

Elle n'est malheureusement plus fabriquée et distribuée par Sparkfun. Cependant, elle est encore distribuée par plusieurs fournisseurs. Sa schématique est disponible en open-source.

<div align="left">
<img alt="SparkFun Monster Moto Shield" src="https://air.imag.fr/images/2/28/MonsterMotoShield%2BSTNucleoF401.jpg" width="800px">
</div>

## Démarrage
Branchez la carte SparkFun Monster Moto Shield sur la carte Nucleo.

Compilez et téléchargez le croquis [MonsterMoto.ino](https://gitlab.com/stm32python/stm32duino/-/tree/master/MonsterMoto) sur la carte Nucleo.

Affichez le moniteur série (configuré en 9600 baud).

## Suggestions d'exercice
* Ajoutez au croquis précédent la carte MEMS IKS01Ax pour corriger la trajectoire du robot au moyen du magnétomètre LIS2MDL.
* Ajoutez au croquis précédent le capteur à ultrason pour l'évitement d'obstacles.
* Créez une application de télécommande des moteurs avec MIT App Inventor.

## Références

* [Code Sources sur GitHub](https://GitHub.com/sparkfun/Monster_Moto_Shield/tree/Hv12Fv10)

> Crédit image : [Air](https://air.imag.fr/images/2/28/MonsterMotoShield%2BSTNucleoF401.jpg)
