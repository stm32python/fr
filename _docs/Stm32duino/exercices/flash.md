---
title: Programmer la mémoire Flash d'un microcontrôleur avec STM32duino
description: Exercice avec la mémoire Flash en STM32duino
---

# Programmer la mémoire Flash d'un microcontrôleur avec STM32duino

Ce tutoriel montre comment sauvegarder (lire et écrire) des données dans [la mémoire non volatile](https://fr.wikipedia.org/wiki/M%C3%A9moire_non_volatile) d'un microcontrôleur avec Arduino, en particulier [la mémoire flash](https://fr.wikipedia.org/wiki/M%C3%A9moire_flash) intégrée à un microcontrôleur STM32L476RG équipant [une carte de prototypage NUCLEO-L476RG](../../Kit/nucleo_l476rg).

Comme déjà indiqué, la mémoire flash a comme caractéristique d'être **non volatile**, c'est à dire qu'elle peut être utilisée pour enregistrer dans le microcontrôleur des informations que l'on souhaite conserver lorsque son alimentation électrique est arrêtée.

Notre présent exemple montre comment placer dans la mémoire flash du STM32L476RG une petite base de données avec six enregistrements ; une liste d'utilisateurs inspirée de [cet exemple](x-nucleo-nfc05a1). Une fois la base écrite en mémoire flash, on pourra modifier sélectivement ses enregistrements et vérifier que ces modifications ne sont pas "oubliées" après un reset matériel de la carte. Un autre exemple d'utilisation de la sauvegarde en mémoire flash, plus complexe et plus utile, est disponible [ici](../projets/sketch10).

 **Attention**, la mémoire flash d'un microcontrôleur n'est pas faite pour être modifiée à grande fréquence, évitez à tout prix les algorithmes qui écrivent dedans "en boucle" afin de la préserver !


## Matériel requis

Une [carte NUCLEO-L476RG de STMicroelectronics](../projets/nucleo_l476rg.md).  Cet exemple devrait fonctionner sur n'importe quelle autre carte programmable à microcontrôleur supportée par Arduino, car nous n'utilisions ici que des fonctions de l'API Arduino.

## Bibliothèque(s) requise(s)

Aucune bibliothèque "importée" n'est nécessaire pour cet exemple, le sketch qui suit est suffisant.
La bibliothèque "EEPROM.h" est en effet automatiquement installée avec les définitions de cartes STM32.

## Le sketch Arduino

>> Le sketch pour cet exemple (et tous les autres) peut être téléchargé [**en cliquant ici**](../../../assets/Sketch/TUTOS.zip).

Lancez l'IDE Arduino, ouvrez un nouveau sketch vide et copiez-y le code qui suit :

```c
/*
  Objet du sketch : Apprendre à lire et écrire des données dans la mémoire 
  non volatile (la mémoire flash) d'un microcontrôleur, notamment de la
  famille STM32.
*/

/*----------------------------------------------------------------------------*/
/* Variables globales, includes, defines                                      */
/*----------------------------------------------------------------------------*/

/* Déclarations diverses */

// Débit du port série du ST-LINK
#define STLK_BDRATE (115200)

// Bibliothèque pour gérer la mémoire flash
#include <EEPROM.h>

#define FIELDS_MAX_CHAR (16)  // Nombre maximum de caractères autorisés

// Structure de type user_record pour les enregistrements
typedef struct {
  char uid[FIELDS_MAX_CHAR];        // Identifiant unique de l'enregistrement
  char lastname[FIELDS_MAX_CHAR];   // Nom
  char firstname[FIELDS_MAX_CHAR];  // Prénom
  uint8_t allowed;                  // Autorisé ou pas ?
} user_record;

// Nombre d'utilisateurs enregistrés dans la base de données
#define NBUSERS (6)

// Est-ce qu'on souhaite enregistrer une nouvelle liste
// d'utilisateurs dans la mémoire flash ?
#define RECORD_NEW_USERS_LIST (0)

// Si une base de données d'utilisateurs est déjà présente en mémoire flash
#if RECORD_NEW_USERS_LIST == 0
// Tableau pour manipuler la liste des utilisateurs en RAM
user_record user_db[NBUSERS];

// Si aucune base de données d'utilisateurs n'est présente en mémoire flash
#else
// Contenu de la base de donnée d'utilisateurs à enregistrer en mémoire flash
// Attention, conformément à la défintion du type user_record, les champs
// UID, nom et prénom ne doivent pas comporter plus de FIELDS_MAX_CHAR
// caractères.
user_record user_db[NBUSERS] = {
  // UID, Nom, Prénom, Niveau autorisation
  { "04262CB22E7380", "DUPONT", "Jean-Kevin", 1 },
  { "CAD17924", "HENRI", "Jean", 1 },
  { "6AE6CF25", "CUST", "Alex-Bonaparte", 1 },
  { "CA076E25", "SANSEL", "Nadine", 1 },
  { "5A056C25", "DE-QUATRE", "Lanfeust", 1 },
  { "AA496B25", "VALJEAN", "Jean", 1 }
};
#endif

/*----------------------------------------------------------------------------*/
/* Démarrage, configuration des périphériques                                 */
/*----------------------------------------------------------------------------*/
void setup() {

  // Démarrage du port série du ST-LINK à STLK_BDRATE bauds
  Serial.begin(STLK_BDRATE);

// Si aucune base de données d'utilisateurs n'est présente en mémoire flash
#if RECORD_NEW_USERS_LIST == 1
  // Enregistre en flash la base de données
  record_user_list_in_EEPROM();
// Si une base de données est déjà présente en mémoire flash
#else
  // Modifie le champ "allowed" du deuxième utilisateur
  modify_record_right(2);
#endif

  // Affiche dans la console du port série le contenu de la base de données
  read_user_list_from_EEPROM();
}

/*----------------------------------------------------------------------------*/
/* Boucle principale                                                          */
/*----------------------------------------------------------------------------*/
void loop() {
  // Rien ici
}

#if RECORD_NEW_USERS_LIST == 1
/*----------------------------------------------------------------------------*/
/* Enregistre la base de données (une liste de struct de type record_user)    */
/* dans la mémoire flash.                                                     */
/*----------------------------------------------------------------------------*/
void record_user_list_in_EEPROM(void) {

  Serial.println("Ecriture de la base de données en mémoire flash ...");

  user_record user;              // Un enregistrement est une structure de type user_record
  uint32_t step = sizeof(user);  // Deux enregistrement sont séparés de step octets en flash
  uint32_t add = 0;              // On commence par lire le premier enregistrement (numéro 0)

  for (uint8_t i = 0; i < NBUSERS; i++) {  // Pour chaque enregistrement ...
    user = (user_record)user_db[i];        // Charge son contenu dans user
    EEPROM.put(add, user);                 // Ecris-le en mémoire flash (non volatile)
    add += step;                           // Passe à l'enregistrement suivant
  }

  Serial.println("Fait !\n");
}
#endif

#if RECORD_NEW_USERS_LIST == 0
/*----------------------------------------------------------------------------*/
/* Modifie (inverse) le champ "allowed" de l'enregistrement record_index dans */
/* la mémoire flash.                                                          */
/*----------------------------------------------------------------------------*/
void modify_record_right(uint8_t record_index) {

  // Si l'index de l'enregistrement à modifier ne sort pas de la liste
  if (record_index < NBUSERS) {

    Serial.println("Modification de l'enregistrement n° " + String(record_index) + " en mémoire flash ...");

    user_record user;                    // Un enregistrement est une structure de type user_record
    uint32_t step = sizeof(user);        // Deux enregistrement sont séparés de step octets en flash
    uint32_t add = record_index * step;  // Position de l'enregistrement visé en flash

    EEPROM.get(add, user);         // Charge cet enregistrement dans user
    user.allowed = !user.allowed;  // Inverse la valeur de son champ "allowed"
    EEPROM.put(add, user);         // Réécrit cet enregistrement modifié en flash

    Serial.println("Fait !\n");

  } else {
    Serial.println("Index hors de la base");
  }
}
#endif

/*----------------------------------------------------------------------------*/
/* Recopie le contenu de la base de données depuis la mémoire flash dans un   */
/* tableau de structures user_db, de type user_record, en RAM                 */
/* Affiche dans le terminal série le contenu du tableau de structures user_db */
/*----------------------------------------------------------------------------*/
void read_user_list_from_EEPROM() {

  Serial.println("Lecture de la base de données depuis la mémoire flash ...");

  user_record user;              // Un enregistrement est une structure de type user_record
  uint32_t step = sizeof(user);  // Deux enregistrement sont séparés de step octets en flash
  uint32_t add = 0;              // On commence par lire le premier enregistrement (numéro 0)


  for (uint8_t i = 0; i < NBUSERS; i++) {  // Pour chaque enregistrement ...
    EEPROM.get(add, user);                 // Charge son contenu dans user
    user_db[i] = user;                     // Recopie le contenu de user à l'indice correspondant de user_db
    add += step;                           // Passe à l'enregistrement suivant
  }

  Serial.println("Fait !\n");

  Serial.println("Contenu de la base de données : ");

  for (uint8_t i = 0; i < NBUSERS; i++) {  // Pour chaque élément de user_db ...

    // Affiche ses différents champs
     Serial.print(i);
    Serial.print(", UID : ");
    Serial.print(user_db[i].uid);
    Serial.print(", Prénom : ");
    Serial.print(user_db[i].firstname);
    Serial.print(", Nom : ");
    Serial.print(user_db[i].lastname);

    Serial.print(", Droits : ");
    if (user_db[i].allowed) {
      Serial.print("Autorisé");
    } else {
      Serial.print("Interdit");
    }
    Serial.print("\n");
  }

  Serial.print("Nombre d'enregistrements : ");
  Serial.println(NBUSERS);
}
```

## Mise en œuvre du sketch, étape 1 : Enregistrer la base de données en flash

Dans un premier temps, nous devons enregistrer notre base de données d'utilisateurs dans la mémoire flash du microcontrôleur. Pour cela, nous allons ajuster la [**directive de préprocesseur**](https://fr.wikipedia.org/wiki/Pr%C3%A9processeur_C) de la ligne 66 comme suit : *#define RECORD_NEW_USERS_LIST (1)*. Ceci fait, le compilateur va ignorer dans le sketch toutes les lignes de code qui ne vérifient pas la condition *#if RECORD_NEW_USERS_LIST == 1*. 

Après avoir vérifié que l'IDE Arduino est correctement configurée, notamment qu'elle est connectée au port COM attribué au ST-LINK de votre carte (sous Windows), cliquez sur "Téléverser".
Si tout s'est passé correctement vous devriez observer ceci sur le terminal série de l'IDE Arduino :

```console
15:36:31.967 -> Ecriture de la base de données en mémoire flash ...
15:36:32.040 -> Fait !
15:36:32.040 -> 
15:36:32.040 -> Lecture de la base de données depuis la mémoire flash ...
15:36:32.072 -> Fait !
15:36:32.072 -> 
15:36:32.072 -> Contenu de la base de données : 
15:36:32.072 -> 0, UID : 04262CB22E7380, Prénom : Jean-Kevin, Nom : DUPONT, Droits : Autorisé
15:36:32.072 -> 1, UID : CAD17924, Prénom : Jean, Nom : HENRI, Droits : Autorisé
15:36:32.103 -> 2, UID : 6AE6CF25, Prénom : Alex-Bonaparte, Nom : CUST, Droits : Autorisé
15:36:32.103 -> 3, UID : CA076E25, Prénom : Nadine, Nom : SANSEL, Droits : Autorisé
15:36:32.103 -> 4, UID : 5A056C25, Prénom : Lanfeust, Nom : DE-QUATRE, Droits : Autorisé
15:36:32.103 -> 5, UID : AA496B25, Prénom : Jean, Nom : VALJEAN, Droits : Autorisé
15:36:32.103 -> Nombre d'enregistrements : 6
```

Et voilà, votre base de données est désormais mémorisée dans la mémoire flash du microcontrôleur, c'est ce que nous allons confirmer à l'étape suivante.

## Mise en œuvre du sketch, étape 2 : Modifier des enregistrements de la base de données 

Débranchez et rebranchez votre carte NUCLEO afin de vous assurer qu'elle a bien été réinitialisée (reset matériel).
A présent, nous allons extraire de la mémoire flash du STM32L476RG les données que nous y avons enregistrées, les copier en mémoire vive (RAM) et les afficher sur le port série du ST-Link.<br>

Pour cela, nous allons ajuster la [**directive de préprocesseur**](https://fr.wikipedia.org/wiki/Pr%C3%A9processeur_C)  de la ligne 66 comme suit : *#define RECORD_NEW_USERS_LIST (0)*. Ceci fait, le compilateur va ignorer dans le sketch toutes les lignes de code qui ne vérifient pas la condition *#if RECORD_NEW_USERS_LIST == 0*. 

Après avoir vérifié que l'IDE Arduino est correctement configurée, notamment qu'elle est connectée au port COM attribué au ST-LINK de votre carte (sous Windows), cliquez sur "Téléverser".
Si tout s'est passé correctement vous devriez observer ceci sur le terminal série de l'IDE Arduino :

```console
15:38:17.752 -> Modification de l'enregistrement n° 2 en mémoire flash ...
15:38:17.788 -> Fait !
15:38:17.788 -> 
15:38:17.788 -> Lecture de la base de données depuis la mémoire flash ...
15:38:17.860 -> Fait !
15:38:17.860 -> 
15:38:17.860 -> Contenu de la base de données : 
15:38:17.903 -> 0, UID : 04262CB22E7380, Prénom : Jean-Kevin, Nom : DUPONT, Droits : Autorisé
15:38:17.903 -> 1, UID : CAD17924, Prénom : Jean, Nom : HENRI, Droits : Autorisé
15:38:17.903 -> 2, UID : 6AE6CF25, Prénom : Alex-Bonaparte, Nom : CUST, Droits : Interdit
15:38:17.903 -> 3, UID : CA076E25, Prénom : Nadine, Nom : SANSEL, Droits : Autorisé
15:38:17.903 -> 4, UID : 5A056C25, Prénom : Lanfeust, Nom : DE-QUATRE, Droits : Autorisé
15:38:17.903 -> 5, UID : AA496B25, Prénom : Jean, Nom : VALJEAN, Droits : Autorisé
15:38:17.903 -> Nombre d'enregistrements : 6
```

On constate qu'effectivement, **le champ "Droits" de l'enregistrement numéro 2 est passé de "Autorisé" à "Interdit".**.

Si on effectue un reset de la carte, alors on obtient ceci :

```console
15:38:17.752 -> Modification de l'enregistrement n° 2 en mémoire flash ...
15:38:17.788 -> Fait !
15:38:17.788 -> 
15:38:17.788 -> Lecture de la base de données depuis la mémoire flash ...
15:38:17.860 -> Fait !
15:38:17.860 -> 
15:38:17.860 -> Contenu de la base de données : 
15:38:17.903 -> 0, UID : 04262CB22E7380, Prénom : Jean-Kevin, Nom : DUPONT, Droits : Autorisé
15:38:17.903 -> 1, UID : CAD17924, Prénom : Jean, Nom : HENRI, Droits : Autorisé
15:38:17.903 -> 2, UID : 6AE6CF25, Prénom : Alex-Bonaparte, Nom : CUST, Droits : Autorisé
15:38:17.903 -> 3, UID : CA076E25, Prénom : Nadine, Nom : SANSEL, Droits : Autorisé
15:38:17.903 -> 4, UID : 5A056C25, Prénom : Lanfeust, Nom : DE-QUATRE, Droits : Autorisé
15:38:17.903 -> 5, UID : AA496B25, Prénom : Jean, Nom : VALJEAN, Droits : Autorisé
15:38:17.903 -> Nombre d'enregistrements : 6
```

Le champ "Droits" de l'enregistrement numéro 2 est de nouveau "Autorisé", ce qui prouve que notre modification de sa valeur en mémoire flash fonctionne comme prévu, sa valeur était encore "Autorisé" juste après le reset, avant que l'on ne vienne l'inverser par la fonction *modify_record_right(...)*.

