---
title: BLE - Diffusion ("broadcast") d'une mesure de température
description: Utilisation du protocole BLE GAP pour émission et lecture de messages hors-connexion avec plusieurs cartes NUCLEO
---

# BLE - Diffusion ("broadcast") d'une mesure de température

Ce tutoriel montre comment configurer plusieurs cartes NUCLEO en scanners et en advertisers de sorte à échanger des informations unilatéralement, des advertisers vers les scanners, sans connexions. Il s'agit d'une application immédiate [du protocole BLE GAP, expliqué sur cette page](../../Embedded/ble) : 
 - Les advertisers émettent des trames qui contiennent des informations, en l'occurrence des données de température obtenues avec une thermistance ;
 - Les scanners sélectionnent ces trames, et affichent les informations qu'elles contiennent.

La structure des trames d'advertising émises est illustrée par la figure suivante :

<br>
<div align="left">
<img alt="BLE GAP advertisement" src="images/trame_GAP.png" width="400px">
</div>
<br>

 - Une première partie contient un identifiant de l'émetteur de la trame
 - Un champ *adv_type* partage avec tous les objets scanners les informations sur les services et caractéristiques accessibles en cas de connexion avec un central.
 - Un champ *rssi* (pour "Received Signal Strength Indication") qui donne l'atténuation de la trame à sa réception sur le scanner. La signification de la mesure, exprimée dans une échelle logarithmique (souvent en dBm) est la suivante : une valeur de 0 dBm correspond à une puissance reçue de 1 mW, −30 dBm correspond à 1 µW. Cela permet de connaitre la qualité de la réception et éventuellement d'ajuster, par rétroaction, le niveau d'émission de l'émetteur distant.
 - Un champ *adv_data* qui contient les données utilisateur. 

Le schéma suivant résume le principe du protocole GAP mis en œuvre :

<br>
<div align="left">
<img alt="BLE GAP use case" src="images/advertiser_scanner.jpg" width="600px">
</div>
<br>

Un advertiser diffuse des trames qui seront reçues et décodées par un scanner. Notre exemple ne montre qu'un seul scanner et un seul advertiser, mais rien n'interdit d'avoir plusieurs exemplaires de chaque ; tous les scanners actifs capturerons les messages de tous les advertisers actifs. 

# Première partie : Création de l'advertiser

Commençons par détailler comment programmer une carte à microcontrôleur de sorte qu'elle se comporte en advertiser.

## Matériel requis

Nos sketchs proposent plusieurs options pour le matériel permettant de communiquer en BLE, qui pourra être constitué au choix :

- D'une carte [NUCLEO-WB55](../../Kit/nucleo_wb55rg) ou bien
- D'une carte [NUCLEO-L476RG](../../Kit/nucleo_l476rg) équipée soit d'un [shield X-NUCLEO-IDB05A1](https://www.st.com/en/ecosystems/x-nucleo-idb05a1.html) soit d'un [shield X-NUCLEO-IDB05A2](https://www.st.com/en/ecosystems/x-nucleo-idb05a2.html) ou bien
- D'une carte [DISCOVERY B-L475E-IOT01A1](https://www.st.com/en/evaluation-tools/b-l475e-iot01a.html) ou bien
- D'une carte [DISCOVERY B-L4S5I-IOT01A](https://www.st.com/en/evaluation-tools/b-l4s5i-iot01a.html).

Pour indiquer au compilateur quel matériel vous utilisez effectivement, prenez soin **d'attribuer la bonne valeur à la directive de préprocesseur *#define HW_CONFIG*** dans le sketch.

>> **Attention**, selon le matériel BLE que vous utilisez, [des mises à jour de firmwares peuvent être nécessaires](https://github.com/stm32duino/STM32duinoBLE) ! Pour la NUCLEO-WB55, la procédure est expliquée [ici](../../tools/cubeprog/cube_prog_firmware_ble_hci).

- Une [carte d'extension de base Grove](https://wiki.seeedstudio.com/Base_Shield_V2/) **avec son commutateur positionné sur 3.3V**. 
- Un [module Grove - Temperature Sensor v1.2](https://wiki.seeedstudio.com/Grove-Temperature_Sensor_V1.2/). Ce module fait l'objet d'un tutoriel dédié disponible [ici](thermistance). Il doit être connecté sur la prise *A2* de la carte d'extension de base Grove.

**Attention**, selon le matériel BLE que vous utilisez, [des mises à jour de firmwares peuvent être nécessaires](https://github.com/stm32duino/STM32duinoBLE) ! Pour la NUCLEO-WB55, la procédure est expliquée [ici](../../tools/cubeprog/cube_prog_firmware_ble_hci.md).

## Bibliothèque(s) requise(s)

La gestion du BLE par STM32duino est assurée par **la bibliothèque STM32duinoBLE** disponible [ici](https://github.com/stm32duino/STM32duinoBLE).

## Le sketch Arduino pour les advertisers

>> Le sketch pour cet exemple (et tous les autres) peut être téléchargé [**en cliquant ici**](../../../assets/Sketch/TUTOS.zip).

Ce sketch doit être chargé dans la carte qui jouera le rôle d'advertiser. **N'oubliez pas de connecter dessus le module Grove capteur de température** comme expliqué dans la section "Matériel requis". Ce module fait l'objet d'un tutoriel dédié disponible [ici](thermistance).

Lancez l'IDE Arduino, ouvrez un nouveau sketch vide et copiez-y le code qui suit :

```c
/* 
  Objet du sketch :

  Mesure la température à partir d'une thermistance d'un module Grove Temperature sensor V1.2.
  Publie ensuite cette température en mode advertising (broadcast) en BLE.

  La thermistance est ici connectée sur la broche Arduino A2.
  L'ADC interne fonctionnant sur 3.3V, le capteur doit être alimenté en 3.3V sur la carte d'adaptation Grove.

  Trois configurations sont possibles, selon la valeur assignée à la directive de préprocesseur HW_CONFIG :
  - Si HW_CONFIG = 1 : Le script sera compilé pour une carte DISCOVERY B-L475E-IOT01A1 ou B-L4S5I-IOT01A
  - Si HW_CONFIG = 2 : Le script sera compilé pour une carte NUCLEO-WB55
  - Si HW_CONFIG = 3 : Le script sera compilé pour une carte NUCLEO-L476RG et un shield IDB05A1 ou IDB05A2
  Le central et le périphérique ne sont pas tenus d'avoir la même configuration matérielle.

  Utilisation :
  Compilez le script, chargez le dans votre carte et observez les messages sur le terminal série de l'IDE.
  Utilisez un objet BLE scanner pour récupérer les trames d'advertising et lire la température à distance.
*/

/* Définitions pour le BLE */

// Bibliothèque disponible ici : https://github.com/stm32duino/STM32duinoBLE
#include <STM32duinoBLE.h>

// Instanciation du BLE

// Sélectionne la configuration matérielle parmi celles disponibles
#define HW_CONFIG 3

#if HW_CONFIG == 1
/* Carte B-L475E-IOT01A1 ou B_L4S5I_IOT01A */
SPIClass SpiHCI(PC12, PC11, PC10);
HCISpiTransportClass HCISpiTransport(SpiHCI, SPBTLE_RF, PD13, PE6, PA8, 8000000, SPI_MODE0);
BLELocalDevice BLEObj(&HCISpiTransport);
BLELocalDevice &BLE = BLEObj;
#elif HW_CONFIG == 2
/* Carte NUCLEO WB55 */
HCISharedMemTransportClass HCISharedMemTransport;
BLELocalDevice BLEObj(&HCISharedMemTransport);
BLELocalDevice &BLE = BLEObj;
#elif HW_CONFIG == 3
/* Carte NUCLEO L476RG et shield IDB05A1/A2 avec la broche SPI clock sur D3 */
// Attention, les lignes ci-dessous informent que les broches Arduino :
// A0, A1, D3, D7, D11 et D12 sont utilisées par le shield IDB05A1 et donc
// ne sont plus disponibles pour y connecter autre chose !
SPIClass SpiHCI(D11, D12, D3);
HCISpiTransportClass HCISpiTransport(SpiHCI, SPBTLE_RF, A1, A0, D7, 8000000, SPI_MODE0);
BLELocalDevice BLEObj(&HCISpiTransport);
BLELocalDevice &BLE = BLEObj;
#endif

// Nom de l'objet
#define DEVICE_ID "Temperature"

// Déclaration du service environnemental et d'une caractéristique température
// UUID disponibles ici : https://www.bluetooth.com/specifications/assigned-numbers/
#define SERVICE_ID "181A"  // Service environnemental
BLEService envService(SERVICE_ID);
#define CHAR_ID "2A1C" // La caractéristique expose une température
#define TEMP_UUID 0x2A1C // La caractéristique expose une température

// La caractéristique pourra être lue et sera publiée
BLEIntCharacteristic tempChar(CHAR_ID, BLERead | BLEBroadcast);

// Paramètres d'advertisement ; ils doivent être déclarés comme variables globales
#define NB_MANUF (4)  // Nombre d'octets dans les identifiants manufacturer
uint8_t manufactData[NB_MANUF] = { 0x01, 0x02, 0x03, 0x04 };
#define NB_DATA (4)  // Nombre d'octets de données (32 bits pour la température)
uint8_t serviceData[NB_DATA] = { 0x00, 0x00, 0x00, 0x00 };

// Déclaration de la zone de données (payload) de la trame d'advertising
BLEAdvertisingData advData;

/* Définitions pour la thermistance */

// Broche analogique pour l'ADC
// Attention, A0 et A1 sont potentiellement déjà utilisées par le shield IB05A1 !
#define TEMP_PIN A2

// Paramètres de l'ADC
#define VREF (3.3)    // Tension de référence de l'ADC (en volts)
#define ADC_RES (12)  // Résolution de l'ADC (en nombre de bits)

// Calcul du pas, ou quantum, de l'ADC
const float scale = pow(2, (float)ADC_RES);
const float ADC_QUANT = VREF / scale;

// Définition des constantes nécessaires pour la conversion.
const float R1 = 10000;

// Coefficients de Steinhart-Hart pour la conversion
const float c1 = 0.001129148, c2 = 0.000234125, c3 = 0.0000000876741;

/* Autres définitions */

// Débit du port série du ST-LINK
#define ST_LINK_BAUDRATE (115200)

// On gère le polling BLE et la publicaton de température avec un timer
// Instanciation de gestion de l'interruption de dépassement du timer 1
TIM_TypeDef *Instance1 = TIM1;
HardwareTimer *Timer1 = new HardwareTimer(Instance1);
// Fréquence de polling du BLE assurée par le timer 1
#define BLE_POLLING_HZ (15)

// Nombre d'interruptions de dépassement du timer 1 avant une publication
// de la température
#define TEMP_ADVERTISING_FREQ (100)
// Est-ce qu'une nouvelle mesure de température doit être publiée ?
volatile uint8_t update_temperature = 0;


/****************************************************************/
/* Initialisations                                              */
/****************************************************************/
void setup() {

  // Initialisation du port série du ST-LINK
  Serial.begin(ST_LINK_BAUDRATE);
  while (!Serial) {};

  Serial.println("ADVERTISER BLE");
  Serial.println("Mon identifiant est " + String(DEVICE_ID));

  // Active l'entrée analogique pour la thermistance
  pinMode(TEMP_PIN, INPUT);

  // Fixe la résolution de l'ADC à ADC_RES
  analogReadResolution(ADC_RES);

  // Initialisation du BLE
  if (!BLE.begin()) {
    Serial.println("Echec de l'initialisation du BLE !");
    while (!Serial) {};
  }

  // Construit le service et la caractéristique
  envService.addCharacteristic(tempChar);
  BLE.addService(envService);

  // Construit le paquet de réponse aux évènements de scan
  BLEAdvertisingData scanData;
  // Nom de l'objet
  scanData.setLocalName(DEVICE_ID);
  // Copie les paramètres précisés dans le paquet
  BLE.setScanResponseData(scanData);

  // Construit le paquet de données d'advertising
  advData.setManufacturerData(0x004C, manufactData, sizeof(manufactData));
  advData.setAdvertisedService(envService);
  advData.setAdvertisedServiceData(TEMP_UUID, serviceData, sizeof(serviceData));

  // Copie les paramètres précisés dans le paquet
  BLE.setAdvertisingData(advData);

  // Lance l'advertising
  BLE.advertise();
  Serial.println("Publication démarrée ...");

  // Initialisation de l'interruption périodique du timer 1
  // - Pour la gestion des évènements BLE avec une interruption périodique
  // - Pour la gestion de la publication avec une interruption périodique
   init_Timer_BLE();

}

/****************************************************************/
/* Boucle principale                                            */
/****************************************************************/
void loop() {

  // Si une mesure de température doit être réalisée
  if (update_temperature > TEMP_ADVERTISING_FREQ) {

    // Mesure et publie la nouvelle température
    read_advertise_temp();

    // Réarme pour une prochaine mesure
    update_temperature = 0;
  }
}

/****************************************************************/
/* Mesure et publication d'une nouvelle température             */
/****************************************************************/
void read_advertise_temp(void) {

  // Acquisition analogique, calcul de la température
  float temp = get_temp(TEMP_PIN);

  Serial.print("Température publiée : ");
  Serial.print(temp, 1);
  Serial.println("°C");

  // Suspend l'advertising
  BLE.stopAdvertise();

  // Copie la valeur de la température dans la table serviceData
  // L'opération réciproque (cast d'un tableau de 4 octets en float) est
  // float x = *((float *)(serviceData));

  *((float *)serviceData) = temp;

  // Met à jour le paquet d'advertising
  advData.setAdvertisedServiceData(TEMP_UUID, serviceData, sizeof(serviceData));
  BLE.setAdvertisingData(advData);

  // Relance l'advertising
  BLE.advertise();
}

/****************************************************************/
/* Démarrage de l'interruption périodique du timer 1            */
/* Polling BLE et mesure puis publication de la température     */
/****************************************************************/
void init_Timer_BLE(void) {
  Timer1->setOverflow(BLE_POLLING_HZ, HERTZ_FORMAT);
  Timer1->attachInterrupt(Timer1_ISR);
  Timer1->resume();
  Serial.println("IT timer 1 active");
}

/* Fonction de service de l'interruption de dépassement du timer 1 */
void Timer1_ISR(void) {
  update_temperature++;
  BLE.poll();
}

/****************************************************************/
/* Réalise une mesure de température (°C) avec la termistance   */
/* connectée à la broche analogique sensorPin                   */
/****************************************************************/
float get_temp(uint8_t sensorPin) {

  // Echantillonnage de la tension de la thermistance par l'ADC
  uint16_t sample = analogRead(sensorPin);

  // Calcule la résistance du thermistor
  float R2 = R1 * (scale / (float)sample - 1.0);

  // Calcule la température en Kelvins à partir de la résistance
  float logR2 = log(R2);
  float Tk = (1.0 / (c1 + c2 * logR2 + c3 * logR2 * logR2 * logR2));

  // Convertit les Kelvins en degrés Celcius.
  float Tc = Tk - 273.15;

  // Arrondi à une décimale
  Tc = floor(10 * Tc + 0.5) / 10;

  return Tc;
}
```

## Mise en œuvre de l'advertiser

Après avoir vérifié que l'IDE Arduino est correctement configurée, notamment qu'elle est connectée au port COM attribué au ST-LINK de votre carte (sous Windows), cliquez sur "Téléverser".

Les logs **dans le terminal série de l'IDE Arduino** devraient ressembler à ceci :

```console
ADVERTISER BLE
Mon identifiant est Temperature
Publication démarrée ...
IT timer 1 active
Température publiée : 24.8°C
Température publiée : 24.8°C
Température publiée : 24.8°C
Température publiée : 24.7°C
Température publiée : 24.7°C
Température publiée : 24.7°C
Température publiée : 24.7°C
Température publiée : 24.7°C
```

La publication de données est active, mais aucun scanner ne les observe à ce stade.

Vous pouvez créer facilement un scanner en chargeant sur votre smartphone l'application [nRF Connect for Mobile (versions Android)](https://play.google.com/store/apps/details?id=no.nordicsemi.android.mcp&hl=fr&gl=US&pli=1) ou  [nRF Connect for Mobile (versions Apple)](https://apps.apple.com/ch/app/nrf-connect-for-mobile/id1054362403?l=fr) de [Nordic Semiconductor ASA](https://infocenter.nordicsemi.com/index.jsp).

Avec NRF connect, vous pourrez observer le contenu de la trame d'advertising comme ci-desous :
<br>
<div align="left">
<img alt="BLE GAP NRF connect" src="images/advertiser_scanner_nrf_connect.jpg" width="600px">
</div>
<br>

 On identifie, **en hexadécimal**, le tableau *manufactData* (ici fortuitement décodé comme *"Company: Apple, Inc"*) et la température du moment contenue dans le tableau *serviceData* (*Data: 0x9A99C940*).


# Deuxième partie : Création du scanner

Plutôt que l'application smartphone nRF Connect for Mobile, **vous pouvez utiliser une autre carte à microcontrôleur comme central**. C'est ce que nous allons réaliser maintenant.

## Matériel requis

Nos sketchs proposent plusieurs options pour le matériel permettant de communiquer en BLE, qui pourra être constitué au choix :

- D'une carte [NUCLEO-WB55](../../Kit/nucleo_wb55rg) ou bien
- D'une carte [NUCLEO-L476RG](../../Kit/nucleo_l476rg) équipée soit d'un [shield X-NUCLEO-IDB05A1](https://www.st.com/en/ecosystems/x-nucleo-idb05a1.html) soit d'un [shield X-NUCLEO-IDB05A2](https://www.st.com/en/ecosystems/x-nucleo-idb05a2.html) ou bien
- D'une carte [DISCOVERY B-L475E-IOT01A1](https://www.st.com/en/evaluation-tools/b-l475e-iot01a.html) ou bien
- D'une carte [DISCOVERY B-L4S5I-IOT01A](https://www.st.com/en/evaluation-tools/b-l4s5i-iot01a.html).

Pour indiquer au compilateur quel matériel vous utilisez effectivement, prenez soin **d'attribuer la bonne valeur à la directive de préprocesseur *#define HW_CONFIG*** dans le sketch.

**Attention**, selon le matériel BLE que vous utilisez, [des mises à jour de firmwares peuvent être nécessaires](https://github.com/stm32duino/STM32duinoBLE) ! Pour la NUCLEO-WB55, la procédure est expliquée [ici](../../tools/cubeprog/cube_prog_firmware_ble_hci).

Précisons *qu'il n'est pas obligatoire que le scanner et l'advertiser soient matériellement identiques*. Par exemple, on peut très bien construire et faire communiquer ensemble un scanner constitué d'une carte NUCLEO-WB55 avec un advertiser constitué d'une carte DISCOVERY B_L4S5I_IOT01A.

## Bibliothèque(s) requise(s)

La gestion du BLE par STM32duino est assurée par **la bibliothèque STM32duinoBLE** disponible [ici](https://github.com/stm32duino/STM32duinoBLE).

Pour que ce sketch fonctionne, il faut cependant que vous ajoutiez des lignes de codes dans deux fichiers source de la bibliothèque STM32duinoBLE, conformément à ce qui est expliqué ici :
> https://github.com/arduino-libraries/ArduinoBLE/issues/123.<br>

Commencez par rechercher dans votre ordinateur l'emplacement des fichiers  *BLEDevice.h* et *BLEDevice.cpp*. Sur un PC exécutant MS Windows, ils sont par exemple dans ce chemin :

 > C:\Users\XXXXX\Documents\Arduino\libraries\STM32duinoBLE\src, Où *XXXXX* désigne le nome de votre compte utilisateur.

 Dans le fichier *BLEDevice.h*, ajoutez :

```c
    int getAdvertisement(uint8_t value[], int length);
```

 Dans le fichier *BLEDevice.cpp*, ajoutez :

```c
  int BLEDevice::getAdvertisement(uint8_t value[], int length)
  {
    if (_eirDataLength > length) return 0;  // Check that buffer size is sufficient

    if (_eirDataLength) {
      memcpy(value, _eirData, _eirDataLength);
    }

    return _eirDataLength;
  }
```

## Le sketch Arduino pour les scanners

>> Le sketch pour cet exemple (et tous les autres) peut être téléchargé [**en cliquant ici**](../../../assets/Sketch/TUTOS.zip).

Lancez l'IDE Arduino, ouvrez un nouveau sketch vide et copiez-y le code qui suit :

```c
/* 
  Objet du sketch :
  Crée un scanner BLE capable de déchiffrer et d'afficher les mesures de température publiées
  par d'autres objets exécutant le sketch BLE_Temperature_Advertising.ino.

  Matériel utilisé :
  Trois configurations sont possibles, selon la valeur assignée à la directive de préprocesseur HW_CONFIG :
  - Si HW_CONFIG = 1 : Le script sera compilé pour une carte DISCOVERY B-L475E-IOT01A1 ou B-L4S5I-IOT01A
  - Si HW_CONFIG = 2 : Le script sera compilé pour une carte NUCLEO-WB55
  - Si HW_CONFIG = 3 : Le script sera compilé pour une carte NUCLEO-L476RG et un shield IDB05A1 ou IDB05A2
  Le central et le périphérique ne sont pas tenus d'avoir la même configuration matérielle.

  Utilisation :
  Compilez le script, chargez le dans votre carte et observez les messages sur le terminal série de l'IDE.
  Bien sûr, vous ne recevrez des valeurs de température que si au moins un autre objet exécute à proximité
  le sketch BLE_Temperature_Advertising.ino.

  IMPORTANT :
  Pour que ce sketch fonctionne, il faut que vous ajoutiez des lignes de codes dans deux fichiers source
  de la lib STM32duinoBLE, conformément à ce qui est expliqué ici :
  https://github.com/arduino-libraries/ArduinoBLE/issues/123.

 Dans BLEDevice.h, ajoutez :

    int getAdvertisement(uint8_t value[], int length);

 Dans BLEDevice.cpp, ajoutez :

  int BLEDevice::getAdvertisement(uint8_t value[], int length)
  {
    if (_eirDataLength > length) return 0;  // Check that buffer size is sufficient

    if (_eirDataLength) {
      memcpy(value, _eirData, _eirDataLength);
    }

    return _eirDataLength;
  }

*/

/* Définitions pour le BLE */

// Nom des advertisers écoutés par le scanner
#define ADVERTISER_NAME "Temperature"

// Bibliothèque disponible ici : https://github.com/stm32duino/STM32duinoBLE
#include <STM32duinoBLE.h>

// Instanciation du BLE

// Sélectionne la configuration matérielle parmi celles disponibles
#define HW_CONFIG 3

#if HW_CONFIG == 1
/* Carte B-L475E-IOT01A1 ou B_L4S5I_IOT01A */
SPIClass SpiHCI(PC12, PC11, PC10);
HCISpiTransportClass HCISpiTransport(SpiHCI, SPBTLE_RF, PD13, PE6, PA8, 8000000, SPI_MODE0);
BLELocalDevice BLEObj(&HCISpiTransport);
BLELocalDevice &BLE = BLEObj;
#elif HW_CONFIG == 2
/* Carte NUCLEO WB55 */
HCISharedMemTransportClass HCISharedMemTransport;
BLELocalDevice BLEObj(&HCISharedMemTransport);
BLELocalDevice &BLE = BLEObj;
#elif HW_CONFIG == 3
/* Carte NUCLEO L476RG et shield IDB05A1/A2 avec la broche SPI clock sur D3 */
// Attention, les lignes ci-dessous informent que les broches Arduino :
// A0, A1, D3, D7, D11 et D12 sont utilisées par le shield IDB05A1 et donc
// ne sont plus disponibles pour y connecter autre chose !
SPIClass SpiHCI(D11, D12, D3);
HCISpiTransportClass HCISpiTransport(SpiHCI, SPBTLE_RF, A1, A0, D7, 8000000, SPI_MODE0);
BLELocalDevice BLEObj(&HCISpiTransport);
BLELocalDevice &BLE = BLEObj;
#endif

// Nombre d'octets maximum constituant la trame d'advertising
#define NB_MAX_BYTES 64

// Tableau pour récupérer les données publiées par le capteur
uint8_t sensorAdvertisementData[NB_MAX_BYTES];

/* Autres définitions */

// Débit du port série du ST-LINK
#define ST_LINK_BAUDRATE (115200)

// On gère le polling BLE avec un timer
// Instanciation de gestion de l'interruption de dépassement du timer 1
TIM_TypeDef *Instance1 = TIM1;
HardwareTimer *Timer1 = new HardwareTimer(Instance1);

// Fréquence de polling du BLE assurée par le timer 1
#define BLE_POLLING_HZ (1)

/****************************************************************/
/* Initialisations                                              */
/****************************************************************/
void setup() {

  // Initialisation du port série du ST-LINK
  Serial.begin(ST_LINK_BAUDRATE);
  while (!Serial) {};

  Serial.println("SCANNER BLE");

  // Initialisation du BLE
  if (!BLE.begin()) {
    Serial.println("Echec de l'initialisation du BLE !");
    while (!Serial) {};
  }

  // Active la gestion de l'évènement de découverte d'un advertiser
  BLE.setEventHandler(BLEDiscovered, bleCentralDiscoverHandler);

  // Démarre le scan à la recherche de périphériques
  BLE.scan(true);

  // Initialisation de l'interruption périodique du timer 1
  init_Timer_BLE();
}

/****************************************************************/
/* Boucle principale                                            */
/****************************************************************/
void loop() {
  // Rien ici
}

/****************************************************************/
/* Gestion des évènements de découverte de périphériques        */
/****************************************************************/
void bleCentralDiscoverHandler(BLEDevice peripheral) {

  // Si l'advertiser détecté a un nom
  if (peripheral.hasLocalName()) {

    String advertiser_name = peripheral.localName();

    // Si l'advertiser détecté est celui que l'on veut écouter
    if (advertiser_name == ADVERTISER_NAME) {

      Serial.println("-----------------------");
      // Nom de l'advertiser
      Serial.print("Advertiser : ");
      Serial.println(advertiser_name);

      // Adresse MAC de l'advertiser
      Serial.print("Adresse MAC : ");
      Serial.println(peripheral.address());

      // Affiche les identifiants des services de l'advertiser
      if (peripheral.hasAdvertisedServiceUuid()) {
        Serial.print("Identifiant(s) de service(s) : ");
        for (int i = 0; i < peripheral.advertisedServiceUuidCount(); i++) {
          Serial.print(peripheral.advertisedServiceUuid(i));
          Serial.print(" ");
        }
        Serial.println();
      }

      // Affiche l'atténuation du signal
      Serial.print("RSSI: ");
      Serial.println(peripheral.rssi());

      // Récupère les informations de mesure du capteur
      get_advertisement_data(peripheral);

      // Serial.print("Trame publiée : ");
      // for (int i = 1; i < sensorAdvertisementData[0]; i++) {
      //   Serial.print(sensorAdvertisementData[i], HEX);
      // }

      // Copie des 4 octets qui encodent la température
      uint8_t serviceData[4];
      serviceData[0] = sensorAdvertisementData[3];
      serviceData[1] = sensorAdvertisementData[4];
      serviceData[2] = sensorAdvertisementData[5];
      serviceData[3] = sensorAdvertisementData[6];

      // Décode les octets utiles en température
      float temp = *((float *)(serviceData));

      Serial.print("Température publiée : ");
      Serial.print(temp, 1);
      Serial.println("°C");
      Serial.println();
    }
  }
}

/****************************************************************/
/* Extrait les données utiles de la trame d'advertising         */
/****************************************************************/
void get_advertisement_data(BLEDevice peripheral) {

  // Contiendra toute la trame d'advertising
  uint8_t advertisement[NB_MAX_BYTES] = { 0 };

  // Si on parvient à récupérer le contenu de la trame d'advertising
  if (peripheral.getAdvertisement(advertisement, NB_MAX_BYTES)) {

    // Efface le contenu du tableau qui va remonter les octets de données
    for (int i = 0; i < sizeof(sensorAdvertisementData); i++) {
      sensorAdvertisementData[i] = 0;
    }

    // Pour tous les octets de la trame
    for (int i = 0; i < NB_MAX_BYTES;) {

      int eirLength = advertisement[i++];
      int eirType = advertisement[i++];

      // Début des données
      if (eirType == 0x16) {

        // Longueur des données
        sensorAdvertisementData[0] = eirLength;

        // Les données
        for (int j = 0; j < (eirLength - 1); j++) {
          uint8_t thisByte = advertisement[i + j];
          sensorAdvertisementData[j + 1] = thisByte;
        }
        break;
      }
      i += (eirLength - 1);
    }
  }
}

/****************************************************************/
/* Démarrage de l'interruption périodique du timer 1            */
/* Polling BLE                                                  */
/****************************************************************/
void init_Timer_BLE(void) {
  Timer1->setOverflow(BLE_POLLING_HZ, HERTZ_FORMAT);
  Timer1->attachInterrupt(Timer1_ISR);
  Timer1->resume();
  Serial.println("IT timer 1 active");
}

/* Fonction de service de l'interruption de dépassement du timer 1 */
void Timer1_ISR(void) {
  // Exécution des ébènements BLE en attente
  BLE.poll();
}

```

## Mise en œuvre du scanner

Après avoir vérifié que l'IDE Arduino est correctement configurée, notamment qu'elle est connectée au port COM attribué au ST-LINK de votre carte (sous Windows), cliquez sur "Téléverser".

Branchez également sur une alimentation le système advertiser que vous avez réalisé dans la première partie. Les logs **dans le terminal série de l'IDE Arduino** devraient ressembler à ceci :

```console
SCANNER BLE
IT timer 1 active
-----------------------
Advertiser : Temperature
Adresse MAC : e4:85:c8:d4:a9:db
Identifiant(s) de service(s) : 181a 
RSSI: -56
Température publiée : 27.2°C

-----------------------
Advertiser : Temperature
Adresse MAC : e4:85:c8:d4:a9:db
Identifiant(s) de service(s) : 181a 
RSSI: -52
Température publiée : 27.3°C
```

Votre scanner capture bien les valeurs de températures publiées par votre advertiser !

## Deux améliorations possibles de cet exemple

On remarquera que ce sketch **filtre les messages d'un seul ensemble d'advertisers**, ceux qui ont comme *localName* la valeur *ADVERTISER_NAME* (soit "Température" dans ce tutoriel). Cette sélection est réalisée par la ligne *if (advertiser_name == ADVERTISER_NAME)* dans la fonction *bleCentralDiscoverHandler*. 
On peut bien sûr rendre ce comportement plus intelligent en filtrant plutôt sur une liste d'adresses MAC prédéfinie ou encore sur les premiers caractères de  *ADVERTISER_NAME*.

Les messages affichés par le scanner **sont souvent répliqués pour chaque advertiser**. Une solution pour éviter cette difficulté consisterait à leur ajouter un horodatage côté advertisers (ou plus simplement un numéro d'ordre périodique qui ne se répèterait pas avant quelques minutes) puis à charger les messages dans une file de réception côté scanner afin de les tier selon ce code à la réception, en ignorant les répliques.
