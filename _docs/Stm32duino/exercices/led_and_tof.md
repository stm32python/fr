---
title: Contrôle d'une LED avec un capteur de distance
description: Contrôle d'une LED avec un capteur de distance. Mise en œuvre des timers des STM32 pour générer des interruption de dépassement (périodiques) avec STM32duino
---

# Contrôle d'une LED avec un capteur de distance

Ce tutoriel explique comment contrôler une LED avec un capteur de distance. Ce système en soit n'a que peu d'intérêt ; ce n'est qu'un prétexte pour expliquer comment utiliser **les timers des microcontrôleurs STM32** afin de générer des interruptions périodiques permettant de cadencer avec précision les opérations d'un firmware.

Vous pouvez imaginer un timer comme un compteur intégré au microcontrôleur. Une fois paramétré et démarré, un timer va incrémenter son compteur interne CNT par pas de 1, depuis 0 jusqu'à une valeur maximum, par exemple CNT_max = 4095. Lorsque CNT = CNT_max le timer génère une *interruption* dite "*de dépassement de compteur*". A ce moment là, CNT revient à zéro et le timer recommence son cycle de décompte : CNT = 0, CNT = 1... Le schéma qui suit illustre le décompte d'un timer pour CNT_max=5 :

<br>
<img alt="Principe décompte timer" src="images/timer.jpg" width="800px">
<br>

On peut configurer un timer afin qu'il lance automatiquement **une fonction à chaque interruption de dépassement**, autrement abrégée **ISR pour "Interrupt Service Routine"**.

Notre application pour illustrer l'usage des timers en mode "interruption de dépassement" consiste à détecter un obstacle et à faire clignoter une LED lorsque celui-ci est très proche. On utilisera pour cela **deux** timers, configurés en mode interruption : un dont l'ISR va fixer la fréquence de mesure du capteur de distance et un autre dont l'ISR va fixer la fréquence de clignotement de la LED.

On notera que les timers intégrés aux microcontrôleurs STM32 sont des périphériques **complexes** aux **multiples fonctions** qui peuvent faire bien plus que générer des interruptions périodiques. Pour en tirer pleinement profit, il est conseillé de les programmer avec l'environnement de développement professionnel intégré mis en avant par STMicroelectronics, [**STM32CubeIDE**](https://www.st.com/content/st_com/en/stm32cubeide.html) et la **couche d’abstraction matérielle HAL**. Il est cependant possible d'accéder à leurs fonctions avancées grâce à [**l'API HardwareTimer**](https://github.com/stm32duino/Arduino_Core_STM32/wiki/HardwareTimer-library#hardwaretimer) de STM32duino, que nous utilisons d'ailleurs dans ce tutoriel.


## Matériel requis

1. Un [module Grove - Time of Flight Distance Sensor VL53L0X](https://wiki.seeedstudio.com/Grove-Time_of_Flight_Distance_Sensor-VL53L0X/).
2. Un [moduleGrove - LED Socket Kit](https://wiki.seeedstudio.com/Grove-LED_Socket_Kit/).
2. Une carte NUCLEO de STMicroelectronics supportée par STM32duino (toutes le sont !)
3. Selon la carte NUCLEO, un [Grove Base Shield V2](https://wiki.seeedstudio.com/Base_Shield_V2/) (pour les NUCLEO 64) ou bien deux [câbles Grove - Dupont femelle](https://fr.vittascience.com/shop/115/Lot-de-5-c%C3%A2bles-Grove---Dupont-femelle) (pour les NUCLEO 32)

Dans notre cas, pour changer, mais aussi pour réaliser un système plus compact, nous avons opté pour une carte au format NUCLEO 32, [**la NUCLEO-F303K8**](https://www.st.com/en/evaluation-tools/nucleo-f303k8.html) équipée d'un [**microcontrôleur STM32F303K8**](https://www.st.com/en/microcontrollers-microprocessors/stm32f303k8.html) :


|La NUCLEO-F303K8 et les fonctions assignées à ses broches|Diagramme blocs du STM32F303K8|
|:-:|:-:|
|<img alt="NUCLEO-F303K8" src="images/Pining_NUCLEO-F303K8.jpg" width="600px">| <img alt="STM32F303K8" src="images/STM32F303K8.jpg" width="360px">|

> Crédit images : [STMicroelectronics](Pining_NUCLEO-F303K8.jpg)

Nous utiliserons le rochage suivant, réalisé avec deux câbles Grove - Dupont femelle :

 - Pour le module Grove LED :
     * Fil de masse sur l'une des deux boches GND de la NUCLEO-F303K8
     * Fil d'alimentation sur la boche 5V de la NUCLEO-F303K8
     * Fil de signal sur la broche LED_PIN (D8 dans notre cas)
 - Pour le module Grove capteur de distance VL53L0X
     * Fil de masse sur l'une des deux boches GND de la NUCLEO-F303K8
     * Fil d'alimentation sur la boche 3V3 de la NUCLEO-F303K8
     * Fil SCL sur la broche D5 de la NUCLEO-F303K8
     * Fil SDA sur la broche D4 de la NUCLEO-F303K8

Bien sûr, si vous choisissez une autre carte, vous devrez prendre soin de modifier les broches en accord avec votre matériel pour la LED (broche numérique) et le capteur de distance (broches I2C).

## Bibliothèque(s) requise(s)

Une seule bibliothèque à télécharger, ```VL53L0X.h```, [ici](https://github.com/pololu/vl53l0x-arduino).

## Le sketch Arduino

> **Le sketch ci-dessous peut être [téléchargé par ce lien](SKETCHS.zip)**.

Lancez l'IDE Arduino, ouvrez un nouveau sketch vide et copiez-y le code qui suit :

```c
/*
  Exemple de contrôle d'une LED sur la base d'un capteur de distance.
  Matériel :
    - Une carte NUCLEO-F303K8
    - Un module Grove LED (verte)
    - Un module Grove capteur de distance VL53L0X
    - Deux câbles Grove - Dupont (femelle) pour connecter les module
  
  Brochage :
    - module Grove LED :
        * Fil de masse sur l'une des deux boches GND de la NUCLEO-F303K8
        * Fil d'alimentation sur la boche 5V de la NUCLEO-F303K8
        * Fil de signal sur la broche LED_PIN (D8 dans notre cas)
    -  module Grove capteur de distance VL53L0X
        * Fil de masse sur l'une des deux boches GND de la NUCLEO-F303K8
        * Fil d'alimentation sur la boche 3V3 de la NUCLEO-F303K8
        * Fil SCL sur la broche D5 de la NUCLEO-F303K8
        * Fil SDA sur la broche D4 de la NUCLEO-F303K8

  Fonctionnement :
    - Si le capteur mesure une distance supérieure à 10 cm, la LED reste éclairée
    - Dans le cas contraire, la LED clignote deux fois par seconde 
*/

// Débit du port série du ST-LINK
#define ST_LINK_BAUDRATE (115200)

// Broche qui commandera la LED
#define LED_PIN D8

/* Défintions pour le contrôleur I2C1 */
#include <Wire.h>  // Bibliothèque Arduino pour l'I2C

// Lignes inutiles ici car le contrôleur I2C1 (Wire) est déjà défini par le package
// STM32duino, mais cela vous montre comment déclarer un autre contrôleur I2C pour les 
// cartes qui en ont plusieurs.

// #define I2C1_SDA (PB7) // Soit D4 pour Arduino, broche SDA de l'I2C1
// #define I2C1_SCL (PB6) // Soit D5 pour Arduino, broche SCL de l'I2C1
// TwoWire Wire(I2C1_SDA, I2C1_SCL); // Instanciation du contrôleur de bus I2C1

/* Définitions pour le capteur de distance */

#include <VL53L0X.h>              // Lib utilisée : https://github.com/pololu/vl53l0x-arduino
VL53L0X sensor;                   // Instance du capteur
#define VL53L0X_TIMEOUT_MS (500)  // Time-out du capteur de distance (ms)
#define VL53L0X_PERIOD_MS (100)   // Période de mesure du capteur de distance (ms)
#define DISTANCE_THRES_MM (100)   // Seuil de distance au-dessous dusquel on prendra une action

#define MAX_UINT16_VAL (8191)           // La plus grande valeur possible pour un entier non signé codé sur 16 bits
uint16_t distance_mm = MAX_UINT16_VAL;  // Valeur mesurée de la distance, au démarrage MAX_UINT16_VAL

/* Définitions pour le timer 1, pour rythmer les mesures du capteur de distance */

// Instance du timer 1
TIM_TypeDef *Instance1 = TIM1;
HardwareTimer *Timer1 = new HardwareTimer(Instance1);

// Est-ce qu'on doit remonter une mesure de distance ?
// L'attribut "volatile" est indispensable du fait que cette variable sera
// modifiée par une routine de service d'interruption !
volatile uint8_t take_measurement = 0;

/* Définitions pour le timer 2, pour rythmer le clignotement de la LED */

// Instance du timer 2
TIM_TypeDef *Instance2 = TIM2;
HardwareTimer *Timer2 = new HardwareTimer(Instance2);

// Est-ce qu'on doit inverser l'état de la LED ?
// L'attribut "volatile" est indispensable du fait que cette variable sera
// modifiée par une routine de service d'interruption !
volatile uint8_t invert_LED = 0;

#define BLINK_FREQ_HZ (2)  // Fréquence de clignotement de la LED

/*******************************************************************/
/* Initialisations                                                 */
/*******************************************************************/
void setup() {

  pinMode(LED_PIN, OUTPUT);  // Initialisation de la LED

  Serial.begin(ST_LINK_BAUDRATE);  // Démarre le port série du ST-LINK

  Wire.begin();  // Démarre le contrôleur du bus I2C1

  sensor.setTimeout(VL53L0X_TIMEOUT_MS);  // Time-out du capteur de distance

  if (!sensor.init())  // Démarre le capteur
  {
    Serial.println("Echec lors de l'initialisation du capteur de distance !");
    while (1) {};
  }

  // Le capteur de distance réalise une mesure toutes les VL53L0X_PERIOD_MS ms
  sensor.startContinuous(VL53L0X_PERIOD_MS);

  Serial.println("Capteur de distance actif");

  // Calcul de la fréquence de mesure, la moitié de celle du module VL53L0X
  uint16_t measure_freq_hz = 1 / (2 * (float)(VL53L0X_PERIOD_MS)*0.001);

  // Initialisation du timer 1 et de son interruption de dépassement

  Timer1->setOverflow(measure_freq_hz, HERTZ_FORMAT);  // Fréquence de l'interruption
  Timer1->attachInterrupt(Timer1_ISR);                 // Routine de gestion de l'interruption
  Timer1->resume();                                    // On démarre le timer

  Serial.println("Timer 1 actif");
  Serial.print("Fréquence des mesures de distance : ");
  Serial.print(measure_freq_hz);
  Serial.println(" Hz");

  // Initialisation du timer 2 et de son interruption de dépassement

  Timer2->setOverflow(BLINK_FREQ_HZ, HERTZ_FORMAT);  // Fréquence de l'interruption
  Timer2->attachInterrupt(Timer2_ISR);               // Routine de gestion de l'interruption
  Timer2->resume();                                  // On démarre le timer

  Serial.println("Timer 2 actif");
  Serial.print("Fréquence de clignotement de la LED : ");
  Serial.print(BLINK_FREQ_HZ);
  Serial.println(" Hz");
}

/*******************************************************************/
/* Fonction / boucle principale                                    */
/*******************************************************************/
void loop() {

  // Si une mesure doit être réalisée ...
  if (take_measurement) {
    distance_mm = sensor.readRangeContinuousMillimeters();  // mesure
    Serial.print("Distance mesurée : ");
    Serial.print(distance_mm);
    Serial.println(" mm");
    take_measurement = 0;  // Signale que la mesure est terminée
  }

  // Si la distance mesurée est inférieure à DISTANCE_THRES_MM mm
  if (distance_mm < DISTANCE_THRES_MM) {
    // Si une inversion doit être réalisée ...
    if (invert_LED) {
      digitalWrite(LED_PIN, !digitalRead(LED_PIN));  // Inversion
      invert_LED = 0;                                // Signale que l'inversion est terminée
    }
  } else                          // Sinon
    digitalWrite(LED_PIN, HIGH);  // Allume la LED
}

/*******************************************************************/
/* Fonction de service de l'interruption de dépassement du timer 1 */
/*******************************************************************/
void Timer1_ISR(void) {
  // Signale qu'une mesure doit être réalisée
  take_measurement = 1;
}

/*******************************************************************/
/* Fonction de service de l'interruption de dépassement du timer 2 */
/*******************************************************************/
void Timer2_ISR(void) {
  // Signale que l'état de la LED doit être inversé
  invert_LED = 1;
}
```
Après avoir vérifié que l'IDE Arduino est correctement configurée, notamment qu'elle est connectée au port COM attribué au ST-LINK de votre carte (sous Windows), cliquez sur "Téléverser".

Si tout s'est passé correctement, vous devriez observer que la LED clignote deux fois par seconde lorsqu'un obstacle se situe à 10 cm ou moins du capteur de distance, et qu'elle reste allumée et fixe dans les autres situations.

### **Remarques importantes**

 - Ce sketch utilise deux timers, TIM1 et TIM2. Mais nous n'avons pas expliqué comment nous avons validé que ces deux timers étaient effectivement présents dans le STM32F303K8. La façon la plus simple de le faire consiste bien évidemment à consulter [**la fiche technique de ce microcontrôleur**](https://www.st.com/resource/en/datasheet/stm32f303k8.pdf) sur le site Internet de STMicroelectronics.

 - Vous constatez que les deux routines de service d'interruption des timers (ISR) ont des codes **très concis** qui consistent simplement à changer la valeur d'une variable qui est par ailleurs **déclarée avec l'attribut volatile**. Ce sont là deux  pratiques **essentielles** lorsqu'on programme avec des interruptions :
   * Le code de l'interruption doit être aussi léger que possible afin qu'il ne monopolise pas trop longtemps le CPU, qui doit tout de même assurer l'exécution de la fonction principale (*void loop()* dans notre cas). De plus, une interruption est supposée traiter un évènement imprévu et soudain (ce qui n'est pas du tout le cas dans notre exemple !) elle doit donc être rapide ;
   * L'attribut volatile est très important en programmation embarquée, et vous devez donc bien comprendre sa raison d’être. Les ISR étant appelées par des timers et pas par la fonction principale, elles apparaissent comme du code "mort" à l'optimiseur du compilateur gcc. Sans plus d'information, il va probablement les supprimer lors de la génération du firmware, ainsi que toutes les variables globales qu'elles modifient. L'attribut volatile lui interdit explicitement de réaliser ces "coupes" dans le code. 


