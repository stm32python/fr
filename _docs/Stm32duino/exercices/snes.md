---
title: Exercice avec la manette Nintendo SNES en C/C++ avec Stm32duino
description: Exercice avec la manette Nintendo SNES en C/C++ avec Stm32duino
---

# Exercice avec la manette Nintendo SNES en C/C++ avec Stm32duino

<h2>Description</h2>
La console Nintendo Super NES (ou SNES) se contrôle avec une manette disposant des caractéristiques suivantes :
 - Une croix directionnelle (haut, bas, droite, gauche)
 - Quatre boutons poussoirs à sa droite (A, B, X et Y)
 - Deux boutons poussoirs en son centre (START et SELECT)
 - Deux boutons poussoirs sur sa tranche avant (L et R)

Elle communique en protocole [SPI](../../Kit/glossaire) avec la console. C'est un protocole de communication qui fonctionne sur le principe de maitre qui envoie les informations (ici la manette) et esclave qui les reçoit (ici la console remplacée par la carte Nucleo).

Le protocole de communication s'établit de cette façon :
1. On envoie une pulsation sur la ligne LATCH pour capturer l’état des boutons de la manette.
2. On récupère ensuite les 16 bits en SPI en utilisant les lignes DATA et CLOCK.
3. On lit l’état des bits reçus.

Chaque bit correspond à un bouton. On obtient alors le tableau suivant :
<table>
   <tr>
       <td><b>Bit 15</b></td>
       <td><b>Bit 14</b></td>
       <td><b>Bit 13</b></td>
       <td><b>Bit 12</b></td>
       <td><b>Bit 11</b></td>
       <td><b>Bit 10</b></td>
       <td><b>Bit 9</b></td>
       <td><b>Bit 8</b></td>
       <td><b>Bit 7</b></td>
       <td><b>Bit 6</b></td>
       <td><b>Bit 5</b></td>
       <td><b>Bit 4</b></td>
       <td><b>Bit 3</b></td>
       <td><b>Bit 2</b></td>
       <td><b>Bit 1</b></td>
       <td><b>Bit 0</b></td>
   </tr>
   <tr>
       <td>"1"</td>
	   <td>"1"</td>
	   <td>"1"</td>
	   <td>"1"</td>
	   <td>R</td>
	   <td>L</td>
	   <td>X</td>
	   <td>A</td>
	   <td>Droite</td>
	   <td>Gauche</td>
	   <td>Bas</td>
	   <td>Haut</td>
	   <td>Start</td>
	   <td>Select</td>
	   <td>Y</td>
	   <td>B</td>
   </tr>
</table>


<h2>Montage</h2>

Afin de connecter la manette à la carte Nucleo on utilise 5 fils et une résistance de 10kΩ. Nous pouvons très bien connecter la manette à la carte Nucleo sans utiliser de résistance. Cependant, dans notre cas, elle nous servira à detecter que la manette est deconnectée.

La connectique de la manette SNES s'organise de la manière suivante :

<div align="left">
<img alt="Schéma de montage manette SNES" src="images/snes-schema.png" width="800px">
</div>


| Manette SNES      | ST Nucleo         |
| :-------------:   | :---------------: |
|        5V         |         5V        |
|      CLOCK        |         D3        |
|      LATCH        |         D2        |
|      DATA         |         D4        |
|      GND          |         GND       |

Sans oublier la résistance que l'on vient placer entre GND et DATA.


<h2>Programme</h2>

Pour simplifier le code nous utilisons une bibliothèque externe. Pour récupérer cette bibliothèque veuillez suivre ce [lien](https://gitlab.com/stm32python/fr/-/blob/master/assets/Script/Snes/Library/Snes.zip). Elle est contenue dans un fichier .zip qu'il faut dézipper et ensuite déplacer le dossier avec son contenu pour le mettre dans *\Documents\Arduino\libraries*. Le fichier du projet Arduino est quant à lui disponible en suivant ce [lien](https://gitlab.com/stm32python/fr/-/blob/master/assets/Script/Snes/snes_controller.ino).

**Etape 1 :** Pour faire fonctionner le programme nous devons dans un premier temps importer la bibliothèque téléchargée précédemment. Pour se faire, il faut l'importer au tout début de notre code de cette facon : 
```
#include <Snes.h>
```

On vient également créer une variable *snes* qui récupéra les informations retournées par la bibliothèque.
```
Snes snes;
```

**Etape 2 :** Ensuite on vient initialiser la bibliothèque qui va alors se charger d'activer la communication série et initialiser la communication avec la manette.
```
void setup()
{
  snes.init();
}
```

**Etape 3 :** Enfin nous affichons les données acquises. La bibliothèque se charge d'acquérir les données et afficher directement le résultat.
```
void loop()
{
  //Pour récupérer les données et les afficher
  snes.print();
}
```

**Etape 3 (bis) :** Si nous souhaitons récupérer les données en affichant seulement la valeur brute recue :
```
void loop()
{
  //Pour ne récupérer que les données
  int donnee = snes.data();
  Serial.println(donnee);
  delay(250);
}
```
A noter que dans ce cas ci la valeur récupérée sera un multiple de 2 (binaire) et chaque valeur correspondra à un bouton poussoir précis.


<h2>Résultat</h2>

Il ne vous reste plus qu'à appuyer sur le bouton *téléverser* pour transférer le programme puis allez dans le menu *Outils* puis *Moniteur Série* pour observer le résultat !

Nous pouvons à présent observer les données extraites de la manette SNES sous cette forme :


<div align="left">
<img alt="Affichage des données de la manette SNES" src="images/snes-c.png" width="400px">
</div>


Appuyez sur les différents boutons de la manette ainsi que sur la croix directionnelle et vous devriez voir le retour d'information sur votre écran.

