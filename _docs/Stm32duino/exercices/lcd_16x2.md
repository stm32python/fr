---
title: Afficheur Grove LCD 16 caractères x 2 lignes V2.0 (ou plus)
description: Mettre en œuvre un afficheur Grove LCD 16 caractères x 2 lignes V2.0 (ou plus) avec STM32duino
---
# Afficheur Grove LCD 16x2 V2.0 (ou plus, non rétroéclairé)

Ce tutoriel explique comment mettre en œuvre un module Grove afficheur I2C LCD 16 caractères x 2 lignes avec STM32duino V2.0 (ou plus).<br>
**Attention**, ne confondez pas ce module avec le [LCD 16x2 RGB rétroéclairé](https://wiki.seeedstudio.com/Grove-LCD_RGB_Backlight/) distribué notamment dans les [Grove Starter Kit (pour Arduino)](https://wiki.seeedstudio.com/Grove_Starter_Kit_v3/). Ce dernier n'est pas conseillé pour une utilisation avec les cartes de prototypage NUCLEO de STMicroelectronics, pour deux raisons : 
1. Il nécessite une alimentation en 5 volts (disponible sur les cartes NUCLEO mais il vaut mieux lui préférer le 3,3 volts) **mais surtout** 
2. **Il n'intègre pas les résistances de tirage des lignes SDA et SCL requises pour le bon fonctionnement du bus I2C**. Ce deuxième problème se pose également avec les afficheurs Grove LCD 16x2 non rétroéclairés de version antérieure à la 2.0 (voir explication à la fin de ce tutoriel).

## Matériel requis

1. La carte NUCLEO de STMicroelectronics
2. Une [carte d'extension de base Grove](https://wiki.seeedstudio.com/Base_Shield_V2/)
3. Un [module LCD Grove](https://wiki.seeedstudio.com/Grove-16x2_LCD_Series/)

**Les modules Grove LCD 16x2 :**

<div align="left">
<img alt="Grove - 16x2 LCD" src="images/grove-lcd_16x2_display.jpg" width="650px">
</div>

> Crédit image : [Seeed Studio](http://wiki.seeedstudio.com/Grove_System/)

Pour afficher des messages sur le LCD, dans cet ordre ...
1. Connecter la carte **Grove Base Shield pour Arduino** sur la NUCLEO.
2. Connecter l'afficheur sur une fiche I2C du shield.

## Bibliothèque(s) requise(s)

Cet exemple nécessite la bibliothèque [**Grove LCD_RGB Backlight par Seeed Studio**](https://GitHub.com/Seeed-Studio/Grove_LCD_RGB_Backlight), téléchargez la directement ou bien installez là à l'aide du gestionnaire de bibliothèque de l'IDE Arduino.

## Le sketch Arduino

>> Le sketch pour cet exemple (et tous les autres) peut être téléchargé [**en cliquant ici**](../../../assets/Sketch/TUTOS.zip).

Lancez l'IDE Arduino, ouvrez un nouveau sketch vide et copiez-y le code qui suit (traduit directement depuis [le Wiki seeedstudio](https://wiki.seeedstudio.com/Grove-16x2_LCD_Series/)) :

```c
/* 
 Objet du sketch :
 Mettre en œuvre un module afficheur I2C Grove LCD 16 caractères x 2 lignes 
 avec STM32duino
*/

#include <Wire.h> // Bibliothèque pour le bus I2C
#include "rgb_lcd.h" // Bibliothèque pour le LCD

// Instanciation de l'afficheur
rgb_lcd lcd;
 
void setup() 
{
    // Initialise le LCD.
    // Précise son nombre de colonnes (16) et de lignes (2)
    lcd.begin(16, 2);
 
    // Positionne le curseur en colonne 0, ligne 0
    // (on compte à partir de 0)
     lcd.setCursor(0, 0);

    // Affiche un message sur le LCD
    lcd.print("hello, world!");
 
    // Temporisation d'une seconde 
    delay(1000);
}
 
void loop() 
{
    // Positionne le curseur en colonne 0, ligne 1
    // (la ligne 1 est la deuxième ligne car on compte à partir de 0)
    lcd.setCursor(0, 1);

    // Affiche le nombre de secondes écoulées depuis le dernier reset
    lcd.print(millis()/1000);
 
    // Temporisation d'une seconde 
    delay(1000);
}
```

Après avoir vérifié que l'IDE Arduino est correctement configurée, notamment qu'elle est connectée au port COM attribué au ST-LINK de votre carte (sous Windows), cliquez sur "Téléverser". Observez sur le LCD le nombre de secondes écoulées depuis le dernier "reset" qui se met à jour chaque seconde.

## Remarque importante : I2C et Pull-up pour des révisions antérieures à la V2.0

Attention, cet afficheur, qui fonctionne sur un bus I2C, peut poser une difficulté aux non initiés à l'électronique : **pour qu'une communication I2C fonctionne avec un périphérique, il faut que ses broches SCL et SDA aient un potentiel porté à +3.3V**.

En pratique, **comme expliqué sur [la page Wiki de l'afficheur](https://wiki.seeedstudio.com/Grove-16x2_LCD_Series/)**, des résistances dites "de tirage" (pull-up en anglais) ont été ajoutées pour répondre à cette problématique sur la version 2.0 de l'afficheur. Donc *si vous souhaitez utiliser un module Grove LCD 16x2 dans sa première révision*, il ne fonctionnera pas à moins que vous ajoutiez en parallèle, sur les broches SCL et SDA, les résistances de tirage requises. 

Cette problématique est bien expliquée [ici](https://www.fabriqueurs.com/afficheur-lcd-2-lignes-de-16-caracteres-interface-i2c/) et par le schéma ci-dessous :

<br>
<div align="left">
<img alt="Grove - 16x2 LCD" src="images/i2c-pullup.jpg" width="600px">
</div>
<br>

Cette remarque est évidemment applicable à **n'importe quel autre module I2C** que vous viendriez connecter à la NUCLEO et qui ne serait pas lui-même en mesure d'apporter ces résistances de tirages polarisées à +3.3V.

