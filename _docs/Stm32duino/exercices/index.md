---
title: Tutoriels et projets avec STM32duino
description: Tutoriels et projets avec STM32duino
---
# Tutoriels et projets avec STM32duino

>> !! AVERTISSEMENT !!<br>
>> Cette section est encore en chantier, merci pour votre bienveillance !

## Démarrage

Pour la plupart de ces tutoriels, vous devrez disposer de la [*carte d’extension Grove pour Arduino*](https://wiki.seeedstudio.com/Base_Shield_V2/) (ou *Grove Base Shield for Arduino* en anglais).
C'est une carte au format Arduino qui vient se connecter sur les cartes NUCLEO et permet de brancher aisément des capteurs et des actionneurs digitaux, analogiques, UART et I2C avec la connectique propriétaire [Grove](http://wiki.seeedstudio.com/Grove_System/).

<br>
<div align="left">
<img alt="Grove base shield pour Arduino" src="images/grove_base_shield.jpg" width="670px">
</div>
<br>

> Crédit images : [Seeedstudio](http://wiki.seeedstudio.com/Grove_System/)

Elle offre :
* Six connecteurs D2, D3... pour des périphériques numériques c'est à dire pilotés par un niveau logique 0 (0V) ou 1 (+3.3V) ;
* Quatre connecteurs A0, A1... pour des périphériques qui envoient un signal analogique en entrée (entre 0 et +3.3V ou 0 et +5V selon la position du petit interrupteur qui l'équipe) ;
* Quatre connecteurs pour des périphériques dialoguant avec le protocole I2C ;
* Un connecteur pour un port série (UART) ;
* Un commutateur 3,3V / 5V qui permet de sélectionner la tension d'alimentation des modules que vous connecterez sur ses fiches.


## Les cartes NUCLEO n'aiment pas les modules 5V !

**Prenez garde cependant aux tensions d'alimentation des modules externes que vous connectez sur les cartes NUCLEO !** Certains nécessitent du 5V, d'autres fonctionnent aussi bien avec du 5V et du 3.3V, d'autres enfin fonctionnent exclusivement en 3,3V et seront peut-être endommagés par du 5V. **Lisez bien leurs fiches techniques avant de les brancher !**<br>
Nous vous recommandons également de prendre connaissance des [**précautions d'utilisations des cartes NUCLEO exposées dans ce tutoriel**](../../Kit/nucleo).

Dans la liste des périphériques qui nécessitent d'être alimentés **exclusivement** en 5V sur les cartes NUCLEO, ci-dessous, figure seulement [le module LCD RGB Grove](lcd_RGB_16x2). Il présente **une autre particularité** : il ne dispose pas des résistances de tirage (pull-up) requises par le bus I2C auquel il est connecté et ne fonctionnera pas si ces résistances ne sont pas ajoutées par vos soins.

D'une façon générale, **la plus grande attention** est requise si vous souhaitez utiliser des périphériques fonctionnant exclusivement en 5V avec des cartes NUCLEO. 
- S'il s'agit de capteurs qui injectent du courant dans le microcontrôleur, **ils pourraient purement et simplement détruire celui-ci**. Donc, si vous ne maîtrisez pas le sujet, nous vous conseillons vivement de **ne pas en utiliser du tout**.
- S'il s'agit de périphériques en logique 5V **pilotés** par le STM32, ils devraient fonctionner correctement.

Il est à noter que la plupart des modules actuels sont vendus pour être alimentés en 5V ou en 3,3V (grâce à un régulateur de tension). Ils ne posent bien sûr aucun problème.

## Précision importante concernant l'adressage sur le bus I2C

Vous constaterez que la plus grande partie des modules présentés ci-après utilisent **le protocole I2C** et doivent donc être connectés au bus du même nom sur la carte NUCLEO. I2C est l'acronyme de "Inter-Integrated Circuit" (en français : bus de communication intégré inter-circuits). Il s'agit d'un bus série fonctionnant selon un protocole inventé par Philips. Pour dialoguer sur un bus I2C, chaque module "esclave" qui s'y trouve branché **est identifié par une adresse codée sur 7 bits** afin de dialoguer avec le contrôleur maître intégré au MCU STM32.

Avec la prolifération des modules I2C, **il pourrait arriver que deux ou plusieurs modules que vous auriez connectés sur un bus I2C aient la même adresse**, ce qui conduirait inévitablement à des plantages. Pour éviter ce type de conflits, vous devrez consulter les fiches techniques de vos modules et, pour ceux qui le permettent, prendre soin de modifier (si nécessaire) leur adresse I2C, généralement codée dans leur firmware.

Les tableaux ci-dessous rappellent, entre parenthèses, dans la colonne *Connectique/protocole*, l'adresse I2C par défaut - celle qui est écrite dans la classe pilote - des différents modules que nous avons utilisés. Si vous choisissez d'autres modules, il est possible que leur adresse change et que vous soyez obligé de la passer en paramètre lors de l'instanciation de leur pilote. Ce sera parfois le cas, par exemple, selon que vous utiliserez un même capteur implémenté dans un module Grove de Seeed Studio ou dans un module Adafruit.

## Liste des tutoriels 

Vous trouverez ici quelques tutoriels essentiellement [pour des modules au format Grove de la société Seeed Studio](http://wiki.seeedstudio.com/Grove_System/). Ils se transposeront facilement à des modules d'autres fabricants pour peu qu'ils utilisent les mêmes protocoles et bus. 

Nous aborderons et expliquerons au gré des tutoriels des notions de programmation embarquée et d'architecture des microcontrôleurs qui vous seront indispensables pour progresser. Lorsque vous rencontrerez un acronyme que vous ne connaissez pas, n'hésitez pas à consulter le [glossaire](../../Kit/glossaire).

>> L’ensemble des sketchs présentés ou utilisés par la suite peuvent être téléchargés dans une seule archive ZIP [**en cliquant ici**](../../../assets/Sketch/TUTOS.zip).

<bR>

**1. Afficheurs & LED**

|**Tutoriel**|**Périphériques abordés**|
|:-|:-:|
[Afficheur Grove LCD 16 caractères x 2 lignes V2.0 (ou plus)](lcd_16x2)|I2C (0x3E)|
[LED, clignotement, méthodes avancées](del_blink)|GPIO, Timers|

<br>

**2. Capteurs spatiaux et de mouvement**

|**Tutoriel**|**Périphériques abordés**|
|:-|:-:|
|||

<br>

**3. Capteurs environnementaux**

|**Tutoriel**|**Périphériques abordés**|
|:-|:-:|
[Capteur de température (thermistance)](thermistance)|GPIO, ADC|

<br>

**4. Enregistrement et saisie de données**

|**Tutoriel**|**Périphériques abordés**|
|:-|:-:|
|||

<br>

**5. Actuateurs**

|**Tutoriel**|**Périphériques abordés**|
|:-|:-:|
[Servomoteur](servomoteur)|PWM (Timers)|

<br>

**6. Protocoles et communications sans fil**

|**Tutoriel**|**Périphériques abordés**|
|:-|:-:|
[BLE - Diffusion d'une mesure de température](ble_advertising)|CLE, GAP|
[BLE - Echange de chaînes de caractères](nus_ble_service)|BLE, GATT|
[BLE - Publication d'une température et contrôle d'une LED](blue_st)|BLE, GATT|
[LoRa - Communication directe entre deux modules LoRa-E5](p2p_loraE5)|UART, LoRa|
[LoRa - Publication LoRaWAN avec un module LoRa-E5](lora-e5)|UART, LoRa & LoRaWAN|
[LoRa - Publication LoRaWAN avec une carte NUCLEO-WL55JC1](lora-nucleo-wl55jc1)|LoRa & LoRaWAN|
[Module RFID X-NUCLEO-NFC05A1](x-nucleo-nfc05a1)|SPI|
[Module Wi-Fi Grove UART V2 - NTP](wifi_ntp)|UART, Wi-Fi|

<br>

**7. Inclassables, périphériques intégrés et projets**

|**Tutoriel**|**Périphériques abordés**|
|:-|:-:|
[Contrôle d'une LED avec un capteur de distance](led_and_tof)|Timers|
[Génération et analyse spectrale de signal (DFT/FFT)](dac_adc_fft)|DAC, ADC|
[Horloge temps-réel intégrée (RTC) des STM32](rtc)|RTC|
[Lampe avec minuteur](timed_light)|Timers|
[Sauvegarder dans la mémoire flash des STM32](flash)|Flash|
[Station météo connectée (gros projet)](../projets/meteo_nucleo_l476rg)|Multiples|
