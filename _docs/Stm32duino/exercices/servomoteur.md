---
title: Servomoteur Grove analogique
description: Mettre en œuvre un module Grove servomoteur avec STM32duino
---
# Servomoteur Grove

Ce tutoriel, adapté de la bibliothèque d'exemples livrée avec l'IDE Arduino, explique comment mettre en œuvre le module Grove servomoteur du [Grove Starter Kit](https://wiki.seeedstudio.com/Grove_Starter_Kit_v3/) avec STM32duino.
Il s'agit de contrôler l'angle de rotation d'un servomoteur à l'aide d'un [potentiomètre](potentiometre), ce qui permet de mettre en pratique une [conversion analogique-numérique (ADC)](https://fr.wikipedia.org/wiki/Convertisseur_analogique-num%C3%A9rique) et une [sortie modulée en largeur d'amplitude (PWM)](https://fr.wikipedia.org/wiki/Modulation_de_largeur_d%27impulsion).

## Matériel requis

1. Une [carte NUCLEO-L476RG de STMicroelectronics](https://www.st.com/en/evaluation-tools/nucleo-l476rg.html) (ou toute autre carte NUCLEO, à condition de connecter le servomoteur sur une broche capable de générer un signal de sortie de type PWM)
2. Une [carte d'extension de base Grove](https://wiki.seeedstudio.com/Base_Shield_V2/)
3. Un [module servomoteur analogique Grove](https://wiki.seeedstudio.com/Grove-Servo/)
4. Un [module potentiomètre Grove](https://wiki.seeedstudio.com/Grove-Rotary_Angle_Sensor/)

**Potentiomètre et servomoteur Grove :**

<div align="left">
<img alt="Grove - rotary encoder" src="images/grove-rotary.jpg" width="150px">
<img alt="Grove - servomotor" src="images/grove-servo.jpg" width="250px">
</div>

> Crédit image : [Seeed Studio](http://wiki.seeedstudio.com/Grove_System/)

**Attention**, le servomoteur est fragile, **ne le faites surtout pas tourner "à la main" en forçant sur son axe vous le casseriez !**

Pour cet exemple, il faut :
1. Connecter la carte **Grove Base Shield pour Arduino** sur la carte NUCLEO avec son commutateur **placé sur 3V3**.
2. Connecter le servomoteur sur une broche capable de générer un signal PWM. Nous supposons travailler avec une NUCLEO-L476RG donc nous choisissons la broche numérique D3 du shield Grove.
3. Connecter le potentiomètre sur l'entrée analogique A0 du shield Grove.

## Bibliothèque(s) requise(s)

Cet exemple nécessite la bibliothèque **Servo**, mais vous n'aurez pas à la télécharger, elle sera intégrée par défaut dans l'IDE Arduino et adaptée à l'architecture STM32 si vous avez suivi correctement [la procédure d'installation](../installation).

## Le sketch Arduino

>> Le sketch pour cet exemple (et tous les autres) peut être téléchargé [**en cliquant ici**](../../../assets/Sketch/TUTOS.zip).

Lancez l'IDE Arduino, ouvrez un nouveau sketch vide et copiez-y le code qui suit (adapté des exemples disponibles dans l'IDE Arduino) :


```c
/*
  Objet du sketch :
  Contrôle d'un servomoteur à l'aide d'un potentiomètre.
  Adaptation de l'exemple Arduino par
  Michal Rinott et Scott Fitzgerald
*/

// Bibliothèque pour gérer le servomoteur
#include <Servo.h>

#define POT_PIN A0 // Broche analogique d'entrée du potentiomètre
#define PWM_PIN D3 // Broche PWM de sortie pour le servomoteur

Servo myservo;  // Instance (myservo) d'un objet de type Servo

void setup() {
  pinMode(A0,INPUT);  // On met le potentiomètre en entrée analogique
  myservo.attach(PWM_PIN);  // On attache le servomoteur à la broche PWM_PIN
}

void loop() {
  // Lecture de la réponse de l'ADC connecté au potentiomètre (entre 0 et 1023)
  uint16_t val = analogRead(POT_PIN);
  // Remappe cette valeur entre 0 et 180 (degrés angulaires) 
  val = map(val, 0, 1023, 0, 180);
  // Fait tourner le servomoteur de l'angle indiqué  
  myservo.write(val);
  // Temporisation de 15 millisecondes                  
  delay(15);                          
}

```

Après avoir vérifié que l'IDE Arduino est correctement configurée, notamment qu'elle est connectée au port COM attribué au ST-LINK de votre carte (sous Windows), cliquez sur "Téléverser". Si vous faites tourner le potentiomètre, vous constaterez que le bras du servomoteur suit la même rotation.
