---
title: Capteur de température (thermistance)
description: Mise en œuvre du module capteur de température Grove v1.2 avec STM32duino
---

# Capteur de température (thermistance)

Ce tutoriel explique comment mettre en œuvre un [module capteur de température Grove v1.2 avec STM32duino](https://wiki.seeedstudio.com/Grove-Temperature_Sensor_V1.2/). Ce module est constitué d'une **thermistance à coefficient de température négatif** de type NCP18WF104F03RC dont vous trouverez la fiche technique [ici](https://files.seeedstudio.com/wiki/Grove-Temperature_Sensor_V1.2/res/NCP18WF104F03RC.pdf). Vous trouverez une vidéo à son propos [ici](https://youtu.be/wjL7xOGqAqg), qui explique les calculs dans le script ci-après, **en anglais**.

## Matériel requis

1. Une [carte d'extension de base Grove](https://wiki.seeedstudio.com/Base_Shield_V2/) **avec son commutateur positionné sur 3.3V**. 
2. Une carte NUCLEO de STMicroelectronics supportée par STM32duino (toutes le sont !)
3. Un [module Grove - Temperature Sensor v1.2](https://wiki.seeedstudio.com/Grove-Temperature_Sensor_V1.2/)

**Le module Grove Temperature Sensor v1.2 :**
<div align="left">
<img alt="Grove NCP18WF104F03RC" src="images/8_temperature/capteur_temp.png" width="350px">
</div>

> Crédit image : [Seeed Studio](http://wiki.seeedstudio.com/Grove_System/)

Dans sa plage de fonctionnement linéaire (-40 à 125 °C, avec une précision de 1,5 °C), la résistance de la thermistance reste proportionnelle à la température ambiante. Cette résistance, en variant, fait également varier la tension de sortie d'un pont diviseur de tension intégré au module Grove. Cette tension est numérisée (on dit plutôt échantillonnée) par un ADC via une broche d'entrée analogique (dans notre exemple, on connecte le module à la broche A0). Notre programme devra recalculer la valeur de la température à partir de la conversion de l'ADC et l'afficher dans le terminal série.

## Bibliothèque(s) requise(s)

Aucune bibliothèque n'est nécessaire pour cet exemple, le sketch qui suit est suffisant.

## Le sketch Arduino

>> Le sketch pour cet exemple (et tous les autres) peut être téléchargé [**en cliquant ici**](../../../assets/Sketch/TUTOS.zip).

Lancez l'IDE Arduino, ouvrez un nouveau sketch vide et copiez-y le code qui suit :

```c
/* Objet du sketch :
Mesure de température à partir d'une thermistance d'un module Grove Temperature sensor V1.2 relié à l'entrée A0
L'ADC interne fonctionnant sur 3.3V, le capteur doit être alimenté en 3.3V sur la carte d'adaptation Grove.
*/

// Paramètres de l'ADC
#define VREF 3.3    // Tension de référence de l'ADC (en volts)
#define ADC_RES 12  // Résolution de l'ADC (en nombre de bits)

// Calcul du pas, ou quantum, de l'ADC
const float scale = pow(2, (float)ADC_RES);
const float ADC_QUANT = VREF / scale;

// Définition des constantes nécessaires pour la conversion.
#define R1 10000

// Coefficients de Steinhart-Hart pour la conversion
#define c1 0.001129148
#define c2 0.000234125
#define c3 0.0000000876741

void setup() {
  
  Serial.begin(9600);  // On initialise l'UART du terminal série à 9600 bauds
  pinMode(A0, INPUT);  // On met le capteur en entrée analogique

  // Fixe la résolution de l'ADC à ADC_RES
  analogReadResolution(ADC_RES);

}

void loop() {

  // Echantillonnage de la tension de la thermistance par l'ADC
  uint16_t sample = analogRead(A0);

  // Affiche la valeur échantillonnée par l'ADC
  Serial.print("Valeur convertie par l'ADC : ");
  Serial.println(sample);

  // Affiche la tension renvoyée par la thermistance
  Serial.print("Tension aux bornes de la thermistance (V) : ");
  float Vther = (float)sample * ADC_QUANT;
  Serial.println(Vther, 1);

  // Calcule la résistance du thermistor
  float R2 = R1 * (scale / (float)sample - 1.0);

  // Calcule la température en Kelvins à partir de la résistance
  float logR2 = log(R2);
  float Tk = (1.0 / (c1 + c2 * logR2 + c3 * logR2 * logR2 * logR2));

  // Convertit les Kelvins en degrés Celsius.
  float Tc = Tk - 273.15;

  // Convertit des degrés Celsius en degrés Fahrenheits.
  float Tf = (Tc * 9.0) / 5.0 + 32.0;

  // Affiche les résultats avec une seule décimale après la virgule

  Serial.print("Température (K): ");
  Serial.println(Tk, 1);

  Serial.print("Température (°C): ");
  Serial.println(Tc, 1);

  Serial.print("Température (°F): ");
  Serial.println(Tf, 1);

  // Saut de ligne
  Serial.print("\n");

  delay(5000);  // Temporisation de cinq secondes
}
```

Après avoir vérifié que l'IDE Arduino est correctement configurée, notamment qu'elle est connectée au port COM attribué au ST-LINK de votre carte (sous Windows), cliquez sur "Téléverser". **Assurez-vous également que le commutateur d'alimentation électrique du Grove base shield est bien positionné sur 3.3V**.

## Affichage de la température sur le terminal série

Si tout s'est passé correctement vous devriez observer les mesures de température qui défilent dans le terminal série de l'IDE Arduino, à l'écoute du ST-LINK, toutes les cinq secondes. 
La précision du ce capteur est étonnante dans notre test : entre 21.1 et 21.2°C selon lui, à comparer aux 21.2°C selon [un système de mesure Velleman DEM500](https://www.velleman.eu/products/view/?country=fr&lang=fr&id=420578) !
