---
title: Exercice avec la carte d'extension IKS01A3 en C/C++ pour Stm32duino
description: Exercice avec la carte d'extension IKS01A3 en C/C++ pour Stm32duino
---
# Exercice avec la carte d'extension IKS01A3 en C/C++ pour Stm32duino

Vous devrez disposez de la carte d’extension IKS01A3 pour continuer ces exercices.

## Démarrage

La carte d’extension IKS01A3 est une carte de démonstration de plusieurs capteurs MEMS de STMicroelectronics. Sa version A3 contient les capteurs suivants:

* LSM6DSO : Accéléromètre 3D + Gyroscope 3D
* LIS2MDL : Magnétomètre 3D
* LIS2DW12 : Accéléromètre 3D
* LPS22HH : Baromètre (260-1260 hPa)
* HTS221 : Capteur d’humidité relative 
* STTS751 : Capteur de température (–40 °C to +125 °C) 

Ces capteurs sont raccordés au bus I2C de la carte P-NUCLEO-WB55.

La carte d’extension IKS01A3 dispose également d'un emplacement au format DIL 24 broches pour y ajouter des capteurs I2C supplémentaires (par exemple, le gyroscope [A3G4250D](https://www.st.com/en/evaluation-tools/steval-mki125v1.html)) 

![image](images/iks01a3.png)

## Branchement

Brancher la carte IKS01A3, attention à bien respecter le marquage des connecteur : CN9 -> CN9, CN5 -> CN5, etc...

Une fois que vous aurez raccordé correctement la carte d’extension, voici la liste des capteurs avec lesquels vous pouvez dialoguer par I2C.

## Installation des bibliothèques pour la carte d'extension IKS01A3

### Sur Windows
A faire

### Sur Linux
A faire

### Sur MacOS
Entrez les commandes suivantes :
```bash
cd ~/Documents/Arduino/libraries/
for lib in X-NUCLEO-IKS01A3 LPS22HH LSM6DSO LIS2DW12 LIS2MDL LPS22HH STTS751 HTS221
do
wget https://codeload.GitHub.com/stm32duino/${lib}/zip/master -O ${lib}-master.zip
unzip ${lib}-master.zip
done 
```

## Utilisation

Lancez l'IDE Arduino (préalablement configuré pour Stm32duino)

Ouvrez le croquis d'exemple `Fichier > Exemples > STM32Duino X-NUCLEO-IKS01A3 > X_NUCLEO_IKS01A3_HelloWorld`.

Branchez la carte Nucleo muni de la carte IKS01A3.

Compilez et chargez le binaire produit.

Ouvrez la console série qui affiche les traces suivantes :
```
| Hum[%]: 73.30 | Temp[C]: 18.10 | Pres[hPa]: 984.41 | Temp2[C]: 18.53 | Temp3[C]: 19.00 | Acc[mg]: -96 26 1008 | Gyr[mdps]: 350 840 -560 | Acc2[mg]: 14 111 962 | Mag[mGauss]: -214 456 -574 |
| Hum[%]: 73.30 | Temp[C]: 18.10 | Pres[hPa]: 984.51 | Temp2[C]: 18.53 | Temp3[C]: 19.00 | Acc[mg]: -83 27 1005 | Gyr[mdps]: 770 210 -630 | Acc2[mg]: 14 96 952 | Mag[mGauss]: -214 462 -573 |
| Hum[%]: 73.60 | Temp[C]: 18.10 | Pres[hPa]: 984.44 | Temp2[C]: 18.53 | Temp3[C]: 19.00 | Acc[mg]: -87 21 1010 | Gyr[mdps]: 280 490 -560 | Acc2[mg]: 7 101 963 | Mag[mGauss]: -196 469 -567 |
| Hum[%]: 73.60 | Temp[C]: 18.10 | Pres[hPa]: 984.40 | Temp2[C]: 18.53 | Temp3[C]: 19.00 | Acc[mg]: -93 -2 1022 | Gyr[mdps]: 1470 -1960 -840 | Acc2[mg]: -15 102 974 | Mag[mGauss]: -199 469 -570 |
| Hum[%]: 73.70 | Temp[C]: 18.10 | Pres[hPa]: 984.54 | Temp2[C]: 18.53 | Temp3[C]: 19.00 | Acc[mg]: -26 45 984 | Gyr[mdps]: 1960 -70 -350 | Acc2[mg]: 34 37 936 | Mag[mGauss]: -211 468 -568 |
| Hum[%]: 73.70 | Temp[C]: 18.10 | Pres[hPa]: 984.46 | Temp2[C]: 18.52 | Temp3[C]: 19.00 | Acc[mg]: -93 31 1004 | Gyr[mdps]: 0 980 -490 | Acc2[mg]: 18 108 959 | Mag[mGauss]: -202 451 -570 |
| Hum[%]: 73.70 | Temp[C]: 18.10 | Pres[hPa]: 984.41 | Temp2[C]: 18.53 | Temp3[C]: 19.00 | Acc[mg]: -88 50 1005 | Gyr[mdps]: 1330 -280 -630 | Acc2[mg]: 39 98 964 | Mag[mGauss]: -213 466 -568 |
| Hum[%]: 73.70 | Temp[C]: 18.10 | Pres[hPa]: 984.46 | Temp2[C]: 18.53 | Temp3[C]: 19.00 | Acc[mg]: -57 42 1017 | Gyr[mdps]: 420 -280 -560 | Acc2[mg]: 31 76 965 | Mag[mGauss]: -213 454 -570 |
| Hum[%]: 73.80 | Temp[C]: 18.20 | Pres[hPa]: 984.53 | Temp2[C]: 18.53 | Temp3[C]: 19.00 | Acc[mg]: -91 22 1005 | Gyr[mdps]: 350 840 -420 | Acc2[mg]: 9 104 955 | Mag[mGauss]: -199 468 -565 |
| Hum[%]: 73.80 | Temp[C]: 18.20 | Pres[hPa]: 984.43 | Temp2[C]: 18.54 | Temp3[C]: 19.00 | Acc[mg]: -75 18 1025 | Gyr[mdps]: 420 -630 -490 | Acc2[mg]: 4 87 974 | Mag[mGauss]: -217 460 -564 |

```

Bougez la carte pour faire varier les valeurs des capteurs Acc, Gyr, Acc2, Mag.






