---
title: BLE - Echange de chaînes de caractères
description: Mise en œuvre du Nordic UART Service pour un échange bi-directionnel de chaînes de caractères.
---

# BLE - Echange de chaînes de caractères

Ce tutoriel montre comment construire un service BLE permettant à un central et un périphérique d'échanger des séquences d'octets qui pourront être interprétées comme des commandes, des valeurs numériques ou encore des séquences de caractères affichables (i.e. du texte). On désigne abusivement ce service par *BLE UART* car il apporte une fonctionnalité similaire à l'[UART](../../Kit/glossaire).

Le schéma de principe suivant résume ce que nous allons réaliser :

<br>
<div align="left">
<img alt="BLE UART use case" src="images/BLEUART.jpg" width="700px">
</div>
<br>

Il montre le protocole GATT en action après la phase d'advertising GAP et la connexion entre le central et un périphérique.
Le périphérique "partage" avec le central **deux caractéristiques** :
- UART_TX dotée de l'attribut "Notify". Le central (client) peut lire son contenu quand bon lui semble et est informé (NOTIFY) des modifications que le périphérique (serveur) réalise sur celle-ci.
- UART_RX dotée le l'attribut "Write". Le central (client) peut donc y écrire (WRITE) du contenu quand bon lui semble.
L'usage conjoint de ces deux caractéristiques permet de simuler un [port série](../startwb55/uart) de faible débit avec sa ligne d'émission (RX) et de transmission (TX), ce qui justifie le nom d'UART attribué à ce service.

# Première partie : Création du périphérique

Commençons par créer le périphérique. Notre exemple va consister à envoyer un message vers un éventuel central connecté chaque fois que l'on appuie sur le bouton "USER" de la carte de prototypage sélectionnée.

## Matériel requis

Nos sketchs proposent plusieurs options pour le matériel permettant de communiquer en BLE, qui pourra être constitué au choix :

- D'une carte [NUCLEO-WB55](../../Kit/nucleo_wb55rg) ou bien
- D'une carte [NUCLEO-L476RG](../../Kit/nucleo_l476rg) équipée soit d'un [shield X-NUCLEO-IDB05A1](https://www.st.com/en/ecosystems/x-nucleo-idb05a1.html) soit d'un [shield X-NUCLEO-IDB05A2](https://www.st.com/en/ecosystems/x-nucleo-idb05a2.html) ou bien
- D'une carte [DISCOVERY B-L475E-IOT01A1](https://www.st.com/en/evaluation-tools/b-l475e-iot01a.html) ou bien
- D'une carte [DISCOVERY B-L4S5I-IOT01A](https://www.st.com/en/evaluation-tools/b-l4s5i-iot01a.html).

Pour indiquer au compilateur quel matériel vous utilisez effectivement, prenez soin **d'attribuer la bonne valeur à la directive de préprocesseur *#define HW_CONFIG*** dans le sketch.

>> **Attention**, selon le matériel BLE que vous utilisez, [des mises à jour de firmwares peuvent être nécessaires](https://github.com/stm32duino/STM32duinoBLE) ! Pour la NUCLEO-WB55, la procédure est expliquée [ici](../../tools/cubeprog/cube_prog_firmware_ble_hci).

## Bibliothèque(s) requise(s)

La gestion du BLE par STM32duino est assurée par **la bibliothèque STM32duinoBLE** disponible [ici](https://github.com/stm32duino/STM32duinoBLE).

## Le sketch Arduino pour le périphérique

>> Le sketch pour cet exemple (et tous les autres) peut être téléchargé [**en cliquant ici**](../../../assets/Sketch/TUTOS.zip).

Lancez l'IDE Arduino, ouvrez un nouveau sketch vide et copiez-y le code qui suit :

```c
/*
  Objet du sketch :
  Implémentation d'un périphérique BLE Nordic UART Service (NUS)
  Ce périphérique expose deux caractéristiques :
  - Une "inscriptible" par le central connecté, RX, qui contient des chaînes de caractères
    de taille maximum MAX_BLE_CHAR. Le périphérique surveille les évènements d'écriture dans RX et
    peut réagir lorsque le central la modifie.
  - Une notifiée au central, TX, qui contient des chaînes de caractères de taille maximum MAX_BLE_CHAR
    et à laquelle il pourra s'abonner pour être averti de ses modifications.
  Le couple RX/TX permet donc au central et à son bériphérique d'échanger des messages à la façon d'un
  port série.

  Matériel utilisé :
  Trois configurations sont possibles, selon la valeur assignée à la directive de préprocesseur HW_CONFIG :
  - Si HW_CONFIG = 1 : Le script sera compilé pour une carte DISCOVERY B-L475E-IOT01A1 ou B-L4S5I-IOT01A
  - Si HW_CONFIG = 2 : Le script sera compilé pour une carte NUCLEO-WB55
  - Si HW_CONFIG = 3 : Le script sera compilé pour une carte NUCLEO-L476RG et un shield IDB05A1 ou IDB05A2
  Le central et le périphérique ne sont pas tenus d'avoir la même configuration matérielle.

  Utilisation :
  Compilez le script, chargez le dans votre carte et observez les messages sur le terminal série de l'IDE
  Arduino. Pour envoyer un message au central connecté, appuyez sur le bouton USER (varie selon le matériel
  que vous utilisez).
*/

// Identifiant du périphérique
#define DEVICE_ID "device_001"

// Débit du port série du ST-LINK
#define ST_LINK_BAUDRATE (115200)

// Broche du bouton
#define BUTTON_PIN USER_BTN

// On gère le polling BLE avec un timer
// Instanciation de gestion de l'interruption de dépassement du timer 1
TIM_TypeDef *Instance1 = TIM1;
HardwareTimer *Timer1 = new HardwareTimer(Instance1);
// Fréquence de polling du BLE assurée par le timer 1
#define BLE_POLLING_HZ (100)

/* Définitions pour le BLE */

// Bibliothèque disponible ici : https://github.com/stm32duino/STM32duinoBLE
#include <STM32duinoBLE.h>

// Instanciation du BLE

// Sélectionne la configuration matérielle parmi celles disponibles
#define HW_CONFIG 3

#if HW_CONFIG == 1
/* Carte B-L475E-IOT01A1 ou B_L4S5I_IOT01A */
SPIClass SpiHCI(PC12, PC11, PC10);
HCISpiTransportClass HCISpiTransport(SpiHCI, SPBTLE_RF, PD13, PE6, PA8, 8000000, SPI_MODE0);
BLELocalDevice BLEObj(&HCISpiTransport);
BLELocalDevice &BLE = BLEObj;
#elif HW_CONFIG == 2
/* Carte NUCLEO WB55 */
HCISharedMemTransportClass HCISharedMemTransport;
BLELocalDevice BLEObj(&HCISharedMemTransport);
BLELocalDevice &BLE = BLEObj;
#elif HW_CONFIG == 3
/* Carte NUCLEO L476RG et shield IDB05A1/A2 avec la broche SPI clock sur D3 */
// Attention, les lignes ci-dessous informent que les broches Arduino :
// A0, A1, D3, D7, D11 et D12 sont utilisées par le shield IDB05A1 et donc
// ne sont plus disponibles pour y connecter autre chose !
SPIClass SpiHCI(D11, D12, D3);
HCISpiTransportClass HCISpiTransport(SpiHCI, SPBTLE_RF, A1, A0, D7, 8000000, SPI_MODE0);
BLELocalDevice BLEObj(&HCISpiTransport);
BLELocalDevice &BLE = BLEObj;
#endif

// Déclarations des UUID pour le service BLE NUS et ses caractéristiques
// Rx (réception) et Tx (émission)
// Ces valeurs sont standardisées, elles permettent notamment à des applications
// central BLE pour smartphones (telles que NRF connect) de reconnaître un périphérique
// BLE NUS.
#define BLE_UART_UUID "6E400001-B5A3-F393-E0A9-E50E24DCCA9E"
#define BLE_UART_RX_UUID "6E400002-B5A3-F393-E0A9-E50E24DCCA9E"
#define BLE_UART_TX_UUID "6E400003-B5A3-F393-E0A9-E50E24DCCA9E"

// Taille maximum du buffer de messages (en nombre de caractères)
#define MAX_BLE_CHAR (128)
// Buffer de messages
char CHAR_BUFFER[MAX_BLE_CHAR];

// Crée le service avec le bon UUID
BLEService remoteService(BLE_UART_UUID);
// Caractéristique Rx
BLEStringCharacteristic rxCharacteristic(BLE_UART_RX_UUID, BLEWrite, MAX_BLE_CHAR);
// Caractéristique Tx
BLEStringCharacteristic txCharacteristic(BLE_UART_TX_UUID, BLENotify, MAX_BLE_CHAR);

// Est-ce qu'un central est connecté ?
volatile uint8_t central_connected = 0;

// Compteur utilisé pour numéroter les messages envoyés au central
volatile uint8_t message_number = 0;

// Est-ce qu'un message doit-être envoyé au central ?
volatile uint8_t pending_message = 0;

/****************************************************************/
/* Initialisations                                              */
/****************************************************************/
void setup() {

  // Initialisation du port série du ST-LINK
  Serial.begin(ST_LINK_BAUDRATE);
  while (!Serial) {};

  Serial.println("PERIPHERIQUE BLE NUS");
  Serial.println("Mon identifiant est " + String(DEVICE_ID));

  // Initialisation du bouton utilisateur et gestion de celui-ci par
  // interruptions
  pinMode(BUTTON_PIN, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(BUTTON_PIN), SendMsgISR, FALLING);

  // Initialisation du BLE
  init_BLE();

  // Initialisation de l'interruption périodique du timer 1 pour
  // gérér les évènements BLE.
  init_Timer_BLE();
}


/****************************************************************/
/* Boucle principale                                            */
/****************************************************************/
void loop() {

  // Si on est connecté à un central
  if (central_connected) {

    // Si un message doit être envoyé au central
    if (pending_message) {

      // On construit le message
      String BLE_TX = "Hello " + String(message_number++);

      // On le convertit en un tableau de caractères
      BLE_TX.toCharArray(CHAR_BUFFER, BLE_TX.length() + 1);

      // On l'envoie au central connecté en écrivant dans sa
      // caractéristique TX
      txCharacteristic.writeValue(CHAR_BUFFER);

      Serial.print("Données envoyées au central : ");
      Serial.println(CHAR_BUFFER);

      // On réarme pour la réception du prochain message
      pending_message = 0;
    }
  }

}

/****************************************************************/
/* Initialisation du BLE                                        */
/****************************************************************/
void init_BLE(void) {

  // Initialisation du BLE
  if (!BLE.begin()) {
    Serial.println("Echec de démarrage du BLE !");
    while (1) {};
  }

  // Nom du service exposé par l'advertising
  BLE.setLocalName(DEVICE_ID);

  // Fixe l'UUID du service de réception
  BLE.setAdvertisedService(remoteService);

  // Ajoute la caractéristique de réception Rx
  // Les messages reçus du central connecté par la carte Nucleo seront écrits dans celle-ci.
  remoteService.addCharacteristic(rxCharacteristic);

  // Ajoute la caractéristique d'émission Tx
  // Pour envoyer un message à un central connecté, la carte Nucleo devra écrire dans celle-ci.
  remoteService.addCharacteristic(txCharacteristic);

  // Ajoute le service de réception
  BLE.addService(remoteService);

  // Gestionnaire des évènements central connecté ou déconnecté au périphérique
  BLE.setEventHandler(BLEConnected, blePeripheralConnectHandler);
  BLE.setEventHandler(BLEDisconnected, blePeripheralDisconnectHandler);

  // Gestionnaire des évènements d'écriture par le central dans la caractéristique Rx
  rxCharacteristic.setEventHandler(BLEWritten, rxCharacteristicWritten);

  // Valeur initiale des charactéristiques
  rxCharacteristic.setValue("");
  txCharacteristic.setValue("");

  // Démarre la publication du service et des caractéristiques
  BLE.advertise();

  Serial.println("Attente de connexion au central pair ...");
}

/****************************************************************/
/* Gestionnaire de l'évènement de connexion au central          */
/****************************************************************/
void blePeripheralConnectHandler(BLEDevice central) {
  Serial.print("Connecté au central ");
  Serial.println(central.address());
  // Signale que le périphérique est actuellement connecté à un
  // central
  central_connected = 1;
}

/****************************************************************/
/* Gestionnaire de l'évènement de déconnexion du central        */
/****************************************************************/
void blePeripheralDisconnectHandler(BLEDevice central) {
  Serial.print("Déconnecté du central\n");
  central_connected = 0;
  // Re-initialise le compteur des messages envoyés
  message_number = 0;
}

/****************************************************************/
/* Gestionnaire de l'évènement d'écriture par le central dans   */
/* la caractéristique de réception Rx                           */
/****************************************************************/
void rxCharacteristicWritten(BLEDevice central, BLECharacteristic characteristic) {
  // On lit ce que le central a écrit dans la caractéristique Rx
  characteristic.readValue(CHAR_BUFFER, MAX_BLE_CHAR);
  Serial.print("Données reçues du central : ");
  Serial.println(CHAR_BUFFER);
}

/****************************************************************/
/* Démarrage de l'interruption périodique du timer 1            */
/* Polling BLE                                                  */
/****************************************************************/
void init_Timer_BLE(void) {
  Timer1->setOverflow(BLE_POLLING_HZ, HERTZ_FORMAT);
  Timer1->attachInterrupt(Timer1_ISR);
  Timer1->resume();
  Serial.println("IT timer 1 active (polling BLE)");
}

/* Fonction de service de l'interruption de dépassement du timer 1 */
void Timer1_ISR(void) {
  // Gestion des évènements BLE
  BLE.poll();
}

/****************************************************************/
/* Fonction de service de l'interruption attachée à USER_BTN.   */
/****************************************************************/
void SendMsgISR(void) {
  // Signale qu'un message doit être envoyé au central connecté
  pending_message = 1;
}

```

## Mise en œuvre du périphérique

Après avoir vérifié que l'IDE Arduino est correctement configurée, notamment qu'elle est connectée au port COM attribué au ST-LINK de votre carte (sous Windows), cliquez sur "Téléverser".

Les logs **dans le terminal série de l'IDE Arduino** devraient ressembler à ceci :

```console
PERIPHERIQUE BLE NUS
Mon identifiant est device_001
Attente de connexion au central pair ...
IT timer 1 active (polling BLE)
```
Le périphérique a démarré, et il attend la connexion d'un central.<br>
Vous pouvez créer facilement un central en chargeant sur votre smartphone l'application [nRF Connect for Mobile (versions Android)](https://play.google.com/store/apps/details?id=no.nordicsemi.android.mcp&hl=fr&gl=US&pli=1) ou [nRF Connect for Mobile (versions Apple)](https://apps.apple.com/ch/app/nrf-connect-for-mobile/id1054362403?l=fr) de [Nordic Semiconductor ASA](https://infocenter.nordicsemi.com/index.jsp).<br>
Voici ce que cela donne lorsqu'un central NRF Connect est effectivement connecté au périphérique :

```console
PERIPHERIQUE BLE NUS
Mon identifiant est device_001
Attente de connexion au central pair ...
IT timer 1 active (polling BLE)
Connecté au central 5c:86:cd:9a:17:d2
Données reçues du central : Test 
Données envoyées au central : Hello 0
```

On a envoyé le message "Test" au périphérique, depuis le smartphone, puis on a pressé le bouton USER du périphérique pour envoyer "Hello 0" au smartphone.


# Deuxième partie : Création du central

Nous allons à présent travailler sur le sketch qui gère le central. Notre exemple va consister à envoyer un message vers un éventuel périphérique connecté chaque fois que l'on appuie sur le bouton "USER" de la carte de prototypage sélectionnée.

## Matériel requis

Nos sketchs proposent plusieurs options pour le matériel permettant de communiquer en BLE, qui pourra être constitué au choix :

- D'une carte [NUCLEO-WB55](../../Kit/nucleo_wb55rg) ou bien
- D'une carte [NUCLEO-L476RG](../../Kit/nucleo_l476rg) équipée soit d'un [shield X-NUCLEO-IDB05A1](https://www.st.com/en/ecosystems/x-nucleo-idb05a1.html) soit d'un [shield X-NUCLEO-IDB05A2](https://www.st.com/en/ecosystems/x-nucleo-idb05a2.html) ou bien
- D'une carte [DISCOVERY B-L475E-IOT01A1](https://www.st.com/en/evaluation-tools/b-l475e-iot01a.html) ou bien
- D'une carte [DISCOVERY B-L4S5I-IOT01A](https://www.st.com/en/evaluation-tools/b-l4s5i-iot01a.html).

Pour indiquer au compilateur quel matériel vous utilisez effectivement, prenez soin **d'attribuer la bonne valeur à la directive de préprocesseur *#define HW_CONFIG*** dans le sketch.

**Attention**, selon le matériel BLE que vous utilisez, [des mises à jour de firmwares peuvent être nécessaires](https://github.com/stm32duino/STM32duinoBLE) ! Pour la NUCLEO-WB55, la procédure est expliquée [ici](../../tools/cubeprog/cube_prog_firmware_ble_hci).

## Bibliothèque(s) requise(s)

La gestion du BLE par STM32duino est assurée par **la bibliothèque STM32duinoBLE** disponible [ici](https://github.com/stm32duino/STM32duinoBLE).

## Le sketch Arduino pour le central

>> Le sketch pour cet exemple (et tous les autres) peut être téléchargé [**en cliquant ici**](../../../assets/Sketch/TUTOS.zip).

Lancez l'IDE Arduino, ouvrez un nouveau sketch vide et copiez-y le code qui suit :

```c
/*
  Objet du sketch : 
  Implémentation d'un central BLE Nordic UART Service (NUS)
  Ce central utilise deux caractéristiques exposées par son périphérique pair :
  - Une dans laquelle il peut écrire des chaînes de caractères de taille maximum MAX_BLE_CHAR.
    Le périphérique surveille les évènements d'écriture dans RX et peut réagir lorsque le central
    la modifie.
  - Une qui lui est notifiée, TX, qui contient des chaînes de caractères de taille maximum 
    MAX_BLE_CHAR et à laquelle il pourra s'abonner pour être averti de ses modifications.
  Le couple RX/TX permet donc au central et à son périphérique d'échanger des messages à la façon d'un
  port série.

  Matériel utilisé :
  Trois configurations sont possibles, selon la valeur assignée à la directive de préprocesseur HW_CONFIG :
  - Si HW_CONFIG = 1 : Le script sera compilé pour une carte DISCOVERY B-L475E-IOT01A1 ou B-L4S5I-IOT01A
  - Si HW_CONFIG = 2 : Le script sera compilé pour une carte NUCLEO-WB55
  - Si HW_CONFIG = 3 : Le script sera compilé pour une carte NUCLEO-L476RG et un shield IDB05A1 ou IDB05A2
  Le central et le périphérique ne sont pas tenus d'avoir la même configuration matérielle.

  Utilisation :
  Compilez le script, chargez le dans votre carte et observez les messages sur le terminal série de l'IDE
  Arduino. S'ils parviennent à s'identifier (voir ci-après), le central et son périphérique devraient
  se connecter automatiquement.
  Pour envoyer un message au périphérique pair (lorsqu'il est connecté), appuyez sur le bouton USER 
  (varie selon le matériel que vous utilisez).

  IMPORTANT : Le central ne pourra se connecter qu'à un périphérique qui expose le nom PAIR_DEVICE_ID.
  Il faut donc que PAIR_DEVICE_ID, tel que défini dans le sketch du central (BLE_NUS_Central.ino) et
  DEVICE_ID, tel que défini dans le sketch du périphérique pair (BLE_NUS_Peripheral.ino) soient 
  identiques pour que la connexion s'établisse.
  Si d'autres potentiels périphériques BLE NUS (ou exposant d'autres services) sont présents, le 
  central ne s'y connectera pas, il persistera en mode scan jusqu'à ce qu'il découvre sont périphérique
  pair et s'y soit connecté.
*/

// Identifiant du périphérique auquel le central est autorisé à se connecter
#define PAIR_DEVICE_ID "device_001"

// Débit du port série du ST-LINK
#define ST_LINK_BAUDRATE (115200)

// Broche du bouton
#define BUTTON_PIN USER_BTN

// On gère le polling BLE avec un timer
// Instanciation de gestion de l'interruption de dépassement du timer 1
TIM_TypeDef *Instance1 = TIM1;
HardwareTimer *Timer1 = new HardwareTimer(Instance1);
// Fréquence de polling du BLE assurée par le timer 1
#define BLE_POLLING_HZ (100)

/* Définitions pour le BLE */

// Bibliothèque disponible ici : https://github.com/stm32duino/STM32duinoBLE
#include <STM32duinoBLE.h>

// Instanciation du BLE

// Sélectionne la configuration matérielle parmi celles disponibles
#define HW_CONFIG 3

#if HW_CONFIG == 1
/* Carte B-L475E-IOT01A1 ou B_L4S5I_IOT01A */
SPIClass SpiHCI(PC12, PC11, PC10);
HCISpiTransportClass HCISpiTransport(SpiHCI, SPBTLE_RF, PD13, PE6, PA8, 8000000, SPI_MODE0);
BLELocalDevice BLEObj(&HCISpiTransport);
BLELocalDevice &BLE = BLEObj;
#elif HW_CONFIG == 2
/* Carte NUCLEO WB55 */
HCISharedMemTransportClass HCISharedMemTransport;
BLELocalDevice BLEObj(&HCISharedMemTransport);
BLELocalDevice &BLE = BLEObj;
#elif HW_CONFIG == 3
/* Carte NUCLEO L476RG et shield IDB05A1/A2 avec la broche SPI clock sur D3 */
// Attention, les lignes ci-dessous informent que les broches Arduino :
// A0, A1, D3, D7, D11 et D12 sont utilisées par le shield IDB05A1 et donc
// ne sont plus disponibles pour y connecter autre chose !
SPIClass SpiHCI(D11, D12, D3);
HCISpiTransportClass HCISpiTransport(SpiHCI, SPBTLE_RF, A1, A0, D7, 8000000, SPI_MODE0);
BLELocalDevice BLEObj(&HCISpiTransport);
BLELocalDevice &BLE = BLEObj;
#endif

// Déclarations des UUID pour le service BLE NUS et ses caractéristiques
// Rx (réception) et Tx (émission)
// Ces valeurs sont standardisées, elles permettent notamment à des applications
// central BLE pour smartphones (telles que NRF connect) de reconnaître un périphérique
// BLE NUS.
#define BLE_UART_UUID "6E400001-B5A3-F393-E0A9-E50E24DCCA9E"
#define BLE_UART_RX_UUID "6E400002-B5A3-F393-E0A9-E50E24DCCA9E"
#define BLE_UART_TX_UUID "6E400003-B5A3-F393-E0A9-E50E24DCCA9E"

// Caractéristique Rx
BLECharacteristic rxCharacteristic;
// Caractéristique Tx
BLECharacteristic txCharacteristic;

// Taille maximum du buffer de messages (en nombre de caractères)
#define MAX_BLE_CHAR (128)
// Buffer de messages
char CHAR_BUFFER[MAX_BLE_CHAR];

// Compteur utilisé pour numéroter les messages envoyés au périphérique
uint8_t message_number = 0;

// Est-ce qu'un message doit-être envoyé au périphérique ?
volatile uint8_t pending_message = 0;

/****************************************************************/
/* Initialisations                                              */
/****************************************************************/
void setup() {

  // Initialisation du port série du ST-LINK
  Serial.begin(ST_LINK_BAUDRATE);
  while (!Serial) {}

  Serial.println("CENTRAL BLE NUS");
  Serial.println("Mon périphérique pair est " + String(PAIR_DEVICE_ID));

  // Initialisation du bouton utilisateur et gestion de celui-ci par
  // interruptions
  pinMode(BUTTON_PIN, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(BUTTON_PIN), SendMsgISR, FALLING);

  // Initialisation du BLE
  if (!BLE.begin()) {
    Serial.println("Echec de démarrage du BLE !");
    while (1) {};
  }

  // Initialisation de l'interruption périodique du timer 1 pour
  // gérér les évènements BLE.
  init_Timer_BLE();

  // Démarre le scan à la recherche du périphérique pair
  scan_for_pair_device();
}

/****************************************************************/
/* Boucle principale                                            */
/****************************************************************/
void loop() {

  // Vérifie si un périphérique a été découvert
  BLEDevice peripheral = BLE.available();

  // Si un périphérique a été découvert ...
  if (peripheral) {

    // Si le périphérique et celui recherché
    if (peripheral.localName() == PAIR_DEVICE_ID) {
      // Affiche son adresse MAC, son nom et le service qu'il publie
      Serial.print("Trouvé : ");
      Serial.print(peripheral.address());
      Serial.print(" '");
      Serial.print(PAIR_DEVICE_ID);
      Serial.print("' ");
      Serial.print(peripheral.advertisedServiceUuid());
      Serial.println();
    } else {
      return;
    }

    // Arrête le scan
    stop_scan();

    // Connexion au périphérique pair et échange de messages
    // avec lui
    connexion_management(peripheral);

    // Le périphérique pair est déconnecté, relance le scan
    scan_for_pair_device();
  }
}

/****************************************************************/
/* Connexion au périphérique pair et échange de messages avec   */
/* lui.                                                         */
/****************************************************************/
void connexion_management(BLEDevice peripheral) {

  // Connexion au périphérique
  Serial.print("Connexion ... ");

  if (peripheral.connect()) {
    Serial.println("succès !");
  } else {
    Serial.println("échec !");
    return;
  }

  // Recherche les attributs du périphérique
  Serial.print("Recherche des attributs ... ");
  if (peripheral.discoverAttributes()) {
    Serial.println("succès !");
  } else {
    Serial.println("échec !");
    peripheral.disconnect();
    return;
  }

  // Recherche la caractéristique RX du périphérique
  rxCharacteristic = peripheral.characteristic(BLE_UART_RX_UUID);
  if (!rxCharacteristic) {
    Serial.println("Le périphérique n'a pas de caractéristique RX !");
    peripheral.disconnect();
    return;
  } else if (!rxCharacteristic.canWrite()) {
    Serial.println("La caractéristique RX du périphérique ne peut pas être écrite");
    peripheral.disconnect();
    return;
  }

  // Recherche la caractéristique TX du périphérique
  txCharacteristic = peripheral.characteristic(BLE_UART_TX_UUID);
  if (!txCharacteristic) {
    Serial.println("Le périphérique n'a pas de caractéristique TX !");
    peripheral.disconnect();
    return;
  }

  // Abonnement aux évènements de mise à jour de TX
  txCharacteristic.subscribe();
  txCharacteristic.setEventHandler(BLEUpdated, bleTXupdated);
  Serial.println("Central abonné aux notifications du service TX");

  // Aussi longtemps que le périphérique est connecté ...
  while (peripheral.connected()) {

    // Vérifie s'il n'y a pas des messages en provenance de ce celui-ci
    // Inutile ici car entièrement géré par bleTXupdated !
    // if (txCharacteristic.valueUpdated()) {
    //   // Si c'est le cas, lis la caractéristique RX
    //   txCharacteristic.readValue(CHAR_BUFFER, MAX_BLE_CHAR);
    //   Serial.print("Données reçues du périphérique : ");
    //   Serial.println(CHAR_BUFFER);
    // }

    // Si un message doit être envoyé au périphérique
    if (pending_message) {

      // On construit le message
      String BLE_RX = "Hello " + String(message_number++);

      // On le convertit en un tableau de caractères
      BLE_RX.toCharArray(CHAR_BUFFER, BLE_RX.length() + 1);

      // On l'envoie au périphérique connecté en écrivant dans sa
      // caractéristique RX
      rxCharacteristic.writeValue(CHAR_BUFFER);

      Serial.print("Données envoyées au périphérique : ");
      Serial.println(CHAR_BUFFER);

      // On réarme pour la réception du prochain message
      pending_message = 0;
    }
  }

  // Re-initialise le compteur de messages envoyés
  message_number = 0;

  // Signale que le périphérique est déconnecté
  Serial.println("Périphérique déconnecté");
}

/*****************************************************************/
/* Cet évènement survient chaque fois que le périphérique écrit  */
/* dans la caractéristique TX                                    */
/*****************************************************************/
void bleTXupdated(BLEDevice peripheral, BLECharacteristic characteristic) {

  // Lecture de la valeur écrite dans TX par le périphérique
  characteristic.readValue(CHAR_BUFFER, MAX_BLE_CHAR);
  Serial.print("Données reçues du périphérique : ");
  Serial.println(CHAR_BUFFER);

}

/*****************************************************************/
/* Démarre le scan à la recherche du périphérique PAIR_DEVICE_ID */
/*****************************************************************/
void scan_for_pair_device(void) {
  int ret = 1;
  do {
    //ret = BLE.scanForUuid(BLE_UART_UUID);
    ret = BLE.scanForName(PAIR_DEVICE_ID);
    if (!ret) {
      BLE.end();
      BLE.begin();
    }
  } while (!ret);
}

/*****************************************************************/
/* Arrête le scan pour la recherche de périphériques             */
/*****************************************************************/
void stop_scan(void) {
  int ret = 1;
  do {
    ret = BLE.stopScan();
    if (!ret) {
      BLE.end();
      BLE.begin();
    }
  } while (!ret);
}

/****************************************************************/
/* Fonction de service de l'interruption attachée à USER_BTN.   */
/****************************************************************/
void SendMsgISR(void) {
  // Signale qu'un message doit être envoyé au périphérique pair
  pending_message = 1;
}


/****************************************************************/
/* Démarrage de l'interruption périodique du timer 1            */
/* Polling BLE                                                  */
/****************************************************************/
void init_Timer_BLE(void) {
  Timer1->setOverflow(BLE_POLLING_HZ, HERTZ_FORMAT);
  Timer1->attachInterrupt(Timer1_ISR);
  Timer1->resume();
  Serial.println("IT timer 1 active (polling BLE)");
}

/* Fonction de service de l'interruption de dépassement du timer 1 */
void Timer1_ISR(void) {
  // Gestion des évènements BLE
  BLE.poll();
}
```

## Mise en œuvre du central

Après avoir vérifié que l'IDE Arduino est correctement configurée, notamment qu'elle est connectée au port COM attribué au ST-LINK de votre carte (sous Windows), cliquez sur "Téléverser". Branchez également sur une alimentation le p^ériphérique réalisé à l'étape précédente.

Les logs, pour le central, **dans le terminal série de l'IDE Arduino** devraient ressembler à ceci :

```console
CENTRAL BLE NUS
Mon périphérique pair est device_001
IT timer 1 active (polling BLE)
Trouvé : f9:ed:e7:49:cc:2f 'device_001' 6e400001-b5a3-f393-e0a9-e50e24dcca9e
Connexion ... succès !
Recherche des attributs ... succès !
Central abonné aux notifications du service TX
```

Pour le périphérique, vous devriez observer ceci :

```console
PERIPHERIQUE BLE NUS
Mon identifiant est device_001
Attente de connexion au central pair ...
IT timer 1 active (polling BLE)
Connecté au central d3:77:8d:dd:67:22
```
La connexion est effectivement établie.
Notez bien que, pour éviter que n'importe quel central NUS se connecte à n'importe quel périphérique NUS, nous avons précisé dans les sketchs que le périphérique pair de ce central est *device_001*. Pour établir une connexion entre deux autres systèmes, il conviendra d'ajuster dans les sketchs les définitions de *#define DEVICE_ID* (côté périphérique) et *#define PAIR_DEVICE_ID* (côté central) afin qu'elles aient **la même valeur** (dans notre cas *device_001*).

Vous pouvez à présent appuyer sur les boutions USER des deux systèmes et confirmer qu'ils communiquent comme prévu.

 