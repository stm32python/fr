---
title: Identification radiofréquence avec une X-NUCLEO-NFC05A1
description: Identification radiofréquence avec une X-NUCLEO-NFC05A1 sous STM32duino
---

# Identification radiofréquence (RFID) avec une X-NUCLEO-NFC05A1

Ce tutoriel met en œuvre [la technologie de radio-identification (RFID)](https://fr.wikipedia.org/wiki/Radio-identification) de STMicroelectronics, à laide de la carte d'extension [X-NUCLEO-NFC05A1](https://www.st.com/en/ecosystems/x-nucleo-nfc05a1.html).

L'utilisateur dispose d'un badge porteur d'un numéro unique (associé à son nom). Lorsque ce badge est lu par la X-NUCLEO-NFC05A1, le programme embarqué interroge une petite base de données.  Si le badge y est enregistré, l'application affichera sur le port série du ST-LINK l'identité de son possesseur.

## Matériel requis

La réalisation de ce système nécessitera :

1. Un lecteur [RFID X-NUCLEO-NFC05A1](https://www.st.com/en/ecosystems/x-nucleo-nfc05a1.html).
2. Plusieurs [badges RFID de type ISO/IEC 14443A/B (13.56 MHz)](https://boutique.semageek.com/fr/248-tag-rfid-mifare-1356mhz-3007245738481.html). En fait, bien d'autres types de badges peuvent être lus, c'est notamment pour cette raison que le programme présenté par la suite est un peu complexe.
3. Une carte programmable à microcontrôleur [NUCLEO-L476RG](https://www.st.com/en/evaluation-tools/nucleo-l476rg.html) pour piloter l'ensemble. N'importe quelle autre carte NUCLEO ferait l'affaire à condition de modifier les broches si nécessaire. 

**La carte d'extension X-NUCLEO-NFC05A1 positionnée au-dessus d'une carte NUCLEO 64 :**

<br>
<div align="left">
<img alt="X-NUCLEO-NFC05A1" src="images/x-nucleo-nfc05a1.jpg" width="500px">
</div>
<br>

>> Credit image : IndiaMart

## Bibliothèque(s) requise(s) pour ce sketch

Trois bibliothèques sont nécessaires pour cet exemple :

- La bibliothèque [STM32duino X-NUCLEO-NFC05A1](https://github.com/stm32duino/X-NUCLEO-NFC05A1) de STMicroelectronics
- La bibliothèque [STM32duino NFC-RFAL](https://github.com/stm32duino/NFC-RFAL) de STMicroelectronics
- La bibliothèque [STM32duino ST25R3911B](https://github.com/stm32duino/ST25R3911B) de STMicroelectronics

Les deux dernières sont des dépendances de la première et, à ce titre, installées automatiquement avec elle. 

## Le sketch Arduino


>> Le sketch pour cet exemple (et tous les autres) peut être téléchargé [**en cliquant ici**](../../../assets/Sketch/TUTOS.zip). Il se trouve dans le dossier *Identification radiofréquence X-NUCLEO-NFC05A1\NFC_alone\\*.

Le sketch que nous proposons est adapté de l'exemple *"Hello World"* de la bibliothèque *STM32duino X-NUCLEO-NFC05A1*. Il est composé de deux fichiers :

- Le sketch arduino proprement dit, *nfc_st25r3911bNFC_alone.ino*, qui contient le code utilisateur ;
- Un fichier inclus *nfc_demo.h* qui contient une grande partie du code source de l'exemple *"Hello World"* afin de rendre le programme de *NFC_alone.ino* plus facilement compréhensible.

Bien sûr, nous ne reproduisons pas ici tout le code contenu dans *nfc_demo.h*, qui mériterait d'être simplifié si vous êtes motivé pour développer une application sur la base de notre tutoriel. 

Ce sketch illustre bien la programmation et l'utilisation d'une **machine à états finis** (avec les *switch(state) ...case: ...*).
C'est une technique de programmation très utilisée qui permet d'écrire un programme qui réalise des actions déterminées en fonction des évènements qui se présentent.

Lancez l'IDE Arduino, ouvrez un nouveau sketch vide et copiez-y le code qui suit :

```c
/*------------------------------------------------------------------------------------------------------------*/
// Objet(s) du sketch :
//  - Lecture d'un badge sans contact NFC 13,56 MHz sur la base de l'exemple X_NUCLEO_NFC05A1_HelloWorld de
//    la bibliothèque https://github.com/stm32duino/X-NUCLEO-NFC05A1 (Auteurs : AST/SRA/STMicroelectronics)
//  - L'identifiant unique (UID) du badge est associé à une petite base de données d'utilisateurs.
//    Lorsqu'un badge est lu, son UID est recherché dans cette base et l'identité de son possesseur
//    (fictif) est affichée sur le terminal série de l'IDE Arduino.
/*-----------------------------------------------------------------------------------------------------------*/
// Matériel & brochage :
//	- Une carte NUCLEO-L476RG
//	- Une carte d'extension X-NUCLEO-NFC05A1
//  - Quelques badges NFC 13.56 MHz (six dans notre exemple)
/*-----------------------------------------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------*/
/* Variables globales, includes, defines                                      */
/*----------------------------------------------------------------------------*/

// Fichier "fourre-tout" qui contient l'essentiel du code de "Hello World"
#include <nfc_demo.h>

// Nombre d'utilisateurs enregistrés dans la base de données de badges/tags
#define NBUSERS (6)

// Structure des enregistrements de la base de données des badges/tags
typedef struct
{
  String tag_uid;    // Identifiant unique du tag
  String lastname;   // Nom du possesseur légitime du tag
  String firstname;  // Prénom du possesseur légitime du tag
  uint8_t allowed;   // A t'il le droit de générer une action ?
} user_record;

// Initialisation de la base de données d'utilisateurs (tags) et de leurs droits
// NB : Vous devrez modifier les UID afin qu'ils correspondent aux badges dont
// vous disposez.
user_record user_db[NBUSERS] = {
  // UID du badge, Nom, Prénom, Niveau autorisation
  { "04262CB22E7380", "DUPONT", "Jean-Kevin", 0 },
  { "CAD17924", "HENRI", "Jean", 1 },
  { "6AE6CF25", "CUST", "Alexandre-Bonaparte", 1 },
  { "CA076E25", "SANSEL", "Géraldine", 0 },
  { "5A056C25", "DE-QUATRE", "Lanfeust", 1 },
  { "AA496B25", "VALJEAN", "Jean", 1 }
};

// Etats possibles de notre machine d'états
#define DEMO_ST_NOTINIT 0          // Non initialisée
#define DEMO_ST_START_DISCOVERY 1  // Démarre la recherche de badges/tags
#define DEMO_ST_DISCOVERY 2        // Un bagde / tag a été découvert
#define DEMO_ST_ACTION 3           // On agit suite à la lecture d'un badge

// Etat courant de notre machine, non initialisée au démarrage
static uint8_t state = DEMO_ST_NOTINIT;

/*----------------------------------------------------------------------------*/
/* Paramètres de démarrage                                                    */
/*----------------------------------------------------------------------------*/
void setup() {

  // Initialisation du port série du ST-LINK
  Serial.begin(115200);

  // Initialisation du lecteur RFID
  init_NFC05A1();

  // Prochain état de la machine d'états
  state = DEMO_ST_START_DISCOVERY;
}

/*----------------------------------------------------------------------------*/
/* Boucle principale                                                          */
/*----------------------------------------------------------------------------*/
void loop() {

  // Allocations et variables locales nécessaires à la lecture des badges/tags
  static rfalNfcDevice *nfcDevice;
  rfalNfcaSensRes sensRes;
  rfalNfcaSelRes selRes;
  rfalNfcbSensbRes sensbRes;
  uint8_t sensbResLen;
  uint8_t devCnt = 0;
  rfalFeliCaPollRes cardList[1];
  uint8_t collisions = 0U;
  rfalNfcfSensfRes *sensfRes;
  rfalNfcvInventoryRes invRes;
  uint16_t rcvdLen;

  // Initilise le lecteur de tags
  rfal_nfc.rfalNfcWorker();

  // Gère le bouton utilisateur et l'affichage du menu
  nfc_manage_usr_interface(state, DEMO_ST_START_DISCOVERY);

  // Branchement de la machine d'états ...
  switch (state) {

    // Si l'état courant est DEMO_ST_START_DISCOVERY ...
    case DEMO_ST_START_DISCOVERY:

      // Active le mode écoute du lecteur de tags
      rfal_nfc.rfalNfcDeactivate(false);
      rfal_nfc.rfalNfcDiscover(&discParam);
      ledsOff(); // Etient les LED

      // A la prochaine itération, passe dans l'état DEMO_ST_DISCOVERY
      state = DEMO_ST_DISCOVERY;
      break;

    // Si recherche de badges / tags
    case DEMO_ST_DISCOVERY:

      if (rfalNfcIsDevActivated(rfal_nfc.rfalNfcGetState())) {

        // Effacement du dernier UID de tag lu
        Tag_UID = "";

        // Validation de la présence du badge / tag
        rfal_nfc.rfalNfcGetActiveDevice(&nfcDevice);
        ledsOff(); // Etient les LED
        delay(50); // Temporisation 50 ms

        // Sélectionne la fonction de lecture selon le type de tag identifié
        switch (nfcDevice->type) {

          // Lecture du contenu d'un tag de type NFCA
          case RFAL_NFC_LISTEN_TYPE_NFCA:

            Read_NFCA(nfcDevice, sensRes, selRes);
            break;

            // Lecture du contenu d'un tag de type NFCB
          case RFAL_NFC_LISTEN_TYPE_NFCB:

            Read_NFCB(nfcDevice, sensbRes, sensbResLen, selRes);
            break;

            // Lecture du contenu d'un tag de type NFCF
          case RFAL_NFC_LISTEN_TYPE_NFCF:

            Read_NFCF(nfcDevice, devCnt, cardList, sensfRes, collisions);
            break;

            // Lecture du contenu d'un tag de type NFCV
          case RFAL_NFC_LISTEN_TYPE_NFCV:

            Read_NFCV(nfcDevice, invRes, rcvdLen);
            break;

            // Lecture du contenu d'un tag de type ST25TB
          case RFAL_NFC_LISTEN_TYPE_ST25TB:

            Read_ST25TB(nfcDevice);
            break;

            // Lecture contenu tag type AP2P
          case RFAL_NFC_LISTEN_TYPE_AP2P:

            Read_AP2P(nfcDevice);
            break;

          default:
            break;
        }

        // Désactive la recherche de badges/tags
        rfal_nfc.rfalNfcDeactivate(false);
        delay(500); // Temporisation une demi seconde

        // J'ai lu un tag, mon prochain état sera une action en conséquence
        state = DEMO_ST_ACTION;
      }
      break;

    // Si je dois réaliser une action suite à la lecture d'un tag...
    case DEMO_ST_ACTION:

      // Je place ici mon code pour cette action
      tag_action();

      // Prochain état : recherche de tag par le lecteur
      state = DEMO_ST_START_DISCOVERY;
      break;

    // Si l'état courant est DEMO_ST_NOTINIT...
    case DEMO_ST_NOTINIT:
      // Ne fais rien
      break;

    // Si l'état courant est autre chose...
    default:
      // Ne fais rien
      break;
  }
}

/*----------------------------------------------------------------------------*/
/* Recherche l'identifiant du tag dans la base de données.                    */
/* Renvoie son index dans la base s'il est trouvé et -1 dans le cas contraire.*/
/* NB : Cette méthode est pertinente pour une base de données ayant peu       */
/* d'éléments. Si la base devient plus grande, il vaudra mieux la trier dans  */
/* Setup() et utiliser ensuite un algorithme de type "recherche binaire".     */
/* L'option de placer la base dans une mémoire externe, ou encore ailleurs    */
/* sur un réseau prend tout son sens si la base contient beaucoup             */
/* d'enregistrements et/ou si elle doit être sécurisée.                       */
/*----------------------------------------------------------------------------*/
int find_tag_uid() {

  // Le compteur i est déclaré en uint8_t car NBUSERS est plus petit
  // que 2^8 - 1 = 255. On doit s'assurer que nos variables ont un type qui
  // permet de mémoriser les plus grandes valeurs numériques utilisées.

  for (uint8_t i = 0; i < NBUSERS; i++) {
    if (user_db[i].tag_uid == Tag_UID) {
      return i;
    }
  }
  return -1;
}

/*----------------------------------------------------------------------------*/
/* Réalise les actions décidées lorsqu'un tag listé dans la base de données   */
/* est identifié par le lecteur.                                              */
/* En l'occurence affiche l'identité fictive du possesseur présumé du badge.  */
/*----------------------------------------------------------------------------*/
void tag_action() {

  // Recherche l'identifiant du tag dans la base de données
  int tag_index;

  tag_index = find_tag_uid();

  // Si le badge/tag est enregistré dans la base de données
  if (tag_index > -1) {

    // Affiche l'UID, le nom, le prénom et indique si l'utilisateur est
    // autorisé ou pas.
    Serial.print("\nUID : ");
    Serial.println(user_db[tag_index].tag_uid);
    Serial.print("Prénom : ");
    Serial.println(user_db[tag_index].firstname);
    Serial.print("Nom : ");
    Serial.println(user_db[tag_index].lastname);
    Serial.print("Droits : ");
    if (user_db[tag_index].allowed) {
      Serial.println("Autorisé");
    } else {
      Serial.println("Interdit");
    }
  }
}
```

Après avoir vérifié que l'IDE Arduino est correctement configurée, notamment qu'elle est connectée au port COM attribué au ST-LINK de votre carte (sous Windows), cliquez sur "Téléverser".

## Affichage sur le terminal série et commentaires

Si tout s'est passé correctement vous devriez observer cette séquence de messages sur le terminal série de l'IDE Arduino lorsque vous posez puis retirez un badge (enregistré dans la base de données, dans notre cas celui de Jean-Kevin) sur le lecteur :

```console
Welcome to X-NUCLEO-NFC05A1
Use the User button to cycle among the different modes:
1. Tap a tag to read its content
2. Present a tag to write a Text record
3. Present a tag to write a URI record and an Android Application record
4. Present an ST tag to format
In Write or Format mode (menu 2, 3 or 4), the demo returns to Read mode (menu 1) if no tag detected after 10 seconds

1. Tap a tag to read its content
NFCA Passive ISO-DEP device found. UID: 04262CB22E7380
NDEF NOT DETECTED (ndefPollerNdefDetect returns 5)
Operation completed
Tag must be removed from the field

UID : 04262CB22E7380
Prénom : Jean-Kevin
Nom : DUPONT
Droits : Interdit
1. Tap a tag to read its content
```

Le terminal signale que des appuis successifs sur le bouton utilisateur permettent de sélectionner des fonctions **d'écriture sur les badges/tags** à condition que ceux-ci le supportent. C'est une fonction présente dans la démo d'origine, *Hello World*, que nous avons *cachée* dans le fichier *nfc_demo.h*. Sous réserve de disposer des tags adéquats, vous pourrez la tester.<br>
Dans notre adaptation, seule nous intéresse la lecture de l'UID de tags et le déclenchement d'une action dont le code doit être contenu dans la fonction *tag_action()*, que vous pourrez modifier sans difficultés.

## Pour aller plus loin

Vous trouverez dans **[l'archive téléchargeable](SKETCHS.zip)**, une application plus développée du sketch qui précède, dans le dossier *Identification radiofréquence X-NUCLEO-NFC05A1\NFC_and_DC_motor\\*.
Ce sketch n'est pas reproduit ici car il est relativement long.

L'objectif est de simuler un mécanisme d'ouverture / fermeture sécurisé, ou encore un mécanisme de distributeur contrôlé par badge. Plus précisément :

- Lorsqu'un badge / tag NFC est passé sur le lecteur, on recherche si son possesseur est enregistré dans la petite base de données en mémoire flash. Chaque utilisateur n'est autorisé à "badger" et activer le mécanisme qu'une seule fois après un reset du système.

- Si c'est le cas, un petit moteur à courant continu est démarré pendant *TIME_MOTOR_RUNNING_MS* dans le sens "avant", puis remis en position de départ, simulant l'ouverture et la fermeture d'une serrure ou encore le mouvement d'un bras qui pousse un objet et revient ensuite à sa position initiale. La gestion du moteur à courant continu impose l'utilisation d'une **carte de contrôle moteur, [un module Grove I2C Motor Drive (TB6612FNG)](https://wiki.seeedstudio.com/Grove-I2C_Motor_Driver-TB6612FNG/)** qui permet d'inverser sur commande du petit moteur à courant continu.

Ce nouvel exemple apporte également quelques raffinements, comme la gestion [d'un écran LCD Grove 16x2 ](lcd_16x2.md) et la possibilité de modifier la base de données d'utilisateurs autorisés dans la mémoire flash du microcontrôleur, sur la base de [cet autre tutoriel](flash).
