---
title: Exercice avec le capteur de distance à ultrason Grove en C/C++ pour Stm32duino
description: Exercice avec le capteur de distance à ultrason Grove en C/C++ pour Stm32duino
---
# Exercice avec le capteur de distance à ultrason Grove en C/C++ pour Stm32duino

**- Prérequis :**


**- Le capteur d'ultrasons (Ultrasonic Ranger):**

Ce capteur permet de mesure des distances précisement via des ultrasons. Un transducteur à ultrasons émet des ultrasons qui rebondissent ensuite sur les parois, un deuxième transducteur récepteur reçoit  ces ondes. Il est alors possible de calculer la distances parcourue par les ultrasons. On peut mesurer des distances de quelques centimètres à un ou deux mètres.
Ce capteur est semblable aux capteurs de recul des voitures.


![Image](images/11_ultrason/capteur_ultrason.png)

 *T --> transducteur émetteur (transmetteur)*  |  *R --> transducteur récepteur*

> Crédit image : [Seeedstudio](http://wiki.seeedstudio.com/Grove_System/)
