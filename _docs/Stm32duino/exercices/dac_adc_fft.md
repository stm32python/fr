---
title: Générateur et analyseur de signaux, transformation de Fourier discrète
description: Création d'un générateur de signaux modulés en tension et d'un analyseur de ces signaux, utilisation de la transformation de Fourier discrète (DFT) pour analyse spectrale.
---

# Générateur et analyseur de signaux, transformation de Fourier discrète

Ce tutoriel montre comment construire un **analyseur de signaux** et un **générateur de signaux carrés périodiques** avec Arduino pour STM32 en utilisant des cartes NUCLEO-L476RG. Pour tout ce qui concerne les explications (superficielles) sur les séries de Fourier, les DFT et FFT, les méthodes spectrales ... nous vous renvoyons à [ce tutoriel STM32python](../../Micropython/grove/dac_adc_dft) qui traite exactement du même sujet.

**Ce tutoriel est constitué de 3 parties** :

- **Partie 1 : Conception du générateur de signaux carrés**<br>
**Le générateur de signaux** va produire [**un signal carré**](https://fr.wikipedia.org/wiki/Signal_carr%C3%A9) à l'aide de son [**convertisseur numérique-analogique (DAC)**](../startl476/dac), en calculant sa [**série de Fourier**](https://fr.wikipedia.org/wiki/S%C3%A9rie_de_Fourier) jusqu'à un terme donné. Pour construire ce générateur, nous utiliserons une [carte NUCLEO-L476RG](../../Kit/nucleo_l476).
Nous le testerons avec [**un analyseur logique Picoscope 2205A**](https://www.picotech.com/oscilloscope/2000/picoscope-2000-overview).

- **Partie 2 : Conception de l'analyseur de signaux**<br>
**L'analyseur de signaux** va capturer un échantillon d'un signal modulé en tension à l'aide de son [**convertisseur analogique-numérique (ADC)**](../startl476/adc), puis calculera sa [**DFT**](https://fr.wikipedia.org/wiki/Transformation_de_Fourier_discr%C3%A8te) pour en déduire [**son spectre**](https://fr.wikipedia.org/wiki/Analyse_spectrale). Pour construire l'analyseur, nous utiliserons une deuxième carte NUCLEO-L476RG.
 
- **Partie 3 : Connexion de l'analyseur de signaux au générateur de signaux**<br>
On vérifiera que l'analyseur détecte bien les différentes fréquences caractéristiques, [**fondamentale** et **harmoniques**](https://fr.wikipedia.org/wiki/Harmonique_(musique)), du signal délivré par le générateur.

## Partie 1 : Conception du générateur de signaux carrés

Vous trouverez ici les indications pour construire et tester le générateur de signaux carrés.

### Le matériel

1. Une [carte NUCLEO-L476RG](../../Kit/nucleo_l476)
2. Deux câbles Dupont mâles
3. Un analyseur logique [**Picoscope 2205A**](https://www.picotech.com/oscilloscope/2000/picoscope-2000-overview).

Connectez ...
- Un câble Dupont sur la broche *'A2'* (signal) de la NUCLEO-L476 ;
- Un câble Dupont sur l'une des broches *'GND'* (masse) de la NUCLEO-L476 ;
- L'autre extrémité de ces deux câbles Dupont (signal et masse) à l'une des sondes du Picoscope.

**NB : L'analyseur logique n'est pas indispensable**. A défaut, on peut par exemple, envoyer une copie du signal qui pilote le DAC sur le [**traceur série de l'IDE Arduino**](../../Stm32duino/installation). Ceci est d'ailleurs prévu dans notre sketch, mais l'affichage à l'aide d'un analyseur logique présente les avantages de ne pas ralentir l'exécution du sketch Arduino tout en étant nettement plus lisible.

### Bibliothèque(s) requise(s)

Aucune bibliothèque n'est nécessaire pour cet exemple, le sketch qui suit est suffisant.

### Le sketch Arduino pour le générateur de signal carré

**1. Précisions préalables**

Le script de l'analyseur devra calculer les *N+1* premiers termes de la somme partielle de Fourier d'un signal carré de fréquence *f* et d'amplitude *a*, que l'on peut exprimer comme ceci :

<br>
<div align="left">
<img alt="Série de Fourier tronquée d'un signal carré (1)" src="images/fourier_carre_truncated_0.jpg" width="350px">
</div>
<br>

Après avoir posé : 
- *a = π / 4* (Pour simplifier)
- *A<sub>i</sub> = 1/(2i+1)* (Coefficients de la série)
- *B<sub>i</sub> = 2i+1* (Coefficients de la série)
- *ω = 2πf* (La pulsation "oméga").  

Nous pouvons réécrire cette somme partielle sous la forme suivante :

<br>
<div align="left">
<img alt="Série de Fourier tronquée d'un signal carré (2)" src="images/fourier_carre_truncated_1.jpg" width="240px"> 
</div>
<br>

Ce sont ces notations et conventions que nous avons utilisées dans le sketch Arduino qui suit.<br>
Nous prenons également le parti de **désigner le produit *ωt*, où *t* est un instant donné**, comme étant la **phase** à l'instant *t*. On constate que la phase, telle que nous l'avons définie, est identique quel que soit le terme d'ordre *i* considéré ; elle ne varie que du fait de la croissance du temps écoulé *t*.

Puisque notre somme partielle est une somme de fonctions sinus *périodiques*, elle est **nécessairement périodique aussi** (et de même période que son terme fondamental d'ordre `N = 0`). Nous pourrons donc nous contenter de calculer la somme partielle sur une seule période, discrétisée dans un tableau contenant `NB_SAMPLE` valeurs. Nous programmerons ensuite le DAC pour générer le "pseudo" signal carré en "lisant en boucle" ce tableau, à une fréquence que nous lui préciserons.

**2. Le sketch commenté**

>> Le sketch **"square_generator.ino"** pour cet exemple peut être téléchargé [**en cliquant ici**](../../../assets/Sketch/TUTOS.zip). Vous le trouverez dans le dossier *"\DAC - ADC - FFT\square_generator\\"*<br>

Lancez l'IDE Arduino, ouvrez un nouveau sketch vide et copiez-y le code qui suit :

```c
/*----------------------------------------------------------------------------------------*/
/* Générateur de signal carré en superposant des signaux sinusoidaux selon                */
/* la série de Fourier correspondante.                                                    */
/* Chaque appui sur le bouton USER ajoute un terme de la série de Fourier                 */
/* au signal généré, pour un maximum de 50 termes (fondamental + harmoniques).            */
/* La sortie peut être soit sur le port série du ST LINK soit sur un DAC du               */
/* du microcontrôleur.                                                                    */
/* Attention : la broche analogique doit correspondre à un DAC (matériel)                 */
/* sur la carte utilisée (ici une NUCLEO-L476RG).                                         */
/* Pour démarrer la génération de signaux : appuyer sur le bouton USER (bleu)             */
/*----------------------------------------------------------------------------------------*/

#define FEQUENCY_HZ 2048        // Fréquence du Timer qui donne le pas de temps
#define STLINK_BAUDRATE 115200  // Débit du ST Link en bits/s
#define NB_SAMPLE 256           // Nombre de pas de temps constituant une période du signal
// Une période dure NB_SAMPLE / FEQUENCY_HZ = 125 ms
// Fréquence fondamentale de notre signal carré : 1 / 125 ms = 8Hz
// Ses harmoniques auront donc pour fréquences : 8Hz, 3 x 8Hz, 5 x 8Hz, 7 x 8Hz, etc.

// Définitions pour le DAC
#define DAC_PIN PA4        // Broche du DAC sur une NUCLEO-L476RG (broche Arduino A2)
#define DAC_RESOLUTION 12  // Résolution du DAC (en bits) sur une NUCLEO-L476RG
// Valeurs min et max supportées en entrée par par le DAC
#define DAC_MIN 0
#define DAC_MAX 4095

#define N 50                         // Nombre maximum de termes dans la série de Fourier partielle
uint8_t n = 0;                       // Nombre actuel de termes dans la série de Fourier partielle
volatile uint8_t computing = 0;      // Est-ce qu'une série est en cours de calcul ?
float period_data[NB_SAMPLE];        // Echantillonnage des valeurs calculées sur une période
uint16_t dac_input_data[NB_SAMPLE];  // Valeurs qui seront envoyées au DAC
float phasis[NB_SAMPLE];             // Phase instantée discrétisée pour chaque instant de la période

// Paramètres du Timer qui génère la base de temps
TIM_TypeDef *Instance_Timer1 = TIM1;
HardwareTimer *Timer1 = new HardwareTimer(Instance_Timer1);
volatile uint16_t tick = 0;  // Décompte des interruptions du timer 1

const float omega = M_TWOPI / (float)NB_SAMPLE;  // Pulsation du signal = 2 * Pi * Fréquence
float A[N];                                      // Coefficients Ai de la série de Fourier
float B[N];                                      // Coefficients Bi de la série de Fourier

// Gestion de l'anti-rebond du bouton
#define DEBOUNCE_DELAY_MS 350

// Broche signalant (à l'analyseur) qu'une nouvelle FFT doit être calculée
#define TRIGGER_FFT_PIN D2

/*-------------------------------------------------------------------------------------------
  Initialisations
  -------------------------------------------------------------------------------------------*/

void setup() {

  Serial.begin(STLINK_BAUDRATE);

  // Définitions pour le DAC
  analogWriteResolution(DAC_RESOLUTION);  // Résolution du DAC
  pinMode(DAC_PIN, OUTPUT);               // Configuration de la broche de sortie du DAC

  // Pré-calcule les coefficients Ai et Bi de la série de Fourier d'un signal carré
  // jusqu'à l'ordre N :
  //   A[i] Suite des inverses des entiers impairs positifs : 1,3,5,7,9 ...
  //   B[i] Suite des entiers impairs positifs : 1,3,5,7,9 ...

  A[0] = 1.;
  B[0] = 1.;

  for (uint8_t i = 1; i < N; i++) {
    B[i] = B[i - 1] + 2.;
    A[i] = 1. / B[i];
  }

  // Précalcule la phase instantanée d'une fonction sinus de pulsation oméga sur une période
  for (uint16_t t = 0; t < NB_SAMPLE; t++) {
    phasis[t] = omega * (float)t;
  }

  computing = 1;  // Force le calcul d'une première série dès l'entrée dans loop()
  n = 0;          // Aucun terme de la série n'a encore été calculé au démarrage

  // Initialisation de la broche du bouton utilisateur (bleu) de la carte NUCLEO
  pinMode(USER_BTN, INPUT_PULLUP);

  // Attache la routine de service 'Button_ISR' à l'interruption du bouton utilisateur
  // Mot clef 'RISING' : l'interruption survient lorsqu'on relâche le bouton
  attachInterrupt(digitalPinToInterrupt(USER_BTN), Button_ISR, RISING);

  // Initialise la broche TRIGGER_FFT_PIN en sortie
  pinMode(TRIGGER_FFT_PIN, OUTPUT);

  // Initialisation du timer 1 qui donne le pas de temps
  // Active l'interruption périodique du timer 1, qui survienda à la fréquence FEQUENCY_HZ
  Timer1->setOverflow(FEQUENCY_HZ, HERTZ_FORMAT);
  // Attache la routine de service 'Timer1_ISR' à l'interruption périodique du timer 1
  Timer1->attachInterrupt(Timer1_ISR);
  // Démarre le timer 1
  Timer1->resume();
}

/*-------------------------------------------------------------------------------------------
  Boucle principale
  -------------------------------------------------------------------------------------------*/

void loop() {

  // Si une demande de calcul d'un terme supplémentaire de la série est reçue ...
  if (computing) {

    n++;  // On détermine l'ordre du prochain terme à calculer

    if (n >= N) {  // Si on dépasse le nombre maximum de termes prévu
      n = 0;       // Retour à l'ordre 0
      // Mise à zéro de la période du signal que le DAC doit générer
      for (uint16_t t = 0; t < NB_SAMPLE; t++) {
        period_data[t] = 0;
      }
    }

    // On affiche ce que l'on fait
    Serial.print("Demande de calcul du terme d'ordre ");
    Serial.print(n);
    Serial.println(" de la série de Fourier");

    // Précalcule une période du pseudo signal carré

    // Valeurs initiales des amplitudes max et min de la série sur la période,
    // volontairement irréalistes (voir normalisation plus bas)
    float min_val = (float)N;
    float max_val = -(float)N;

    // Pour chaque pas de la période ...
    for (uint16_t t = 0; t < NB_SAMPLE; t++) {

      // Calcule et ajoute le nouveau terme à la série
      period_data[t] += A[n - 1] * sin(B[n - 1] * phasis[t]);

      // Normalisation :
      // Calcule les valeurs minimum et maximum de la série de Fourier sur la période
      // Elles seront utiles pour "mapper" l'amplitude de celle-ci entre 0 et 2^12 - 1
      // (valeur de commande maximum à envoyer au DAC pour une sortie en 3.3V sur DAC_PIN).
      float val = period_data[t];
      if (val > max_val) {
        max_val = val;
      } else if (val < min_val) {
        min_val = val;
      }
    }

    // Mappe l'amplitude du signal entre les valeurs supportées par le DAC
    float inv_range = (DAC_MAX - DAC_MIN) / (max_val - min_val);

    // Reseigne les valeurs calculées pour une période complète dans le tableau
    // d'entiers non signés qui sera transmis au DAC.
    for (uint16_t t = 0; t < NB_SAMPLE; t++) {
      dac_input_data[t] = (uint16_t)((period_data[t] - min_val) * inv_range);
      // Affiche les valeurs du signal sur le moniteur série
      // Serial.print(t);
      // Serial.print(",");
      // Serial.println(dac_input_data[t]);
    }

    // Envoie une impulsion sur la broche TRIGGER_FFT_PIN
    digitalWrite(TRIGGER_FFT_PIN, LOW);
    digitalWrite(TRIGGER_FFT_PIN, HIGH);

    // Indique que le calcul est terminé
    computing = 0;

  }
  // Dès lors qu'un nouveau terme vient d'être ajouté à la série ...
  // Lance la génération "en boucle" du signal sur le DAC à partir
  // de la période pré-calculée.
  else {

    // Nombre de pas de temps écoulés depuis le début de la période
    static uint16_t time_step_cnt = 0;

    // Si un pas de temps vient de s'écouler ...
    if (tick) {

      // Génère sur la broche de sortie du DAC un signal de tension :
      //       Vdac = dac_input_data[time_step_cnt] * 3.3 / DAC_MAX Volts
      analogWrite(DAC_PIN, dac_input_data[time_step_cnt]);

      // Passe au pas de temps / à l'échantillon suivant de la période
      time_step_cnt++;

      // Si on a terminé la période, on recommence au pas de temps 0
      if (time_step_cnt == NB_SAMPLE - 1) time_step_cnt = 0;

      // Réarme pour le pas de temps suivant
      tick = 0;
    }
  }
}

/*-------------------------------------------------------------------------------------------
  Gestion de l'interruption du timer 1 (base de temps)
  -------------------------------------------------------------------------------------------*/
void Timer1_ISR(void) {
  tick = 1;  // Signale qu'un pas de temps s'est écoulé
}

/*-------------------------------------------------------------------------------------------
  Gestion de l'interruption du bouton utilisateur (signale que l'on doit calculer un terme
  supplémentaire de la série de Fourier partielle).
  -------------------------------------------------------------------------------------------*/
void Button_ISR(void) {
  //  Gestion anti-rebond du bouton (délai non bloquant)
  //  On attend que DEBOUNCE_DELAY_MS millisecondes se soient écoulées après l'interruption
  //  pour s'assurer que le niveau du bouton n'oscille plus.
  static unsigned long storeMillis;
  unsigned long currentMillis = millis();
  // Lorsque le bouton n'oscille plus ...
  if (currentMillis - storeMillis > DEBOUNCE_DELAY_MS) {
    storeMillis = currentMillis;
    // Signale que l'on doit calculer un nouveau terme de la série
    computing = 1; 
  }
}
```

Après avoir vérifié que l'IDE Arduino est correctement configurée, notamment qu'elle est connectée au port COM attribué au ST-LINK de votre carte (sous Windows), cliquez sur "Téléverser".

Ce code est relativement simple, mais il peut être délicat de comprendre comment est calculée la série de Fourier du signal carré du fait de diverses optimisations, en particulier les "pré-calculs" dans ```setup()``` des tableaux ```A[]```, ```B[]``` et ```phasis[]```.

### Visualisation avec un Picoscope

Connectez la sonde A du Picoscope à la bloche ```A2``` du la carte NUCLEO (sortie DAC) et à l'une de ses broches de masse ```GND```.
Appuyez quatre fois sur le bouton bleu de la carte, vous devriez observer ceci dans le terminal série de l'IDE Arduino :

```console
Demande de calcul du terme 1 de la série
Demande de calcul du terme 2 de la série
Demande de calcul du terme 3 de la série
Demande de calcul du terme 4 de la série
```

En parallèle, sur le Picoscope, vous devriez observer le signal carré approximatif avec le [phénomène de Gibbs](https://fr.wikipedia.org/wiki/Ph%C3%A9nom%C3%A8ne_de_Gibbs) (oscillations hautes-fréquences dans les zones de transition abruptes) :

<div align="left">
<img alt="Echantillonnage d'une sinusoïde" src="images/picoscope_spectre ordre 4.jpg" width="800px">
</div>
<br>

Vous pouvez continuer d'appuyer sur le bouton USER et observer l'évolution du signal dont la forme se rapproche d'un carré lorsque le nombre de coefficients de la série de Fourier augmente.

## Partie 2 : Conception de l'analyseur de signaux

Vous trouverez ici les indications pour construire l'analyseur de signaux.

### Le matériel

Exclusivement une [carte NUCLEO-L476RG](../../Kit/nucleo_l476).

### Bibliothèque(s) requise(s)

Aucune bibliothèque n'est nécessaire pour cet exemple, le sketch qui suit est suffisant.

### Le sketch Arduino

>> Le sketch **"signal_analyzer.ino"** pour cet exemple peut être téléchargé [**en cliquant ici**](../../../assets/Sketch/TUTOS.zip). Vous le trouverez dans le dossier *"\DAC - ADC - FFT\signal_analyzer\\"*<br>

Lancez l'IDE Arduino, ouvrez un nouveau sketch vide et copiez-y le code qui suit :

```c
/*---------------------------------------------------------------------------------*/
/* Analyseur de signaux                                                            */
/* Capture un signal d'entrée sur une broche analogique                            */
/* Restitue ce signal ou bien son spectre de Fourier sur le port série du ST-LINK  */
/* Analyse le spectre du signal.                                                   */
/* Utilise un algorithm de FFT "in-place" (tableaux d'entrée = tableaux de sortie) */
/* tiré de https://lloydrochester.com/post/c/example-fft/                          */
/*---------------------------------------------------------------------------------*/

#define COMPUTE_FFT 1     // Est-ce qu'on calcule la FFT ?
#define FEQUENCY_HZ 512  // Fréquence du Timer pour l'échantillonnage du signal d'entrée
// Cette fréquence est à comparer avec celles du signal en entrée, elle doit être au moins
// deux fois plus élevée que la plus grande des fréquences d'entrée que l'on souhaite
// résoudre (théorème de Shannon - Nyquist).
// Par exemple pour un signal carré de fondamentale 8Hz les quatre premiers termes ont
// pour fréquences 8Hz, 3 x 8Hz, 5 x 8Hz, 7 x 8Hz. Si on souhaite "résoudre" le
// quatrième terme, il faut donc que FEQUENCY_HZ soit au moins égal à 2 x 7 x 8Hz

#define STLINK_BAUDRATE 115200  // Débit du ST-Link

#define VREF 3.3    // Tension de référence de l'ADC (théorique)
#define ADC_PIN A0  // Broche ADC utilisée
#define ADC_RES 12  // Résolution de l'ADC (en bits)

// Calcul du pas, ou quantum, de l'ADC
const float scale = pow(2, (float)ADC_RES) - 1;
const float ADC_QUANTUM = VREF / scale;

// Paramètres du Timer qui génère la base de temps pour l'échantillonnage
TIM_TypeDef *Instance1 = TIM1;
HardwareTimer *Timer1 = new HardwareTimer(Instance1);
volatile uint8_t tick = 0;

// Définitions pour le calcul de la FFT
#if COMPUTE_FFT == 1

// Broche signalant qu'une nouvelle FFT doit être calculée
#define TRIGGER_FFT_PIN D2

// Nombre d'échantillons pour le calcul de la FFT.
// Doit être une puissance entière de 2.
#define NB_SAMPLE 128

// Incrément de fréquence pour le spectre
const float FREQ_STEP = (float)FEQUENCY_HZ / NB_SAMPLE;

// Tableaux pour le calcul des parties réelles et imaginaires des FFT
float Re[NB_SAMPLE] = { 0 };
float Im[NB_SAMPLE] = { 0 };

// Variable globale signalant qu'une nouvelle FFT doit être calculée
volatile uint8_t trig = 0;

// Amplitude minimum relative au pic fondamental, affichée par l'analyseur
#define TH_PERCENT 5

#endif

/*-------------------------------------------------------------------------------------------
  Initialisations
  -------------------------------------------------------------------------------------------*/

void setup() {

  Serial.begin(STLINK_BAUDRATE);

  // Fixe la résolution de l'ADC
  analogReadResolution(ADC_RES);

  // Initialisation du timer qui donne le pas de temps
  init_Timer1();

#if COMPUTE_FFT == 1
  // Active une interruption en cas de front montant sur la broche TRIGGER_FFT_PIN
  pinMode(TRIGGER_FFT_PIN, INPUT);
  attachInterrupt(digitalPinToInterrupt(TRIGGER_FFT_PIN), Trigger_FFT_ISR, RISING);
#endif
}

/*-------------------------------------------------------------------------------------------
  Boucle principale
  -------------------------------------------------------------------------------------------*/

void loop() {

  if (tick) {  // A chaque tick (interruption) du timer ...

#if COMPUTE_FFT == 0  // Si on ne doit pas travailler en FFT, affiche le signal échantilloné

    Serial.println(analogRead(ADC_PIN));

#else  // Sinon, affiche le spectre du signal échantilloné

    // Enregistre les valeurs pour le calcul de la FFT
    static uint16_t sample_index = 0;
    Re[sample_index] = (float)analogRead(ADC_PIN);
    sample_index++;

    // Une fois l'acquisition terminée, calcule et affiche la FFT
    if ((sample_index == NB_SAMPLE)) {

      fft(Re, Im, NB_SAMPLE);  // Calcul de la FFT / DFT

      // Si l'analyseur a confirmé le calcul d'un nouveau terme via une
      // impulsion sur TRIGGER_FFT_PIN.
      
      if (trig) {

        // Reconstruction du spectre
        for (uint16_t k = 0; k < NB_SAMPLE / 2; k++) {
          Re[k] = sqrt(Re[k] * Re[k] + Im[k] * Im[k]);
        }

        // Affiche le spectre (debug)
        // for (uint16_t k = 2; k < NB_SAMPLE / 2; k++) {
        //   Serial.println(Re[k], 4);
        // }

        // Parcours le spectre et extrait sa fondamentale et ses harmoniques
        parse_spectrum(Re, NB_SAMPLE);

        trig = 0;
      }

      // Efface le contenu des tableaux pour la prochaine FFT
      for (uint16_t i = 0; i < NB_SAMPLE; i++) {
        Re[i] = 0;
        Im[i] = 0;
      }

      // Au prochain tick, recommence à l'indice 0
      sample_index = 0;
    }

#endif
    // On signale que le travail a été fait pour ce tick du timer 1
    tick = 0;
  }
}

/*-------------------------------------------------------------------------------------------
  Initialisation du timer pour la base de temps de l'échantillonnage
  -------------------------------------------------------------------------------------------*/
void init_Timer1(void) {
  Timer1->setOverflow(FEQUENCY_HZ, HERTZ_FORMAT);
  Timer1->attachInterrupt(Timer1_ISR);
  Timer1->resume();
}

/*-------------------------------------------------------------------------------------------
  Gestion de l'interruption du timer (base de temps)
  -------------------------------------------------------------------------------------------*/
void Timer1_ISR(void) {
  tick = 1;
}

#if COMPUTE_FFT == 1

/*-------------------------------------------------------------------------------------------
  Gestion de l'interruption de la broche TRIGGER_FFT_PIN
  -------------------------------------------------------------------------------------------*/
void Trigger_FFT_ISR(void) {
  // Signale qu'une nouvelle FFT doit être calculée
  trig = 1;
}

/*-------------------------------------------------------------------------------------------
  Implémentation de l'algorithme de FFT / DFT
  Algortihme "in-place" : les mêmes tableaux sont utilisés pour les données d'entrée et de
  sortie (data_re, data_im). N est le nombre d'élements / échantillons (puissane de 2)
  Source : https://lloydrochester.com/post/c/example-fft/
  -------------------------------------------------------------------------------------------*/
void fft(float data_re[], float data_im[], const uint16_t N) {
  rearrange(data_re, data_im, N);
  compute(data_re, data_im, N);
}

/* Etape de tri "bit reversal" sur les données d'entrée  */
void rearrange(float data_re[], float data_im[], const uint16_t N) {
  unsigned int target = 0;
  for (unsigned int position = 0; position < N; position++) {
    if (target > position) {
      const float temp_re = data_re[target];
      const float temp_im = data_im[target];
      data_re[target] = data_re[position];
      data_im[target] = data_im[position];
      data_re[position] = temp_re;
      data_im[position] = temp_im;
    }
    unsigned int mask = N;
    while (target & (mask >>= 1))
      target &= ~mask;
    target |= mask;
  }
}

/* Calcul de la FFT proprement dite */
void compute(float data_re[], float data_im[], const uint16_t N) {
  const float pi = -PI;

  for (unsigned int step = 1; step < N; step <<= 1) {
    const unsigned int jump = step << 1;
    const float step_d = (float)step;
    float twiddle_re = 1.0;
    float twiddle_im = 0.0;
    for (unsigned int group = 0; group < step; group++) {
      for (unsigned int pair = group; pair < N; pair += jump) {
        const unsigned int match = pair + step;
        const float product_re = twiddle_re * data_re[match] - twiddle_im * data_im[match];
        const float product_im = twiddle_im * data_re[match] + twiddle_re * data_im[match];
        data_re[match] = data_re[pair] - product_re;
        data_im[match] = data_im[pair] - product_im;
        data_re[pair] += product_re;
        data_im[pair] += product_im;
      }

      if (group + 1 == step) {
        continue;
      }

      float angle = pi * ((float)group + 1) / step_d;
      twiddle_re = cos(angle);
      twiddle_im = sin(angle);
    }
  }
}

/*-------------------------------------------------------------------------------------------
 Parcours le spectre et extrait sa fondamentale et ses harmoniques
---------------------------------------------------------------------------------------------*/
void parse_spectrum(float *data, const uint16_t n) {

  // Crée un tableau d'index du spectre
  uint16_t data_index[n / 2];
  for (uint16_t k = 0; k < n / 2; k++) {
    data_index[k] = k;
  }

  // Trie data dans l'ordre croissant, réordonne les index en conséquence
  ShellSort(data, data_index, n / 2);

  // Normalise les amplitudes par rapport à l'amplitude maximum (on ignore le terme d'ordre 0)

  float max_amp = data[n / 2 - 2];

  if (max_amp > 0) {
    float inv_max_amp = 100. / max_amp;
    for (uint16_t k = 0; k < n / 2 - 1; k++) {
      data[k] = roundf(data[k] * inv_max_amp);
    }
  }

  // Affiche toutes les fréquences d'amplitude supérieures à TH_PERCENT :
  Serial.println("Fréquences détectées :");

  for (uint16_t k = n / 2 - 2; k >= 0; k--) {
    float amp = data[k];
    if (amp < TH_PERCENT) break;
    Serial.print("f(Hz) : ");
    Serial.print(data_index[k] * FREQ_STEP, 1);
    Serial.print(" ; Amplitude(%) : ");
    Serial.println(amp, 0);
  }
  Serial.println("");
}

/*-------------------------------------------------------------------------------------------
 Algorithme "Shell Sort" pour deux tableaux. 
 Trie (en place) par valeurs croissantes le contenu de arr, en commençant par l'indice 0
 jusqu'à l'indice n-1. 
 Trie le contenu de arr_index dans le même ordre que arr, qui est donc la clef de tri.
 NB : Un "Quick Sort" serait préférable, mais son implémentation est complexe pour un gain
 de temps qui n'est pas significatif compte-tenu des faibles valeurs de n choisies.
---------------------------------------------------------------------------------------------*/
void ShellSort(float arr[], uint16_t arr_index[], uint16_t n) {

  uint16_t i, j, inc;
  float ftemp;
  uint16_t itemp;

  inc = 1;
  do {
    inc *= 3;
    inc++;
  } while (inc <= n);

  do {
    inc /= 3;
    for (i = inc; i < n; i++) {

      ftemp = arr[i];
      itemp = arr_index[i];

      j = i;
      while (arr[j - inc] > ftemp) {
        arr[j] = arr[j - inc];
        arr_index[j] = arr_index[j - inc];
        j -= inc;
        if (j <= inc) break;
      }
      arr[j] = ftemp;
      arr_index[j] = itemp;
    }
  } while (inc > 1);
}
#endif
```

Après avoir vérifié que l'IDE Arduino est correctement configurée, notamment qu'elle est connectée au port COM attribué au ST-LINK de votre carte (sous Windows), cliquez sur "Téléverser".
Ce code est **relativement complexe**, il faut appel à des algorithmes que nous prenons "tels quels" sans chercher à les expliquer comme ```fft()``` ou encore ```ShellSort()```. **Nous allons le mettre en œuvre dans la partie 4 qui suit.**

## Partie 4 : Utilisation conjointe du générateur et de l'analyseur de signaux

Dans cette dernière partie, nous connections le générateur de signaux à l'analyseur de signaux. Nous vérifions que l'analyseur identifie correctement les fréquences du signal envoyé par le générateur.

### Le matériel

1. Deux cartes NUCLEO-L476
2. Trois câbles Dupont mâles

Connectez, avec les câbles Dupont ...
- La broche *'A2'* de la NUCLEO-L476 "générateur" sur la broche *'A0'* de la NUCLEO-L476 "analyseur" (signal) ;
- Une broche *'GND'* de la NUCLEO-L476 "générateur" sur une broche *'GND'* de la NUCLEO-L476 "analyseur" (masse commune) ;
- La broche *'D2'* de la NUCLEO-L476 "générateur" sur la broche *'D2'* de la NUCLEO-L476 "analyseur" (commande de  prise de mesure du générateur à l'analyseur).

Le système est représenté par la figure suivante :

<br>
<div align="left">
<img alt="Générateur et analyseur de signaux" src="images/generateur_analyseur.jpg" width="800px">
</div>
<br>

## Mise en œuvre

Connectez un terminal série Arduino à l'analyseur et au générateur, puis générez un signal avec 7 harmoniques sur le générateur, en appuyant 8 fois sur le bouton USER (BLEU) de la NUCLEO-L476.

Si votre montage est correct, vous devriez lire **sur le terminal Arduino de l'analyseur** la fondamentale (8 Hz) et les 7 premiers harmoniques d'un signal carré (24 Hz, 40 Hz, 56 Hz, etc.) comme ceci :

```console
Fréquences détectées :
f(Hz) : 8.0 ; Amplitude(%) : 100
f(Hz) : 24.0 ; Amplitude(%) : 33
f(Hz) : 40.0 ; Amplitude(%) : 20
f(Hz) : 56.0 ; Amplitude(%) : 14
f(Hz) : 72.0 ; Amplitude(%) : 11
f(Hz) : 88.0 ; Amplitude(%) : 9
f(Hz) : 104.0 ; Amplitude(%) : 8
f(Hz) : 120.0 ; Amplitude(%) : 7
...
```

Vous pouvez pousser plus loin la génération d'harmoniques, l'analyseur devrait être capable d'en résoudre au moins 10 avec les paramètres utilisés.

## Pour aller plus loin avec l'analyseur de spectre

Les méthodes d'analyse spectrale sont utilisées dans un grand nombre de situations par des systèmes embarqués. Selon les applications, il peut être nécessaire **d'optimiser les algorithmes de FFT au détriment de leur lisibilité** pour économiser de la mémoire ou pour réduire les temps de calcul. C'est pourquoi vous trouverez **quatre versions du sketch de l'analyseur** dans  [**l'archive téléchargeable**](../../../assets/Sketch/TUTOS.zip) :

1. **Une première version**, dans le dossier *"\DAC - ADC - FFT\signal_analyzer\\"*, intitulée **"signal_analyzer.ino"**.<br>
C'est celle que nous avons présentée ci-avant car elle est facile à lire. L'algorithme de FFT utilise **deux tableaux** de ```floats``` comportant ```NB_SAMPLE``` éléments : ```Re[]``` et ```Im[]```. On mémorise en entrée les valeurs échantillonnées (réelles) provenant de l'ADC dans ```Re[]``` tandis que l'on remplis ```Im[]``` de ```0``` (pas de parties imaginaires dans les données d'entrée). Une fois la FFT réalisée, on vient lire ses résultats dans ```Re[]``` et ```Im[]```, mais on a besoin finalement de seulement ```NB_SAMPLE / 2``` éléments de ces deux tableaux pour calculer le spectre. **Compte tenu de ces observations, on doit donc pouvoir diviser par deux la mémoire nécessaire pour notre cas d'usage ...**.

2. **Une deuxième version**, dans le dossier *"\DAC - ADC - FFT\signal_analyzer_nrfft\\"*, intitulée **"signal_analyzer_nrfft.ino"**.<br>
Cette version **intermédiaire** utilise autant de mémoire que **"signal_analyzer.ino"**, mais propose un algorithme de FFT plus performant de [**l'équipe NUMERICAL RECIPES**](https://numerical.recipes/). Les parties réelles et imaginaires de la FFT ne sont plus rangées dans deux tableaux distincts, mais alternent dans un seul tableau ```data[]``` comportant ```2*NB_SAMPLE``` éléments, ce qui complique l'écriture du code et nuit à sa lisibilité.

3. **Une troisième version**, dans le dossier *"\DAC - ADC - FFT\signal_analyzer_refft\\"*, intitulée **"signal_analyzer_refft.ino"**.<br>
Cette version **optimisée** utilise **la moitié de la mémoire** requise par les précédentes. L'algorithme de FFT adapté pour des données d'entrée **réelles** provient également de [**l'équipe NUMERICAL RECIPES**](https://numerical.recipes/). Les parties réelles et imaginaires de la FFT alternent dans un seul tableau ```data[]``` comportant cette fois-ci ```NB_SAMPLE``` éléments.

4. **Une quatrième version**, dans le dossier *"\DAC - ADC - FFT\signal_analyzer_cmsis\\"*, intitulée **"signal_analyzer_cmsis.ino"**.<br>
Elle utilise [les bibliothèques **FFT de CMSIS** (Cortex Microcontroller Software Interface Standard) **par ARM**](https://github.com/stm32duino/ArduinoModule-CMSIS), qui exploitent le coprocesseur digital (DSP) des microcontrôleurs STM32 (tout du moins de ceux qui en intègrent un ; c'est effectivement le cas du STM32L476RG).

Quelques mesures du temps et de la mémoire nécessaires pour réaliser la FFT (pour 128 éléments en entrée sur une NUCLEO-L476RG) permettent d'établir le tableau de comparaison suivant entre ces quatre versions :

|Version|Sketch|Paramètre de compilation|Temps FFT (µs)|Mémoire requise pour la FFT|
|:-:|:-:|:-:|:-:|:-:|
|1|*signal_analizer.ino*|*"Smallest -Os (default)"*|805µs|256 floats 32 bits|
|1|*signal_analizer.ino*|*"Fastest -O3 with LTO"*|780µs|256 floats 32 bits|
|2|*signal_analizer_nrfft.ino*|*"Smallest -Os (default)"*|543µs|256 floats 32 bits|
|2|*signal_analizer_nrfft.ino*|*"Fastest -O3 with LTO"*|510µs|256 floats 32 bits|
|3|*signal_analizer_refft.ino*|*"Smallest -Os (default)"*|399µs|128 floats 32 bits|
|3|*signal_analizer_refft.ino*|*"Fastest -O3 with LTO"*|345µs|128 floats 32 bits|
|4|*signal_analizer_cmsis.ino*|*"Smallest -Os (default)"*|100µs|256 floats 32 bits|
|4|*signal_analizer_cmsis.ino*|*"Fastest -O3 with LTO"*|92µs|256 floats 32 bits|

Avec le paramètre de compilation *"Smallest -Os (default)"*, on constate que le temps d'exécution **a été divisé par 8** entre l'algorithme "naïf" et celui de ARM CMSIS qui est le plus optimisé et exploite de DSP. Le passage au paramètre de compilation *"Fastest -O3 with LTO"* permet de réduire le temps d'exécution de 7% supplémentaires. Le meilleur compromis performances / temps d'exécution est *signal_analizer_refft.ino* si la FFT ne doit pas être calculée en continu.

## Pour aller plus loin avec le générateur de signaux

Une amélioration évidente de cet exemple consisterait à modifier le code du générateur afin qu'il puisse générer d'autres types de signaux (triangulaires par exemple).

## Liens et références

Voici une liste de références sur **l'analyse de Fourier** que vous pouvez consulter afin de compléter vos connaissances au-delà de ce tutoriel :

- [**Signal carré** (Wikipedia)](https://fr.wikipedia.org/wiki/Signal_carr%C3%A9)
- [**Séries de Fourier** (Wikipedia)](https://fr.wikipedia.org/wiki/S%C3%A9rie_de_Fourier)
- [**Harmoniques** (Wikipedia)](https://fr.wikipedia.org/wiki/Harmonique_(musique))
- [**Série et spectre de Fourier d'un signal carré** (Fais tes effets guitare)](https://fais-tes-effets-guitare.com/spectre-et-serie-de-fourier/)
- [**Décomposition en séries de Fourier d'un signal périodique** (Université de Genève)](https://www.unige.ch/sciences/physique/tp/tpi/Liens/Protocoles/Complements/Decomp-serie-fourier-signal-periodique.pdf)
- [**Phénomène de Gibbs et approximation sigma** (Wikipedia)](https://fr.wikipedia.org/wiki/Approximation_sigma)
- [**Transformation de Fourier** (Wikipedia)](https://fr.wikipedia.org/wiki/Transformation_de_Fourier) 
- [**Transformation de Fourier discrète (DFT)** (Wikipedia)](https://fr.wikipedia.org/wiki/Transformation_de_Fourier_discr%C3%A8te)
- [**Transformation de Fourier rapide (FFT)** (Wikipedia)](https://fr.wikipedia.org/wiki/Transformation_de_Fourier_rapide)
- [**Echantillonnage** (Wikipedia)](https://fr.wikipedia.org/wiki/%C3%89chantillonnage_(signal))
- [**Fréquence de Nyquist** (Wikipedia)](https://fr.wikipedia.org/wiki/Fr%C3%A9quence_de_Nyquist)
- [**Théorème d'échantillonnage** (Wikipedia)](https://fr.wikipedia.org/wiki/Th%C3%A9or%C3%A8me_d%27%C3%A9chantillonnage)

Pour ce qui concerne les algorithmes de FFT (et d'autres), nous avons utilisé les références suivantes :

 - [Lloyd Rochester's Geek Blog](https://lloydrochester.com/post/c/example-fft/)
 - [NUMERICAL RECIPES](https://numerical.recipes/)
 - [Package STM32Cube pour ARM CMSIS-DSP](https://www.st.com/en/embedded-software/x-cube-dspdemo.html)
 - [Portage Arduino de ARM CMSIS-DSP](https://github.com/stm32duino/ArduinoModule-CMSIS)
 - [Chez Digikey, une intoduction à la FFT de ARM CMSIS-DSP pour les MCU STM32](https://www.digikey.fr/fr/blog/audio-processing-with-stm32)
 - [L'application note de STMicroelectronics sur ARM CMSIS-DSP ](./en.DM00273990.pdf)
