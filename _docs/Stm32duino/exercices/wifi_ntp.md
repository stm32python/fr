---
title: Récupérer la date et l'heure par NTP avec un module Grove UART Wi-Fi V2
description: Mise en œuvre du module Grove UART Wi-Fi v2 avec STM32duino pour récupérer la date et l'heure avec le protocole NTP.
---

# Récupérer la date et l'heure par NTP avec un module Grove UART Wi-Fi V2

Ce tutoriel utilise un [module Grove UART Wi-Fi v2 avec STM32duino](https://wiki.seeedstudio.com/Grove-UART_Wifi_V2/) construit autour [d'un système sur puce ESP8285](https://www.espressif.com/sites/default/files/documentation/0a-esp8285_datasheet_en.pdf), un composant conçu par la société [Espressif Systems](). Les puces ESP sont très populaires et très utilisées dans les systèmes IoT amateurs car elles ne coûtent pas cher et sont équipées d'un microcontrôleur performant. Dans le cas du module qui nous intéresse, l'ESP8285 est connecté sur un UART de la carte NUCLEO et il est "piloté" par le MCU STM32, qui lui envoie des commandes en mode "texte". On parle [**de commandes AT**](https://docs.espressif.com/projects/esp-at/en/latest/esp32/AT_Command_Set/) ; un firmware est installé sur le microcontrôleur de l'ESP8285 qui les reçoit et les exécute.

Ce tutoriel montre comment :
- Se connecter au réseau Wi-Fi d'un routeur TCP/IP (en général votre box télécom domestique) afin d'accéder à Internet ;
- Adresser une requête [**Network Time Protocol (NTP)**](https://fr.wikipedia.org/wiki/Network_Time_Protocol) à un serveur distant pour récupérer la date et l'heure du moment exprimées en [**Temps Universel Coordonné (UTC)**](https://fr.wikipedia.org/wiki/Temps_universel_coordonn%C3%A9).

Ce tutoriel est utilement complété par le tutoriel [**Régler la date et l'heure de la RTC d'un microcontrôleur STM32**](rtc).

## Matériel requis

1. Une [carte NUCLEO-L476RG de STMicroelectronics](../projets/nucleo_l476rg.md). N'importe quelle autre carte NUCLEO fera l'affaire à condition qu'elle expose sur ses broches un port série UART libre en plus de celui utilisé pour l'interaction avec le ST-LINK et l'IDE Arduino (voir plus loin).
2. Un [câble adaptateur Grove / Dupont (mâle ou, de préférence, femelle)](https://fr.vittascience.com/shop/115/Lot-de-5-c%C3%A2bles-Grove---Dupont-femelle) pour connecter le module Grove sur les broches de la carte NUCLEO. 
3. Un  [module Grove UART Wi-Fi v2 avec STM32duino](https://wiki.seeedstudio.com/Grove-UART_Wifi_V2).<br>
 **Attention** : Il est bien question ici de la **version 2** de ce module. Nous ne sommes pas parvenus à faire fonctionner la version 1 ; la raison de cet échec étant probablement liée à une incompatibilité de son firmware AT avec [la bibliothèque Arduino WiFiESP](https://github.com/bportaluri/WiFiEsp).

   **Le module Grove UART Wi-Fi v2 et son brochage :**
   <br>
   <div align="left">
   <img alt="Grove UART Wi-Fi V2" src="images/uart_wifi_v2.jpg" width="450px">
   </div> 
   <br>

   L'une des difficultés de ce tutoriel consiste à connecter correctement le module Wi-Fi sur la carte NUCLEO-L476RG. La figure qui précède précise cette étape. Du côté de la carte NUCLEO, nous avons choisi les broches `PA0` (le `A0` du connecteur Arduino, pour `Tx`) et `PA1` (le `A1` du connecteur Arduino, pour `Rx`) qui correspondent à l'UART4 du STM32L476RG.  Plusieurs autres choix sont possibles pour ces broches, il suffit juste de choisir un couple qui correspond au même UART, d'éviter l'USART2 (c'est celui du ST-LINK) et, bien sûr, d'adapter les déclarations dans le sketch Arduino. Si vous utilisez une autre carte NUCLEO, vous aurez donc besoin de la cartographie de ses UART, disponible dans sa fiche technique.
   
   **Attention** de ne pas vous tromper sur les broches 3V3 et GND, notamment de ne pas les inverser ! Vous risqueriez d'endommager (sinon détruire) le module Wi-Fi et la carte NUCLEO.<br>
<br>

## Première étape : Se connecter à un point d'accès

Ce premier sketch montre comment se connecter à un point d'accès avec le module Wi-Fi. On remarquera l'usage de la fonction `HardwareSerial()`, qui connecte le module à un "vrai" UART du microcontrôleur STM32L475RG. Chaque fois que c'est possible, utilisez cette fonction plutôt que `SofwareSerial()` qui émule un UART avec le microcontrôleur et qui n'est ni aussi fiable, ni aussi performante.

### Bibliothèque(s) requise(s) pour ce sketch

Seule la [bibliothèque WiFiESP](https://github.com/bportaluri/WiFiEsp) est nécessaire.

### Le sketch Arduino

>> Le sketch pour cet exemple (et tous les autres) peut être téléchargé [**en cliquant ici**](../../../assets/Sketch/TUTOS.zip).

Lancez l'IDE Arduino, ouvrez un nouveau sketch vide et copiez-y le code qui suit :

```c
/*------------------------------------------------------------------------------------------------------------
  Objet(s) du sketch :
  Connectivité Wi-Fi avec un module ESP01 ou Grove Wi-Fi V2
  Cet exemple est adapté de celui distribué avec la bibliothèque WiFiESP :
  https://github.com/bportaluri/WiFiEsp  
  -------------------------------------------------------------------------------------------------------------
  Matériel & brochage :
	- Carte NUCLEO-L476RG
	- Module Wi-Fi ESP-01 ou Grove UART Wi-Fi v2 sur UART4
  -----------------------------------------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------*/
/* Variables globales, includes, defines                                       */
/*----------------------------------------------------------------------------*/
#define STLK_BDRATE (115200) // Débit du port série du ST-LINK
#define DELAY_5S (5000) // Temps d'attente 5s pour différents usages

// Bibliothèque pour le WiFi
#include <WiFiEsp.h>

// Débit du port série attribué au module Wi-Fi.
// Il est très important de conserver cette valeur qui est paramétrée
// dans le firmware du SoC ESP du module Wi-Fi.
#define WIFI_BDRATE (115200)

// L'objet WiFi_Serial sera un port série physique connecté aux broches
// PA1 (pour la ligne RX) et PA0 (pour la ligne TX)
HardwareSerial WiFi_Serial(PA1, PA0);

// Mémorisation des informations de connexion au réseau Wi-Fi domestique
// Nom du réseau Wi-Fi auquel vous souhaitez vous connecter
#define WIFI_SSID "mySSID"

// Clef de sécurité du réseau Wi-Fi auquel vous souhaitez vous connecter
#define WIFI_PASS "myPASS"

// Tableaux de caractères pour contenir les identifiants
char ssid[] = WIFI_SSID;
char pass[] = WIFI_PASS;

// Nombre de tentatives de connexion avant de forcer un reset du STM32
#define RESET_RETRY (2)

/*----------------------------------------------------------------------------*/
/* Paramètres de démarrage                                                    */
/*----------------------------------------------------------------------------*/
void setup() {

  // Initialise le port série du ST-LINK
  Serial.begin(STLK_BDRATE);
  while (!Serial) delay(100);
  Serial.println("Débit du ST-LINK en bauds : " + String(STLK_BDRATE));

  // Initialise le module Wi-Fi
  Initialize_WiFi(&Serial);

  // Liste les réseaux Wi-Fi à proximité
  listWiFiNetworks(&Serial);

  // Connexion du module Wi-Fi au réseau spécifié
  Connect_WiFi(&Serial);
}

/*----------------------------------------------------------------------------*/
/* Boucle principale                                                          */
/*----------------------------------------------------------------------------*/
void loop() {
  // Absolument rien dans cette partie
}

/*----------------------------------------------------------------------------*/
/* Initialisation du module WiFi                                             */
/*----------------------------------------------------------------------------*/
void Initialize_WiFi(HardwareSerial *serial) {

  // Initialise le port série du WiFi
  WiFi_Serial.begin(WIFI_BDRATE);
  while (!WiFi_Serial) delay(100);

  serial->println("Débit du module Wi-Fi en bauds : " + String(WIFI_BDRATE));

  // Initialise le module ESP01
  WiFi.init(&WiFi_Serial);

  if (WiFi.status() == WL_NO_SHIELD) {
    serial->println("Module Wi-Fi non détecté");
    while (true)
      ;
  }
  serial->println("Module Wi-Fi prêt");
}

/*----------------------------------------------------------------------------*/
/* Connexion au WiFi                                                          */
/*----------------------------------------------------------------------------*/
void Connect_WiFi(HardwareSerial *serial) {

  uint8_t status = WL_IDLE_STATUS;
  
  // Décompte du nombre de tentatives de connexion
  uint8_t n = 0;

  // Aussi longtemps que le module ne valide pas sa connexion au point d'accès
  while (status != WL_CONNECTED) {

    serial->println("Tentative de connexion à " + (String)ssid);

    // Tentative de connexion
    status = WiFi.begin(ssid, pass);
    n++;

  // Si trop de tentatives de reconnexion infructueuses, RESET !
  if (n > RESET_RETRY) HAL_NVIC_SystemReset();

    // Cinq secondes entre deux tentatives de connexion
    delay(DELAY_5S);
  }

  serial->println("Connexion réussie !");
}

/*----------------------------------------------------------------------------*/
/* Renvoie la liste de réseaux WiFi présents sur le port série spécifié       */
/*----------------------------------------------------------------------------*/
void listWiFiNetworks(HardwareSerial *serial) {

  // Cherche les réseaux Wifi à proximité
  int numSsid = WiFi.scanNetworks();

  if (numSsid == -1) {
    serial->println("Aucun point d'accès Wi-Fi n'a été détecté");
    while (true)
      ;
  }

  // Renvoie la liste des réseaux trouvés sur le port série serial
  serial->println("Points d'accès Wi-Fi détectés :");

  for (int thisNet = 0; thisNet < numSsid; thisNet++) {

    if (strlen(WiFi.SSID(thisNet))) {  
      // Si des points d'accès on été trouvés affiche les informations :
      serial->print(" ");
      serial->print(thisNet); // Numéro détecté
      serial->print(" : ");
      serial->print(WiFi.SSID(thisNet)); // Identifiant
      serial->print(" Signal : ");
      serial->print(WiFi.RSSI(thisNet)); // Atténuation
      serial->print(" dBm");
      serial->print(" Chiffrement : "); // Mode de sécurité

      int thisType = WiFi.encryptionType(thisNet);

      switch (thisType) {
        case ENC_TYPE_WEP:
          serial->println("WEP");
          break;
        case ENC_TYPE_WPA_PSK:
          serial->println("WPA_PSK");
          break;
        case ENC_TYPE_WPA2_PSK:
          serial->println("WPA2_PSK");
          break;
        case ENC_TYPE_WPA_WPA2_PSK:
          serial->println("WPA_WPA2_PSK");
          break;
        case ENC_TYPE_NONE:
          serial->println("Aucun");
          break;
      }
    }
  }
}
```

Après avoir vérifié que l'IDE Arduino est correctement configurée, notamment qu'elle est connectée au port COM attribué au ST-LINK de votre carte (sous Windows), cliquez sur "Téléverser".

### Affichage sur le terminal série

Si tout s'est passé correctement vous devriez observer cette séquence de messages (où, bien sûr, "mySSID" devrait être remplacé par le nom de votre point d'accès) sur le terminal série de l'IDE Arduino :

```console
Débit du ST-LINK en bauds : 115200
Débit du module Wi-Fi en bauds : 115200
[WiFiEsp] Initializing ESP module
[WiFiEsp] Initilization successful - 2.2.1
Module Wi-Fi prêt
Points d'accès Wi-Fi détectés :
 0 : mySSID Signal : -29 dBm Chiffrement : WPA2_PSK
Tentative de connexion à mySSID
[WiFiEsp] Connected to mySSID
Connexion réussie !
```
<br>


## Deuxième étape : Faire une requête NTP pour obtenir la date et l'heure (UTC)

Nous sommes maintenant capables de connecter la carte NUCLEO à Internet. L'étape suivante consiste à modifier notre sketch pour qu'il interroge un serveur NTP à une fréquence donnée, et qu'il affiche la date et l'heure universelles (évaluées sur le méridien de Greenwich).<br>
Afin d'agrémenter ce tutoriel de connaissances techniques supplémentaires, nous utiliserons [un timer]() pour gérer l'interrogation du serveur NTP à intervalles de temps réguliers. Cette fonction aurait bien évidemment pu être réalisée bien plus facilement avec `delay()` mais l'intérêt d'utiliser un timer est de ne pas bloquer la boucle principale, comme dans [cet autre exemple](del_blink).<br>
Pour la partie connexion au Wi-Fi, nous retrouvons les fonctions de la première partie de ce tutoriel. Deux nouvelles fonctions permettent d'interroger le serveur NTP, d'obtenir sa réponse et la décoder :
1. ***Get_Display_Universal_Time*** : Récupère et affiche le temps universel. Cette fonction réalise essentiellement deux opérations. Elle commence par appeler la fonction  *getUTC* (voir point 2) pour obtenir le temps universel et elle le décode grâce à la fonction *setTime* de la bibliothèque [Time](https://github.com/PaulStoffregen/Time). En fait, elle est un peu plus subtile que cela. Elle interroge un premier server NTP (*TimeServer1*) et, si celui-ci ne lui répond pas, elle répète l'opération avec un deuxième serveur NTP (*TimeServer2*). Si aucun des deux ne lui a répondu, elle force un reset du microcontrôleur STM32.
2.  ***getUTC*** : C'est la fonction qui fait le travail important ; elle réalise [une requête UDP](https://fr.wikipedia.org/wiki/User_Datagram_Protocol) à l'attention du serveur NTP dont l'adresse IP lui est donnée en argument. Elle attend la réponse du serveur et la retourne, sous la forme d'un entier signé encodé sur 32 bits (type *uint32_t*). Cette fonction est directement recopiée d'un exemple de la bibliothèque [WiFiESP](https://github.com/bportaluri/WiFiEsp).

### Bibliothèque(s) requise(s) pour ce sketch

- La bibliothèque [WiFiESP](https://github.com/bportaluri/WiFiEsp) de Bruno Portaluri
- La bibliothèque  [Time](https://github.com/PaulStoffregen/Time) de Paul Stoffregen

### Le sketch Arduino

> **Le sketch ci-dessous peut être [téléchargé par ce lien](SKETCHS.zip)**.

Lancez l'IDE Arduino, ouvrez un nouveau sketch vide et copiez-y le code qui suit :

```c
/*------------------------------------------------------------------------------------------------------------
  Objet(s) du sketch :
   - Connectivité Wi-Fi avec un module ESP01 ou Grove Wi-Fi V2
   - Interrogation d'un serveur NTP et affichage du temps universel coordonné (UTC) sous un format lisible
  Cet exemple est adapté de celui distribué avec la bibliothèque WiFiESP :
  https://github.com/bportaluri/WiFiEsp
  Il utilise la bibliothèque Time pour décoder la réponse du serveur NTP :
  https://github.com/PaulStoffregen/Time
  -------------------------------------------------------------------------------------------------------------
  Matériel & brochage :
	- Carte NUCLEO-L476RG
	- Module Wi-Fi ESP-01 ou Grove UART Wi-Fi v2 sur UART4
  -----------------------------------------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------*/
/* Variables globales, includes, defines                                       */
/*----------------------------------------------------------------------------*/

/* Déclarations pour le Wi-Fi */

#include <WiFiEsp.h>

#define WIFI_BDRATE (115200)
#define WIFI_SSID "mySSID"
#define WIFI_PASS "myPASS"
#define RESET_RETRY (2)
#define DELAY_5S (5000)

HardwareSerial WiFi_Serial(PA1, PA0);

char ssid[] = WIFI_SSID;
char pass[] = WIFI_PASS;

/* Déclarations pour le service UDP */
/* et pour l'interrogation du NTP */

#include <WiFiEspUdp.h>
#define UDP_PORT (2390)
WiFiEspUDP Udp;

// Adresses IP de deux serveurs NTP

#define NTP_SERVER_1 "216.239.35.4"
#define NTP_SERVER_2 "time.nist.gov"

char TimeServer1[] = NTP_SERVER_1;
char TimeServer2[] = NTP_SERVER_2;

// Bibliothèque "Time" pour décoder le temps universel renvoyé par le
// serveur NTP
#include "TimeLib.h"

/* Gestion de l'interrogation réglière du NTP avec un timer */

// Fréquence du Timer 1 en Hz
#define TIMER1_FREQ (1)  // Un tic par seconde

// Nombre d'overflows du timer 1 constituant la base de temps
#define TIME_BASE (3600)  // Soit, un heure dans notre cas

// Instanciation du timer 1
TIM_TypeDef *Instance = TIM1;
HardwareTimer Timer1(Instance);

// Compteur pour le temps écoulé
volatile uint32_t TickCounter = 0;

/* Déclarations diverses */

// Débit du port série du ST-LINK
#define STLK_BDRATE (115200)

/* Initialisations */
void setup() {

  // Initialisation du port série
  Serial.begin(STLK_BDRATE);
  while (!Serial) delay(100);
  Serial.println("Port série du ST-LINK initialisé");

  // Initialise le module Wi-Fi
  Initialize_WiFi(&Serial);

  // Liste les réseaux Wi-Fi à proximité
  listWiFiNetworks(&Serial);

  // Connexion du module Wi-Fi au réseau spécifié
  Connect_WiFi(&Serial);

  // Initialisation et démarrage du Timer 1 et de son interruption pour la base de temps
  Timer1_Start();

  Serial.println("Initialisations terminées !");

  // Premier affichage du temps universel coordonné
  Get_Display_Universal_Time(&Serial);
}

/* Boucle principale */
void loop() {

  // Interrogation du serveur NTP toutes les TIME_BASE interruptions du timer 1
  if (TickCounter > TIME_BASE - 1) {

    // Affichage du temps universel coordonné
    Get_Display_Universal_Time(&Serial);

    TickCounter = 0;
  }
}

/****************************************************************/
/* Démarrage du Timer 1 et de son interruption (base de temps)  */
/****************************************************************/
void Timer1_Start(void) {
  Timer1.setOverflow(TIMER1_FREQ, HERTZ_FORMAT);
  Timer1.attachInterrupt(Timer1_ISR);
  Timer1.resume();
  Serial.println("Timer 1 actif !");
}

/*******************************************************************/
/* Fonction de service de l'interruption de dépassement du Timer 1 */
/*******************************************************************/
void Timer1_ISR(void) {
  TickCounter++;
}

/*----------------------------------------------------------------------------*/
/* Initialisation du module WiFi                                             */
/*----------------------------------------------------------------------------*/
void Initialize_WiFi(HardwareSerial *serial) {

  // Initialise le port série du WiFi
  WiFi_Serial.begin(WIFI_BDRATE);
  while (!WiFi_Serial) delay(100);

  serial->println("Port série du module Wi-Fi initialisé");

  // Initialise le module ESP01
  WiFi.init(&WiFi_Serial);

  if (WiFi.status() == WL_NO_SHIELD) {
    serial->println("Aucun module Wi-Fi n'a été détecté !");
    while (true)
      ;
  }
  serial->println("Module Wi-Fi initialisé");
}

/*----------------------------------------------------------------------------*/
/* Connexion au WiFi                                                          */
/*----------------------------------------------------------------------------*/
void Connect_WiFi(HardwareSerial *serial) {

  uint8_t status = WL_IDLE_STATUS;
  uint8_t n = 0;

  // Aussi longtemps que le module ne valide pas sa connexion au point d'accès
  while (status != WL_CONNECTED) {

    serial->println("Tentative de connexion à " + (String)ssid);
    status = WiFi.begin(ssid, pass);
    n++;

    // Si trop de tentatives de reconnexion infructueuses, RESET !
    if (n > RESET_RETRY) HAL_NVIC_SystemReset();

    // Cinq secondes entre deux tentatives de connexion
    delay(DELAY_5S);
  }

  serial->println("Module Wi-Fi connecté au point d'accès");
}

/*----------------------------------------------------------------------------*/
/* Renvoie la liste de réseaux WiFi présents sur le port série spécifié       */
/*----------------------------------------------------------------------------*/
void listWiFiNetworks(HardwareSerial *serial) {

  // Cherche les réseaux Wifi à proximité
  int numSsid = WiFi.scanNetworks();

  if (numSsid == -1) {
    serial->println("Aucun point d'accès Wi-Fi n'a été trouvé");
    while (true)
      ;
  }

  // Renvoie la liste des réseaux trouvés sur le port série serial
  serial->println("Points d'accès Wi-Fi disponibles :");

  for (int thisNet = 0; thisNet < numSsid; thisNet++) {

    if (strlen(WiFi.SSID(thisNet))) {
      // Si des points d'accès on été trouvés affiche les informations :
      serial->print(" - ");
      serial->print(thisNet);  // Numéro détecté
      serial->print(" ");
      serial->print(WiFi.SSID(thisNet));  // Identifiant
      serial->print(" Signal: ");
      serial->print(WiFi.RSSI(thisNet));  // Atténuation
      serial->print(" dBm");
      serial->print(" Encryption: ");  // Mode de sécurité

      int thisType = WiFi.encryptionType(thisNet);

      switch (thisType) {
        case ENC_TYPE_WEP:
          serial->println("WEP");
          break;
        case ENC_TYPE_WPA_PSK:
          serial->println("WPA_PSK");
          break;
        case ENC_TYPE_WPA2_PSK:
          serial->println("WPA2_PSK");
          break;
        case ENC_TYPE_WPA_WPA2_PSK:
          serial->println("WPA_WPA2_PSK");
          break;
        case ENC_TYPE_NONE:
          serial->println("Aucune");
          break;
      }
    }
  }
}

/*----------------------------------------------------------------------------*/
/* Récupération et affichage du temps universel                               */
/*----------------------------------------------------------------------------*/
void Get_Display_Universal_Time(HardwareSerial *serial) {

  serial->println("Interrogation du serveur de temps");

  uint8_t n = 0;
  bool try_alternative_ntp_server = false;
  bool success = false;

  // Aussi longtemps qu'on n'a pas obtenu une réponse à la requête NTP
  while (!success) {

    uint32_t UTC_NOW;

    // Récupère l'heure et la date universelles de l'instant
    if (!try_alternative_ntp_server) {
      UTC_NOW = getUTC(TimeServer1);  // Interroge le serveur NTP n°1
    } else {                          // Si le serveur NTP n°1 n'a pas répondu ...
      UTC_NOW = getUTC(TimeServer2);  // ... interroge le serveur NTP n°2
    }

    // Si la requête au serveur NTP est un succès
    if (UTC_NOW) {

      // Fonction de la bibliothèque Time : à partir du temps universel coordonné,
      // met à l'heure une horloge virtuelle
      setTime(UTC_NOW);

      // Affiche le temps UTC sous forme de date et heure lisibles
      char buf[40];
      sprintf(buf, "%02d/%02d/%4d %02d:%02d:%02d", day(), month(), year(), hour(), minute(), second());
      serial->print("Temps universel coordonné : ");
      serial->println(buf);

      success = true;

    } else {  // Si la requête au serveur NTP a échoué

      serial->println("Echec de la requête NTP !");

      // Teste l'autre serveur NTP
      try_alternative_ntp_server = true;
      n++;

      // Si c'est le deuxième échec consécutif, reset du microcontrôleur
      if (n > RESET_RETRY) HAL_NVIC_SystemReset();

      delay(DELAY_5S);
    }
  }
}

/*----------------------------------------------------------------------------*/
/* Requête au serveur de temps internet pour récupérer le temps universel     */
/* coordonné (UTC).                                                           */
/*----------------------------------------------------------------------------*/
uint32_t getUTC(char *ntpSrv) {

  const uint32_t NTP_PACKET_SIZE = 48;
  const uint32_t UDP_TIMEOUT = 2000;
  byte packetBuffer[NTP_PACKET_SIZE];

  uint32_t UTC = 0;
  memset(packetBuffer, 0, NTP_PACKET_SIZE);

  packetBuffer[0] = 0b11100011;
  packetBuffer[1] = 0;
  packetBuffer[2] = 6;
  packetBuffer[3] = 0xEC;

  packetBuffer[12] = 49;
  packetBuffer[13] = 0x4E;
  packetBuffer[14] = 49;
  packetBuffer[15] = 52;

  Udp.begin(UDP_PORT);
  Udp.beginPacket(ntpSrv, 123);  //NTP requests are to port 123
  Udp.write(packetBuffer, NTP_PACKET_SIZE);
  Udp.endPacket();

  uint32_t startMs = millis();
  while (!Udp.available() && (millis() - startMs) < UDP_TIMEOUT)
    ;

  if (Udp.parsePacket()) {

    Udp.read(packetBuffer, NTP_PACKET_SIZE);
    uint32_t highWord = word(packetBuffer[40], packetBuffer[41]);
    uint32_t lowWord = word(packetBuffer[42], packetBuffer[43]);
    uint32_t secsSince1900 = highWord << 16 | lowWord;
    const uint32_t seventyYears = 2208988800UL;

    UTC = secsSince1900 - seventyYears;
    Udp.stop();
  }
  return UTC;
}
```

Après avoir vérifié que l'IDE Arduino est correctement configurée, notamment qu'elle est connectée au port COM attribué au ST-LINK de votre carte (sous Windows), cliquez sur "Téléverser".

### Affichage sur le terminal série

Si tout s'est passé correctement vous devriez observer cette séquence de messages (où, bien sûr, "mySSID" devrait être remplacé par le nom de votre point d'accès) sur le terminal série de l'IDE Arduino :

```console
08:33:58.269 -> Port série du ST-LINK initialisé
08:33:58.269 -> Port série du module Wi-Fi initialisé
08:33:58.269 -> [WiFiEsp] Initializing ESP module
08:34:01.683 -> [WiFiEsp] Initilization successful - 2.2.1
08:34:01.683 -> Module Wi-Fi initialisé
08:34:03.882 -> Points d'accès Wi-Fi disponibles :
08:34:03.882 ->  - 0 mySSID Signal: -31 dBm Encryption: WPA2_PSK
08:34:03.882 -> Tentative de connexion à mySSID
08:34:08.966 -> [WiFiEsp] Connected to mySSID
08:34:13.935 -> Module Wi-Fi connecté au point d'accès
08:34:13.935 -> Timer 1 actif !
08:34:13.935 -> Initialisations terminées !
08:34:13.935 -> Interrogation du serveur de temps
08:34:14.028 -> Temps universel coordonné : 23/02/2023 07:34:13
09:33:57.998 -> Interrogation du serveur de temps
09:33:58.069 -> Temps universel coordonné : 23/02/2023 08:33:57
10:33:42.328 -> Interrogation du serveur de temps
10:33:42.422 -> Temps universel coordonné : 23/02/2023 09:33:41
```

On notera l'excellente précision du timer du STM32, qui selon les horodatages du terminal série d'IDE Arduino, mais aussi selon les remontées des serveurs NTP, a bien appelée la fonction *Get_Display_Universal_Time* à une heure d'intervalle, avec un décalage de 16 secondes que l'on peut attribuer aux délais de calcul et d'affichage.<br>
On remarque également que le temps coordonné est **en retard d'une heure** sur l'horodatage de l'IDE Arduino du fait de l'ajustement de l'heure d'hivers en France.
