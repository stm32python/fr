---
title: Publication de la température et contrôle d’une LED en BLE
description: Utilisation de BlueST pour publier une température et simuler un interrupteur commandé à distance avec STM32duino BLE
---
# Publication de la température et contrôle d’une LED en BLE

>> Tutoriel en cours de rédaction, merci pour votre bienveillance !

>> Cet exemple est inspiré d'un tutoriel disponible [**sur le "wiki" STM32 MCU**](https://wiki.st.com/stm32mcu/wiki/Connectivity:STM32WB_BLE_Arduino). Merci à Lionel CHASTILLON pour son support !

## Pour aller plus loin : utiliser MIT APP Inventor plutôt que BlueST

## Ressources