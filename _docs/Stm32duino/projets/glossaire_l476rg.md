---
title: Anatomie du microcontrôleur STM32L476RG
description: Glossaire concernant l'anatomie du microcontrôleur STM32L476RG
---

# Anatomie du microcontrôleur STM32L476RG

Nous présentons ici un glossaire minimaliste des périphériques et acronymes les plus fréquemment cités pour le microcontrôleur STM32L476RG et pour les CPU ARM Cortex M4 plus généralement. Les acronymes sont volontairement conservés en anglais pour éviter des confusions avec leur version française.

**ADC : Analog to Digital Converter**. Circuit électronique spécialisé dans la conversion de signaux d’entrée analogiques (des tensions éventuellement variables dans le temps, comprises entre 0 et +3.3V) en signaux numériques (des nombres entiers proportionnels à la tension d’entrée à chaque instant et compris entre 0 et 4096).

**AHB : ARM High-performance Bus**. But haut débit intégré aux microprocesseurs ARM Cortex. Ses lignes d’interconnections relient le CPU et le DMA à deux bus APB de moindre débit qui sont eux-mêmes connectés aux périphériques du microcontrôleur.

**APB : ARM Peripheral Bus.** Bus intégré aux microprocesseurs ARM Cortex, faisant l’interface entre la majorité des périphériques et le bus AHB.

**ALU : Arithmetical and Logical Unit**. L’unité arithmétique et logique est un circuit électronique intégré au CPU chargé d'effectuer les calculs sur des nombres entiers et/ou binaires.

**ARM Cortex M4 CPU** : Cœur pour microcontrôleurs, essentiellement un microprocesseur, offrant un excellent compromis performances / consommation conçu par la société ARM. L’architecture propriétaire de la société ARM rencontre depuis 2020 un tel succès qu’elle semble désormais en mesure de s’imposer sur les ordinateurs de bureau, les stations travail et les serveurs après s'être imposée dans tous les smartphones.

**ART (Adaptative Real-Time) accelerator**. Circuit électronique conçu pour compenser la latence des accès CPU à la mémoire flash ou à la SRAM, interne ou externe au microcontrôleur, lorsqu’il exécute un programme.

**CAN : Controller Area Network**. Contrôleur de bus série multi-maître et multiplexé conçu par Bosch pour augmenter la fiabilité et abaisser le coût des réseaux embarqués dans les automobiles. Le protocole CAN gère matériellement les erreurs, les priorités des messages et l’ajout ou la suppression dynamique et « à chaud » de nœuds / stations sur son réseau.

**COMP : Comparator**. Ce circuit électronique compare deux tensions d’entrée analogiques variables et indique à chaque instant laquelle est la plus élevée (par 0 ou 1). Sa sortie est donc numérique.

**CPU : Central Processing Unit**. Unité centrale de traitement du microcontrôleur, en l’occurrence un modèle Cortex M4 conçu par la société ARM et optimisé pour être intégré dans les microcontrôleurs et autres SoC.

**CRC calculation unit**. Circuit électronique spécialisé dans le calcul de Codes de Redondance Cycliques. La technique du CRC permet de détecter (mais pas de corriger) des erreurs dans des séquences binaires. Elle sert par exemple à détecter lorsqu’un message transmis sur un bus est accidentellement modifié entre son émission et sa réception.

**DAC : Digital to Analog Converter**. Un convertisseur numérique-analogique est un circuit électronique dont la fonction est de traduire de façon dynamique une valeur numérique en une valeur analogique qui lui est proportionnelle.

**DFSDM : Digital Filter for Sigma-Delta modulators interface**. Circuit d’interface électronique offrant les fonctionnalités d’un convertisseur analogique-numérique dont la partie analogique est située à l’extérieur du microcontrôleur et peut donc être adaptée à des applications spécifiques.

**DMA : Direct Memory Access**. Circuit électronique connecté, sur le même bus AHB que le CPU, spécialisé dans les transferts d’informations d’une zone mémoire vers une autre. Le contrôleur DMA fonctionne en parallèle avec le CPU et est nettement plus rapide que celui-ci pour les opérations spécialisées qu’il réalise.

**Dual bank flash memory**. Une mémoire flash à deux plages peut exécuter un programme dans l’une de ses plages pendant que son autre plage est en cours d’effacement ou de programmation. Elle permet d’éviter que le CPU ralentisse pendant les opérations de programmation et rend le système plus résistant aux coupures d’alimentations.

**ECC (Error-Correcting Code) flash memory** : Une mémoire flash à code correcteur d'erreurs contient un circuit lui permettant de détecter et de corriger les accidents les plus courants de corruptions de données.

**ETM : Embedded Trace Macrocell**. Composant électronique haute performance et basse consommation intégré dans le CPU dont la fonction est de reconstituer l’historique d’exécution d’un programme pour les besoins des opérations de débogage.

**EU** : Exécution Unit. Circuit intégré au CPU chargé d’exécuter une instruction après son chargement (voir IFU) et son décodage (voir IDU). Elle est constituée de l’ALU, de la PCU, de la FPU et d’autres unités intégrées que nous ne détaillerons pas ici (LSU : Load Store Unit, AGU : Address Generation Unit…).

**EXTI : Extended Interrupts and Events Controller**. Circuit dédié à la gestion des interruptions et évènements provenant des GPIO et de circuits externes au microcontrôleur.

**Firmware**. Nom donné au fichier binaire qui contient un programme compilé destiné à un microcontrôleur.

**Flash (memory)**. Mémoire qui ne s’efface pas lorsque son alimentation électrique est éteinte. Elle plus dense, càd qu’elle peut stocker plus de bits par unité de surface, que la RAM. En contrepartie, la mémoire flash est plus lente à lire et à écrire que la RAM.

**FPU** : Floating Point Unit. Composant électronique intégré dans le CPU dont la fonction est l’accélération des opérations mathématiques portant sur des nombres réels codés (et approximés) « en virgule flottante ».

**FSMC : Flexible Static Memory Controller**. Circuit électronique totalement configurable d’interfaçage du microcontrôleur avec des mémoires externes à celui-ci, de types Flash NOR, Flash NAND, PSRAM et SRAM ou avec d’autres périphériques ayant une interface parallèle.

**GPIO : General Purpose Inputs / Outputs**. Les Ports d’entrée-sortie à usage général désignent l’ensemble des fonctions (programmables) que peuvent remplir les broches du microcontrôleur. Ils permettent de sélectionner les circuits internes au microcontrôleur que l’on souhaite utiliser afin d’interagir avec des composants externes et connectés à celui-ci tels que mémoires, moteurs, capteurs…

**I<sup>2<sup>C ou I2C : Inter Integrated Circuit**. Contrôleur de bus série fonctionnant selon un protocole inventé par Philips. Tout comme le bus SPI, le bus I2C a été créé pour fournir un moyen simple de transférer des informations numériques entre des capteurs, des actuateurs, des afficheurs et un microcontrôleur.

**IDE : Integrated Development Environment**. Un environnement de développement intégré est un ensemble d'outils logiciels qui permet d'augmenter la productivité des programmeurs qui développent de nouveaux logiciels.

**IDU : Instructions Decoder Unit**. Circuit intégré au CPU chargé d’interpréter à la volée les instructions du programme en cours d’exécution, c’est-à-dire de préparer les lignes de contrôle du CPU pour réaliser les opérations requises par l’instruction sur ses données (également désignées par « opérandes »).

**IFU : Instructions Fetch Unit**. Circuit intégré au CPU chargé de recopier à la volée les instructions et données requises par un programme en cours d’exécution depuis la mémoire de masse (Flash, RAM…) vers les registres du CPU, avant l’intervention du décodeur d’instructions.

**IRTIM : Infrared Timer**. Architecture timer spécialisée dans la gestion des LED infrarouge pour les applications de télécommandes.

**IWDG : Independant Watchdog**. Le chien de garde indépendant est un circuit électronique qui fonctionne avec une horloge dédiée. Il réalise un compte à rebours depuis une valeur maximum programmable jusqu’à zéro. L’IWDG force un redémarrage (reset) du microcontrôleur lorsque le compte à rebours atteint zéro sauf si celui-ci est réinitialisé à sa valeur maximale avant cette échéance par une instruction du programme utilisateur.

**LCD : Liquid Crystal Display**. Contrôleur pour piloter un afficheur à cristaux liquides.

**MCU : Microcontroller Unit**. Un microcontrôleur est un « système sur puce » (SoC) qui rassemble sur une même puce de silicium un microprocesseur et d’autres composants et périphériques : des bus, des horloges, de la mémoire, des ports entrées-sorties généralistes, etc.

**MPU : Memory Protection Unit**. Composant électronique intégré dans le CPU dont la fonction est la protection de l’intégrité et le cloisonnement de la mémoire dans laquelle s’exécutent les programmes.

**NVIC : Nested Vectored Interrupts Controller**. Circuit intégré au CPU ARM Cortex M4 chargé de la gestion des interruptions. Il optimise en particulier la réactivité du système aux interruptions en leur assignant des priorités et en gérant astucieusement les situations où plusieurs surviennent dans un intervalle de temps très court.

**OPAMP : Operational Amplifier**. Un amplificateur opérationnel est un circuit électronique qui amplifie fortement une différence de potentiel électrique présente à ses entrées.

**PCU : Processor control unit**. Circuit intégré au cœur du CPU, c’est le « chef d’orchestre » qui coordonne les opérations, les flux de données et d’instructions entre les mémoires, l’ALU, la FPU, etc.

**PWM : Pulse Width Modulation**. La modulation en largeur d’impulsion est une technique permettant de faire varier le potentiel électrique d’une broche entre 0 et +3.3V (pour le microcontrôleur concerné) selon un signal rectangulaire de fréquence et de rapport cyclique (ratio entre le temps où le signal est à 0V et celui où il est à +3.3V au sein d’une période) dynamiquement ajustables. La génération de signaux PWM est l’une des fonctions assurées par les timers, essentiellement pour piloter des moteurs électriques.

**PWR : Power Management**. Désigne toute la partie de l’architecture du CPU dédiée à la gestion de ses modes basse consommation. Les circuits PWR contrôlent notamment les alimentations et offrent les fonctions pour préserver certains registres ainsi que la RTC lorsque le CPU est placé dans des modes avancés d’économie d’énergie.

**Quad SPI** : Evolution du bus SPI capable de plus hauts débits d’informations pour communiquer, notamment, avec les puces de mémoire flash.

**RAM : Random Access Memory**. La mémoire à accès aléatoire ou mémoire vive est rapide et peut-être adressée bit par bit. Elle est nettement plus performante que la mémoire flash mais est disponible en quantité plus réduite du fait de son architecture plus complexe et moins dense. Toutes les informations enregistrées dans une mémoire RAM sont perdues lorsque son alimentation électrique est interrompue.

**RCC : Reset and Clock Control**. Désigne toute la partie de l’architecture du CPU dédiée à la génération et à la distribution de signaux d’horloges et d’alimentations dans le CPU et pour les périphériques du microcontrôleur.

**ROM : Read Only Memory**. Mémoire en lecture seule, qui peut être extrêmement dense car potentiellement encodée dans la structure même du microcontrôleur lors de sa fabrication. Les instructions exécutées par le CPU sont par exemple répertoriées dans une ROM.

**RTC : Real-Time Clock**. Circuit électronique remplissant les fonctions d’une horloge très précise pour des problématiques d’horodatage ou de gestions d’alarmes déclenchées suivant un calendrier. Il et capable de mettre le microcontrôleur en sommeil ou de le réveiller. Afin qu’elle soit aussi précise et stable que possible, la RTC du ST32L476RG doit idéalement utiliser comme source de fréquence les oscillations piézoélectriques à 32kHz d’un cristal de quartz.

**SAI : Serial Audio Interface**. Circuit électronique conçu pour recevoir ou émettre des flux de données audio selon plusieurs formats d’encodage.

**SD/SDIO/MMC : Secure Digital / Secure Digital Input Output / MultiMedia Card**. Contrôleur pour gérer les cartes SD et multimédia ainsi que les cartes d’extension au format SD (Bluetooth, GPS, etc.).

**SIMD DSP : Single Instruction Multiple Data Digital Signal Processor**. Circuit intégré dans le CPU, conçu pour accélérer les opérations arithmétiques les plus couramment requises par les algorithmes de traitement de signaux vidéo ou audio entre autres(et en particulier).

**SoC : System-on-Chip**. Littéralement « Système sur Puce », dont le microcontrôleur STM32L476RG est un parfait exemple. Désigne un système complet supporté par une seule puce (« circuit intégré ») et pouvant comprendre de la mémoire, un ou plusieurs microprocesseurs, des périphériques d'interface ou tout autre composant nécessaire à la réalisation d’une fonction requise.

**SPI : Serial Peripheral Interface**. Contrôleur de bus série fonctionnant selon un protocole inventé par Motorola. Un bus série de ce type permet la connexion, de type maître-esclave, de plusieurs circuits disposant d’interfaces compatibles avec trois fils de liaisons pour les données. Une ligne supplémentaire est requise pour chaque esclave connecté au bus, afin de l'adresser (un seul esclave est activé à chaque transaction sur le bus).

**SWD / JTAG : Serial Wire Debug / Joint Test Action Group**. Interfaces (bus) de communication avec les fonctions de débogage en temps réel intégrées dans l’architecture ARM Cortex.

**SysTick timer** : Timer précis et intégré au CPU, capable de générer une base de temps précise et régulière, dont les performances sont normalisées. Il est essentiellement conçu pour cadencer les éventuels systèmes d’exploitation en temps réel qui pourraient être installés sur le microcontrôleur.

**SWPMI : Single Wire Protocol Master Interface**. Contrôleur implémentant le protocole Single-Wire en modes Full-Duplex et Master. Ce protocole permet d’échanger des informations avec un périphérique compatible (capteur de température, par exemple) en utilisant une seule ligne pour les données (d’où sa dénomination).

**Timer (16/32 bits, basic, low power, general propose, advanced motor control…)**. Circuit électronique basé sur un compteur qui coordonne des fonctions variées (selon son architecture) telles que la lecture de signaux extérieurs variables dans le temps, la génération de signaux modulés en largeur d’impulsion (PWM & contrôle de moteurs électriques), la génération d’évènements périodiques, etc.

**TRNG : True Random Numbers Generator**. Circuit spécialisé dans la génération de séquences binaires réellement aléatoires sur la base d’un processus physique tel que le bruit thermique d’une résistance.

**TSC : Touch Sensing Controller**. Contrôleur capacitif pour les interfaces et écrans tactiles

**ULP UART** : Ultra Low Power Universal Asynchronous Receiver Transmitter. Circuit USART dont l’architecture est optimisée pour réduire autant que possible sa consommation énergétique.

**USART : Universal Synchronous/Asynchronous Receiver Transmitter**. Emetteur-récepteur synchrone ou asynchrone universel. En langage courant, c'est un composant utilisé pour faire la liaison entre le microcontrôleur et l’un de ses ports série.

**USB OTG : Universal Serial Bus On-The-Go**. Contrôleur USB capable de gérer des claviers, des périphériques de stockage, etc.

**WWDG : Windowed Watchdog**. Circuit spécialisé dans la surveillance d’une application. Lorsqu’il est activé et configuré le chien de garde fenêtré provoque un redémarrage (reset) du microcontrôleur chaque fois que le temps d’exécution du programme qu’il surveille est plus court ou plus long que des valeurs spécifiées par le programmeur. 






