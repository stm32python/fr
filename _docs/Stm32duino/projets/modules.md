---
title: Les modules et capteurs externes
description: Les modules et capteurs utilisés par notre station météo
---

# Les modules et capteurs externes

Cette section fait la revue des différents modules et capteurs que nous allons ajouter à notre carte NUCLEO-L476RG pour construire la station météo.

## Le shield X-NUCLEO IKS01A2

Vous trouverez [ici](https://www.st.com/en/ecosystems/x-nucleo-iks01a2.html) la description détaillée du shield environnemental et inertiel X-NUCLEO IKS01A2. Ce shield est une carte d’extension aux formats Arduino et Morpho qui communique avec le microcontrôleur STM32L476RG par le bus I2C numéro 1 (I2C1) ce qui implique qu’il utilise les broches Arduino D15 (soit PB8 pour Morpho) pour la ligne I2C1 SCL et D14 (soit PB9) pour la ligne I2C1 SDA (d’après [l’annexe 1](nucleo_l476rg_pins)).

Si vous ne savez pas ce qu’est un bus I2C (pour « Inter Integrated Circuits ») vous pouvez vous référer à [Wikipedia](https://fr.wikipedia.org/wiki/I2C) ou à [l’annexe 4](glossaire_l476rg). Pour la suite il vous sera utile de garder à l’esprit que :

 1. C’est un bus série, et que nous pourrons y connecter d’autres capteurs et modules au besoin (notamment le capteur de C0<sub>2</sub>), jusqu’à la concurrence théorique de 128 périphériques.

 2. Les différents périphériques connectés au bus I2C **doivent avoir un identifiant unique codé sur 7 ou 8 bits**. Aussi il pourrait arriver que deux périphériques aient par défaut le même identifiant et entrent en conflit. Le script proposé [ici](https://playground.arduino.cc/Main/I2cScanner/) permet de vérifier que tous les identifiants 7 bits sur un même bus I2C sont différents. En cas de conflit (nous ne serons pas confrontés à cette difficulté) il conviendra de consulter les fiches techniques des périphériques et leurs bibliothèques pour savoir comment modifier les identifiants dupliqués.

Pour notre projet nous utiliserons seulement deux capteurs du shield (figure 1.4) :

 * Le capteur piézoélectrique de pression barométrique **LPS22HB** ; vous trouverez **sa bibliothèque STM32duino [ici](https://GitHub.com/stm32duino/LPS22HB)**.
 * Le capteur capacitif d’humidité relative et de température **HTS221** ; vous trouverez **sa bibliothèque STM32duino [ici](https://GitHub.com/stm32duino/HTS221)**.

<br>
<div align="left">
<img alt="Shield X-NUCLEO IKS01A2" src="images/fig_1_4.jpg" width="700px">
</div>
<br>

> Crédit images : [STMicroelectronics](https://www.st.com/content/st_com/en.html)

Le capteur piézoélectrique LPS22HB est un exemple de **technologie MEMS** (système micro-électro-mécanique). Les capteurs MEMS ont révolutionné les applications électroniques depuis une quinzaine d’année et sont à la base – avec des capteurs d’image et de distance sans cesse plus performants – de toutes les fonctions ludiques et avancées des smartphones et des objets connectés en général. Une excellente introduction aux technologies MEMS est disponible [ici](https://eduscol.education.fr/sti/sites/eduscol.education.fr.sti/files/ressources/pedagogiques/5616/5616-les-technologies-mems-ens.pdf). Le shield IKS01A2 comporte deux autres capteurs MEMS « professionnels » :

 * Un accéléromètre + gyroscope 3D, le LSM6DSL
 * Un accéléromètre + magnétomètre 3D, le LSM303AGR

## Le capteur de CO<sub>2</sub>, température et humidité Grove SCD30

Vous trouverez [ici](https://wiki.seeedstudio.com/Grove-CO2_Temperature_Humidity_Sensor-SCD30/) la description détaillée du module I2C Grove pour la mesure de CO<sub>2</sub>, température et humidité (figure 1.5). Il est basé sur un capteur **SENSIRION SCD30** dont la fiche technique est disponible [ici](https://www.sensirion.com/fileadmin/user_upload/customers/sensirion/Dokumente/9.5_CO2/Sensirion_CO2_Sensors_SCD30_Datasheet.pdf). La technologie du SCD30 pour mesurer la concentration de dioxyde de carbone, utilisant **l’absorption de la lumière infrarouge**, est expliquée [ici](https://www.vaisala.com/sites/default/files/documents/CEN-TIA-HVAC-CO2-Measurement-Accuracy-B211311EN-A.pdf). Pour notre projet, nous exploiterons uniquement le capteur de CO<sub>2</sub> du SCD30, les capteurs d’humidité et température du shield IKS01A2 étant plus performants que les siens. 

La **bibliothèque Arduino** que pour le SCD30 est fournie par Sparkfun, [ici](https://GitHub.com/sparkfun/SparkFun_SCD30_Arduino_Library).

<br>
<div align="left">
<img alt="Module Grove SCD30" src="images/fig_1_5.jpg" width="700px">
</div>
<br>

> Crédit images : [Seeed Studio](http://wiki.seeedstudio.com/Grove_System/)

## Le module pour carte SD

On trouve pléthore de **modules pour cartes SD** compatibles avec Arduino et gérés par un **bus SPI** (Serial ou Synchronous Peripheral Interface). Tous ces modules utilisent 6 broches, 2 pour l’alimentation et 4 pour établir la liaison avec le bus (figure 1.6, voir également [cette référence](https://www.aranacorp.com/fr/lire-et-ecrire-sur-une-carte-sd-avec-arduino/). Nous avons sélectionné un module qui peut être alimenté directement en 3,3V qui nous semble plus fiable (à l’usage) que les modèles alimentés en 5V.

Les shields SD pour Arduino ont généralement recours aux broches ICSP des cartes Arduino UNO, ils ne fonctionnent pas avec les cartes NUCLEO qui ne disposent pas de ces broches. C’est aussi pour cela que nous avons choisi un module SPI.

<br>
<div align="left">
<img alt="Module carte SD" src="images/fig_1_6.jpg" width="700px">
</div>
<br>

> Crédit images : [Fritzing](https://fritzing.org/)

Pour une présentation soignée du bus SPI, vous pouvez consulter [cette ressource Eduscol](https://eduscol.education.fr/sti/sites/eduscol.education.fr.sti/files/ressources/techniques/3300/3300-le-bus-spi.pdf). Le bus SPI est un bus série courte portée avec un contrôleur « maître » et autant de modules « esclaves » que possible, la sélection de l’esclave actif se faisant en mettant à l’état « bas » sa broche CS et en laissant à l’état « haut » toutes les broches CS des autres « esclaves ».

Pour notre station météo, **le module pour carte SD sera le seul présent sur le bus SPI**. Il servira de **mémoire temporaire de sauvegarde** pour les données mesurées avec le shield IKS01A2 et pour le module Grove SCD30 (voir chapitre 2). La **bibliothèque Arduino** pour les modules SD est décrite [ici](https://www.arduino.cc/en/reference/SD).

## Le module Bluetooth HC-06

Nous avons sélectionné [le **module Bluetooth esclave HC-06**](https://www.aranacorp.com/fr/arduino-et-le-module-bluetooth-hc-06/) (figure 1.7) pour **sa simplicité de mise en œuvre** et son faible coût. Il dialogue avec le microcontrôleur STM32L476RG via une **connexion série** assurée par [**un contrôleur USART / UART**](https://fr.wikipedia.org/wiki/UART) (pour « Universal Synchronous / Asynchronous Receiver Transmitter »).

Nous utiliserons ce module pour envoyer des commandes à notre station météo. Certaines permettront de changer le nom et mot de passe du réseau Wi-Fi, d’autres les coordonnées géographiques de la station, etc.

<br>
<div align="left">
<img alt="Module Bluetooth HC-06" src="images/fig_1_7.jpg" width="700px">
</div>
<br>

> Crédit images : [Fritzing](https://fritzing.org/)

D’après [Wikipédia](https://fr.wikipedia.org/wiki/Bluetooth), **Bluetooth** est une norme de communication permettant l'échange bidirectionnel de données à très courte distance en utilisant des ondes radio UHF sur une bande de fréquence de 2,4 GHz. Sa vocation est de simplifier les connexions entre les appareils électroniques en supprimant des liaisons filaires **à courte distance** (de 5 à 10 mètres au maximum).

Nous communiquerons avec le module HC-06 en l’appariant avec un ordinateur ou un smartphone dans un premier temps, puis en lui envoyant des commandes sous forme de texte (séquences de caractères) – on parle de **commandes « AT »** - à l’aide d’un logiciel client pour port série tel que le moniteur série de l’IDE Arduino, le programme « Termite » ou encore le programme « PuTTY » (nous y reviendrons dans [cette section](sketchs_stm32duino)).

Avant d’utiliser un module HC-06 il peut être nécessaire **de le configurer**, justement à l’aide de commandes « AT ». Pour notre station météo, nous allons changer le nom du module, nous assurer que sa vitesse de transmission est bien de 9600 bauds et éventuellement changer son code PIN pour l’appairage (qui devrait être par défaut « 1234 »). [L’annexe 2](config_hc06), résumé du tutoriel sur [le site Aranacorp](https://www.aranacorp.com/fr/arduino-et-le-module-bluetooth-hc-06/) déjà mentionné, explique comment réaliser ces opérations à l’aide d’un sketch STM32duino adapté pour la carte NUCLEO-L476RG.

Il est important de noter que pour les objets connectés, la technologie Bluetooth que nous utiliserons est en perte de vitesse au profit du [**Bluetooth basse consommation**](http://tvaira.free.fr/bts-sn/activites/activite-ble/bluetooth-ble.html) (désigné par « BLE » pour « Bluetooth Low Energy ») qui permet une autonomie bien plus grande sur batterie.

En contrepartie de sa frugalité, le BLE repose sur **le protocole GATT** (pour « Generic Attribute Profile ») qui impose la conception d’un logiciel client spécifique sur smartphone, tablette ou PC, ce qui nécessite du travail supplémentaire. Un exemple de telle application pour Android, écrite avec [**MIT App Inventor**](https://appinventor.mit.edu/) est disponible sur notre site [**STM32python**](https://stm32python.gitlab.io/fr/docs/Micropython/BLE/MITAppInventor_1). Par ailleurs, la bonne programmation d’une application gérant correctement GATT reste nettement plus technique et difficile à réaliser que la simplissime et immédiate émulation de port série offerte par le protocole Bluetooth que nous avons choisi.

STMicroelectronics propose plusieurs microcontrôleurs et/ou cartes programmables équipés de modules radio BLE. Des bibliothèques et exemples de mise en œuvre du BLE pour les technologies de STMicroelectronics sont disponibles sur [**le dépôt GitHub de STM32duino**](https://GitHub.com/stm32duino/STM32duinoBLE) notamment pour [**la carte NUCLEO-WB55**](https://www.st.com/en/evaluation-tools/p-nucleo-wb55.html). Remplacer le module HC-06 par [**un shield X-NUCLEO-IDB05A1**](https://www.st.com/en/ecosystems/x-nucleo-idb05a1.html) ou par une carte NUCLEO-WB55 (qui se substituerait alors à la NUCLEO-L476RG) serait un bon exercice pour perfectionner notre station.

## Le module Wi-Fi ESP-01

Notre station météo fera grand usage de la communication Wi-Fi précisément pour :

 1. Se connecter à intervalles de temps réguliers à un [**serveur de temps universel coordonné UTC**](https://fr.wikipedia.org/wiki/Temps_universel_coordonné), via le protocole adapté (il s’agira de NTP pour « Network Time Protocol ») afin de récupérer l’heure sur le méridien de Greenwich. Elle sera ensuite ajustée selon la position géographique de la station et les éventuels décalages d’heure d’été et d’hivers. L’**heure locale** ainsi calculée servira à « recaler » périodiquement l’horloge interne du microcontrôleur (sa RTC pour « Real Time Clock ») ce qui permettra de disposer d’enregistrements précis du moment des mesures.

 2. Se connecter à intervalles de temps réguliers à [**un serveur MQTT**](https://fr.wikipedia.org/wiki/MQTT) (pour « Message Queuing Telemetry Transport »), en l’occurrence [**ThingsBoard**](https://thingsboard.io/), afin d’y publier les mesures de temps, température, humidité et dioxyde de carbone et de les rendre accessibles sur Internet (ou un Intranet) par un navigateur.

 3. Se connecter à intervalles de temps réguliers à [**l’API OpenWeather**](https://openweathermap.org/api) et l’interroger avec une requête http afin d’obtenir les prévisions météo des prochaines 15 heures et l’heure à son emplacement (en redondance avec la fonction évoquée au point 1.

**Nous reviendrons plus en détails sur chacun de ces points dans la [deuxième partie](sketchs_stm32duino) de ce tutoriel**. Pour une application de station météo « à la maison » il ne sera pas forcément nécessaire de développer toutes ces fonctions. Mais notre objectif est avant tout pédagogique et consiste à passer en revue différentes techniques qui vous seront également utiles pour d’autres projets.

Le module Wi-Fi que nous avons retenu est un [**ESP-01**](https://www.electro-info.ovh/esp8266-presentation-du-module-ESP-01) équipé d’un [**microcontrôleur ESPRESSIF ESP8266**](https://www.espressif.com/sites/default/files/documentation/0a-esp8266ex_datasheet_en.pdf). Afin de simplifier sa connexion nous utiliserons également une carte d’adaptation (figure 1.8). Ces deux composants sont disponibles chez de nombreux fournisseurs pour quelques euros.

<br>
<div align="left">
<img alt="Module Wi-Fi ESP-01" src="images/fig_1_8.jpg" width="700px">
</div>
<br>

Les ESP8266 sont des microcontrôleurs très populaires et largement utilisés pour des projets amateurs du fait de leur excellent ratio performances / prix. Les ressources sur Internet expliquant leur programmation foisonnent (par exemple [ce site](https://www.instructables.com/ESP8266-WiFi-Module-for-Dummies/)) mais elles ne traitent généralement que de leur utilisation « standalone ».

Pour notre projet, l’ESP-01 **sera utilisé comme esclave** et le microcontrôleur STM32L476RG le pilotera **par l’intermédiaire de commandes « AT »** qui lui seront adressées à l’aide de l’USART auquel il sera connecté. Des commandes AT permettront également d’ajuster la configuration de l’ESP-01 si nécessaire (voir [annexe 3](config_esp01)).

La bibliothèque Arduino pour piloter l’ESP-01 se nomme *WiFiEsp*, elle est disponible [ici](https://GitHub.com/bportaluri/WiFiEsp). **Pour qu’elle soit compilée sans erreur par STM32duino, il sera nécessaire de modifier légèrement l’un de ses fichiers**. Voici comment procéder : 

 1. Une fois la bibliothèque installée, utilisez l’explorateur de fichiers pour trouver *EspDrv.h*. Il devrait être situé dans l’arborescence *« …\Arduino\libraries\WiFiEsp\src\utility »*. 
    
 2. Editez ce fichier en mode texte (avec l'application Notepad++, par exemple, sous Windows) et ajoutez la ligne suivante à la suite des `#include` qui apparaissent en début de fichier : `#include <stdarg.h>`.

La figure 1.9 montre les 29 premières lignes de *EspDrv.h* après cette correction.

<br>
<div align="left">
<img alt="Corrections à ESPDrv.f de la bibliothèque WiFiEsp" src="images/fig_1_9.jpg" width="700px">
</div>
<br>

## Câblage de l'ensemble des modules externes

Après la présentation des différents modules qui constitueront notre station météo, nous illustrons ici l’emplacement des broches qui seront utilisées pour les connecter à la carte NUCLEO (figure 1.10).

Le choix des broches indiqué ici n’est évidemment pas le seul possible ; il a été établi à partir des informations de [l’annexe 1](nucleo_l476rg_pins). A l’exception du shield X-NUCLEO IKS01A2 placé sur les connecteurs Arduino, toutes les autres broches retenues appartiennent aux connecteurs Morpho.

Pour connecter l’ESP-01, le HC-06 et le module pour carte SD aux broches Morpho, 14 câbles dupont femelle-femelle seront nécessaires. Pour connecter le module Grove SCD30, **un câble Grove - dupont femelle** (de Seeed Studio) sera nécessaire.

<br>
<div align="left">
<img alt="Câblage de l'ensemble des modules externes" src="images/fig_1_10.jpg" width="700px">
</div>
<br>
