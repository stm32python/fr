---
title: Configuration du module HC-06 avec la NUCLEO-L476RG
description: Configuration du module HC-06 avec la NUCLEO-L476RG
---

# Configuration du module HC-06 avec la NUCLEO-L476RG

## Commandes AT du module

Vous aurez peut-être besoin de changer le code PIN de votre module HC-06, ou bien son débit de communication (baudrate). Pour ce faire vous devrez utiliser des commandes « AT ». La liste des commandes « AT » pour le HC-06 est disponible ci-dessous (d’après [cette référence](https://lyceehugobesancon.org/btssn/spip.php?article1152)) :

<br>
<div align="left">
<img alt="Commandes AT d'un module HC-06" src="images/hc06_at.jpg" width="550px">
</div>
<br>

## Configuration du module

Les explications qui suivent sont **un copier-coller du tutoriel [du site aranacorp.com](https://www.aranacorp.com/fr/arduino-et-le-module-bluetooth-hc-06/)**. La configuration du module Bluetooth peut être intéressante pour vérifier le bon fonctionnement du module et pour modifier ses paramètres notamment lorsque vous utilisez plusieurs modules. Le module doit être alimenté mais **non-appairé**. Le script qui suit vous permettra d’envoyer des commandes « AT » sur un module HC-06 connecté aux broches PD2 et PC12 de l’UART5 d’une carte NUCLEO-L476RG.

```c++
HardwareSerial hc06(PD2, PC12);

void setup(){
  
  // Initialise le moniteur série connecté au ST-LINK
  Serial.begin(9600);
  Serial.println("ENTER AT Commands:");
 
  // Initialise le port série du module HC-06
  // Valable pour un module configuré en 9600 bauds par défaut.
  // Si la valeur est différente (ou si vous l’avez changée), changez là !
  hc06.begin(9600);
}

void loop(){
  
  // Ecrit des données depuis le module HC-06
  // vers le moniteur série connecté au ST-LINK
  // (réponses du module HC-06)
  if (hc06.available()){
    Serial.write(hc06.read());
  }
  
  // Ecrit des données depuis le moniteur série 
  // connecté au ST-LINK vers le module HC-06
  if (Serial.available()){
    hc06.write(Serial.read());
  }  
}
```

Pour tester la communication, tapez « AT » dans le monitor série de l’IDE Arduino. Assurez-vous de sélectionner le bon baudrate (9600) et « Pas de fin de ligne » dans les options de communication. Si tout va bien, le module doit répondre « OK ». Si ça ne fonctionne pas vérifiez le branchement et la version du module.

Pour modifier le nom du module, tapez « AT+NAMEnom_module ». Le module devrait répondre « OKsetname ». (Ex: si vous voulez changer le nom du module en « BTM1 » tapez « AT+NAMEBTM1 »).

Pour modifier le code PIN du module, tapez « AT+PINxxxx ». Le module devrait répondre « OKsetPIN ». (Ex: si vous voulez changer le PIN en « 0000 » tapez « AT+PIN0000 »).

Pour modifier la vitesse de communication du module (seulement si nécessaire), tapez « AT+BAUDx ». Ex: si vous voulez changer le baudrate en 9600 tapez « AT+BAUD4 ». Le module devrait répondre  « OK9600 ». (Note: 1 pour 1200, 2 pour 2400, 3 pour 4800, 4 pour 9600, 5 pour 19200, 6 pour 38400, 7 pour 57600, 8 pour 115200)

**ATTENTION** : Différentes versions du module HC-06 existent et la liste des commandes « AT » peut varier. Vérifiez bien le numéro de série écrit sur le module et la version du firmware en tapant la commande « AT+VERSION ». Par exemple, le module HC-06 labellisé « ZS-040 » avec la version 3.0-20170609 retourne « ERROR(0) » lorsqu’on lui envoie la commande « AT+NAMExxxx » (avec « xxxx » le nouveau nom choisi pour le module). Les commandes « AT » pour ce module sont :

 * « AT+NAME=xxxx » pour configurer le nom du module
 * « AT+PSWD: xxxx » pour configurer le mot de passe du module
 * « AT+UART=115200,0,0 » pour configurer le baudrate

## Appairage du module

Une fois la configuration du module effectuée, vous pouvez **appairer** le module HC-06 avec le système de votre choix comme n’importe quel périphérique Bluetooth. Sélectionnez le nom dans la liste des périphériques détectés (par défaut : HC-06) et entrez le code PIN que vous avez choisi (par défaut: 1234). Lorsque c’est fait, la LED présente sur le module doit **cesser de clignoter**.

