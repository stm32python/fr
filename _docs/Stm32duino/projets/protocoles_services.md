---
title: Connectivité, protocoles et services
description: Vue d'ensemble de la connectivité, des protocoles et des services utilisés par notre station météo
---

# Connectivité, protocoles et services

La figure ci-dessous, résume la connectivité, les protocoles réseaux et les services Internet utilisés par notre station météo que nous nous proposons de construire :

<br>
<div align="left">
<img alt="Connectivité, protocoles et services" src="images/fig_1_15.jpg" width="750px">
</div>
<br>

Ainsi...

 * Le Bluetooth permettra d’envoyer des commandes à la station, sous la forme de chaînes de caractères, par une liaison série. Un smartphone avec une application émulateur de port série pourra donc lui servir de télécommande.

 * Une autre liaison série, celle qui passe par le ST-LINK, (éventuellement) connectée à un ordinateur personnel, permettra à la fois de collecter les messages de débogage de la station et de l’alimenter en électricité.

 * Une liaison Wi-Fi permettra à notre station de communiquer, à travers une Gateway (votre « BOX Internet »), vers Internet et les trois services qu’elle utilisera :

  1. [**NTP**](https://fr.wikipedia.org/wiki/Network_Time_Protocol) pour régler la RTC ;
  2. [**ThingsBoard**](https://thingsboard.io/) pour publier ses mesures de température, pression, humidité et CO<sub>2</sub> ;
  3. [**OpenWeather**](https://openweathermap.org/) pour récupérer les prévisions météo.

L’interrogation de ces services nécessitera l’utilisation de trois protocoles Internet différents : [**UDP**](https://fr.wikipedia.org/wiki/User_Datagram_Protocol), [**HTTP**](https://fr.wikipedia.org/wiki/Hypertext_Transfer_Protocol) et [**MQTT**]( https://fr.wikipedia.org/wiki/MQTT).

La [deuxième partie de ce tutoriel]() présente en détail la mise en œuvre logicielle de ces protocoles et précise le code informatique (des sketchs Arduino écrits en C++) permettant d’interroger les services évoqués et de récupérer leurs réponses.
