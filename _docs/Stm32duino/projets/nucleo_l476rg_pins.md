---
title: Mappage périphériques - broches pour la NUCLEO-L476RG
description: Revue du mappage périphériques internes du microcontrôleur STM32L476RG sur les broches de la NUCLEO-L476RG
---

# Mappage périphériques - broches pour la NUCLEO-L476RG

La présente annexe donne une représentation graphique des mappages des périphériques du microcontrôleur STM32L476RG (et / ou des broches pour la carte NUCLEO-L476RG dans le cadre de STM32duino). Ces figures illustrent le contenu du fichier *PeripheralPins.c* du dépôt GitHub [core STM32](https://github.com/stm32duino/Arduino_Core_STM32).

Veuillez noter que cette correspondance n’est pas imposée « en dur » : il est possible de rediriger les périphériques internes du microcontrôleur vers d’autres broches de son boitier – et donc de la carte NUCLEO. Cette reconfiguration, qui sort du cadre de la programmation STM32duino, est possible avec [les bibliothèques LL et HAL](https://community.st.com/s/question/0D50X00009XkhQSSAZ/hal-vs-ll) et est facilitée par l’utilisation de l’outil de configuration graphique [STM32Cube MX](https://www.st.com/en/development-tools/stm32cubemx.html).

## Broches pour le bouton et la LED utilisateur

<br>
<div align="left">
<img alt="Broches pour le bouton et la LED utilisateur" src="images/fig_a1_1.jpg" width="650px">
</div>
<br>

## Broches pour les six contrôleurs de ports séries

<br>
<div align="left">
<img alt="Broches pour les six contrôleurs de ports séries" src="images/fig_a1_2.jpg" width="650px">
</div>
<br>

## Broches pour les trois contrôleurs I<sup>2</sup>C

<br>
<div align="left">
<img alt="Broches pour les trois contrôleurs I2C" src="images/fig_a1_3.jpg" width="650px">
</div>
<br>

## Broches pour les trois contrôleurs SPI

<br>
<div align="left">
<img alt="Broches pour les trois contrôleurs SPI" src="images/fig_a1_4.jpg" width="650px">
</div>
<br>

## Broches pour les vingt-sept canaux PWM

<br>
<div align="left">
<img alt="Broches pour les vingt-sept canaux PWM" src="images/fig_a1_5.jpg" width="650px">
</div>
<br>

## Broches pour les douze canaux ADC

<br>
<div align="left">
<img alt="Broches pour les douze canaux ADC" src="images/fig_a1_6.jpg" width="650px">
</div>
<br>



