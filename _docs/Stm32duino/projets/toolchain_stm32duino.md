---
title: Les chaînes de compilation et le modèle STM32duino
description: Présentation des chaînes de compilation et du modèle STM32duino
---

# Les chaînes de compilation et le modèle STM32duino

## Les chaînes de compilation

Les environnements de développement intégrés de Arduino, de ARM (Keil µVision, MBED online, MBED Studio), de STMicroelectronics (STM32 Cube IDE) … font tous appel « en coulisse » à un ensemble d’outils logiciels nommé [**« chaîne de compilation »**](https://fr.wikipedia.org/wiki/Chaîne_de_compilation) (en anglais : « toolchain »).

Les composants logiciels peuvent varier d’une chaîne de compilation à l’autre, mais la structure d’une chaîne de compilation est quasiment la même pour toutes les offres. La figure illustre une chaîne de compilation « standard » pour microcontrôleurs équipés de cœurs ARM.

<br>
<div align="left">
<img alt="Chaînes de compilation pour STM32duino" src="images/fig_1_16.jpg" width="750px">
</div>
<br>

> Source image : *The Definitive Guide to ARM Cortex-M3 and Cortex-M4 Processors (Third edition), Yiu Joseph, ISBN : 9780124080829*

Le sketch STM32duino correspond au **code source, écrit en langage C++** (son extension est « .ino » mais il s’agit bien d’un « .cpp »). Parmi les autres sources on peut avoir du code **écrit directement en langage d’assemblage** (fichiers « .s ») du Cortex M4.

Le sketch sera ensuite traduit en instructions binaires, du **langage machine**, par le **compilateur** ([gcc et G++](https://fr.wikipedia.org/wiki/GNU_Compiler_Collection) dans notre cas). Les sources en assembleur seront également converties en langage machine, par **l’assembleur** ([gas](https://fr.wikipedia.org/wiki/GNU_Assembler) dans notre cas). Les résultats de ces opérations seront enregistrés dans différents **fichiers objets** (« .o ») qui contiennent donc du code binaire ainsi que des références aux fonctions et objets utilisés dans les fichiers « .s » et « .cpp » d’origine.

C’est ensuite le rôle de **l’éditeur de liens** ou **linker** de fusionner tous ces fichiers objets en un seul fichier « .elf » contenant l’image exécutable – appelée **firmware** – de notre sketch STM32duino. Le linker remplace les références aux fonctions et objets dans les fichiers « .o » par des adresses des registres et de la mémoire du microcontrôleur cible. Pour cela il utilise **un fichier de cartographie de la mémoire** qui recense les différentes zones de la mémoire du microcontrôleur cible et contient des directives sur l’utilisation de ces dernières.

Le firmware est enfin copié dans la mémoire flash du microcontrôleur par **un programmeur**, dans notre cas **le module ST-LINK de la carte NUCLEO**. Le microcontrôleur est ensuite redémarré et sa séquence de reset lance l’exécution du programme encodé dans le firmware. 

L’IDE Arduino ne propose pas (encore) de fonctions de **débogage en temps réel** du firmware. Un debugger permet – entre autres fonctions – « d’espionner » au cours de l’exécution du firmware l’évolution de ses variables dans la mémoire du microcontrôleur afin de traquer les erreurs de programmation.

## Le modèle STM32duino

Le modèle STM32duino est illustré par la figure 1.17. Bien qu’elles ne soient pas indispensables pour utiliser STM32duino, les références qui suivent vous en apprendront un peu plus sur l’architecture de cette API. Ceci vous sera utile si vous décidez d’utiliser des fonctions spécifiques à l’architecture STM32, via les API HAL et LL, pour dépasser les limitations de l’API Arduino, comme nous le ferons ponctuellement dans le sketch de notre station météo.

Toutes ces API sont écrites en langages C ou C++, qui restent **de très loin** les langages les plus utilisés par les professionnels pour la programmation des SoC.

<br>
<div align="left">
<img alt="Le modèle STM32duino" src="images/fig_1_17.jpg" width="750px">
</div>
<br>

Concernant les différentes "couches" logicielles, vous trouverez des informations plus détaillées concernant...
 - CMSIS sur [cette page](https://www.keil.com/pack/doc/CMSIS/General/html/index.html)
 - STM32 Cube sur [cette page](https://www.st.com/en/ecosystems/stm32cube.html)
 - Les API HAL et LL pour les microcontrôleurs STM32L4 sur [cette documentation](https://www.st.com/resource/en/user_manual/dm00173145-description-of-stm32l4l4-hal-and-lowlayer-drivers-stmicroelectronics.pdf)   
