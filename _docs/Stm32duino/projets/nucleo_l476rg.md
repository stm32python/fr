---
title: La carte NUCLEO-L476RG
description: Revue de la carte NUCLEO-L476RG
---

# La carte NUCLEO-L476RG

## Description

Le cœur de notre station sera constitué d’une **carte NUCLEO-L476RG de STMicroelectronics** (figure 1.1). Vous trouverez sa description détaillée et sa fiche technique [ici](https://www.st.com/en/evaluation-tools/nucleo-l476rg.htm). Cette carte est équipée d’un **microcontrôleur STM32L476RG** cadencé à 80 MHz offrant **1 Mb de mémoire Flash**, 128 kb de SRAM et **un grand nombre d’entrées-sorties**.

Notre station météo utilisera un bus I2C, un bus SPI, deux contrôleurs série (UART/USART) et un nombre important de broches d’alimentation (3V3 et 5V) et de masses (GND). Grace aux **connecteurs Arduino** (figure 1.2) et surtout **aux connecteurs Morpho** (figure 1.3) nous aurons assez de broches à notre disposition pour pouvoir nous passer d’une platine d’expérimentation (« breadboard ») et d’une alimentation externe.

**Nous alimenterons la carte NUCLEO par le port USB qui sert également à la programmer** (relié au ST-LINK). Cette approche n’optimisera pas la consommation d’énergie, mais elle aura le mérite de la simplicité. Contrôler parfaitement la consommation de la station nécessite des compétences solides en programmation embarquée que ce tutoriel pour néophytes ne pourra pas vous apporter.

<br>
<div align="left">
<img alt="Cartes NUCLEO 64" src="images/fig_1_1.jpg" width="700px">
</div>
<br>

> Crédit images : [STMicroelectronics](https://www.st.com/content/st_com/en.html)

Le cristal de quartz a ici la même utilité que dans les montres ; il s’agit d’un oscillateur piézoélectrique très stable qui fournit une « source d’horloge » de qualité pour le microcontrôleur. En l’absence de quartz, on peut utiliser comme références de fréquence des oscillateurs (i.e. des circuits RLC) intégrés dans le STM32L476RG.

## Les connecteurs de la carte NUCLEO-L476RG

Le microcontrôleur STM32L476RG propose un grand nombre de fonctions et 64 broches sur son boitier, qui ne sont donc pas toutes accessibles par les 32 broches des connecteurs Arduino (figure 1.2). Pour notre projet, les connecteurs Arduino seront utilisés avec le **shield X-NUCLEO IKS01A2** (présenté plus loin). Pour tous les autres capteurs et modules radiofréquence nous utiliserons des broches des connecteurs Morpho (figure 1.3).

Dans le sketch de notre station, nous utiliserons **les désignations des broches Morpho**. Plusieurs précisions importantes s’imposent pour lire correctement les figures 1.3 et 1.4 :

 1. **Les broches Arduino dupliquent certaines des broches Morpho**. Par exemple, la broche Arduino D1 (broche numéro 2 du connecteur CN9) est connectée à la broche Morpho PA2 (broche 35 du connecteur CN10). De même, la broche Arduino D3, quatrième du connecteur CN9, offre une fonction PWM et est connectée à la broche Morpho PB3 qui expose donc la même PWM.

 2.	**Toutes les broches Morpho ne sont** (évidemment) **pas connectées à des broches Arduino**. C’est notamment le cas pour celles de la colonne de droite pour CN10 et de la colonne de gauche pour CN7. Par exemple, la broche Morpho AGND (broche 32 du connecteur CN10) n’est reliée à aucune broche Arduino. De même pour les broches PD1, PD0, VDD … de CN7. 

 3.	**La liste exhaustive des fonctions du microcontrôleur** qui sont câblées sur les broches Morpho de la carte NUCLEO-L476RG, dans le cadre de STM32duino, est disponible dans le fichier *PeripheralPins.c* situé – sous MS Windows et pour la release 2.4.0 de STM32 Cores  – à cet emplacement :

 > *C:\Users\user\AppData\Local\Arduino15\packages\STMicroelectronics\hardware\stm32\2.4.0\variants\STM32L4xx\L496R(E-G)T_L4A6RGT\PeripheralPins.c*

 Où *user* est le nom de votre compte Windows. La lecture de ce fichier étant fastidieuse, une représentation graphique en est donnée par les figures de [l’annexe 1](nucleo_l476rg_pins).

<br>
<div align="left">
<img alt="Connecteurs cartes NUCLEO 64" src="images/fig_1_2_fig_1_3.jpg" width="700px">
</div>
<br>

> Crédit images : [STMicroelectronics](https://www.st.com/content/st_com/en.html)
