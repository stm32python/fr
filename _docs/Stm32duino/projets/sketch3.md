---
title: Utiliser le mode Basse Consommation
description: Le sketch Arduino final pour utiliser le mode Basse Consommation
---

# Utiliser le mode Basse Consommation

> L'ensemble des sketchs de notre projet peuvent être [**téléchargés ici**](meteo.zip).

**Le skech « low_power.ino »** est une légère évolution de « write_sd.ino », qui apporte essentiellement les modifications requises pour gérer la temporisation de loop avec le mode « Low Power » du microcontrôleur. 

 - Dans la partie *déclarations* globales nous avons changé la valeur attribuée à *SET_REC_MOD*, qui passe à 10. 

 - Dans la fonction *setup*  nous avons rajouté l’instruction pour initialiser le mode Low Power à la ligne 121 :

```c++
118   // Démarre le mode basse consommation
119   Serial.println("Low power mode activated");
120   delay(100);
121   LowPower.begin();
```

 - La fonction *loop* devient :

```c++
127 void loop() {
128 
129   if (rec_mod == SET_REC_MOD) { // Toutes les SET_REC_MOD itérations...
130 
131     rec_mod = 0;
132 
133     // Allume la LED utilisateur
134     digitalWrite(LED_BUILTIN, HIGH);
135 
136     // Lecture des capteurs environnementaux
137     Read_Sensors(&Serial);
138 
139     // Construit un objet "String" nommé "payload" à partir des mesures
140     String payload = String(measure.temperature) + ";" + String(measure.pressure)
141                       + ";" + String(measure.humidity);
142 
143     // Envoie le contenu de payload dans le tableau de caractères Record_Payload
144     payload.toCharArray( Record_Payload, RECORD_PAYLOAD_SIZE );
145 
146     // Ecris le contenu de Record_Payload comme nouvelle ligne dans logfilename
147     SD_Write_Record(logfilename, Record_Payload);
148 
149     // Affichage des mesures sur le port série du ST-LINK
150     Serial.println("Payload : " + payload);
151 
152     // Eteint la LED utilisateur
153     digitalWrite(LED_BUILTIN, LOW);
154   } // Clôture de if (rec_mod == SET_REC_MOD)
155 
156   rec_mod++;
157 
158   if (sd_card_listing){ // Liste des enregistrements sur la carte SD
159     LogFile_List(logfilename, &Serial);
160     sd_card_listing = false;
161   }
162 
163   // Mise en veille (mode "STOP") avant l'itération suivante
164   delay(5);
165   LowPower.deepSleep(MAIN_LOOP_DELAY);
166 }
```

Dans cette nouvelle version, les mesures et les enregistrements sur la carte SD s’effectuent tous les *SET_REC_MOD* = 10 tours de boucle. Sachant que chacun dure *MAIN_LOOP_DELAY* = 10 s cela fait donc une période de lecture et enregistrement de 100 secondes.

La ligne 165 **place le microcontrôleur en mode sommeil** pendant *MAIN_LOOP_DELAY* ms. Dans cet état, il ne fait presque plus rien et ne consomme pratiquement pas. Il se réveillera après *MAIN_LOOP_DELAY* ms **ou bien lorsque surviendra une interruption externe**, du type de l’appui sur le bouton utilisateur.

Ainsi, lorsqu’on appuie sur le bouton utilisateur alors que le microcontrôleur est endormi, le NVIC le réveille et demande l’exécution de *Button_Down_ISR*. La boucle loop va ensuite se dérouler et, lorsqu’elle arrivera à l’instruction *LowPower.deepSleep(MAIN_LOOP_DELAY)* le microcontrôleur se mettra de nouveau en sommeil. On a gagné sur tous les plans : notre station météo reste réactive aux sollicitations de l’utilisateur et ne consomme de l’énergie que lorsqu’elle accomplit une tâche ayant une plus-value.
