---
title: Installer et prendre en main l'IDE Arduino et les packages pour STM32 sous Windows
description: Comment configurer l'environnement Arduino pour STM32 sous Windows
---

# Installer et prendre en main l'IDE Arduino et les packages pour STM32 sous Windows

L'IDE Arduino est sans doute l'une des plus populaires pour l'apprentissage de la programmation embarquée. Elle est facile à installer, très épurée et efficace.

Dans cette section nous rappelons comment :
 1. Installer l'IDE Arduino et les bibliothèques pour les cartes et microcontrôleurs de STMicroelectronics.
 2. Configurer l'IDE Arduino pour développer un projet avec une carte donnée.
 3. Manipuler les principales fonctions de l'IDE Arduino.
 4. Ouvrir un code source de programme (un "sketch" ou un "croquis" dans le jargon Arduino), le compiler en firmware et l'installer dans le microcontrôleur qui anime la carte.

Après de longues années de bons et loyaux services, la version 1 de l’IDE Arduino accusait de sérieux retards sur ses fonctionnalités par comparaison avec d’autres alternatives gratuites développées depuis son apparition. Résultat d'une longue phase de développement et de test, **l’IDE Arduino version 2 est enfin disponible** et comble ces lacunes. Elle apporte notamment **l’auto-complétion** du code en cours de frappe et la possibilité de **déboguer le firmware compilé pas à pas** (à l'aide de l'outil [gdb](https://www.sourceware.org/gdb/)).

La documentation "officielle" du débogueur est [**ici**](https://docs.arduino.cc/software/ide-v2/tutorials/ide-v2-debugger). Un tutoriel dédié à cet outil pour son utilisation avec STM32duino est par ailleurs accessible par [ce lien](../Stm32duino/debogueur).

## Etape 1 : Téléchargement et installation de l’IDE Arduino sous Windows

Téléchargez et installez la dernière version de l'IDE Arduino depuis le lien [https://www.arduino.cc/en/software](https://www.arduino.cc/en/software) :

<br>
<div align="left">
<img alt="Page de téléchargement IDE Arduino" src="exercices/images/1_installation/arduino_IDE_download.png" width="700px">
</div>
<br>

Dans l’encart de droite, sélectionnez : *Windows ZIP file*. Vous pouvez choisir *Just Download* ou *Contribute & Download* (si vous souhaitez contribuer financièrement). 

**NB** : Si vous avez des droits d’administration sur le PC cible, nous vous conseillons de télécharger l’installeur par le lien *Windows, Win 10 and newer, 64 bits*.

Dézippez le dossier  *arduino-ide_2.x.x_Windows_64bits.zip* à l’emplacement souhaité (ou lancez l’installeur *arduino-ide_2.x.x_Windows_64bits.exe* selon votre choix à l’étape 1) puis lancez *arduino.exe* depuis le répertoire ainsi créé (ou cliquez sur le raccourci *Arduino IDE* sur votre bureau Windows).

## Etape 2 – Téléchargement et installation du support des cartes ST dans l’IDE Arduino

Une fois l'application *arduino.exe* lancée, allez dans le menu *Fichiers / Préférences*, volet *Paramètres* puis collez dans la boite *URL de gestionnaire de cartes supplémentaires* l’adresse suivante :

> **https://GitHub.com/stm32duino/BoardManagerFiles/raw/main/package_stmicroelectronics_index.json**

**NB** : Il est possible que cette adresse ait été modifiée depuis la rédaction de ces lignes. Si elle s'avère incorrecte vous pourrez trouver sa version active sur [la page GitHub de STM32duino, ici](https://GitHub.com/stm32duino/Arduino_Core_STM32#getting-started).

La fenêtre *Préférences*, onglet *Paramètres* de l’IDE Arduino devrait avoir l’aspect suivant :

<br>
<div align="left">
<img alt="Page de préférences IDE Arduino" src="exercices/images/1_installation/arduino_IDE_preferences.png" width="650px">
</div>
<br>

L'onglet *Paramètres* offre un ensemble d’autres options de configuration importantes ; nous vous laissons les explorer (elles s’expliquent assez bien d’elles-mêmes ; nous y reviendrons par la suite). Notamment, paramétrez la version de l'IDE en français si nécessaire, pour suivre ce tutoriel (comme nous l'avons fait).

**NB** : A la date de rédaction de ce tutoriel, de nombreuses lacunes demeurent dans la version française de l'IDE, un certain nombre de messages d'erreur, d'entrées de menus et de labels de l'interface graphique sont encore en anglais.


## Etape 3 – Configuration d’un éventuel proxy

- Si votre connexion Internet passe par un serveur proxy sélectionnez ensuite l'onglet *Réseaux*, cochez la case *Détecter automatiquement* et renseignez le proxy avec la valeur qui convient. La configuration de cette page dépend des modalités d’administration de votre réseau local, nous vous conseillons donc de vous rapprocher de votre administrateur réseau.

- Si votre connexion Internet ne passe par un serveur proxy la configuration par défaut (vide !) ci-après fera l’affaire :

<br>
<div align="left">
<img alt="Page de proxy IDE Arduino" src="exercices/images/1_installation/arduino_IDE_no_proxy.png" width="650px">
</div>
<br>

## Etape 4 – Installation du package STM32 MCU based boards

Il faut à présent télécharger l'ensemble des bibliothèques et définitions (essentiellement des codes sources en langages C et C++) pour les cartes et microcontrôleurs de STMicroelectronics.

Allez dans le menu *Outils / Carte / Gestionnaire de carte*. Un volet vertical intitulé *GESTIONNAIRE DE CARTE* s'ouvre à droite, avec une zone de recherche en haut. Tapez *STM32* dans celle-ci, à la place de *Filtrez votre recherche*. Le filtre devrait vous proposer une entrée intitulée *STM32 based boards par STMicroelectronics* comme illustré ci-dessous : 

<br>
<div align="left">
<img alt="Page gestionnaire de cartes IDE Arduino" src="exercices/images/1_installation/arduino_IDE_boards_manager.png" width="800px">
</div>
<br>

L'autre package affiché, intitulé *STM8 MCU based boards par STMicroelectronics* correspond au support des microcontrôleurs 8 bits de la famille STM8, une architeture propriétaire de STMicroelectronics. Ne l'installez pas.

Si nécessaire installez la dernière version du package *STM32 based boards par STMicroelectronics* en cliquant sur son bouton *INSTALLER* ; cela peut prendre jusqu’à 20 minutes selon le débit de votre connexion Internet. 

Si tout s’est bien déroulé, vous devriez voir affiché *INSTALLED* sur fond bleu comme sur le cas de notre capture d'écran ci-dessus et un message le confirmant dans la zone de messages *Sortie*. Dans notre cas, nous venons d'installer les définitions de cartes 2.4.0, le message *Platform STMicroelectronics:stm32@2.4.0 installed*, dans l'onglet *Sortie*, confirme que l'opération s'est déroulée correctement.

S’il y a un problème (fréquemment à cause d'un proxy mal configuré, d'un blocage par un pare-feu zélé ou  un antivirus intolérant) l’IDE Arduino affichera des messages d’erreur dans la zone de messages de l'onglet *Sortie*.
Le cas échéant, nous vous conseillons de vous rapprocher de votre administrateur informatique.


## Etape 5 – Vérification du bon accès aux bibliothèques

Il faut à présent vérifier que vous pourrez télécharger sans difficultés les bibliothèques dont vous aurez besoin pour programmer par la suite.

Allez dans le menu *Croquis / Importer une bibliothèque / Gérer des bibliothèques…*. Un volet vertical intitulé *GESTIONNAIRE DE BIBLIOTHEQUE* s'ouvre à droite, avec une barre de recherche en haut. Entrez *STM32duino* dans cette barre de recherche pour sélectionner toutes les bibliothèques Arduino traitant de STM32duino.
Si vous obtenez des réponses et pas de message d’erreur, tout va bien :

<br>
<div align="left">
<img alt="Page gestionnaire de bibliothèques IDE Arduino" src="exercices/images/1_installation/arduino_IDE_libraries_manager.png" width="800px">
</div>
<br>

Si cette étape est compromise, des messages d'erreur s'afficheront dans la fenêtre *Sortie*.
Le cas échant, nous vous conseillons de vous rapprocher de votre administrateur informatique.


## Etape 6 - Installation d'une bibliothèque

Nous allons à présent installer une bibliothèque contenant les drivers et les exemples pour la carte d'extension (ou shield) [X-NUCLEO-IKS01A3](https://www.st.com/en/ecosystems/x-nucleo-iks01a3.html).

Saisir *IKS01A3* dans la barre de recherche du gestionnaire de bibliothèque. Une seule bibliothèque est proposée, intitulée *STM32duino X-NUCLEO-IKS01A3 expansion board*. Cliquez sur le bouton *INSTALLER*.
Une fenêtre	intitulée *Install library dependencies* s'ouvre alors pour signaler que son installation nécessite également celle de tout un ensemble d'autres bibliothèques dont elle aura besoin pour fonctionner correctement.<br>
Votre écran devrait afficher à peu près ceci :

<br>
<div align="left">
<img alt="Page gestionnaire de bibliothèques IDE Arduino" src="exercices/images/1_installation/arduino_IDE_boards_manager_dependencies.png" width="800px">
</div>
<br>

Bien évidemment, il faut approuver en cliquant sur *INSTALL ALL*.<br>
L'onglet *Sortie* affiche les messages de déroulé de l'installation, il devrait confirmer que chacune des bibliothèques sélectionnées a bien été téléchargée et installée. Dans le cas contraire, *Sortie* listera ce qui s'est mal passé via des messages d'erreur et nous vous conseillons à nouveau de vous rapprocher de votre dévoué administrateur informatique.


# Configurer l’IDE Arduino pour programmer la carte NUCLEO-WB55RG

Pour les besoins de ce tutoriel, nous avons choisi de configurer l'IDE Arduino pour programmer une carte de prototypage de STMicroelectronics de la famille *NUCLEO*, la [NUCLEO-WB55RG](https://os.mbed.com/platforms/ST-Nucleo-WB55RG/). La procédure serait semblable si vous choisissiez une autre carte de STMicroelectronics, aux détails d'architecture près (la localisation du connecteur USB *ST-LINK* essentiellement).

## Etape 1 : Sélection du groupe de cartes NUCLEO

Lancez l’IDE Arduino, puis allez dans le menu *Outils*. Cliquez sur *Carte*, puis *STM32 MCU based board* et enfin sur *Nucleo-64*.

<br>
<div align="left">
<img alt="Menu sélection de groupe de cartes IDE Arduino" src="exercices/images/1_installation/arduino_IDE_select_nucleo64.png" width="800px">
</div>
<br>

## Etape 2 : Sélection de la carte NUCLEO-WB55RG

Retournez dans le menu *Outils*. Cliquez sur *Board part number*. Dans le menu qui s’affiche, sélectionnez enfin le modèle de la carte qui nous concerne, à savoir la *P-Nucleo-WB55RG*.

<br>
<div align="left">
<img alt="Menu sélection de carte IDE Arduino" src="exercices/images/1_installation/arduino_IDE_select_nucleo_WB55_node.png" width="800px">
</div>
<br>

## Etape 3 : Connexion de la carte à l'IDE via le ST-Link

Commencez par brancher la carte NUCLEO-WB55RG sur un port USB de votre PC. Pour cela vous aurez besoin d'un câble USB A (ou C) à l'une de ses extrémités (connexion au PC) et mini-USB à l'autre (connexion à la carte NUCLEO).<br>
La carte disposant de deux connecteurs mini-USB, prenez soin d'utiliser celui qui est sérigraphié *USB ST-LINK* comme indiqué ci-dessous. Assurez vous également que le cavalier de sélection de l'alimentation de la carte (signalé en rouge) est positionné sur *USB STL* :

<br>
<div align="left">
<img alt="Connexion ST-LINK Nucleo-WB55RG" src="exercices/images/1_installation/ST_LINK_Nucleo_WB55RG.png" width="350px">
</div>
<br>

Si tout est correctement connecté, LED5 et LED6 (sur la carte) s'allument et un nouveau lecteur de disque amovible nommé *NOD_WB55RG* apparaît dans l'explorateur de fichiers de Windows (image ci-dessous). Ce lecteur de disque virtuel est contenu dans la mémoire flash du composant ST-LINK de la carte et contient deux fichiers : *DETAILS.TXT* (version du firmware du *ST-LINK*) et *MBED.HTM* (un lien vers la description détaillée de la carte sur le site *os.mbed.com*).

<br>
<div align="left">
<img alt="Le lecteur NOD_WB55RG et son contenu" src="exercices/images/1_installation/NOD_WB55RG_content.png" width="550px">
</div>
<br>

Retournez dans le menu *Outils*. Allez sur la ligne *Port* et sélectionnez dans le menu déroulant le port qui s’affiche (*COM14* dans notre cas, mais votre IDE Arduino affichera probablement un autre port). Si tout est OK, **il doit être coché** :

<br>
<div align="left">
<img alt="Connexion ST-LINK Nucleo_WB55RG" src="exercices/images/1_installation/arduino_IDE_port_COM.png" width="800px">
</div>
<br>

Il est aussi possible que votre ordinateur propose **plusieurs ports COM** dans la liste, dont un seul sera attribué à la carte. Dans ce cas, le moyen le plus simple pour identifier le port COM de votre carte consistera à repérer quel port apparait et disparait du menu ci-dessus lorsqu’on débranchera et rebranchera le câble USB. 

## Etape 4 : Sélection de l'Upload method

Si vous souhaitez utiliser le débogueur, il vous faudra procéder également **à l'installation de [STM32CubeProgrammer](https://www.st.com/en/development-tools/stm32cubeprog.html)**, comme expliqué par [ce tutoriel](../tools/cubeprog/index).
Dans le menu *Outils*, vérifiez que l'option du mode de chargement, dans *Upload method*, du firmware est bien validée sur *STM32CubeProgrammer (SWD)* :

<br>
<div align="left">
<img alt="Upload STM32CubeProgrammer (SWD) Nucleo_WB55RG" src="exercices/images/1_installation/arduino_IDE_upload_method.png" width="800px">
</div>
<br>

Si l'utilisation du débogueur ne vous intéresse pas, sélectionnez plutôt *Mass storage* comme *Upload method*.

# Présentation de l'IDE Arduino 2

Cette section est essentiellement une version française et écourtée du tutoriel officiel, en langue anglaise, disponible [**ici**](https://docs.arduino.cc/software/ide-v2/tutorials/getting-started-ide-v2).

L'IDE Arduino dans sa révision 2 est très épurée par comparaison avec les autres environnements de développement intégrés. La figure qui suit liste ses principales fonctions :

<br>
<div align="left">
<img alt="Description IDE Arduino" src="exercices/images/1_installation/desc_GUI_arduino_IDE.png" width="800px">
</div>
<br>

## Légende

 * **(1)** Bouton *Vérifier*<br>
  Lorsque vous appuyez sur ce bouton, le programme écrit dans votre sketch est transformé en instructions binaires par le compilateur gcc, puis il est « lié » pour fournir un fichier appelé *Firmware*. Pour plus d'explications sur ces mécanismes, nous vous renvoyons à [cet article](https://stm32python.gitlab.io/fr/docs/Microcontrollers/programmer).

 * **(2)** Bouton *Téléverser*<br>
   Réalise les mêmes opérations que le bouton *Vérifier* et transfère finalement le firmware dans le microcontrôleur de la carte sélectionnée et connectée.

 * **(3)** Bouton *Débogage*<br>
  Lance le débogage pas à pas du croquis chargé, sous réserve que le débogueur soit configuré et qu'une carte soit connectée.

 * **(4)** Sélecteur de port et de carte<br>
   Liste les ports COM actifs et les cartes éventuellement connectées dessus, suite à un processus de détection automatique. Permet de sélectionner rapidement une carte parmi toutes celles branchées sur votre PC.

 * **(5)** Bouton *Traceur série*<br>
  Affiche ou masque la fenêtre du *Traceur série*. Comme pour le moniteur série (voir plus loin), il faut préciser le débit du port série connecté au ST-LINK de la carte (en bauds). Le traceur série tracera ensuite les informations renvoyées par la carte sous forme de courbe(s) si elles sont correctement formattés dans le sketch concerné.<br>

 * **(6)** Bouton *Moniteur série*<br>
   Le moniteur série est connecté au ST-LINK pour afficher les messages générés par les appels à `Serial.print(...);` (et ses variantes). La fenêtre du moniteur série apparaît comme un nouvel onglet dans la zone de messages (13), intitulé *Moniteur série*.<br> La copie d'écran ci-dessous montre ce que cela donne pour notre carte connectée au port COM14.<br> Le *débit du port série* doit être précisé (ici *9600 bauds*), ainsi que le format du séparateur de lignes (ici *Nouvelle ligne*). Ces valeurs doivent être les mêmes que celles écrites dans le sketch Arduino autrement l'affichage sera illisible.<br>Le sketch donné en exemple envoie le message *Setup OK* sur le port série, initialisé avec un débit de 9600 bauds (ligne 5). Ce message s'affiche sur le moniteur série à l'écran du PC. On peut aussi envoyer un message depuis le PC au microcontrôleur de la carte en tapant son contenu dans la zone *Message (Enter to send message to 'Nucleo-64' on 'COM14')* et en validant avec la touche *Entrée*.
 
  <br>
  <div align="left">
  <img alt="Moniteur série IDE Arduino" src="exercices/images/1_installation/arduino_sermon.png" width="800px">
  </div>
  <br>
    
 * **(7)** Afficher / Masquer le carnet de croquis<br>
   Déploie (ou masque) une barre latérale à droite intitulée *CARNET DE CROQUIS*.<br>
 Tous les croquis que vous aurez sauvegardés sur votre ordinateur (ou éventuellement sur votre compte Cloud Arduino) s'y trouveront. Pour qu'un premier croquis y apparaisse, il vous faut procéder à sa sauvegarde à l'aide de la commande *Enregistrer* accessible par *Fichier / Enregistrer (*[CTRL]* + *[S]*)*.<br>
 Vous pouvez préciser le dossier de votre ordinateur dans lequel les croquis sont enregistrés par la zone de saisie *Localisation du croquis* de la boite de dialogue *Préférences* (menu *Fichiers / Préférences*, onglet *Paramètres*).

 * **(8)** Afficher / Masquer le gestionnaire de carte<br>
   Cette icône est une autre façon d'afficher (ou masquer) le volet *GESTIONNAIRE DE CARTE* autrement accessible par le menu *Outils /Carte / Gestionnaire de carte...* ou encore par le raccourci clavier *Ctrl + Maj + B*.

 * **(9)** Afficher / Masquer le gestionnaire de bibliothèque<br>
   Cette icône est une autre façon d'afficher (ou masquer) le volet *GESTIONNAIRE DE BIBLIOTHEQUE* autrement accessible par le menu *Croquis /Importer une bibliothèque / Gérer les bibliothèques...* ou encore par le raccourci clavier *Ctrl + Maj + I*.<br>
   **NB** : Vous pouvez afficher la liste de toutes les bibliothèques que vous avez déjà installées avec le menu déroulant *Type* en choisissant l'entrée *Installed*.

 * **(10)** Afficher / Masquer le débogueur<br>
   Donne accès à la barre latérale contenant les outils de débogage pas à pas / en temps réel.

 * **(11)** Afficher / Masquer les outils de recherche<br>
   Donne accès à la barre latérale contenant les outils pour rechercher et éventuellement remplacer des séquences de texte dans les différents fichiers de votre projet.

  * **(12)** Zone d'édition de code<br>
   Editeur de texte interactif pour le code source en C/C++ de votre sketch. Il offre l'auto-complétion du code en cours de frappe et bien d'autres options très utiles que nous allons voir plus en détails dans l'une des sections qui suivent.

  * **(13)** Zone de messages<br>
   C'est à cet emplacement que vont s'afficher différents onglets fournissant des consoles et messages importants. A l'aide **d'un clic droit de la souris** sur le fond de cette zone, on fait apparaître un menu contextuel qui permet d'en effacer le contenu ou bien de le recopier dans le presse-papier de Windows.<br>
   La console dans l'onglet *Sortie* affiche tous les messages et avertissements du compilateur, de l'éditeur de lien et de l'IDE en général. Les autres onglets que l'on peut trouver dans cette zone sont **la console du moniteur série** et **la console de débogage**. 

  * **(14)** Position du curseur (ligne, colonne) à l'intérieur de la zone d'édition de code.<br>

  * **(15)** Format d'encodage des caractères utilisés par le sketch<br>
   En l'occurrence, il s'agit de UTF8 (abréviation de l'anglais *Universal Character Set Transformation Format - 8 bits*). Quel que soit le système d'encodage, **évitez comme la peste les caractères accentués** dans vos sketchs et tenez vous strictement aux 95 caractères imprimables du [bon vieux code ASCII](https://fr.wikipedia.org/wiki/American_Standard_Code_for_Information_Interchange), sauf au sein des commentaires. En effet, la plupart des afficheurs (écrans LCD, OLED ...) ne sont pas capables de tracer les caractères accentués et de nombreuses bibliothèques ont la même limitation.

  * **(16)** Rappel de la carte actuellement connectée à l'IDE et sélectionnée et du port qu'elle utilise.<br>

  * **(17)** En cliquant ici vous aurez accès à l'historique des notifications de l'IDE.<br>

  * **(18)** Affiche ou masque la zone de messages.<br>


## Outils et raccourcis bien pratiques situés dans les menus

Bien sûr, toutes les entrées des menus sont utiles. Certaines sont redondantes avec les fonctions de l'IDE que nous venons de lister. Nous ne les commenterons pas toutes. Le propos de cette section est de lister *quelques fonctions des menus qui vous feront gagner beaucoup de temps* lorsque vous programmerez. 

### Menu Fichier

 - Entrée *Nouveau croquis [CTRL] + [N]*. Ouvre une nouvelle instance de l'IDE avec un sketch minimaliste, qui ne contient que deux fonctions vides, sans instructions : `setup()` et `loop()`. Cette structure est la base du modèle de programmation Arduino, idéale pour les débutants en programmation embarquée. En contrepartie, elle occulte le modèle de programmation par *interruptions* qui est la principale force des microcontrôleurs. 

 - Entrée *Enregistrer [CTRL] + [S]*. Pour sauvegarder le sketch en cours à l'emplacement précisé dans la boite de dialogue *Préférences*, onglet *Paramètres* (voir ci-après).

 - Entrée *Préférences [CTRL] + [,]*. Affiche la boite de dialogue des options de configuration de l'IDE ; nous en avons déjà parlé. Prenez le temps d'étudier son contenu, elle est essentielle pour adapter l'apparence de l'IDE à votre convenance.

 - Entrée *Exemples*. Ce menu rassemble ce qui a fait le succès de l'aventure Arduino : *des exemples de sketchs, prêts à compiler et à charger dans votre carte, pour la plupart des fonctions que l'on trouve dans les systèmes à microcontrôleurs*. De plus, chaque fois que vous téléchargez et installez *une bibliothèque*, une nouvelle entrée est créée ici, qui contient tout un ensemble de sketchs mettant en œuvre ses fonctions.

### Menu Modifier

 - Entrée *Formatage automatique [CTRL] + [T]*. Cette fonction force l'indentation de votre code, ce qui contribue à améliorer sa lisibilité, mais surtout à piéger des erreurs de syntaxe qui seraient autrement très difficiles à localiser (omission d'un `';'`, d'un `'{'` ou d'un `'}'`) au sein d'un listing / sketch un peu long.

 - Entrée *Commenter / Décommenter [CTRL] + [:]*. Commencez par sélectionner des lignes de code dans la zone d'édition puis appliquez leur cette fonction. Elle ajoutera (respectivement, retirera) des balises "double slash" (`//`) au début des lignes ce qui les transformera en commentaires (respectivement, ce qui les transformera en lignes de code).

### Menu Croquis

 - Entrée *Ouvrir le dossier de croquis [ALT] + [CTRL] + [K]*, pour ouvrir une fenêtre de l'explorateur de fichiers Windows directement dans le dossier où sont sauvegardés vos croquis.

 - Entrée *Importer une bibliothèque*. Nous l'avons déjà utilisée pour afficher le gestionnaire de bibliothèques. Elle offre également une option pour importer des bibliothèques contenues dans des archives compressées ZIP.

### Menu Outils

 - Nous ne reviendrons pas sur les entrées *Moniteur série*, *Traceur série*, *Carte*, *Port*, *Board part number* et *Upload method* essentielles mais déjà expliquées.

 - Entrée *C Runtime Library*. Prenez soin de sélectionner l'option *Newlib Nano + Float Printf/Scanf*. Nous n'expliquerons pas ici la motivation de ce choix car il est nécessaire de connaître la programmation en C ou C++ et l'usage des fonctions `printf()` et `scanf()` pour l'apprécier. Il apporte un réel confort d'écriture du code pour les sketchs qui convertissent des nombres à virgule (format "float") en leur représentation sous forme de chaîne de caractères.


# Utiliser l'IDE Arduino

Pour finir ce tutoriel, et valider complètement l'installation, nous allons charger dans l'IDE l'exemple *Blink*, puis le compiler et programmer le microcontrôleur STM32 de notre carte avec le fichier binaire (le firmware) produit.

## Etape 1 : Retours sur les configurations de l'IDE Arduino

Dans l'IDE Arduino vous conseillons de configurer les options du menu *Fichiers / Préférences*, volet *Paramètres* comme ci-dessous :

<br>
<div align="left">
<img alt="IDE Arduino, préférences compilation" src="exercices/images/1_installation/arduino_IDE_preferences_compiler.png" width="800px">
</div>
<br>

Notamment, nous demandons l'affichage de **tous** (All) les avertissements du compilateur. Ces options permettent d'obtenir dans la console de l’IDE des informations sur les problèmes de compilation et de link (édition de liens) de la chaine de compilation « gcc pour ARM ».

Les messages en question vous signaleront des erreurs de code basiques, des maladresses de syntaxe, etc. Au départ ils vous paraitront obscurs, mais ils vous seront très rapidement indispensables. Par exemple, pour un sketch (non détaillé ici) dans lequel il manque un `';'` (une erreur très classique en C ou C++ !) vous obtiendrez le message suivant :

```
C:\Users\yanni\AppData\Local\Temp\.arduinoIDE-unsaved20221127-18136-c12ngl.gfif\Blink\Blink.ino: In function 'void loop()':
C:\Users\yanni\AppData\Local\Temp\.arduinoIDE-unsaved20221127-18136-c12ngl.gfif\Blink\Blink.ino:
34:14: error: expected ';' before 'digitalWrite'
   34 |   delay(1000)                       // wait for a second
      |              ^
      |              ;
   35 |   digitalWrite(LED_BUILTIN, LOW);   // turn the LED off by making the voltage LOW
      |   ~~~~~~~~~~~~

Utilisation de la bibliothèque SrcWrapper version 1.0.1 dans le dossier:
C:\Users\yanni\AppData\Local\Arduino15\packages\STMicroelectronics\hardware\stm32\2.4.0\libraries\SrcWrapper 
exit status 1

Compilation error: expected ';' before 'digitalWrite'
```

## Etape 2 : Ouverture et lecture du sketch *Blink*

Chargeons à présent le sketch *Blink* dans l'IDE. Allez dans le menu *Fichiers/Exemples/01.Basics/Blink*. Une nouvelle fenêtre de l'IDE s'ouvre et affiche le code correspondant :

<br>
<div align="left">
<img alt="IDE Arduino, code source de Blink" src="exercices/images/1_installation/arduino_IDE_Blink.png" width="800px">
</div>
<br>

Le sketch commence par un long commentaire sur plusieurs lignes (1 à 23), délimité par les balises ```/*``` (début du commentaire) et ```*/``` (fin du commentaire).

A la ligne 25, on trouve un commentaire sur une seule ligne, il commence par la balise ```//``` :

```cpp
// the setup function runs once when you press reset or power the board
```

A la ligne 26, on entre dans la fonction `setup()` qui procède aux diverses **initialisations** des périphériques qui seront utilisés. Comme toute fonction en langage C ou C++, il est précisé quel type de valeur elle renvoie, en l'occurrence rien du tout, d'où le mot clef `void`, et les instructions qu'elle contient sont contenues entre une accolade gauche `'{'` (ligne 26) et une accolade droite `'}'` (ligne 29).

A la ligne 28 figure la seule instruction de la fonction `setup()` : 

```cpp
  pinMode(LED_BUILTIN, OUTPUT);
```
Comme précise le commentaire de la ligne 27, la fonction `pinMode()` initialise la broche digitale `LED_BUILTIN` en mode sortie `OUTPUT`.

**Important** : Le séparateur d'instructions en langage C ou C++ est le **point virgule** `';'`. A de rares exceptions près, il faut systématiquement placer ce symbole **après chaque instruction**. Lorsqu'on l'oublie (et c'est fréquent !) le compilateur peut signaler toutes sortes d'erreurs farfelues et la compilation échouera. 

Tous les sketchs Arduino commencent par une initialisation  `setup()` ...
 - Pour indiquer quelles broches du microcontrôleur seront utilisées ;
 - Pour indiquer comment ces broches seront utilisées (entrées ou sorties par ex.) ;
 - Pour mettre les capteurs/actuateurs dans un état prêt à fonctionner ;
 - Pour pré-calculer et mémoriser des expressions mathématiques qui seront utilisées très souvent par le firmware ;
 - Etc.

L'approche de programmation Arduino est en fait basée sur le *langage C++* avec (essentiellement) des bibliothèques gérant le matériel : boutons, capteurs, LED … (références disponibles [ici](https://www.arduino.cc/reference/en/)).

A la ligne 32 commence la **fonction principale** `loop()` qui correspond à une **boucle infinie**.
Elle va répéter les instructions qu'elle contient, les unes à la suite les autres, de la première à la dernière, puis recommencer à la première instruction, etc. aussi longtemps que la carte sera alimentée en courant / tension (en l'occurrence via le port *USB ST-LINK*). 
Ce mode de programmation intuitif construit avec une boucle s'appelle *polling* (en français *scrutation*). 

Tous les sketchs Arduino comportent une boucle infinie ...
 - Pour lire les broches (c'est à dire interroger les capteurs connectés dessus).
 - Pour écrire sur les broches (c'est à dire envoyer des commandes à des actuateurs, à des diodes …).
 - Pour faire des calculs ou manipulations sur des données, en fonction des entrées et sorties.
 - Etc.

Dans notre cas la boucle contient 4 appels de fonctions (lignes 33 à 36) :

```cpp
  digitalWrite(LED_BUILTIN, HIGH);  // Allume la LED (« HIGH » pour niveau tension haut)
  delay(1000);                      // On attend 1000 ms
  digitalWrite(LED_BUILTIN, LOW);   // Éteint la LED (« LOW » pour niveau tension bas)
  delay(1000);                      // On attend 1000 ms
```

* La fonction `digitalWrite(LED_BUILTIN, ...)` change l'état de la broche digitale `LED_BUILTIN` qui est en fait connectée à la LED bleue (LED1, reprendre [le dessin de la carte NUCLEO-WB55RG](https://gitlab.com/stm32python/fr/-/blob/master/_docs/Stm32duino/installation.md#etape-3-connexion-de-la-carte-%C3%A0-lide-via-le-st-link) plus haut). 

* La fonction `delay(1000)` demande au microcontrôleur de marquer une pause,exprimée en millisecondes (1000 ici, soit une seconde). 

## Etape 3 : Compilation du sketch et chargement du firmware dans le MCU de la carte

Cliquez à présent sur le bouton *Téléverser* et attendez que les messages du compilateur arrêtent de défiler dans la console de sortie. Cela peut prendre quelques minutes !
Lorsque c'est terminé, le message *Upload complete on NOD_WB55RG* apparaît dans la console, confirmant que le chargement du firmware s'est déroulé correctement.
*Et voilà, la LED1 clignote une fois toutes les deux secondes !* (ce qui est précisément l'objet du sketch *Blink*).

# Fonctionnalités avancées de l'IDE Arduino

Pour finir, parcourons superficiellement quelques une des fonctionnalités avancées offertes par l'IDE Arduino version 2.

## Moniteur série et traceur série

On commence par modifier le sketch pour y ajouter l'initialisation du port série associé au ST-Link (`Serial`) et les fonctions qui lui permettent d'écrire dans le moniteur série (`Serial.println(...)`). On enregistre le nouveau sketch  dans le carnet de croquis sous le nom "Blink_Mod" : 

<br>
<div align="left">
<img alt="Sketch Blink modifié" src="exercices/images/1_installation/blink_mod.png" width="800px">
</div>
<br>

Si vous ouvrez le traceur série vous observerez sous forme de série temporelle les informations renvoyées par les instructions `Serial.println(...)` dans le moniteur série :

<br>
<div align="left">
<img alt="Moniteur série et traceur série" src="exercices/images/1_installation/ser_trac.png" width="950px">
</div>
<br>

## Assistance à la rédaction du code

La version 2 de l'IDE Arduino offre des fonctions d'assistance à la rédaction du code :

- L'IDE analyse ce que vous écrivez et vous propose des suggestions de code en cours de frappe (fonction *Intellisense*) :

<br>
<div align="left">
<img alt="Intellisense" src="exercices/images/1_installation/intellisense.png" width="800px">
</div>
<br>

 - Si vous commencez à taper le nom d'une *classe* dans le sketch, l'IDE affichera, au moment où vous taperez le `'.'`, la liste des méthodes qui lui sont associées afin que vous puissiez choisir celle dont vous avez besoin  (auto-complétion) :

<br>
<div align="left">
<img alt="Méthodes d'une classe" src="exercices/images/1_installation/auto_completion.png" width="800px">
</div>
<br>

- Si vous passez le curseur de la souris sur une des *méthodes* du sketch (par exemple le `.begin(9600)` ) et le laissez assez longtemps au-dessus, vous constaterez que l'IDE affiche une fenêtre précisant les paramètres et convention d'appel de celle-ci :

<br>
<div align="left">
<img alt="Interface d'une méthode" src="exercices/images/1_installation/interface_fonction.png" width="800px">
</div>
<br>

## Trouver le code source d'une fonction

Le menu contextuel flottant, qui s'affiche lorsque l'on clique sur le fond de l'éditeur de code avec le bouton droit de la souris, propose quelques fonctions essentielles : *Annuler Ctrl + Z* et *Rétablir Ctrl + Maj + Z* pour revenir sur des erreurs de frappe mais surtout *Atteindre la définition F12*.

***Atteindre la définition* permet de retrouver en un instant le listing / code source d'une fonction que vous utilisez dans votre sketch**.

Par exemple, placez la souris sur l'instruction `pinMode(...)` et appuyez sur la touche *F12*. Une nouvelle page de code va s'ouvrir, dans un onglet intitulé *wiring_digital.c* qui correspond au fichier contenant le code source de la fonction `pinMode()` :

<br>
<div align="left">
<img alt="Trouver les sources d'une fonction" src="exercices/images/1_installation/atteindre_definition.png" width="800px">
</div>
<br>

## Débogage pas à pas 

L'IDE Arduino offre désormais un débogueur complet et fonctionnel pour les cartes et microcontrôleurs de STMicroelectronics. Un tutoriel dédié à cet outil pour son utilisation avec STM32duino est accessible par [ce lien](../Stm32duino/debogueur).

# Mission accomplie !

> C'est terminé ! Vous disposez à présent d'un environnement Arduino fonctionnel et de toutes les connaissances requises pour développer vos propres projets. Bien évidemment, il y aurait bien plus à raconter sur ce sujet. Notre site se concentre sur l'utilisation de STM32duino, un parmi tous les sujets possibles. La littérature concernant Arduino est abondante sur Internet et nous vous conseillons vivement de compléter votre expertise grâce à l'excellent [**Blog d'Eskimon**](https://eskimon.fr/) !
