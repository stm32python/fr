---
title: STM32duino
description: Section STM32duino
---

# STM32duino : Arduino pour STM32

L'initiative STM32duino illustre comment mettre en œuvre Arduino sur la famille de microcontrôleurs STM32 de STMicroelectronics.

<br>
<div align="left">
<img align="center" src="images/equation_stm32duino.jpg" alt="stm32duino" width="400"/>
</div>
<br>

STM32duino est un projet rassemblant des [**bibliothèques Arduino**](https://www.arduino.cc/reference/en/) pour les cartes de développement à base de [**microcontrôleurs STM32**](https://www.st.com/en/microcontrollers-microprocessors/stm32-32-bit-arm-cortex-mcus.html) ([Nucleo](https://www.st.com/en/evaluation-tools/stm32-nucleo-boards.html) et [Discovery](https://www.st.com/en/evaluation-tools/stm32-discovery-kits.html)) et pour les [**composants MEMS de STMicroelectronics**](https://www.st.com/en/microcontrollers-microprocessors/stm32-32-bit-arm-cortex-mcus.html). Il permet de développer et compiler des programmes (autrement appelés des _sketchs_ dans la vocable Arduino) depuis l'environnement de développement [**Arduino IDE**](https://docs.arduino.cc/software/ide-v2).
Le projet STM32duino n'est pas seulement un portage des bibliothèques Arduino pour STM32, il les étend aussi afin de mieux tirer profit des spécificités de cette architecture.

La chaîne de compilation utilisée par STM32duino est [**gcc pour ARM**](https://gcc.gnu.org/) (gratuite et open-source) et le langage de programmation est C/C++, qui dominent dans le monde professionnel pour le développement de [**systèmes embarqués**](https://fr.wikipedia.org/wiki/Informatique_embarqu%C3%A9e). Par conséquent, même si la "philosophie Arduino", du fait de sa prérogative pédagogique, masque une bonne partie de la complexité de l'architecture STM32 et de ses atouts, le projet STM32duino reste une excellente porte d'entrée pour l'apprentissage de la programmation embarquée dans les filières d'enseignement professionnel et/ou technique et/ou technologique sur des puces que l'on retrouve dans les applications électroniques de l'industrie, de l'automobile, de la téléphonie, des objets connectés ...

<br>

## Le modèle STM32duino

La figure ci-dessous précise l'architecture du projet STM32duino et apporte des précisions importantes pour quiconque envisage tirer le maximum de celle-ci, voire étendre plus tard ses connaissances et compétences en programmation embarquée au-delà du "paradigme Arduino". Examinons chacun de ses éléments.

<br>
<div align="left">
<img align="center" src="images/stm32duino_model.jpg" alt="modèle stm32duino" width="500"/>
</div>
<br>

Les fondations de STM32duino reposent sur les outils et ressources utilisés pour le développement professionnel sur les puces de STMicroelectronics : 

- [**CMSIS**](https://developer.arm.com/tools-and-software/embedded/cmsis) est l'acronyme de *Common Microcontroller Software Interface Standard*, une bibliothèque (logicielle) d'abstraction matérielle conçue par la société ARM, en langage C. L'ambition de CMSIS est que les programmes écrits pour les différents microcontrôleurs (MCU) utilisant des processeurs ARM Cortex soient "portables", c'est à dire qu'ils puissent être recompilés et fonctionner sans modifications sur ces différents MCU. STMicroelectronics utilise CMSIS mais en a aussi divergé en créant sa propre bibliothèque d'abstraction matérielle (HAL, voir plus loin) qui n'est portable qu'entre MCU STM32.

- **LL** (*Low Layer*) et **HAL** (*Hardware Abstraction Layer*) désignent des bibliothèques complémentaires développées par STMicroelectronics en langage C. La bibliothèque HAL offre des API de haut niveau et orientées fonctionnalités, avec un niveau de portabilité élevé. La bibliothèque LL offre des API de bas niveau au niveau des registres, avec une meilleure optimisation mais moins de portabilité et nécessite une connaissance approfondie des spécifications du MCU et de ses périphériques. Les deux peuvent être utilisées ensembles si elles n’adressent pas les mêmes périphériques. **Les pilotes de périphériques (ou drivers en anglais)** sont écrits avec les fonctions de LL ou HAL selon leur complexité.

- [**STM32Cube**](https://www.st.com/content/st_com/en/ecosystems/stm32cube-ecosystem.html) est un ensemble d'outils logiciels et de bibliothèques de logiciels embarqués sur PC pour le développement de projets basés sur des MCU STM32 : Environnement de développement graphique, outils de configuration, outils de programmation, outils de benchmarks, drivers, bibliothèques pour l'IA embarquée, le contrôle de moteurs, etc.

Ces outils permettent de développer la distribution Arduino pour STM32 proprement dite, qui est constituée des éléments suivants :

 - [**MCU / board porting layer**](https://github.com/stm32duino/Arduino_Core_STM32) est l'ensemble des ressources qui permettent d'utiliser les MCU et les cartes de prototypage de STMicroelectronics dans l'environnement Arduino.
 
 - [**Basic Core APIs**](https://www.arduino.cc/reference/en/) n'est ni plus ni moins que l'ensemble des fonctions de la bibliothèque Arduino "standard" qui permet d'apprendre facilement à programmer un microcontrôleur en faisant l'impasse sur une bonne partie de la complexité de son architecture. Cette "couche" d'abstraction permet donc de programmer les MCU STM32 avec les conventions d'Arduino exactement comme s'il s'agissait de [MCU 8 bits AVR ATmega328P](http://ww1.microchip.com/downloads/en/DeviceDoc/Atmel-7810-Automotive-Microcontrollers-ATmega328P_Datasheet.pdf) tels qu'on les trouve sur les très populaires [cartes Arduino UNO](https://store.arduino.cc/products/arduino-uno-rev3).

 - [**Extended APIs**](https://github.com/stm32duino/Arduino_Core_STM32/wiki/API) est une extension de la bibliothèque Arduino "standard" qui permet d'exploiter mieux l'architecture STM32 tout en restant dans l'environnement Arduino. En effet, les [MCU 32 bits STM32](https://www.st.com/en/microcontrollers-microprocessors/stm32-32-bit-arm-cortex-mcus.html) de STMicroelectronics on l'avantage d'être beaucoup plus puissants (plus de fonctions, plus de mémoire, 32 bits et pas seulement 8 bits, etc.) que les ATmega328P et l'API Arduino de base bride certaines de leurs fonctions. 

 - **Sketches et Libraries** désigne simplement les bibliothèques et exemples de classeurs (ou sketchs) Arduino téléchargeables à travers l'IDE éponyme. Pour ces aspects, STM32duino est également 100% compatible avec Arduino.

 Enfin, le dernier étage de notre schéma rappelle les ressources nécessaires pour l'installation des bibliothèques STM32duino dans l'environnement Arduino. Ce sujet est spécifiquement traité dans [ce tutoriel](installation).
 
 <br>
 
  > **En conclusion, le modèle STM32duino se prête très bien à une continuité pédagogique**  en permettant d'écrire des sketchs  "à la mode Arduino" sans se soucier des détails de l'architecture du microcontrôleur programmé (pertinent en bac pro par exemple pour aborder la programmation embarquée), mais en offrant aussi la possibilité d'utiliser des bibliothèques professionnelles beaucoup plus abouties (LL ou HAL, utile en BTS par exemple).

<br>

## Pour démarrer

Avant toute chose, assurez vous d'avoir une installation fonctionnelle, le protocole à suivre pour ce faire est détaillé dans la section [Installation et prise en main de l'IDE sous Windows](installation).

Dans la section [Tutoriels](exercices), vous trouverez des exemples de mise en œuvre de périphériques internes aux microcontrôleurs (MCU) STM32, mais aussi de capteurs et actuateurs externes avec STM32duino.

<br>

## Sommaire

* [Installation et prise en main de l'IDE sous Windows](installation)
* [Tutoriels](exercices)
* [Téléchargements](Telechargement)
* [Liens utiles](ressources)
* [Foire aux Questions](../Kit/faq)