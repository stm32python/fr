---
title: Coder en C++ avec Arduino 
description: Notions essentielles de C++ pour écrire des sketchs Arduino
---

# Coder en C/C++ avec Arduino

Commençons par rappeler que [Arduino](https://www.arduino.cc/) est une société italienne qui a développé à la fois [des cartes programmables (et leurs accessoires)](https://www.arduino.cc/en/hardware) et [une plateforme logicielle](https://www.arduino.cc/en/software) (on préfèrera par la suite utiliser le terme anglo-saxon *framework*) pour faciliter la création de [microcodes (ou *firmwares*)](https://fr.wikipedia.org/wiki/Firmware) pour les microcontrôleurs qui les animent.

Outre la société Arduino, pléthore de fabricants distribuent désormais des [pilotes logiciels](https://fr.wikipedia.org/wiki/Pilote_informatique) afin que leur matériel puisse être programmé avec le framework Arduino. Par exemple, [STM32duino](https://github.com/stm32duino), qui fait l'objet de nos tutoriels, est une extension du framework Arduino pour une bonne partie des [cartes à microcontrôleurs de STMicroelectronics](../Kit/nucleo).

Pour simplifier la programmation des cartes à microcontrôleurs, le framework Arduino offre [**un ensemble de bibliothèques**](https://docs.arduino.cc/programming/). Les débutants les qualifient souvent et **incorrectement** de "langage Arduino". Chaque fois que vous écrivez le code source d'un **programme Arduino** (que l'on appelle usuellement *croquis* en français, ou encore *sketch* en anglais) vous utilisez en fait [**le langage C**](https://fr.wikipedia.org/wiki/C_(langage)) et son extension, [**le langage C++**](https://fr.wikipedia.org/wiki/C%2B%2B). L'essentiel de votre sketch va donc appeler des **"fonctions"** [^1] contenues dans les bibliothèques du framework, elles-mêmes écrites en C et C++.

Bien que l'objectif de ce site ne soit pas l'enseignement de la programmation, nous avons estimé utile de rédiger cette section pour rappeler des éléments essentiels de langage C/C++ et du framework Arduino utilisés par la plupart de nos [tutoriels](../Stm32duino/exercices/index).

## Exemple de sketch Arduino

Un sketch Arduino est une suite d'instructions en langage C/C++, dans un ordre précis, qui réalise une tâche spécifique. Les lignes d'instructions opèrent sur des **variables** et sont séparées les unes des autres par le caractère ```;```. On trouve aussi dans le programme des **constantes**, **des commentaires**, des **directives de préprocesseur** ... Nous allons revoir plus en détail toutes ces notions à l'aide des tutoriels qui suivent.

## Eléments de langage C et C++ pour l'embarqué

### 1 - [Les variables](variables)

### 2 - [Les constantes](constantes)

### 3 - [Les tableaux](tableaux)

### 4 - [Eléments de syntaxe](elements)

### 5 - [Les opérateurs](operateurs)

### 6 - [Les structures conditionnelles](structs_cond)

### 7 - [Les structures itératives](structs_iter)

### 8 - [Les fonctions](fonctions)

### 9 - [Masques binaires et registres](masques)


## Liens et ressources

* Le site [KOOR.fr sur le langage C](https://koor.fr/C/Index.wp)
* Le site [Developpez.com, à propos du langage C](https://c.developpez.com/)
* Le site [Developpez.com, à propos du langage C++](https://c.developpez.com/)
* [Le cours sur le langage C du site Open Classrooms](https://openclassrooms.com/fr/courses/19980-apprenez-a-programmer-en-c)
* [Le cours sur le langage C++ du site Open Classrooms](https://openclassrooms.com/fr/courses/1894236-apprenez-a-programmer-en-c)
* [Le cours sur le langage C du site Zeste de Savoir](https://zestedesavoir.com/tutoriels/755/le-langage-c-1/)
* Cours "PROTOTYPAGE AVEC ARDUINO", créé par **un enseignant du lycée Ampère**, à Marseille.


## Notes au fil du texte

[^1]: Une **fonction** informatique est un ensemble d’instructions regroupées sous un nom et s’exécutant à la demande (l’appel de la fonction, voir [cette section](fonctions)).
