---
title: RIOT
description: Comment reconstruire MicroPython avec RIOT OS
---

# Reconstruction du firmware MicroPython avec RIOT OS

**Outils nécessaires à l'installation de MicroPython :**
Il est nécessaire d'utiliser un __ordinateur Windows avec une machine virtuelle Linux__ installée ou un _ordinateur Linux directement_.

## Installation de la chaine de développement pour RIOT OS pour STM32 sur un ordinateur Linux

Depuis le bureau Linux, ouvrez un terminal en faisant
Clic Droit puis « Ouvrir un terminal ici »
Entrez ensuite les commandes suivantes une par une
afin d’installer les logiciels pré-requis.

```bash
sudo apt-get install git
sudo apt-get install make
sudo apt-get install gcc
sudo apt-get install gcc-arm-none-eabi
sudo apt-get install cmake
sudo apt-get install lisusb-1.0
```
C'est nécessaire aussi d'installer l'outil __ST-LINK-UTILITY__
```bash
git clone https://GitHub.com/texane/stlink
cd stlink
make release
cd build/Release
sudo make install
cd
sudo ldconfig
```

Pour vérifier si l'outil est bien installé, vous pouvez utiliser le commande __*`st-info`*__

Une fois les logiciels pré-requis installées, il est nécessaire de récupérer le projet RIOT depuis l’outil git en écrivant dans un terminal (ouvert depuis le bureau comme précédemment) les commandes suivantes:

```bash
git clone https://GitHub.com/RIOT-OS/RIOT
```

## Construction en chargement de l'interpréteur MicroPython sur la carte Nucleo F446RE

Construisez le firmware contenant l'interpréteur MicroPython pour votre carte (ici `nucleo-f446re`).
```bash
cd RIOT
cd examples/MicroPython
make BOARD=nucleo-f446re
```

Chargez le firmware sur votre carte (ici `nucleo-f446re`).
```bash
make BOARD=nucleo-f446re flash-only
```
> Remarque: `make` execute OpenOCD pour le chargement.

Vous pouvez vous connecter à la console Python avec minicom, pyterm, PuTTY, Teraterm ...
```
>>> main(): This is RIOT! (Version: 2021.07-devel-171-g77864)                   
-- Executing boot.py                                                            
boot.py: MicroPython says hello!                                                
-- boot.py exited. Starting REPL..                                              
MicroPython v1.4.2-6568-gbb8e51f6d on 2021-05-05; riot-p-nucleo-wb55 with stm32 
Type "help()" for more information.                                             
```

## Construction en chargement de l'interpréteur MicroPython sur la carte P-NUCLEO-WB55

Le portage de RIOT sur la carte P-NUCLEO-WB55 est partiel : la liste des fonctionnalités disponibles est dans le document [doc.txt](https://GitHub.com/RIOT-OS/RIOT/blob/master/boards/p-nucleo-wb55/doc.txt) de la carte.

Construisez le firmware contenant l'interpréteur MicroPython pour votre carte (ici `p-nucleo-wb55`).

```bash
cd RIOT
cd examples/MicroPython
make BOARD=p-nucleo-wb55
```

Chargez le firmware sur votre carte (ici `p-nucleo-wb55`).
```bash
make BOARD=p-nucleo-wb55 flash-only
```
> Remarque: `make` execute OpenOCD pour le chargement.

Vous pouvez vous connecter au port console de la carte.

Il se peut que le chargement du firmware avec OpenOCD échoue : Chargez alors le firmware en utilisant l'application [STM32CubeProgrammer](https://www.st.com/en/development-tools/stm32cubeprog.html) préalablement installée.

Le binaire à charger est situé dans `examples/Micropython/bin/p-nucleo-wb55/MicroPython.bin`.

Une autre alternative pour charger le firmware est de copier le binaire sur le volume `NOD_WB55RG` de la carte soit via l'explorateur de fichiers, soit via la commande `copy` ou `cp` avec la ligne de commande suivante:
```bash
cp bin/p-nucleo-wb55/MicroPython.bin /Volumes/NOD_WB55RG/
```

Vous pouvez vous connecter à la console Python avec minicom, pyterm, PuTTY, Teraterm ...
```
>>> main(): This is RIOT! (Version: 2021.07-devel-171-g77864)                   
-- Executing boot.py                                                            
boot.py: MicroPython says hello!                                                
-- boot.py exited. Starting REPL..                                              
MicroPython v1.4.2-6568-gbb8e51f6d on 2021-05-05; riot-p-nucleo-wb55 with stm32 
Type "help()" for more information.                                             
```


## Construction en chargement de l'interpréteur MicroPython sur la carte NUCLEO-WL55JC

Le portage de RIOT sur la carte NUCLEO-WL55JC est partiel : la liste des fonctionnalités disponibles est dans le document [doc.txt](https://GitHub.com/RIOT-OS/RIOT/blob/master/boards/nucleo-wl55jc/doc.txt) de la carte.

Construisez le firmware contenant l'interpréteur MicroPython pour votre carte (ici `nucleo-wl55jc`).

```bash
cd RIOT
cd examples/MicroPython
make BOARD=nucleo-wl55jc
```

Chargez le firmware sur votre carte (ici `nucleo-wl55jc`).
```bash
make BOARD=nucleo-wl55jc flash-only
```
> Remarque: `make` execute OpenOCD pour le chargement.

Vous pouvez vous connecter au port console de la carte.

Il se peut que le chargement du firmware avec OpenOCD échoue : Chargez alors le firmware en utilisant l'application [STM32CubeProgrammer](https://www.st.com/en/development-tools/stm32cubeprog.html) préalablement installée.

Le binaire à charger est situé dans `examples/Micropython/bin/nucleo-wl55jc/MicroPython.bin`.

Une autre alternative pour charger le firmware est de copier le binaire sur le volume `NOD_WL55JC` de la carte soit via l'explorateur de fichiers, soit via la commande `copy` ou `cp` avec la ligne de commande suivante:
```bash
cp bin/nucleo-wl55jc/MicroPython.bin /Volumes/NOD_WL55JC/
```

Vous pouvez vous connecter à la console Python avec minicom, pyterm, PuTTY, Teraterm ...
```
>>> main(): This is RIOT! (Version: 2021.07-devel-171-g77864)                   
-- Executing boot.py                                                            
boot.py: MicroPython says hello!                                                
-- boot.py exited. Starting REPL..                                              
MicroPython v1.4.2-6568-gbb8e51f6d on 2021-05-05; riot-nucleo-wl55jc with stm32 
Type "help()" for more information.                                             
```

## Personnalisation des instructions exécutées au démarrage

Le fichier `boot.py` contient des instructions Python qui sont exécutées au démarrage de la carte avant le passage en mode interactif (REPL).

Il peut être compléter à votre guise.

Il faut alors reconstruire et charger le nouveau firmware sur la carte.
