---
title: RIOT
description: Comment installer MicroPython avec RIOT OS
---

# Installation de MicroPython avec RIOT OS

## Chargement de l'interpréteur MicroPython sur la carte NUCLEO F446RE

La méthode la plus simple pour charger le firmware [MicroPython.bin](https://stm32python.gitlab.io/assets/RIOT/nucleo-f446re/MicroPython.bin) sur la carte est de copier le binaire sur le volume `NODE_F446RE` de la carte via l'explorateur de fichiers.

Vous pouvez aussi utiliser la commande `copy` ou `cp` avec la ligne de commande suivante:
```bash
cp ~/Downloads/MicroPython.bin /Volumes/NODE_F446RE/
```

## Chargement de l'interpréteur MicroPython sur la carte P-NUCLEO-WB55

Le portage de RIOT sur la carte P-NUCLEO-WB55 est partiel : la liste des fonctionnalités disponibles est dans le document [doc.txt](https://GitHub.com/RIOT-OS/RIOT/blob/master/boards/p-nucleo-wb55/doc.txt) de la carte.

La méthode la plus simple pour charger le firmware [MicroPython.bin](https://stm32python.gitlab.io/assets/RIOT/p-nucleo-wb55/MicroPython.bin) sur la carte est de copier le binaire sur le volume `NOD_WB55RG` de la carte via l'explorateur de fichiers.

Vous pouvez aussi utiliser la commande `copy` ou `cp` avec la ligne de commande suivante:
```bash
cp ~/Downloads/MicroPython.bin /Volumes/NOD_WB55RG/
```

## Chargement de l'interpréteur MicroPython sur la carte NUCLEO-WL55JC

Le portage de RIOT sur la carte P-NUCLEO-WB55 est partiel : la liste des fonctionnalités disponibles est dans le document [doc.txt](https://GitHub.com/RIOT-OS/RIOT/blob/master/boards/nucleo-wl55jc/doc.txt) de la carte.

La méthode la plus simple pour charger le firmware [MicroPython.bin](https://stm32python.gitlab.io/assets/RIOT/nucleo-wl55jc/MicroPython.bin) sur la carte est de copier le binaire sur le volume `NOD_WL55JC` de la carte via l'explorateur de fichiers.

Vous pouvez aussi utiliser la commande `copy` ou `cp` avec la ligne de commande suivante:
```bash
cp ~/Downloads/MicroPython.bin /Volumes/NOD_WL55JC/
```

## Connection à la console de la carte

Connectez vous à la console USBSerial de la carte avec PuTTY sur Windows (pyterm ou minicom sur Linux/MacOSX, Teraterm qui est multiplateforme) : La vitesse du port série doit être configurée à 115200 bauds.

Entrez les instructions Python suivantes

```python
help()

print("Hello World " * 6)

import utime
utime.time()

import riot
print(riot.thread_getpid())
```

Le résultat est:
```python
>>> main(): This is RIOT! (Version: 2021.04-devel-330-g7212a)
-- Executing boot.py
boot.py: MicroPython says hello!
-- boot.py exited. Starting REPL..
MicroPython v1.4.2-6568-gbb8e51f6d on 2021-01-27; riot-nucleo-f446re
Type "help()" for more information.
>>> help()
Welcome to the Micro Python RIOT port!

Quick overview of commands for the board:
(none so far)

Control commands:
  CTRL-A        -- on a blank line, enter raw REPL mode
  CTRL-B        -- on a blank line, enter normal REPL mode
  *[CTRL]-[C]*        -- interrupt a running program
  *[CTRL]-[D]*        -- on a blank line, do a soft reset of the board
 (all probably not working in this initial port)
For further help on a specific object, type help(obj)
>>> print("Hello World " * 6)
Hello World Hello World Hello World Hello World Hello World Hello World
>>> import utime
>>> utime.time()
436108
>>> import riot
>>> print(riot.thread_getpid())
1
```
> Remarque: un fichier `boot.py` présent dans le firmware contient des instructions Python qui sont exécutées au démarrage de la carte avant le passage en mode interactif (REPL). Il peut être complété à votre guise en reconstruisant le [firmware MicroPython avec la chaine de développement RIOT](./build).