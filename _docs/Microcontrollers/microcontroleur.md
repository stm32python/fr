---
title: Les microcontrôleurs et l'architecture STM32
description: Les bases pour comprendre ce qu'est un microcontrôleur et en particulier les microcontrôleurs de la famille STM32.
---
# Les microcontrôleurs et l'architecture STM32

Un [**microcontrôleur**](https://fr.wikipedia.org/wiki/Microcontr%C3%B4leur) est avant tout [**une puce électronique**](numérique) c’est-à-dire un assemblage de composants électroniques ultra-miniaturisés, de minuscules [*transistors à effet de champ (MOS)*](numérique) interconnectés selon des schémas très élaborés. Mais c’est aussi **un système sur puce**, en quelque sorte tout un ordinateur construit sur un petit quadrilatère de silicium.

## Anatomie d’un microcontrôleur, l'essentiel

La figure ci-dessous donne le schéma de principe d'un microcontrôleur :

<br>
<div align="left">
<img alt="Schéma de principe d'un microcontrôleur" src="images/Schema_microcontroleur.jpg" width="400px">
</div>
<br>

Il contient donc :

  - Un [*microprocesseur*](https://fr.wikipedia.org/wiki/Microprocesseur), en l'occurrence un modèle *Cortex* conçu par la société *ARM* intégrant une [*unité de calcul généraliste*](https://fr.wikipedia.org/wiki/Unit%C3%A9_arithm%C3%A9tique_et_logique), des [*registres*](https://fr.wikipedia.org/wiki/Registre_de_processeur), des *circuits de débogage*, la gestion des [*interruptions*](https://fr.wikipedia.org/wiki/Interruption_(informatique)), etc. Bien sûr, d'autres microprocesseurs sont possibles, notamment ceux basés sur l'architecture ouverte [*Risk V*](https://fr.wikipedia.org/wiki/RISC-V), ou encore ceux des [*STM8*](https://www.st.com/en/microcontrollers-microprocessors/stm8-8-bit-mcus.html) de STMicroelectronics. 
   - Tout un ensemble de composants conçus et ajoutés par le fabricant du microcontrôleur que nous allons examiner par la suite.

### **Le microprocesseur**

Le [*microprocesseur*](https://fr.wikipedia.org/wiki/Microprocesseur) est un calculateur très rapide qui opère sur des nombres écrits en notation binaire. Entre autres opérations qu’il sait réaliser il peut ajouter, soustraire, multiplier ces nombres (autrement appelés *données*) mais aussi les déplacer, les copier dans différentes mémoires, internes (ses *registres*) comme externes (*RAM*, *Flash*, *EEPROM*), etc. Il est également [*programmable*](programmer) ce qui signifie que l’on peut lui donner une séquence d’ordres – on préfère utiliser le terme [*instructions*](https://fr.wikipedia.org/wiki/Jeu_d%27instructions) - constituant un *programme* qu’il va ensuite lire et exécuter.

Pour en apprendre plus sur les microprocesseurs et pour comprendre leur fonctionnement,  nous vous conseillons la lecture de [cet article](https://info.blaisepascal.fr/nsi-simulateur-de-cpu/) sur le site [**L’informatique, c’est fantastique !**](https://info.blaisepascal.fr/) du [lycée Blaise Pascal de Clermont Ferrand](https://lyc-blaise-pascal-clermont.ent.auvergnerhonealpes.fr/).


### **Les mémoires**

Le microprocesseur a donc besoin de *mémoire* pour enregistrer les instructions des programmes qu’il exécute mais aussi les données qu’il manipule et les résultats de ses opérations. La mémoire lui sert également d’espace de travail, exactement comme la serviette en papier sur laquelle vous griffonnez vos multiplications et vos additions lorsque vous vérifiez la note d’un restaurant. En pratique, des contraintes de coûts et de performances imposent de décliner la mémoire des microcontrôleurs selon *plusieurs architectures spécialisées*. Parmi les principaux types de mémoires intégrées aux microcontrôleurs on distingue :

 -	Celles, très rapides mais exiguës, contenues dans le microprocesseur, appelées [*registres*](https://fr.wikipedia.org/wiki/Registre_de_processeur). Les registres sont utilisés par le microprocesseur pour calculer et manipuler instructions et données pendant l’exécution des programmes, mais aussi pour interagir avec les *périphériques* du microcontrôleur. 
 -	Celles qui sont utilisées pour alimenter les registres pendant l’exécution des programmes, moins rapides que ceux-ci mais nettement plus spacieuses, [*les mémoires vives*](https://fr.wikipedia.org/wiki/M%C3%A9moire_vive) (ou *RAM* pour *Random Access Memories*). 
 -	Celles, plus spacieuses et plus lentes, qui enregistrent les programmes et beaucoup de données, les [*Flash*](https://fr.wikipedia.org/wiki/M%C3%A9moire_flash) et les [*EEPROM*](https://fr.wikipedia.org/wiki/Electrically-erasable_programmable_read-only_memory) essentiellement. Elles tiennent pour le microcontrôleur le même rôle que le disque dur pour un ordinateur.

Les registres et les RAM appartiennent à la famille des [*mémoires volatiles*](https://fr.wikipedia.org/wiki/M%C3%A9moire_volatile) qui ont besoin d’être alimentées en courant et tension sous peine de s’effacer. Par opposition, les Flash ou les EEPROM sont classées dans la famille des [*mémoires non-volatiles*](https://fr.wikipedia.org/wiki/M%C3%A9moire_non_volatile), qui conservent leurs enregistrements lorsque le microcontrôleur est éteint. Les progrès techniques rendent désormais possibles de [nouvelles mémoires](https://fr.wikipedia.org/wiki/M%C3%A9moire_RAM_non_volatile) qui ont les avantages de ces deux familles, mais elles sont encore en cours de développement et bien trop chères pour se démocratiser.

### **Les bus internes (et externes)**

Les mémoires, comme tous les autres périphériques du microcontrôleur, sont directement ou indirectement connectées au microprocesseur par une ou plusieurs lignes de communication appelées [*bus*](https://fr.wikipedia.org/wiki/Bus_de_donn%C3%A9es). Les bus sont constitués de fils conducteurs sur lesquels transitent les données et instructions des programmes, mais aussi de circuits d’interfaces appelés *contrôleurs*. 

Les contrôleurs sont chargés de réguler le trafic des informations sur leur bus mais aussi de les coder et décoder pour assurer leur transmission sans les altérer. Ils appliquent pour ce faire des normes appelées *protocoles de communication*. 

Le protocole de communication d’un bus est en quelque sorte la langue qu’il parle, intimement liée à son architecture. A titre d’exemple de bus, vous connaissez sûrement le très populaire *bus série universel* (acronyme *USB* pour *Universal Serial Bus*, en anglais).

### **Les entrées-sorties**

Le microcontrôleur interagit avec le monde extérieur par l’intermédiaire de circuits appelés *entrées-sorties* capables de :

 - Convoyer un signal électrique arrivant sur certaines broches de son boitier et les convertir en informations binaires que le microprocesseur pourra manipuler ;
 - Convoyer un signal binaire depuis le microprocesseur, en passant par des broches dédiées, en le convertissant au passage de sorte qu’il puisse piloter un moteur, activer un relai, activer un capteur ... *à l'extérieur du microcontrôleur*.

Certains bus du microcontrôleur sont connectés à ses entrées-sorties et à ses broches, l’exemple de l’USB est encore opportun ici. Le circuit contrôleur USB est intégré au microcontrôleur, mais le bus lui même est destiné à relier à l'application finale des périphériques externes tels que claviers, souris, Web CAM, clef USB, etc. 

### **Les périphériques**

On désigne par [*périphériques*](https://openclassrooms.com/fr/courses/5119661-initiez-vous-a-lelectronique-embarquee-capteurs-et-actionneurs/5119668-decouvrez-les-principaux-peripheriques-d-un-microcontroleur-autour-d-un-exemple) des circuits intégrés au microcontrôleur qui réalisent des fonctions parfois très spécialisées requises par les applications (conversion de signaux, pilotes d’écrans tactiles, communication avec des capteurs…). Les microcontrôleurs récents offrent communément plusieurs dizaines de périphériques. Il est impossible de les lister tous ici, en voici quelques-uns parmi les plus communs extraits du [*glossaire*](../Kit/glossaire) :
 
 - **ADC** : Analog to Digital Converter (fr : Convertisseur Analogique vers Numérique). Circuit électronique spécialisé dans la conversion de signaux d’entrée analogiques (des tensions éventuellement variables dans le temps, comprises entre 0 et +3.3V) en signaux numériques (des nombres entiers proportionnels à la tension d’entrée à chaque instant et compris entre 0 et 4096 pour un ADC avec une résolution de 12 bits).

 - **DAC** : Digital to Analog Converter (fr : Convertisseur Numérique vers Analogique). Un convertisseur numérique-analogique est un circuit électronique dont la fonction est de transformer une valeur numérique qu’il reçoit en une valeur analogique qui lui est proportionnelle et qu’il transmet aux circuits électroniques suivants, capables uniquement de traiter un signal analogique encodé sous forme d’une tension variable.

 - **DMA** : Direct Memory Access (fr : Contrôleur d’accès direct à la mémoire). Circuit électronique connecté, sur le même bus interne que le microprocesseur, spécialisé dans les transferts d’informations d’une zone mémoire vers une autre. Le contrôleur DMA fonctionne en parallèle avec le microprocesseur et est nettement plus rapide que celui-ci pour les opérations de copie qu’il réalise.

 - **I2C** : Inter-Integrated Circuit (fr : bus de communication intégré inter-circuits). Contrôleur de bus série fonctionnant selon un protocole inventé par Philips. Le bus I2C a été créé pour fournir un moyen simple de transférer des informations numériques entre des capteurs, des actuateurs, des afficheurs et un microcontrôleur.

 - **IWDG** : Independant Watchdog (fr : chien de garde indépendant). Le chien de garde indépendant est un circuit électronique qui fonctionne avec une horloge dédiée. Il réalise un compte à rebours depuis une valeur maximum programmable jusqu’à zéro. L’IWDG génère un redémarrage (reset) du microcontrôleur lorsque le compte à rebours atteint zéro sauf si celui-ci est relancé à partir de sa valeur maximum avant cette échéance par une instruction du programme utilisateur.

 - **RTC** : Real-Time Clock (fr : Horloge Temps-Réel). Circuit électronique remplissant les fonctions d’une horloge très précise pour des problématiques d’horodatage. Suivant sa complexité, la RTC pourra aussi gérer des fonctions plus complexes comme des alarmes déclenchées suivant un calendrier, voire mettre le microcontrôleur en sommeil et le réveiller si elle est intégrée dans celui-ci.

 - **SPI** : Serial Peripheral Interface (fr : Interface Série pour Périphériques). Contrôleur de bus série fonctionnant selon un protocole inventé par Motorola. Un bus série de ce type permet la connexion, de type maître-esclave, de plusieurs circuits disposant d’interfaces compatibles avec trois fils de liaisons.

 - **Timer** (fr : Compteur). Circuit électronique basé sur un compteur qui coordonne des fonctions variées (selon son architecture) telles que : lecture de signaux extérieurs variables dans le temps, génération de signaux modulés en largeur d’impulsion (pour le contrôle de moteurs électriques), génération d’évènements périodiques, etc.

 - **UART** : Universal Asynchronous Receiver Transmitter (fr : Récepteur-Transmetteur Asynchrone Universel). En langage courant, c’est un composant utilisé pour échanger des messages (souvent du texte) entre le microcontrôleur et certains modules au fonctionnement complexe tels que récepteurs GPS ou modules radio-fréquence Wi-Fi.

### **Les horloges et les alimentations**

Pour finaliser notre introduction à l’anatomie des microcontrôleurs, il nous faut encore parler *des horloges et des alimentations*.

Un microprocesseur fonctionne comme un automate très complexe qui enchaîne ses opérations *dans un ordre précis* pour réaliser ses calculs. On peut le comparer à une chaîne de fabrication robotisée avec des tapis roulants comme on en trouve par exemple dans l’industrie automobile. Si le tapis roulant qui emmène les pièces et les robots qui les manipulent ne sont pas parfaitement synchronisés, toute la belle mécanique s’enraye !

On doit donc disposer d'un signal périodique et très stable pour synchroniser l'activité de tous les composants intégrés au microcontrôleur. En pratique, ce tempo est généré par des circuits spécialisés appelés *horloges*. Lorsqu'une grande précision est requise, les horloges du microcontrôleur utilisent comme signaux-sources les oscillations piézoélectriques d'un [cristal de quartz](https://fr.wikipedia.org/wiki/Quartz_(%C3%A9lectronique)) exactement comme les "montres à quartz".

Les microprocesseurs modernes, fruits de 60 ans d’ingéniosité et d’optimisations, peuvent changer l’ordre de certaines instructions au moment de l’exécution ou encore les répartir sur plusieurs files de traitement indépendantes et simultanées, mais ils ont toujours besoin d’horloges pour fixer leur cadence.

Les microprocesseurs les plus rapides actuellement sur le marché supportent des [*fréquences d’horloge*](https://fr.wikipedia.org/wiki/Fr%C3%A9quence_d%27horloge) approchant (ou dépassant légèrement) les 5 Gigahertz, ce qui signifie que leurs transistors peuvent passer de l’état ouvert à l’état fermé cinq milliards de fois par seconde. En conséquence, ces microprocesseurs pourront réaliser environs 3 milliards d’opérations (additions, multiplications, soustractions …) par seconde. En général ils sont réservés aux applications pour lesquelles la puissance de calcul est plus importante que la frugalité énergétique, telles que les ordinateurs personnels, les serveurs et les fermes de calcul. On ne les retrouve pas au cœur des microcontrôleurs car de telles fréquences impliquent une consommation d’énergie excessive, un échauffement intenable et des coûts de conception et fabrication bien trop élevés pour les applications ciblées. 

Les fréquences des horloges intégrées à un microcontrôleur et à son microprocesseur dépassent donc rarement quelques centaines de millions de cycles par seconde (on dit de Mégahertz). Cependant, la progression spectaculaire du marché des systèmes sur puces, portée par la téléphonie mobile, pousse à la conception de microcontrôleurs d'une nouvelle génération ayant des fréquences d'horloges très proches de celles des microprocesseurs de nos ordinateurs personnels.

Les circuits qui génèrent et apportent des signaux d’horloges à tous les composants du microcontrôleur sont couplés aux *circuits d'alimentation électrique* de celui-ci. On peut donc programmer un microcontrôleur pour éteindre ou allumer ses périphériques et leurs horloges selon les besoins, afin notamment *d’optimiser sa consommation d’énergie* et de décharger moins vite une éventuelle batterie si l'application étudiée est un téléphone portable, une montre connectée ou tout autre objet sans fil. 

## **Des microcontrôleurs pour les systèmes embarqués**

Ordinateurs condensés sur une puce, les microcontrôleurs se sont imposés dans les applications requérant l’une ou plusieurs des caractéristiques suivantes :

 1. *Un excellent compromis entre les performances de calcul et la consommation d’énergie*. Correctement programmé, un microcontrôleur basse-consommation peut traiter un signal audio, communiquer en Bluetooth et réaliser bien d’autres tâches tout en étant alimenté par une pile bouton ;
 2. *Un prix contenu de quelques euros tout au plus*, bien inférieur à celui des microprocesseurs centraux des ordinateurs ou des smartphones haut de gamme ;
 3. *Une réactivité dite "temps-réel"*. Le temps de réponse du microcontrôleur à certains signaux extérieurs prédéfinis doit être court et garanti.
 
Les applications qui répondent à ce cahier des charges sont généralement des [*systèmes embarqués*](https://fr.wikipedia.org/wiki/Syst%C3%A8me_embarqu%C3%A9). Quelques exemples concrets :

 - Les microcontrôleurs se sont taillée la part du lion dans le marché en pleine croissance des objets connectés qui tire profit – et dépend désormais totalement – de leurs atouts et spécificités. 
 - On les trouve dans quasiment tous les objets du quotidien dont ils simplifient ou augmentent considérablement l’ergonomie et l’expérience, depuis les tableaux de contrôle et les afficheurs tactiles des appareils électroménagers et des automobiles jusqu’aux écouteurs qui annulent le bruit ambiant, en passant par les jouets éducatifs, les manettes des consoles de jeu et les ampoules LED.
 - Leur caractéristique temps-réel leur a permis également de conquérir les marchés des équipements de sécurité automobile, des unités de stabilisation des drones, des automates industriels, etc.

**Les microcontrôleurs sont tout autour de nous et nous les utilisons à longueur de journée sans en avoir conscience !**

## **Les microprocesseurs ARM Cortex M et l’architecture STM32**

La société anglaise [*ARM*]( https://fr.wikipedia.org/wiki/ARM_(entreprise)) est à l’origine de l'architecture de microprocesseurs [*Cortex M*](https://fr.wikipedia.org/wiki/ARM_Cortex-M) spécialement conçue pour les microcontrôleurs. 

Pour reprendre les propos de [Wikipedia]( https://fr.wikipedia.org/wiki/Architecture_ARM) :

>*Une particularité des processeurs ARM est leur mode de vente. En effet, ARM Ltd. ne fabrique ni ne vend ses processeurs sous forme de circuits intégrés. Cette société vend les licences de ses processeurs à d'autres fabricants, qui pourront les intégrer dans leurs puces. Aujourd'hui, la plupart des grands fondeurs de puces ont acheté des licences à ARM et proposent des systèmes sur puces intégrant des microprocesseurs Cortex.*

C’est le cas de [*la société Franco-Italienne STMicroelectronics*](https://www.st.com/content/st_com/en.html) qui les utilise pour sa gamme de microcontrôleurs [*STM32*]( https://fr.wikipedia.org/wiki/STM32). La figure ci-dessous donne le schéma de principe d'un microcontrôleur STM32F103, l'un des tout premiers nés de la famille STM32 :

<br>
<div align="left">
<img alt="Schéma de principe d'un microcontrôleur STM32F103" src="images/Schema_STM32F103.jpg" width="550px">
</div>
<br>

> Source : [STMicroelectronics](https://www.st.com/content/st_com/en.html)

Ce schéma précise l'architecture du Cortex M3, on constate qu'il ne s'agit pas exclusivement d'un microprocesseur (autrement appelé *CPU* pour *Central Processing Unit*) car on y trouve également :

 - Le **JTAG/SWD** pour *Joint Test Action Group / Serial Wire Debug* (pas de version française usuelle). Il s'agit d'interfaces (bus) de communication avec les fonctions de débogage en temps réel intégrées dans le Cortex. Ces circuits rendent les logiciels de débogage plus performants.
 - Le **SysTick timer** (en français : Timer système). C'est un compteur capable de générer une base de temps précise et régulière, dont les performances sont normalisées. Il est essentiellement conçu pour cadencer les éventuels [*systèmes d’exploitation en temps réel*](https://fr.wikipedia.org/wiki/Syst%C3%A8me_d%27exploitation_temps_r%C3%A9el) qui pourraient être installés sur le microcontrôleur.
 - Le **NVIC** pour *Nested Vectored Interrupts Controller* (en français : Contrôleur d’Interruptions Vectorisées et Imbriquées). Ce circuit intégré est chargé de la gestion des [*interruptions*](https://fr.wikipedia.org/wiki/Interruption_(informatique)). Il optimise en particulier la réactivité du système à celles-ci en leur assignant des priorités et en gérant les situations où plusieurs surviennent dans un intervalle de temps très court. Le sujet des interruptions est central pour les microcontrôleurs, vous trouverez quelques explications supplémentaires et un exemple pour leur mise en œuvre en MicroPython avec [ce tutoriel](https://stm32python.gitlab.io/fr/docs/Micropython/startwb55/bouton_it).

On retrouve bien entendu les mémoires, bus et entrées-sorties déjà expliqués et un grand nombre de périphériques rajoutés par STMicroelectronics, généralistes ou spécialisés pour répondre à des problématiques précises posées par les applications. Ils sont listés exhaustivement [sur ce site](https://www.theengineeringknowledge.com/introduction-to-stm32f103/) et vous trouverez également une brève description de chacun d’entre eux dans [**le glossaire**](../Kit/glossaire). 

Dans "STM32", le "32" signifie que l'architecture est de type *32 bits*. Le Cortex M dispose en effet de registres capables de mémoriser 32 bits, ce qui lui permet de calculer et manipuler des informations codées au maximum avec des séquences de 32 symboles "0" et "1" consécutifs à chaque cycle, mais aussi d'adresser (i.e. d'utiliser) jusqu'à 2<sup>32</sup> octets de mémoire individuels (soit environ 4 milliards d'octets, mais la mémoire disponible en pratique sur un système sur puce excède rarement quelques millions d'octets).

Depuis le STM32F103, STMicroelectronics [*a largement diversifié son offre de microcontrôleurs basse consommation STM32*](https://www.st.com/en/microcontrollers-microprocessors/stm32-32-bit-arm-cortex-mcus.html). Parmi les périphériques embarqués dans les nouvelles puces on trouve notamment des émetteurs – récepteurs radiofréquence (les *STM32WB* en 2019 puis les *STM32WL* en 2021) pour les applications de type objets connectés. Nous avons choisi le [*STM32WB55*]( https://www.st.com/en/microcontrollers-microprocessors/stm32wb-series.html), équipé d'une fonction "radio" Bluetooth, pour supporter l’initiative pédagogique [**STM32python**](../Micropython/index).

Un effort tout particulier est également fait par STMicroelectronics pour la sécurisation des architectures avec les familles [*STM32L5*](https://www.st.com/en/microcontrollers-microprocessors/stm32l5-series.html) et [*STM32U5*](https://www.st.com/en/microcontrollers-microprocessors/stm32u5-series.html) et sur la conception de SoC "hybrides" avec la famille [*STM32MP*](https://www.st.com/en/microcontrollers-microprocessors/stm32mp1-series.html), qui associe des microprocesseurs très performants à un microcontrôleur basse consommation.
