---
title: Les transistors, les puces, le numérique
description: Les bases pour comprendre les fondations des technologies numériques
---

# Les transistors, les puces, le numérique

Pour comprendre les technologies numériques omniprésentes il faut d’abord appréhender l’une des réalisations technologiques les plus révolutionnaires de l'histoire : **les puces électroniques**. Leur histoire est aussi celle des **transistors au silicium.**

## Les puces électroniques et les transistors 

Les [*puces électroniques*](https://fr.wikipedia.org/wiki/Circuit_int%C3%A9gr%C3%A9), autrement appelées *circuits intégrés*, sont PARTOUT et peuvent être considérées comme *une matière première* pour toutes les technologies que nous utilisons à longueur de journée. Ordinateurs, smartphones, voitures électriques, cartes de crédit, cartes de transports en commun, TV, box Internet, équipements médicaux, réseaux électriques intelligents… fonctionnent **tous** grâce à une multitude de circuits intégrés.<br>

Mais qu'est-ce qu'un circuit intégré ?

Il s'agit tout simplement d’un circuit électronique minuscule constitué de *milliards* de câbles de connexion, de condensateurs, de résistances, de diodes … mais surtout de [*transistors*](https://fr.wikipedia.org/wiki/Transistor). Un transistor est un composant qui se comporte comme *un interrupteur* que l’on peut commander avec une tension électrique. Et le succès de la microélectronique repose  sur l'invention de transistors très particuliers que l'on ne cesse de miniaturiser depuis les années 1950. 

## La "loi" de Moore

L’industrie qui fabrique les puces, autrement nommée *industrie des semi-conducteurs*, bénéficie d’une croissance soutenue (plus de 7% par an en moyenne depuis 1996) essentiellement due au potentiel stupéfiant de miniaturisation des transistors (dans le secteur on préfère parler *d’intégration*) offert par ses procédés de fabrication. L’un des co-fondateurs de la société Intel, *Gordon Earle Moore*, est devenu célèbre pour son énoncé empirique, [*la loi de Moore*](https://fr.wikipedia.org/wiki/Loi_de_Moore), qui se vérifia infailliblement jusqu’au début des années 2000 : 

>*"Le nombre de transistors à l’intérieur d’un circuit intégré de surface donnée double tous les 18 mois"*.

Cela ne vous parait sans doute pas évident si vous n’êtes pas féru de mathématiques, mais ne vous y trompez pas : il s’agit d’une tendance *exponentielle*, qui a permis aux transistors de rapetisser à une vitesse vertigineuse et à leurs performances d’augmenter au même rythme.

En 1945, le tout premier calculateur électronique de l’histoire, [l’ENIAC](https://fr.wikipedia.org/wiki/ENIAC), était conçu avec des [tubes à vide](https://fr.wikipedia.org/wiki/Tube_%C3%A9lectronique) comme transistors ; il en totalisait 17468 d’une hauteur individuelle d'une dizaine de centimètres. ENIAC emplissait un hangar de 167 mètres carrés. En fonctionnement, il consommait autant d’énergie qu’une petite ville pour une puissance de calcul approximativement égale à celle d'une calculette contemporaine (autour d’un millier d’opérations par seconde).

En 2021, [la puce M1 de Apple](https://fr.wikipedia.org/wiki/Apple_M1) contient 16 milliards de *transistors au silicium*, d’une largeur individuelle de 5 nanomètres (5 milliardièmes de mètres, grosso-modo 10 fois plus petits que la particule virale SARS-Cov 2 tristement célèbre). Le Apple M1 calcule environs 2,6 milliards de fois plus vite que l’ENIAC et consomme à peine autant que deux ampoules LED contemporaines.

C'est bien entendu le passage de la technologie des tubes à vide à celle des transistors au silicium qui a permis pareille intégration (figure suivante).

<br>
<div align="left">
<img alt="Evolution des transistors" src="images/Evolution_transistors.jpg" width="700px">
</div>
<br>

Ces comparaisons et analogies ne sont pas rigoureuses mais elles donnent une intuition correcte des progrès phénoménaux de la microélectronique depuis sa naissance. Risquons-en une dernière. Le smartphone que vous transportez dans votre poche n’aurait pas pu être construit au temps des premières puces car il fait appel à un grand nombre de technologies (capteurs et connectivité) développées entre 1980 et 2000. Mais si on reconstruisait partiellement la puce qui l’anime, son *microcontrôleur* principal, avec les tubes à vide de l’ENIAC de 1945, celle-ci aurait une surface de … **110 kilomètres carrés** ! (à titre de comparaison, Paris intra-muros s’étale sur 150 kilomètres carrés).

La loi de Moore présente cependant, depuis les années 2000, un essoufflement manifeste : la miniaturisation se poursuit (certaines compagnies envisagent de produire en quantités industrielles des puces avec des transistors de 2 nanomètres de large d’ici quelques années) mais a considérablement ralenti car *le coût* de fabrication de puces intégrant de plus en plus de transistors augmente dans des proportions vertigineuses. Il ne faut cependant pas sous-estimer l’inventivité des ingénieurs. Ils élaborent de nouvelles approches pour prolonger la loi de Moore, notamment en associant ou en empilant plusieurs puces très minces et en les interconnectant dans un même boitier.

## La fabrication des puces

Les puces ont une structure en couches très fines de l’ordre du *millionième de mètre*, une mesure de distance que l'on appelle le *micron*. Leurs fondations reposent sur un [*substrat de silicium*](https://fr.wikipedia.org/wiki/Wafer) (symbole chimique *Si*) purifié à l’extrême et parfaitement [*cristallisé*](https://fr.wikipedia.org/wiki/Silicium) ayant l'aspect d'une galette circulaire de 20 ou 30 cm de diamètre et 0.75 mm d'épaisseur (voir le complément plus bas).

Sur ces galettes de silicium cristallisé, on vient d’abord fabriquer des dizaines de milliards de *transistors*. Ceux ci sont généralement des [transistors à effet de champ de type métal-oxyde-semiconducteur](https://fr.wikipedia.org/wiki/transistor_%C3%A0_effet_de_champ_%C3%A0_grille_m%C3%A9tal-oxyde) (MOS) encore majoritairement utilisés dans les composants électroniques tels que les microcontrôleurs. Pour des explications supplémentaires sur leur fonctionnement, n'hésitez pas à consulter le complément qui leur est consacré dans la section *Pour aller plus loin*, ci-après.

Ensuite, on forme au-dessus du plan des transistors entre dix et vingt couches ayant pour fonction principale de les interconnecter et de les regrouper d'abord en puces séparées mais aussi en blocs fonctionnels au sein de chaque puce. Ces blocs pourront être commandés depuis l’extérieur, par les [*broches*](https://fr.wikipedia.org/wiki/Bo%C3%AEtier_de_circuit_int%C3%A9gr%C3%A9) du boitier de la puce une fois celle-ci terminée et assemblée. Les broches ne sont pas les seules solutions possibles pour connecter une puce au monde extérieur, elles sont parfois remplacées par un réseau de micro-billes plus compact qui réduit l'encombrement du boitier.

La figure suivante résume les principales étapes de la fabrication des puces jusqu'à leur assemblage dans un boitier :

<br>
<div align="left">
<img alt="Fabrication des puces" src="images/Si_to_packaged_dice.jpg" width="900px">
</div>
<br>

On part [d'une galette de silicium ultra-pur parfaitement cristallisé](https://fr.wikipedia.org/wiki/Wafer) initialement découpée dans un [lingot de silicium obtenu par une méthode de croissance cristalline très élaborée](https://www.sil-tronix-st.com/fr/services/lingot-de-silicium). Un long et complexe processus de fabrication (voir le complément dans le paragraphe "Pour aller plus loin", ci-après) permet de former sur les plaques un grand nombre de puces identiques ; elles ont alors l'aspect de gaufres c'est pourquoi on les désigne avec le mot anglais correspondant : *wafers*.

Une particularité de ce processus de fabrication est qu'il doit intégralement se dérouler dans un environnement aussi propre que possible, [*une salle blanche*](https://fr.wikipedia.org/wiki/Salle_blanche). Les poussières, particules et contaminations de toutes sortes, qui sont des ennemis mortels pour les puces, imposent cette précaution. [Les techniques et les technologies pour fabriquer les puces à partir du *Si*](https://www.cea.fr/comprendre/Pages/nouvelles-technologies/la-fabrication-d-une-puce-electronique.aspx) sont passionnantes, complexes et très onéreuses ; une usine pour les mettre en œuvre coûtant en 2021 près d’une dizaine de milliards d’euros lorsqu’il s’agit de produire les microprocesseurs ou les mémoires les plus performants.

Une fois formées sur les wafers, les puces sont *testées* puis le wafer est découpé de sorte à les séparer. Celles qui ne fonctionnent pas sont éliminées, les autres sont *assemblées dans des boitiers* puis testées à nouveau à la fin de cette étape. C'est fini, les puces qui fonctionnent peuvent être vendues à des concepteurs de systèmes électroniques !

## Architecture des puces et technologies numériques

Il est important de comprendre que toutes les [technologies dites "numériques"](https://fr.wikipedia.org/wiki/Technologies_de_l%27information_et_de_la_communication) existent du fait de **l’opportunité de réaliser le traitement de l’information au moyen de transistors**. Précisons… 

Les mathématiciens précurseurs puis architectes de l’informatique [ont établi](https://fr.wikipedia.org/wiki/Alg%C3%A8bre_de_Boole_(logique)) qu’en associant astucieusement des interrupteurs il était possible de fabriquer des *portes logiques* (voir [ce site](https://fr.wikipedia.org/wiki/Fonction_logique) ou encore [celui-ci.](https://www.irif.fr/~carton/Enseignement/Architecture/Cours/Gates/)) pour manipuler des nombres *binaires* c'est à dire écrits avec seulement les symboles "0" et "1". Ils ont ensuite démontré que l'on pourrait réaliser n’importe quelle opération mathématique - élémentaire (addition, multiplication, soustraction, division) ou plus complexe - sur n’importe quels nombres au moyen d'assemblages élaborés de portes logiques.

Il se trouve qu’*un transistor*, qui peut être fermé (état "1") ou ouvert (état "0"), *se comporte exactement comme un interrupteur électrique*. On peut donc construire des réseaux de portes logiques avec des transistors. Lorsque les physiciens de [la matière condensée](https://fr.wikipedia.org/wiki/Physique_du_solide) ont découvert la technologie du transistor MOS au silicium ils ont rapidement pris conscience de son fantastique potentiel de miniaturisation et inventé par la même occasion la dernière brique technologique qui manquait pour que la microélectronique connaisse l’essor fantastique que nous appelons rétrospectivement "Loi de Moore".

La figure qui suit illustre (avec certains parti-pris) la hiérarchie de structures d'un microcontrôleur : 

<br>
<div align="left">
<img alt="Portes logiques" src="images/Portes_logiques.jpg" width="900px">
</div>
<br>

La puce électronique est un circuit très complexe de quelques millimètres de côté construit sur une base en silicium. Si on agrandit une toute petite portion de celle-ci plusieurs centaines de fois, on constate que les structures actives au contact du silicium sont constituées d'assemblages complexes de transistors MOS en portes logiques. Chaque transistor MOS fonctionne comme un interrupteur électrique ; vous pouvez en apprendre plus en consultant la section qui leur est consacrée dans le paragraphe *Pour aller plus loin*, ci-après.

Il nous faut ici rendre hommage au génial mathématicien [*Alan Mathison Turing*](https://fr.wikipedia.org/wiki/Alan_Turing) (1912-1954) pour son expérience de pensée en 1936 que l'on nommera ensuite "Machine de Turing" qui est à l’origine de tous les microprocesseurs modernes. Il est également célèbre pour ses travaux en cryptographie et pour son énoncé du [*"test de Turing"*](https://fr.wikipedia.org/wiki/Test_de_Turing) dans le domaine de l’[*Intelligence Artificielle*](https://fr.wikipedia.org/wiki/Intelligence_artificielle), très populaire en ce début de XXI<sup>ème</sup> siècle.

**Les technologies numériques dépendent des puces qui manipulent l’information sous la forme de séries de "0" et de "1" à l’aide de centaines de milliers, de millions, voire de milliards de transistors construits sur une base de silicium, qui commutent entre un état ouvert et un état fermé jusqu’à plusieurs milliards de fois par seconde** !


## Pour aller plus loin

Vous trouverez dans cette section des compléments d'informations sur différents sujets évoqués tout au long de ce chapitre. 

### **Le silicium cristallisé**
Rappelons que le [*silicium*](https://fr.wikipedia.org/wiki/Silicium) est le deuxième élément chimique le plus abondant de la croute terrestre dont il constitue presque 26% de la masse. Ainsi, la plupart des roches sont majoritairement composées de *Si* et le verre commun n’est rien d’autre que du dioxyde de silicium (*SiO<sub>2</sub>*). Les plaquettes utilisées pour fabriquer les puces sont faites de Si pur et *cristallin*, ce qui signifie (en l'occurrence) que les atomes de *Si* sont parfaitement arrangés en un réseau qui ressemble aux alvéoles d’une ruche d’abeilles, selon [une structure dite *diamant*](https://fr.wikipedia.org/wiki/Structure_diamant). La figure ci-dessous illustre cette structure :
 
 <br>
 <div align="left">
 <img alt="Maille cubique face centrée d'un cristal de silicium" src="images/CFC.jpg" width="450px">
 </div>
 <br>

Purifier et cristalliser le silicium de cette façon nécessitent un savoir faire et une technologie de pointe que doivent maitriser [les sociétés qui fabriquent et vendent les plaquettes de silicium](https://www.sil-tronix-st.com/fr/services/lingot-de-silicium) aux fabricants de puces tels que STMicroelectronics. Voir par exemple [cette référence](https://microelectronique.univ-rennes1.fr/fr/index_chap3.htm) et [celle-ci](https://ecoinfo.cnrs.fr/2010/10/20/le-silicium-la-fabrication/) pour plus de détails à ce sujet. 

### **Les transistors MOS à effet de champ**
Un transistor MOS à effet de champ se comporte comme un interrupteur entre sa *source* et son *drain*, commandé par la tension appliquée sur sa *grille*, selon le processus illustré ci-dessous : 

 <br>
 <div align="left">
 <img alt="Principe du MOSFET" src="images/Principe_du_MOS.jpg" width="1000px">
 </div>
 <br>

**Légende :**
1. **Transistor ouvert**. Les électrons ne peuvent pas passer de la source au drain. Un champ électrique aux interfaces source/canal et drain/canal les maintient à l'intérieur de la source et du drain. Puisqu'aucun courant ne peut traverser le canal, le transistor est équivalent à un interrupteur ouvert.
2. **Transistor fermé**. Une tension positive est appliquée sur l'électrode de grille. Lorsqu'elle atteint une valeur suffisamment élevée (+V<sub>CC</sub>),  les électrons, attirés par la polarité "+", se condensent sous l'oxyde de grille en un pont conducteur traversant le canal. 
3. **Transistor fermé (bis)**. Les électrons circulent de la source au drain, le transistor est maintenant équivalent à un interrupteur fermé.
 Si la tension positive sur la grille retombe à zéro, les électrons seront repoussés du canal et le transistor retrouvera son état ouvert initial.

La bonne compréhension du fonctionnement d'un transistor à effet de champ, et de termes tels que [*"dopage"*](https://fr.wikipedia.org/wiki/Dopage_(semi-conducteur)), *"polycristallin"*, *"monocristallin"* ... nécessite des connaissances en [*physique des semiconducteurs*](https://fr.wikipedia.org/wiki/Semi-conducteur), un vaste sujet que nous ne souhaitons pas aborder dans ce document.

### **Un aperçu du processus de fabrication des puces**

Sans doute êtes-vous curieux d'apprendre comment il est possible de fabriquer en quantités industrielles des puces constituées de milliards de transistors désormais plus petits que des virus ? Bien évidemment, l'approche naïve qui consisterait à fabriquer tous ces transistors un par un avec de minuscules machines-outils n'est pas envisageable (une telle technologie relève actuellement de la science fiction). Pour former les transistors on utilise un ensemble de procédés physico-chimiques appelés [**photolithographie**](https://fr.wikipedia.org/wiki/Photolithographie). Examinons quelques étapes de la photolithographie : 

Dans un premier temps, des ingénieurs vont concevoir les plans de la puce à fabriquer puis les reproduire sur des diapositives en quartz et en chrome appelées **masques**. Pour un modèle de puce donnée, quelques dizaines de masques sont nécessaires, afin de reproduire ses différents "étages". 

La figure qui suit illustre cette première étape de **conception des masques** :
 
 <br>
 <div align="left">
 <img alt="Jeu de masques pour une puce" src="images/DAO.jpg" width="700px">
 </div>
 <br>


On procède ensuite à la [**photographie** ou **photolithographie**](https://fr.wikipedia.org/wiki/Photolithographie), une étape qui tire son nom du procédé utilisé pour faire des photos avant que les capteurs d'images ne remplacent les pellicules aux sels d'argent. On étale sur une plaque en silicium une couche d'un composé chimique appelé **résine photosensible**, qui a la particularité de changer de structure par réaction chimique lorsqu'il est exposé à la lumière ultraviolette.<br>
On projette sur cette "pellicule" de résine l'image du premier des masques, avec une source de lumière ultraviolette (UV). Les UV impressionnent cette image sur la résine.<br>
Les zones de résine insolées (ou exposées), qui ont réagi sous l'effet du rayonnement UV, sont ensuite retirées au cours d'une étape appelée **développement**. Elle consiste à verser sur la résine un produit chimique qui dissout sélectivement ses parties exposées. La plaque sera ensuite rincée pour ôter tout résidu.<br>
La figure qui suit illustre ces trois étapes de **photographie** sur une toute petite fraction de la surface d'une puce :

 <br>
 <div align="left">
 <img alt="Principe de la photograhie" src="images/photo.jpg" width="700px">
 </div>
 <br>

En pratique, au moment de l'insolation UV, on interpose entre le masque et la surface de résine [tout un ensemble de lentilles ou miroirs](https://fr.wikipedia.org/wiki/Lithographie_extr%C3%AAme_ultraviolet) (non représentés sur le dessin pour simplifier) qui réduisent et focalisent l'image du masque sur la plaquette, de sorte que les motifs photographiés sur la résine aient les dimensions minuscules requises. Une photo ne couvrant la surface que d'une seule puce, soit quelques millimètres carrés après réduction de l'image du masque par le système optique, il faudra répéter les étapes 2, 3, 4 jusqu'à couvrir toute la surface de la plaque de silicium d'images du niveau.

Nous disposons à présent d'un "pochoir" de résine à la surface du silicium. On plonge alors la plaque dans un [*réacteur de gravure*](https://fr.wikipedia.org/wiki/Gravure_ionique_r%C3%A9active), une chambre qui contient un gaz chargé électriquement (*un plasma*) et corrosif, choisi en l'occurrence pour dissoudre le silicium mais pas la résine. En conséquence le silicium est *gravé* partout où la résine a été développée. La résine qui n'a pas été gravée peut alors être retirée (elle est "brûlée", puis la plaque est nettoyée).

La figure qui suit illustre cette étape de **gravure** :

 <br>
 <div align="left">
 <img alt="Principe de la gravure plasma" src="images/gravure.jpg" width="800px">
 </div>
 <br>

La composition chimique du plasma de gravure sera bien entendu modifiée selon celle de la couche à graver. Certains plasmas sont adaptés pour attaquer du verre, d'autres du silicium, d'autres encore de l'aluminium, etc.

Nous disposons à présent d'une plaquette de silicium avec des motifs gravés qui reproduisent exactement ceux du masque initial. Supposons que nous souhaitions faire de ces lignes gravées des barrières isolantes. L'étape suivante va consister à les remplir de verre (isolant) suivant un procédé de [**dépôt chimique en phase vapeur**](https://fr.wikipedia.org/wiki/D%C3%A9p%C3%B4t_chimique_en_phase_vapeur) (CVD). On place la plaque dans un réacteur CVD contenant différents gaz qui vont réagir à sa surface. Ils formeront ainsi une couche de verre qui va non seulement remplir les tranchées mais aussi les recouvrir complètement.

La figure qui suit illustre cette étape de **formation de couche** par la méthode CVD :

 <br>
 <div align="left">
 <img alt="Principe du dépôt chimique en phase vapeur" src="images/CVD.jpg" width="500px">
 </div>
 <br>

La méthode du CVD n'est pas la seule utilisée pour former des couches sur les plaquettes. On se sert aussi [**du dépôt physique par phase vapeur**](https://fr.wikipedia.org/wiki/D%C3%A9p%C3%B4t_physique_par_phase_vapeur) (PVD) ou encore [**de l'oxydation thermique**](https://fr.wikipedia.org/wiki/Oxydation_thermique) selon les matériaux qui doivent constituer la couche et la qualité requise pour celle-ci (uniformité et densité, aptitude à remplir des tranchées étroites et profondes...). Tout ceci devient rapidement technique et complexe !

L'étape suivante va retirer l'excédent de verre pour n'en laisser que dans les tranchées initialement gravées. Elle est réalisée par un processus de [**polissage mécano-chimique**](https://fr.wikipedia.org/wiki/Planarisation_m%C3%A9cano-chimique) (CMP). Celui-ci consiste à passer la surface de la plaque sous une polisseuse très précise, qui va retirer le matériau en excès par abrasion physique et par attaque chimique conjointes.

La figure qui suit illustre cette étape de **CMP** :

 <br>
 <div align="left">
 <img alt="Principe du polissage mécano-chimique" src="images/CMP.jpg" width="550px">
 </div>
 <br>

**Et voilà, la reproduction du niveau est terminée !** Au départ nous avions une plaque de silicium vierge et le plan d'un étage de la puce sur un masque. A l'arrivée nous avons fidèlement reproduit dans la plaque de silicium une petite partie, un étage, du circuit électrique complet qui figurait sur le masque. 

**Bien évidemment cet exemple n'est pas complet** et passe sous silence pléthore de subtilités cruciales, essentiellement la gestion de diverses complications liées aux propriétés des matériaux utilisés et aux dimensions microscopiques des structures. Il a pour objectif d'expliquer simplement et de façon très superficielle quelques-unes des étapes technologiques clef qui interviennent dans la fabrication des puces.<br>

En pratique, le processus complet pour fabriquer un ensemble de puces identiques sur une plaque de silicium est bien plus long (plusieurs semaines, jours et nuits) et complexe car il faut construire non pas une, mais **au moins une vingtaine de couches interconnectées**, en alternant des matériaux divers (cuivre, aluminium, tungstène, alliages de cobalt et de silicium, composés d'azote et de silicium, etc.).<br>

Nous avons notamment passée jusqu'ici sous silence *une étape cruciale*, [**l'implantation ionique (ou le dopage)**](https://fr.wikipedia.org/wiki/Implantation_ionique) qui n'était pas nécessaire pour l'exemple que nous avions choisi. Le dopage consiste en l'insertion d'impuretés dans le silicium, après photolithographie, afin de moduler sa conductivité électrique selon le masque considéré. Durant cette étape, une très petite quantité d'éléments chimiques est introduite dans les zones exposées afin de modifier localement et moduler les caractéristiques électriques du silicium. Ces éléments dopants sont introduits dans la structure cristalline du silicium par implantation puis activation thermique (un recuit bref et à très haute température).

La figure qui suit illustre cette étape de **l'implantation ionique** :

 <br>
 <div align="left">
 <img alt="Principe de l'implantation ionique" src="images/implantation_ionique.jpg" width="800px">
 </div>
 <br>

Ce "dopage sélectif" est indispensable pour que les transistors fonctionnent comme des interrupteurs.

Enfin, de nombreuses étapes de **contrôle** jalonnent le processus pour détecter et corriger d'éventuels défauts physiques et chimiques (particules ou substances susceptibles de compromettre le bon fonctionnement de la puce) mais aussi pour anticiper des pannes sur les machines de la ligne de fabrication via un suivi statistique.<br>

La figure qui suit résume ce long et complexe processus de fabrication :
 <br>
 <div align="left">
 <img alt="Processus de fabrication des puces en salle blanche" src="images/fabrication_salle_blanche.jpg" width="900px">
 </div>
 <br>

Précisons que ce procédé ne peut être réalisé que dans une [**salle blanche**](https://www.gataka.fr/limportance-salles-blanches-microelectronique/) c'est à dire un environnement stérile plusieurs centaines de fois plus propre qu'un bloc opératoire, dans lequel l'air est filtré sans cesse pour réduire au maximum la présence de poussières et autres contaminations indésirables. Cet environnement s'impose, autrement, aucune puce fonctionnelle ne sortirait de l'usine. Imaginez les conséquences désastreuses que provoquerait *un seul grain de poussière* ou de pollen, des milliers de fois plus gros que les transistors, qui viendrait se déposer sur une portion du circuit intégré pendant sa fabrication !

**Vous devez surtout retenir qu'en alternant dans le bon ordre plusieurs centaines d'étapes de photolithographie, gravure, dépôt ou croissance de couche, polissage, dopage, nettoyage, contrôle, recuit... on peut construire simultanément, couche par couche, plusieurs centaines ou milliers de puces identiques (et donc des milliards de transistors) sur une plaquette de silicium.**


 > Sources pour cet article : STMicroelectronics



