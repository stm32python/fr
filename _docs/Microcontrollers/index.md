---
title: Des transistors aux microcontrôleurs
description: Introduction aux microcontrôleurs et aux systèmes embarqués
---

# Des transistors aux microcontrôleurs

Cette section est une introduction à l’univers de la microélectronique en général et à celui des microcontrôleurs en particulier. Elle s’adresse en priorité à quiconque souhaite s’initier à ces sujets pour les étudier plus en détails, pour les enseigner dans le secondaire ou encore par curiosité et passion de la technologie.<br>
- Elle commence par expliquer ce que sont les puces électroniques et quels sont les principes qui interviennent dans leur fabrication, puis dans leur fonctionnement. On précise à cette occasion le lien entre le mot "numérique" et l'architecture des puces [(Chapitre 1)](numerique).<br>
- Elle se focalise ensuite sur les microcontrôleurs, un type de puce très répandu dans les applications actuelles [(Chapitre 2)](microcontroleur).<br>
- Enfin, elle donne une vue d'ensemble des différentes façons de programmer un microcontrôleur [(Chapitre 3)](programmer).<br>

Le [glossaire](../Kit/glossaire) apporte des définitions concises des acronymes les plus souvent rencontrés dans les documentations techniques concernant les microcontrôleurs ainsi que les systèmes embarqués et les systèmes connectés qu'ils animent.


## Sommaire

1. [Les transistors, les puces, le numérique](numerique)
2. [Qu'est-ce qu'un microcontrôleur ?](microcontroleur)
3. [Comment programme-t-on un microcontrôleur ?](programmer)
4. [Glossaire](../Kit/glossaire)

