---
title: La bande ISM pour les LPWAN (couche RF)
description: Présentation de la bande Industrielle Scientifique et Médicale et son utilisation par LoRa
---

# La bande ISM pour les LPWAN (couche RF)

Les LPWAN, comme la plupart des technologies RF de l'IoT (RFID, WiFi, Bluetooth, ZigBee, etc.) utilisent des bandes de fréquences à usage libre – sans licence – **I**ndustrielles **S**cientifiques et **M**édicales (ISM) disponibles mondialement. En contrepartie de leur gratuité, elles doivent être partagées avec un grand nombre d'autres utilisateurs. Elles sont donc régulées pour que leur bande passante ne soit pas saturée, mais aussi pour respecter des contraintes en matière de puissance rayonnée. 

Ces régulations sont différentes selon les régions du monde. En Europe, LoRaWAN utilise **la bande ISM des 868 MHz (de 865 à 870 MHz)**,  découpée en **six sous-bandes** soumises aux règles suivantes :

* La **puissance d'émission** doit être inférieure à une valeur donnée (en général 25 mW).
* La **fréquence d'émission** des objets doit être contenue afin de réduire les risques de collisions entre les messages de plusieurs objets. Des **coefficients d’utilisation limite** (ou **duty cycle** en anglais) sont appliqués dans ce but. Le duty cycle est défini comme le pourcentage du temps pendant lequel le dispositif émet effectivement dans la bande de fréquences concernée. Par exemple, un duty cycle de 1% autorise un objet à émettre au plus 36 secondes chaque heure.

Le tableau suivant précise les puissances et les fréquences d'émission pour les différentes bandes ISM sub-gigahertz qui nous intéressent :

|Bande (MHz)|Duty Cycle (%)|Puissance émission (mW)|Commentaire|
|:-:|:-:|:-:|:-|
|865 - 868|1|25|Canaux LoRaWAN possibles, utilisée par RFID|
|**868 - 868.6**|**1**|**25**|**Canaux standard de LoRaWAN et de Sigfox**|
|868.7 - 869.2|0.1|25|Adapté pour des objets qui émettent une fois par jour|
|869.3 - 869.4|NA|10|Interdit aux LPWAN, utile pour des communications locales (ex. base-terminal de DECT)|
|869.4 - 869.65|10|500|Adapté aux passerelles pour le downlink vers de nombreux nœuds|
|869.7 - 870|1|25|Autres canaux disponibles pour LoRaWAN

Source : [Blog disk91.com](https://www.disk91.com/2017/technology/sigfox/all-what-you-need-to-know-about-regulation-on-rf-868mhz-for-lpwan/)

L'essentiel des communications LoRaWA (et Sigfox) se déroule dans la bande 868 MHz - 868.6 MHz, qui est découpée **en 3 canaux de 125 kHz de large**, **mais pas seulement**. La figure suivante détaille **l'ensemble des canaux LoRaWAN possibles et leurs bandes passantes en Europe** :

<br>
<div align="left">
<img alt="Canaux LoRaWAN EU" src="images_lora/lora_eu_chan.jpg" width="700px">
</div>
<br>

Source : [Mobile Fish](https://www.mobilefish.com/download/lora/lora_part11.pdf)

Les canaux sont réservés pour des opérations précises qui nécessitent des notions expliquées dans d'autres sections de cet article : [le facteur d'étalement (SF)](lora_css) et les ["slots" de downlink](lora_classes).<br>
Chaque canal a donc une largeur, **sa bande passante** ou **bandwidth** (BW) en anglais avec une fréquence minimum et une fréquence maximum telles que F<sub>max</sub> – F<sub>min</sub> = 125 kHz.<br>
Par exemple, pour le canal de fréquence centrale (ou **porteuse**) 867.1 Mhz :

<br>
<div align="left">
<img alt="Bande passante d'un canal LoRa" src="images_lora/lora_channel_bw.jpg" width="400px">
</div>
<br>

Source : [Mobile Fish](https://www.mobilefish.com/download/lora/lora_part11.pdf)

## Ressources

Cette section est fortement inspirée des articles suivants :
- [Blog disk91.com - *All you need to know about regulation on RF 868MHz for LPWAN*](https://www.disk91.com/2017/technology/sigfox/all-what-you-need-to-know-about-regulation-on-rf-868mhz-for-lpwan/)
- [Mobile Fish - *LoRa / LoRaWAN tutorial part 11*](https://www.mobilefish.com/download/lora/lora_part11.pdf)