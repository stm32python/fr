---
title: LoRa et LoRaWAN
description: Explication de de la technologie LPWAN LoRa et du protocole réseau associe, le LoRaWAN
---

# LoRa et LoRaWAN

## Généralités

[**LoRa**](https://fr.wikipedia.org/wiki/LoRaWAN), acronyme de *"**Lo**ng **Ra**nge"* ("longue portée") est une technologie de  [modulation](https://fr.wikipedia.org/wiki/Modulation_du_signal) d'ondes radios créée et brevetée par la start-up française Cycleo en 2009 (la modulation [CSS](https://en.wikipedia.org/wiki/Chirp_spread_spectrum), que nous verrons plus en détails) et rachetée en 2012 par [*Semtech*](https://www.semtech.fr/). [**LoRaWAN**](https://fr.wikipedia.org/wiki/LoRaWAN) est l'acronyme de *"**Lo**ng **Ra**nge **W**ide **A**rea **N**etwork"*, il s'agit du *"réseau étendu à longue portée"* et des protocoles qui reposent sur LoRa.

LoRaWAN appartient à la famille plus large des [LPWAN](https://fr.wikipedia.org/wiki/Low_Power_Wide_Area_Network) (pour *"Low Power Wide Area Networks"*) dédiée aux applications [IoT](https://fr.wikipedia.org/wiki/Internet_des_objets) qui est constituée de protocoles de communication à très bas débit, très longue portée et très faible consommation. Outre LoRaWAN, les LPWAN les plus connus sont le [*Narrow Band IoT* (NB-IoT)](https://en.wikipedia.org/wiki/Narrowband_IoT), le [*Long Term Evolution for Machines* (LTE-M)](https://en.wikipedia.org/wiki/LTE-M), tous deux utilisant le réseau cellulaire et [Sigfox](https://fr.wikipedia.org/wiki/Sigfox).

LoRaWAN est un réseau en étoile qui permet à un très grand nombre d'objets connectés, ou **nœuds**, de communiquer avec **des passerelles**. Les passerelles reçoivent et décodent les messages en provenance des nœuds et les postent vers des serveurs réseaux (voir la section [**L'infrastructure LoRaWAN**](lorawan_infra)) :


<br>
<div align="left">
<img alt="Réseau LoRaWAN" src="images_ttn_cayenne/lorawan_schematics.jpg" width="600px">
</div>
<br>

> Source : [Deltalab](https://deltalabprototype.fr/category/projets/lorawan/)


Pour fixer les idées sur les performances de LoRa, le tableau qui suit compare plusieurs protocoles radiofréquence fréquemment utilisés pour les objets connectés :

<br>
<div align="left">
<img alt="Performances LoRa" src="images_lora/lora_range_conso.jpg" width="500px">
</div>
<br>

> Source : [Mobile Fish](https://www.mobilefish.com/download/lora/lora_part1.pdf)

On constate que la portée des communications LoRa est exceptionnelle (en pratique plus de 800 km réalisés !) mais, en contrepartie, **leur débit est extrêmement faible** (quelques centaines d'octets par jour du fait [des choix technologiques](lora_css) et des [contraintes réglementaires sur les bandes de fréquences utilisées](lora_ism)). 

Puisqu'il est possible de déployer un réseau LoRaWAN sans souscription à un abonnement auprès d'un opérateur, ce LPWAN est idéal pour l'enseignement et est également devenu très populaire dans la sphère publique, pour l'installation de maillages de capteurs environnementaux. Du fait de sa gratuité, et malgré l'arrivée de la 5G et de ses protocoles dédiés à l'IoT via le réseau cellulaire (le NB-IoT et le LTE-M déjà cités), il parait peu probable que l'engouement pour LoRa et LoRaWAN faiblisse à court ou moyen terme.<br>

## Table des matières

Pour la suite de cet article, nous allons nous appuyer sur la **pile protocolaire LoRa / LoRaWAN**, illustrée ci-dessous, et commenter ses différentes couches, de la plus basse à la plus haute :

<br>
<div align="left">
<img alt="Pile LoRa/LoRaWAN" src="images_lora/lora_stack.jpg" width="600px">
</div>
<br>

> Source : [Mobile Fish](https://www.mobilefish.com/download/lora/lora_part2.pdf)

Nous vous conseillons de lire les différentes sections de cet article dans l'ordre ci-dessous, qui introduisent les notions importantes pas à pas :

1. [**Notions sur la transmission radiofréquence**](lora_rf)

2. [**La bande ISM pour les LPWAN (couche RF)**](lora_ism)

3. [**La modulation LoRa par étalement de spectre (couche PHY)**](lora_css)

4. [**Les trames LoRa, le Time On Air et le débit binaire (couche PHY)**](lora_trames)

5. [**L'Adaptative Data Rate de LoRaWAN (couche MAC)**](lorawan_adr)

6. [**Les classes d'objets LoRaWAN (couche MAC)**](lora_classes)

7. [**Activation et sécurité d'une liaison LoRaWAN (couche MAC)**](lorawan_secu)

8. [**L'infrastructure LoRaWAN (couche applicative)**](lorawan_infra)

## Ressources & références pour aller plus loin

**Sur LoRa et LoRaWAN en général :**
 - [Une vidéo de Sylvain MONTAGNY et Antoine AUGAGNEUR de Université de Savoie - Mont Blanc](https://youtu.be/j0ONEdkOm28)
 - [Le wiki de STMicroelectronics](https://wiki.st.com/stm32mcu/wiki/Category:LoRaWAN)
 - [L'application note AN1200.22 sur le site de Semtech](https://www.semtech.fr/)
 - [Le site LoRa developpers](https://lora-developers.semtech.com/documentation/tech-papers-and-guides/lora-and-lorawan/)
 - [Le site de l'Alliance LoRa](https://resources.lora-alliance.org/)
 - [Le site de l'INSA Toulouse](https://gei.insa-toulouse.fr/fr/formation_initiale/bureaux-d-etudes-et-tp/reseau-lora.html)
 - [Les tutoriels LoRa et LoRaWAN par Robert Lie, sur le site "Mobile Fish"](https://www.mobilefish.com/developer/lorawan/lorawan_quickguide_tutorial.html)
 - [L'article Wikipédia sur LoRaWAN](https://fr.wikipedia.org/wiki/LoRaWAN)
 - [Le livre gratuit de Sylvain Montagny de l'Université de Savoie - Mont Blanc](https://www.univ-smb.fr/lorawan/livre-gratuit/)
 - [Le site de Sakshama Ghoslya](http://www.sghoslya.com/p/lora-vs-lorawan.html)
 - [Le blog IoT disk91.com](https://www.disk91.com/all-about-lorawan/)
 - [Le site Wireless Pi](https://wirelesspi.com/understanding-lora-phy-long-range-physical-layer/)
 - [La ressource Eduscol "Caractérisation de l’interface radio LoRa d’un réseau de communication LoRaWAN" (BTS, Lycée Dorian, Paris)](https://eduscol.education.fr/sti/sites/eduscol.education.fr.sti/files/ressources/pedagogiques/10864/10864-caracterisation-interface-radio-lor-vf.pdf)

**Travaux pratiques LoRa et LoRaWAN  sur notre site :**
 - [Configuration d'une passerelle LoRaWAN The Things Indoor (TTIG)](ttig)
 - [Publication LoRaWAN avec le module Grove LoRa-E5, en MicroPython](../Micropython/grove/lora-e5)
 - [Communication pair-à-pair entre deux modules Grove LoRa-E5, en MicroPython](../Micropython/grove/lora-e5-p2p)
 - [Publication LoRaWAN avec le module Grove LoRa-E5, version Arduino](../Stm32duino/exercices/lora-e5)
 - [Communication pair-à-pair entre deux modules Grove LoRa-E5, version Arduino](../Stm32duino/exercices/p2p_loraE5)
 - [Publication LoRaWAN avec la NUCLEo6WL55JC1, version Arduino](../Stm32duino/exercices/lora-nucleo-wl55jc1)