---
title: Configuration d'une passerelle LoRaWAN The Things Indoor 
description: Configuration d'une passerelle The Things Indoor (TTIG) sur The Things Network (TTN) pour la création d'un réseau LoRaWAN
---

# Configuration d'une passerelle LoRaWAN The Things Indoor

Ce tutoriel explique comment mettre en œuvre rapidement **un réseau LoRaWAN privé**, constitué d'une passerelle LoRaWAN [The Things Indoor Gateway (TTIG)](https://www.thethingsnetwork.org/docs/gateways/thethingsindoor/) enregistrée sur [The Things Network (TTN)](https://www.thethingsnetwork.org/) avec une interface graphique fournie par les serveurs de [TagoIO](https://tago.io/).<br>
Notre réseau loRaWAN sera précisément celui illustré ci-dessous :

<br>
<div align="left">
<img alt="Réseau LoRaWAN" src="images_ttn_cayenne/lorawan_schematics.jpg" width="500px">
</div>
<br>

Crédit image : [Deltalab](https://deltalabprototype.fr/category/projets/lorawan/)

Comme objet connecté, nous utiliserons notamment une [carte NUCLEO-WB55](../../Kit/nucleo_wb55rg) équipée d'un [un module Grove LoRa-E5 de Seeed studio](https://wiki.seeedstudio.com/Grove_LoRa_E5_New_Version/) et d'un capteur environnemental, en écho et complément à [cet autre tutoriel](../Micropython/grove/lora-e5) qui montre comment utiliser LoRaWAN avec MicroPython. Mais tout ce que nous allons raconter s'appliquerait à n'importe quel autre objet équipé d'une radio LoRa en changeant essentiellement son identifiant unique *DevEui*. Nous vous invitons à lire notre article [LoRa et LoRaWAN](lora), préalable à ce tutoriel.


## The Things Indoor Gateway (TTIG)

La passerelle LoRaWAN que nous avons choisi est la [The Things Indoor Gateway (TTIG)](https://www.thethingsnetwork.org/docs/gateways/thethingsindoor/) qui est à la fois peu onéreuse, compacte et facile à configurer. Particulièrement sobre, **la passerelle TTIG a cet aspect :**

<br>
<div align="left">
<img alt="Passerelle TTIG" src="images_ttn_cayenne/ttig.jpg" width="350px">
</div>
<br>

Crédit image : [The Things Network](https://www.thethingsnetwork.org/)

Elle comporte une LED, un bouton *Reset* et un bouton *Setup*. Elle peut être alimentée directement sur une prise électrique ou bien à l'aide d'un câble USB C. Dépourvue de connecteur RJ45 - Ethernet, elle se connectera à l'Internet via votre réseau Wi-Fi.

## Création du compte TTN et choix du réseau loRaWAN

**Commencez par vous rendre sur [le site de TTN](https://www.thethingsnetwork.org/)** et cliquez sur le bouton *Sign Up* **dans le bandeau en haut complètement à droite** pour créer puis valider votre compte TTN. Il faut choisir l'option *Experiment and explore with The Things Network* sur la page qui s'affiche. Si votre compte est déjà créé, cliquez sur le bouton *Login* juste à côté pour vous y connecter avec vos identifiants, ce qui devrait vous renvoyer sur une invite ressemblant à ceci :

<br>
<div align="left">
<img alt="Invite de login TTN" src="images_ttn_cayenne/login_ttn.jpg" width="300px">
</div>
<br>

Une fois connecté, l'icône de votre profil apparaît toujours **dans le bandeau en haut complètement à droite** avec le nom que vous avez choisi. Développez le menu associé pour le faire apparaitre, il comporte trois entrées : *My Profile*, *Console* et *Log Out*. Cliquez sur *Console* :

<br>
<div align="left">
<img alt="Accès à la console TTN" src="images_ttn_cayenne/console_popup.jpg" width="200px">
</div>
<br>

Sur la page suivante, intitulée **Choose a network cluster** sélectionnez le menu déroulant *Device or gateway location* et précisez votre pays ("France" dans notre cas) puis sélectionnez  votre *Existing cluster*  dans la colonne de gauche ("Europe 1" dans notre cas) :

<br>
<div align="left">
<img alt="Choix du réseau" src="images_ttn_cayenne/choose_a_network_cluster.jpg" width="800px">
</div>
<br>

A l'issue de l'étape ci-dessus, vous devriez vous retrouver sur [la page à cette URL : https://eu1.cloud.thethings.network/console/](https://eu1.cloud.thethings.network/console/) qui ressemble à ceci :

<br>
<div align="left">
<img alt="Page console" src="images_ttn_cayenne/console.jpg" width="800px">
</div>
<br>

## Enregistrement d'une passerelle TTIG sur TTN

Vous pouvez à présent vous rendre sur la page de configuration de votre passerelle, ou *gateway*, en anglais. Cliquez donc sur *Go to gateways*. Une nouvelle page s'affiche avec un bandeau en haut qui propose, entre autres liens [*Applications*](https://eu1.cloud.thethings.network/console/applications) et [*Gateways*](https://eu1.cloud.thethings.network/console/gateways) (c'est là que nous sommes positionnés).

Cliquez sur le bouton *+ Register gateway*. Il vous faut à présent renseigner les paramètres de la passerelle que nous avons sélectionnée, à savoir **The Things Indoor Gateway (TTIG)**. Le tutoriel "officiel" pour cette opération se trouve [ici](https://www.thethingsindustries.com/docs/gateways/models/thethingsindoorgateway/). L'étape la plus délicate est la détermination **de son identifiant Gateway EUI**. Celui-ci s'obtient à partir **de l'étiquette située sur la passerelle**.

Munissez-vous d'une loupe si vous n'avez pas une vue exceptionnelle, car c'est écrit en tout petit, est repérez le code de 12 caractères situé sous le QR Code de l'étiquette ainsi que le mot de passe de connexion au Wi-Fi intitulé *WiFi PW* comportant 8 caractères. Par exemple, pour une passerelle donnée on obtient à partir de l'étiquette :

  - Gateway EUI : 58AADB804EAA
  - **WiFi PWD : XqnoWgYH**

L'étape suivante consiste à **modifier le Gateway EUI de l'étiquette** en insérant les caractères *FFFE* (oui, en dernier, c'est bien un *E* !) après le sixième caractère du code inscrit sur l'étiquette de la passerelle.  Ainsi 58AADB804EAA devient **58AADBFFFE804EAA**. **C'est ce nouveau code qu'il faudra utiliser par la suite comme EUI**.

Vous disposez à présent de toutes les informations pour enregistrer votre passerelle sur TTN. Dans notre cas, cela donne :

<br>
<div align="left">
<img alt="Enregistrer une passerelle, 1" src="images_ttn_cayenne/register_gateway_1.jpg" width="600px">
</div>
<br>

Renseignez ensuite le mot de passe Wi-Fi dans la case *Claim authentication code* ainsi que le *Frequency Plan* *(Europe 863-870 MHz...)* et cliquez sur le bouton *Claim gateway*. Vous devriez obtenir ceci :

<br>
<div align="left">
<img alt="Enregistrer une passerelle, 2" src="images_ttn_cayenne/register_gateway_2.jpg" width="600px">
</div>
<br>

Et voilà, **votre passerelle TTIG est déclarée et configurée sur TTN !**<br>
Il ne vous reste plus qu'à la brancher sur une alimentation USB (ou secteur) et à paramétrer sa connexion à votre réseau Wi-Fi domestique, afin qu'elle ait accès à Internet et qu'elle valide sa disponibilité auprès des serveurs de TTN.

## Connexion de la passerelle TTIG à votre réseau Wi-Fi domestique

Connectons à présent la passerelle TTIG à votre réseau Wi-Fi domestique.
Le tutoriel "officiel" pour cette étape se trouve [ici](https://www.thethingsindustries.com/docs/gateways/models/thethingsindoorgateway/#new-gateways), en voici une traduction :
 - Maintenez le bouton RESET (petit bouton à l'arrière de la passerelle à côté du port USB-C) enfoncé pendant 5 secondes jusqu'à ce que la LED clignote rapidement du VERT au ROUGE deux fois.
 - Maintenez le bouton SETUP (en haut de la passerelle, à côté de la LED) enfoncé pendant 10 secondes jusqu'à ce que la LED clignote rapidement en ROUGE.

 - La passerelle expose maintenant un point d'accès Wi-Fi dont le SSID est MINIHUB-xxxxxx, où xxxxxx sont les 6 derniers chiffres de son EUI. Le mot de passe pour ce réseau est le même mot de passe Wi-Fi que nous avons relevé à l'étape qui précède.

 - Connectez votre ordinateur ou votre smartphone sur ce réseau et rendez-vous à l'adresse 192.168.4.1 en la tapant dans la barre d'un navigateur web pour accéder à la page de configuration Wi-Fi de la passerelle. Vous accèderez à une page html qui liste tous les réseaux Wi-Fi détectés par la passerelle (cachés dans la copie d'écran qui suit) :

<br>
<div align="left">
<img alt="Connecter TTIG au réseau Wi-Fi domestique" src="images_ttn_cayenne/ttig_wifi_setup.jpg" width="550px">
</div>
<br>

 - Sélectionnez le réseau Wi-Fi domestique qui vous convient en cliquant sur le bouton **"+"** à côté de son identifiant (et entrez son mot de passe s'il s'agit d'un réseau sécurisé).<br>
**Important** : La passerelle TTIG utilise un composant ESP32 qui n'acceptera pas de se connecter à un réseau Wi-Fi s'il est sécurisé en mode WPA-Enterprise.

 - Cliquez sur **Save and reboot** pour terminer.

- Si la configuration est correcte,

  - La passerelle clignote en VERT pendant quelques secondes jusqu'à ce qu'elle se connecte au réseau Wi-Fi domestique que vous avez sélectionné.
  - Ensuite, elle passe du VERT au ROUGE et vice versa pendant quelques secondes, le temps de se connecter au serveur et de récupérer la configuration nécessaire.
  - Comptez 5 à 10 minutes pour que la passerelle récupère la nouvelle configuration.

* Si c'est la première fois que votre passerelle est mise sous tension/connectée au Wi-Fi, il se peut qu'elle commence par télécharger un nouveau micrologiciel (ou firmware) plus récent que celui installé dessus en usine. Ceci est indiqué par des clignotements alternés VERT/ROUGE de la LED. Laissez la passerelle sous tension lorsque cela se produit.

Si tout s'est déroulé correctement, vous pouvez revenir sur [cette page](https://eu1.cloud.thethings.network/console/gateways) après quelques minutes et vérifier que les serveurs de TTN ont effectivement établi la communication avec votre passerelle en consultant son statut :

<br>
<div align="left">
<img alt="Passerelle connectée" src="images_ttn_cayenne/connected_gateway_1.jpg" width="900px">
</div>
<br>

Pour en savoir un peu plus, vous pouvez même cliquer dans la colonne *ID* sur l'identifiant *eui-58aadbfffe804eaa* de votre passerelle et constater que le champ *Live data* accuse bien réception de messages en provenance de la passerelle : 

<br>
<div align="left">
<img alt="Passerelle connectée" src="images_ttn_cayenne/connected_gateway_2.jpg" width="900px">
</div>
<br>

## Création d'une application, enregistrement d'un objet

Il est temps à présent de déclarer votre objet connecté à TTN de sorte que toutes les passerelles LoRaWAN qui capteront ses messages (et pas seulement la votre) puissent les relayer sur Internet vers les serveurs de TTN. Pour cela, vous devez commencer par **créer une application**.

Allez sur [la page *Applications* de la console](https://eu1.cloud.thethings.network/console/applications) et cliquez sur *+ Create application*.
Renseignez l'*Application ID* avec un texte de votre choix, en respectant toutefois les contraintes indiquées sur les caractères. En option, renseignez l'*Application Name* et *Description*. Là encore, vous êtes libre de mettre ce qui vous plaît. Cliquez enfin sur *Create Application*. 

<br>
<div align="left">
<img alt="Création d'une application" src="images_ttn_cayenne/create_application.jpg" width="550px">
</div>
<br>

Une fois votre application créée, double-cliquez sur son *ID* pour arriver sur cet écran :

<br>
<div align="left">
<img alt="Application" src="images_ttn_cayenne/application.jpg" width="900px">
</div>
<br>

Cliquez ensuite sur *+ Register end device* en bas à droite, et sélectionnez *Enter end device specifics manually* sur l'écran suivant. Suivez pas à pas les indications (nous ne reproduisons pas ici tous les écrans, ce serait vraiment trop long). Renseignez alors les différents champs qui vous seront demandés comme ceci :

 - **Frequency Plan** : Europe 863-870 (SF9 for RX2 _ Recommended)
 - **LoRaWAN version** : D'après la documentation de votre objet
 - **Regional Parameters Version** : D'après la documentation de votre objet
 - **JoinEUI ou AppEUI (anciennement)** : Ne mettre que des *0* et cliquer sur *Confirm*
 - **DevEUI** : Indiqué sur votre objet ou obtenu par votre objet
 - **AppKey** : Obtenue via TTN en cliquant sur *Generate*

Assurez-vous que l'option *View registered device* est cochée. Finalement, cliquez sur le bouton *Register end device*.

[**Pour un module Grove LoRa-E5**](https://wiki.seeedstudio.com/Grove_LoRa_E5_New_Version/) Les paramètres de votre objet devraient ressembler à ceci :

<br>
<div align="left">
<img alt="Application" src="images_ttn_cayenne/end_device_settings.jpg" width="550px">
</div>
<br>

Son *DevEUI* est obtenu en lui envoyant la commande `AT+ID` (voir [ce tutoriel](../Micropython/grove/lora-e5)) et la valeur de *LoRaWAN version* / *Regional Parameters version* est **V102B : LoRaWAN alliance recommended Class B protocol**. Différentes autres versions possibles sont configurables via la commande `AT+LW=VER,Vxxx`, mais [la documentation des commandes AT](https://files.seeedstudio.com/products/317990687/res/LoRa-E5%20AT%20Command%20Specification_V1.0%20.pdf) suggère de ne pas changer de version sans une bonne raison.

A cette étape, **copiez la valeur de _AppKey_** et mettez là de côté, vous devrez la préciser dans le code embarqué de votre objet (voir [cet exemple](../Micropython/grove/lora-e5)).

## Création d'un lien vers l'intégration TagoIO

Il faut maintenant indiquer à TTN qu'il devra envoyer les données en provenance de vos objets connectés à [**l'intégration (serveur IoT) TagoIO**](https://tago.io/) qui permettra de les visualiser sous forme de courbes sur un joli tableau de bord (on utilise en général le terme anglais "dashboard") dans un navigateur Internet.

Pour ce faire vous devrez ...

 **1**-**Ouvrir un compte chez TagoIO**, en vous inscrivant [ici](https://admin.tago.io/auth/login).

 **2**-**Depuis votre compte TagoIO**, créez une [*authorisation*](https://help.tago.io/portal/en/kb/articles/218-authorization) depuis [cette page](https://admin.tago.io/devices/authorization). Il faut cliquer sur le bouton *Generate* du bandeau en haut, intitulé **"_Service Authorization_ / Create authorization to allow integration with other services"**.  Dans la boîte *Create New Authorization* qui s'affiche alors, saisir dans le champ *Name*, par exemple, *lora-stm32python* (toujours par référence à [ce tutoriel](../Micropython/grove/lora-e5)), puis cliquez sur le bouton *Generate*. Copiez l'autorisation générée (une clef de 137 caractères qui commence par "at") et collez-la dans un fichier texte pour la suite.
 
 **3**-**Retournez dans la console de votre compte TTN** puis ... 
 - Sélectionnez votre application ;
 - Dans le menu vertical de gauche choisir *Integrations* puis *Webhooks* ;
 - Cliquez sur *+ Add Webhook* ;
 - Dans la page *Choose webhook template*  sélectionnez *TagoIO* ;
 - Dans la page *Setup webhook for TagoIO*, allez dans la boîte *Webhook ID* et saisissez y par exemple *lora-stm32python* puis cliquez sur *Create TagoIO webhook* :

<br>
<div align="left">
<img alt="Application" src="images_ttn_cayenne/Setup_webhook_for_TagoIO.jpg" width="450px">
</div>
<br>

- Cliquez sur le Webhook, vous devriez vous retrouver sur une page intitulée *Edit webhook for TagoIO*. En principe la configuration par défaut du Webhook est la bonne, mais par mesure de précaution, vous pouvez la vérifier dans [ce tutoriel](https://help.tago.io/portal/en/community/topic/how-to-integrate-tagoio-with-ttn-v3).

**4**-**Retournez dans la console de votre compte TagoIO** puis ...
 - Choissez *Devices* dans le menu vertical à droite.
 - Dans la colonne *Networks*, cliquez sur le lien *LoRaWAN TTI/TTN v3*.
 - Choissez le connecteur *Custom TTI/TTN*, cliquez dessus. Une page devrait s'afficher qui vous demande entre autres de renseigner *Device Name* (mettez par exemple *lora-stm32python*) et le *Device EUI* (toujours le même !). Cliquez sur le bouton *Create my Device*.

>>**Et voilà, c'est terminé pour la configuration LoRaWAN !**<br>
L'étape suivante consiste à programmer la clef *AppKey* dans le firmware de l'objet connecté afin qu'il puisse se connecter à TTN et y poster ses mesures, qui devront préalablement être formattées afin d'être décodées par un ["Payload parser" de TagoIO](https://help.tago.io/portal/en/kb/articles/147-payload-parser). Des exemples sont disponibles :<br>
**Pour MicroPython** [dans ce tutoriel](../Micropython/grove/lora-e5) avec [le module Grove LoRa-E5](https://wiki.seeedstudio.com/Grove_LoRa_E5_New_Version/) ;<br>
**Pour STM32duino** [dans ce tutoriel](../Stm32duino/exercices/lora-e5) avec [le module Grove LoRa-E5](https://wiki.seeedstudio.com/Grove_LoRa_E5_New_Version/) ;<br>
**Pour STM32duino** [dans ce tutoriel](../Stm32duino/exercices/lora-nucleo-wl55jc1) avec [la carte NUCLEO_WL55JC1](../Kit/nucleo_wl55jc1).

## Pour aller plus loin : Réseaux LoRaWAN privés **locaux**

Notre tutoriel est allé (presque) au plus simple pour mettre en place **un réseau LoRaWAN privé** en s'appuyant sur un network server, un application server (TTN) et une intégration (TagoIO) **en ligne**. Nous avons d'ailleurs sélectionné notre passerelle TTIG, un produit de TTN (évidemment, parfaitement supportée par TTN) dans cette logique simplificatrice.

En fait, il y avait moyen de faire plus simple, en optant pour le [**réseau LoRaWAN opéré**, proposé par l'opérateur Orange (Orange Live Objects) en France](https://www.orange-business.com/fr/reseau-iot). Dans ce cas, nous n'aurions eu à nous préoccuper que de la configuration de notre objet (comme [sur ce tutoriel](../Micropython/grove/lora-e5), en adaptant toutefois le format des trames loRaWAN) le réseau LoRaWAN étant fourni "clef en main". Il y aurait eu bien sûr une contrepartie : **ce service est payant et impose un abonnement**. 

Il est aussi possible de faire nettement plus compliqué et de configurer **un réseau LoRaWAN privé** avec un network server, un application server et une intégration **installés localement** sur des PC, des Raspberry Pi, etc. Les avantages de cette approche sont nombreux : pas d'abonnement à payer, meilleur contrôle de la sécurité de l'infrastructure, possibilité d'augmenter la fréquence d'émission, possibilité de configurer la radio LoRa pour mieux traverser les murs en intérieur, etc. Si vous êtes intéressé par le sujet, nous vous renvoyons aux solutions [chirpstack.io](https://www.chirpstack.io/) et [lorawan-server](https://github.com/gotthardp/lorawan-server).

## Sources et ressources

La rédaction de ce tutoriel s'est inspirée des ressources en ligne ci-dessous ainsi que des conseils de **Acacio Marques, enseignant à l'écoles des Mines de Saint-Etienne (à Gardanne)** et de **Didier Donsez, professeur des Universités en Informatique Université Grenoble Alpes Polytech Grenoble** que je remercie chaleureusement.

Sur LoRa et LoRaWAN en général :
 - [www.univ-smb.fr](https://www.univ-smb.fr/lorawan/livre-gratuit/)
 - [sghoslya.com](http://www.sghoslya.com/p/lora-vs-lorawan.html)
 - [lora.readthedocs.io](https://lora.readthedocs.io/en/latest/)
 - [frugalprototype.com](https://www.frugalprototype.com/technologie-lora-reseau-lorawan/)
 - [linuxembedded.fr](https://www.linuxembedded.fr/2017/12/introduction-a-lora)

Mise en œuvre d'un réseau LoRaWAN privé sur TTN :
 - [The Things Network (TTN)](https://www.thethingsnetwork.org/)
 - [silanus.fr](http://silanus.fr/bts/formationIOT/LoRa/lora.pdf)
 - [genelaix.free.fr](http://genelaix.free.fr/IMG/pdf/11_tp_stm32_iot.pdf)
 - [www.framboise314.fr, gateway LoRa RAK833](https://www.framboise314.fr/connecter-une-gateway-lora-rak833-a-ttn-v3/)
 - [www.framboise314.fr, module Grove LoRa-E5](https://www.framboise314.fr/carte-seeedstudio-lora-grove-e5/)
 - [wikifab.org](https://wikifab.org/wiki/Cr%C3%A9er_un_compte_%22The_Things_Network%22_et_rejoindre_une_communaut%C3%A9)

Mise en œuvre d'un réseau LoRaWAN privé local :
 - [chirpstack.io](https://www.chirpstack.io/)
 - [lorawan-server](https://github.com/gotthardp/lorawan-server)

Sur TagoIO :
 - [Démarrer avec TagoIO](https://help.tago.io/portal/en/kb/articles/1-getting-started)
 - [Configurer TagoIO, tutoriel de TTN](https://www.thethingsnetwork.org/docs/applications/tago/)
 - [Intégration TagoIO avec TTN](https://help.tago.io/portal/en/community/topic/how-to-integrate-tagoio-with-ttn-v3)
 - [Configuration d'un Payload parser avec TagoIO](https://help.tago.io/portal/en/kb/articles/147-payload-parser)
 - [Configuration d'un Payload parser LoRaWAN avec TagoIO](https://help.tago.io/portal/en/community/topic/how-to-build-a-lorawan-sigfox-payload-parser)
  - [Création d'un dashboard TagoIO](https://help.tago.io/portal/en/kb/articles/15-dashboard-overview)

Installer / configurer une passerelle LoRaWAN :
 - [thethingsindustries.com](https://www.thethingsindustries.com/docs/gateways/models/thethingsindoorgateway/)
 - [mobilefish.com](https://www.mobilefish.com/developer/lorawan/lorawan_quickguide_build_lora_gateway.html)
 - [wikifab.org](https://wikifab.org/wiki/D%C3%A9ployer_une_passerelle_LoRaWAN_pour_The_Things_Network/fr)
