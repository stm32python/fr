---
title: L'infrastructure LoRaWAN (couche applicative)
description: Présentation de l'infrastructure LoRaWAN
---

# L'infrastructure LoRaWAN (couche applicative)

L'ensemble *[objets connectés LoRa + passerelles + serveurs réseau + serveurs d'applications]* **constitue l'infrastructure LoRaWAN**. Le serveur d'applications IoT, en revanche, n'en fait pas partie, c'est un élément du réseau Internet. Le schéma qui suit rappelle l'architecture générale d'une infrastructure LoRaWAN plus un serveur IoT :

<br>
<div align="left">
<img alt="Réseau LoRaWAN" src="images_ttn_cayenne/lorawan_schematics.jpg" width="600px">
</div>
<br>

> Source : [Deltalab](https://deltalabprototype.fr/category/projets/lorawan/)

La plupart de temps, les données proviennent des objets et sont à destination des serveurs (*uplink*). Mais l'inverse est possible, les serveurs peuvent également envoyer des messages aux objets (*downlink*).

Quelques précisions :

- Les **objets (connectés) ou nœuds LoRa** sont des dispositifs de petite dimension qui consomment très peu d'énergie et n'envoient que de faibles quantités de données en modulation CSS.
- **Les passerelles LoRaWAN** ont pour fonction de capter les trames émises par les objet et de les démoduler pour les relayer ensuite vers un serveur réseau puis un serveur d'application ([TTN](https://www.thethingsnetwork.org/) joue ces deux rôles dans notre cas).
- **Le serveur réseau** va collecter les trames, éliminer celles en doublons, authentifier celles qui sont retenues. 
- **Le serveur d'application** est chargé de chiffrer/déchiffrer les trames avant de les mettre à disposition de plateformes d'application IoT (dans notre exemple, [TagoIO](https://tago.io/)). 
- **La plateforme IoT** enregistre les payloads des trames dans une base de données et fournit une représentation structurée de celles-ci (en général un tableau de bord graphique, communément désigné par le terme anglo-saxon "dashboard") à toutes les applications clientes (sur PC, MAC, smartphones...).

>> **Ceci conclue notre article sur LoRa et LoRaWAN !**