---
title: L'Adaptative Data Rate de LoRaWAN (couche MAC)
description: Présentation de l'Adaptative Data Rate de LoRaWAN
---

# L'Adaptative Data Rate de LoRaWAN (couche MAC)

Nous abordons à présent la couche MAC de LoRaWAN.<br>
Le **débit de données adaptatif** ou [**Adaptative Data Rate** (ADR)](https://lora-developers.semtech.com/documentation/tech-papers-and-guides/implementing-adaptive-data-rate-adr/implementing-adaptive-data-rate/) est une fonctionnalité du protocole LoRaWAN qui permet de modifier dynamiquement, et d'optimiser, les paramètres des transmissions des nœuds LoRa vers les passerelles (uplink). Les paramètres concernés sont :

* Le facteur d'étalement (SF)
* La bande passante (BW)
* La puissance d'émission (P<sub>E</sub>)

L'ADR est activé par une requête exprimée dans la trame d'uplink d'un nœud. Le serveur réseau est alors autorisé à contrôler et ajuster les paramètres de transmission de ce nœud.

**Le principe (simplifié) de l'ADR est le suivant :** Le serveur réseau commence par examiner les SNR, les débits binaires, ... des 20 uplinks les plus récents en provenance du nœud.  Il utilise ensuite ces informations pour calculer les SF, BW et P<sub>E</sub> qui permettront le meilleur compromis entre débit binaire et consommation d'énergie possible. Ces paramètres optimisés sont ensuite renvoyés au nœud (downlink) qui va se reconfigurer en conséquence.

**Cette optimisation n'est possible que pour des périodes où le nœud reste immobile, ce qui assure que ses paramètres de transmission ne changeront pas**.

