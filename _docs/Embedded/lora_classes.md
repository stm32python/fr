---
title: Les classes d'objets LoRaWAN (couche MAC)
description: Présentation des classes d'objets LoRaWAN
---

# Les classes d'objets LoRaWAN (couche MAC)

De plus en plus de protocoles radiofréquence limitent leur consommation énergétique en **synchronisant les phases d'émission et de réception** entre leurs objets et les passerelles de sorte à désactiver leurs émetteurs-récepteurs la plus grande partie du temps. Cette stratégie est par exemple implémentée [par le protocole BLE depuis sa création](ble), mais aussi par LoRaWAN dans le cadre des **classes d'objets** que nous allons présenter ici.

La  [spécification LoRaWAN 1.0](https://resources.lora-alliance.org/technical-specifications/ts001-1-0-4-lorawan-l2-1-0-4-specification) met à disposition  **trois** classes d'objets, selon les besoins de l'application :

## La classe A(ll)

En classe A, après chaque uplink (émission) vers une (des) passerelle(s), deux courtes fenêtres de downlink (réception) sont ouvertes par le nœud :

<br>
<div align="left">
<img alt="Objet de classe A" src="images_lora/classe_a.jpg" width="900px">
</div>
<br>

Source : [Mobile Fish](https://www.mobilefish.com/download/lora/lora_part4.pdf)

* À tout moment, un nœud terminal peut émettre un signal. Après cette transmission par liaison montante (tx), le nœud attend une réponse de la passerelle.
* Le nœud ouvre deux **créneaux de réception** (on préfère le terme anglais **slot**)  à t<sub>1</sub> = 1 seconde +/ 20 μs et t<sub>2</sub> = 2 seconde après tx. La passerelle peut répondre pendant le premier ou le deuxième slot, mais pas pendant les deux.
* La La fréquence d'émission et le [**débit binaire**](lora_trames) sont configurables mais fixes. Entre les slots le node-end est en sommeil (basse consommation).

**Un nœud LoRa qui est uniquement de classe A ne peut pas recevoir s’il n’a pas émis. Il n’est donc pas joignable facilement.**

La classe A est adaptée aux nœuds alimentés sur batteries qui n'ont pas de  contraintes de latence de réception de messages.

## La classe B(eacon)

Comme pour la classe A, mais le nœud ouvre des fenêtres de réception supplémentaires à des moments planifiés :

<br>
<div align="left">
<img alt="Objet de classe B" src="images_lora/classe_b.jpg" width="900px">
</div>
<br>

Source : [Mobile Fish](https://www.mobilefish.com/download/lora/lora_part4.pdf)

* En plus des deux slots de la classe A, les nœuds de classe B ouvrent des slots  supplémentaires à des moments programmées. 
* Le nœud reçoit un signal de synchronisation (des "balises") de la part de la passerelle. Ces balises permettent à la passerelle de savoir quand exactement le nœud est à l'écoute.

**Un nœud LoRa de classe B est joignable régulièrement sans qu’il soit nécessairement obligé d’émettre.**

La classe B est adaptée aux applications ayant une contrainte de latence de réception de message de quelques secondes et restent économes en énergie ; les nœuds consomment cependant plus qu’en classe A.

## La classe C(ontinuous)

Comme pour la classe A, mais le nœud est en mode réception permanente d'éventuels messages en provenance des passerelles. 

<br>
<div align="left">
<img alt="Objet de classe C" src="images_lora/classe_c.jpg" width="900px">
</div>
<br>

Source : [Mobile Fish](https://www.mobilefish.com/download/lora/lora_part4.pdf)

**Un nœud LoRa de classe C est joignable en permanence.**

La classe C est pertinente pour les applications ayant une contrainte de latence de réception forte (moins d’une seconde) mais pas sur leur autonomie. Ces nœuds sont généralement connectés au réseau électrique compte tenu de leur consommation élevée.

**NB :** La passerelle utilisée pour le downlink est celle qui a reçu le dernier message du nœud LoRa. Un message ne pourra jamais arriver à destination d’un nœud LoRa si celui-ci n’a jamais transmis, quelle que soit sa classe A, B ou C. 


## Ressources

- [Mobile Fish - *LoRaWAN Device Classes*](https://www.mobilefish.com/download/lora/lora_part4.pdf)
- **Cours LoRa de l'Ecole des Mines de Saint-Etienne à Gardanne (Acacio MARQUES et al.)**