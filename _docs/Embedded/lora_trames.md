---
title: Les trames LoRa, le Time On Air et le débit binaire (couche PHY)
description: Présentation des trames LoRa, le Time On Air et le débit binaire
---

# Les trames LoRa, le Time On Air et le débit binaire (couche PHY)

## Structure des trames LoRa

Les trames LoRa sont constituées d'un **préambule**, d'un **en-tête** (ou **header** en anglais, optionnel) et d'une **charge utile** (on préfère utiliser le terme anglais **payload**). Elles peuvent donc se présenter sous deux formats :

1. **Avec un en-tête**, qui précise la taille de la payload (en octets), le **Coding Rate (CR)** et l'utilisation (ou pas) d'un **Code de Redondance Cyclique (CRC) de 16 bits** pour détecter des erreurs de transmission. CR est un paramètre qui indique combien de bits on a ajoutés dans la payload afin de pouvoir la reconstruire dans le cas où elle serait corrompue par des interférences. Evidemment, cette redondance augmente la taille de la trame au détriment de l'énergie nécessaire pour l'émettre.

2. **Sans en-tête** (en-tête implicite). Dans ce cas le CR et l'utilisation (ou pas) d'un CRC sont définis par défaut et identiques sur les nœuds et les passerelles, ce qui permet de supprimer l'en-tête et de réduire les temps de transmission.

Le schéma qui suit illustre la structure générale d'une trame LoRa :

<br>
<div align="left">
<img alt="Structure générale d'une trame LoRa" src="images_lora/trame_lora.jpg" width="700px">
</div>
<br> 

> Source : [Alexandre Boyer, INSA Toulouse](https://www.researchgate.net/figure/LoRa-packet-structure_fig1_316999094)

Et voici un exemple de spectrogramme d'une trame LoRa :

<br>
<div align="left">
<img alt="Spectrogramme d'une trame LoRa" src="images_lora/spectrogramme_trame_lora.jpg" width="700px">
</div>
<br> 

> Source : [LoRa Backscatter: Enabling The Vision of Ubiquitous Connectivity (Vamsi Talla et al.)](https://slideplayer.fr/slide/14250959/)


Quelques précisions :

* **Le préambule** permet au récepteur de détecter le début de la trame. Il comporte 12 symboles : 8 CHIRP montants, 2 symboles modulés qui ont la valeur du mot de synchronisation (par convention 0x34 avec LoRaWAN) et 2 CHIRP descendants ;
* **L'en-tête** explicite ou implicite, déjà décrit ;
* **La payload (plus un éventuel CRC)** dimensionnée selon les informations de l'en-tête.

## Time On Air et débit binaire

Le **Time on Air (ToA)** d'une trame est la durée pendant laquelle la radio doit être active pour moduler et émettre /  recevoir et démoduler une trame [^1]. Il dépend du nombre de CHIRP présents dans la trame et varie entre **quelques dizaines** et **quelques centaines de millisecondes** selon les valeurs de BW, CR, SF, CRC (actif ou pas), la présence ou l'absence d'en-tête.

Le **débit binaire (ou bitrate)** est le nombre de bits **utiles** (les bits réellement utiles sont ceux de la payload) transmis par seconde pendant la communication d'une trame. Il est directement lié au ToA et dépend des mêmes paramètres. Son calcul n'est pas très ludique, vous trouverez une discussion à ce sujet dans [cette référence](ttps://fr.wikipedia.org/wiki/LoRaWAN). Par exemple, pour une bande passante BW = 125 kHz et en introduisant dans les trames LoRa approximativement 20% de bits destinés à la correction d'erreurs (CR = 4/5) :

>> * Pour SF = 7, CR = 4/5, Débit binaire = 5.5 kbits/s
>> * Pour SF = 12, CR = 4/5, Débit binaire = 0.29 kbits/s

> Source : [Mobile Fish](https://www.mobilefish.com/download/lora/lora_part15.pdf)

La relation entre le ToA et le bitrate est illustrée par le schéma qui suit :

<br>
<div align="left">
<img alt="Portée et bitrates pour différents facteurs d'étalement" src="images_lora/range_bitrate_sf.jpg" width="700px">
</div>
<br> 

> Source : [Research Gate](https://www.researchgate.net/figure/LoRa-Spreading-Factor-SF-Bitrates-and-Time-on-Air-LoRa-is-chosen-as-the-wireless_fig7_324043563)


Pour un calcul complet du débit binaire, il faut aussi intégrer les [**contraintes supplémentaires imposées par la régulation ISM concernant les puissances d'émission et l'utilisation de la bande passante**](lora_ism). En Europe, la puissance de l'émetteur ne peut pas dépasser 25 mW et **il faut respecter un temps d'occupation maximum du canal radio (ou duty-cycle en anglais) de 1 % sur la bande 868 MHz**. En conséquence, le débit binaire doit encore être revu à la baisse : 

>> * Pour SF = 7, CR = 4/5, Débit binaire = 1.73 bits/s
>> * Pour SF = 12, CR = 4/5, Débit binaire = 6.9 bits/s

> Source : [Ouvrage de Sylvain Montagny](https://www.univ-smb.fr/lorawan/livre-gratuit/)

Des restrictions supplémentaires sur l'utilisation de la bande passante peuvent encore être dictées par **l'opérateur de réseau**. Par exemple vous trouverez celles de TTN [ici](https://www.thethingsnetwork.org/docs/lorawan/duty-cycle.html).

## Notes au fil du texte

[^1]: Il ne s'agit *pas* du temps pour qu'une trame voyage de son émetteur jusqu'à d'éventuels récepteurs ! C'est logique, puisque le délai entre émission et réception d'une trame dépend des positions respectives des nœuds et des passerelles, imprévisibles et potentiellement variables. Le terme "time on air" que l'on est tenté de traduire par "temps de vol" est donc trompeur et particulièrement mal choisi ...

