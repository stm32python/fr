---
title: La modulation LoRa par étalement de spectre (couche PHY)
description: Présentation du principe de la modulation LoRa par étalement de spectre
---

# La modulation LoRa par étalement de spectre (couche PHY)

## Généralités

Comme déjà évoqué, LoRa est une technologie de communication radiofréquence. Commençons par quelques généralités sur son fonctionnement :

* Chaque objet (ou nœud) LoRa dispose d'un module radio qui encode l'information sur un signal porteur à l'aide de son [**modulateur**](https://fr.wikipedia.org/wiki/Modulateur_RF).
* Ce signal modulé est transmis et reçu par une (ou plusieurs) passerelle(s).
* Chaque passerelle dispose également d'un module radio qui utilisera son [**démodulateur**](https://fr.wikipedia.org/wiki/D%C3%A9modulateur) pour extraire l'information du signal modulé.
* La communication entre les nœuds et les passerelles est bidirectionnelle (uplink et downlink). Ce sont donc des [émetteur-récepteurs](https://fr.wikipedia.org/wiki/%C3%89metteur-r%C3%A9cepteur) (on dit parfois **transcepteurs**, mais c'est le terme anglo-saxon [**transceiver**](https://en.wikipedia.org/wiki/Transceiver) qui est utilisé en pratique).


<br>
<div align="left">
<img alt="Modulateur et démodulateur" src="images_lora/modem.jpg" width="700px">
</div>
<br>

> Sources : [STMicroelectronics et Mobile Fish](https://www.mobilefish.com/download/lora/lora_part12.pdf)

## Les CHIRP

La principale particularité de LoRa est **sa modulation par étalement de spectre (ou de fréquence)** de type **Chirp Spread Spectrum (CSS)**[^1]. Les messages sont composés de symboles binaires qui sont eux mêmes encodés sous forme de signaux radio dont la fréquence varie en fonction du temps [^2]. 
D
Plus précisément, les symboles LoRa sont encodés sur des **CHIRP**, acronyme pour ***C**ompressed **HI**gh density **R**adar **P**ulse* (soit "Impulsions radar comprimées de forte densité" en français [^3]) ; ils peuvent être représentés par leur amplitude en fonction du temps ou par leur fréquence en fonction du temps :

|CHIRP montant, représentation amplitude = f(t)| |CHIRP montant, représentation fréquence = f(t)|
|:-:|:-:|:-:|
|<img alt="HIRP montant, représentation amplitude = f(t)" src="images_lora/up_chirp_amp.jpg" width="410px">| |<img alt="CHIRP montant, représentation fréquence = f(t)" src="images_lora/up_chirp_freq.jpg" width="310px">|

> Source : [Mobile Fish](https://www.mobilefish.com/download/lora/lora_part12.pdf)

La fréquence centrale (porteuse) F<sub>P</sub> du CHIRP est celle de son **canal**, la **bande passante (désignée par BW pour *bandwidth* en anglais)** est la largeur de ce canal, en général égale à F<sub>max</sub> – F<sub>min</sub> = 125 kHz [^4] comme déjà expliqué [ici](lora_ism). Du fait que BW est grande, on dit que LoRa est une technologie de type **Ultra-Wide Band** (UWB, émission en bande de fréquence ultra-large)[^5].

## La modulation CSS

La **porteuse** du signal LoRa est donc constituée d'un train de CHIRP et la modulation CSS consiste à lui appliquer des sauts de fréquence pour encoder un message. Voici à quoi ressemble un spectrogramme LoRa avant et après modulation CSS :

<br>
<div align="left">
<img alt="Modulateur et démodulateur" src="images_lora/css_modulation.jpg" width="900px">
</div>
<br> 

> Source : [Mobile Fish](https://www.mobilefish.com/download/lora/lora_part13.pdf)

**Comment chaque symbole est-il encodé sur son CHIRP ?**<br>
Supposons que l'on souhaite encoder un nombre représenté par **SF = 2 bits**.<br>
Par anticipation sur la suite, précisons que SF est ce que l'on appelle le **Spreading Factor (SF)**, ou "facteur d'étalement" en français, de la modulation.<br>
La solution adoptée par CSS consiste à altérer le profil de chaque CHIRP en introduisant SF-1 sauts dans sa rampe de fréquence, puis à appliquer des décalages circulaires. Pour notre exemple SF = 2, on obtient le jeu de 2<sup>SF</sup> = 4 symboles suivant : 

<br>
<div align="left">
<img alt="Symmboles LoRa pour SF = 2" src="images_lora/css_symb_sf2.jpg" width="900px">
</div>
<br> 

> Source : [Wireless Pi](https://wirelesspi.com/understanding-lora-phy-long-range-physical-layer/)

On remarque que le premier symbole, qui encode la séquence de bits "00", est un CHIRP (non modulé). Son seul point commun avec les autres symboles de la famille SF = 2 **est la pente de sa rampe de fréquence**, identique.

Par ailleurs, **le choix d'une valeur de SF impose non seulement le nombre de sauts de fréquence mais aussi la pente de la rampe de fréquence de chaque CHIRP**. Plus SF est élevé, plus la montée en fréquence est lente, et plus les CHIRP durent longtemps, ce qui permet de les découper en un nombre SF plus élevé de segments (on dit "chips") et d'encoder plus de bits **sans compromettre la qualité du signal**. La figure ci-dessous montre le profil de 5 CHIRP pour SF variant de 7 à 11 :

<br>
<div align="left">
<img alt="Comparaison des profils de CHIRP pour SF variant de 7 à 11" src="images_lora/css_chirp_sf_compare.jpg" width="900px">
</div>
<br> 

> Source : [Mobile Fish](https://www.mobilefish.com/download/lora/lora_part15.pdf)

Ce schéma permet de comprendre pourquoi, à **bande passante F<sub>max</sub>- F<sub>min</sub> constante, chaque fois que SF augmente d'une unité** :
- Le temps d'émission d'un CHIRP est multiplié par deux donc, nécessairement, le flux d'information diminue.
- Simultanément, puisque l'on module 2<sup>SF</sup> symboles par CHIRP, le nombre de symboles possibles double. 

Pour augmenter la portée du signal et améliorer sa résistance au bruit, on devine qu'il est opportun de faire durer les CHIRP aussi longtemps que possible en prenant des valeurs de SF élevées, ce qui est équivalent à leur donner plus d'énergie. L'augmentation du Spreading Factor (jusqu'à SF12) permet de couvrir une distance plus grande entre le nœud et les passerelles au détriment de la bande passante disponible et du flux de données (Bitrate), mais aussi de la consommation du nœud (Time on Air), ces points sont précisés [dans la section suivante](lora_trames) de notre article.

## Tolérance aux interférences

L'étalement linéaire en fréquence des CHIRP permet aux récepteurs d’éliminer aisément les décalages de fréquences ([**effets Doppler**](https://fr.wikipedia.org/wiki/Effet_Doppler)) provenant du mouvement relatif émetteurs-récepteur. Mais le principal avantage de CSS est **d'offrir un signal qui tolère les interférences à bandes de fréquences simultanément étroite et large**.

* Concernant **les interférences à bande étroite**, il est très peu probable qu'une source radio "parasite" émette simultanément dans toute la gamme de fréquences des CHIRP. La figure qui suit illustre cette propriété :

    <br>
    <div align="left">
    <img alt="Stratégie Ultra-Wide Band" src="images_lora/uwb.jpg" width="350px">
    </div>
    <br> 

    > Source : [LoRa developpers](https://lora-developers.semtech.com/documentation/tech-papers-and-guides/lora-and-lorawan/)


  De ce fait, des valeurs élevées du facteur d'étalement permettent  **une communication plus résistante au bruit** et augmentent **la sensibilité de réception**.<br>
  Le tableau ci-dessous indique **le rapport signal sur bruit SNR** (expliqué [ici](lora_rf)) minimum requis pour la démodulation et les sensibilités pour différents facteurs d'étalement pour BW = 125 kHz et un bruit provenant de l'électronique du récepteur NF = 6 dB (NF est appelé le "noise figure") :


    |Facteur d'étalement (SF)|SNR minimum pour démodulation|Sensibilité réception|
    |:-:|:-:|:-:|
    |*7*|*-7.5 dB*|*-125 dBm*|
    |*8*|*-10 dB*|*-127 dBm*|
    |*9*|*-12.5 dB*|*-130 dBm*| 
    |*10*|*-15 dB*|*-132 dBm*|
    |*11*|*-17.5 dB*|*-135 dBm*|
    |*12*|*-20 dB*|*-137 dBm*|

> Source : [Mobile Fish](https://www.mobilefish.com/download/lora/lora_part16.pdf)

<br>

* Concernant **les interférences à large bande**, la démodulation CSS va les atténuer en utilisant un **filtrage de corrélation**[^6]). 

* Pour finir, signalons que LoRa utilise une "astuce" supplémentaire pour que ses communications soient plus résistantes aux interférences : 8 (respectivement 9) canaux sont possibles (voir [cette section](lora_ism)) pour émettre (respectivement recevoir) les symboles. Les nœuds LoRa changent de canal de manière pseudo-aléatoire pour chaque transmission. Pour plus de détails sur ce sujet, vous pouvez consulter [cette référence](https://www.mobilefish.com/download/lora/lora_part11.pdf).

## Notes au fil du texte

[^1]: [Semtech](https://www.semtech.fr) développe également une extension de la couche physique LoRa pour introduire une nouvelle [**modulation**](https://fr.wikipedia.org/wiki/Modulation_du_signal), le [LR FHSS](https://news.rakwireless.com/lora-css-vs-lora-fhss/), notamment pour les besoins de la communication avec les (nano)satellites en orbites basses.

[^2]: Nous n'expliquerons pas comment la modulation et la démodulation CSS sont réalisées. Si ce sujet assez technique vous intéresse vous pouvez consulter [cette source](https://www.univ-smb.fr/lorawan/livre-gratuit/) et [celle-ci](https://wirelesspi.com/understanding-lora-phy-long-range-physical-layer/). Par ailleurs, du fait que les détails de la démodulation CSS restent la propriété intellectuelle non publiée de Semtech, [on ne peut que spéculer à leur sujet](https://eduscol.education.fr/sti/sites/eduscol.education.fr.sti/files/ressources/pedagogiques/10864/10864-caracterisation-interface-radio-lor-vf.pdf). 

[^3]: Le verbe "to chirp" se traduit de l'anglais par "gazouiller". Comme quoi, on peut faire de la technologie et avoir de l'humour ...

[^4]: Ces figures représentent un CHIRP "montant" dont la fréquence **augmente** linéairement au cours du temps entre F<sub>min</sub> et F<sub>max</sub>. LoRa utilise également des CHIRP "descendants", dont la fréquence **diminue** linéairement de F<sub>max</sub> à F<sub>min</sub>.

 [^5]: A l'opposé, [la modulation D-BPSK (Differential Binary Phase Shift Keying) de Sigfox](https://linuxembedded.fr/2020/03/introduction-a-sigfox) utilise des bandes de fréquences très étroites (100Hz < BW < 600 Hz). Elle consiste à émettre le message avec des séquences d'impulsions plus puissantes que le bruit de fond. On dit que Sigfox est une technologie de type **Ultra-Narrow band** (UNB, émission en bande de fréquence très étroite).

[^6]: Le filtrage de corrélation repose sur l'idée de mesurer la similarité (précisément **la corrélation** au sens mathématique) entre deux signaux ou séries de données afin de repérer des motifs, des structures ou des caractéristiques communes même en présence de bruit. Dans le cas de LoRaWAN, on effectue la corrélation entre le signal reçu et des modèles de chirps modulés. Le filtrage de corrélation est également capable de gérer des variations de fréquence dues à l'effet Doppler, qui peuvent se produire lorsque le récepteur et l'émetteur sont en mouvement relatif.