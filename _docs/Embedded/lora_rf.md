---
title: Notions sur la transmission radiofréquence
description: Quelques notions sur la transmission radiofréquence en général, appliquées à LoRa en particulier
---

# Notions sur la transmission radiofréquence

LoRa/LoRaWAN est avant tout une solution de [communication radiofréquence (RF)](https://fr.wikipedia.org/wiki/Propagation_des_ondes_radio), au même titre que Bluetooth, NFC, BLE, Sigfox, Wi-Fi ... ce qui signifie que les messages (ou trames) sont portés par des [ondes radio](https://fr.wikipedia.org/wiki/Onde_radio) qui voyagent entre un émetteur et un récepteur. Pour s'assurer que ce trajet se déroule dans des conditions optimales et que la trame sera lisible par les récepteurs, il faut intégrer de nombreuses contraintes théoriques et techniques lors de la conception des [émetteur-récepteurs](https://fr.wikipedia.org/wiki/%C3%89metteur-r%C3%A9cepteur), de leurs [antennes](https://fr.wikipedia.org/wiki/Antenne_radio%C3%A9lectrique), des stratégies de [modulation - démodulation](https://fr.wikipedia.org/wiki/Modulation_du_signal), etc.

Toutes ces problématiques sont un champ d'expertise à part entière, bien trop vaste pour cet article. Cependant, certaines unités et indicateurs qui permettent de caractériser la qualité d'une liaison RF Sont indispensables pour la suite de notre exposé. Nous les passons donc en revue dans cette section.

## Le bilan de liaison

Intéressons-nous à **la puissance** de notre signal radio loRa. Les causes de ses variations au cours de son trajet sont résumées par le schéma de son [**bilan de liaison**](https://fr.wikipedia.org/wiki/Bilan_de_liaison) :

<br>
<div align="left">
<img alt="Bilan de liaison" src="images_lora/lora_bilan_liaison.jpg" width="700px">
</div>
<br>

> Source : [Mobile Fish](https://www.mobilefish.com/download/lora/lora_part16.pdf)

De gauche à droite, étape par étape :
1. **Le signal est émis par le transmetteur/modulateur du nœud LoRa** avec une certaine puissance.
2. **Le signal se propage dans un câble jusqu'à l'antenne du nœud LoRa**. Une partie de son énergie est perdue (par effet joule et rayonnement), il est [atténué par sa ligne de transmission](https://fr.wikipedia.org/wiki/Att%C3%A9nuation).
3. **Le signal est amplifié par l'antenne du nœud LoRa**, qui augmente sa puissance.
4. **Le signal se propage ensuite de l'antenne du nœud jusqu'à l'antenne de la passerelle**, sous forme d'ondes radio. Il est [fortement atténué pendant ce trajet](https://fr.wikipedia.org/wiki/Affaiblissement_de_propagation), essentiellement par des obstacles (édifices, montagnes, précipitations et autres) bloquants, dispersants, réfléchissants ou réfractants.
5. **Le signal est amplifié par l'antenne de la passerelle LoRa**.
6. **Le signal se propage dans un câble jusqu'au récepteur de la passerelle LoRa**, il perd de la puissance comme à l'étape 2.
7. **Si la puissance reçue par la passerelle est finalement suffisamment élevée par comparaison avec la puissance du bruit reçu**, le signal pourra être démodulé. Autrement, la passerelle demandera au nœud de réémettre sa trame. 

## Le décibel-milliwatt (dBm)

Lorsqu'on s'intéresse à la transmission des signaux RF on exprime le plus souvent les puissances sous formes de rapports en [**décibels-milliwats (dBm)**](https://fr.wikipedia.org/wiki/DBm), comme sur le graphique qui précède.
Le décibel-milliwatt (dBm) est une unité **relative** et **logarithmique**, elle exprime les puissances par rapport à 1 mW :

>> *Puissance (dBm) = 10 log<sub>10</sub> ( Puissance (Watt) / 0.001 )*

Quelques valeurs calculées avec cette définition :

|Rapport de puissance en dBm|Puissance en mW|
|:-:|:-:|
|*+10 dBm*|*10 mW*|
|*+3 dBm*|*&asymp; 2 mW*| 
|*+0 dBm*|*1 mW*|
|*-3 dBm*|*&asymp; 0.5 mW*|
|*-10 dBm*|*0.1 mW*|

> Source : [Ouvrage de Sylvain Montagny](https://www.univ-smb.fr/lorawan/livre-gratuit/)

Travailler avec des unités relatives et logarithmiques présente deux avantages :
- Lorsqu'une mesure physique donne un nombre très grand ou très petit, son logarithme est plus lisible car il s'exprime avec moins de chiffres.<br>Par exemple, *log<sub>10</sub> (0,000 001) = -6* ou encore *log<sub>10</sub> (5 352 452 323) &asymp; -9,73*.
- Elles simplifient les calculs (par exemple celui du [**bilan de liaison**](https://fr.wikipedia.org/wiki/Bilan_de_liaison)) **en transformant les multiplication en additions**.

Des exemples de calculs avec les dBm sont donnés dans [cette référence](https://www.univ-smb.fr/lorawan/livre-gratuit/) et dans [celle-ci](https://www.mobilefish.com/download/lora/lora_part5.pdf).

## Le Received Signal Strength Indication (RSSI)

L'indication de puissance du signal reçu, ou [**Received Signal Strength Indication (RSSI)**](https://fr.wikipedia.org/wiki/Received_Signal_Strength_Indicator), est la puissance du signal reçu mesurée en dBm. Plus elle sera élevée, plus le signal sera "audible" par le récepteur. Le RSSI a toujours une valeur négative, et ne peut pas être meilleur que 0 dBm. Il doit être comparé avec [**la sensibilité du récepteur**](https://en.wikipedia.org/wiki/Sensitivity_(electronics)) (voir plus loin) ; s'il lui est inférieur, alors le signal est indétectable. 

## Le Signal-to-Noise Ratio (SNR)

Le rapport signal sur bruit ou [**Signal-to-Noise Ratio (SNR ou S/N)**](https://fr.wikipedia.org/wiki/Rapport_signal_sur_bruit) est le rapport des puissances entre la partie du signal qui représente une information et tout ce qui est reçu en plus, qui constitue [**le bruit de fond**](https://fr.wikipedia.org/wiki/Bruit_de_fond). Ce bruit indésirable a différentes origines, il peut provenir des émetteurs récepteurs tout comme de perturbations électromagnétiques extérieures.
Il s'exprime en [**décibels (dB)**](https://fr.wikipedia.org/wiki/D%C3%A9cibel)[^1] et est calculé à partir de la puissance reçue et de la puissance du bruit comme suit :

>> *SNR (dB) = RSSI (dBm) - P<sub>bruit</sub> (dBm)*

Un SNR positif signifie que la puissance "utile" du signal reçu est supérieure à celle du bruit (figure de gauche, ci-dessous) et que le récepteur pourra le démoduler. Dans le cas contraire le signal est noyé dans le bruit et ne peut en principe pas être démodulé (figure de droite ci-dessous) :

<br>
<div align="left">
<img alt="Rapport signal sur bruit" src="images_lora/lora_snr.jpg" width="700px">
</div>
<br>

> Source : [Mobile Fish](https://www.mobilefish.com/download/lora/lora_part10.pdf)

Mais la modulation CSS à large bande permet à LoRa (en vertu du [théorème de Shannon-Hartley](https://fr.wikipedia.org/wiki/Th%C3%A9or%C3%A8me_de_Shannon-Hartley)) **de démoduler également des signaux qui sont au-dessous du bruit de fond** pour peu que celui-ci ne soit pas trop puissant.

## Sensibilité d'un récepteur

La sensibilité d'un récepteur est par définition le niveau minimal (en µV, dBµV ou dBm) que doit présenter le signal appliqué à l'entrée du récepteur pour pouvoir être démodulé et écouté en sortie, compte tenu du rapport signal sur bruit.

## Notes au fil du texte

[^1]: De la même façon que le décibel-milliwatt (dBm), le décibel (dB) est une unité relative définie comme dix fois le logarithme décimal du rapport entre deux puissances.
