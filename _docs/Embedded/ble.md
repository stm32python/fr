---
title: Présentation du BLE
description: Le BLE et ses protocoles
---

# Présentation du Bluetooh Low Energy (BLE)

Cette section est essentiellement une traduction, avec quelques adaptations, des [présentations du BLE par la société Adafruit](https://learn.adafruit.com/introduction-to-bluetooth-low-energy) et par [Digikey](https://www.digikey.fr/fr/articles/comparing-low-power-wireless-technologies). Pour une explication technique plus complète, nous conseillons le livre _Getting Started with Bluetooth Low Energy_ de Kevin Townsend, Carles Cufi Akiba et Robert Davidson, éditions O′Reilly (13 mai 2014), ISBN-10 : 1491949511.

Les termes anglo-saxons (advertising, peripheral, central ...) sont volontairement conservés car leur traduction systématique rendrait les explications techniques inutilement confuses. 

## Origines et objectifs du BLE

Le Bluetooth Low Energy ou « Bluetooth Smart » a vu le jour en 2006 sous le nom de projet Wibree, réalisé dans le Centre de recherche de Nokia. La technologie a été adoptée par le Bluetooth SIG (Special Interest Group), qui l'a présentée comme une forme de Bluetooth à consommation énergétique ultrabasse lors de l'introduction de sa version 4.0 en 2010.

La technologie fonctionne dans [la bande ISM (**I**ndustrielle, **S**cientifique et **M**édicale)](https://fr.wikipedia.org/wiki/Bande_industrielle,_scientifique_et_m%C3%A9dicale) de 2,4 GHz et est adaptée pour la transmission de données à partir de capteurs sans fil compacts ou d'autres périphériques prenant en charge une communication entièrement asynchrone. Ces dispositifs transmettent de faibles volumes de données (c'est-à-dire quelques octets) par intermittence. Leur rapport cyclique s'étend de quelques fois par seconde à une fois par minute, ou plus.

Le BLE permet des économies d'énergie en optimisant le temps de veille et en utilisant des connexions rapides et une faible puissance d'émission/réception de crête. Sa faible consommation énergétique réside principalement dans le fait que, contrairement au Bluetooth classique qui est une radio à intervalles de connexion fixes, le BLE présente généralement un état « déconnecté » écoénergétique, dans lequel chacune des deux extrémités d'une liaison est consciente de l'autre, mais ne se connecte qu'en cas de nécessité, et ce aussi brièvement que possible. En pratique la consommation du BLE est **entre deux fois et dix fois plus faible que celle du Bluetooth** lorsqu'on compare des fonctionnalités qui peuvent être implémentées avec ces deux protocoles. 

Pour résumer, BLE répond au cahier des charges suivant :
- Fonctionne avec la bande ISM 2.4 GHz 
- Faible complexité donc **faible coût**
- Faible bande passante (débit brut maximum inférieur à 2 Mbps en version 5.x)
- Faible consommation (courants de quelques microampères, alimentation sur pile bouton possible)
- Faible portée maximum (de 20 à 50 mètres selon l'environnement)[^1].

Le tableau ci-dessous rappelle les caractéristiques comparées des protocoles BLE, Wi-Fi et Zigbee :

||**BLE**|**Wi-Fi**|**Zigbee**|
|:-|:-:|:-:|:-:|
|**Bande de fréquences**|2.4GHz|2.4GHz/5GHz|2.4GHz|
|**Modulation**|GFSK, Coded|OFDM, DSSS|DSSS|
|**Topologie de réseau**|Point-à point, étoile, maillage|Etoile|Etoile, maillage|
|**Débit**|1Mbps, 2Mbps (Bluetooth 5.0)|> 150Mbps|250 kbps|
|**Pic de consommation**|~5.5mA|RX 60mA, TX 200 mA|RX 19mA, TX 35 mA|
|**Courant de repos**|< 2µA|< 100µA|5 µA|

> **Source**: [Argnenox](https://www.argenox.com/library/bluetooth-low-energy/introduction-to-bluetooth-low-energy-v4-0/)

BLE est clairement le champion de la basse consommation !

Le BLE ne couvre pas les mêmes usages que le Bluetooth (et à fortiori encore moins ceux du Wi-Fi !), mais il y a souvent confusion entre les deux technologies du fait que les composants de nos smartphones sont généralement **dual mode** et contiennent à la fois une radio BLE et une radio Bluetooth en permettant une interopérabilité entre les deux protocoles.<br>
Le [SoC STM32WB55](../Kit/nucleo_wb55rg) qui anime la NUCLEO-WB55 est destiné aux objets connectés et implémente de ce fait les normes Bluetooth LE 5.2 et IEEE 802.15.4 mais **il n'est pas dual mode**. Il ne peut donc pas "discuter" avec des modules Bluetooth standard (comme par exemple les HC-05 et HC-06). 

## La pile protocolaire de BLE

La figure suivante donne une vue d'ensemble de la **pile protocolaire BLE** :

<div align="left">
<img alt="Pile protocolaire du BLE" src="images_ble/ble_stack.jpg" width="300px">
</div>
<br>

> **Source** : [Arup Barua et al.](https://ieeexplore.ieee.org/stamp/stamp.jsp?arnumber=9706334)

**1 - La couche de contrôle** est **la radio** qui module et démodule les signaux analogiques. Elle rassemble :

 * **La couche physique** ou **PHY**, que nous détaillerons plus loin ;
 
 * **La couche de liaison** qui est la toute première couche logicielle du protocole.   
 
   - Elle réalise le contrôle d'intégrité des paquets. 
 
   - Elle crée le lien radio entre les objets et définit deux rôles : **maître** et **esclave**. Le maître initie la connexion et l'esclave se comporte en **"annonceur" ou "advertiser"**. Un maître peut être connecté à plusieurs esclaves. Les esclaves sont généralement des appareils moins puissants, ils dorment par défaut et se réveillent périodiquement pour recevoir des paquets du maître.

   - Elle gère les différents états : *standby* (l'objet ne fait rien), *advertising* (l'objet se comporte en annonceur), *scanning* (l'objet scanne les trames d'advertising), *initiating* (après sélection d'un advertiser, pour s'y connecter) et *connection* (transmission de données entre deux objets connectés). 
 
**2 - L'interface hôte - contrôleur (HCI)** standardise et gère les communications bidirectionnelles entre l'hôte et le contrôleur (la radio). Elle permet d'associer des hôtes et des contrôleurs de fabricants différents [^2].

**3 - La couche hôte** permet aux applications de scanner, de découvrir, de se connecter et d'échanger des informations avec des dispositifs BLE homologues de manière standard et interopérable. Elle contient :

* **La couche L2CAP**, pour "Logical Link Control and Adaptation Protocol" dont le rôle est de traduire les différents messages qui arrivent des couches supérieures en paquets au format BLE. Elle est également chargée de segmenter ou fusionner les paquets selon leur longueur pour optimiser les communications ;

* **La couche SM**, pour "Security Manager" qui gère les problématiques cryptographiques (chiffrage AES, échange sécurisé des clefs, sûreté du mécanisme appairage ...) ;

* **La couche AP**, pour "Attribute Protocol", également désignée par "**ATT**(ributes) définit les **attributs**, leurs identifiants uniques **UUID** codés sur 16 bits et leurs type (valeurs en lecture et/ou écriture) et les permissions pour les clients ;

* **La couche GATT** pour "Generic Attribute Protocol" repose sur la couche ATT. Elle Rassemble les attributs en **services** et définit certaines bibliothèques BLE. GATT gère les échanges entre les objets : le client adresse ses requêtes au serveur GATT et peut lire/écrire des données dans le serveur ;

* **La couche GAP** pour "Generic Access Profile" est chargée de rendre l'objet visible. Elle gère la découverte de l'objet et l'établissement des connexions, mais aussi certains aspects de sécurité (par exemple, est-ce que l'objet peut se connecter ou simplement se comporter en annonceur / advertiser ?).

**4 - La couche applicative** correspond à l'application "utilisateur" sur le smartphone, le PC, l'objet connecté, etc.  

Nous allons à présent explorer plus en détail les couches et/ou protocoles PHY, GAP et GATT.

## Compléments sur la couche physique

Le BLE a introduit une toute nouvelle radio inspirée de celle du Bluetooth qui fonctionne dans la bande s'étendant de 2400MHz à 2483.5MHz, en la découpant en **40 canaux de 1 Mhz de large, espacés de 2 MHz** :

<div align="left">
<img alt="Spectre du BLE" src="images_ble/bluetooth_ble_spectrum.png" width="500px">
</div>

> **Source** : [Argenox](https://www.argenox.com/library/bluetooth-low-energy/introduction-to-bluetooth-low-energy-v4-0/)

Pendant les transmissions, BLE effectue constamment des **sauts entre ces 37 différents canaux** (Adaptative Frequency Hopping - AFH), également pour réduire les interférences.

Trois de ces canaux sont alloués à **l'advertising** et sont utilisés pour les objets qui se comportent comme **des balises** (voir le protocole GAP plus ci-après). Ils sont placés sur les bords et au milieu de la bande pour réduire les risques d'interférences avec le Wi-Fi. Par exemple si le canal 38 et ses voisins sont perturbés par une émission Wi-Fi dans une bande large de 40MHz, il restera deux autres canaux d'advertising non bruités : le 37 et le 39.

La modulation BLE est de type **GFSK** pour "Gaussian Frequency Shift Keying", en français ["Modulation gaussienne par déplacement de fréquence"](https://fr.wikipedia.org/wiki/Modulation_par_d%C3%A9placement_de_fr%C3%A9quence). Ceci signifie que les bits 0 et 1 sont encodés par des modulations de fréquence (de +/- 185 kHz) autour de la fréquence centrale du canal actif. Le principe de la modulation par déplacement de fréquence est rappelé par la figure suivante :

<div align="left">
<img alt="FSK" src="images_ble/Frequency-shift_keying_fr.png" width="400px">
</div>

> **Source**: [Wikipédia](https://fr.wikipedia.org/wiki/Modulation_par_d%C3%A9placement_de_fr%C3%A9quence)

Dans notre cas, cependant, il s'agit d'une modulation **gaussienne** par déplacement de fréquence. Ceci signifie qu'un **filtre gaussien** est utilisé pour lisser les transitions de fréquence lors du processus de codage, afin de ne pas introduire des artefacts HF.

La norme Bluetooth 5.0 a également introduit la modulation **codée** à 1Mbps, qui intègre des **bits de redondance** pour améliorer la résistance du signal au bruit.

## Compléments sur le Generic Access Profile (GAP)

_Ce protocole permet aux objets BLE de signaler leur présence ("advertising" ou "notifications" en français) et réglemente comment deux d'entre eux pourront interagir et éventuellement se connecter l'un à l'autre._

GAP attribue des **rôles** aux objets :

- **Peripheral (esclave)** : Objet disposant de peu de ressources (ex. capteur connecté) qui renverra des informations à un central. 
- **Central (maître)** : Objet disposant de ressources plus étendues (ex: smartphone) auquel se connectent les périphériques.
- **Advertiser/broadcaster** : L’objet ne fait qu’émettre des données (31 octets de données utiles au maximum) sans se connecter à un autre objet.
- **Scanner/observer** : L’objet ne fait que recevoir des données, sans se connecter à un autre objet. Il est à l'écoute des messages d'éventuels objets qui tiendraient un rôle d'advertiser.

Les objets en mode advertiser peuvent donc partager une information avec tous les objets en mode scanner. Le graphique suivant montre un 
scanner/observer entouré d'advertisers/broadcasters :

<br>
<div align="left">
<img alt="BLE GAP, network topology" src="images_ble/BLE_1.jpg" width="600px">
</div>
<br>

L'objet en mode scanner reste *passif*, il écoute simplement les émissions à intervalle régulier (*advertising interval*) de l'objet en mode advertiser. Ces émissions, les *advertising data*, contiennent jusqu'à 31 octets de données utiles. Le graphique suivant illustre ce processus : 

<br>
<div align="left">
<img alt="BLE GAP, advertising" src="images_ble/BLE_2.jpg" width="800px">
</div>
<br>

En fait, GAP permet des échanges un peu plus riches, illustrés par le graphique suivant :

<br>
<div align="left">
<img alt="BLE GAP, advertising plus response" src="images_ble/BLE_3.jpg" width="800px">
</div>
<br>

L'objet en mode scanner est, dans ce deuxième cas, *actif*. Il envoie une *scan request* à l'objet en mode advertiser qui lui répondra avec un nouveau message *scan reponse data* contenant 31 octets d'informations différentes de celles contenues dans les messages *advertising data*.

Le processus d'advertising est très utile pour des applications où les objets BLE servent de balises, par exemple pour la géolocalisation ou la recherche d'objets en intérieur (voir [IBeacon de Apple](https://fr.wikipedia.org/wiki/IBeacon)), la publicité dans les magasins, les visites interactives dans les musées, etc. La balise se limite à envoyer à tous les objets du voisinage des messages du genre, *"Salut ! Je suis une balise ! Voici mon nom et je pense que vous devriez visiter cette URL !"*. Aucune connexion ou appairage avec un smartphone ou autre objet ne sont nécessaires.

Une fois qu'une connexion est établie, l’advertising prend fin et le protocole GATT entre en action. Les échanges sont alors limités entre le Périphérique et l’unique Central auquel il est connecté.

## Compléments sur le Generic Attributes Profile (GATT)

_Ce protocole définit comment deux objets échangent des données à l’aide de services et de caractéristiques. GATT entre en jeu après GAP, une fois que deux objets sont connectés._ 

L'un des objets, en général qui était un *advertiser/broadcaster* avant connexion, adoptera le rôle de *peripheral* et se comportera comme un *serveur* (de caractéristiques). L'autre objet, qui était un *scanner/observer* avant connexion, deviendra un *central*, et se comportera comme un *client* (des caractéristiques mises à sa disposition par le périphérique). Par exemple, lorsque vous connectez un bracelet cardiofréquencemètre à votre smartphone via BLE, le bracelet devient un périphérique serveur de données cardiaques et le smartphone qui les enregistre et les affiche devient un central, client du bracelet.

Un aspect important à conserver à l'esprit concernant GATT, c'est que *les connexions sont exclusives* : un périphérique BLE ne peut pas être connecté à plus d'un central à un moment donné. A l'inverse, un central peut être connecté à plusieurs périphériques simultanément, comme le résume la figure suivante :

<br>
<div align="left">
<img alt="BLE GATT, network topology" src="images_ble/BLE_4.jpg" width="500px">
</div>
<br>

Si des données doivent être échangées entre deux périphériques, il sera donc nécessaire de passer par le central qui servira  de "boîte à lettres".

On dit que l'objet peripheral est un *esclave* du central, qui sera donc son *maître*. A l'établissement de la connexion, le peripheral propose au central un *intervalle de connexion* pour cadencer leurs échanges, mais c'est bien le central qui décidera in-fine de la fréquence de ses échanges futurs avec les différents peripherals auxquels il est connecté.

Le graphique suivant illustre le processus des échanges de données entre un objet peripheral (le serveur GATT) et un objet central (le client GATT), qui initie chaque transaction :

<br>
<div align="left">
<img alt="BLE GATT, master-slave exchanges" src="images_ble/BLE_5.jpg" width="800px">
</div>
<br>

Le protocole GATT repose sur un autre protocole d'échange de données bidirectionnel et *structuré* nommé *Attribute Protocol* (ATT) qui met en œuvre les concepts de *Profils*, de *Services* et de *Caractéristiques*, que l'on peut imaginer comme des conteneurs imbriqués selon la figure suivante :

<br>
<div align="left">
<img alt="BLE GATT, profiles, services, caracteristics" src="images_ble/BLE_6.jpg" width="220px">
</div>
<br>

- **Les profils** n'existent pas sur les périphériques, ce sont simplement des collections prédéfinies de services qui ont été compilées soit par le Bluetooth SIG, soit par le concepteur du périphérique.<br>
Le profil "Heart Rate", par exemple, rassemble le service "Heart Rate" et le service "Device Information". La liste complète des profils GATT officiellement adoptée est disponible [ici](https://www.bluetooth.com/specifications/gatt).

- **Les services** segmentent les données en unités logiques appelées *caractéristiques*. Un service peut contenir une ou plusieurs caractéristiques et chaque service est identifié de façon unique par un entier appelé **UUID** pour "*Universally Unique IDentifier*". L'UUID est codé sur 16 bits pour les services BLE officiels et sur 128 bits pour les services BLE personnalisés. Une liste complète des services BLE officiels peut être consultée [ici](https://www.bluetooth.com/specifications/gatt/services).<br><br>
**Par exemple**, le [service "Heart Rate"](https://www.bluetooth.com/specifications/gatt/viewer?attributeXmlFile=org.bluetooth.service.heart_rate.xml) a comme UUID la valeur 0x180D et contient jusqu'à trois caractéristiques, seule la première étant obligatoire : *Heart Rate Measurement*, *Body Sensor Location* et *Heart Rate Control Point*.

- **Les caractéristiques** sont les éléments de plus bas niveau pour les transactions GATT, elles encapsulent un unique point de mesure (une valeur pour un capteur de température ou bien un triplet (A<sub>x</sub>, A<sub>y</sub>, A<sub>z</sub>) pour un accéléromètre trois axes, par exemple). Les caractéristiques disposent toutes, à l'image des services, d'un UUID codé sur 16 ou 128 bits, et vous êtes libre soit d'utiliser [les caractéristiques standard définies par le Bluetooth SIG](https://www.bluetooth.com/specifications/gatt/characteristics), ce qui facilitera une compatibilité entre plusieurs dispositifs BLE, soit de définir vos propres caractéristiques pour un écosystème propriétaire (comme par exemple [Blue-ST](https://www.st.com/resource/en/user_manual/dm00550659-getting-started-with-the-bluest-protocol-and-sdk-stmicroelectronics.pdf) de STMicroelectronics).<br><br>
**Par exemple**, la caractéristique *Heart Rate Measurement* est obligatoire pour le service *Heart Rate*, et utilise un UUID égal à 0x2A37. La trame correspondante commence avec 8 bits qui décrivent le format des mesures cardiaques (entier non signé codé sur 8 bits, sur 16 bits ...) et se termine avec les octets encodant les valeurs mesurées.<br><br>
Les caractéristiques sont le **vecteur d'échange de données** en BLE, un central pourra *lire* une caractéristique pour prendre connaissance des mesures d'un périphérique. Mais il pourra aussi *écrire* dans une caractéristique exposée par l'un de ses périphériques pour lui envoyer des informations en retour. C'est par exemple comme cela qu'est implémenté le service UART, avec une caractéristique RX en lecture seule pour réaliser un canal de réception des données et une caractéristique TX en écriture pour réaliser un canal de réponse.

## Structure des trames d'annonce (GAP) et des trames de données (GATT)

Les trames (ou paquets) BLE sont de type **annonce** (ou **advertisement**) pour le protocole GAP ou bien **données** (ou **data**) pour le protocole GATT. La figure suivante précise leurs structures :

<br>
<div align="left">
<img alt="Trames BLE" src="images_ble/ble_packets.jpg" width="600px">
</div>
<br>

> **Source** : [Arup Barua et al.](https://ieeexplore.ieee.org/stamp/stamp.jsp?arnumber=9706334)

* Toutes les trames commencent avec un **préambule** d'un octet utilisé pour synchroniser les échanges.
* Le préambule est suivi par une **adresse d'accès** de 4 octets utilisée pour identifier la communication radio dans la couche physique.
* S'ensuit une **Protocol Data Unit (PDU)** constituée d'**un en-tête** de deux octets et d'une **payload** dont les tailles dépendent du type de trame :
  - Pour une trame d'annonce, la payload est constituée de 0 à 37 octets ;
  - Pour une trame de données, la payload est constituée de 0 à 255 octets incluant éventuellement un **en-tête L2CAP** de 4 octets et un **Message Integrity Check (MIC)** de 4 octets, en fait une "signature" [AES-128](https://fr.wikipedia.org/wiki/Advanced_Encryption_Standard) permettant d'authentifier l'émetteur.
* Toutes les trames se terminent par un **code de redondance cyclique** (CRC) de 3 octets pour détecter une éventuelle corruption des données.

Pour plus de détails sur les trames BLE vous pouvez consulter [ce document](https://beaujeant.github.io/resources/publications/ble.pdf).

## Pour résumer

La figure suivante illustre une séquence d'une connexion pair-à-pair entre deux objets BLE :

<br>
<div align="left">
<img alt="Séquence de connexion BLE" src="images_ble/ble_sequence.jpg" width="800px">
</div>
<br>

> **Source** : [STMicroelectronics](www.st.com)

### 1 - Avant que les objets se connectent : protocole GAP

- **Deux rôles possibles** : Objets *Advertisers* et objets *Scanners*

- Les **objets advertisers diffusent (broadcasting) des messages (trames)** qui signalent leur présence et indiquent les services qu'ils peuvent rendre.

- Les **objets scanners écoutent les trames d'annonce (advertising)**. Ils **recherchent** puis **listent** les objets advertisers qui diffusent à proximité et identifient leurs services. A ce stade, l'objet scanner peut **se connecter** à un ou plusieurs objets advertisers parmi ceux qu'il a recensés.

**Exemple** : Une smartwatch se comporte comme une balise (advertiser) et diffuse des trames GAP qui indiquent aux smartphones à proximité (scanners) qu'elle est disposée à fournir des relevés périodiques de la fréquence cardiaque de son porteur ainsi que le niveau de charge de sa batterie à quiconque souhaiterait se connecter avec elle.

### 2 - Une fois les objets connectés : protocole GATT

- **Deux rôles possibles** : Objet *Périphérique* et objet *Central*.

- Une fois que l'objet scanner est connecté à un objet advertiser, les deux changent de rôles :

    - L'objet scanner devient un central, qui va recevoir des données du périphérique. On considère alors qu'il devient client de celui-ci. Mais ce n'est pas tout à fait vrai, car le central pourra aussi envoyer des informations au périphérique. **Un central peut être connecté à plusieurs périphériques à la fois**.

    - L'objet advertiser cesse sa diffusion et communique désormais **exclusivement avec le central auquel il est connecté**. Il devient un périphérique de celui-ci et lui fournit des données ; il est client du central. Mais il peut aussi recevoir des données du central.

    - Tous les échanges de données périphériques-central se font via la lecture ou l'écriture dans des caractéristiques exposées par les périphériques.

**Reprenons l'exemple de notre smartwatch**. Le possesseur du smartphone scanner décide de s'y connecter. La smartwatch cesse d'émettre des trames d'advertising et établit une connexion exclusive avec le smartphone. La smartwatch devient alors un périphérique et le smartphone un central. Le smartphone lit les caractéristiques "Cardio" et "Batterie" exposées par la smartwatch. Il peut désormais afficher dans une application les données de battement cardiaque de son porteur et l'état de charge de la batterie de la montre.

## Informations et ressources

- Les [spécifications par le Bluetooth Special Interest Group](https://www.bluetooth.com/specifications/bluetooth-core-specification)
- Le [portail des développeurs Bluetooth](https://www.bluetooth.com/develop-with-bluetooth)
- La [liste officielle des profils et services BLE](https://www.bluetooth.com/specifications/gatt)
- La [liste officielle des caractéristiques BLE](https://www.bluetooth.com/specifications/gatt/characteristics)
- Pour travailler avec le BLE, l'application pour smartphone [nRF Connect for Mobile (versions Android)](https://play.google.com/store/apps/details?id=no.nordicsemi.android.mcp&hl=fr&gl=US&pli=1) ou [nRF Connect for Mobile (versions Apple)](https://apps.apple.com/ch/app/nrf-connect-for-mobile/id1054362403?l=fr) de [Nordic Semiconductor ASA](https://infocenter.nordicsemi.com/index.jsp) est incontournable.
- [Article de Argenox, Introduction to Bluetooth Low Energy](https://www.argenox.com/library/bluetooth-low-energy/introduction-to-bluetooth-low-energy-v4-0/)
- [Article de Argenox, Maximizing BLE Range](https://www.argenox.com/library/bluetooth-low-energy/maximizing-bluetooth-low-energy-ble-range/)
- [Article de circuitselectroniques.fr](https://circuitselectroniques.fr/plongez-dans-la-couche-phy-ble-les-bases-de-la-radio-phy-bluetooth-le/)
- [Article *Security and Privacy Threats for Bluetooth Low Energy in IoT and Wearable Devices: A Comprehensive Survey*](https://ieeexplore.ieee.org/stamp/stamp.jsp?arnumber=9706334) par ARUP BARUA et al., IEEE Open Journal of the communications society
- [Présentation de M. Beaujeant sur le BLE](https://beaujeant.github.io/resources/publications/ble.pdf).

## Notes au fil du texte

[^1]: D'après [Argenox](https://www.argenox.com/library/bluetooth-low-energy/introduction-to-bluetooth-low-energy-v4-0/) il existe cependant une variante "Bluetooth Long Range" pour les applications industrielles supposée porter jusqu'à 1 km en champ libre !

[^2]: Dans le cas du SoC STM32WB55, **hôte et contrôleur sont intégrés dans la même puce** par STMicroelectronics.