---
title: Les technologies de l'embarqué
description: Introduction aux technologies des systèmes embarqués
---

# Les technologies de l'embarqué

Cette section a pour vocation de proposer des tutoriels détaillés sur les différentes technologies (bus, périphériques, protocoles RF, etc.) que l'on rencontre et manipule dans les cadres de la programmation embarquée et des objets connectés.

## Sommaire


1. [L'Internet des Objets (IoT)](../IoT/index)
2. [Le Bluetooth Low Energy (BLE)](ble)
3. [LoRa et LoRaWAN](lora)
4. [Configuration d'une passerelle LoRaWAN The Things Indoor](ttig)


