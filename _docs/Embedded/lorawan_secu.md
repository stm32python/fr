---
title: Activation et sécurité d'une liaison LoRaWAN (Couche applicative)
description: Présentation de l'activation et de la sécurité d'une liaison LoRaWAN
---

# Activation et sécurité d'une liaison LoRaWAN (Couche applicative)

**L'infrastructure LoRAWAN n'est pas stabilisée**. La partie *[serveur réseau - serveur d'application - serveur d'application IoT]* fait notamment l'objet d'un travail de normalisation par la [**LoRa Alliance**](https://lora-alliance.org/) pour assurer une sécurité solide avec le chiffrement de bout en bout des messages. [La spécification 1.1.0](https://resources.lora-alliance.org/technical-specifications/ts009-1-1-0-certification-protocol) répond à cette problématique. Mais, sur le terrain, elle n'a pas été adoptée ; les infrastructures LoRaWAN suivent généralement [la spécification 1.0](https://resources.lora-alliance.org/technical-specifications/ts001-1-0-4-lorawan-l2-1-0-4-specification) expliquée par cette section.

## Activer une liaison LoRaWAN

Pour configurer une infrastructure LoRaWAN (comme, par exemple dans [ce tutoriel](../Micropython/grove/lora-e5)) il faut **activer** chaque nœud afin que ses trames soient acceptées un serveur d'application (hébergé par TTN dans notre tutoriel évoqué).
**Deux** méthodes d'activation sont possibles : **Activation-By-Personalisation (ABP)** et  **Over-The-Air-Activation (OTAA)** ; passons les en revue ...

### Principe de l'Activation-By-Personalisation (ABP)

**La méthode ABP** nécessite de renseigner **trois clefs** dans les nœuds et dans les serveurs :
* **NwkSKey** est la **Network Session Key**. Elle sert à vérifier l'intégrité des trames et à  crypter ou décrypter leur payload.
* **AppSKey** est l'**Application Session Key**. Elle sert à  sécuriser les communications de bout en bout entre le nœud et le serveur d'applications par une méthode de [**cryptographie symétrique**](https://fr.wikipedia.org/wiki/Cryptographie_sym%C3%A9trique).
* La **DevAddr** est la **Device Address**. Elle est similaire à une adresse IP pour le nœud.

ABP impose de pré-enregistrer DevAddr, AppSKey et NwkSKey sur les nœuds, sur les serveurs réseau et sur les serveurs d'applications.

Le principe de ABP est résumé par les figures suivantes :

<br>
<div align="left">
<img alt="Activation ABP" src="images_lora/abp.jpg" width="800px">
</div>
<br>

> Source : [Livre gratuit de Sylvain Montagny de l'Université de Savoie - Mont Blanc](https://www.univ-smb.fr/lorawan/livre-gratuit/)

**Mais le fait de copier les clefs sur plusieurs systèmes, qui sont autant de vecteurs d'attaques, n'est pas idéal pour la sécurité ...**

### Principe de l'Over-The-Air-Activation (OTAA)

**La méthode OTAA** nécessite également **trois clefs** dans les nœuds **avant activation** :

* Le **DevEUI**, identifiant unique du nœud ;
* L'**AppEU/JoinEUI**, identifiant du serveur d'application pour l'étape de *join request* (voir plus bas) ;
* L'**AppKey (Application key)**, une [clef cryptographique symétrique](https://fr.wikipedia.org/wiki/Advanced_Encryption_Standard) utilisée pour contrôler l'intégrité des messages.
  
Initialement DevEUI, AppEUI/JoinEUI et AppKey doivent être enregistrées sur les nœuds tandis que le serveur réseau a besoin de connaître aussi AppKey (cryptographie symétrique obligeant). Au cours de leur session d'activation, les nœuds se connectent au serveur (c'est la **join request**) qui génère les trois clef ABP en utilisant les trois clefs OTAA. Les clés ABP sont conservées jusqu'à leur réinitialisation.

Le principe de OTAA est résumé par les figures suivantes :

<br>
<div align="left">
<img alt="Activation OTAA, avant join" src="images_lora/otaa_1.jpg" width="800px">
</div>

<br>

<div align="left">
<img alt="Activation OTAA, après join" src="images_lora/otaa_2.jpg" width="800px">
</div>
<br>

> Source :  [Livre gratuit de Sylvain Montagny de l'Université de Savoie - Mont Blanc](https://www.univ-smb.fr/lorawan/livre-gratuit/)


**La méthode OTAA est plus utilisée que la méthode ABP car elle est considérée comme plus sûre**.

**En pratique, lorsque le Network Server (NS) choisi est [The Things Network](https://www.thethingsnetwork.org/)** (par exemple), pour configurer OTAA sur un objet, il faut procéder dans l'ordre suivant :
  
1. Choisir une clef **AppEUI** de 8 octets (par ex. *0101010101010101*). Peu importe sa valeur, mais **elle doit avoir la même valeur dans le firmware de votre objet et dans le NS**.

2. Choisir une clef **DevEUI** de 8 octets (par ex. *0080E11505310656*). Peu importe sa valeur, mais il faut absolument **qu'elle soit unique** et **qu'elle ait la même valeur dans le firmware de votre objet et dans le NS**. Pour s'assurer de son unicité, une procédure et en général fournie par le fabricant ou le vendeur de l'objet pour la générer ou bien elle est pré-générée et étiquetée dessus par ses soins. Mais vous pouvez aussi la créer vous même.

3. Obtenir une **AppKey** de 16 octets (par ex. *780C7F43AA05C35B9D4087B26167FB18*). C'est le NS qui la génère lorsqu'on enregistre l'objet dans son interface. **Il faut ensuite la renseigner dans l'objet**, dans un fichier de configuration lu par son firmware ou compilé directement dans celui-ci. La valeur de AppKey n'est évidemment pas arbitraire puisqu'il s'agit d'une clef cryptographique asymétrique publique.

Plusieurs exemples sur ce site illustrent cette procédure, entre autres [ce tutoriel en MicroPython](../Micropython/grove/lora-e5) ou encore [celui-ci avec Arduino](../Stm32duino/exercices/lora-e5).

## Attaques Replay et Frame Counter

Les communications LoRaWAN sont chiffrées avec des clefs AES 128 bits, ce qui assure un bon niveau de sécurité. Mais ce chiffrement ne protège pas d'un type d'attaque connue sous le nom de **replay** dont le principe est le suivant :
1. Le hacker enregistre des trames chiffrées circulant sur le réseau LoRaWAN ;
2. Le hacker réémet ces trames plus tard.

Le principe de l'attaque par replay est résumé par la figure suivante :

<br>
<div align="left">
<img alt="Attaque par replay" src="images_lora/replay.jpg" width="900px">
</div>
<br>

> Source :  [Livre gratuit de Sylvain Montagny de l'Université de Savoie - Mont Blanc](https://www.univ-smb.fr/lorawan/livre-gratuit/)


Même si le Hacker ne comprend pas le contenu des trames chiffrées, les données transportées sont, elles, bien comprises par l’Application Server. Des actions potentiellement malveillantes peuvent donc être réalisées simplement par cette attaque. 

Pour contrecarrer les attaques par replay, la trame LoRaWAN peut intégrer un champ variable appelé **Frame Counter**. Il s’agit d’un simple numéro d'ordre **qui augmente de 1 pour chaque nouvelle trame émise**. L’Application Server acceptera une nouvelle trame uniquement si la valeur de son Frame Counter est supérieure à celle de la précédente trame reçue. 

Donc, si un hacker retransmet une trame copiée, son Frame Counter ne respectera pas la séquence croissante attendue par le serveur et celui-ci la refusera. Du fait que les trames sont chiffrées, il est très difficile de falsifier leur champ Frame Counter pour échapper à cette stratégie de sécurité.

## Ressources

Cette brève section a été rédigée à partir des sources suivantes :

- [Le livre gratuit de Sylvain Montagny de l'Université de Savoie - Mont Blanc](https://www.univ-smb.fr/lorawan/livre-gratuit/)
- [Le chapitre dédié à ce sujet sur Mobile Fish](https://www.mobilefish.com/download/lora/lora_part21.pdf)
- [Le site The Things Network](https://www.thethingsnetwork.org/docs/lorawan/end-device-activation/)
- [Le site The Things Industries](https://www.thethingsindustries.com/docs/devices/abp-vs-otaa/)
- [La spécification LoRaWAN 1.0](https://resources.lora-alliance.org/technical-specifications/ts001-1-0-4-lorawan-l2-1-0-4-specification)

