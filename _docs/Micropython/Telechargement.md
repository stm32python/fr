---
title: Ressources et téléchargements
description: Liens de téléchargement des firmwares et tutoriels, liens vers des ressources en ligne concernant MicroPython et STM32python
---

# Ressources MicroPython

Vous trouverez dans cette section des liens vers des ressources en ligne utiles (ou indispensables !) pour l'apprentissage de MicroPython en général, et de MicroPython sur les microcontrôleurs STM32 en particulier. De nombreuses sources ou ressources recensées ici ont servi à la construction de ce site et sont utilisées au fil des tutoriels.

## Environnements de programmation en ligne

 - [L'IDE et les ressources proposées par la société Vittascience](https://fr.vittascience.com/) 

## Documentation et outils en ligne

**Site officiel de MicroPython**

  - [Page d'accueil du site](https://MicroPython.org/)
  - [Documentation générale sur MicroPython](http://docs.MicroPython.org/en/latest/)
  - [Exemple d’utilisation de la librairie pyb, basée sur un MCU STM32](http://docs.MicroPython.org/en/latest/pyboard/quickref.html#general-board-control)
  - [Les fonctions du firmware MicroPython spécifiques aux MCU STM32](https://docs.micropython.org/en/latest/library/stm.html)
  - [Les cartes de STMicroelectronics pour lesquelles un firmware MicroPyhton est disponible](https://MicroPython.org/stm32/)
  - [Code source du projet MicroPython](https://GitHub.com/Micropython/MicroPython)
 
**Site francophone dédié à MicroPython**

 - [http://micropython.fr/](http://micropython.fr/)

**Documentation technique des cartes NUCLEO et X-NUCLEO**

 - [NUCLEO-WB55](https://www.st.com/en/evaluation-tools/p-nucleo-wb55.html)
 - [NUCLEO-L476RG](https://www.st.com/en/evaluation-tools/nucleo-l476rg.html)
 - [X-NUCLEO IKS01A3](https://www.st.com/en/ecosystems/x-nucleo-iks01a3.html)

**Site officiel de Python 3**

 - [https://www.python.org/](https://www.python.org/)

**Documentation de l’application Android STBLESensor**

 - [https://github.com/STMicroelectronics/STBLESensor_Android](https://github.com/STMicroelectronics/STBLESensor_Android)

 **Documentation de l’application smartphone NRF Connect for Mobile**

 - [https://infocenter.nordicsemi.com/index.jsp?topic=%2Fug_gsg_ncs%2FUG%2Fgsg%2Ftest_mobile.html](https://infocenter.nordicsemi.com/index.jsp?topic=%2Fug_gsg_ncs%2FUG%2Fgsg%2Ftest_mobile.html)

**Pilotes et exemples avancés pour MicroPython**

 - [Des exemples de scripts MicroPython par Dave Hylands, notamment pour l'utilisation avancée des timers](https://github.com/dhylands/upy-examples)
 - [Bibliothèques et pilotes ESP8266 pour MicroPython, aisément adaptables aux STM32](https://GitHub.com/mchobby/esp8266-upy)
 - [Une (superbe) synthèse de pilotes et bibliothèques pour MicroPython](https://awesome-MicroPython.com/)
 - [Bibliothèques et pilotes pour Adafruit CircuitPython, aisément adaptables aux STM32](https://GitHub.com/adafruit/Adafruit_CircuitPython_Bundle/tree/master/libraries/drivers)

**Environnements de développement**

 L'IDE en ligne de la société [Vittascience](https://fr.vittascience.com/)  permet de programmer soit en utilisant des blocs ("façon scratch") soit directement en code Python. Elle sait  gérer un grand nombre de cartes à microncontrôleurs et de modules Grove (et autres).  NB : **Vous n'êtes pas obligés d'acheter des kits chez Vittascience pour utiliser cette IDE**. 

**Protocoles IoT**

 - [Le guide Internet de Steve sur le MQTT](http://www.steves-internet-guide.com/)
 - [Le site The Things Network](https://www.thethingsnetwork.org/)
 - [Un TP de M. Silanus sur LoRaWAN et TTN](http://silanus.fr/bts/formationIOT/LoRa/lora.pdf)
 - [La plateforme en ligne TagoIO](https://tago.io/)
 - [Le site "All About LoRa and LoRaWAN"](http://www.sghoslya.com/p/lora-vs-lorawan.html)

## Ouvrages

 - [MicroPython et Pyboard, Python sur microcontrôleur : de la prise en main à l'utilisation avancée (Editions ENI)](https://www.editions-eni.fr/livre/MicroPython-et-pyboard-python-sur-microcontroleur-de-la-prise-en-main-a-l-utilisation-avancee-9782409022906) de Dominique MEURISSE
 - [ LoRa - LoRaWAN et l'Internet des Objets](https://www.univ-smb.fr/lorawan/livre-gratuit/) de Sylvain MONTAGNY, Université de Savoie - Mont Blanc

## Téléchargements

### Editeurs de texte et de code
  - [Notepad++](https://notepad-plus-plus.org/)
  - [Thonny](https://thonny.org/)
  - [PyScripter](https://sourceforge.net/projects/pyscripter/)
  - [Mu](https://codewith.mu/) (avec le mode Pyboard)
  - [Geany](https://www.geany.org/)
  - [Arduino Lab for MicroPython](https://labs.arduino.cc/en/labs/micropython)

### Emulateurs de terminal série
  - [TeraTerm](https://osdn.net/projects/ttssh2/)
  - [PuTTY](https://www.PuTTY.org/)

### Applications pour smartphones
  - Pour créer facilement des applications sur smartphones Android ou Apple, l'environnement [MIT App inventor](../tools/AppInventor/)
  - Une application client BLE sur smartphone par STMicroelectronics : [STBLESensor](https://www.st.com/en/embedded-software/stblesensor.html)
  - Une application client BLE sur smartphone, [nRF Connect for Mobile (versions Android)](https://play.google.com/store/apps/details?id=no.nordicsemi.android.mcp&hl=fr&gl=US&pli=1) ou [nRF Connect for Mobile (versions Apple)](https://apps.apple.com/ch/app/nrf-connect-for-mobile/id1054362403?l=fr) de [Nordic Semiconductor ASA](https://infocenter.nordicsemi.com/index.jsp)

### Firmwares MicroPython pour STM32 (depuis le site micropython.org)
  - Pour la NUCLEO-WB55 : [Firmware 1.24.0 au format .hex](https://micropython.org/resources/firmware/NUCLEO_WB55-20241025-v1.24.0.hex)
  - Pour la NUCLEO-L476 : [Firmware 1.24.0 au format .hex](https://micropython.org/resources/firmware/NUCLEO_L476RG-20241025-v1.24.0.hex)

 Vous trouverez les instructions pour les programmer sur [cette page](../tools/cubeprog/cube_prog_firmware_stlink) ou [cette page](../tools/robocopy/index).

  **NB** : Nous nous concentrons ici sur deux cartes NUCLEO en particulier. Vous trouverez bien plus de firmwares pour les MCU de STMicroelectronics dans [la section de téléchargement du site officiel de MicroPython](https://MicroPython.org/stm32/).

### Logiciels pour manipuler le système de fichiers MicroPython

 - Dans quelques circonstances (décrites au fil des tutoriels) vous pourrez être amené à effacer la mémoire flash du microcontrôleur STM32 sur votre carte NUCLEO, ou encore à mettre à jour les firmwares d'autres composants sur celle-ci. Pour ces opérations vous aurez besoin du logiciel [STM32CubeProgrammer](https://www.st.com/en/development-tools/stm32cubeprog.html) et de [ce tutoriel](../tools/cubeprog/index).

 - Il existe sous Linux un outil en ligne de commande réalisé pour accéder à la mémoire flash des microcontrôleurs qui n'exposent pas leur contenu comme un disque virtuel ("mass storage") tel une clef USB. Il s'agit de **Rshell**, que vous trouverez [ici](https://github.com/dhylands/rshell).

 - Sous Windows et Linux, l'application [Ampy](https://github.com/scientifichackers/ampy) de Adafruit permet comme Rshell d'accéder au système de fichiers MicroPython pour y déposer des scripts, lire son contenu, etc.

>> **Une page sur notre site est consacrée à Rshell et Ampy**, [ici](../tools/rshell_ampy/index).

### Scripts MicroPython pour les tutoriels de ce site

- **Tutoriels pour démarrer avec la carte NUCLEO-WB55**<br>
Vous trouverez [ici](../../assets/Script/NUCLEO_WB55.zip) un fichier ZIP qui rassemble tous les codes sources présentés dans la section [Démarrer avec la carte NUCLEO-WB55](startwb55).

- **Tutoriels avec la carte X-NUCLEO-IKS01A3**<br>
Vous trouverez [ici](../../assets/Script/IKS01A3.zip) un fichier ZIP qui rassemble tous les codes sources présentés dans la section [Tutoriels avec la carte X-NUCLEO-IKS01A3](IKS01A3).

- **Tutoriels avec des modules externes (Grove, Adafruit, DFRobot, etc.)**<br>
Vous trouverez [ici](../../assets/Script/MODULES.zip) un fichier ZIP qui rassemble tous les codes sources présentés dans la section [Tutoriels avec des modules externes](grove).

- **Tutoriels avec le BLE**<br>
Vous trouverez [ici](../../assets/Script/BLE.zip) un fichier ZIP qui rassemble tous les codes sources présentés dans la section [Tutoriels avec le BLE](BLE).

- [**Lampe connectée**](http://icnisnlycee.free.fr/index.php/61-stm32/ble-en-MicroPython/78-exemple-ble-lampe-connectee)<br>
Piloter une lampe à distance via BLE avec MIT App Inventor. **Merci à Julien Launay !**<br>

- [**Station météo**](../../assets/projects/station_meteo.zip)<br>
Prototyper une station météo inspirée du produit Ikea Klockis. **Merci à Christophe Priouzeau & Gérald Hallet !**<br>
