---
title: Découverte du firmware MicroPython
description: Présentation et utilisation du firmware MicroPython pour les MCU STM32
---

# Découverte du firmware MicroPython

L’objectif de cette section est de présenter l'essentiel de l’environnement MicroPython exécuté sur [microcontrôleur (MCU)](../../Microcontrollers/microcontroleur) et ses  particularités pour [l'architecture STM32](../../Kit/stm32) en particulier [la carte NUCLEO-WB55](../../Kit/nucleo_wb55rg) mise en avant sur ce site.

## Le firmware MicroPython

Le "moteur" de l'environnement MicroPython est contenu dans un microprogramme (ou firmware en anglais) que l'on [installe sur le MCU cible](../install_win_wb55/index). Ce n'est pas exactement [un interpréteur](../../Microcontrollers/programmer) qui exécuterait pas à pas les instructions des scripts qu'on lui soumet. Il commence en fait par les compiler dans un langage intermédiaire, **le bytecode**, qui est ensuite exécuté par une **machine virtuelle**.

Le compilateur bytecode et la machine virtuelle sont donc installés avec le firmware MicroPython **dans** le MCU. **Mais ce n'est pas tout**, comme nous allons le voir MicroPython offre d'autres commodités que l'on trouve d'ordinaire dans les systèmes d'exploitation : **un système de fichiers**, des **interfaces de programmation (API) pour manipuler [les périphériques internes du MCU](../../Microcontrollers/microcontroleur)** et **une interface en ligne de commande (REPL)**. 

**Le principal avantage de MicroPython** (très appréciable à l'école), est sa facilité de mise en œuvre. On a seulement besoin d'une carte à MCU coûtant une vingtaine d'euros, d'un éditeur de texte et d'un logiciel émulateur de port série pour démarrer. En bonus, il permet un apprentissage raisonnablement facile de la programmation embarquée **et** du langage Python.

**Les principales limites de MicroPython** sont son empreinte mémoire conséquente et ses performances en retrait par comparaison avec un langage compilé tel que c/c++. Ces contreparties imposent d'utiliser des MCU "haut de gamme" intégrant autant de [mémoire RAM et de mémoire Flash](../../Kit/glossaire) que possible ce qui fait que tous les MCU ne sont pas capables d'exécuter MicroPython.
Pour plus d'informations (très techniques) sur les bonnes pratiques d'optimisation de MicroPython sur MCU, consultez [ce lien](https://docs.micropython.org/en/latest/reference/constrained.html#).

### Vue d'ensemble de l'environnement MicroPython

Le schéma ci-dessous résume la structure d'une installation MicroPython sur une carte [NUCLEO-WB55](../../Kit/nucleo_wb55rg) :

<br>
<div align="left">
<img alt="MicroPython" src="images/firmware_NUCLEO-WB55.jpg" width="800">
</div>
<br>

Pour utiliser MicroPython, vous avez besoin :

- D'un **ordinateur** avec un système Windows, Linux, MacOS ... Les deux outils logiciels indispensables sur cet ordinateur seront **un éditeur de texte** pour manipuler les scripts Python (stockés dans le système de fichiers, dans la flash du MCU) et **un émulateur de terminal série** (pour interagir avec le firmware via REPL).

- D'une **carte NUCLEO-WB55** avec son microcontrôleur STM32WB55RG. Bien sûr, sur ce MCU doit être installé [**le firmware adéquat**](https://micropython.org/download/NUCLEO_WB55/) conformément [aux instructions que vous trouverez ici](../install_win_wb55/index).

Une fois la carte NUCLEO-WB55 sous tension et connectée à l'ordinateur par son port USB-USER, elle exposera spontanément le contenu **du système de fichiers** créé par le firmware dans **la mémoire flash** du STM32WB55RG. Celui-ci apparaît comme un périphérique de stockage de masse, un lecteur USB nommé **PYBFLASH**, directement accessible par le **gestionnaire de fichiers** de l'ordinateur. 

Le même port USB-USER permet aussi d’accéder à **l’interpréteur de commandes Python (REPL)** que nous présentons un peu plus loin.

**Le firmware MicroPython**, tout comme le système de fichiers, est installé dans la mémoire flash du STM32WB55RG. Il gère le REPL et la compilation puis l'exécution des scripts de l'utilisateur. Il donne également accès à **l'API (découpée en modules)** qui permet de piloter le microcontrôleur STM32WB55RG : entrées sorties, timers, PWM, etc. L'ensemble de tutoriels [**Démarrer avec MicroPython et la NUCLEO-WB55**](../startwb55/index) est une introduction à cette API.

### L'invite de commandes REPL

**REPL**, acronyme de **"Read - Evaluate - Print Loop"**, est l'interface en ligne de commande de la machine virtuelle (MV) du firmware. Elle permet d'envoyer directement des instructions à la MV pour manipuler le système de fichier installé sur le microcontrôleur hôte et/ou lui faire exécuter des programmes Python.

REPL est tout à fait semblable aux interfaces en ligne de commande que l'on trouve sur les systèmes UNIX et Linux (les différents "shells") ou sous MS Windows (DOS, PowerShell).  L'utilisateur lance des commandes sous forme d'une entrée texte exécutée par REPL et la console lui renvoie le résultat également sous forme de texte.

L'utilisation de REPL se fait donc avec un **émulateur de port série** installé sur votre ordinateur (tel que [PuTTY](https://www.putty.org/), voir [ce tutoriel](../../Micropython/install_win_wb55/index)) et connecté au microcontrôleur par l'un de ses [UART](../startwb55/uart) intégrés. Par exemple, sur notre schéma ci-dessus, le PC est connecté au port USB-USER de la NUCLEO-WB55, lequel dialogue par une liaison UART avec le STM32WB55RG qui exécute REPL.

Le parti pris sur ce site est d'utiliser **le moins possible** REPL car notre objectif prioritaire est **d'enseigner aux élèves ce qu'est un système embarqué**. Lorsqu'on pilote son MCU depuis un PC avec REPL, le système embarqué n'est plus autonome et perd l'essentiel de sa raison d'être. Aussi, la plupart du temps, utilisons nous seulement deux commandes au clavier dans la console PuTTY / REPL :

- **[Ctrl]+[D]** pour relancer la MV du firmware après avoir copié un nouveau script *main.py* dans le disque PYBFLASH. Ceci force la MV à compiler et exécuter le script.
- **[Ctrl]+[C]** pour interrompre l'exécution en cours avant de soumettre un nouveau script  *main.py* à la MV.

Les copies de la console PuTTY ci-dessous illustrent une session REPL sur une carte NUCLEO-WB55 :

- On commence par un *[Ctrl]+[C]* pour interrompre un éventuel script déjà en cours d'exécution par la MV :

```console
KeyboardInterrupt: 
MicroPython v1.20.0 on 2023-04-26; NUCLEO-WB55 with STM32WB55RGV6
Type "help()" for more information.
>>> 
```

- On tape *dir()* puis *Entrée*, pour obtenir a liste des éléments disponibles dans l’espace de noms local :

```console
>>> dir()
['machine', '__name__', 'pyb']
>>> 
```
La commande nous informe de la présence de trois éléments, ```'machine'```, ```'__name__'``` et ```'pyb'```.

- Examinons à présent le contenu de  ```'pyb'``` :

```console
>>> dir(pyb)
['__class__', '__name__', 'main', '__dict__', 'RTC', 'ADC', 'ADCAll', 'ExtInt', 'Flash', 'LED', 'Pin', 'SPI', 'Switch', 'Timer', 'UART', 'USB_HID', 'USB_VCP', 'country', 'disable_irq', 'enable_irq', 'fault_debug', 'hid_keyboard', 'hid_mouse', 'repl_info', 'repl_uart', 'rng', 'usb_mode', 'wfi']
```
- Etc.

Bien que ce ne soit pas notre priorité, quelques exemples sur ce site abordent l'usage de REPL, en particulier :

 - [Paramétrage d'un module Grove RTC DS1307](../grove/rtc_DS1307)
 - [Prise de contrôle du REPL par BLE](../BLE/BLEREPL)
 - [Prise de contrôle du REPL par Bluetooth](../grove/repl_bc417)

Notons enfin que la console REPL offre aussi un historique des commandes tapées et des outils de navigation / auto-complétion au cours de la frappe à l'aide des touches *[.]* et *[TAB]*. 
Vous trouverez plus d'informations sur les possibilités offertes par REPL sur [cette page](https://docs.micropython.org/en/latest/reference/repl.html).

### Les fichiers du firmware

L'image ci-dessous montre *PYBFLASH* et son contenu dans un environnement PC - Windows :

<br>
<div align="left">
<img alt="Contenu du dosser PYBFLASH" src="images/PYBFLASH.jpg" width="700px">
</div>
<br>

Après installation du firmware celui-ci génère automatiquement un système de fichiers dans la mémoire flash du STM32WB55RG, qui contient ces quatre fichiers :

1. **boot.py** contient du code qui est exécuté au démarrage du firmware. Il sert à initialiser le matériel (USB, connexion à un réseau, montage d'un système de fichiers, duplication REPL sur un UART ...) ou à déterminer au boot quel sera  le script utilisateur à exécuter (*main.py* ou autre chose).
2. **main.py** contient du code qui est exécuté **après le démarrage du firmware**, c'est le script du programme Python de l'utilisateur. Il démarre automatiquement après l'exécution de *boot.py*. Juste après l'installation du firmware sur votre MCU, ce fichier est bien sûr vide.
3. **pybcdc.inf** est le pilote de port série pour les systèmes Windows.
4. **README.txt** un fichier d'aide qui explique comment configurer le port série du REPL.

Vous pouvez aussi créer vos propres fichiers "bibliothèques", d'extension *".py"*, dans PYBFLASH. Notamment, dans presque tous nos tutoriels, nous ajoutons dans PYBFLASH une ou plusieurs **bibliothèques de pilotes**, par exemple le fichier *ssd1308.py* pour piloter le module Grove afficheur OLED 1308 dans [ce tutoriel](../grove/oled_1308).

Pour finir, signalons que **le système de fichiers de PYBFLASH est "fragile"**, il supporte mal certaines opérations communes sur UNIX, Linux ou Windows ! Nous vous invitons à consulter [la page des FAQ](../../Kit/faq), pour y trouver quelques conseils afin de ne pas le "casser".

### L'API pour STM32 et les bibliothèques MicroPython

Peut-être vous interrogez vous sur l'omniprésence des mots *Pyboard* et *pyb* dans votre exploration du firmware MicroPython pour la NUCLEO-WB55 ? 
La raison est simple : [la gamme de cartes Pyboard](https://store.micropython.org/category/pyboards) est le matériel officiel pour le langage MicroPython. **Et les Pyboards sont toutes construites autour de microcontrôleurs STM32**. Par exemple, pour [la Pyboard v1.1](https://store.micropython.org/product/PYBv1.1H), le MCU est un STM32F405RG. Par conséquent, **tous les firmwares développés pour les MCU STM32 sont des variantes du firmware des cartes Pyboards**, et conservent dans leur architecture des références à celles-ci.

Dans la console REPL, tapez la commande *help(’modules’)* pour lister toutes les bibliothèques intégrées au firmware :


```console
MicroPython v1.21.0 on 2023-10-06; NUCLEO-WB55 with STM32WB55RGV6
Type "help()" for more information.
>>> help('modules')
__main__          bluetooth         io                re
_asyncio          builtins          json              select
_onewire          cmath             machine           socket
array             collections       math              stm
asyncio/__init__  deflate           micropython       struct
asyncio/core      dht               network           sys
asyncio/event     errno             onewire           time
asyncio/funcs     framebuf          os                uasyncio
asyncio/lock      gc                platform          uctypes
asyncio/stream    hashlib           pyb
binascii          heapq             random
Plus any modules on the filesystem
>>>
```

Cet affichage oncerne la révision 1.21 du firmware ; les bibliothèques disponibles évoluent bien sûr au fil de ses révisions. Chacune de ces bibliothèques contient un certain nombre de **classes**, lesquelles contiennent à leur tour des **méthodes** ou **fonctions** qui permettent de programmer le STM32WB55 en Python, de manipuler son système de fichier PYBFLASH et de contrôler ses entrées-sorties et ses périphériques intégrés.<br>

La documentation de toutes les bibliothèques est disponible [ici](https://docs.micropython.org/en/latest/library/index.html#python-standard-libraries-and-micro-libraries).

Deux de ces bibliothèques sont exclusivement dédiées au contrôle du MCU : [*machine*](https://docs.micropython.org/en/latest/library/machine.html) et [*pyb*](https://docs.micropython.org/en/latest/library/pyb.html) : 

- La bibliothèque *machine* est générique ; ses fonctions sont les mêmes pour tous les MCU, pas seulement les STM32. Elle est recommandée pour écrire des bibliothèques portables de pilotes pour des capteurs et modules externes au MCU.

- La bibliothèque *pyb* donne au contraire un accès optimisé à des fonctions propres à l'architecture STM32, mais les scripts qui l'utilisent ne seront en contrepartie plus portables vers d'autres architectures de MCU.


Signalons que *machine* et *pyb* sont importées par *boot.py* et qu'en principe vous n'aurez pas besoin de rajouter les instructions ```'import machine'``` et ```'import pyb'``` dans vos fichiers scripts *.py*.

Nous ne détaillerons les bibliothèques du firmware, leur utilisation est illustrée par les nombreux tutoriels sur ce site, à commencer par ceux concernant [le démarrage en MicroPython avec la carte NUCLEO-WB55](../startwb55/index). Pour en savoir plus, nous vous conseillons d'acquérir et de lire les chapitres correspondants de [l'ouvrage de Dominique MEURISSE](https://www.editions-eni.fr/livre/MicroPython-et-pyboard-python-sur-microcontroleur-de-la-prise-en-main-a-l-utilisation-avancee-9782409022906).

## Installer le firmware MicroPython sur une carte NUCLEO

La méthode la plus sûre pour installer le dernier firmware MicroPython à jour sur une carte NUCLEO consiste à :

- Télécharger le firmware **au format .hex** le plus récent pour votre carte directement depuis [micropython.org](https://micropython.org/download/).
- Installer [STM32CubeProgrammer](../../tools/cubeprog/index).
- Programmer le firmware dans le MCU STM32 selon la procédure indiquée [ici](../../tools/cubeprog/cube_prog_firmware_stm32).

Il existe une alternative nettement plus simple en ligne de commande avec l'utilitaire Windows **robocopy**, présentée  [ici](../../tools/robocopy/index).

**NB :** Tout ceci suppose que vous utilisez un ordinateur personnel avec un système MS Windows. Si votre système est Linux, vous trouverez un tutoriel sur [cette page](../install_linux/index). Si vous possédez un MAC ... la procédure reste à votre discrétion !

## Les outils pour programmer en MicroPython

Programmer en MicroPython ne nécessite pas beaucoup de logiciels autres que les outils déjà installés dans le MCU par le firmware. Comme déjà expliqué, il est indispensable d'installer au moins sur son PC / MAC : 

- **Un émulateur de port série**. Nous utilisons [PuTTY](https://www.PuTTY.org/) pour interagir avec REPL.
- **Un éditeur de texte**. Nous utilisons  [Notepad++](https://notepad-plus-plus.org/) pour rédiger nos scripts Python.

Il existe beaucoup d'environnements de programmation et d'outils **gratuits** pour MicroPython. Tout est affaire de préférence, chacun selon son projet ou ses pratiques pédagogiques choisira des solution différentes. En particulier, citons **trois IDE** pour MicroPython souvent utilisées à l'école :

- L'IDE en ligne de la société [Vittascience](https://fr.vittascience.com/) qui permet de programmer soit en utilisant des blocs ("façon scratch") soit directement en code Python. Elle sait  gérer un grand nombre de cartes à microncontrôleurs et de modules Grove (et autres).  NB : **Vous n'êtes pas obligés d'acheter des kits chez Vittascience pour utiliser cette IDE**. L'utilisation de ce site avec votre propre matériel est **totalement gratuite**.

- [Mu](https://codewith.mu/) s'installe en local sur votre PC/MAC et qui offre un *mode pyboard*, donc approprié pour les firmwares MicroPython STM32.

- [Thonny](https://thonny.org/), une autre "hors ligne", comme Mu, un peu plus "professionnelle", appréciée par les enseignants des bac pro et BTS.

Quelques informations supplémentaires et une liste un peu plus à jour de ressources en ligne, notamment de bibliothèques pour des capteurs, afficheurs et actuateurs est disponible sur la page [*Ressources MicroPython*](../Telechargement).

La plupart des cartes NUCLEO, qui ne disposent que d'un seul connecteur USB ST-LINK, (par exemple la [NUCLEO-L476RG](../../Kit/nucleo_l476)) **n'exposent pas** le système de fichiers *PYBFLASH*. Un outil supplémentaire sera donc nécessaire pour déposer vos scripts MicroPython dans *PYBFLASH*. Nous avons identifié et testé deux solutions :

 - Sous Linux **Rshell**, que vous trouverez [ici](https://github.com/dhylands/rshell)
 - Sous Windows et Linux, l'application [Ampy](https://github.com/scientifichackers/ampy) de Adafruit

>> **Une page sur notre site est consacrée à Rshell et Ampy**, [ici](../../tools/rshell_ampy/index)).

## Sources et ressources

Vous trouverez bien plus d'informations sur l'environnement MicroPython dans les ressources suivantes, qui ont largement inspiré la rédaction du présent article :

- [Le site officiel de MicroPython](https://docs.micropython.org/en/latest/reference/index.html)
- [Le site francophone dédié à MicroPython](http://micropython.fr/)
- L'ouvrage de  Dominique MEURISSE, [*MicroPython et Pyboard, Python sur microcontrôleur : de la prise en main à l'utilisation avancée (Editions ENI)*](https://www.editions-eni.fr/livre/MicroPython-et-pyboard-python-sur-microcontroleur-de-la-prise-en-main-a-l-utilisation-avancee-9782409022906)
- [Le GitHub "micropython-notes" de George Hawkins](https://github.com/george-hawkins/micropython-notes/tree/master)

