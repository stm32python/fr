---
title: Mise en œuvre de l'accéléromètre et du gyroscope LSM6DSO
description: Tutoriels pour la mise en œuvre de l'accéléromètre et du gyroscope LSM6DSO avec MicroPython
---

# Mise en œuvre de l'accéléromètre et du gyroscope LSM6DSO

L'exemple qui suit montre comment mesurer des accélérations et des rotations selon trois axes orthogonaux tous les dixièmes de seconde à l'aide du capteur MEMS [LSM6DSO](https://www.st.com/content/st_com/en/products/mems-and-sensors/inemo-inertial-modules/lsm6dso.html#tools-software) ([fiche technique](lsm6dso.pdf)) de STMicroelectronics.

## Matériel requis

1. La carte NUCLEO-WB55
2. Une carte d'extension I2C équipée du capteur MEMS LSM6DSO (par exemple la X-NUCLEO-IKS01A3)

## Le code MicroPython

**Vous pouvez télécharger les scripts MicroPython de ce tutoriel (entre autres) en cliquant [ici](../../../assets/Script/IKS01A3.zip)**.

Le fichier *lsm6dso.py* est la bibliothèque contenant les classes pour gérer cette centrale à inertie miniaturisée. Il doit être copié dans le répertoire du disque USB virtuel associé à la NUCLEO-WB55 : *PYBFLASH*.

Éditez maintenant le script *main.py* et copiez-y le code qui suit :

``` python
# Objet du script :
# Mesures d'accélérations et de rotations selon trois axes orthogonaux tous les dixièmes de seconde.
# Allume ou éteint les LED de la carte selon les valeurs des accélérations
# Affiche les vitesses de rotation angulaires sur le port série
# Cet exemple nécessite un capteur MEMS - centrale à inertie LSM6DSO 
# (par exemple celui du shield X-NUCLEO-IKS01A3)

from machine import I2C # Pilot du bus I2C
import lsm6dso # Pilote de la centrale à inertie
from time import sleep_ms # Pour temporiser

# On utilise l'I2C n°1 de la carte NUCLEO-W55 pour communiquer avec le capteur
i2c = I2C(1) 

# Pause d'une seconde pour laisser à l'I2C le temps de s'initialiser
sleep_ms(1000)

# Liste des adresses I2C des périphériques présents
print("Adresses I2C utilisées : " + str(i2c.scan()))

intertial_sensor = lsm6dso.LSM6DSO(i2c) # Instanciation du capteur
intertial_sensor.scale_g('2000') # Moindre sensibilité pour les mesures angulaires
intertial_sensor.scale_a('2g') # Sensibilité maximum pour les mesures d'accélérations

# Instanciation des LED
led_blue = pyb.LED(1)
led_green = pyb.LED(2)
led_red = pyb.LED(3)

while True: # Boucle sans clause de sortie

	# Temporisation
	sleep_ms(100)

	# Mesures des accélérations selon les 3 axes 
	ax = intertial_sensor.ax()
	ay = intertial_sensor.ay()
	az = intertial_sensor.az()

	# Mesures des angles de rotation selon les 3 axes 
	gx = intertial_sensor.gx()
	gy = intertial_sensor.gy()
	gz = intertial_sensor.gz()

	 # Si la valeur absolue de l'accélération sur l'axe X est supérieur à 700 mG alors
	if abs(ax) > 700 :
		led_green.on()
		print("ax : " + str(ax) + " mg")
	else:
		led_green.off()
		
	# Si la valeur absolue de l'accélération sur l'axe Y est supérieur à 700 mG alors
	if abs(ay) > 700 :
		led_blue.on()
		print("ay : " + str(ay) + " mg")
	else:
		led_blue.off()
		
	# Si la valeur absolue de l'accélération sur l'axe Z est supérieur à 700 mG alors
	if abs(az) > 700 :
		led_red.on()
		print("az : " + str(az) + " mg")
	else:
		led_red.off()

	print("Gx = " + str(gx/1000) + " °/s")
	print("Gy = " + str(gy/1000) + " °/s")
	print("Gz = " + str(gz/1000) + " °/s")
```
Vous pouvez à présent lancer le script avec la combinaison de touches CTRL + D dans le terminal série et observer les données d'accélération et de giration mesurées par le capteur, ainsi que le clignotement des LED selon les accélérations :

<br>
<div align="left">
<img alt="Capteur LSM6DSO, mesures simples" src="images/LSM6DSO_raw.jpg" width="500px">
</div>
<br>

## Pour aller plus loin

La fonction gyroscope de ce capteur n’est pas vraiment exploitable (sauf pour détecter des mouvements). La principale difficulté étant la dérive de la mesure d’angle ; le capteur ne dispose d’aucune indication sur une direction fixe dans l’espace qui pourrait l’aider à se recalibrer régulièrement. <br>
Pour construire une centrale à inertie efficace et mesurer des angles de rotation raisonnablement précis et absolus, il faut coupler le MEMS gyroscope à un [MEMS accéléromètre](lis2dw12) (qui donne la direction de la verticale) mais aussi, idéalement, à un [MEMS magnétomètre](lis2mdl) (qui donne une direction acceptable du nord magnétique lorsqu’il est calibré). C’est le principe de la [**fusion de données**](https://fr.wikipedia.org/wiki/Fusion_de_donn%C3%A9es), que nous illustrons dans [ce tutoriel](fuse).
