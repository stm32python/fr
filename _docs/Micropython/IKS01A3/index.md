---
title: Tutoriels pour la carte d'extension X-NUCLEO-IKS01A3
description: Tutoriels en MicroPython pour la carte d'extension X-NUCLEO-IKS01A3 
---

# Tutoriels pour la carte d'extension X-NUCLEO-IKS01A3

## Présentation de la X-NUCLEO-IKS01A3

Vous trouverez une présentation de la carte d'extension X-NUCLEO-IKS01A3 de STMicroelectronics sur [cette page](../../Kit/x_nucleo_iks01A3).

## Liste des tutoriels et précisions sur le bus I2C

Le tableau qui suit donne la liste des tutoriels MicroPython proposés avec la X-NUCLEO-IKS01A3.<br>
Vous constaterez qu'ils utilisent **le protocole I2C** et sont donc connectés au bus du même nom sur la carte NUCLEO-WB55.

I2C est l'acronyme de "Inter-Integrated Circuit" (en français : bus de communication intégré inter-circuits). Il s'agit d'un bus série fonctionnant selon un protocole inventé par Philips. Chaque module "esclave" branché sur celui-ci **est identifié par une adresse unique codée sur 7 bits** (rappelée en dernière colonne du tableau).<br>
Avec la prolifération des modules I2C, **il pourrait arriver que deux ou plusieurs modules que vous auriez connectés sur un bus I2C aient la même adresse**, ce qui conduirait inévitablement à des plantages. Pour éviter ce type de conflits, vous devrez consulter les fiches techniques de vos modules et, pour ceux qui le nécessiteraient (et le permettraient), prendre soin de modifier leur adresse I2C, généralement codée dans leur firmware, à l'aide de cavaliers ou encore de points de soudure.

On notera qu'un grand nombre de modules produits par des sociétés telles que Seeed Studio, Adafuit, DFRobot ... sont également conçus autour de composants MEMS de STMicroelectronics. Pour ces modules, les bibliothèques et exemples de la X-NUCLEO-IKS01A3 devraient fonctionner sans aucune modification sauf, peut-être, un changement de l'adresse I2C.

<br>

|**Tutoriel**|**Bus**|**Adresse**|
|:-|:-:|:-:|
[Mise en œuvre de l'accéléromètre LIS2DW12 et programmation d'un inclinomètre](lis2dw12)|I2C|0x19|
[Mise en œuvre de l'accéléromètre et du gyroscope LSM6DSO](lsm6dso)|I2C|0x6B|
[Mise en œuvre du magnétomètre LIS2MDL et programmation d'une boussole](lis2mdl)|I2C|0x1E, 0x19|
[Mise en œuvre du baromètre et thermomètre LPS22HH et programmation d'un altimètre](lps22hh)|I2C|0x5D|
[Mise en œuvre du capteur de température et d'humidité relative HTS221](hts221)|I2C|0x5F|
[Mise en œuvre du capteur de température de précision STTS751](stts751)|I2C|0x4A|
[Mise en œuvre de la fusion de données](fuse)|I2C|0x6B, 0x1E|
