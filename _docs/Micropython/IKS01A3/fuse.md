---
title: Mise en œuvre de de la fusion de données 
description: Tutoriels pour la mise en œuvre de la fusion de données avec MicroPython
---

# Mise en œuvre de de la fusion de données

Cet exemple montre comment programmer en MicroPython une application qui réalise la [**fusion de données**](https://fr.wikipedia.org/wiki/Fusion_de_donn%C3%A9es) avec des capteurs inertiels de la carte d’extension X-NUCLEO-IKS01A3.

La fusion de données est une technique de traitement du signal qui consiste à tirer profit de plusieurs capteurs conçus avec des technologies différentes et/ou complémentaires, qui ont des domaines de sensibilités et des fréquences de mesures complémentaires, pour reconstruire à l’aide d’opérations mathématiques élaborées une information qui nous intéresse.  [Selon Wikipedia](https://fr.wikipedia.org/wiki/Filtre_de_Kalman) : *"Un exemple d'utilisation (de la fusion de données à l'aide d'un filtre de Kalman) peut être la mise à disposition, en continu, d'informations telles que la position ou la vitesse d'un objet à partir d'une série d'observations relatives à sa position, incluant éventuellement des erreurs de mesures"*.

Son application la plus populaire est sans doute la création de **centrales inertielles IMU** (« Inertial Measurement Unit ») telles que celles des avions, des drones, des casques de réalité virtuelle, des robots … afin qu'ils puissent **déterminer leur orientation dans l’espace de façon autonome**. Concrètement, il s’agit de calculer à chaque instant [**les 3 angles d’Euler**]( https://fr.wikipedia.org/wiki/Angles_d%27Euler) entre les axes *(O<sub>x</sub>, O<sub>y</sub>, O<sub>z</sub>)* d’un [trièdre orthonormé direct](https://fr.wikipedia.org/wiki/Tri%C3%A8dre) attaché au système mobile et ceux d’un trièdre orthonormé direct immobile (« le sol »).

L'invention des [**MEMS**]( https://fr.wikipedia.org/wiki/Microsyst%C3%A8me_%C3%A9lectrom%C3%A9canique) **inertiels** (accéléromètres, gyroscopes, magnétomètres) a permis de construire des IMU bon marché, minuscules et consommant très peu d’énergie ; on les trouve désormais dans de nombreux produits grand public. Nous allons donc, sur la base [**des tutoriels proposés par Peter Hinch**](https://GitHub.com/MicroPython-IMU/MicroPython-fusion), mettre en œuvre un [**filtre de Madgwick**]( https://ahrs.readthedocs.io/en/latest/filters/madgwick.html) utilisant [**l'algèbre des quaternions**](https://fr.wikipedia.org/wiki/Quaternions_et_rotation_dans_l%27espace) (ce n'est pas simple !) pour la fusion de données sur un LSM6DSO dans un premier exemple, puis sur un LSM6DSO associé à un LIS2MDL.<br>
Dans les deux cas, les conventions pour les angles et les orientations restent les mêmes que pour le [tutoriel sur la boussole](lis2mdl), sur la carte d’extension X-NUCLEO-IKS01A3, rappelés par la figure qui suit : 

<br>
<div align="left">
<img alt="Définition des angles d'Euler" src="images/IKS01A3_Compass_Tilt.jpg" width="450px">
</div>
<br>

Vous n'êtes pas obligés d'utiliser la  X-NUCLEO-IKS01A3 et ses capteurs, mais nous vous conseillons de respecter les orientations relatives des axes de sorte que :
- L'angle *Heading* (le cap) varie de 0 à  360° lorsqu'on fait tourner l'axe *z* dans le sens horaire ;
- L'angle *Pitch* (le tangage) varie de 0 à 90° lorsqu'on effectue une rotation autour de l'axe *y* dans le sens qui fait tourner l'axe *x* vers le haut ;
- L'angle *Roll* (le roulis) varie de 0 à 90° lorsqu'on effectue une rotation autour de l'axe *x* dans le sens qui fait tourner l'axe *y* vers le bas.

Le tutoriel de Peter Hinch propose deux variantes pour la fusion de capteur, une synchrone et une asynchrone. Nous avons choisi cette deuxième approche pour illustrer l'usage de [**la bibliothèque uasyncio de MicroPython**](https://GitHub.com/peterhinch/MicroPython-async/blob/master/v3/docs/TUTORIAL.md).

## Matériel requis

1. La carte NUCLEO-WB55
2. La carte d'extension I2C X-NUCLEO-IKS01A3 

## Premier exemple : IMU à six degrés de liberté

Les IMU les plus répandues sont de type **6 degrés de liberté** (en anglais ["6 Degrees Of Freedom"]( https://en.wikipedia.org/wiki/Six_degrees_of_freedom#:~:text=Six%20degrees%20of%20freedom%20(6DOF,body%20in%20three%2Ddimensional%20space.)), on utilise donc en pratique l'abréviation "6DOF"), ce qui signifie qu’elles réalisent la fusion de six mesures obtenues à l’aide de deux MEMS :

- **Un accéléromètre**, pour mesurer 3 valeurs indépendantes d’accélérations *(a<sub>x</sub>, a<sub>y</sub>, a<sub>z</sub>)* selon les axes d’un trièdre orthonormé direct.
- **Un gyroscope**, pour mesurer des angles de rotation *(g<sub>x</sub>, g<sub>y</sub>, g<sub>z</sub>)* autour des 3 axes de ce même repère.

On remarquera *qu’il est impossible de déterminer l’angle de cap* (heading) avec une IMU 6DOF car il nous manque une indication sur une direction fixe dans le plan horizontal (celle du pôle nord magnétique).

Pour cet exemple, quatre scripts sont nécessaires, que vous trouverez dans l’archive ZIP de **[la zone de téléchargement](../../../assets/Script/IKS01A3.zip)** :
1.	Le fichier *lsm6dso.py*, bibliothèque contenant les classes pour gérer le capteur du même nom ;
2.	Le fichier *deltat.py*, bibliothèque utilisée pour la discrétisation temporelle lors de la fusion de données ;
3.	Le fichier *fusion_async.py* pour réaliser la fusion de données ;
4.	Le fichier *main.py* qui contient notre habituel programme principal.

Tous doivent être copiés dans le répertoire du disque USB virtuel *PYBFLASH* associé à la NUCLEO-WB55.<br>
Voici le contenu de *main.py* : 

``` python
# Objet du script :
# Mise en œuvre d'un algorithme de Madgwick pour la fusion de données
# dans le cas d'une centrale à inertie mesurant 6 degrés de libertés (6DOF) :
# 3 d'accélération et 3 de rotation.
# Le capteur choisi est le LSM6DSO de STMicroelectronics et les axes Ox, Oy, Oz sont orientés
# comme sur le shield X-NUCLEO-IKS01A3. 
# Cet exemple est une application directe des excellentes ressources partagées par Peter Hinch
# à cette URL : https://GitHub.com/MicroPython-IMU/MicroPython-fusion.
# Le tutoriel de Peter Hinch propose deux variantes pour la fusion de capteur, une synchrone
# et une asynchrone. Nous avons choisi cette deuxième approche pour illustrer l'usage 
# la bibliothèque uasyncio de MicroPython.
# NB : le cap "Heading" reste égal à zéro car un magnétomètre est nécessaire pour le déterminer
# (voir l'exemple avec 9 degrés de liberté).

from machine import I2C # Pilote du bus I2C
import lsm6dso # Pilote de la centrale à inertie
from time import sleep_ms # Pour temporiser

import uasyncio as asyncio # Pour la gestion asynchrone
import gc # Appel du "ramasse miettes"
from fusion_async import Fusion # Pour la fusion de données

# On utilise l'I2C n°1 de la carte NUCLEO-W55 pour communiquer avec le capteur
i2c = I2C(1)

# Pause d'une seconde pour laisser à l'I2C le temps de s'initialiser
sleep_ms(1000)

# Liste des adresses I2C des périphériques présents
print("Adresses I2C utilisées : " + str(i2c.scan()))

# Instanciation du capteur
intertial_sensor = lsm6dso.LSM6DSO(i2c)

# Moindre sensibilité pour les mesures angulaires
intertial_sensor.scale_g('2000')

# Sensibilité maximum pour les mesures d'accélérations
intertial_sensor.scale_a('2g') 

# Coroutine pour la collecte des données et le paramétrage de sa fréquence
async def read_coro():
	
	# Temporisation
	await asyncio.sleep_ms(20)

	# Mesures des accélérations selon les 3 axes 
	ax = intertial_sensor.ax()
	ay = -1 * intertial_sensor.ay()
	az = intertial_sensor.az()

	# Construction du tuple des accélérations
	accel = (ax,ay,az)

	# Mesures des angles de rotation selon les 3 axes (en degrés/s)
	# Il est impératif que ces valeurs soient exprimées en degrés/s
	# et pas en radians/s !
	gx = intertial_sensor.gx() / 1000
	gy = -1 * intertial_sensor.gy() / 1000
	gz = intertial_sensor.gz() / 1000
	
	# Construction du tuple des gyrations
	gyro = (gx, gy, gz)

	# Affichage sur le port série
#	print("Accélérations")
#	print("Ax (mg) = %d" %accel[0])
#	print("Ay (mg) = %d" %accel[1])
#	print("Az (mg) = %d" %accel[2])
#	print("")
	
#	print("Rotations")
#	print("Gx (°/s) = %d" %gyro[0])
#	print("Gy (°/s) = %d" %gyro[1])
#	print("Gz (°/s) = %d" %gyro[2])
#	print("")

	return accel, gyro

# Instanciation pour la fusion des données
fuse = Fusion(read_coro)

# Couroutine d'appels forcés et réguliers au ramasse-miettes 
# Nécessaire pour la stabilité sur le long terme
async def mem_manage():
	while True:
		await asyncio.sleep_ms(100)
		gc.collect()
		gc.threshold(gc.mem_free() // 4 + gc.mem_alloc())
		

# Couroutine pour l'affichage
async def display():
	fs = 'Heading: {:4.0f} Pitch: {:4.0f} Roll: {:4.0f}'
	while True:
		print(fs.format(fuse.heading, fuse.pitch, fuse.roll))
		await asyncio.sleep_ms(100)

# Couroutine pour la fusion des données
async def fuse_task():
	await fuse.start()
	loop = asyncio.get_event_loop()
	await display()

# Création et lancement des tâches 
asyncio.create_task(mem_manage())
asyncio.run(fuse_task())
```

Vous pouvez à présent lancer le script avec la combinaison de touches CTRL + D dans le terminal série de PuTTY et observer l'évolution des angles de tangage et de roulis lorsque vous faites tourner la carte NUCLEO et son shield X-NUCLEO dans l'espace :

<br>
<div align="left">
<img alt="Capteur LSM6DSO, fusion de données 6DOF" src="images/fusion_6dof.jpg" width="400px">
</div>
<br>

## Deuxième exemple : IMU à neuf degrés de liberté

Toujours à l'aide du X-NUCLEO-IKS01A3 nous allons réaliser une IMU à **neuf degrés de liberté** (abréviation "9DOF"), qui réalisent la fusion de neuf mesures obtenues à l’aide de trois MEMS :

- **Un accéléromètre**, pour mesurer les accélérations *(a<sub>x</sub>, a<sub>y</sub>, a<sub>z</sub>)* ;
- **Un gyroscope**, pour mesurer des angles de rotation *(g<sub>x</sub>, g<sub>y</sub>, g<sub>z</sub>)* ;
- **Un magnétomètre**, pour mesurer les champs magnétiques *(b<sub>x</sub>, b<sub>y</sub>, b<sub>z</sub>)*.

Les conventions pour les axes restent celles de l'exemple 6DOF.

Cette exemple utilise deux composants de STMicroelectronics : l'accéléromètre et gyromètre LSM6DSO et le magnétomètre LIS2MDL. Ce dernier permet de déterminer la direction du pôle nord magnétique et, par conséquent, *de calculer l'angle de cap*.

### Préalable : calibrer le LIS2MDL
Comme expliqué en détails dans [**ce tutoriel**](lis2mdl), il est indispensable que vous commenciez par déterminer les **coefficients de calibration** avec les scripts de [cette section](https://gitlab.com/stm32python/fr/-/blob/master/_docs/Micropython/IKS01A3/lis2mdl.md#le-code-pour-obtenir-les-valeurs-qui-serviront-%C3%A0-calibrer-la-boussole). Reportez-les ensuite dans le script *main.py* ci-dessous.

Nous rappelons que les mesures des magnétomètres sont fortement bruitées par les champs magnétiques environnants et les masses métalliques dans les bâtiments, nous vous conseillons donc de procéder au calibrage en extérieur si-possible.

### Les scripts MicroPython

Pour cet exemple, cinq scripts sont nécessaires, que vous trouverez dans l’archive ZIP de **[la zone de téléchargement](../../../assets/Script/IKS01A3.zip)** :
1. Le fichier *lsm6dso.py*, pour gérer l'accéléromètre et le gyroscope ;
2. Le fichier *lis2mdl.py*, pour gérer le magnétomètre ; 
3. Le fichier *deltat.py*, pour la discrétisation temporelle de la fusion de données ;
4. Le fichier *fusion_async.py* pour réaliser la fusion de données ;
5. Le fichier *main.py* qui contient notre habituel programme principal.

Tous doivent être copiés dans le répertoire du disque USB virtuel *PYBFLASH* associé à la NUCLEO-WB55.<br>
Voici le contenu de *main.py* : 

``` python
# Objet du script :
# Mise en œuvre d'un algorithme de Madgwick pour la fusion de données
# dans le cas d'une centrale à inertie mesurant 9 degrés de libertés (9DOF) :
# 3 d'accélération, 3 de rotation et 3 de champ magnétique.
# Les capteurs choisis sont le LSM6DSO et le LIS2MDL de STMicroelectronics. 
# Leurs axes Ox, Oy, Oz respectifs sont orientés comme sur le shield X-NUCLEO-IKS01A3. 
# Cet exemple est une application directe des excellentes ressources partagées par Peter Hinch
# à cette URL : https://GitHub.com/MicroPython-IMU/MicroPython-fusion.
# Le tutoriel de Peter Hinch propose deux variantes pour la fusion de capteur, une synchrone
# et une asynchrone. Nous avons choisi cette deuxième approche pour illustrer l'usage 
# la bibliothèque uasyncio de MicroPython.
# On suppose que les constantes de calibrage du magnétomètre ont été préalablement déterminées par le script
# dédié à cette opération dans le tutoriel https://stm32python.gitlab.io/fr/docs/Micropython/IKS01A3/lis2mdl.

from machine import I2C # Pilote du bus I2C
import lsm6dso # Pilote de la centrale à inertie
import lis2mdl # Pilote du magnétomètre
from time import sleep_ms # Pour temporiser

import uasyncio as asyncio # Pour la gestion asynchrone
import gc # Appel du "ramasse miettes"
from fusion_async import Fusion # Pour la fusion de données

# On utilise l'I2C n°1 de la carte NUCLEO-W55 pour communiquer avec le capteur
i2c = I2C(1)

# Pause d'une seconde pour laisser à l'I2C le temps de s'initialiser
sleep_ms(1000)

# Liste des adresses I2C des périphériques présents
print("Adresses I2C utilisées : " + str(i2c.scan()))

# Instanciation du gyroscope et de l'accéléromètre
intertial_sensor = lsm6dso.LSM6DSO(i2c)

# Instanciation du magnétomètre
magnetometer = lis2mdl.LIS2MDL(i2c)

# Moindre sensibilité pour les mesures angulaires
intertial_sensor.scale_g('2000')

# Sensibilité maximum pour les mesures d'accélérations
intertial_sensor.scale_a('2g') 

# Valeurs extrêmes selon chaque axe du champ magnétique obtenues par le script de calibration
# de la page https://stm32python.gitlab.io/fr/docs/Micropython/IKS01A3/lis2mdl.

min_bx = const(-75)
max_bx = const(18)
min_by = const(-24)
max_by = const(61)
min_bz = const(-120)
max_bz = const(-42)

# Calcul des décalages (offsets) "Hard Iron" pour chaque axe :

offset_x = (max_bx + min_bx) / 2
offset_y = (max_by + min_by) / 2
offset_z = (max_bz + min_bz) / 2

# Calcul des coefficients pour la correction approximative des 
# distorsions "Soft Iron" pour chaque axe :

avg_delta_x = (max_bx - min_bx) / 2
avg_delta_y = (max_by - min_by) / 2
avg_delta_z = (max_bz - min_bz) / 2

avg_delta = (avg_delta_x + avg_delta_y + avg_delta_z) / 3

scale_x = avg_delta / avg_delta_x
scale_y = avg_delta / avg_delta_y
scale_z = avg_delta / avg_delta_z

# Coroutine pour la collecte des données et le paramétrage de la fréquence
async def read_coro():
	
	# Temporisation
	await asyncio.sleep_ms(20)

	# Mesures des accélérations selon les 3 axes 
	ax = intertial_sensor.ax()
	ay = -1 * intertial_sensor.ay()
	az = intertial_sensor.az()

	# Mesures des angles de rotation selon les 3 axes (en degrés/s)
	# Il est impératif que ces valeurs soient exprimées en degrés/s
	# et pas en radians/s !
	gx = intertial_sensor.gx() / 1000
	gy = -1 * intertial_sensor.gy() / 1000
	gz = intertial_sensor.gz() / 1000
	
	# Mesure du champ magnétique selon les 3 axes
	# Puis on applique les corrections hard iron et soft iron.
	bx = (magnetometer.x() - offset_x) * scale_x
	by = (magnetometer.y() - offset_y) * scale_y
	bz = (magnetometer.z() - offset_z) * scale_z
	
	# Renvoie les mesures sous forme de rumples
	return (ax,ay,az),(gx, gy, gz),(bx, by, bz)

# Instanciation pour la fusion des données
fuse = Fusion(read_coro)

# Couroutine d'appels forcés et réguliers au ramasse-miettes 
# Nécessaire pour la stabilité sur le long terme
async def mem_manage():
	while True:
		await asyncio.sleep_ms(100)
		gc.collect()
		gc.threshold(gc.mem_free() // 4 + gc.mem_alloc())

# Couroutine pour l'affichage
async def display():
	fs = 'Heading: {:4.0f} Pitch: {:4.0f} Roll: {:4.0f}'
	while True:

		# Si le cap obtenu est négatif, 
		# calcule son complément à 360° 
		if fuse.heading < 0:
			fuse.heading += 360

		print(fs.format(fuse.heading, fuse.pitch, fuse.roll))
		await asyncio.sleep_ms(100)

# Couroutine pour la fusion des données
async def fuse_task():
	await fuse.start()
	loop = asyncio.get_event_loop()
	await display()

# Création et lancement des tâches 
asyncio.create_task(mem_manage())
asyncio.run(fuse_task())
```

### Expérimentation

Vous pouvez à présent lancer le script avec la combinaison de touches CTRL + D dans le terminal série de PuTTY et observer l'évolution des angles de cap, de tangage et de roulis lorsque vous faites tourner la carte NUCLEO et son shield X-NUCLEO dans l'espace :

<br>
<div align="left">
<img alt="Capteurs LSM6DSO et LIS2MDL, fusion de données 9DOF" src="images/fusion_9dof.jpg" width="400px">
</div>
<br>

Le magnétomètre est très sensible à son environnement magnétique. Aussi, si les indications de cap vous paraissent erronées, nous vous conseillons de tester l'IMU en extérieur, raisonnablement loin de toute masse métallique.
