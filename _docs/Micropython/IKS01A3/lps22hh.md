---
title: Mise en œuvre du baromètre et thermomètre LPS22HH et programmation d'un altimètre
description: Tutoriels pour la mise en œuvre du baromètre et capteur de température LPS22HH et la programmation d'un altimètre barométrique avec MicroPython
---

# Mise en œuvre du baromètre et thermomètre LPS22HH et programmation d'un altimètre barométrique

Cet exemple montre comment mesurer la température et la pression toutes les cinq secondes avec le capteur MEMS [LPS22HH](https://www.st.com/content/st_com/en/products/mems-and-sensors/pressure-sensors/lps22hh.html) de STMicroelectronics.

## Matériel requis

1. La carte NUCLEO-WB55
2. Une carte d'extension I2C équipée du capteur MEMS LPS22HH (par exemple la X-NUCLEO-IKS01A3)

## Le code MicroPython

**Vous pouvez télécharger les scripts MicroPython de ce tutoriel (entre autres) en cliquant [ici](../../../assets/Script/IKS01A3.zip)**

Le fichier *lps22.py* est la bibliothèque contenant les classes pour gérer ce capteur. Il doit être copié dans le répertoire du disque USB virtuel associé à la NUCLEO-WB55 : *PYBFLASH*.

Éditez maintenant le script *main.py* et copiez-y le code qui suit :

``` python
# Objet du script :
# Mesure la température et la pression toutes les cinq secondes. 
# Rapporte la pression au niveau de la mer sur la base de l'altitude du lieu.
# Cet exemple nécessite un baromètre MEMS LPS22HH.

from machine import I2C # Pilote du bus I2C
import lps22 # Pilote du baromètre
from time import sleep_ms # Pour temporiser

local_elevation = const(470) # en mètres

# Pression rapportée au niveau de la mer
def SeaLevelPressure(pressure, altitude):
	return pressure * pow(1.0 - (altitude * 2.255808707E-5), -5.255)

# On utilise l'I2C n°1 de la carte NUCLEO-WB55 pour communiquer avec le capteur
i2c = I2C(1)

# Pause d'une seconde pour laisser à l'I2C le temps de s'initialiser
sleep_ms(1000)

# Instanciation du capteur
sensor = lps22.LPS22(i2c)

# Instanciation des LED
led_blue = pyb.LED(1)
led_green = pyb.LED(2)
led_red = pyb.LED(3)

while True:

	# Mesure de température
	temp = round(sensor.temperature())
	
	# Mesure de pression
	press = round(SeaLevelPressure(sensor.pressure(),local_elevation))

	print("Température : " + str(temp) + "°C, Pression : " + str(press) + "hPa, ", end = '')

	if temp > 25 :
		led_red.on()
		print("Chaud")
	elif temp > 17 and temp <= 25 :
		led_green.on()
		print("Confortable")
	else:
		led_blue.on()
		print("Froid")

	# Onéteint les LED
	led_red.off()
	led_green.off()
	led_blue.off()

	# Temporisation
	sleep_ms(5000)
```

Vous pouvez à présent lancer le script avec la combinaison de touches CTRL + D dans le terminal série et observer les données de température et d'humidité mesurées par le capteur, ainsi que le comportement des LED lorsque la température change :

<br>
<div align="left">
<img alt="Capteur LPS22HH, mesures simples" src="images/LPS22HH_run.jpg" width="500px">
</div>
<br>

## Programmer un altimètre avec le LPS22HH

Le baromètre intégré au LPS22HH est très précis et peut être utilisé pour estimer l'altitude ; nous allons donc montrer comment programmer un altimètre avec ce MEMS. La discussion et l'exemple qui suivent sont inspirés de [cette source](https://www.best-microcontroller-projects.com/bmp280.html#L1080).

Les *altimètres barométriques* ne souffrent pas de la concurrence du GPS, ils sont majoritairement utilisés dans les instruments de vol des avions et par les alpinistes. On pourrait penser que l'altitude fournie par un GPS est plus précise que celle déduite d'un baromètre, mais, comme toujours ce n'est pas aussi simple. Les deux technologies ont des limites et des atouts qu'il convient de connaître pour sélectionner la plus appropriée selon votre projet.

**L'erreur verticale rapportée par le système GPS** peut atteindre *jusqu'à 45 mètres* du fait de perturbations diverses !<br>
Mais ce n'est pas tout. Pour déterminer votre altitude au-dessus du niveau de la mer ou du sol, le GPS se réfère à un modèle pour la forme de la surface terrestre, le plus utilisé étant WGS 84 (World Geodetic System - créé en 1984). Avec WGS 84 la Terre est approximée par un *sphéroïde* qui, comme toute approximation, introduit des erreurs. Ainsi, au Royaume Uni, WFS 84 vous placera en moyenne 70 m au-dessus du sol et en Inde, 100 m au-dessous !

Le principal avantage du GPS, c'est que les informations qu'il rapporte ne sont pas perturbées par les conditions météo. Si vous mesurez l'altitude en un lieu donné par beau temps, vous obtiendrez peu ou prou le même résultat qu'un jour de tempête. Et, bien évidemment, si vous fournissez à votre récepteur GPS une carte d'élévation locale précise (une carte topographique IGN par exemple) pour se substituer à WGS 84, il vous donnera l'altitude exacte déterminée à partir la position (x, y) qu'il obtient sur celle-ci.

La principale limitation des récepteurs GPS pour les applications embarquées est *leur consommation*. Un module GPS a besoin d'un courant de 45 mA alors qu'un baromètre MEMS ne nécessitera que 4 µA ; cela fait quand même un rapport 11250 en défaveur du GPS si on ne s'intéresse qu'à la détermination de l'altitude !

Enfin, souvenez-vous qu'un récepteur GPS ne fonctionnera correctement que s'il est *à l'extérieur* et/ou équipé d'une antenne assez efficace pour recevoir les signaux d'au moins *quatre satellites* de sa constellation. C'est une contrainte très forte. Les altimètres barométriques, eux, fonctionnent aussi bien en intérieur qu'en extérieur, y compris dans des espaces clos et métalliques (à condition qu'ils ne soient pas pressurisés, comme l'est l'intérieur d'un avion de ligne).

**L'erreur verticale déduite d'un baromètre MEMS** est bien inférieure aux 45 mètres du GPS. Dans le cas du LPS22HH, la précision absolue est de 0.5 hPa, ce qui correspond à +/- 6 mètres d'erreur. Mais il est possible d'être encore plus précis en calibrant le baromètre avec la pression rapportée au niveau de la mer mise à disposition par les aérodromes de votre région.

Un altimètre barométrique ne présente cependant pas que des avantages sur le GPS. Puisqu'il déduit l'altitude de la pression atmosphérique (i.e. du poids de la colonne d'air au-dessus de nos têtes) il est fortement perturbé par les conditions météorologiques. Si la météo devient mauvaise, la pression chutera et l'altimètre indiquera des valeurs surestimées. C'est pour compenser les variations rapides de la météo que les alpinistes recalibrent régulièrement leurs altimètres par référence à une carte topographique.

### Le code MicroPython

**Vous pouvez télécharger les scripts MicroPython de ce tutoriel (entre autres) en cliquant [ici](../../../assets/Script/IKS01A3.zip)**

Voici le script *main.py* qui permettra de mesurer l'altitude avec le LPS22HH, en utilisant la "bonne formule" et la valeur du moment pour la pression de référence au niveau de la mer du lieu, donnée dans _LocalSeaLevelPress_ et récupérée sur un site Internet. Cette formule donne une estimation correcte de l'altitude locale *à quelques dizaines de centimètres près !* Bien sûr, cette  précision exceptionnelle ne peut être maintenue que si on met à jour *LocalSeaLevelPress* tout au long de la journée.

``` python
# Objet du script :
# Mesure l'altitude toutes les cinq secondes.
# Cet exemple utilise comme paramètre la valeur de la pression atmosphérique rapportée au niveau de la
# mer pour le lieu où vous vous trouvez au cours de la dernière heure (variable LocalSeaLevelPress).
# Cette valeur n'est pas constante et il sera nécessaire de l'ajuster d'heure en heure pour intégrer 
# l'évolution des conditions météorologiques locales et les compenser dans l'estimation de l'altitude.
# URL d'un site Internet qui donne les valeurs de LocalSeaLevelPress toutes les 10 minutes en France :
# https://www.infoclimat.fr/cartes/observations-meteo/temps-reel/pression-au-niveau-de-la-mer/france.html
# Cet exemple nécessite un baromètre MEMS LPS22HH.

from machine import I2C
import lps22
from time import sleep_ms

# Pression au niveau de la mer dans la région "en ce moment"
LocalSeaLevelPress = 1028 # en hPa

# Calcul de l'altitude
def getAltitude(pressure, sea_lvl_press ):
	return 44330.0*(1-pow((pressure)/(sea_lvl_press),1.0/5.255));
	
# On utilise l'I2C n°1 de la carte NUCLEO-WB55 pour communiquer avec le capteur
i2c = I2C(1)

# Pause d'une seconde pour laisser à l'I2C le temps de s'initialiser
sleep_ms(1000)

# Instanciation du capteur
sensor = lps22.LPS22(i2c)

while True:

	# Temporisation de 5 secondes
	sleep_ms(5000)

	# Calcul de l'altitude intégrant la correction de pression
	altitude = getAltitude(sensor.pressure(), LocalSeaLevelPress)

	# Affichage sur le port série de l'USB USER
	print("Altitude : %.1f m" % altitude)
```


Vous pouvez à présent lancer le script avec la combinaison de touches CTRL + D dans le terminal série et lire la valeur estimée de l'altitude à votre emplacement :

<br>
<div align="left">
<img alt="Capteur LPS22HH, altimètre" src="images/LPS22HH_alti.jpg" width="500px">
</div>
<br>

## Pour aller plus loin

Une amélioration évidente de ce projet consisterait à ajouter à notre NUCLEO-WB55 une connectivité BLE (ou Wi-Fi) afin qu’elle récupère la pression locale au niveau de la mer régulièrement. Ceci nécessite la programmation d’un autre système capable d’effectuer des requêtes http sur un site Internet tel que www.infoclimat.fr ainsi que leur post-traitement et leur émission avec un protocole RF ; un Raspberry Pi semble tout indiqué.
