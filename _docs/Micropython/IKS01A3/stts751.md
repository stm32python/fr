---
title: Mise en œuvre du capteur de température de précision STTS751
description: Tutoriels pour la mise en œuvre du capteur de température de précision STTS751 avec MicroPython
---

# Mise en œuvre du capteur de température de précision STTS751

Cet exemple montre comment mesurer la température (en degrés Celsius) toutes les secondes à l'aide du capteur MEMS [STTS751](https://www.st.com/content/st_com/en/products/mems-and-sensors/temperature-sensors/stts751.html) de STMicroelectronics.

## Matériel requis

1. La carte NUCLEO-WB55
2. Une carte d'extension I2C équipée du capteur MEMS STTS751 (par exemple la X-NUCLEO-IKS01A3)

## Le code MicroPython

**Vous pouvez télécharger les scripts MicroPython de ce tutoriel (entre autres) en cliquant [ici](../../../assets/Script/IKS01A3.zip)**

Le fichier *stts751.py* est la bibliothèque contenant les classes pour gérer ce capteur. Il doit être copié dans le répertoire du disque USB virtuel associé à la NUCLEO-WB55 : *PYBFLASH*.

Éditez maintenant le script *main.py* et copiez-y le code qui suit :

``` python
# Objet du script :
# Mesure de température toutes les cinq secondes.
# Allume ou éteint les LED de la carte selon les valeurs des températures, les affiche sur le port série
# Cet exemple nécessite un capteur MEMS de température STTS751.

from machine import I2C # Pilote du bus I2C
import stts751 # Pilote du capteur de température
from time import sleep_ms # Pour temporiser

# On utilise l'I2C n°1 de la carte NUCLEO-W55 pour communiquer avec le capteur
i2c = I2C(1)

# Pause d'une seconde pour laisser à l'I2C le temps de s'initialiser
sleep_ms(1000)

# Liste des adresses I2C des périphériques présents
print("Adresses I2C utilisées : " + str(i2c.scan()))

# Instance du capteur
sensor = stts751.STTS751(i2c)

# Instance des LED
led_blue = pyb.LED(1)
led_green = pyb.LED(2)
led_red = pyb.LED(3)

while True: # Boucle sans clause de sortie

	# Temporisation de cinq secondes
	sleep_ms(5000)
	
	# Mesure de la température
	temp = sensor.temperature()

	# Affichage de la température
	print("Température %.1f°C " %temp, end='')

	# Affichage du ressenti
	if temp > 25 :
		led_red.on()
		print("(Chaud)")
	elif temp > 18 and temp <= 25 :
		led_green.on()
		print("(Confortable)")
	else:
		led_blue.on()
		print("(Froid)")

	# On éteint les LED
	led_red.off()
	led_green.off()
	led_blue.off()
```

Vous pouvez à présent lancer le script avec la combinaison de touches CTRL + D dans le terminal série et lire la valeur mesurée de la température :

<br>
<div align="left">
<img alt="Capteur STTS751, température" src="images/STTS751_temp.jpg" width="500px">
</div>
<br>
