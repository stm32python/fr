---
title: Mise en œuvre de l'accéléromètre LIS2DW12
description: Tutoriels pour la mise en œuvre de l'accéléromètre LIS2DW12 avec MicroPython
---

# Mise en œuvre de l'accéléromètre LIS2DW12

Cet exemple montre comment mesurer une accélération selon trois axes à l'aide du capteur MEMS [LIS2DW12](https://www.st.com/content/st_com/en/products/mems-and-sensors/accelerometers/lis2dw12.html) de STMicroelectronics.

Il consiste à allumer une LED selon l’axe sur lequel l’accélération de la gravité est détectée. Le vecteur accélération est décomposé sur un trièdre (x, y, z) orthogonal précisé sur [cette page](index).

Voici la configuration des axes de l’accélération et des LED que nous leur associons :

*   Accélération détectée selon l'axe x : LED verte allumée
*   Accélération détectée selon l'axe y : LED bleue allumée
*   Accélération détectée selon l'axe z : LED rouge allumée

## Matériel requis

1. La carte NUCLEO-WB55
2. Une carte d'extension I2C équipée du capteur MEMS LIS2DW12 (par exemple la X-NUCLEO-IKS01A3)

## Le code MicroPython

**Vous pouvez télécharger les scripts MicroPython de ce tutoriel (entre autres) en cliquant [ici](../../../assets/Script/IKS01A3.zip)**.

Le fichier *lis2dw12.py* est la bibliothèque contenant les classes pour gérer l'accéléromètre.
Il doit être copié dans le répertoire du disque USB virtuel associé à la NUCLEO-WB55 : *PYBFLASH*.

Éditez maintenant le script *main.py* et copiez-y le code qui suit :

``` python
# Objet du script :
# Mesure les accélérations suivant 3 axes orthogonaux (en mg) toutes les demi secondes. 
# Allume ou éteint les LED de la carte selon les valeurs des accélérations
# Cet exemple nécessite un accéléromètre MEMS LIS2DW12 

from machine import I2C
import lis2dw12  # Pilote de l'accéléromètre
from time import sleep_ms # Pour temporiser

# On utilise l'I2C n°1 de la carte NUCLEO-WB55 pour communiquer avec le capteur
i2c = I2C(1)

# Pause d'une seconde pour laisser à l'I2C le temps de s'initialiser
sleep_ms(1000)

# Instanciation de l'accéléromètre
accelerometer = lis2dw12.LIS2DW12(i2c)

led_blue = pyb.LED(1)
led_green = pyb.LED(2)
led_red = pyb.LED(3)

while True:

	# Temporisation de 500 millisecondes
	sleep_ms(500)

	# Si la valeur absolue de l'accélération sur l'axe x est supérieure à 700 mg alors
	if abs(accelerometer.x()) > 700 :
		led_green.on()
		print("LED verte allumée")

	# Si la valeur absolue de l'accélération sur l'axe y est supérieure à 700 mg alors
	if abs(accelerometer.y()) > 700 :
		led_blue.on()
		print("LED bleue allumée")

	# Si la valeur absolue de l'accélération sur l'axe z est supérieure à 700 mg alors
	if abs(accelerometer.z()) > 700 :
		led_red.on()
		print("LED rouge allumée")

	# Eteint toutes les LED
	led_red.off()
	led_green.off()
	led_blue.off()
```

Une fois le script lancé avec *[CTRL]-[D]*, faites varier l'orientation du module  et observez les messages et le comportement des LED.

# Programmer un inclinomètre

Avec un peu de trigonométrie, il est possible de programmer un *inclinomètre* à l'aide d'un accéléromètre. Le principe est le suivant : lorsque l'accéléromètre est immobile (ou en mouvement rectiligne uniforme sans vibrations) et que son plan est horizontal, il mesure exactement 1g, l'accélération de la pesanteur, suivant son axe *z* (qui coïncide avec la verticale). Si le plan de l'accéléromètre dévie de l'horizontale, alors l'accélération de la gravité terrestre ne sera plus colinéaire à l'axe *z*.<br>
Le détail des calculs est donné dans **la note d'application de Freescale AN3461**. Nous avons utilisé [la révision 2 de cette note](AN3461_Rev2.pdf) pour notre exemple. Si vous souhaitez améliorer cet algorithme, vous trouverez une approche plus élaborée dans la [révision 6, disponible ici](AN3461_Rev6.pdf). 

## Le code MicroPython

**Vous pouvez télécharger les scripts MicroPython de ce tutoriel (entre autres) en cliquant [ici](../../../assets/Script/IKS01A3.zip)**

Copiez  *LIS2DW12.py* dans le répertoire du disque USB virtuel associé à la NUCLEO-WB55 : *PYBFLASH*.
Éditez maintenant le script *main.py* :

``` python
# Objet du script :
# Mesure les accélérations suivant 3 axes orthogonaux (en mg) toutes les 20 millisecondes. 
# Utilise cette mesure pour évaluer l'inclinaison du plan de l'accéléromètre par rapport au plan horizontal (tilt)
# Source : Freescale AN3461 rev 2 (page 4)
# Cet exemple nécessite un accéléromètre MEMS LIS2DW12.

from machine import I2C # Pilote pour le bus I2C
import lis2dw12 # Pilote de l'accéléromètre
from time import sleep_ms # Pour temporiser
from math import sqrt, atan, pi # Fonctions et constantes trigonométriques

# Facteur de conversion entre les radians et les degrés pour les angles
RadToDeg = 180 / pi

# On utilise l'I2C n°1 de la carte NUCLEO-WB55 pour communiquer avec le capteur
i2c = I2C(1) 

# Pause d'une seconde pour laisser à l'I2C le temps de s'initialiser
sleep_ms(1000)

# Instanciation de l'accéléromètre
accelerometer = lis2dw12.LIS2DW12(i2c)

# Valeurs initiales des angles
phi = 0
rho = 0
theta = 0
last_phi = phi
last_rho = rho
last_theta = theta

# Seuil de détection des mouvements (°)
ANGLE_TH = (1.5) # Un degré et demi

while True:

	# Mesure des accélération
	ax = accelerometer.x()
	ay = accelerometer.y()
	az = accelerometer.z()

	# Angle entre l'axe des x et le sol
	denom = sqrt(ax*ax + az*az)
	if denom: # équivalent à if denom != 0:
		phi = atan(ay/denom) * RadToDeg

	# Angle entre l'axe des y et le sol
	denom = sqrt(ay*ay + az*az)
	if denom: # équivalent à if denom != 0:
		theta = atan(ax/denom) * RadToDeg
			
	# Angle entre l'axe des z et la verticale
	if az:  # équivalent à if az != 0
		rho = atan(sqrt(ax*ax + ay*ay)/az) * RadToDeg

	# Si l'un au moins des trois angles a varié de plus que ANGLE_TH degrés depuis la dernière mesure
	# met à jour l'affichage
	if abs(rho-last_rho) > ANGLE_TH or abs(theta-last_theta) > ANGLE_TH or abs(phi-last_phi) > ANGLE_TH:
		print("Angle entre l'axe des x et le plan horizontal : %d°" % phi)
		print("Angle entre l'axe des y et le plan horizontal : %d°" % theta)
		print("Angle entre l'axe des z et la direction verticale : %d°" % rho)
		print("")

	# Mémorisation des angles pour l'itération suivante 
	last_phi = phi
	last_rho = rho
	last_theta = theta
	
	# Temporisation de 20 ms
	sleep_ms(20)
```

## Affichage sur le terminal série de l'USB User

Une fois le script lancé avec *[CTRL]-[D]*, faites varier l'orientation du module autour des axes `Ox`, `Oy` et `Oz`. Observez les valeurs affichées pour les différents angles :

<div align="left">
<img alt="Axes LIS2DW12" src="images/LIS2DW12_axes_angles.jpg" width="500px">
<img alt="Sortie tilt LIS2DW12" src="images/LIS2DW12_clinometer_output.png" width="420px">
</div>
