---
title: Guide de démarrage rapide sous Windows avec la NUCLEO-L476
description: Guide de démarrage rapide sous Windows pour la carte NUCLEO-L476

---
# Démarrer sous Windows avec la NUCLEO-L476

Dans cette section figurent les instructions pour installer et tester le firmware MicroPython [**distribué dans la page de téléchargements de notre site**](../../Micropython/Telechargement) pour la carte [NUCLEO-L476](../../Kit/nucleo_l476rg) à l'aide d'un environnement informatique **Microsoft Windows**. Pour en apprendre plus sur le firmware MicroPython, vous pouvez consultez [la section que nous lui avons dédiée](../firmware/index).

**NB :** [Le site officiel de la fondation MicroPython](https://MicroPython.org/download/) propose des firmwares pour un grand nombre d'autres cartes de STMicroelectronics, que vous pourrez installer selon les mêmes procédures que celles décrites ci-après.

Plus précisément, nous allons vous expliquer comment :
- Programmer le firmware MicroPython dans le microcontrôleur (MCU) de la carte NUCLEO-L476 (un STM32L476RG).
- Utiliser le terminal de communication UART [**PuTTY**](https://www.putty.org/) pour communiquer avec ce firmware.
- Exécuter sur la NUCLEO-L476 un premier script / programme MicroPython.

## Programmation du firmware MicroPython dans la NUCLEO-L476

Commencez par connecter un câble micro USB sur le port ***ST-LINK*** de la NUCLEO-L476 (vous aurez besoin d’un câble USB vers mini-USB). L'autre extrémité du câble doit bien sûr être connectée sur votre PC :

<br>
<div align="left">
<img alt="Nucleo_L476RG_Config_STL" src="images/Nucleo_L476_Config_STL.png" width="300px">
</div>
<br>

 Si la configuration des cavaliers sur la carte est correcte (normalement il s'agit de la configuration par défaut), la carte et son module ST-Link devraient se mettre sous tension.

**Nous pouvons à présent installer le dernier firmware MicroPython à jour sur votre NUCLEO-L476**. La méthode la plus sûre pour cela consiste à :

1. Télécharger le firmware le plus récent pour votre carte, **au format .hex**, directement depuis [micropython.org](https://micropython.org/download/) ou depuis  [notre page de téléchargement](../Telechargement).
2. Installer [STM32CubeProgrammer](../../tools/cubeprog/index) en suivant les instructions sur le site de STMicroelectronics.
3. Programmer le firmware MicroPython dans le MCU STM32L476RG selon la procédure indiquée [ici](../../tools/cubeprog/cube_prog_firmware_stm32).

**NB :** Il existe une alternative nettement plus simple pour programmer le firmware MicroPython, en ligne de commande, avec l'utilitaire Windows **robocopy**, présentée  [ici](../../tools/robocopy/index).

**Une fois la programmation du firmware terminée**, redémarrez la carte NUCLEO en appuyant sur son bouton "Reset" (noir), ou encore en débranchant - rebranchant le câble USB.

**Votre carte NUCLEO est maintenant capable d'interpréter un fichier ".py".**

## Rédiger votre script MicroPython avec Notepad++

Nous allons voir dans cette partie comment rédiger avec Notepad++ un script MicroPython pour notre carte NUCLEO-L476. Lancez Notepad++ après l'avoir installé (vous pouvez aussi utiliser la version portable). Créez sur votre PC un fichier ***main.py*** et copiez à l'intérieur le contenu suivant :

```python
for i in range(1, 11):
    print("MicroPython est génial,", i, "fois")
```
Sauvegardez ce fichier *main.py* sur votre PC, dans le dossier qui vous convient.

Notepad++ est un outil très efficace pour rédiger des programmes. Il reconnaît automatiquement les scripts Python et améliore leur lisibilité en y ajoutant des couleurs. Cependant, sa gestion par défaut des tabulations n'est pas très pratique et nous vous conseillons de la modifier selon les instruction de **l'annexe 1**, plus bas.

Ce premier script affichera 10 fois le message "MicroPython est génial" avec le numéro du message à travers le port série du connecteur USB ST-LINK de la carte NUCLEO-L476.<br>
**Il faut à présent le transférer dans la NUCLEO-L476**.

## Transférer des scripts MicroPython dans la mémoire flash du MCU STM32L476RG

Après installation du firmware MicroPython, celui-ci crée un espace de stockage dans la mémoire flash du microcontrôleur STM323L476RG de la NUCLEO-L476. Cet espace contient un système de fichiers, *PYBFLASH*, qui est destiné à contenir les scripts MicroPython que la carte va exécuter (tout ceci est expliqué dans notre article sur le firmware MicroPython pour STM32, [ici](../firmware/index)).
 
La NUCLEO-WB55, qui dispose de deux ports USB, expose directement le contenu de *PYBFLASH* comme un dossier dans l'Explorateur Windows (voir [cet article](../install_win_wb55/index)), ce qui permet de copier-coller directement nos programmes dedans. Mais la NUCLEO-L476 n'offre pas cette facilité et **[Le logiciel Ampy](../../tools/rshell_ampy/index) est nécessaire pour transférer nos scripts dans *PYBFLASH***.

 - Commencez par **installer Ampy** comme expliqué sur [**cette page**](../../tools/rshell_ampy/index).
 - Ceci fait, **identifiez  le port COM virtuel que Windows a attribué à votre carte**. L'**annexe 3**, à la fin de ce tutoriel, explique comment faire.
 - Une fois ce port COM identifié (COM8 dans notre cas), ouvrez un terminal dans le dossier Windows contenant le fichier *main.py* à transférer et tapez, puis validez avec *[Entrée]* la ligne de commande appropriée :

 ```console
ampy -p COM8 put main.py
```

Cette commande va transférer le fichier *main.py* dans la mémoire flash du MCU de la carte NUCLEO connectée sur le port série **COM8** de votre PC. Répétez cette opération autant de fois que nécessaire pour transférer d'autres fichiers *.py* éventuellement requis par votre application.

**NB : Pensez de temps en temps à "faire le ménage"** dans la mémoire flash en effaçant les anciens fichiers *.py* avec la commande *rm*. Consultez la page sur [Ampy](../../tools/rshell_ampy/index) pour en savoir un peu plus.

## Exécuter des scripts MicroPython avec PuTTY

Il ne reste à présent plus qu'à vous connecter à **l'interface REPL** ([voir cette page](../firmware/index)) pour lancer l'exécution de notre script. Pour cela :

**(1) Démarrez PuTTY**. Dans la première fenêtre, intitulée *PuTTY Configuration*, choisissez l'option *Serial*. Dans la boîte *Serial line* recopiez *COM8* et dans la boite *Speed* indiquez *115200*. **Vous devriez avoir ceci :**

<br>
<div align="left">
<img alt="Configuration de PuTTY (1)" src="images/putty_config_1.jpg" width="400px">
</div>
<br>

**(2) Cliquez sur le bouton *"Open"*** en bas. Un terminal intitulé *COM8 - PuTTY* s'ouvre. Sélectionnez le et tapez la commande clavier *[CTRL]*+*[C]*, puis *[CTRL]*+*[D]* pour lancer le script *main.py*. **Vous devriez avoir ceci :**

<br>
<div align="left">
<img alt="Configuration de PuTTY (4)" src="images/putty_config_4.jpg" width="500px">
</div>
<br>

**Notre script a bien fonctionné !**

Nous vous conseillons de **sauvegarder la configuration de PuTTY par défaut pour votre carte NUCLEO**. Ceci est expliqué dans **l'annexe 2**, un peu plus loin.

## Annexe 1 : Configurer Notepad++ pour MicroPython

Notepad++ n'est pas configuré par défaut pour utiliser des tabulations ; il les simule par plusieurs espaces consécutifs. Pour le codage en MicroPython, il est préférable de modifier ce comportement.

Pour ce faire, on commence par forcer l'affichage des espaces et tabulations, en allant dans le menu "View" (ou "Affichage" pour la version française), puis dans le sous-menu "Show Symbol" (VF : "Symboles spéciaux" où on coche les options "Show White Space and TAB" (VF : "Afficher les blancs et les tabulations") et "Show Indent guide" (VF : "Afficher le guide d'indentation").

<br>

![Image](images/Menu_1.png)

<br>

Ensuite, on demande à Notepad++ d’effectivement remplacer les espaces par des tabulations dans les scripts Python, en allant dans le menu “Settings” (VF : “Paramètres”), puis dans la boite “Preferences” (VF : “Préférences”). Dans la colonne de gauche il faut sélectionner “Language” (VF : “Langage”). Dans l’encadré à droite “Tab Setting” (VF : “Tabulations”) on sélectionne “python” et enfin dans l’encadré “Use default value” (VF : “Valeur par défaut”) il faut désactiver la case “Replace by space” (VF : “Insérer des espaces”).

<br>

![Image](images/Menu_2.png)

<br>

## Annexe 2 : Configurer PuTTY pour MicroPython

L'utilisation de PuTTY pour lancer des scripts MicroPython peut vite devenir pénible si vous ne prenez pas une petite minute pour sauvegarder sa configuration.

Supposons par exemple que votre carte NUCLEO soit tout le temps la même, et que le port COM qui lui est attribué par Windows soit COM8 (il ne changera pas par la suite) avec un baudrate de 115200. Si vous ne souhaitez pas avoir à renseigner de nouveau, à chaque lancement de PuTTY, le port COM et le baudrate, suivez cette procédure :


**(1) Démarrez PuTTY**. Dans la première fenêtre, intitulée *PuTTY Configuration*, choisissez l'option *Serial*. Dans la boîte *Serial line* recopiez *COM8* et dans la boite *Speed* indiquez *115200*. **Vous devriez avoir ceci :**

<br>
<div align="left">
<img alt="Configuration de PuTTY (1)" src="images/putty_config_1.jpg" width="400px">
</div>
<br>

**(2) Dans la colonne de gauche** de *PuTTY Configuration*, sélectionnez *Windows / Behaviour* et assurez vous que la case *"Warn before closing Windows"* n'est pas cochée. **Vous devriez avoir ceci :**

<br>
<div align="left">
<img alt="Configuration de PuTTY (2)" src="images/putty_config_2.jpg" width="400px">
</div>
<br>

Cette action vous évitera aussi une crise de nerfs à long terme, en éradiquant l'absurde et insupportable message de confirmation que vous envoie PuTTY à chaque fermeture ...

**(3) Dans la colonne de gauche** de *PuTTY Configuration*, sélectionnez *Session*. Dans la section *Saved Sessions* à droite, sélectionnez *Default Settings* puis cliquez sur le bouton *Save*. **Vous devriez avoir ceci :**

<br>
<div align="left">
<img alt="Configuration de PuTTY (3)" src="images/putty_config_3.jpg" width="400px">
</div>
<br>

**Et voilà, votre configuration par défaut est sauvegardée !**

## Annexe 3 : Identifier le port COM attribué par Windows à votre carte NUCLEO-L476

Chaque fois que vous connectez la NUCLEO-L476 à votre PC par un câble USB, *un port COM* est créé et alloué par Windows au port série virtuel exposé par le firmware MicroPython. Cette annexe explique comment identifier ce port COM sachant que Windows attribue peut être simultanément d'autres ports COM à des périphériques USB connectés au PC tels que clavier, souris, Web CAM...

Ecrivez `gestionnaire de périphériques` ( `devices manager` en anglais) dans la barre de recherche Windows, puis cliquez sur `Ouvrir` :

<br>
<div align="left">
<img alt="Port COM" src="images/port_COM.png" width="600px">
</div>
<br>

Une nouvelle fenêtre s'ouvre :

<br>
<div align="left">
<img alt="Windows Devices Manager" src="images/gestion_peripheriques.png" width="600px">
</div>
<br>

Relevez le numéro du port ***COM***. Dans l'exemple ci-dessus, la NUCLEO-L476 est branchée sur le port ***COM3***. C'est ce numéro qu'il faudra rentrer dans la ligne de commande de Ampy puis dans votre émulateur de terminal série (PuTTY, TeraTerm...).