---
title: Exécution REPL via BLE
description: Interaction avec la Read Evaluate Print Loop (REPL) de l'interpréteur MicroPython à travers le BLE
---

# Echanges de chaînes de caractères entre deux NUCLEO-WB55

Ce tutoriel montre comment envoyer des commandes à la ["Read Evaluate Print Loop" (REPL)](https://docs.micropython.org/en/latest/esp8266/tutorial/repl.html) de l'interpréteur MicroPython à travers le BLE, en utilisant le [Nordic UART Service](https://developer.nordicsemi.com/nRF_Connect_SDK/doc/latest/nrf/libraries/bluetooth_services/services/nus.html). Il s'agit d'une simple adaptation de [cette source](https://github.com/Micropython/micropython/blob/master/examples/bluetooth/ble_uart_repl.py), proposée par Damien George. Son principe consiste à "connecter" le flux de l'UART BLE virtuel sur le terminal REPL du firmware MicroPython à l'aide de [la méthode *os.dupterm()*](https://docs.micropython.org/en/latest/library/os.html). Facile à dire, mais beaucoup plus difficile à faire sans plagier le code proposé par Damien !<br>
Cette thématique plutôt technique centrée sur REPL est un peu éloignée de la plupart des autres tutoriels de ce site, pensés pour s'exécuter de façon "autonome" sur un MCU STM32, à l'image de la vaste majorité des applications embarquées. Néanmoins, avec un peu d'imagination, l'envoi de commandes Python à distance en utilisant REPL via BLE est une opportunité unique pour ajouter des fonctions complexes à un objet connecté, ou tout simplement pour travailler si vous n'avez pas de câbles USB sous la main !

## Matériel requis

 - **Central** : Un smartphone (ou une tablette) avec l'application [Bluefruit Connect](https://play.google.com/store/apps/details?id=com.adafruit.bluefruit.le.connect&hl=fr&gl=US) installée.
 - **Périphérique** : Une carte NUCLEO-WB55. **Attention**, il est possible que vous deviez mettre à jour le firmware BLE HCI de votre carte, la procédure est expliquée [ici](../../tools/cubeprog/cube_prog_firmware_ble_hci.md).


## Le script MicroPython (pour le périphérique)

> **Vous pouvez télécharger les scripts MicroPython de ce tutoriel (entre autres) en cliquant [ici](../../../assets/Script/BLE.zip)**.

Trois fichiers scripts MicroPython seront nécessaires pour le périphérique :
 - Le script permettant de construire les trames d'advertising, intitulé *ble_advertising.py*.
 - Le script permettant un connexion avec le service BLE NUS sur le périphérique, intitulé *ble_uart_peripheral.py*.
 - **Le script du programme principal**, *main.py*.

Créez donc un script *main.py* sur votre ordinateur et copiez-y le code suivant :

```python
# Preuve de concept de REPL via BLE UART.
# Testé avec l'application Android Bluefruit connect de Adafruit
# Paramétrez le caractère de fin de ligne (EoL) sur l'app à \r\n.
# Cet exemple est une simple traduction cette source, par Damien George :
# https://github.com/Micropython/micropython/blob/master/examples/bluetooth/ble_uart_repl.py

# Bilbliothèques requises
import bluetooth
import io
import os
import micropython
import machine

from ble_uart_peripheral import BLEUART

_MP_STREAM_POLL = const(3)
_MP_STREAM_POLL_RD = const(0x0001)
_DELAY_MS = const(50)
_TX_BUF_SIZE = const(100)

_timer = machine.Timer(-1)

# Ecris toutes les delay_ms millisecondes
def schedule_in(handler, delay_ms):
	def _wrap(_arg):
		handler()
	_timer.init(mode=machine.Timer.ONE_SHOT, period=delay_ms, callback=_wrap)

# Définit un flux - buffer pour respecter les contraintes de 
# la méthode dupterm()
class BLEUARTStream(io.IOBase):
	def __init__(self, uart):
		self._uart = uart
		self._tx_buf = bytearray()
#		self._uart.irq(self._on_rx)

#	def _on_rx(self):
#		# Requis pour les ESP32.
#		if hasattr(os, "dupterm_notify"):
#			os.dupterm_notify(None)

	def read(self, sz=None):
		return self._uart.read(sz)

	def readinto(self, buf):
		avail = self._uart.read(len(buf))
		if not avail:
			return None
		for i in range(len(avail)):
			buf[i] = avail[i]
		return len(avail)

	def ioctl(self, op, arg):
		if op == _MP_STREAM_POLL:
			if self._uart.any():
				return _MP_STREAM_POLL_RD
		return 0

	def _flush(self):
		data = self._tx_buf[0:_TX_BUF_SIZE]
		self._tx_buf = self._tx_buf[_TX_BUF_SIZE:]
		self._uart.write(data)
		if self._tx_buf:
			schedule_in(self._flush, _DELAY_MS)

	def write(self, buf):
		empty = not self._tx_buf
		self._tx_buf += buf
		if empty:
			schedule_in(self._flush, _DELAY_MS)

# Optimise le bytecode pour STM32
@micropython.native
def start():

	# Démarrage du service BLE UART
MPY	ble = bluetooth.BLE()
	uart = BLEUART(ble, name="MPY-NIMBLE")
	
	# Clone le terminal REPL sur le flux de l'UART BLE 
	stream = BLEUARTStream(uart)
	os.dupterm(stream)

# Lance le service BLE planifié
start()
```

### Mise en œuvre

 Glissez-déplacez les trois fichiers .py dans *PYBFLASH*, connectez un terminal PuTTY à l'interpréteur MicroPython embarqué et lancez *main.py* avec la combinaison de touches *[CTRL]-[D]*.

 Ensuite, lancez l'application *Bluefruit Connect*, connectez vous à l'objet BLE *MPY NIMBLE* :
 
<br>
<div align="left">
<img alt="Bluefruit Connect, connect" src="images/bluefruit_connect1.jpg" width="300px">
</div>
<br>

 Allez sur le module *UART* et envoyez des commandes (par exemple *help()*) à votre NUCLEO-WB55 :
 
<br>
<div align="left">
<img alt="Bluefruit Connect, modules" src="images/bluefruit_connect2.jpg" width="300px">
<img alt="Bluefruit Connect, console UART" src="images/bluefruit_connect3.jpg" width="300px">
</div>
<br>
 
 Vous constaterez que la console REPL sur PuTTY et celle affichée sur *Bluefruit Connect* sont synchronisées :

```console
>>> help()
Welcome to MicroPython!

For online help please visit http://micropython.org/help/.

Quick overview of commands for the board:
  pyb.info()    -- print some general information
  pyb.delay(n)  -- wait for n milliseconds
  pyb.millis()  -- get number of milliseconds since hard reset
  pyb.Switch()  -- create a switch object
                   Switch methods: (), callback(f)
  pyb.LED(n)    -- create an LED object for LED n (n=1,2,3,4)
                   LED methods: on(), off(), toggle(), intensity(<n>)
  pyb.Pin(pin)  -- get a pin, eg pyb.Pin('X1')
  pyb.Pin(pin, m, [p]) -- get a pin and configure it for IO mode m, pull mode p
                   Pin methods: init(..), value([v]), high(), low()
  pyb.ExtInt(pin, m, p, callback) -- create an external interrupt object
  pyb.ADC(pin)  -- make an analog object from a pin
                   ADC methods: read(), read_timed(buf, freq)
  pyb.DAC(port) -- make a DAC object
                   DAC methods: triangle(freq), write(n), write_timed(buf, freq)
  pyb.RTC()     -- make an RTC object; methods: datetime([val])
  pyb.rng()     -- get a 30-bit hardware random number
  pyb.Servo(n)  -- create Servo object for servo n (n=1,2,3,4)
                   Servo methods: calibration(..), angle([x, [t]]), speed([x, [t]])
  pyb.Accel()   -- create an Accelerometer object
                   Accelerometer methods: x(), y(), z(), tilt(), filtered_xyz()

Pins are numbered X1-X12, X17-X22, Y1-Y12, or by their MCU name
Pin IO modes are: pyb.Pin.IN, pyb.Pin.OUT_PP, pyb.Pin.OUT_OD
Pin pull modes are: pyb.Pin.PULL_NONE, pyb.Pin.PULL_UP, pyb.Pin.PULL_DOWN
Additional serial bus objects: pyb.I2C(n), pyb.SPI(n), pyb.UART(n)

Control commands:
  CTRL-A        -- on a blank line, enter raw REPL mode
  CTRL-B        -- on a blank line, enter normal REPL mode
  *[CTRL]-[C]*        -- interrupt a running program
  *[CTRL]-[D]*        -- on a blank line, do a soft reset of the board
  CTRL-E        -- on a blank line, enter paste mode

For further help on a specific object, type help(obj)
For a list of available modules, type help('modules')
```

## Source

 Nous avons repris le [Code proposé par Damien Georges sur le GitHub MicroPython.](https://github.com/Micropython/micropython/blob/master/examples/bluetooth/ble_uart_repl.py)

