---
title: Utilisation du BLE avec la carte NUCLEO-WB55
description: Utilisation du BLE avec la carte NUCLEO-WB55
---

# Utilisation du BLE avec la carte NUCLEO-WB55 

## Présentation du Bluetooth Low Energy (BLE)

Vous trouverez sur [cette page](../../Embedded/ble) une présentation du Bluetooh Low Energy (BLE). Prenez le temps d'assimiler ces informations car les protocoles du BLE sont subtilement différents des logiques "client-serveur" et "maître-esclave" plus répandues et introduisent un vocabulaire et des notions que nous utilisons dans tous les tutoriels qui suivent.

## Liste des tutoriels 

Les liens suivants donnent quelques exemples d'applications du BLE avec la carte NUCLEO-WB55. Nous rappelons que l'ensemble des scripts présentés ou utilisés par la suite pourront être récupérés sur la [**page Téléchargements**](../Telechargement).

La colonne *Central* précise si ce dernier est une NUCLEO-WB55 ou bien un smartphone équipé soit de l'application ST BLE Sensor, soit d'une application Android réalisée avec MIT App Inventor. Le *Peripheral* (fr : périphérique) est TOUJOURS une NUCLEO-WB55. Entre autres possibilités, le central peut aussi être un ordinateur personnel (MAC ou PC) ou encore un micro-ordinateur de type Raspberry Pi exécutant le système d'exploitation Linux. Nous ajouterons probablement des exemples correspondants dans le futur.<br>

<br>

|**Central**|**Peripheral**|**Protocole**|**Description et hyperlien**|**Standard**|
|:-|:-:|:-:|:-:|:-:|
|ST BLE Sensor (smartphone)|NUCLEO-WB55|GAP puis GATT|[Publication de la température et contrôle d'une LED](STBLESensor)|Blue-ST|
|ST BLE Sensor (smartphone)|NUCLEO-WB55|GAP puis GATT|[Station environnementale](StationEnvBlueST)|Blue-ST|
|MIT App Inventor (smartphone)|NUCLEO-WB55|GAP puis GATT|[Publication de la température et contrôle d'une LED](MITAppInventor_1)|Blue-ST|
|MIT App Inventor (smartphone)|NUCLEO-WB55|GAP puis GATT|[Publication de la température et de l'humidité](MITAppInventor_2)|Bluetooth SIG|
|MIT App Inventor (smartphone)|NUCLEO-WB55|GAP puis GATT|[Echange de chaînes de caractères](MITAppInventor_3)|Nordic UART Service|
|NUCLEO-WB55|NUCLEO-WB55|GAP|[Emission et lecture de messages hors-connexion](BLEGAP)|Bluetooth SIG|
|NUCLEO-WB55|NUCLEO-WB55|GAP puis GATT|[Echange de la température et de l'humidité](BLESIG)|Bluetooth SIG|
|NUCLEO-WB55|NUCLEO-WB55|GAP puis GATT|[Echange de chaînes de caractères](BLEUART)|Nordic UART Service|
|Bluefruit Connect (smartphone)|NUCLEO-WB55|GAP puis GATT|[Exécution REPL via BLE](BLEREPL)|Nordic UART Service|
