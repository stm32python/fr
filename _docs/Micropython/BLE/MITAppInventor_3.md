---
title: Echange de chaînes de caractères entre MIT App Inventor et une NUCLEO-WB55 
description: Exemple de mise en œuvre du service UART BLE de Nordic Semiconductors entre une NUCLEO-WB55 et l'application MIT App Inventor sur un smartphone.
---

# Echange de chaînes de caractères entre MIT App Inventor et une NUCLEO-WB55

Ce tutoriel montre comment mettre en œuvre le service UART BLE de Nordic Semiconductors (NUS) entre une carte NUCLEO-WB55 (le périphérique) et une application sur un smartphone (le central) sous Android, réalisée avec MIT App Inventor.

Le schéma de principe est le suivant :

<br>
<div align="left">
<img alt="MIT APP Inventor UART use case" src="images/MITTAPPInvUART.jpg" width="700px">
</div>
<br>

## Avertissement

Le comportement des applications réalisées avec MIT App Inventor peut être **très différent** d'une machine Android à une autre. 
Aussi, si notre application ne fonctionne pas sur votre smartphone, testez là sur un autre terminal en priorité avant d'y rechercher des erreurs. Notamment **[la version actuelle (20200828) de l'extension BluetoothLE pour App Inventor](http://iot.appinventor.mit.edu/#/bluetoothle/bluetoothleintro) ne fonctionne pas sur Android 12 et plus**. Son développeur travaille sur le sujet (voir [ce lien](https://community.appinventor.mit.edu/t/android-permissions-ble-android-12/66469/30)), donc, wait and see... Une solution de contournement consiste à ajouter dans l'application App Inventor les blocs pour activer l'accès au Bluetooth, comme expliqué [ici](https://community.appinventor.mit.edu/t/android-permissions-ble-android-12/66469/3).<br>
Nous rappelons aussi que cette extension indispensable à l'utilisation du BLE **ne marche pas sur iOS**.

## Matériel requis

1. La carte NUCLEO-WB55
2. Un smartphone Android pour y installer l'application créée avec MIT App Inventor

**Attention**, il est possible que vous deviez mettre à jour le firmware BLE HCI de votre carte NUCLEO-WB55, la procédure est expliquée [ici](../../tools/cubeprog/cube_prog_firmware_ble_hci.md).


## Les codes MicroPython pour le périphérique

> **Vous pouvez télécharger les scripts MicroPython de ce tutoriel (entre autres) en cliquant [ici](../../../assets/Script/BLE.zip)**.

Pour ce tutoriel, l'implémentation BLE nécessite **deux** scripts :
1. **ble_advertising.py**, une bibliothèque de fonctions qui seront utilisées pour construire les trames d'avertising du protocole GAP, lancé pour et avant la connexion à un central.
2. **main.py**, le script contenant le programme principal mais surtout *la classe BLEUART* qui implémente le service UART BLE de Nordic Semiconductors. Ce script est adapté de [celui disponible ici](https://GitHub.com/Micropython/MicroPython/blob/master/examples/bluetooth/ble_uart_peripheral.py). 

Éditez le script *main.py* contenu dans le répertoire du disque USB virtuel associé à la NUCLEO-WB55 : *PYBFLASH* et copiez-y le code suivant :

``` python
# Objet du script : Mise en œuvre du service UART BLE de Nordic Semiconductors (NUS pour "Nordic UART Service").
# Sources : https://GitHub.com/Micropython/MicroPython/blob/master/examples/bluetooth/ble_uart_peripheral.py

import bluetooth  # Classes pour le bluetooth et le BLE
from ble_advertising import advertising payload # Pour construire la trame d'advertising
from binascii import hexlify # Convertit une donnée binaire en sa représentation hexadécimale

# Constantes requises par le service BLE UART

_IRQ_CENTRAL_CONNECT = const(1)
_IRQ_CENTRAL_DISCONNECT = const(2)
_IRQ_GATTS_WRITE = const(3)
_FLAG_WRITE = const(0x0008)
_FLAG_NOTIFY = const(0x0010)

# Définition du service UART avec ses deux caractéristiques RX et TX

_UART_UUID = bluetooth.UUID("6E400001-B5A3-F393-E0A9-E50E24DCCA9E")

# Cette caractéristique notifiera le central des modifications que lui apportera le périphérique
_UART_TX = (
	bluetooth.UUID("6E400003-B5A3-F393-E0A9-E50E24DCCA9E"),
	_FLAG_NOTIFY, 
)

# Le central pourra écrire dans cette caractéristique
_UART_RX = (
	bluetooth.UUID("6E400002-B5A3-F393-E0A9-E50E24DCCA9E"),
	_FLAG_WRITE,  
)

_UART_SERVICE = (
	_UART_UUID,
	(_UART_TX, _UART_RX),
)

# Icône de la caractéristique : org.bluetooth.characteristic.gap.appearance.xml
_ADV_APPEARANCE_GENERIC_COMPUTER = const(128)

# Nombre maximum d'octets qui peuvent être échangés par la caractéristique RX
_MAX_NB_BYTES = const(100)

ascii_mac = None
is_connected = False

class BLEUART:

	# Initialisations
	def __init__(self, ble, name="mpy-uart", charbuf=_MAX_NB_BYTES):
		self._ble = ble
		self._ble.active(True)
		self._ble.irq(self._irq)
		self._ble.config(mtu=_MAX_NB_BYTES)

		# Enregistrement du service
		((self._tx_handle, self._rx_handle),) = self._ble.gatts_register_services((_UART_SERVICE,))

		# Définit la taille du tampon rx et active le mode "append"
		self._ble.gatts_set_buffer(self._tx_handle, charbuf, True)
		self._ble.gatts_set_buffer(self._rx_handle, charbuf, True)

		self._connections = set()
		self._rx_buffer = bytearray()
		self._handler = None
		
		# Advertising du service :
		# On peut ajouter en option services=[_UART_UUID], mais cela risque de rendre la payload de la caractéristique trop longue
		self._payload = advertising payload(name=name, appearance=_ADV_APPEARANCE_GENERIC_COMPUTER)
		self._advertise()

		# Affiche l'adresse MAC de l'objet
		dummy, byte_mac = self._ble.config('mac')
		hex_mac = hexlify(byte_mac) 
		global ascii_mac
		ascii_mac = hex_mac.decode()
		print("Adresse MAC : %s" %ascii_mac)

	# Interruption pour gérer la réception
	def irq(self, handler):
		self._handler = handler

	# Surveille les connexions afin d'envoyer des notifications
	def _irq(self, event, data):
	
		global is_connected
	
		# Si un central se connecte
		if event == _IRQ_CENTRAL_CONNECT:
			conn_handle, _, _ = data
			self._connections.add(conn_handle)
			print("Central connecté")
			is_connected = True

		# Si un central se déconnecte
		elif event == _IRQ_CENTRAL_DISCONNECT:
			conn_handle, _, _ = data
			if conn_handle in self._connections:
				self._connections.remove(conn_handle)
			print("Central déconnecté")
			is_connected = False

			# Redémarre l'advertising pour permettre de nouvelles connexions
			self._advertise()
			
		# Lorsqu'un client écrit dans une caractéristique exposée par le serveur
		# (gestion des évènements de recéption depuis le central)
		elif event == _IRQ_GATTS_WRITE:
			conn_handle, value_handle = data
			if conn_handle in self._connections and value_handle == self._rx_handle:
				self._rx_buffer += self._ble.gatts_read(self._rx_handle)
				if self._handler:
					self._handler()

	# Appelée pour vérifier s'il y a des messages en attente de lecture dans RX
	def any(self):
		return len(self._rx_buffer)

	# Retourne les catactères reçus dans RX
	def read(self, sz=None):
		if not sz:
			sz = len(self._rx_buffer)
		result = self._rx_buffer[0:sz]
		self._rx_buffer = self._rx_buffer[sz:]
		return result

	# Ecrit dans TX un message à l'attention du central
	def write(self, data):
		for conn_handle in self._connections:
			self._ble.gatts_notify(conn_handle, self._tx_handle, data)

	# Mets fin à la connexion au port série simulé
	def close(self):
		for conn_handle in self._connections:
			self._ble.gap_disconnect(conn_handle)
		self._connections.clear()

	# Pour démarrer l'advertising, précise qu'un central pourra se connecter au périphérique
	def _advertise(self, interval_us=500000):
		self._ble.gap_advertise(interval_us, adv_data=self._payload, connectable = True)

def demo_NUS():

	global is_connected
	
	import time # Pour gérer les temporisations

	# Instance du BLE et du service UART
	ble = bluetooth.BLE()
	uart = BLEUART(ble)

	# Gestion de la réception par interruptions
	def on_rx():
		print("RX: ", uart.read().decode().strip())

	# On active la réception par interruptions
	uart.irq(handler=on_rx)
	nums = [4, 8, 15, 16, 23, 42]
	i = 0
	
	# Boucle pour envoyer des messages au central
	try: # Gestion de l'interruption du clavier (*[CTRL]-[C]*)
		while True:
		
			# Si un central est connecté
			if is_connected: 
			
				# On lui envoie des messages
				i = (i + 1) % len(nums)
				sent_message = ascii_mac + "_" + str(nums[i])
				print("TX: " + sent_message)
				uart.write(sent_message + "\r\n")
							
			# Temporisation de cinq secondes
			time.sleep_ms(5000)

	# Si une interruption du clavier est interceptée, continue l'exécution du programme
	except KeyboardInterrupt:
		pass

	# Arrête le service UART
	uart.close()

# Si le nom du script est "main", exécute la fonction "demo_NUS()"
if __name__ == "__main__":
	demo_NUS()
```

## L'application MIT App Inventor pour le central

Nous ne reviendrons pas sur les étapes de création d'une application MIT App Inventor adaptée à cet exemple, vous pourrez directement la récupérer dans la section BLE de [**la page de téléchargement**](../../../assets/Script/BLE.zip) ; il s'agit du fichier *BLE_CENTRAL_NUS.aia*. Un tutoriel assez détaillé expliquant comment construire avec MIT App Inventor une application central BLE est disponible [ici](MITAppInventor_1).

## Mise en œuvre

- Commencez par déplacer les scripts *ble_advertising.py* et *main.py* dans le dossier *PYBFLASH* de la NUCLEO-WB55. Connectez-vous à celle-ci par le port USB USER avec PuTTY et lancez le programme contenu dans *main.py* comme à l'ordinaire, par la combinaison de touches *[CTRL]-[D]* dans la console.
L'adresse matérielle MAC attribuée à la carte par le protocole BLE s'affiche ; dans l'image ci-dessous il s'agit de `02 02 27 4E 25 16` (sans les espaces) :

<br>
<div align="left">
<img alt="MIT APP Inventor UART use case" src="images/MITTAPPInvUART2.png" width="500px">
</div>
<br>

- Chargez ensuite l'application *BLE_CENTRAL_NUS.aia* sur votre smartphone (voir [ce tutoriel](MITAppInventor_1)) puis lancez celle-ci. Sur l'écran du smartphone, renseignez l'adresse MAC obtenue ci-avant en rajoutant des caractères "deux points" entre les octets comme ceci : `02:02:27:4E:25:16` et appuyez sur le bouton  `Se connecter`. Vous devriez alors obtenir ceci :

<br>
<div align="left">
<img src="images/APP_INV_UART_1.png" width="400px">
<img src="images/APP_INV_UART_2.jpg" width="300px">
</div>
<br>

Chaque fois que vous tapez un message dans la boite `Message à envoyer :` de l'application smartphone (image de droite) puis que vous appuyez sur le bouton `Envoyer!` ce message est transmis à la NUCLEO-WB55 qui l'affiche dans la console de PuTTY (image de gauche). A l'inverse, vous retrouvez dans la boite `Messages reçus :` la séquence des messages émis par le script *main.py* depuis la NUCLEO-WB55.

