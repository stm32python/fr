---
title: Programmer un chenillard
description: Tutoriel pour programmer un chenillard avec MicroPython
---

# Programmer un chenillard

Ce tutoriel explique comment faire clignoter *selon une séquence cyclique* les trois diodes électroluminescentes de la carte NUCLEO-WB55 avec MicroPython. C'est ce que l'on appelle un "chenillard".
Dans tous nos tutoriels, pour éviter d'éventuelles confusions entre les acronymes français et anglo-saxons, nous ferons le choix **d'utiliser systématiquement les acronymes anglo-saxons**. Donc, nous désignerons par la suite les diodes électroluminescentes par l'acronyme **LED** (pour *Light Emitting Diode*).

## Matériel requis

La carte NUCLEO-WB55. Nous ferons clignoter ses LED 1, 2 et 3 :

<div align="left">
<img alt="LEDS" src="images/leds.jpg" width="500px">
</div>

Voici l’organisation des LED par couleur et numéro:

1. LED 1 : Bleu
2. LED 2 : Vert
3. LED 3 : Rouge

## Le code MicroPython

**Vous pouvez télécharger les scripts MicroPython de ce tutoriel (entre autres) en cliquant [ici](../../../assets/Script/NUCLEO_WB55.zip)**.

Sous MicroPython, le module **pyb.LED** permet de gérer les LED très simplement.
Éditez le script *main.py* contenu dans le répertoire du disque USB virtuel associé à la NUCLEO-WB55 : *PYBFLASH* et copiez-y le code suivant :

``` python
# Objet du script : Programmer un "chenillard"
# Exemple de configuration des GPIO pour une gestion des LED intégrées de la NUCLEO-WB55

import pyb # For access aux GPIO
from time import sleep_ms # Pour les temporisations

print( "Les LED avec MicroPython c'est facile" )

# Initialisation des LED
led_blue = pyb.LED(1) # sérigraphiée LED1 sur le PCB
led_green = pyb.LED(2) # sérigraphiée LED2 sur le PCB
led_red = pyb.LED(3) # sérigraphiée LED3 sur le PCB

# Initialisation du compteur de LED
led_counter = 0

while True: # Création d'une boucle "infinie" (pas de clause de sortie)

	if led_counter == 0:
		led_blue.on()
		led_red.off()
		led_green.off()
	elif led_counter == 1:
		led_blue.off()
		led_green.on()
		led_red.off()
	else :
		led_blue.off()
		led_green.off()
		led_red.on()

	# On veut allumer la prochaine LED à la prochaine itération de la boucle
	led_counter = led_counter + 1
	if led_counter > 2:
		led_counter = 0
	
	# Delai de 100 ms
	sleep_ms(100)
```

Vous pouvez lancer le script avec Ctrl + D sur le terminal PuTTY et observer les 3 LED qui s'allument puis s'éteignent selon la séquence LED3 -> LED2 -> LED1 -> LED3 -> LED2 ...

## Pour aller plus loin 

Vous trouverez [ici](bouton_it) une variante de ce code qui explique comment lui rajouter une interaction avec un bouton en utilisant le mécanisme des *interruptions*.
