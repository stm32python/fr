---
title: Faire clignoter plusieurs LED simultanément
description: Comment créer une application multitâche avec MicroPython
---

# Faire clignoter plusieurs LED

Ce tutoriel aborde le sujet de la programmation multitâches avec MicroPython. Bien évidemment, nous n'allons pas réaliser un système d'exploitation !
Dans un premier temps, nous nous limiterons à démontrer comment, par une gestion astucieuse du temps, et éventuellement par l'usage de *timers*, il est possible de donner l'illusion que le STM32WB55 exécute plusieurs tâches **indépendantes** simultanément. Nous avons choisi trois tâches très simples utilisant les LED intégrées à la NUCLEO-WB55 :

 - Tâche 1 : fait clignoter la LED rouge (sérigraphiée *LED3* sur le PCB) à la fréquence de 0.5 Hz
 - Tâche 2 : fait clignoter la LED verte (sérigraphiée *LED2* sur le PCB) à la fréquence de 2 Hz
 - Tâche 3 : fait clignoter la LED bleue (sérigraphiée *LED1* sur le PCB) à la fréquence de 10 Hz

Mais MicroPython permet aussi d'implémenter simplement **du véritable multitâche** avec les bibliothèques [`uasyncio`](https://docs.MicroPython.org/en/latest/library/uasyncio.html) et [`thread`](https://docs.MicroPython.org/en/latest/library/_thread.html). Nous finirons donc notre tutoriel avec un script mettant en œuvre la bibliothèque `uasyncio` (sachant que  [`thread`](https://mpython.readthedocs.io/en/master/library/Micropython/_thread.html#), plus simple d'utilisation, est encore en phase béta à la date où ce tutoriel est rédigé). 

## Méthode numéro 1 : Partage du temps avec la méthode time.ticks_ms()

> **Vous pouvez télécharger les scripts MicroPython de ce tutoriel (entre autres) en cliquant [ici](../../../assets/Script/MODULES.zip)**.

Cette première approche est directement inspirée de nombreux exemples que l'on trouve sur Internet, généralement pour Arduino.
Tout le traitement est entièrement réalisé dans la "boucle principale" (*while True:*) : 

### Le code de *main.py*

```python
# Objet du script : Faire clignoter simultanément les trois LED de la carte NUCLEO-WB55.
# Cette première approche utilise la méthode "time.ticks_ms()" qui mesure le temps écoulé 
# depuis de démarrage du script. 
# Ce script permet de comprendre le principe de base du multitâche préemptif.
# Trois tâches sont définies (ici faire clignoter différentes LED) et le microcontrôleur 
# alterne très rapidement de l'une à l'autre ce qui donne l'illusion qu'elles s'éxécutent de 
# façon simultanée.

from time import ticks_ms, ticks_diff # Pour gérer les temporisations

# Variable pour suivre le temps écoulé depuis le lancement du script
n = 0

# Variables globales pour les dates de clignotement des LED
blink1 = 0
blink2 = 0
blink3 = 0

while True: # Boucle infinie

	# On teste le temps écoulé pour chaque tâche.
	# Remarquez que les tests sur les durées ne portent pas sur une égalité (stricte) mais sur un dépassement.
	# Par exemple on écrit "if time.ticks_diff(now, blink1) > 99" et pas "if time.ticks_diff(now, blink1) == 99".
	# En effet, il est peu probable que la boucle infinie réalise son test exactement à l'instant qui est prévu.
	# Il y aura donc une certaine incertitude (faible) sur les fréquences de clignotement des LED.
	
	# Nombre de millisecondes écoulées depuis le lancement du script
	n = ticks_ms()
	
	# Tâche 1 : clignotement de la LED bleue
	if ticks_diff(n, blink1) > 99:# Toutes les 100 ms depuis la dernière inversion...
		pyb.LED(1).toggle() # On inverse la LED...
		blink1 = n # Et on mémorise la date de cet évènement.
	
	# Tâche 2 : clignotement de la LED verte
	if ticks_diff(n, blink2) > 499:  # Toutes les 500 ms depuis la dernière inversion...
		pyb.LED(2).toggle() # On inverse la LED...
		blink2 = n # Et on mémorise la date de cet évènement.
	
	# Tâche 3 : clignotement de la LED rouge
	if ticks_diff(n, blink3) > 1999: # Toutes les 2000 ms depuis la dernière inversion...
		pyb.LED(3).toggle() # On inverse la LED...
		blink3 = n # Et on mémorise la date de cet évènement.
```

Le code est particulièrement lisible, mais la méthode est impitoyable pour le microcontrôleur qui est actif et consomme de l'énergie 100% du temps alors que le clignotement des LED - la partie utile du programme - ne l'occupe qu'une très petite fraction du temps. Cette méthode peut facilement se généraliser à plus de tâches (faire clignoter d'autres LED, scruter plusieurs boutons, etc.). Mais une autre de ses limitations apparaît immédiatement : la lisibilité et la facilité à maintenir et modifier le programme vont rapidement se dégrader lorsqu'on multipliera les tâches.


## Méthode numéro 2 : Partage du temps à l'aide d'un timer système

> **Vous pouvez télécharger les scripts MicroPython de ce tutoriel (entre autres) en cliquant [ici](../../../assets/Script/MODULES.zip)**.

Cette deuxième approche utilise un *timer* qui est démarré avant la boucle principale et qui décompte le temps de façon totalement indépendante. Vous pouvez imaginer le timer comme un compteur intégré au microcontrôleur.

Une fois paramétré et démarré, un timer va incrémenter son compteur interne CNT par pas de 1, depuis 0 jusqu'à une valeur maximum, par exemple CNT_max = 4095. Lorsque CNT = CNT_max le timer génère une *interruption* dite "*de dépassement de compteur*". A ce moment là, CNT revient à zéro et le timer recommence son cycle de décompte : CNT = 0, CNT = 1... Le schéma qui suit illustre le décompte d'un timer pour CNT_max=5.

<br>
<div align="left">
<img alt="Principe décompte timer" src="images/timer.jpg" width="800px">
</div>
<br>

Nous utilisons l'interruption de dépassement de compteur pour générer une base de temps qui sert au clignotement des LED.
Par comparaison avec la première solution, cette version n'est pas tellement plus compliquée et présente un avantage : ce n'est plus le Cortex M4 intégré au STM32WB55 qui compte le temps écoulé, mais l'un de ses timers. Le Cortex M4 sera donc soulagé de cette tâche et on peut imaginer que le timing des diodes sera plus précis. Il reste néanmoins monopolisé à 100% (du temps) par la boucle principale. Cependant, du fait que nous utilisons une interruption, nous pouvons bénéficier de la fonction *pyb.wfi()* pour placer le microcontrôleur en *mode économie d'énergie jusqu'à la prochaine interruption* (qui va le "réveiller"), ou pendant une milliseconde.

### Le code de *main.py*

```python
# Objet du script : Faire clignoter simultanément les trois LED de la carte NUCLEO-WB55.
# Cette deuxième approche utilise un timer du STM32WB55 et exploite son interruption de dépassement de compteur 
# pour incrémenter une variable globale qui décompte le temps.
# Par comparaison avec la première solution, cette version n'est pas tellement plus compliquée et présente un 
# avantage : ce n'est plus le Cortex M4 qui compte le temps écoulé dans la boucle principale, mais l'un des 
# timers du STM32WB55.Le Cortex M4 sera donc soulagé de cette tâche et on peut imaginer que le timing des 
# diodes sera plus précis.
# Nous utilisons la fonction pyb.wfi() pour placer le microcontrôleur en mode économie d'énergie en mode 
# économie d'énergie jusqu'à la prochaine interruption (qui va le "réveiller"), ou pendant une milliseconde.

# Variable globale qui servira de référence de temps
n = 0

# Variables globales pour les dates de clignotement des LED
blink1 = 0
blink2 = 0
blink3 = 0

# Routine de service de l'interruption (ISR) de dépassement de compteur du timer 1.
# Elle incrémente la variable n de 1 tous les 100-ièmes de seconde.
def tick(timer):
	global n  # TRES IMPORTANT, ne pas oublier le mot-clef "global" devant la variable n
	n += 1

# Démarre le timer 1 à la fréquence de 100 Hz.
# Assigne la fonction "tick" à l'interruption de dépassement de compteur du timer 1.
# Elle sera appelée 100 fois par seconde.
tim1 = pyb.Timer(1, freq=100, callback=tick)

while True: # Boucle infinie

	# On teste le temps ; chaque unité de n indiquant qu'un centième de seconde s'est écoulé.
	# Remarquez que les tests sur les durées ne portent pas sur une égalité (stricte) mais sur un dépassement.
	# Par exemple on écrit "n - blink1 > 9" et pas "n - blink1 == 10".
	# En effet, le timer incrémentant n de façon idépendante, il est probable que la boucle infinie "rate" la valeur
	# n = 10 à l'instant précis où elle survient.

	pyb.wfi() # Place le microcontrôleur en mode économie d'énergie
	
	if n - blink1 > 9: # Toutes les 0.1s depuis sa dernière inversion...
		pyb.LED(1).toggle() # On inverse la LED bleue...
		blink1 = n # Et on mémorise la date de cet évènement.
	if n - blink2 > 49: # Toutes les 0.5s depuis sa dernière inversion...
		pyb.LED(2).toggle() # On inverse la LED verte...
		blink2 = n  # Et on mémorise la date de cet évènement.
	if n - blink3 > 199:  # Toutes les deux secondes depuis sa dernière inversion...
		pyb.LED(3).toggle() # On inverse la LED rouge...
		blink3 = n # Et on mémorise la date de cet évènement.
		
		# On remet les compteurs de temps à zéro lorsque la LED avec la plus longue période a changé d'état.
		# Ceci afin d'éviter un dépassement de capacité de la variable n si le script "tourne" trop longtemps.
		n = 0
		blink1 = 0
		blink2 = 0
		blink3 = 0
```

## Méthode numéro 3 : Utiliser plusieurs timers

> **Vous pouvez télécharger les scripts MicroPython de ce tutoriel (entre autres) en cliquant [ici](../../../assets/Script/MODULES.zip)**.

Cette troisième approche utilise trois timers du STM32WB55, les paramétrise aux fréquences désirées et exploite leurs interruptions respectives de dépassement de compteur pour inverser les LED.

- **Avantage 1** : On se rapproche d'un véritable multitâche matériel, mais on n'y arrive pas tout à fait car les fonctions de service des interruptions de dépassement des timers doivent être exécutées par le Cortex M4.
- **Avantage 2** : Il n'y a pas de programme "principal" (i.e. : de boucle "while True:") ; cette approche consomme donc bien moins d'énergie que celles qui précèdent.
- **Inconvénient** : On utilise un timer pour piloter chaque LED. Cette débauche de ressources matérielles atteindra vite ses limites. L'interpréteur MicroPython pour la NUCLEO-WB55 implémentant 4 timers (TIM1, TIM2, TIM16 et TIM17) on ne pourra pas gérer plus que 4 LED (ou, plus généralement, 4 tâches) de cette façon.

### Variante 1 : Le code avec des *lambda-expressions*

Nous utilisons des *lambda-expressions* pour en réponse aux interruptions des timers. C'est une écriture compacte et formelle propre à MicroPython.

```python
# Objet du script : Faire clignoter simultanément les trois LED de la carte NUCLEO-WB55.
# Cette troisième approche utilise trois timers du STM32WB55, les paramétrise aux fréquences désirées
# et exploite leurs interruptions de dépassement de compteur pour inverser les LED.

# Avantage 1 : On se rapproche d'un véritable multitâche matériel, mais on n'y arrive pas tout à fait car les
# fonctions de service de l'interruption de dépassement des timers doivent être exécutées par le Cortex M4
# du STM32WB55.

# Avantage 2 : Il n'y a pas de programme "principal" (ie : de boucle while "infinie") ; cette approche consomme donc
# bien moins d'énergie que celles qui précèdent.

# Inconconvénient : On utilise un timer pour piloter chaque LED. Cette débauche de ressources matérielles atteindra
# vite ses limites. Comme l'interpréteur MicroPython pour la NUCLEO-WB55 n'implémente que 4 timers (TIM1, TIM2, 
# TIM16 et TIM17) on ne pourra pas gérer plus que 4 LED de cette façon.

from pyb import Timer # Bibliothèque pour gérer les timers

tim1 = Timer(1, freq= 0.5) # Fréquence du timer 1 fixée à 0.5 Hz

# Callback : appelle la routine de service de l'interruption de dépassement du timer.
# Ici cette routine est pyb.LED(1).toggle(), l'écriture est condensée sous forme d'une lambda-expression.
tim1.callback(lambda t: pyb.LED(1).toggle()) # On inverse la LED bleue une fois toutes les deux secondes

tim2 = Timer(2, freq= 2) # Fréquence du timer 2 fixée à 2 Hz
tim2.callback(lambda t: pyb.LED(2).toggle()) # On inverse LED verte deux fois par seconde.

tim16 = Timer(16, freq=10) # Fréquence du timer 16 fixée à 10 Hz
tim16.callback(lambda t: pyb.LED(3).toggle()) # On inverse LED rouge dix fois par seconde.
```

### Variante 2 : Le code sans *lambda-expressions*

Nous utilisons explicitement des **routines de service pour les trois interruptions (ISR) de dépassement des timers**, une écriture équivalente aux lambda-expressions mais plus conventionnelle en programmation embarquée.

La boucle *while True:* réduit la consommation du microcontrôleur entre deux interruption grâce à l'instruction *pyb.wfi()* déjà présentée.


```python
# Objet du script : Faire clignoter simultanément les trois LED de la carte NUCLEO-WB55.
# Cette variante de la troisième approche utilise trois timers intégrés au STM32WB55, les paramétrise 
# aux fréquences désirées et exploite leurs interruptions de dépassement de compteur pour inverser les LED.
# Le code ci-dessous n'utilise pas de lamda-expressions dans les callbacks des timers.
# On écrit explicitement les routines de service des trois interruptions (ISR) de dépassement des timers.

from pyb import Timer # Bibliothèque pour gérer les timers

def blink_LED_red(timer):  # ISR du timer 1
	pyb.LED(3).toggle()

# Démarrage du timer 1, on attache l'ISR à correspondante à son interruption de dépassement de compteur
# Fréquence du timer 1 fixée à 0.5 Hz
tim1 = Timer(1, freq= 0.5, callback = blink_LED_red)

def blink_LED_green(timer):
	pyb.LED(2).toggle()
	
# Fréquence du timer 2 fixée à 2 Hz
tim2 = Timer(2, freq= 2, callback = blink_LED_green)

def blink_LED_blue(timer):
	pyb.LED(1).toggle()
	
# Fréquence du timer 16 fixée à 10 Hz
tim16 = Timer(16, freq=10, callback = blink_LED_blue)

# Boucle principale
while True:
	pyb.wfi() # Place le micorocontrôleur en mode économie d'énergie
```

## Méthode numéro 4 : Gestion du multitâche avec *uasyncio*

> **Vous pouvez télécharger les scripts MicroPython de ce tutoriel (entre autres) en cliquant [ici](../../../assets/Script/MODULES.zip)**.

Cette dernière approche utilise la bibliothèque [`uasyncio`](https://docs.MicroPython.org/en/latest/library/uasyncio.html) intégrée au firmware MicroPython. Celle-ci est une excellente initiation aux notions et mécanismes des *systèmes d'exploitation temps réel pour l'embarqué* (RTOS) tels que [*FreeRTOS*](https://fr.wikipedia.org/wiki/FreeRTOS) très utilisés, mais aussi difficiles à maîtriser. Même s'il offre des possibilités qui rappellent celles d'un RTOS, [`uasyncio`] n'est pas aussi puissant car il ne gère pas la priorité des tâches et il ne garantit pas un temps d'exécution minimum pour celles-ci. La simplicité de sa mise en œuvre et la frugalité de son usage de la RAM doivent bien avoir des contreparties !

L'exemple de programmation asynchrone que nous allons utiliser peut se transposer facilement à bien d'autres applications pour lesquelles le système doit rester réactif aux interaction de l'utilisateur. Vous trouverez un tel exemple avec le [**tutoriel sur le module Grove capteur de gestes PAJ7620U2**](../grove/paj7620u2).

On remarquera que cette approche ne fait intervenir aucune interruption et qu'elle sollicite donc au maximum le microcontrôleur. Elle est élégante, simple mais, en contrepartie, elle n'est pas adaptée pour réduire la consommation d'énergie.

### Le code de *main.py*

```python
# Objet du script : 
# Faire clignoter simultanément les trois LED de la NUCLEO-WB55 à des fréquences différentes.
# On utilise la bibliothèque "uasyncio" pour la programmation asynchrone.
# Code directement adapté de https://docs.MicroPython.org/en/latest/library/uasyncio.html
# Tutoriel sur uasyncio : 
#  https://GitHub.com/peterhinch/MicroPython-async/blob/master/v3/docs/TUTORIAL.md

import uasyncio # On importe la bibliothèque pour l'exécution asynchrone
print("Version of uasyncio: ", uasyncio.__version__) # Version de uasyncio

# Coroutine asynchrone pour faire clignoter la LED sur la broche led
async def blink(led, period_ms):
	while True:
		# Allume la LED
		led.on()
		# Temporisation non blocante de period_ms millisecondes
		await uasyncio.sleep_ms(period_ms)
		# Eteint la LED
		led.off()
		# Temporisation non blocante de period_ms millisecondes
		await uasyncio.sleep_ms(period_ms)

# Crée trois tâches concurrentes "blink", une par LED
async def main(LED_red, LED_green, LED_blue):

	task = uasyncio.create_task(blink(LED_red, 2000))  # tâche pour la LED rouge
	uasyncio.create_task(blink(LED_green, 500)) # tâche pour la LED verte
	uasyncio.create_task(blink(LED_blue, 100)) # tâche pour la LED bleue

	# Prolonge l'exécution du programme jusqu'à ce que task ait terminé 
	# (ce qui n'arrivera jamais)
	await task

# Le code en syntaxe pyboard (valable avec la NUCLEO-WB55)
from pyb import LED
# Appel au planificateur qui lance l'exécution de la fonction main avec ses trois 
# tâches concurrentes. Aucune instruction du script située au-dessous de cette 
# ligne ne sera exécutée.
uasyncio.run(main(LED(3), LED(2), LED(1)))

# Le code en syntaxe générique (pour des LED externes par exemple, connectées
# aux broches D2, D3, D4).
# from machine import Pin
# uasyncio.run(main(Pin('D2'), Pin('D3'), Pin('D4')))
```

## Ressources

Vous trouverez **un tutoriel complet** sur l'usage de la bibliothèque `uasyncio` sur [cette page](https://GitHub.com/peterhinch/MicroPython-async/blob/master/v3/docs/TUTORIAL.md).
