---
title: Le chien de garde (Watchdog Timer)
description: Mise en œuvre du chien de garde (Watchdog Timer) du STM32WB55 en MicroPython
---

# Le chien de garde (Watchdog Timer)

Le STM32WB55 est équipé d'un circuit appelé [**chien de garde indépendant**](https://fr.wikipedia.org/wiki/Chien_de_garde_(informatique)) (abrégé par IDWG pour « Independant Watchdog ») qui fait office de compteur-à-rebours autonome. On fixe sa valeur maximum de départ et on le démarre.<br>
Une fois lancé l’IDWG décompte les microsecondes, de façon totalement indépendante du programme en cours d’exécution, jusqu’à atteindre la valeur zéro. Il force alors un « reset » du microcontrôleur. Il est possible d’interrompre et relancer le décompte de l’IDWG à partir de sa valeur maximum à tout moment, depuis le programme utilisateur, et d’empêcher ce reset.<br>
On comprend l’intérêt de ce périphérique : si le firmware est « planté » (à cause d’un bug, d’un capteur défaillant, d’un module Wi-Fi qui ne se connecte pas, etc.) alors il ne « rechargera » pas l’IDWG avant la fin de son décompte, et l’IDWG forcera le système à redémarrer.<br>
L'[IDWG est implémenté dans MicroPython](https://docs.MicroPython.org/en/latest/library/machine.WDT.html), mais il est désigné par le terme **Watchdog Timer**. Ce tutoriel explique donc comment mettre en œuvre le watchdog timer en MicroPython.

## Matériel requis

La carte NUCLEO-WB55. Nous ferons clignoter la LED 1, bleue :

<div align="left">
<img alt="LEDS" src="images/leds.jpg" width="500px">
</div>

## Le code MicroPython

**Le script présenté ci-après est disponible dans [la zone de téléchargement](../../../assets/Script/NUCLEO_WB55.zip)**.

Le Watchdog Timer est programmé pour être "nourri" toutes les 2 secondes. La boucle principale "fait un tour" toutes les secondes, faisant clignoter la LED bleue de la NUCLEO-WB55.
Une routine d'interruption permet de retarder cette boucle de 2 secondes après 5 appuis successifs sur le bouton SW1. Le temps d'exécution de la boucle devient alors supérieur au compte à rebours du Watchdog Timer, qui va en conséquence redémarrer (RESET software) la NUCLEO-WB55.

Éditez le script *main.py* contenu dans le répertoire du disque USB virtuel associé à la NUCLEO-WB55, *PYBFLASH*, et copiez y ce code :

``` python
# Objet du script :
# Démonstration du Watchdog Timer 
# Référence : https://docs.MicroPython.org/en/latest/library/machine.WDT.html
# Après 5 appuis sur le bouton SW1, le script sde met en pause pendant une durée
# qui dépasse le seuil de redémarrage imposé par le watchdog.

from time import sleep_ms # Pour gérer les temporisations

# Variable globale qui compte les appuis sur le bouton
cnt = 0

# Initialisation du bouton SW1
sw1 = pyb.Pin('SW1')
sw1.init(pyb.Pin.IN, pyb.Pin.PULL_UP, af=-1)

# Fonction de service de l'interruption pour SW1 (incrémente cnt)
def Press(line):
	global cnt
	cnt += 1

# On attache une routine de service d'interruption à SW1
irq_1 = pyb.ExtInt(sw1, pyb.ExtInt.IRQ_FALLING, pyb.Pin.PULL_UP, Press)

# Initialisation de la LED bleue
blue_LED = pyb.LED(1) # screen printed LED1 on the PCB

# Temps d'attente (ms) avant de changer l'état de la LED
DELAY_LED = const(500)

# Temps d'attente (ms) avant que le watchdog ne rédémarre la NUCLEO-WB55
WDT_TIMEOUT = const(2000)

# Pour gérer le watchdog
from machine import WDT

# On démarre le watchdog
wdt = WDT(timeout = WDT_TIMEOUT)

while True:

	# On "recharge" le watchdog pour WDT_TIMEOUT millisecondes
	wdt.feed()

	if cnt == 5:
		print("Pause pendant %d secondes" %( WDT_TIMEOUT // 1000 ))
		cnt = 0
		sleep_ms(WDT_TIMEOUT)

	blue_LED.on() # Allume la LED
	sleep_ms(DELAY_LED)  # Attends delai ms

	blue_LED.off() # Eteint la LED
	sleep_ms(DELAY_LED) # Attends delai ms
```

Vous pouvez lancer le script avec Ctrl + D sur le terminal PuTTY et observer que la LED clignote à une fréquence de 1 Hz (un cycle allumée - éteinte par seconde). Si vous appuyez ensuite 5 fois sur SW1, le message "Pause pendant 2 secondes" s'affiche sur votre terminal, puis vous serez déconnecté de celui-ci suite au RESET généré par le watchdog. Vous constaterez que la LED bleue recommence à clignoter, ce qui signifie que le programme contenu dans *main.py* a bien été relancé.
