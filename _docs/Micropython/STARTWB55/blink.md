---
title: Faire clignoter une LED
description: Tutoriels pour faire clignoter une LED avec MicroPython
---

# Faire clignoter une LED

Ce tutoriel explique comment faire clignoter une *diode électroluminescente* avec MicroPython. Dans tous nos tutoriels, pour éviter d'éventuelles confusions entre les acronymes français et anglo-saxons, nous ferons le choix **d'utiliser systématiquement les acronymes anglo-saxons**. Donc, nous désignerons par la suite les diodes électroluminescentes par l'acronyme **LED** (pour *Light Emitting Diode*).
Cet exemple est adapté du tutoriel Adafruit disponible [ici](https://learn.adafruit.com/MicroPython-basics-blink-a-led/blink-led) qui détaille également la communication REPL avec le microcontrôleur.

## Matériel requis

La carte NUCLEO-WB55. Nous ferons clignoter la LED 1, bleue :

<div align="left">
<img alt="LEDS" src="images/leds.jpg" width="500px">
</div>

## Le code MicroPython

**Vous pouvez télécharger les scripts MicroPython de ce tutoriel (entre autres) en cliquant [ici](../../../assets/Script/NUCLEO_WB55.zip)**.

Éditez le script *main.py* contenu dans le répertoire du disque USB virtuel associé à la NUCLEO-WB55 : *PYBFLASH* et copiez-y le code suivant :

``` python
# Objet du script :
# Exemple faisant clignoter la LED bleue de la NUCLEO-WB55 à une fréquence donnée.

import pyb # pour les accès aux périphériques
from time import sleep # pour les temporisations

# Initialisation de la LED bleue
led_blue = pyb.LED(1) # sérigraphiée LED1 sur le PCB

delay = 0.5 # Temps d'attente avant de changer l'état de la LED

# La boucle va se répéter dix fois (pour i de 0 à 9)
for i in range(10):

	# Affiche l'index de l'itération sur le port série de l'USB User
	# Voir https://www.geeksforgeeks.org/python-output-formatting/ pour l'explication de la ligne qui suit.
	# print("Itération {:2d}".format(i))
	# Il existe plusieurs façons d'afficher une valeur avec print, ci-dessous une alternative plus lisible

	print("Itération %d: "%i)

	led_blue.on() # Allume la LED
	print("LED bleue allumée")
	sleep(delay) # Attends delai secondes

	led_blue.off() # Eteint la LED
	print("LED bleue éteinte")
	sleep(delay) # Attends delai secondes
```

Vous pouvez lancer le script avec Ctrl + D sur le terminal PuTTY et observer les messages qu'il renvoie pendant que la LED clignote à une fréquence de 1 Hz (un cycle allumée - éteinte par seconde) :

<div align="left">
<img alt="Sortie blink" src="images/output_blink.png" width="700px">
</div>

## Pour aller plus loin 

Les variantes possibles autour de cet exemple sont infinies ... 
 - Vous trouverez sur [cette page](blink_many) sa généralisation aux trois LED de la NUCLEO-WB55.
 - Vous trouverez [ici](chenillard) comment faire clignoter en séquence les trois LED de la NUCLEO-WB55.
