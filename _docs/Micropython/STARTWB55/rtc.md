---
title: Horloge temps-réel
description: Mise en œuvre de la Real Time Clock (RTC) du STM32WB55 en MicroPython
---

# La Real Time Clock

Ce tutoriel explique comment mettre en œuvre l'horloge temps réel du STM32WB55 en MicroPython.

L'horloge temps réel (Real-Time Clock en anglais, abrégée RTC) est un circuit électronique intégré dans le microcontrôleur STM32WB55 remplissant les fonctions d’une horloge très précise pour des problématiques d’horodatage, de gestions d’alarmes déclenchées suivant un calendrier et capable de mettre le microcontrôleur en sommeil ou de le réveiller, etc. Afin qu’elle soit aussi précise et stable que possible, la RTC peut utiliser comme source de fréquence un cristal de quartz vibrant à 32kHz.

## Utilisation

La RTC est assez simple à utiliser puisque seulement une méthode sera nécessaire : `pyb.RTC().datetime()`.
La date utilisée prendra alors le format suivant :
- \[year,  m,  j,  wd,  h,  m,  s,  sub]
- année  mois  jour  jour_de_la_semaine  heure  minute  seconde  subsecond(compteur interne)

Nous commencerons créer une instance de la RTC :

```python
rtc = pyb.RTC()
```
`rtc.datetime()` utilisée seule renvoie la date actuelle au format indiqué.
`rtc.datetime(date)` remplace la date actuelle.
La date du samedi 2 janvier 2021 à 12h21 51s peut alors être définie comme ceci :
```python
rtc.datetime((2021, 1, 2, 6, 12, 21, 51, 0))
```
et elle peut être récupérée comme cela :
```python
date = rtc.datetime()
```

## Exemples

**Vous pouvez télécharger les scripts MicroPython de ce tutoriel (entre autres) en cliquant [ici](../../../assets/Script/NUCLEO_WB55.zip)**.

Ainsi un premier code pour afficher sur le terminal serait :

```python
# Objet du script : Mise en œuvre de l'horloge temps réel (RTC pour "Real Time Clock")
# du STM32WB55.

from time import sleep # Pour temporiser

# Instanciation de la RTC
rtc = pyb.RTC()

# annee mois jour jour_de_la_semaine heure minute seconde subseconde(compteur interne)
#       year  m  j  wd h  m  s  sub
date = [2021, 1, 1, 5, 0, 0, 0, 0]
#indices : 0 1 2 3 4 5 6 7

# On initialise la date
rtc.datetime(date)

while True :
	# On récupère la date mise à jour
	date = rtc.datetime()
	# Et on l'affiche
	print('{0:02d}'.format(date[4])+'h'+'{0:02d}'.format(date[5])+'min'+'{0:02d}'.format(date[6])+'s')
	# On actualise toute les secondes
	sleep(1)
```
On remarquera l'utilisation de `'{0:02d}'.format(date[4])` qui permet l'affichage de _02_ plutôt que _2_.

En se servant du [tutoriel de l'afficheur 8x7-segments TM1638](../grove/tm1638) on peut réaliser une horloge à affichage LED avec le code suivant :

```python
# Objet du script : Mise en œuvre de l'horloge temps réel (RTC pour "Real Time Clock")
# du STM32WB55.
# Création d'une horloge LED avec un afficheur 8x7-segments TM1638.

import tm1638 # Pilote de l'afficheur
from machine import Pin # Pour gérér les GPIO
from time import sleep_ms # Pour les temporisations

# On déclare la carte de l'afficheur
tm = tm1638.TM1638(stb=Pin('D2'), clk=Pin('D3'), dio=Pin('D4'))

# On déclare la RTC
rtc = pyb.RTC()

# On réduit la luminosité
tm.brightness(0)

# annee mois jour jour_de_la_semaine heure minute seconde subseconde(compteur interne)
#		year  m  j  wd h  m  s  sub
date = [2021, 1, 1, 5, 0, 0, 0, 0]
#indices : 0 1 2 3 4 5 6 7

# On initialise la date
rtc.datetime(date)

while True :
	# On récupère la date mise à jour
	# rtc.datetime() renvoie un objet non modifiable, on le change en liste pour le modifier
	date = list(rtc.datetime())

	# Et on l'affiche en faisant clignoter le point des secondes
	if (date[6] % 2) :
		tm.show('{0:02d}'.format(date[4])+'h'+'{0:02d}'.format(date[5])+' .'+'{0:02d}'.format(date[6]))
	else :
		tm.show('{0:02d}'.format(date[4])+'h'+'{0:02d}'.format(date[5])+' '+'{0:02d}'.format(date[6]))

	# On récupère l'information sur les boutons pour régler l'heure
	buttons = tm.keys()

	# On compare bit à bit pour identifier le bouton
	if (buttons & 1) :
		date[4] += 1
		if (date[4] > 23):
			date[4] = 0
		rtc.datetime(date)
	if (buttons & 1<<1) :
		date[5] += 1
		if (date[5] > 59) :
			date[5] = 0
		rtc.datetime(date)
	if (buttons & 1<<2) :
		date[6] += 1
		if (date[6] > 59):
			date[6] = 0
		rtc.datetime(date)

	# On actualise toute les 100 milisecondes (pour une lecture des boutons plus sensible,
	# peut être modifié)
	sleep_ms(100)
```

## Pour aller plus loin

L’utilisation de la RTC nécessite que le STM32WB55 reste sous tension (autrement le paramétrage est perdu) mais aussi une procédure automatique de mise à la date et à l’heure chaque fois qu’il démarre. On peut imaginer transmettre l’heure au microcontrôleur depuis un smartphone (via le BLE, en s’inspirant de [cet exemple](https://stm32python.gitlab.io/fr/docs/Micropython/BLE/MITAppInventor_3)), par l’UART depuis un ordinateur (en s’inspirant de [cet autre exemple](uart)) ou encore via un module Wi-Fi qui interroge [un serveur NTP](https://fr.wikipedia.org/wiki/Network_Time_Protocol) (exemple à venir).
