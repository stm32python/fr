---
title: Mise en œuvre du bus I2C - piloter un afficheur OLED
description: Mise en œuvre du bus I2C et d'un afficheur OLED SSD1306 avec MicroPython
---

# Mise en œuvre du bus I2C : piloter un afficheur OLED

Ce tutoriel explique comment mettre en œuvre un afficheur I2C (ou I<sup>2</sup>C) OLED avec MicroPython.
Il est très facile d’utiliser un écran OLED avec MicroPython pour afficher des messages. Nous verrons dans cet exemple comment brancher l’écran sur le bus I2C, puis comment le piloter pour y envoyer des messages. Pour l'exemple, nous utiliserons l’écran monochrome OLED, 192 x 32 pixels de Adafruit, cependant tous les écrans intégrant le contrôleur SSD1306 sont compatibles.

## Qu'est-ce que l'I2C ?

[I<sup>2</sup>C](https://fr.wikipedia.org/wiki/I2C) est l'acronyme de "Inter-Integrated Circuit" (en français : bus de communication intégré inter-circuits). Il s'agit d'un bus série fonctionnant selon un protocole inventé par Philips. Pour dialoguer sur un bus I2C, chaque module "esclave" qui s'y trouve branché **est identifié par une adresse codée sur 7 bits** afin de dialoguer avec le contrôleur maître intégré au STM32WB55.

Avec la prolifération des modules I2C, **il pourrait arriver que deux ou plusieurs modules que vous auriez connectés sur un bus I2C aient la même adresse**, ce qui conduirait inévitablement à des plantages. Pour éviter ce type de conflits, vous devrez consulter les fiches techniques de vos modules et, pour ceux qui le permettent, prendre soin de modifier (si nécessaire) leur adresse I2C, généralement codée dans leur firmware.

Le nombre maximum d'équipements est donc limité par le nombre d'adresses disponibles, 7 bits d'adressage et un bit R/W (lecture ou écriture), soit 128 périphériques, mais il dépend également de la capacité du bus (dont dépend la vitesse maximale de celui-ci).

Le module I2C utilise **4 broches** : deux pour son alimentation (GND et V<sub>DD</sub>) et deux pour communiquer avec le bus : 
 - SDA (Serial Data Line) : ligne de données bidirectionnelle,
 - SCL (Serial Clock Line) : ligne d'horloge de synchronisation bidirectionnelle.

Ces deux lignes sont portées au niveau de tension V<sub>DD</sub> par des **résistances de tirage** (ou de **pull-up** en anglais).

Vous trouverez plus d'informations sur [**la page Wikipédia dédiée à l'I2C**](https://fr.wikipedia.org/wiki/I2C).


## Matériel requis

1. La carte NUCLEO-WB55
3. Un afficheur avec un contrôleur SSD1306


Voici comment brancher l’écran OLED :
* D15=SCL
* D14=SDA

<div align="left">
<img alt="oled" src="images/oled.png" width="400px">
</div>

**Attention à ne pas vous tromper de broches !**<br>
Relier les mauvaises broches pourrait **détruire** l'afficheur !

## Le code MicroPython

**Vous pouvez télécharger les scripts MicroPython de ce tutoriel (entre autres) en cliquant [ici](../../../assets/Script/NUCLEO_WB55.zip)**. 

Lorsque vous aurez terminé le téléchargement, il faudra transférer les fichiers *main.py* et *ssd1306.py* dans le répertoire du périphérique *PYBFLASH*. Contenu de *main.py* :

```python
# Objet du script :
# Ecrire un texte sur un afficheur OLED contrôlé par un SSD1306.
# Démonstration de mise en œuvre du bus I2C.

from machine import Pin, I2C # Pour piloter les entrées-sorties et le bus I2C
import ssd1306  # Pour piloter l'afficheur
from time import sleep_ms # Pour temporiser

# Initialisation du périphérique I2C
i2c = I2C(1)

# Pause d'une seconde pour laisser à l'I2C le temps de s'initialiser
sleep_ms(1000)

# Paramétrage des caractéristiques de l'écran OLED
screen_width = 128
screen_length = 32
oled = ssd1306.SSD1306_I2C(screen_width, screen_length, i2c)

# Envoi du texte à afficher sur l'écran OLED
oled.text('MicroPython OLED!', 0, 0)
oled.text(' I2C ', 0, 10)
oled.text('Trop facile !!!', 0, 20)

# Affiche
oled.show()
```

Enregistrez les modifications avec (*[CTRL]* + *[S]* sous Notepad++). Vous pouvez lancer le script avec Ctrl + D sur le terminal PuTTY et observez les messages qui s'affichent sur le module OLED.
