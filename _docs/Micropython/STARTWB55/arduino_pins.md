---
title: Appel des broches Arduino dans les scripts MicroPython
description: Appel de broches Arduino par les scripts MicroPython sur la NUCLEO-WB55
---

# Appel des broches Arduino dans les scripts MicroPython

La NUCLEO-WB55 est opportunément équipée des broches Arduino, dont le mappage (standardisé par la carte Arduino UNO) est rappelé [ici](../../Kit/nucleo_wb55rg).

## Cas numéro 1 : Vous utilisez un firmware 1.17 ou plus récent, pour la carte NUCLEO-WB55

Depuis la version 1.17, **tous** les firmwares MicroPython pour la carte NUCLEO-WB55 intègrent les alias pour les broches Arduino. 
En particulier, [ceux que nous proposons en téléchargement sur ce site](../Telechargement), qui proviennent du site MicroPython officiel, intègrent ces alias. Par exemple, [l'exercice avec le buzzer Grove](../grove/buzzer), **connecté sur la broche `D3`**, donne le script *main.py* suivant :


```python
# Objet du script : Jouer un jingle sur un buzzer (Grove ou autre).
# Cet exemple fait la démonstration de l'usage de la PWM pour la broche D3 sur laquelle est 
# branché le buzzer.

from pyb import Pin, Timer

# Liste des notes qui seront jouées par le buzzer
frequency = [262, 294, 330, 349, 370, 392, 440, 494]

# D3 génère une PWM avec TIM1, CH3
BUZZER = Pin('D3') 

while True :
	# Itération entre 0 et 7
	for i in range (0,7) :
		# On ajuste la fréquence pendant l'itération
		tim1 = Timer(1, freq=frequency[i])
		ch3 = tim1.channel(3, Timer.PWM, pin=BUZZER)
		# Rapport cyclique paramétré à 5% (le buzzer est alimenté 5% du temps d'une période)
		ch3.pulse_width_percent(5)
```

L'appel à la broche `D3` **se fait tout simplement par l'instruction `Pin('D3')`.**

**Tous les exemples que nous donnons sur ce site utilisent correspondent à ce premier cas**. 

## Cas numéro 2 : Vous n'êtes pas dans le cas numéro 1

**Les anciens firmwares MicroPython pour STM32 (versions avant la 1.17)** ainsi que ceux (même récents) pour les cartes **autres que la NUCLEO-WB55 n'utilisent pas les alias de l'API Arduino pour accéder aux broches, mais ceux de la carte Pyboard**. Dans cette situation, vous trouverez peut-être commode de redéfinir les "alias Pyboard" en "alias Arduino" à l'aide de la procédure expliquée ici.<br>

Commencez par enregistrez le contenu qui suit dans un fichier nommé *arduino_pins.py*.

```python
# Correspondance broches Arduino / broches Pyboard
from pyb import Pin
A0=Pin.cpu.C0
A1=Pin.cpu.C1
A2=Pin.cpu.A1
A3=Pin.cpu.A0
A4=Pin.cpu.A4
A5=Pin.cpu.A5
D0=Pin.cpu.A3
D1=Pin.cpu.A2
D2=Pin.cpu.C6
D3=Pin.cpu.A10
D4=Pin.cpu.C10
D5=Pin.cpu.A15
D6=Pin.cpu.A8
D7=Pin.cpu.C13
D8=Pin.cpu.C12
D9=Pin.cpu.A9
D10=Pin.cpu.A4
D11=Pin.cpu.A7
D12=Pin.cpu.A6
D13=Pin.cpu.A5
D14=Pin.cpu.B9
D15=Pin.cpu.B8
```

Ce fichier doit être copié sur *PYBFLASH* et importé dans le script *main.py* de votre application. Par exemple, la version modifiée de [l'exercice avec le buzzer Grove](../grove/buzzer) donnera le script *main.py* suivant :

```python
# Objet du script : Jouer un jingle sur un buzzer (Grove ou autre).
# Cet exemple fait la démonstration de l'usage de la PWM pour la broche D3 sur laquelle est 
# branché le buzzer.

import arduino_pins # Alias pour accéder aux broches Arduino 
from pyb import Timer

# Liste des notes qui seront jouées par le buzzer
frequency = [262, 294, 330, 349, 370, 392, 440, 494]

# D3 génère une PWM avec TIM1, CH3
BUZZER = arduino_pins.D3 # Appel de la broche selon la convention Arduino

while True :
	# Itération entre 0 et 7
	for i in range (0,7) :
		# On ajuste la fréquence pendant l'itération
		tim1 = Timer(1, freq=frequency[i])
		ch3 = tim1.channel(3, Timer.PWM, pin=BUZZER)
		# Rapport cyclique paramétré à 5% (le buzzer est alimenté 5% du temps d'une période)
		ch3.pulse_width_percent(5)
```

