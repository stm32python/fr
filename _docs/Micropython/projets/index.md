---
title: Projets et applications avec STM32python
description: Liste des projets et applications avec STM32python
---

# Projets et applications avec STM32python


## [Station météo](../../assets/projects/station_meteo.zip)

Prototyper une station météo avec une NUCLEO-WB55 inspirée du produit Ikea Klockis. **Merci à Christophe Priouzeau & Gérald Hallet !**
