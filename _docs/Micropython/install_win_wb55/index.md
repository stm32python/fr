---
title: Guide de démarrage rapide sous Windows avec la NUCLEO-WB55
description: Guide de démarrage rapide sous Windows pour la carte NUCLEO-WB55

---
# Démarrer sous Windows avec la NUCLEO-WB55

Dans cette section figurent les instructions pour installer et tester le firmware (ou micrologiciel) MicroPython [**distribué dans la page de téléchargements de notre site**](../../Micropython/Telechargement) pour la carte [NUCLEO-WB55](../../Kit/nucleo_wb55rg), qui est notre cible principale, à l'aide d'un environnement informatique **Microsoft Windows**. Pour en apprendre plus sur le firmware MicroPython, vous pouvez consultez [la section que nous lui avons dédiée](../firmware/index).

**NB :** [Le site officiel de la fondation MicroPython](https://MicroPython.org/download/) propose des firmwares pour un grand nombre d'autres cartes de STMicroelectronics, que vous pourrez installer selon les mêmes procédures que celles décrites ci-après.

Plus précisément, nous allons vous expliquer comment :
- Programmer le firmware MicroPython dans le microcontrôleur (MCU) de la carte NUCLEO-WB55 (un STM32WB55RG).
- Utiliser un terminal de communication UART (TeraTerm ou PuTTY) pour communiquer avec ce firmware.
- Exécuter sur la NUCLEO-WB55 un premier script / programme MicroPython.

## Programmation du firmware MicroPython dans la NUCLEO-WB55

**Pour commencer**, nous allons placer le MCU STM32WB55RG en mode programmation "ST-LINK".<br>
Assurez-vous que le cavalier du bloc de configuration MCU est positionné sur ***USB_STL***. Voici comment doit être configurée la carte (le cavalier qui nous intéresse est coloré en rouge sur le dessin) :

<br>
<div align="left">
<img alt="Nucleo_WB55_Config_STL" src="images/Nucleo_WB55_Config_STL.png" width="350px">
</div>
<br>

**Connectez ensuite** un câble micro USB sur le port ***ST-LINK*** en ***dessous des LED4 et LED5*** (vous aurez besoin d’un câble USB vers micro-USB). L'autre extrémité du câble doit bien sûr être connectée sur votre PC.

**Nous pouvons à présent installer le dernier firmware MicroPython à jour sur votre NUCLEO-WB55**. La méthode la plus sûre pour cela consiste à :

1. Télécharger le firmware le plus récent pour votre carte, **au format .hex**, directement depuis [micropython.org](https://micropython.org/download/) ou depuis  [notre page de téléchargement](../Telechargement).
2. Installer [STM32CubeProgrammer](../../tools/cubeprog/index) en suivant les instructions sur le site de STMicroelectronics.
3. Programmer le firmware MicroPython dans le MCU STM32WB55RG selon la procédure indiquée [ici](../../tools/cubeprog/cube_prog_firmware_stm32).

**NB :** Il existe une alternative nettement plus simple pour programmer le firmware MicroPython, en ligne de commande, avec l'utilitaire Windows **robocopy**, présentée  [ici](../../tools/robocopy/index).

**Une fois la programmation du firmware terminée**, placez le cavalier sur ***USB_MCU*** ainsi que le câble micro USB sur le connecteur ***USB_USER*** sous le ***bouton RESET*** :

<br>
<div align="left">
<img alt="Nucleo_WB55_Config_MCU" src="images/Nucleo_WB55_Config_MCU.png" width="350px">
</div>
<br>


Finalement, **redémarrez la carte NUCLEO-WB55** (en appuyant sur le bouton "Reset SW4", ou en débranchant - rebranchant le câble USB).

Votre carte NUCLEO est maintenant capable d'interpréter un fichier ".py".<br>
Regardons les fichiers générés par le système MicroPython avec l'explorateur Windows :

Ouvrez le périphérique *PYBFLASH* avec l'explorateur Windows :

<br>

![Image](images/dfu_util_3.png)

<br>

Nous verrons plus tard comment éditer les scripts MicroPython disponibles dans le système de fichier *PYBFLASH*.

**NB : **Le script *boot.py* peut être modifié par les utilisateurs avancés, il permet d'initialiser MicroPython et notamment de choisir quel script sera exécuté après le démarrage de MicroPython, par défaut le script *main.py*.

## Installation de l'environnement de programmation

Les environnements de développement pour MicroPython sont nombreux, vous trouverez quelques suggestions sur [notre page de ressources et téléchargements](../Telechargement). Sur ce site, nous avons choisi les outils suivants :

- Pour se connecter au firmware MicroPython et interagir avec, **un émulateur de terminal série**, par exemple [PuTTY](PuTTY) ou [TeraTerm](https://osdn.net/projects/ttssh2/).
- Pour éditer et modifier vos programmes / scripts MicroPython, le logiciel [Notepad++](https://notepad-plus-plus.org/) qui propose la coloration syntaxique du langage Python.

**NB** : Ces logiciels sont disponibles en **version portable** pour le cas où vous ne disposeriez pas de droits d'administration sur votre PC.


## Communication avec le firmware MicroPython via un terminal série

Maintenant que le firmware MicroPython est installé dans la NUCLEO-WB55, nous allons tester son bon fonctionnement. Ce test consiste à lui envoyer une commande Python et à vérifier que son exécution se déroule correctement.

La communication s'effectue par USB  via un port série, nous avons donc besoin d'un logiciel capable d'envoyer les commandes Python sous forme de texte à la carte et de recevoir le résultat de leur exécution. C'est ce que l'on appelle **la console REPL**, une composante du firmware MicroPython expliquée plus en détails [sur cette page](../firmware/index). Pour cela ...

- **Ouvrez TeraTerm** après l'avoir installé (ou bien **PuTTY** "portable" si vous n'avez pas les droits d'administration).

- Sélectionnez l'option ***Serial*** et le ***port COM*** correspondant à votre carte (**COMx: USB Serial Device**) :

<br>

![Image](images/TeraTerm.PNG)<br><br>

<br>

  Sous Windows, l'identification du port COM attribué à la carte NUCLEO n'est pas immédiate, nous vous renvoyons à l'**annexe 2 : Identifier le port COM attribué par Windows à votre carte**, plus bas dans cette section, qui explique comment obtenir cette information.

- Configurez les champs suivants dans le menu ***Setup*** / ***Serial Port*** :

<br>

![Image](images/TeraTerm_Setup.png)

<br>

Une nouvelle fenêtre s'affiche :

<br>

![Image](images/TeraTermDisplay_1.png)

<br>

 - Si nécessaire, appuyez sur **[CTRL]-[D]** pour faire un **Reset software**.<br>Vous pouvez maintenant exécuter des commandes Python en mode "interactif" REPL :

```python
print("Hello World")
```

<br>

![Image](images/TeraTermDisplay_2.png)

<br>

Gardez la fenêtre TeraTerm ouverte, elle vous sera utile pour visualiser l'exécution des scripts.

## Ecrire un script MicroPython avec Notepad++ et l'exécuter avec TeraTerm

Nous allons voir dans cette partie comment rédiger avec Notepad++ un script MicroPython pour notre carte NUCLEO-WB55. Lancez Notepad++ après l'avoir installé (vous pouvez aussi utiliser la version portable).

Ouvrez le fichier ***main.py*** présent sur la Nucleo (représenté par le lecteur **PYBFLASH**).

Notre premier script affichera 10 fois le message "MicroPython est génial" avec le numéro du message à travers le port série du connecteur USB user de la carte NUCLEO.

Ecrivez l'algorithme suivant dans l'éditeur de script :

<br>

![Image](images/Notepad_test1.png)

<br>

Prenez soin de sauvegarder le fichier (**[CTRL]*+*[S]**) ***main.py*** sur la carte (lecteur *PYBFLASH*).<br>
Faites un software reset de la carte en appuyant sur **[CTRL]-[D]** dans votre terminal série (TeraTerm par exemple), le script est directement interprété !

<br>

![Image](images/TeraTerm_test1.png)

<br>

Le script s'est exécuté avec succès ; nous voyons bien notre message s'afficher 10 fois dans le terminal.<br>
Vous venez d'écrire votre premier script Python embarqué dans une carte NUCLEO-WB55!

**Remarque :** Notepad++ n'est pas configuré par défaut pour utiliser des tabulations ; il les simule par plusieurs espaces consécutifs. Pour le codage en MicroPython, il est préférable de modifier ce comportement.

Pour ce faire, on commence par forcer l'affichage des espaces et tabulations, en allant dans le menu "View" (ou "Affichage" pour la version française), puis dans le sous-menu "Show Symbol" (VF : "Symboles spéciaux" où on coche les options "Show White Space and TAB" (VF : "Afficher les blancs et les tabulations") et "Show Indent guide" (VF : "Afficher le guide d'indentation").

<br>

![Image](images/Menu_1.png)

<br>

Ensuite, on demande à Notepad++ d’effectivement remplacer les espaces par des tabulations dans les scripts Python, en allant dans le menu “Settings” (VF : “Paramètres”), puis dans la boite “Preferences” (VF : “Préférences”). Dans la colonne de gauche il faut sélectionner “Language” (VF : “Langage”). Dans l’encadré à droite “Tab Setting” (VF : “Tabulations”) on sélectionne “python” et enfin dans l’encadré “Use default value” (VF : “Valeur par défaut”) il faut désactiver la case “Replace by space” (VF : “Insérer des espaces”).

<br>

![Image](images/Menu_2.png)

<br>

## Annexe 1 : Précautions à prendre pour ne pas corrompre le système de fichiers de MicroPython

Vous pourrez rencontrer occasionnellement des difficultés lorsque vous modifiez le fichier *main.py* ou d'autres fichiers situés sur le lecteur *PYBFLASH*. En effet, ce lecteur contient un système de fichiers FAT quelque peu fragile et susceptible d'être corrompu par certaines manipulations qui ne posent pas de problème sur un PC. Aussi nous vous **recommandons de prendre les précautions suivantes** :

- Le plus important : **évitez autant que possible d'éditer et modifier directement les scripts MicroPython directement sur le lecteur _PYBFLASH_**. Travaillez sur vos script dans un dossier enregistré sur votre PC/MAC puis faites un glisser-déplacer (ou une copie) de ceux-ci sur *PYBFLASH* lorsque vous être prêts à les tester sur la carte NUCLEO.

- Quand vous effacez un fichier contenu sur *PYBFLASH*, sous Windows **utilisez la combinaison de touches [SHIFT]+[SUPPR].** 

- Si malgré notre conseil de ne pas le faire, vous éditez un fichier sur *PYBFLASH*, **ne pas oublier de sauvegarder le fichier avant de déconnecter le câble USB qui relie le PC à votre carte NUCLEO.**

- Quand vous voulez déconnecter le câble USB du PC, **ne pas oublier de démonter/éjecter proprement *PYBFLASH*.**

- Quand vous voulez tester une nouvelle version de votre code, **utilisez *[CTRL]-[C]* et *[CTRL]-[D]* dans le terminal série.**

    - *[CTRL]-[C]* : pour arrêter l’exécution courante.
    - *[CTRL]-[D]* : Demande à MicroPython de faire une synchronisation du système de fichiers FAT et un RESET logiciel.

- Quand vous voulez tester une nouvelle version de votre code et que MicroPython indique qu’il y a une erreur (non visible) dans le code, **vous devez démonter/éjecter *PYBFLASH*, déconnecter et reconnecter le câble USB au PC/MAC.**

Si, malgré ces précautions, *PYBFLASH* devient inaccessible, vous n'aurez d'autre choix que réinstaller le firmware comme indiqué au début de cette page ; vous pourriez même être contraint de réinitialiser la mémoire flash du microcontrôleur avec l'outil [STM32CubeProgrammer](../../tools/cubeprog/index).

## Annexe 2 : Identifier le port COM attribué par Windows à votre carte NUCLEO-WB55

Chaque fois que vous connectez la NUCLEO-WB55 à votre PC par un câble USB, *un port COM* est créé et alloué par Windows au port série virtuel exposé par le firmware MicroPython. Cette annexe explique comment identifier ce port COM sachant que Windows attribue peut être simultanément d'autres ports COM à des périphériques USB connectés au PC tels que clavier, souris, Web CAM...

Ecrivez `gestionnaire de périphériques` ( `devices manager` en anglais) dans la barre de recherche Windows, puis cliquez sur `Ouvrir` :

<br>
<div align="left">
<img alt="Port COM" src="images/port_COM.png" width="600px">
</div>
<br>

Une nouvelle fenêtre s'ouvre :

<br>
<div align="left">
<img alt="Windows Devices Manager" src="images/gestion_peripheriques.png" width="600px">
</div>
<br>

Relevez le numéro du port ***COM***. Dans l'exemple ci-dessus, la NUCLEO-WB55 est branchée sur le port ***COM3***. C'est ce numéro qu'il faudra rentrer dans votre émulateur de terminal série (PuTTY, TeraTerm...).
