---
title: Démarrer avec MicroPython et la NUCLEO-L476
description: Démarrer avec MicroPython et la NUCLEO-L476
---

# Démarrer avec MicroPython et la NUCLEO-L476

Cette section propose quelques exercices pour débuter la programmation en MicroPython avec la carte NUCLEO-L476. Nous aborderons et expliquerons au gré des tutoriels des notions de programmation embarquée et d'architecture des microcontrôleurs qui vous seront indispensables pour progresser. Lorsque vous rencontrerez un acronyme que vous ne connaissez pas, n'hésitez pas à consulter le [glossaire](../../Kit/glossaire).

## Présentation de la  NUCLEO-L476

Vous trouverez une présentation de la carte  NUCLEO-L476 de STMicroelectronics sur [cette page](../../Kit/nucleo_l476rg).

## Liste des tutoriels 

Les liens suivants donnent quelques exemples de mise en œuvre de la carte NUCLEO-L476. Nous rappelons que l'ensemble des scripts présentés ou utilisés par la suite pourront être récupérés sur la [**page Téléchargements**](../Telechargement).

>> EN COURS DE REDACTION, MERCI POUR VOTRE COMPREHENSION
