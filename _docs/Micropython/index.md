---
title: MicroPython pour STM32
description: MicroPython pour STM32 sur la NUCLEO-WB55
---

# STM32python : MicroPython pour STM32

L'initiative STM32python illustre comment mettre en œuvre MicroPython sur la famille de microcontrôleurs STM32 de STMicroelectronics.
MicroPython supporte [une longue liste de cartes de prototypage (NUCLEO et DISCOVERY) équipées de STM32](https://MicroPython.org/stm32/) et nous avons choisi [la NUCLEO-WB55](../Kit/nucleo_wb55rg) qui présente l'avantage d'embarquer le protocole de communication radiofréquence (RF)[**Bluetooth Low Energy**](BLE) ainsi que [la NUCLEO-L476](../Kit/nucleo_l476rg), dépourvue de connectivité RF mais moins onéreuse et équipée de nombreux périphériques intégrés.

<br>
<div align="left">
<img align="center" src="images/equation_stm32python.jpg" alt="stm32duino" width="400"/>
</div>
<br>


Vous trouverez dans cette partie un grand nombre de tutoriels MicroPython. A l'exception de ceux qui utilisent les fonctions BLE spécifiques à la NUCLEO-WB55 et moyennant quelques ajustements sur les noms des broches utilisées, tous les programmes que nous partageons devraient se transposer sans difficultés aux autres cartes des gammes NUCLEO de STMicroelectronics.

<br>

## Quelques mots sur MicroPython

**MicroPython** est une implémentation légère et efficace du langage de programmation Python 3 incluant un petit sous-ensemble de la bibliothèque standard Python et optimisée pour fonctionner sur des microcontrôleurs et dans des environnements contraints.

MicroPython regorge de fonctionnalités avancées telles qu'une invite en lignes de commandes, la capacité de réaliser des opérations mathématique sur des entiers de taille arbitraire, la gestion de listes, la gestion des exceptions et plus encore. Pourtant, il est suffisamment compact pour tenir et fonctionner dans seulement 256 Ko d'espace de code et 16 Ko de RAM.

* [Site web](http://MicroPython.org/)
* [Bibliothèques](http://docs.MicroPython.org/en/latest/library/index.html)

<br>

## Sommaire

* [Guide de démarrage rapide sous Windows, carte NUCLEO-WB55](../Micropython/install_win_wb55/index)
* [Guide de démarrage rapide sous Windows, carte NUCLEO-L476](../Micropython/install_win_l476/index)
* [Guide de démarrage rapide sous Linux](install_linux)
* [Le firmware MicroPython pour STM32](firmware)
* [Tutos avec la carte NUCLEO-WB55](../Micropython/STARTWB55/index)
* [Tutos avec la carte NUCLEO-L476](../Micropython/startl476/index)
* [Tutos avec la carte d'extension X-NUCLEO-IKS01A3](IKS01A3)
* [Tutos avec des modules Grove & autres](grove)
* [Tutos avec le BLE sur NUCLEO-WB55](BLE)
* [Téléchargements](Telechargement)
* [Liens utiles](ressources)
* [Foire aux Questions](../Kit/faq)