---
title: Capteur de pression intégré MPX5700AP
description: Mise en œuvre du capteur de pression intégré [MPX5700AP](https://docs.rs-online.com/6bdd/0900766b8138445c.pdf) Grove en MicroPython
---

# Capteur de pression intégré MPX5700AP

Ce tutoriel explique comment mettre en œuvre le [kit de mesure de pression Grove](https://wiki.seeedstudio.com/Grove-Integrated-Pressure-Sensor-Kit/) en MicroPython. Le capteur utilisé est basé sur un composant MPX5700AP qui renvoi une valeur analogique proportionnelle à la pression, donc entre 0 et 4095 après conversion par l'un des ADC du MCU STM32WB55RG.

## Matériel requis

1. Une [carte d'extension de base Grove](https://wiki.seeedstudio.com/Base_Shield_V2/)
2. La carte NUCLEO-WB55
3. Un [kit de mesure de pression Grove](https://wiki.seeedstudio.com/Grove-Integrated-Pressure-Sensor-Kit/)
4. Un autre capteur de pression atmosphérique absolue, pour étalonnage (voir ci-dessous)

<br>
<div align="left">
<img alt="Grove - capteur de pression intégré MPX5700AP" src="images/Grove-MPX5700AP.jpg" width="550px">
<img alt="Grove - capteur de pression intégré MPX5700AP broches" src="images/Grove-PX5700AP-pin.jpg" width="450px">
</div>
<br>

> Crédit images : [Seeed Studio](http://wiki.seeedstudio.com/Grove_System/)

Connectez le module capteur de pression intégré MPX5700AP sur la prise *A0* de la carte d'extension de base Grove.

## Quelques explications

Ce tutoriel donne un exemple trivial de conversion analogique-numérique. La conversion de la valeur analogique échantillonnée en une pression absolue (physique) mérite cependant quelques explications.

Dans un premier temps, on ne branche pas la seringue au capteur, la pression qu'il mesure est donc la pression atmosphérique absolue du lieu. On fait dix acquisitions sur l'ADC et on les somme, ce qui nous donne la valeur de `pref_analog` (6883 dans notre cas).<br>
Ensuite, à l'aide d'un capteur [BME820](bme280) ou du [LPS22HH](https://stm32python.gitlab.io/fr/docs/Micropython/IKS01A3/lps22hh) de la carte d'extension IKS01A3, on mesure la pression atmosphérique absolue du lieu. Dans notre cas, on a trouvé `pref_hpa`.<br>
Par cette méthode, on fait *l'hypothèse implicite que la réponse du capteur est linéaire*, c'est à dire que la valeur qu'il renvoie est proportionnelle à la pression exercée sur le piston de la seringue du kit. Cette hypothèse s'avère tout à fait légitime après consultation de [la fiche technique du MPX5700AP](https://docs.rs-online.com/6bdd/0900766b8138445c.pdf) :

<br>
<div align="left">
<img alt="Grove - capteur de pression intégré MPX5700AP" src="images/Linearite.jpg" width="500px">
</div>
<br>

## Le code MicroPython

> **Vous pouvez télécharger les scripts MicroPython de ce tutoriel (entre autres) en cliquant [ici](../../../assets/Script/MODULES.zip)**.

Editez maintenant le script *main.py* sur le disque *PYBFLASH* et collez-y ce code :

```python
# Lecture et numérisation du signal d'un capteur Grove de pression intégré

from pyb import Pin, ADC # Gestion de la broche analogique et de l'ADC
import time # Gestion du temps et des temporisations

# Pression de référence (atmosphérique) : 956 hPa
pref_hpa = const(956)

# Somme de dix values analogiques pour 956 hPa
pref_analog = const(6883)

ratio = pref_hpa / pref_analog

# Instance du convertisseur analogique-numérique
adc = ADC(Pin('A0'))

while True:
	
	value = 0 # Valeur analogique
	counter = 0 

	# On effectue dix conversions analogiques-numériques de la pression
	while counter < 10:
		value = value + adc.read()
		counter = counter + 1

	# Conversion de la value analogique en pression
	pressure = value * ratio

	print("Valeur analogique = %d" %value)

	print("Pression = %d hPa" %pressure)

	# Pause pendant 1 s
	time.sleep(1)
```

## Affichage sur le terminal série de l'USB User

On appuie et on relâche la seringue connectée au capteur de pression :

<br>
<div align="left">
<img alt="One Wire output" src="images/MPX5700AP_output.png" width="300px">
</div>
<br>
