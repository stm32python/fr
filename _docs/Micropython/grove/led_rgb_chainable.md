---
title: LED RGB chaînable
description: Mise en œuvre de modules Grove LED RGB chaînables avec MicroPython
---
# Mise en œuvre de modules Grove LED RGB chaînables

Ce tutoriel explique comment mettre en œuvre trois modules Grove LED RGB chaînables avec MicroPython.

## Matériel requis

1. La carte NUCLEO-WB55
2. Une [carte d'extension de base Grove](https://wiki.seeedstudio.com/Base_Shield_V2/)
3. Trois [modules Grove LED RGB chaînables](https://wiki.seeedstudio.com/Grove-Chainable_RGB_LED/)


**Le module Grove LED RGB chaînables :**

<div align="left">
<img alt="Grove - RGB LCD" src="images/Grove-Chainable_RGB_LED_V2.0.jpg" width="250px">
</div>

> Crédit image : [Seeed Studio](http://wiki.seeedstudio.com/Grove_System/)

## Objectifs de l'exercice

Connecter (en série) trois modules LED RGB chaînables et leur faire afficher dans cet ordre "bleu" (première LED), "blanc" (deuxième LED) et "rouge" (troisième LED).

Dans cet ordre ...
1. Connecter la carte **Grove Base Shield pour Arduino** sur la NUCLEO-WB55.
2. Connecter la carte X-NUCLEO-IKS01A3 sur la carte NUCLEO-WB55.
3. Construire et connecter la chaîne de modules selon la figure ci-dessous :

<br>
<div align="left">
<img alt="Grove - RGB LED chain" src="images/RGB_LED_chain.png" width="800px">
</div>
<br>

## Le code MicroPython

> **Vous pouvez télécharger les scripts MicroPython de ce tutoriel (entre autres) en cliquant [ici](../../../assets/Script/MODULES.zip)**.

Copier la bibliothèque *p9813.py* dans le dossier *PYBFLASH* et le code suivant dans le fichier *main.py* situé également dans le dossier *PYBFLASH* :

```python
# Objet du script : câbler ensemble trois modules Grove LED RGB chaînables
# et leur faire afficher dans cet ordre "bleu" (première LED), "blanc" 
# (deuxième LED) et "rouge" (troisième LED)

from machine import Pin # Pour gérer les entrées / sorties
import p9813 # Pour gérer les LED chaînables
from time import sleep # Pour temporiser

# Correspond au connecteur D7 du Grove base shield
pin_clk = Pin('D7', Pin.OUT) # Broche d'horloge
pin_data = Pin('D8', Pin.OUT) # Broche de données

# Nombre de modules LED RGB
num_led = 3

# Instanciation d'une chaîne de 3 modules LED RGB 
chain = p9813.P9813(pin_clk, pin_data, num_led)

# Eteint toutes les LED
chain.reset()

# Temporisation d'une seconde
sleep(1)

# Première LED en bleu
chain[0] = (0, 0, 255)

# Deuxième LED en blanc
chain[1] = (255, 255, 255)

# Troisième LED en rouge
chain[2] = (255, 0, 0)

# Applique les paramètres, allume les LED
chain.write()

# Change la couleur de toutes les LED en rouge
#chain.fill((0,0,255))
#chain.write()
```

## Mise en œuvre

Appuyez sur *[CTRL]-[D]* dans le terminal PuTTY. Si vous avez correctement suivi les indications les couleurs du drapeau français devraient s'afficher de gauche à droite sur la chaîne de modules.
