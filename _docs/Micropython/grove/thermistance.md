---
title: Capteur de température, thermistance générique
description: Mise en œuvre de la lecture de la température avec une thermistance en MicroPython
---

# Capteur de température, thermistance générique

Ce tutoriel explique comment mesurer et afficher une température en utilisant une thermistance avec MicroPython. Un autre tutoriel similaire, qui traite du module Grove capteur de température basé également sur une thermistance, est disponible [ici](NCP18WF104F03RB).

## Description

Une thermistance est simple à utiliser, peu coûteuse, avec une précision acceptable ce qui la rend opportune pour certains projets à budget très contenu. Les stations météo bas de gamme, les systèmes domotiques et les circuits de contrôle et de protection des équipements sont des applications où les thermistances trouvent une place idéale. On en retrouve également de très performantes (et très chères !) dans des projets dédiés au domaine spatial, à l'intérieur de sondes de grande précision.

Une thermistance (ou thermistor en anglais) est une résistance variable en fonction de la température. La tension à ses bornes va donc varier et on pourra estimer la température après une opération de conversion analogique-numérique. Il  existe **deux types** de thermistances, classées en fonction de la manière dont leur résistance réagit aux changements de température :

 - Des thermistances à coefficient de température négatif (NTC) : leur résistance diminue lorsque la température s'élève.
 - Des thermistances à coefficient de température positif (PTC) : leur résistance augmente avec la température.

Les plus communes sont les NTC et c'est également celles que nous utiliserons dans ce tutoriel. Les NTC sont fabriquées à partir d'un matériau semiconducteur qui est chauffé et compressé pour former un matériau conducteur sensible à la température.

Dans cet exemple nous allons voir comment récupérer la température puis l'afficher dans un terminal série en degrés Kelvin et en degrés Celsius.

## Montage

Pour le montage nous utilisons une thermistance ainsi qu'une résistance et c'est tout ! Cependant avant de les câbler nous avons besoin de connaitre la valeur de résistance de la thermistance. Pour cela vous pouvez utiliser la documentation du composant ou un multimètre (en position ohm-mètre). Généralement il n'existe que deux valeurs : 10 kΩ (des kilo-Ohms) et 100 kΩ. Relevez la valeur et arrondissez la au plus proche par rapport aux deux valeurs précédentes (par exemple on relève 13 kΩ au multimètre, alors c'est une 10 kΩ). Une fois cette valeur connue on choisit une résistance avec la même valeur que celle de la thermistance.

**Remarque :** Dans ce tutoriel nous utiliserons une thermistance de 10 kΩ.

On réalise le montage suivant :

<br>
<div align="left">
<img alt="Schéma de montage thermistance" src="images/thermistance-schema.png" width="600px">
</div>
<br>

On alimente le montage avec une tension de 3,3V et on utilise le port A1 de la carte NUCLEO-WB55.


<h2>Le code MicroPython</h2>

> **Vous pouvez télécharger les scripts MicroPython de ce tutoriel (entre autres) en cliquant [ici](../../../assets/Script/MODULES.zip)**.

La thermistance est un composant très simple à utiliser, son code est donc tout aussi simple. Il faut tout de même avoir quelques notions sur le convertisseur analogique numérique (ADC en anglais ou CAN en français), la loi d'Ohm et le pont diviseur de tension. Cependant ne vous faites pas de soucis, nous reviendrons sur ces différents éléments lors de la programmation.

**Etape 1 :** On importe les bibliothèques dans notre code. On retrouve ainsi une bibliothèque `ADC` pour le convertisseur analogique numérique mais aussi une bibliothèque `time` pour afficher l'intervalle de temps entre deux mesures. Enfin on importe une bibliothèque `math` pour utiliser le logarithme pendant les calculs.

```python
from pyb import ADC, Pin # Gestion du convertisseur analogique-numérique et des broches d'entrées-sorties
from time import sleep, time # Pour mesurer le temps écoulé et temporiser
from math import log # Pour calculer des logarithmes
```

**Etape 2 :** Dans un premier temps on renseigne les caractéristiques de notre circuit. On retrouve une tension d'alimentation (**Vref**) de 3,3V mais également la plage de valeurs de l'ADC (**echADC**) qui se calcule grâce à la formule echADC = 2<sup>nbitadc</sup> - 1 avec nbitadc = 12 bits. Ensuite on caractérise la thermistance à l'aide de 3 valeurs issues de la documentation technique. Respectivement on retrouve **R** la résistance caractéristique de la thermistance à 25°C (ici 10 kΩ), **B** son indice thermique et **T1** sa température absolue (exprimée en `Kelvins`, symbole : `K`).

```python

# Tension de référence / étendue de mesure de l'ADC : +3.3V
vref = const(3.3)

# Résolution de l'ADC 12 bits = 2^12 = 4096 (mini = 0, maxi = 4095)
RESOLUTION = const(4096)

# Quantum de l'ADC
quantum = vref / RESOLUTION

#Caractéristiques de la thermistance utilisée
R = 10			# Résistance à 25°C : 10kOhms
B = 3950.0		# Indice thermique
T1 = 25			# Température absolue en K
```

Dans un second temps on définit la patte A1 de la carte en tant qu'entrée analogique pour lire la valeur de tension de la thermistance. On définit également une variable (**oldtempC**) qui nous permettra de stocker l'ancienne valeur de température pour la comparer à la nouvelle valeur. Enfin on crée un variable (**temp_s**) qui mémorisera le temps en secondes au lancement du programme.

```python
# Broche A1 en entrée analogique
adc_A1 = ADC(Pin('A1'))

oldtempC = 0.0

temps_s = time()
```

**Etape 3 :** Dans un premier nous allons acquérir la température. Pour cela on récupère la valeur de tension grâce à l'ADC et on la stocke dans la variable **valeur**. Pour ensuite convertir cette valeur en tension on applique la formule suivante : tension = valeur x quantum.

Ensuite on utilise la formule du pont diviseur de tension pour obtenir la valeur de résistance de la thermistance : Vout = V<sub>in</sub> x (R<sub>2</sub> / (R<sub>1</sub> + R<sub>2</sub>)) que l'on simplifie de cette façon R<sub>2</sub> = R<sub>1</sub> x (V<sub>in</sub> / V<sub>out</sub>) et donc, dans notre cas, R<sub>t</sub> = R x (tension / (V<sub>ref</sub> - tension)).

Enfin, à partir de cette formule, on calcule la température en (degrés) Kelvin avec la formule : tempK = 1/(1/(273.15 + T<sub>1</sub>) + log(R<sub>t</sub>/R)/B). 

**Remarque** : la dernière formule est générale. Cependant certains constructeurs fournissent leur propre formule qui peut être différente, mais le principe expliqué ici reste le même.

```python
while True:

	# Lecture de la broche A1
	value = adc_A1.read()
	
	# Calcul de la tension
	voltage = value * quantum
	
	# Calcul de la valeur de résistance de la thermistance
	Rt = R * voltage / (Vref - voltage)
	
	# Calcul des températures en Kelvins et degrés Celsius
	tempK = 1/(1/(273.15 + T1) + math.log(Rt/R)/B)
	tempC = tempK - 273.15
```

Finalement, et toujours dans la boucle *while*, on réalise l'affichage. On souhaite afficher la température uniquement lorsque celle-ci varie de ±0,2°C afin d'avoir un affichage clair et propre. Si cette condition n'est pas remplie alors on mémorise la nouvelle température dans la variable **oldtempC** puis on affiche le temps en secondes et les températures en degrés Celsius et Kelvin.

```python
	# Affichage du résultat (si valeur dépassée à 0,2 près)
	if((tempC < (oldtempC-0.2)) or (tempC > (oldtempC+0.2))):
		oldtempC = tempC
		print("-"*21, "Temps :", time.time()-temps_s , "s", "-"*21)
		print("Température : %.2f°C\t\tTemperature : %.2f°K" %(tempC, tempK))
	# Temps de pause
	time.sleep(1)
```

**Et voici le code complet après quelques optimisations de syntaxe :**

```python
# Objet du script : mesurer et afficher une température en utilisant une thermistance avec MicroPython.
# Matériel requis en supplément de la NUCLEO-WB55 : une thermistance et une résistance.

from pyb import ADC, Pin # Gestion du convertisseur analogique-numérique et des broches d'entrées-sorties
from time import sleep, time # Pour mesurer le temps écoulé et temporiser
from math import log # Pour calculer des logarithmes

# Tension de référence / étendue de mesure de l'ADC : +3.3V
varef = const(3.3)

# Résolution de l'ADC 12 bits = 2^12 = 4096 (mini = 0, maxi = 4095)
RESOLUTION = const(4096)

# Quantum de l'ADC
quantum = varef / RESOLUTION

# Caractéristiques de la thermistance utilisée
R = 10 # Résistance à 25°C : 10 kOhms
B = 3950.0 # Indice thermique
T1 = 25 # Température absolue en K

# Broche A1 en entrée analogique
adc_A1 = ADC(Pin('A1'))

oldtempC = 0.0
temps_s = time()

while True:
	
	# Lecture de la broche A1
	value = adc_A1.read()
	
	# Calcul de la tension
	voltage = value * quantum
	
	# Calcul de la valeur de résistance de la thermistance
	Rt = R * voltage / (Vref - voltage)
	
	#Calcul des températures en Kelvins et degrés Celsius
	tempK = 1/(1/(273.15 + T1) + log(Rt/R)/B)
	tempC = tempK - 273.15

	# Affichage du résultat (si valeur dépassée à 0,2 près)
	if tempC < (oldtempC-0.2) or tempC > (oldtempC+0.2):
		oldtempC = tempC
		print("-"*21, "Temps :", time() - temps_s , "s", "-"*21)
		print("Température : %.2f°C\t\tTempérature : %.2f°K" %(tempC, tempK))
		
	# Temps de pause
	sleep(1)
```


<h2>Résultat</h2>

Il ne vous reste plus qu'à sauvegarder l'ensemble puis à redémarrer votre carte NUCLEO-WB55.<br>
Placez votre doigt sur la thermistance pour la chauffer ou placez là dans un courant d'air pour la refroidir. Vous devriez alors obtenir un résultat similaire à celui-ci :

<br>
<div align="left">
<img alt="Affichage de la température" src="images/thermistance-python.PNG" width="50%">
</div>
<br>

**Remarque :** On obtient bien la température mais on remarque qu'elle n'est pas des plus précises. En effet la précision de la mesure dépend à la fois de la précision de la thermistance, de la résistance utilisée dans le montage et de la précision du calcul dans le programme.

