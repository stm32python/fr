---
title: Capteur de CO2 SCD30
description: Mise en œuvre du capteur de CO2 SCD30 Grove avec MicroPython
---

# Capteur de dioxyde de carbone SCD30

Ce tutoriel explique comment mettre en œuvre le module Grove I2C capteur de gaz carbonique SCD30 avec MicroPython. Le [SCD30 de Sensirion](https://www.sensirion.com/fileadmin/user_upload/customers/sensirion/Dokumente/9.5_CO2/Sensirion_CO2_Sensors_SCD30_Datasheet.pdf) est basé sur une technologie d'illumination infrarouge non-dispersive. 

Ce capteur renvoie des valeurs en **parties par million** (symbole ppm). Le ppm permet de savoir combien de molécules du gaz mesuré (en l'occurrence le CO<sub>2</sub>) on trouve sur un million de molécules d'air. Il peut mesurer des concentrations entre 0 et 40000 ppm avec une précision (reproductibilité) de 30 ppm dans la plage 400 - 10000 ppm (d'où la nécessité de le calibrer, comme tous les capteurs de toutes façons !).

La concentration moyenne de CO<sub>2</sub> dans l'air extérieur est de l'ordre de 300 à 400 ppm. Les niveaux intérieurs sont en général plus élevés, en raison du CO<sub>2</sub> exhalé par les occupants du bâtiment. Il est intéressant de surveiller la concentration en CO<sub>2</sub> à l'intérieur d'un logement car celle-ci donne une assez bonne estimation de la qualité de son aération. Les valeurs limites supérieures réglementaires actuelles varient entre 1000 et 1500 ppm ([source: ANSES](https://www.anses.fr/fr/content/dioxyde-de-carbone-co2-dans-l%E2%80%99air-int%C3%A9rieur)).

**Le module Grove SCD30 :**

<br>
<div align="left">
<img alt="Grove - SCD30" src="images/grove_scd30.jpg" width="500px">
</div>
<br>

> Crédit image : [Seeed Studio](http://wiki.seeedstudio.com/Grove_System/)

## Matériel requis

1. Une [carte d'extension de base Grove](https://wiki.seeedstudio.com/Base_Shield_V2/)
2. La carte NUCLEO-WB55
3. Un [module SCD30 de Grove](https://wiki.seeedstudio.com/Grove-CO2_Temperature_Humidity_Sensor-SCD30/)

Le capteur doit être connecté *sur l'un des connecteurs I2C du Grove Base Shield*.

## Le code MicroPython

> **Vous pouvez télécharger les scripts MicroPython de ce tutoriel (entre autres) en cliquant [ici](../../../assets/Script/MODULES.zip)**.

Ce code est adapté de [l'exemple fourni ici](https://pypi.org/project/MicroPython-scd30/).
Il faut ajouter le fichier *scd30.py* dans le répertoire du périphérique PYBFLASH.<br>
Editez maintenant le script *main.py* et collez-y le code suivant :

```python
# Objet du script : Mise en œuvre du module I2C Grove SCD30.
# Source : https://pypi.org/project/micropython-scd30/

from time import sleep_ms
from machine import I2C, Pin
from scd30 import SCD30

# On utilise l'I2C n°1 de la carte NUCLEO-W55 pour communiquer avec le capteur
i2c = I2C(1)

# Pause d'une seconde pour laisser à l'I2C le temps de s'initialiser
sleep_ms(1000)

# Liste des adresses I2C des périphériques présents
print("Adresses I2C utilisées : " + str(i2c.scan()))

# Instanciation du capteur
I2C_ADDRESS = const(0x61)
scd30 = SCD30(i2c, addr = I2C_ADDRESS)

# Corrige la pression grâce à l'altitude du lieu (en m)
altitude = 0

while True:

	# Attend que le capteur renvoie des valeurs (par défaut toutes les 2 secondes)
	while scd30.get_status_ready() != 1:
		sleep_ms(200)

	# Lecture des valeurs mesurées
	scd30data = scd30.read_measurement() 

	# Séparation et formattage (arrondis) des mesures
	conco2 = int(scd30data[0])
	temp = round(scd30data[1],1)
	humi = int(scd30data[2])

	# Affichage des mesures
	# Syntaxe utilisant la conversion en chaines de caractères plutôt que
	# les instructions de formattage de Python
	print('=' * 40) # Imprime une ligne de séparation
	print("Concentration en CO2 : " + str(conco2) + " ppm")
	print("Température : " + str(temp) + " °C")
	print("Humidité relative : " + str(humi) + " %")

```

## Sortie sur le port série de l'USB USER

Appuyez sur *[CTRL]-[D]* dans le terminal PuTTY et observez les valeurs qui défilent :

<br>
<div align="left">
<img alt="Grove - SCD30 sortie" src="images/grove_scd30_output.png" width="450px">
</div>
<br>