---
title: Horloge temps-réel PCF85063TP
description: Mise en œuvre du module Grove Horloge temps-réel de précision PCF85063TP en MicroPython
---

# L'horloge temps-réel PCF85063TP

Ce tutoriel explique comment mettre en œuvre un [module I2C Grove horloge temps-réel de précision PCF85063TP](https://wiki.seeedstudio.com/Grove_High_Precision_RTC/) en MicroPython, basé sur le composant PCF85063TP de NXP ([fiche technique](https://www.nxp.com/docs/en/data-sheet/PCF85063TP.pdf).

**Le module Grove High Precision RTC :**

<div align="left">
<img alt="Grove - High Precision RTC (PCF85063TP)" src="images/PCF85063TP.jpg" width="300px">
</div>

> Crédit image : [Seeed Studio](http://wiki.seeedstudio.com/Grove_System/)

Ce module RTC (pour "Real-Time Clock" en anglais) remplis la fonction d’une horloge calendrier très précise pour des problématiques d’horodatage. Son pilote se concentre sur l'essentiel ; il offre moins de fonctions que celui de la RTC intégrée au STM32WB55, [présentée ici](https://stm32python.gitlab.io/fr/docs/Micropython/startwb55/rtc). Mais il est plus facile d'usage pour des projets simples. Il est équipé de sa propre pile bouton ce qui permet, une fois qu'on l'a mis à l'heure et à la bonne date, de conserver le suivi du temps lorsque la NUCLEO-WB55 est déconnectée de son alimentation. 


## Matériel requis

1. Une [carte d'extension de base Grove](https://wiki.seeedstudio.com/Base_Shield_V2/)
2. La carte NUCLEO-WB55
3. Un [module Grove horloge temps-réel de précision PCF85063TP](https://wiki.seeedstudio.com/Grove_High_Precision_RTC/)
4. Une pile bouton 5V Lithium CR 1225 (pour le module RTC)


## Le code MicroPython

> **Vous pouvez télécharger les scripts MicroPython de ce tutoriel (entre autres) en cliquant [ici](../../../assets/Script/MODULES.zip)**. 

Il faut ajouter le fichier *pcf85063tp.py* dans le répertoire du périphérique *PYBFLASH*.<br>
Editez maintenant le script *main.py* :

```python
# Objet du script :
# Démonstration de la mise en œuvre d'un module High Precision RTC utilisant un composant
# PCF85063TP de NXP (fiche technique : https://www.nxp.com/docs/en/data-sheet/PCF85063TP.pdf)

from pcf85063tp import RTC_HP # Pilote du PCF85063TP
from machine import I2C # Pilote du bus I2C
from time import sleep # Pour temporiser

# On utilise l'I2C n°1 de la carte NUCLEO-WB55 pour communiquer avec le capteur
i2c = I2C(1) 

# Pause d'une seconde pour laisser à l'I2C le temps de s'initialiser
sleep(1)

# On crée une instance de la RTC
rtc = RTC_HP(i2c)

# On fixe la date au 8 août 2021
rtc.fillByYMD(2021,8,10)

# On fixe l'heure à 8h 15 minutes et 22 secondes
rtc.fillByHMS(8,15,22)

# On fixe le jour de la semaine à "MARDI"
rtc.fillDayOfWeek('MAR')

# On démarre l'horlode du module RTC
rtc.startClock()

# On affiche pendant une minute, toutes les secondes, l'heure et la date
for i in range(60):
	print(rtc.readTime())
	sleep(1)

# On arrête l'horloge du module RTC
#rtc.stopClock()
```

## Sortie sur le port série de l'USB USER

Appuyez sur *[CTRL]-[D]* dans le terminal PuTTY et observez les valeurs qui défilent :

<br>
<div align="left">
<img alt="Grove - High Precision RTC sortie" src="images/grove_hprtc_output.png" width="350px">
</div>
<br>

## Paramétrage en mode REPL

Pour le paramétrage de cette RTC, le mode REPL de l'interpréteur MicroPython est particulièrement adapté. Il vous sera plus facile de régler l'horloge avec une précision d'une seconde en mode REPL plutôt qu'en exécutant le script ci-dessus.
La copie d'écran ci-dessous illustre la mise à l'heure de la RTC en mode REPL via le terminal PuTTY et son interrogation par la suite pour s'assurer qu'elle fonctionne correctement :

<br>
<div align="left">
<img alt="Grove - High Precision RTC REPL" src="images/grove_hprtc_repl.png" width="550px">
</div>
<br>

**Note :** En mode REPL, lorsque vous tapez "rtc." puis TAB, le terminal affiche toutes les méthodes disponibles pour la classe correspondante.
