---
title: Clavier matriciel
description: Mise en œuvre de la lecture d'une touche avec un clavier matriciel
---

# Clavier matriciel

Ce tutoriel explique comment récupérer la touche appuyée sur un clavier matriciel avec MicroPython.

<h2>Description</h2>

Un clavier matriciel, également appelé keypad en anglais, permet de récupérer des caractères (chiffres 0 à 9, lettres A à D, symboles * et #).

Ses touches sont disposées en matrice de 4x4 ou 3x4 ; ce sont de simples boutons poussoirs.
Le dispositif est passif et comprend 7 ou 8 pattes physiques :
 - 4 pattes reliées aux touches d'une même colonne
 - 3 ou 4 pattes reliées aux touches d'une même ligne

<br>
<div align="left">
<img alt="Fonctionnement keypad" src="images/keypad-decomposition.png" width="80%">
</div>
<br>

> Crédit image : [Mbed](https://os.mbed.com/components/Keypad-12-Button-COM-08653-ROHS/)

Le clavier doit être utilisé comme indiqué comme ci-dessous :
1. Quatre pattes de la carte doivent être configurées comme sorties et quatre autres comme entrées. Des résistances de tirage peuvent être ajoutées afin de fixer l'état logique quand aucune touche n'est pressée.
2. Les pattes en sorties sont mises à l'état haut et on lit l'état des pattes en entrée. En pressant une touche cela fera apparaître un état logique haut sur une des pattes d'entrée.
3. En combinant les 0 et les 1 sur les pattes en sortie on peut déterminer quel bouton est pressé.


<h2>Montage</h2>

**Montage 1 :** Pour un clavier 3x4 on réalise le montage suivant :

<br>
<div align="left">
<img alt="Schéma de montage 1 keypad" src="images/keypad-schema1.png" width="70%">
</div>
<br>

Lors du câblage faites attention à bien laisser une patte à chaque extremum de la connectique pour ce clavier ci.

**Montage 2 :** Pour un clavier 4x4 on réalise le montage suivant :

<br>
<div align="left">
<img alt="Schéma de montage 2 keypad" src="images/keypad-schema2.png" width="70%">
</div>
<br>

Comme expliqué précédemment le dispositif est passif et n'a donc pas besoin d'alimentation. On peut également rajouter des résistances de tirage de 1kOhms.

<h2>Le code MicroPython</h2>

> **Vous pouvez télécharger les scripts MicroPython de ce tutoriel (entre autres) en cliquant [ici](../../../assets/Script/MODULES.zip)**.

Afin de simplifier le code et le rendre plus propre nous avons créé une bibliothèque externe *keypad.py*. Téléchargez-la et déposez-la dans la carte NUCLEO-WB55. Cette bibliothèque nous permet d'éviter de programmer les étapes décrites plus haut. Cependant vous pouvez toujours l'ouvrir et observer la structure du code.

**Etape 1 :** Pour faire fonctionner le programme nous devons importer la bibliothèque téléchargée précédemment.
Dans le cas du **montage 1** avec un clavier 3x4 :

```python
from keypad import Keypad3x4
```
Dans le cas du **montage 2** avec un clavier 4x4 :

```python
from keypad import Keypad3x4
```

**Etape 2 :** On crée l'entité du clavier.
Dans le cas du **montage 1** avec un clavier 4x4 :

```python
keyboard = Keypad3x4()
```

Dans le cas du **montage 2** avec un clavier 4x4 :

```python
keyboard = Keypad4x4()
```

**Etape 3 :** Enfin on lis la touche et on l'affiche dans le terminal série.

```python
while True:
	key = keyboard.read_key()
	print(touche)
```

<h2>Résultat</h2>

Il ne vous reste plus qu'à sauvegarder l'ensemble puis à redémarrer votre carte NUCLEO-WB55. Essayez les différentes touches, celles-ci devraient s'afficher dans le moniteur série.

<h2>Pour aller plus loin : programmer un digicode</h2>

> **Vous pouvez télécharger les scripts MicroPython de ce tutoriel (entre autres) en cliquant [ici](../../../assets/Script/MODULES.zip)**.

Vous trouverez dans le fichier *Clavier matriciel/Pour aller plus loin/*  de l'archive *MODULES.zip* le script MicroPython *main.py* qui implémente un digicode. Ce script n'utilise pas la bibliothèque *keypad.py** et permet peut être de mieux comprendre le principe de fonctionnement du keypad. Le voici :

```python
# Objet du script : 
# Mise en œuvre d'un digicode avec le keypad.

from pyb import Pin, ExtInt
from time import sleep
print("start")

#*** Définition des vecteurs d'entrée, de sortie et d'interruptions ***
IN = [0]*4
OUT = [0]*4
IRQ = [0]*4

#*** Définition des connexions physiques ***
ROW = ['A15', 'C10', 'A10', 'C6']
COLUMN = ['A9','C12', 'C13', 'A8']

#*** Définition de la matrice représentative du clavier ***
MATRIX = [[1,2,3,'A'],[4,5,6,'B'],[7,8,9,'C'],['*',0,'#','D']]	

#*** Attribution des sorties ***
for a in range(4):
	OUT[a] = Pin(ROW[a], Pin.OUT)
	OUT[a].value(1)

blue_led = pyb.LED(3)
green_led = pyb.LED(2)
red_led = pyb.LED(1)

#*** Flags et fonctions d'interruption ***
flag = 0
flag_BP = 0

def inter(line):	# Appui keypad
	global flag
	sleep(0.2)
	flag = 1
	
def BP(line):		# Appui SW1
	global flag_BP
	global nb_input
	print("Saisir nouveau code : ")
	blue_led.on()
	flag_BP = 1
	nb_input = 0

#*** Attribution des entrées en interruption ***
for a in range(4):
	IN[a] = pyb.Pin( COLUMN[a] , pyb.Pin.IN)
	IN[a].init(pyb.Pin.IN, pyb.Pin.PULL_DOWN, af=-1)
	IRQ[a] = ExtInt(IN[a], ExtInt.IRQ_RISING, Pin.PULL_DOWN, inter)
	
sw1 = pyb.Pin('SW1')
sw1.init(pyb.Pin.IN, pyb.Pin.PULL_UP, af=-1)
irq_BP = pyb.ExtInt(sw1, pyb.ExtInt.IRQ_FALLING, pyb.Pin.PULL_UP, BP)

#*** Définition des variables permettant la gestion du digicode ***	
old_carac = -1
nb_input = 0
code = [1,2,3,'A']
input_code = [0]*4

#*** Fonction qui gère l'appuie sur une touche du keypad ***
def appuie():
	global flag
	global old_carac
	global nb_input
	global input_code
	global code
	
	flag = 0
		
	for i in range(4):
		if(IN[i].value()==1):	# Test quelle colonne est concernée par l'appuie 
			for j in range(4):	# Eteint les différentes lignes
				OUT[j].value(0);
				if(IN[i].value()==0):
					# Trouve le bouton appuyé et l'affiche en gérant les rebonds et 
					# supprimant les appuies longs
					if (MATRIX[i][j]!=old_carac):
						print(MATRIX[i][j])
						old_carac = MATRIX[i][j]
						input_code[nb_input] = MATRIX[i][j]
						
						if(flag_BP == 1):
							code[nb_input] = MATRIX[i][j]	# Change le code
							 
						nb_input = nb_input + 1
						
					for k in range(4):
						OUT[k].value(1)		# Rallume toutes les lignes
					break;	

#*** Fonction qui teste si le code entré est le bon ***
def test_code():
	global nb_input
	
	for i in range(4):
		if(code[i] != input_code[i]):
			print("Code faux")
			red_led.on()
			nb_input = 0
			sleep(2)
			red_led.off()
			print("\nEntrez le code à 4 chiffres : ")
			return -1
	print("Code juste")
	green_led.on()
	nb_input = 0
	sleep(2)
	green_led.off()
	print("\nEntrez le code à 4 chiffres : ")

			
#*** Début du programme principal ***
print("Entrez le code à 4 chiffres : ")	
	
while True:
	if((flag == 1) and (nb_input < 4)):	# Si une touche a été appuyée
		appuie()
		
	if (nb_input >= 4):	# Si le code est saisie en entier
		
		if(flag_BP==1):		# Gestion du nouveau code
			flag_BP=0
			print("Le nouveau code est : ", code[0], code[1], code[2], code[3])
			nb_input = 0
			sleep(2)
			blue_led.off()
			print("\nEntrez le code à 4 chiffres : ")	
			
		else:
			test_code()

```

**Principe de fonctionnement du digicode :** 
- On définit un code par défaut (123A).
- L'utilisateur peut saisir en boucle un code à 4 chiffres.
- Si le code de l'utilisateur correspond au code en mémoire alors la LED verte de la carte s'allume pendant 2 secondes et le message "Code juste" apparaît sur le terminal. 
- En revanche, si les codes ne correspondent pas, la LED rouge s'allume et le message "Code faux" est inscrit sur le terminal.

L'utilisateur peut choisir de lui-même définir le code secret en pressant le premier bouton poussoir de la carte (SW1). La LED bleue s'allume pendant toute la durée de la saisie du nouveau code et 2 secondes après. Sur le terminal, on peut voir l'instruction de saisir un nouveau code et ce code est inscrit une fois qu'il a été renseigné.

**Attention, on ne peut pas saisir 2 fois le même caractère à la suite avec ce code !**

En cas de problème vérifiez votre câblage et/ou votre code pour voir si vous utilisez bien le bon type de clavier.
