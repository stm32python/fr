---
title: Afficheur LCD 16 caractères x 2 lignes Grove V2.0 (ou plus)
description: Mettre en œuvre un afficheur LCD 16 caractères x 2 lignes V2.0 avec MicroPython
---

# Mise en œuvre de l'afficheur LCD 16x2 Grove V2.0 (ou plus)

Ce tutoriel explique comment mettre en œuvre un module Grove afficheur I2C LCD 16 caractères x 2 lignes V2.0 (ou plus) avec MicroPython. Nous allons simplement montrer comment afficher des messages sur le LCD.

**Les modules Grove LCD 16x2 :**

<br>
<div align="left">
<img alt="Grove - 16x2 LCD" src="images/grove-lcd_16x2_display.jpg" width="650px">
</div>
<br>

> Crédit image : [Seeed Studio](http://wiki.seeedstudio.com/Grove_System/)

## Matériel requis

1. La carte NUCLEO-WB55
2. Une [carte d'extension de base Grove](https://wiki.seeedstudio.com/Base_Shield_V2/)
3. Un [module LCD Grove 16x2 V2.0 (ou plus)](https://wiki.seeedstudio.com/Grove-16x2_LCD_Series/)

Dans cet ordre ...
1. Connecter la carte **Grove Base Shield pour Arduino** sur la NUCLEO-WB55.
2. Connecter l'afficheur sur un connecteur I2C.

## Le code MicroPython

> **Vous pouvez télécharger les scripts MicroPython de ce tutoriel (entre autres) en cliquant [ici](../../../assets/Script/MODULES.zip)**.

Copier la bibliothèque *i2c_lcd.py* dans le dossier *PYBFLASH*.<br>
Copiez le code suivant dans le fichier *main.py* et faites le glisser dans le dossier *PYBFLASH* :

```python
# Objet du script :
# Mise en œuvre du module Grove LCD 16x2 I2C
# Bibliothèques pour le LCD copiées et adaptées depuis ce site : 
# https://GitHub.com/Bucknalla/MicroPython-i2c-lcd

from time import sleep # pour temporiser
from machine import I2C # Pilote du bus I2C
from i2c_lcd import lcd # Pilote du module LCD 16x2

# On utilise l'I2C n°1 de la carte NUCLEO-WB55 pour communiquer avec le capteur
i2c = I2C(1) 
# Pause d'une seconde pour laisser à l'I2C le temps de s'initialiser
sleep(1)

# Liste des adresses I2C des périphériques présents
print("Adresses I2C utilisées : " + str(i2c.scan()))

# Instanciation de l'afficheur
display = lcd(i2c)

while True:

	# Positionne le curseur en colonne 1, ligne 1
	col = 1
	row = 1
	display.setCursor(col - 1, row - 1)

	# Affiche "Hello World"
	display.write('Hello World')

	# Positionne le curseur en colonne 1, ligne 2
	col = 1
	row = 2
	display.setCursor(col - 1, row - 1)

	# Affiche "Bonjour Monde"
	display.write('Bonjour Monde')

	# Attends cinq secondes
	sleep(5)

	# Efface l'afficheur
	display.clear()

	# Attends cinq secondes
	sleep(5)
```

### **Résultat**

Après avoir sauvegardé et redémarré le script sur votre carte NUCLEO-WB55 vous pourrez voir les messages "Hello World" et "Bonjour Monde" s'afficher et s'effacer toutes les cinq secondes.

## Remarque importante : I2C et Pull-up pour des révisions antérieures à la V2.0

Attention, **pour qu'une communication I2C fonctionne avec un périphérique, il faut que ses broches SCL et SDA aient un potentiel porté à +3.3V**. Or, les modules Grove LCD de version antérieure à la 2.0 n'intègrent pas ces résistances, et les cartes NUCLEO non plus.

En pratique, **comme expliqué sur [la page Wiki de l'afficheur](https://wiki.seeedstudio.com/Grove-16x2_LCD_Series/)**, des résistances dites "de tirage" (pull-up en anglais) ont été ajoutées pour répondre à cette problématique sur la version 2.0 de l'afficheur. Donc *si vous souhaitez utiliser un module Grove LCD 16x2 dans sa première révision*, il ne fonctionnera pas à moins que vous ajoutiez en parallèle, sur les broches SCL et SDA, les résistances de tirage requises. 

Cette problématique est bien expliquée [ici](https://www.fabriqueurs.com/afficheur-lcd-2-lignes-de-16-caracteres-interface-i2c/) et par le schéma ci-dessous :

<br>
<div align="left">
<img alt="Grove - 16x2 LCD" src="images/lcb_rgb_grove_pullup.png" width="600px">
</div>
<br>

Cette remarque est évidemment applicable à **n'importe quel autre module I2C** que vous viendriez connecter à la NUCLEO-WB55 (ou bien à toute autre carte NUCLEO) et qui ne possèderait pas les deux résistances de tirage.
