---
title: Compteur de particules HM3301
description: Mise en œuvre compteur de particules HM3301 Grove avec MicroPython
---

# Compteur de particules HM3301

Ce tutoriel explique comment mettre en œuvre un module compteur de particules HM3301 dont le principe de fonctionnement est résumé par la figure qui suit :

<br>
<div align="left">
<img alt="Principe d'un compteur de particules" src="images/compteur_de_particules.jpg" width="480px">
</div>
<br>

Le système de ventilation du capteur souffle de l'air prélevé dans l'environnement à travers une canalisation interne au capteur. Cette canalisation est traversée à un endroit par un laser infrarouge. Les poussières et particules portées par le flux d'air diffusent la lumière du laser lorsqu'elles le traversent. Une partie de la lumière diffusée est capturée par un miroir qui la renvoie sur un photo-détecteur. Un algorithme analyse ensuite le signal de ce détecteur pour estimer la concentration des poussières dans l'air et leurs dimensions.

Vous trouverez [ici](HM-3300&3600_V2.1.pdf) la fiche technique de ce module.

**Le module Grove compteur de particules :**

<br>
<div align="left">
<img alt="Grove  Laser PM2.5 Sensor" src="images/Grove_HM3301.jpg" width="380px">
</div>
<br>

> Crédit image : [Seeed Studio](http://wiki.seeedstudio.com/Grove_System/)

## Matériel requis et montage

1. Une [carte d'extension de base Grove](https://wiki.seeedstudio.com/Base_Shield_V2/)
2. La carte NUCLEO-WB55
3. Un [module Grove - Laser PM2.5 Sensor (HM3301)](https://wiki.seeedstudio.com/Grove-Laser_PM2.5_Sensor-HM3301/)

Connectez le capteur sur **une broche I2C**.

## Le code MicroPython

> **Vous pouvez télécharger les scripts MicroPython de ce tutoriel (entre autres) en cliquant [ici](../../../assets/Script/MODULES.zip)**.

Commencez par copier le fichier *hm3301.py* dans le dossier *PYBFLASH*.<br>
Créez ensuite un fichier *main.py* dans *PYBFLASH* et copiez-collez dans celui-ci le code qui suit :

```python
# Objet du script : Mise en œuvre du module Grove - Laser PM2.5 Sensor (HM3301)
# Fiche technique : https://files.seeedstudio.com/wiki/Grove-Laser_PM2.5_Sensor-HM3301/res/HM-3300%263600_V2.1.pdf
# Ce module donne une estimation de la masse moyenne des particules présentes dans un mètre-cube d'air
# selon leur diamètre approximatif : 1 µm, 2.5 µm ou 10 µm.

from time import sleep_ms # Pour gérer les temporisations
from machine import I2C # Pour gérer l'I2C
import hm3301 # Pour gérer le capteur 

# On utilise l'I2C n°1 de la carte NUCLEO-WB55 pour communiquer avec le capteur
i2c1 = I2C(1)

# Pause d'une seconde pour laisser à l'I2C le temps de s'initialiser
sleep_ms(1000)

# Liste des adresses I2C des périphériques présents
print("Adresses I2C utilisées : " + str(i2c1.scan()))

# Instanciation du capteur
sensor = hm3301.HM3301(i2c=i2c1)

while True :
	
	# Concentration massique des particules de taille 1 µm
	std_PM1 = sensor.getData(0)
	# Concentration massique des particules de taille 2.5 µm
	std_PM2_5 = sensor.getData(1)
	# Concentration massique des particules de taille 10 µm
	std_PM10 = sensor.getData(2)
	
	# Affichage
	print("Concentration des particules de taille 1 µm : %d µg/m^3" % std_PM1)
	print("Concentration des particules de taille 2,5 µm : %d µg/m^3" % std_PM2_5)
	print("Concentration des particules de taille 10 µm : %d µg/m^3\n" % std_PM10)
	
	# Temporisation de 5 secondes
	sleep_ms(5000)
```
## Affichage sur le terminal série

Appuyez sur *[CTRL]-[D]* dans le terminal série connecté à la NUCLEO-WB55. Une série de valeurs sera affichée dans le terminal :

<br>

<div align="left">
<img alt="Grove sunlight sensor output" src="images/Grove_particles_counter.jpg" width="600px">
</div>

<br>

L'augmentation du décompte de particules, dans la troisième série de mesures en partant du haut, est la conséquence d'un acte de gaspillage assumé : nous avons déchiré une feuille de papier devant la prise d'air du capteur !<br>
Ceci explique pourquoi, dans les salles blanches où l'on fabrique les puces électroniques, le papier ordinaire est proscrit car il produit, lorsqu'on le déchire, des quantités phénoménales de particules potentiellement fatales aux circuits intégrés encore en cours de fabrication.
