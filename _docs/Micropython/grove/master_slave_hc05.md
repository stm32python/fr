---
title: Liaison maître - esclave avec des modules Bluetooth HC-05
description: Coupler deux modules Bluetooth HC-05 dans une liaison maître-esclave et commander un servomoteur à distance avec un potentiomètre.
---

# Liaison maître - esclave avec des modules Bluetooth HC-05

Ce tutoriel explique comment réaliser une liaison maître - esclave entre deux [module Bluetooth HC-05](https://files.seeedstudio.com/wiki/Grove-Serial_Bluetooth_v3.0/res/Bluetooth_module.pdf) pour commander à distance un servomoteur avec un potentiomètre. Le cas d'usage qui nous intéresse est illustré par la figure suivante :

<br>
<div align="left">
<img alt="Bluetooth, connexion maître - esclave" src="images/bt_master_slave.jpg" width="900px">
</div>
<br>

* **Dans un premier temps, on établit la connexion entre les deux modules, à l'aide de commandes "AT"** (voir plus loin). La liste des commandes "AT" du module HC-05 est disponible [dans ce document](HC-05_Datasheet.pdf).
* **Dans un deuxième temps, une fois la connexion établie entre les deux modules**, la carte NUCLEO-WB55 qui porte le module configuré esclave envoie l'angle de rotation du servomoteur, encodé par son potentiomètre, à la carte NUCLEO-WB55 qui porte le module Bluetooth configuré en maître ainsi que le servomoteur.

Le module HC-05 est basé sur un composant [Cambridge Silicon Radio CSR BlueCore (BC417)](CSR-BC417-datasheet.pdf), il ressemble à ceci :

|Module Bluetooth HC-05, face avant|Module Bluetooth HC-05, face arrière et broches|
|:-:|:-:|
|<img alt="Module Bluetooth HC-05, face avant" src="images/Module-Bluetooth HC-05.jpg" width="210px">| <img alt="Module Bluetooth HC-05, dos" src="images/Module-Bluetooth HC-05_dos.jpg" width="210px">|


> Crédit images : [AZDelivery](https://www.az-delivery.de/fr/products/hc-05-6-pin)

**Sa connectique** est la suivante :

* **Les broches TX (émission, sortie) et RX (réception, entrée)** doivent être connectées aux lignes de l'UART 2 de la NUCLEO-WB55.
* **La broche +5V (parfois VCC)** doit être connectée à la broche 5V de la NUCLEO-WB55. Même si ce module est alimenté en 5V, ses broches RX et TX utilisent des niveaux logiques de commande 3.3V appropriés pour un microcontrôleur STM32.
* **La broche GND** doit être reliée à l'une des masses de la NUCLEO-WB55.
* **La broche EN (Enable, entrée)**, optionnelle, permet d'activer (niveau haut) ou désactiver (niveau bas) le mode "AT commands" du module. Le **bouton sur la face avant du module** (s'il est présent) a la même fonction. On en reparle plus loin.
* **La broche STATE (Etat, sortie)**, optionnelle, indique selon son niveau (haut ou bas) si le module est apparié ou pas. 

<br>

**NB 1 :** Le composant BC417 est le même que celui qui équipe les [modules Grove Serial Bluetooth v3.01](CSR-BC417-datasheet.pdf), mais les commandes "AT" de ces derniers ne sont pas identiques à celles des modules HC-05. La connexion directe entre deux modules Grove Serial Bluetooth v3.01 s'est en outre révélée compliquée à mettre en œuvre c'est pourquoi nous avons finalement décidé d'écrire un tutoriel basé sur le HC-05.

**NB 2 :** On trouve sur Internet un grand nombre de fournisseurs pour les modules HC-05, et notre expérience a démontré qu'une partie de ces sociétés expédiait des modules défaillants (contrefaits ?) ou programmés avec des firmwares "exotiques" non documentés. Donc, pour commander vos modules HC-05, nous vous conseillons de choisir des intermédiaires fiables et connus, disposés à fournir un SAV si nécessaire.

**NB 3 :** Vous trouverez aussi sur Internet des références au [**module Bluetooth HC-06**](HC-06_Datasheet.pdf ). Quelle est la différence entre un module HC05 et un module HC06 ?<br>
Le HC-05 peut être maître (il peut proposer à un autre objet Bluetooth de s’appairer avec lui) ou esclave (il ne peut que recevoir des demandes d’appairage d’autres objets Bluetooth), tandis que le HC-06 ne peut être qu’esclave. Donc, dans notre tutoriel, nous aurions pu utiliser un module HC-05 et un module HC-06.
 
## Bluetooth versus Bluetooth Low Energy ?

Il ne faut pas confondre [le Bluetooth](https://fr.wikipedia.org/wiki/Bluetooth) avec [le Bluetooth basse consommation (Bluetooth Low Energy ou Bluetooth Smart), désigné par l'acronyme BLE](https://fr.wikipedia.org/wiki/Bluetooth_%C3%A0_basse_consommation). Selon Wikipédia, les modes BLE (bande passante plus limitée et très faible consommation) et Bluetooth standard (niveau d’émission plus élevé et portée plus grande) sont des technologies **complémentaires**. Vous trouverez plus d'informations concernant le BLE [sur cette page](../BLE/index.).

La carte [NUCLEO-WB55](../../Kit/nucleo_wb55rg) intégrant la norme BLE via son SoC STM32WB55RG, il peut paraître futile d'y ajouter un module externe Bluetooth. Mais notre objectif est ici de vous montrer comment on peut réaliser, avec MicroPython, une connexion Bluetooth entre deux carte à microcontrôleurs. Il existe aussi une bonne raison de préférer le Bluetooth au BLE : sa simplicité d'utilisation. Une fois les modules configurés et connectés, les deux modules Bluetooth se comportent **comme un simple câble virtuel** par lequel transitent les messages de l'UART concerné.

## Matériel requis et connexion du module Bluetooth HC-05 sur la NUCLEO-WB55

### **Matériel : Un maître et un esclave**

Pour ce tutoriel, nous aurons besoin d'assembler **deux** systèmes, chacun basé sur une NUCLEO-WB55, **un maître et un esclave**.
Pour les désigner sans ambiguïté au fil de ce tutoriel, nous avons fait cette distinction **arbitraire** entre un "système maître" et un "système esclave". En effet, les notions de maître et d'esclave n'ont en fait de sens que pour la procédure d'appairage des modules Bluetooth HC-05 avec les commande AT que nous allons aborder plus loin. Une fois celle-ci terminée, on peut permuter les modules HC-05 entre les deux cartes NUCLEO-WB55 sans que cela ne compromette le fonctionnement des scripts présentés dans cette section.

### **Pour le système "maître"** : 

1. Une [carte d'extension de base Grove](https://wiki.seeedstudio.com/Base_Shield_V2/) **avec son commutateur d'alimentation positionné sur 5V**.
2. Une carte NUCLEO-WB55
3. Un module [Bluetooth HC-05](https://www.aranacorp.com/fr/votre-arduino-communique-avec-le-module-hc-05/). Ce module sera configure en maître (voir plus loin).
4. Un [câble Grove-Dupont femelle](https://fr.vittascience.com/shop/115/lot-de-5-cables-grove---dupont-femelle) pour relier le module HC-05 au connecteur UART la carte d'extension de base Grove.
5. Un [potentiomètre Grove rotatif](https://wiki.seeedstudio.com/Grove-Rotary_Angle_Sensor/) ou [linéaire](https://wiki.seeedstudio.com/Grove-Slide_Potentiometer/) connecté sur la **broche A0**.


### **Pour le système "esclave"** : 

1. Une [carte d'extension de base Grove](https://wiki.seeedstudio.com/Base_Shield_V2/) **avec son commutateur d'alimentation positionné sur 5V**.
2. Une carte NUCLEO-WB55
3. Un module [Bluetooth HC-05](https://www.aranacorp.com/fr/votre-arduino-communique-avec-le-module-hc-05/). Ce module sera configuré en escalve (voir plus loin).
4. Un [câble Grove-Dupont femelle](https://fr.vittascience.com/shop/115/lot-de-5-cables-grove---dupont-femelle) pour relier le module HC-05 au connecteur UART la carte d'extension de base Grove.
5. Un [module servomoteur analogique Grove](https://wiki.seeedstudio.com/Grove-Servo/) connecté sur la **broche D6**.

### **Branchement des modules Bluetooth HC-05 sur les NUCLEO-WB55**

Vous devrez relier chaque module Bluetooth HC-05 à la NUCLEO-WB55 par les connecteurs ARDUINO avec un câble Grove-Dupont femelle. La première figure de ce tutoriel montre comment les modules devront être câblés. **I l ne faut pas oublier de croiser RX et TX** ; les broches TX des modules devront être câblées sur les broches RX/D0 des NUCLEO WB55 et les broches RX des modules devront être câblées sur les broches TX/D1 des NUCLEO WB55. 

> **Attention !**<br>
Comme tous les modules électroniques, les modules Bluetooth **sont très sensibles aux sauts de tension**. Nous avons constaté **une très forte propension des modules basés sur le BC417 à "griller" lorsqu'on les branche et débranche "à chaud", sur une carte à microcontrôleur alimentée**. Nous insistons donc sur la nécessité de déconnecter votre carte NUCLEO-WB55 de son alimentation lorsque vous manipulez ces modules.

## Configuration des deux modules Bluetooth HC-05 avec des commandes AT

### **1.Comment envoyer des commandes AT à un module Bluetooth HC-05 ?**

Nous expliquons à présent comment procéder pour envoyer des commandes AT à un module Bluetooth HC-05 à l'aide d'un script MicroPython. La liste de ces commandes AT est disponible [dans ce document](HC_05-module-AT-commands.pdf).

**Il faut d'abord placer le module dans le mode "réception de commandes AT"**. Pour cela, deux solutions sont possibles, selon que le module dispose d'un bouton sur sa face avant ou pas : 

* **Cas 1 : Si le module dispose d'un bouton sur sa face avant, maintenez le appuyé tout en branchant soit le système maître, soit le système esclave (tels que décrits ci-avant) via l'USB ST-LINK sur votre PC** . Relâchez le bouton après une seconde.

* **Cas 2 : Si le module n'a pas de bouton sur sa face avant, reliez sa broche *EN* à la broche *3V3* de la NUCLEO-WB55** avec un câble Dupont mâle-femelle. Branchez ensuite soit le système maître, soit le système esclave via l'USB ST-LINK sur votre PC. **Cette solution est préférable à la première** car elle est plus fiable ; le module restera en mode "réception de commandes AT" quoi qu'il advienne.

La LED du module devrait **clignoter une fois toutes les deux secondes**, ce qui confirme que le module est effectivement en mode de "réception de commandes AT".

Afin de ne pas alourdir ce tutoriel, nous ne recopions pas le script *main.py* qui relaye les commandes AT au STM32WB55, **vous le trouverez [dans cette archive téléchargeable](../../../assets/Script/MODULES.zip)**, dans le chemin *"\Liaison Bluetooth maître - esclave BC417\Script AT\main.py*. Copiez ce *main.py* dans le dossier *PYBFLASH*.

Vous remarquerez que, dans ce script, le débit en bauds de l'UART auquel est connecté le module est égal à 38400. **En mode "réception de commande AT", les modules HC-05 ont généralement un baudrate par défaut de 38400.** Il est cependant possible que vos modules soient configurés avec une autre valeur par défaut ; pensez à vérifier ce point à l'aide de leur fiche technique sur le site du fabricant ou du fournisseur.

Connectez un terminal PuTTY à la console REPL de l'interpréteur MicroPython de la NUCLEO-WB55.<br>
Démarrez le script avec *[CTRL]+[D]*. La console de PuTTY devrait ressembler à ceci :


```console
MicroPython v1.20.0 on 2023-04-26; NUCLEO-WB55 with STM32WB55RGV6
Type "help()" for more information.
>>>
>>>
MPY: sync filesystems
MPY: soft reboot
Entrez votre commande :
```

Dans la console PuTTY, tapez "AT" puis *[Entrée]*.<br>
Si les paramètres de la connexion avec le module (à savoir les valeurs des constantes *BAUDRATE*, *UART_NUMBER* et *EOL*) sont corrects, le module devrait vous répondre simplement *OK*, comme ci-dessous :

```console
MicroPython v1.20.0 on 2023-04-26; NUCLEO-WB55 with STM32WB55RGV6
Type "help()" for more information.
>>>
>>>
MPY: sync filesystems
MPY: soft reboot
Entrez votre commande : AT
Envoyé : AT
Message reçu : OK
Entrez votre commande :
```

Tout fonctionne comme prévu, nous pouvons passer à la suite !

### **2. Configurer un module Bluetooth HC-05 en esclave**

* **Branchez le système esclave (et lui seulement !) en mode "commandes AT"**, ainsi que nous l'avons expliqué au paragraphe qui précède, via l'USB ST-LINK sur votre PC. **Sa LED devrait clignoter une fois toutes les deux secondes**.

* Copiez le *main.py* contenant le "script AT" dans le dossier *PYBFLASH*, connectez un terminal PuTTY à la console REPL de l'interpréteur MicroPython de la NUCLEO-WB55 et démarrez le script avec *[CTRL]+[D]*.

* Ensuite, et dans cet ordre, envoyez les commandes AT suivantes au module :

    1. **AT+ORGL** : Réinitialise le module avec ses paramètres d'usine (optionnel). **Attention**, si votre module a été placé en mode "commandes AT" avec son bouton, cette commande va le refaire "basculer en mode normal". Donc, pour continuer, vous deverez de nouveau le configurer en mode "commandes AT" !
    2. **AT+NAME=Slave** : Attribue le nom "Slave" au module (optionnel).
    3. **AT+ROLE=0** : Demande au module de se comporter en esclave (inutile en fait, car c'est sa configuration par défaut).
    4. **AT+ADDR** : Demande au module de renvoyer son adresse MAC.

La console PuTTY / REPL du système esclave devrait ressembler à ceci :

```console
MPY: sync filesystems
MPY: soft reboot
Entrez votre commande : AT+ORGL
Envoyé : AT+ORGL
Message reçu : OK
Entrez votre commande : AT+NAME=Slave
Envoyé : AT+NAME=Slave
Message reçu : OK
Entrez votre commande : AT+ROLE = 0
Envoyé : AT+ROLE = 0
Message reçu : OK
Entrez votre commande : AT+ADDR
Envoyé : AT+ADDR
Message reçu : +ADDR:98D3:71:F6E077
OK
Entrez votre commande :
```

L'adresse MAC du module esclave est donc **98D3:71:F6E077**. Elle nous servira pour établir la connexion avec le module maître.

### **3.  Configurer un module Bluetooth HC-05 en maître et l'appairer au module esclave**

* **Branchez le système esclave (ne le mettez pas en mode "commandes AT" !)**

* **Branchez le système maître en mode "commandes AT"**, ainsi que nous l'avons expliqué au paragraphe qui précède, via l'USB ST-LINK sur votre PC. **Sa LED devrait clignoter une fois toutes les deux secondes**.

* Copiez le *main.py* contenant le "script AT" dans le dossier *PYBFLASH*, connectez un terminal PuTTY à la console REPL de l'interpréteur MicroPython de la NUCLEO-WB55 et démarrez le script avec *[CTRL]+[D]*.

* Ensuite, et dans cet ordre, envoyez les commandes AT suivantes au module :

    1. **AT+ORGL** : Réinitialise le module avec ses paramètres d'usine (optionnel). **Attention**, si votre module a été placé en mode "commandes AT" avec son bouton, cette commande va le refaire "basculer en mode normal". Donc, pour continuer, vous deverez de nouveau le configurer en mode "commandes AT" !
    2. **AT+NAME=Master** : Attribue le nom "Master" au module (optionnel).
    3. **AT+ROLE=1** : Demande au module de se comporter en maître.
    4. **AT+BIND=98D3,71,F6E077"** : Enregistre l'adresse MAC du module esclave pour que le module maître puisse s'appairer avec. **Remarquez que les ":" de l'adresse MAC ont été replacés dans cette ligne de commande par des ",".**

La console PuTTY / REPL du système maître devrait ressembler à ceci :

```console
Entrez votre commande : AT+ORGL
Envoyé : AT+ORGL
Message reçu : OK
Entrez votre commande : AT+NAME=Master
Envoyé : AT+NAME=Master
Message reçu : OK
Entrez votre commande : AT+ROLE=1
Envoyé : AT+ROLE=1
Message reçu : OK
Entrez votre commande : AT+BIND=98D3,71,F6E077
Envoyé : AT+BIND=98D3,71,F6E077
Message reçu : OK
Entrez votre commande :
```

> **Et voilà, les modules sont configurés pour s'appairer. Nous pouvons passer à l'étape suite et développer les scripts pour assurer la communication entre le système esclave et le système maître.**

**NB :** Vous pouvez vérifier que les deux modules sont correctement configurés en rebranchant le système esclave et le système maître **sans les configurer pour recevoir des commandes AT**. Après quelques secondes à clignoter rapidement et indépendamment, les LED des deux modules Bluetooth devraient **clignoter (deux fois rapidement) de façon synchrone toutes les cinq secondes**, ce qui confirmera leur appairage.

## Commande à distance d'un servomoteur à l'aide d'un potentiomètre

Nous pouvons à présent réaliser le système présenté au début de ce tutoriel. Deux scripts sont nécessaires, un pour le système maître et un pour le système esclave.

### **Le script MicroPython du système maître**

> **Vous trouverez le script du système maître [dans cette archive téléchargeable](../../../assets/Script/MODULES.zip)**, dans le chemin *"\Liaison Bluetooth maître - esclave BC417\Maître\main.py*. Copiez ce *main.py* dans le dossier *PYBFLASH*.

```python
# Objet du script : Réception de la valeur d'un potentiomètre par une liaison Bluetooth et
# commande de l'angle d'un servomoteur en conséquence. 
# Ce script est paramétré (UART_NUMBER, BAUDRATE, EOL) pour la communication avec un module
# Bluetooth HC-05 connecté à une carte NUCLEO-WB55.

import pyb
from machine import UART
from time import sleep_ms

# Constantes relatives au paramétrage de l'UART
DELAY_TIMEOUT = const(10000) # Durée (en millisecondes) pendant laquelle l'UART attend de reçevoir un message
BAUDRATE = const(9600)	# Débit, en bauds, de la communication série
UART_NUMBER = const(2)	# Identifiant de l'UART de la carte NUCLEO-WB55 qui sera utilisé
RX_BUFF = const(512)	# Taille du buffer de réception (les messages reçus seront tronqués 
						# à ce nombre de caractères)
# Servomoteur sur D6
d6 = pyb.Pin('D6', pyb.Pin.OUT_PP)
tim_d6 = pyb.Timer(1, freq=50)
pwm_d6 = tim_d6.channel(1, pyb.Timer.PWM, pin=d6)

# Convertit l'ange de rotation (en entrée) en une valeur de PWM puis l'envoie au servo.
@micropython.native # Optimise le bytecode pour STM32
def setServoAngle(timer, angle):
	if (angle >= 0 and angle <= 180):
		pw_percent = 3 + angle * (12.5 - 3) / 180
		timer.pulse_width_percent(pw_percent)
	else:
		raise ValueError("La commande d'angle du servomoteur doit être comprise entre 0° et 180°")

# Fonction pour remapper un intervalle de valeurs dans un autre
@micropython.native # Optimise le bytecode pour STM32
def map (value, from_min, from_max, to_min, to_max):
	return (value-from_min) * (to_max-to_min) / (from_max-from_min) + to_min

EOL = "\r\n" # Terminaison de commande pour valider l'envoi

# Initialisation de l'UART
uart = UART(UART_NUMBER, BAUDRATE, timeout = DELAY_TIMEOUT, bits=8, parity=None, stop=1, rxbuf = RX_BUFF)

print("Module maître (servomoteur)")

while True:

	try:
	
		message_content = uart.readline()
	
		decoded_msg = message_content.decode("utf-8").strip()
		
		#print("Valeur du potentiomètre : " + decoded_msg)
	
		# Convertit la lecture analogique du potentiomètre en un angle entre 0° et 180°
		angle = int(map(int(decoded_msg), 0, 4095, 0, 180))
						
		# Applique cet angle au servomoteur
		setServoAngle(pwm_d6, angle)
			
	except ValueError:
	
		print("Mesure du potentiomètre erronée" ) 
```

On remarquera que le baudrate du module Bluetooth HC-05 est désormais de 9600. **En mode "normal", les modules HC-05 ont généralement un baudrate par défaut de 9600.** Il est cependant possible que vos modules soient configurés avec une autre valeur par défaut ; pensez à vérifier ce point à l'aide de leur fiche technique sur le site du fabricant ou du fournisseur.

### **Le script MicroPython du système esclave**

> **Vous trouverez le script du système maître [dans cette archive téléchargeable](../../../assets/Script/MODULES.zip)**, dans le chemin *"\Liaison Bluetooth maître - esclave BC417\Esclave\main.py*. Copiez ce *main.py* dans le dossier *PYBFLASH*.

```python
# Objet du script : Transmission de la valeur d'un potentiomètre par une liaison Bluetooth.
# Ce script est paramétré (UART_NUMBER, BAUDRATE, EOL) pour la communication avec un module
# Bluetooth HC-05 connecté à une carte NUCLEO-WB55.

import pyb
from machine import UART
import gc # Ramasse miettes, pour éviter de saturer la mémoire 
from time import sleep_ms

# Constantes relatives au paramétrage de l'UART
DELAY_TIMEOUT = const(1000) # Durée (en millisecondes) pendant laquelle l'UART attend de reçevoir un message
BAUDRATE = const(9600)	# Débit, en bauds, de la communication série
UART_NUMBER = const(2)	# Identifiant de l'UART de la carte NUCLEO-WB55 qui sera utilisé
RX_BUFF = const(512)	# Taille du buffer de réception (les messages reçus seront tronqués 
						# à ce nombre de caractères)
						
# Potentiomètre branché sur A0
a0 = pyb.ADC('A0')

EOL = "\r\n" # Terminaison de commande pour valider l'envoi

# Initialisation de l'UART
uart = UART(UART_NUMBER, BAUDRATE, rxbuf = RX_BUFF)

print("Module esclave (potentiomètre)")

while True:
	
	# Lecture et numérisation de la valeur du potentiomètre
	adc_read = a0.read()
	
	str_adc_read = str(adc_read)
	
	# Message de débug, commenté compte tenu de la haute fréquence de
	# l'échantillonage du potentiomètre.
	#print("Message envoyé : " + str_adc_read)
	
	# Envoi de la valeur du  potentiomètre au module HC-05 maître
	uart.write(str_adc_read + EOL)
	
	# Temporisation de 50 ms
	sleep_ms(50)
```

Même remarque que pour le script du système maître : le baudrate par défaut du module HC-05 en mode "normal" est de 9600.

### **Résultat**

Pour démarrer le système ...

1. Branchez l'USB USER de la carte NUCLEO-WB55 du système maître sur une alimentation, ou sur un port USB de votre PC. Dans ce second cas, lancez le script maître avec la commande *[CTRL]*+*[D]* sous PuTTY.
2. De même branchez la carte NUCLEO-WB55 du système esclave et démarrez son script.

Les deux modules Bluetooth HC-05 ne devraient pas tarder à s'apparier : après quelques instants leurs LED devraient **clignoter (deux fois rapidement) de façon synchrone toutes les cinq secondes**.

**Vous pourrez dès lors faire tourner le servomoteur du système maître d'un angle arbitraire entre 0° et 180° à l'aide du potentiomètre du système esclave.** 

## Ressources

Ce tutoriel s'inspire de ressources réalisées pour l'environnement Arduino, avec des modules Bluetooth HC-05 et HC-06. On peut notamment citer :

* [Le tutoriel Arduino sur le site **How To Mechatronics**](https://howtomechatronics.com/tutorials/arduino/how-to-configure-pair-two-hc-05-bluetooth-module-master-slave-commands/)
* [Le tutoriel Arduino sur le site **AranaCorp**](https://www.aranacorp.com/fr/votre-arduino-communique-avec-le-module-hc-05/).
* [Le tutoriel Arduino sur **mataucarre.fr**, changement du code d'appairage d'un module HC-05](https://mataucarre.fr/index.php/2018/04/19/utiliser-module-bluetooth-hc05-arduino/)
* [Le tutoriel Arduino sur **AranaCorp**, configuration d'un module HC-06](https://www.aranacorp.com/fr/arduino-et-le-module-bluetooth-hc-06/)
* [Le tutoriel Arduino sur le site **Instructables**](https://www.instructables.com/Modify-The-HC-05-Bluetooth-Module-Defaults-Using-A/)