---
title: Module Ultimate GPS Breakout d'Adafruit
description: Mise en œuvre d'un module UART Ultimate GPS Breakout d'Adafruit avec MicroPython
---

# Module Ultimate GPS Breakout d'Adafruit

Ce tutoriel explique comment mettre en œuvre un [module Ultimate GPS Breakout d'Adafruit](https://www.adafruit.com/product/746) avec MicroPython. 

Plus précisément, nous allons voir comment :
1. Mettre en œuvre un module GPS UART, en l'occurrence fabriqué par Adafruit
2. Décoder les messages renvoyés par celui-ci et afficher les informations de position, altitude et vitesse sur le port série REPL / USB USER.

**Le module Ultimate GPS Breakout d'Adafruit :**
<br>
<div align="left">
<img alt="Adafruit - GPS" src="images/adafruit-gps.jpg" width="350px">
</div>
<br> 

> Crédit image : [Adafruit](https://www.adafruit.com/product/746)

## Qu'est-ce donc que le GPS ?

Le [système GPS](https://fr.wikipedia.org/wiki/Global_Positioning_System), pour *"Global Navigation System"*, permet de se repérer partout sur Terre par une méthode de **[trilatération](https://fr.wikipedia.org/wiki/Trilat%C3%A9ration)** utilisant des **signaux radio émis par des satellites**. Le système GPS se décompose comme ceci :

1. **Un segment spatial** constitué d'une constellation de satellites filant sur des orbites elliptiques. Cette constellation est conçue de sorte qu'à chaque instant **au moins 4 satellites soient toujours visibles au-dessus de l'horizon** de n'importe quel point du globe. 

2. **Un segment terrestre**, avec un maillage de stations à la surface de la Terre, qui envoient périodiquement aux satellites des consignes pour corriger leurs signaux radio afin de maintenir constante la précision du système GPS au cours du temps.

3. **Un segment utilisateur**, justement constitué de l'ensemble des **récepteurs GPS** utilisés par des milliards d'objets sur Terre à chaque instant. Ces récepteurs sont essentiellement constitués d'une antenne et d'une puce GPS chargée d'effectuer les calculs de trilatération. Le module Ultimate GPS Breakout est, bien sûr, un récepteur GPS.

Le principe de fonctionnement de la géolocalisation par GPS est le suivant :
- Le récepteur reçoit plusieurs signaux radio simultanément de 3, 4, 5 ... satellites (plus il y en aura, plus ce sera précis). 
- Il évalue la distance des différents satellites en mesurant avec une grande précision le temps de propagation des signaux depuis les satellites jusqu'à son antenne.
- Il calcule sa position à la surface du globe terrestre, exprimée en **latitude**, **longitude** et **altitude** à l'aide sa puce GPS. La trilatération n'est pas possible avec moins de 4 satellites en vue au-dessus de l'horizon. De ce fait **la localisation par GPS ne fonctionne pas** (ou mal, selon la situation) **à l'intérieur d'enceintes fermées** (bâtiments, tunnels, grottes, sous-sols ...) **ou lorsqu'on se promène dans une gorge**, puisque les signaux des satellites sont perturbés (réfléchis de multiples fois) voire bloqués avant d'atteindre le récepteur. 

<br>
<div align="left">
<img alt="Lalitude et longitude" src="images/latitude_et_longitude.png" width="600px">
</div>
<br>

> Crédit image : [Lukaves](https://www.istockphoto.com/portfolio/lukaves?mediatype=illustration) via [iStockphoto](https://www.istockphoto.com/).

Lorsque le nombre de satellites en vue est suffisant (supérieur à 4), la précision horizontale du GPS est de l'ordre de 5 à 10 m, la précision verticale de l'ordre de 25 à 50 m. On peut faire l'améliorer significativement, jusqu'à quelques centimètres, avec différentes méthodes que nous ne détaillerons pas ici, en général commerciales (payantes ...). La principale limite à la précision du GPS trouve son origine dans les perturbations des signaux des satellites par l'ionosphère.

Il y aurait encore beaucoup de d'autres choses intéressantes à raconter à propos des systèmes de géolocalisation par satellites, nous n'allons évidemment pas développer plus le sujet ici. Les ressources abondent sur Internet (par exemple, [cette référence](https://parlonssciences.ca/ressources-pedagogiques/documents-dinformation/les-mathematiques-qui-font-fonctionner-le-gps) pour aller un peu plus loin).

Lorsque le module GPS est parvenu à "capter" suffisamment de satellites pour calculer sa position (on dit alors qu'on a un "fix" GPS), sa puce renvoie des messages de navigation normalisés, des chaînes de caractères appelées **trames NMEA** (pour *"National Marine Electronics Association"*). Par exemple, pour une trame GPS de type GGA :<br>

```Console
$GPGGA,064036.289,4836.5375,N,00740.9373,E,1,04,3.2,200.2,M,,,,0000*0E
```

Elle indique dans l'ordre : l'heure UTC, la latitude, la longitude, le nombre de satellites utilisés, l'altitude en mètres, quelques champs vides et une somme de contrôle.<br>

Vous pouvez consulter [Wikipédia ici](https://fr.wikipedia.org/wiki/NMEA_0183) pour plus d'explications sur les différentes trames NMEA et leur structure. Pour ce qui concerne le module Adafruit, les trames NMEA sont émises sur un [port série asynchrone (UART)](../STARTWB55/uart). Nous allons donc connecter l'UART du module Adafruit à l'UART de notre carte NUCLEO-WB55 et charger dans son microcontrôleur un script MicroPython qui décode les trames NMEA. 

Pour finir, précisons que système GPS a été créé par le département de la défense des Etats Unis et a été mis en service en 1978. Depuis, trois autres systèmes globaux ont été déployés : européen ([Gallileo](https://fr.wikipedia.org/wiki/Galileo_(syst%C3%A8me_de_positionnement))), russe ([GLONASS](https://fr.wikipedia.org/wiki/GLONASS)) et chinois ([BeiDou](https://fr.wikipedia.org/wiki/Beidou)). On peut citer également [QZSS](https://fr.wikipedia.org/wiki/) mais il s'agit d'un système régional qui ne couvre que l'archipel japonais.

Les récepteurs récents sont capables d'utiliser simultanément plusieurs de ces systèmes pour améliorer la précision du positionnement. De ce fait la géolocalisation par satellite porte désormais le nom de [**GNSS**](https://fr.wikipedia.org/wiki/Syst%C3%A8me_de_positionnement_par_satellites) pour *"Global Navigation Satellite System"*. Pour ce qui concerne la puce MTK3329/MTK3339 de notre module Adafruit, il semble, d'après [sa documentation](https://cdn-shop.adafruit.com/datasheets/PMTK_A11.pdf), qu'elle peut exclusivement utiliser la constellation GPS.

## Matériel requis et montage

1. Une [carte d'extension de base Grove](https://wiki.seeedstudio.com/Base_Shield_V2/)
2. Une carte NUCLEO-WB55
3. Un [module Ultimate GPS Breakout d'Adafruit](https://www.adafruit.com/product/746). Vous aurez sans doute besoin de faire appel à un ami maker pour souder les broches sur la plaquette du module ...
4. Un [câble Grove - Dupont mâle](https://fr.vittascience.com/shop/229/lot-de-5-cables-grove---dupont-male) pour connecter le module Adafruit à la carte d'extension de base Grove.  

Le module doit être branché *sur le connecteur UART du Grove base shield*, qui peut être alimenté en 3.3V ou 5V. Veillez à ce que le fil *jaune* du câble Grove - Dupont soit connecté *la broche T  X* du module et le fil *blanc* sur *la broche RX* du module.

## Le script MicroPython

> **Vous pouvez télécharger les scripts MicroPython de ce tutoriel (entre autres) en cliquant [ici](../../../assets/Script/MODULES.zip)**. Une fois l'archive ZIP décompressée vous trouverez le fichier *main.py* et le fichier *adafruit_gps.py* dans le dossier *\\Module GPS Adafruit\\Pour commencer\\*. 

Ce code est adapté de [l'exemple fourni par Adafruit disponible ici](https://GitHub.com/alexmrqt/MicroPython-gps).
Il a été testé avec :
1. [Le module Ultimate GPS Breakout d'Adafruit](https://www.adafruit.com/product/746)
2. [Le module GNSS Grove SIM28 de Seeed Studio](https://wiki.seeedstudio.com/Grove-GPS/)
3. [Le module GNSS Grove AIR530 de Seeed Studio](https://wiki.seeedstudio.com/Grove-GPS-Air530/) **qui semble être le plus précis des trois**.

**Attention !**<br>Ce script fonctionnera avec n'importe quel module GNSS UART, **mais vous devez veiller à renseigner la bonne valeur du baudrate pour votre module !** Par exemple, on a ici 9600 bauds.

**Attention ! (bis)**<br>les commandes d'initialisation du script qui suit sont propres à la puce GPS MTK3329/MTK3339 du module Adafruit (par exemple ```gps.send_command('PMTK314,...')```) sont documentées [ici](https://cdn-shop.adafruit.com/datasheets/PMTK_A11.pdf). La plupart des modules GNSS peuvent être configurés de la même façon, il s'agit juste de trouver la syntaxe des commandes dans la documentation fournie par leur fabricant.

**Attention ! (ter)**<br>Les antennes fournies avec ces modules n'étant généralement pas actives (amplifiées), vous serez très certainement obligé de tester les modules à l'extérieur, sans un étage au-dessus de votre tête, pour obtenir le fix des satellites.

Copiez les fichiers **adafruit_gps.py** dans le répertoire du périphérique *PYBFLASH*. Editez maintenant le script *main.py* et copiez-y le code qui suit :

```python
# Exemple de décodage de trames GPS à l'aide la lib adafruit_gps https://GitHub.com/alexmrqt/MicroPython-gps
# Ce code est adapté de l'exemple fourni par Adafruit.
# Il a été testé et fonctionne correctement, en l'état:
#  - avec le module GPS Grove (SIM28) de Seeed Studio: https://wiki.seeedstudio.com/Grove-GPS/
#  - avec le module Ultimate GPS Breakout d'Adafruit: https://www.adafruit.com/product/746
# En principe, il devrait fonctionner avec tous les modules GPS UART sauf les
# commandes d'initialisation qui sont propres au module PMTK du GPS Adafruit :
# https://cdn-shop.adafruit.com/datasheets/PMTK_A11.pdf.
# Consulter Wikipédia pour l'explication des trames NMEA : https://fr.wikipedia.org/wiki/NMEA_0183

import pyb
from pyb import UART # Classe pour gérer l'UART
import time
import adafruit_gps # Classe pour décoder les trames NMEA

# Numéro de l'UART disponible sur la NUCLEO-WB55
LPUART_ID = const(2)

# Baudrate du module GPS
GPS_BAUDRATE = const(9600)

# Time-Out du module GPS, en millisecondes
GPS_TIMOUT_MS = const(5000)

# Ouverture du LPUART sur les broches D0(RX) D1(TX)
# Attention, le Time-Out doit être plus grand que la fréquence d'interrogation du module GPS !
uart = UART(LPUART_ID, GPS_BAUDRATE, timeout = GPS_TIMOUT_MS)

# Crée une instance du module GPS
gps = adafruit_gps.GPS(uart)

# Initialise le module GPS en précisant quelles trames il renvoie et à quelle fréquence.

# Renvoie les trames GGA et RMC :
#gps.send_command('PMTK314,0,1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0')

# Renvoie les infos minimum (trames RMC seulement, position):
#gps.send_command('PMTK314,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0')

# Demande au GPS de ne plus renvoyer aucune trame (pour économiser l'énergie)
# gps.send_command('PMTK314,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0')

# Demande au GPS de transmettre TOUTES les trames NMEA (mais la lib adafruit_gps
# ne sait pas toutes les décoder) :
# gps.send_command('PMTK314,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0')

# Demande au GPS de renvoyer les trames une fois par seconde:
# gps.send_command('PMTK220,1000') # 1000 => 1000 ms <=> 1s
# Si vous augmentez ce délai au delà de 1000 ms, assurez vous que le TimeOut de 
# l'UART est plus grand que sa nouvelle valeur !
# Vous pouvez aussi réduire ce délai. Par exemple, pour deux mesures par seconde:
# gps.send_command('PMTK220,500')
# Mais sachez que si la fréquence devient trop élevée, la lib adafruit_gps ne sera 
# plus forcément capable de décoder les trames assez vite et vous risquerez perdre
# des informations.

last_print = time.ticks_ms() # Mesure du temps initial

print("Acquisition des satellites en cours...")

# Boucle sans clause de sortie avec une temporisation de 1000 ms.
# Affiche la localisation chaque seconde si le "fix" des satellites est validé.

while True:
	# Assurez vous d'appeler gps.update() à chaque itération et au moins deux fois
	# plus souvent que la fréquence des trames renvoyées par le module GPS.
	# Cette méthode renvoie un bool qui prend la valeur 'True' si elle est parvenue à décoder une nouvelle trame.
	# Nous ne vérifions pas son retour et testons plutôt la propriété 'has_fix'.
	gps.update()
	
	current = time.ticks_ms() # Mesure du temps actuel
	
	if current-last_print >= 1000: # Si le temps écoulé est au moins égal à 1 seconde
	
		last_print = current # mémorise le temps pour l'itération suivante

		if gps.has_fix: # Acquisition des satellites réalisée !
		
			# Affiche les informations de la trame : localisation, date, etc.
			print('=' * 40)  # Imprime une ligne de séparation
			
			# Décode et formatte les données d'horodatage (instant du fix)
			print('Horodatage acquisition: {}/{}/{} {:02}:{:02}:{:02}'.format(
				gps.timestamp_utc[1],
				gps.timestamp_utc[2],
				gps.timestamp_utc[0],
				gps.timestamp_utc[3],
				gps.timestamp_utc[4],
				gps.timestamp_utc[5]))
			print('Latitude: {} degrés'.format(gps.latitude)) # Latitude du module
			print('Longitude: {} degrés'.format(gps.longitude)) # Longitude du module
			print('Qualité acquisition: {}'.format(gps.fix_quality)) # Qualité de la localisation fournie

			# Certaines informations au-delà de la latitude, la longitude et l'étiquette d'horodatage sont optionnelles
			# selon la configuration que vous avez appliquée au module.
			if gps.satellites is not None:
				print('# satellites: {}'.format(gps.satellites)) # Nombre de satellites acquis par le module
			if gps.altitude_m is not None:
				print('Altitude: {} mètres'.format(gps.altitude_m)) # Altitude estimée du module au-dessus d'un ellipsoïde théorique "moyen" qui englobe la Terre
			if gps.track_angle_deg is not None:
				print('Vitesse: {} noeuds'.format(gps.speed_knots)) # Vitesse estimée du module en noeuds (1 noeud = 1,852 km/h)
			if gps.track_angle_deg is not None:
				print('Cap: {} degrés'.format(gps.track_angle_deg)) # Angle entre la direction du nord géographique et la direction suivie par le module
			if gps.horizontal_dilution is not None:
				print('Dilution de précision horizontale: {}'.format(gps.horizontal_dilution)) # Qualité de la localisation du module en latitude et longitude
			if gps.height_geoid is not None:
				print('Elévation au-dessus du géoïde: {} mètres'.format(gps.height_geoid)) # Ecart entre l'altitude du géoïde terrestre et celle de l'ellipsoïde moyen (altitude effectivement rapportée) à l'emplacement du module
```

## Sortie sur le port série de l'USB USER

Lancez le script avec *[CTRL]-[D]* dans le terminal PuTTY et observez les relevés de position :

```console
>>>
MPY: sync filesystems
MPY: soft reboot
Acquisition des satellites en cours...
========================================
Horodatage acquisition : 4/27/2023 16:47:03
Latitude : 48.84611 degrés
Longitude : 2.34621 degrés
Qualité acquisition : 6
Nb satellites : 8
Altitude : 325.3 mètres
Vitesse : 6.29 noeuds
Cap : 136.62 degrés
Dilution de précision horizontale : 99.99
Elévation au-dessus du géoïde : 44.1 mètres
```

Soyez attentif à la représentation choisie pour la latitude et la longitude, qui sont **exprimées ici en degrés décimaux (DD)**. Plusieurs écritures équivalentes peuvent être utilisées, notamment les degrés-minutes-secondes (DMS), ou [**système sexagésimal**](https://fr.wikipedia.org/wiki/Coordonn%C3%A9es_g%C3%A9ographiques), expression traditionnelle des coordonnées géographiques. Google Maps permet de passer des DD au DMS et vérifier que le relevé de position est pertinent. Par exemple, si vous tapez ```48.84611, 2.34621``` dans le champ de recherche de Google Maps vous obtiendrez :

<br>
<div align="left">
<img alt="Coordonnées Panthéon" src="images/gps_pantheon.jpg" width="400px">
</div>
<br> 

> Crédit image : [Google Maps](https://www.google.com/maps/)

L'expression des coordonnées en DMS est donnée par la première ligne sous la photo du lieu : ```48°50'46.0"N 2°20'46.4"E```. 

## Pour aller plus loin : réception et décodage asynchrones

Afin de rendre la collecte des trames NMEA plus efficace, il est possible d'exploiter les méthodes de la classe ```asyncio``` de l'interpréteur MicroPython. Vous trouverez cet exemple dans le dossier *\\Module GPS Adafruit\\GPS asychone\\*, dans l'archive ZIP téléchargeable [ici](../../../assets/Script/MODULES.zip). Il s'agit d'une adaptation du travail de **Peter Hinch**, disponible [sur ce dépôt GitHub](https://github.com/peterhinch/micropython-async/blob/master/v3/docs/GPS.md).


## Liens et références

Les liens suivants pourraient vous être utiles :
- [Le site Adafruit pour le module GPS étudié ici](https://www.adafruit.com/product/)
- [Un calculateur en ligne de sommes de contrôle NMEA](https://nmeachecksum.eqth.net/)
- [Une explication du calcul des sommes de contrôle NMEA (Arduino)](https://forum.arduino.cc/t/nmea-checksums-explained/1046083)

