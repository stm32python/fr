---
title: Sonar à ultrasons US100 (Adafruit)
description: Mise en œuvre du capteur de distance par ultrasons Adafruit US100 en MicroPython
---

# Sonar à ultrasons Adafruit US100

Ce tutoriel explique comment mettre en œuvre un capteur de distance Adafruit par ultrasons US100, configuré en UART, en MicroPython.

**Le capteur de distance à ultrasons US100 de Adafruit :**

<br>
<div align="left">
<img alt="Adafruit - Ultrasonic" src="images/Adafruit-Ultrasonic-Distance-Sensor.jpg" width="500px">
</div>
<br>

> Crédit image : [Distrelec France](https://www.distrelec.fr/)

Le capteur US100 émet des ultrasons et calcule le temps qu'ils mettent à revenir vers lui, ce qui lui permet de mesurer la distance le séparant d'un objet qui les a réfléchis, comprise entre 2 cm et 4,5 m. Il communique avec la NUCLEO-WB55 par le biais de la liaison série (UART) à une fréquence de 9600 bauds. L'UART2 (ou LPUART) est câblé sur les broches PA2 et PA3 du STM32WB55, qui correspondent au connecteur "UART" du Grove Base Shield.

## Matériel requis et montage

1. Une [carte d'extension de base Grove](https://wiki.seeedstudio.com/Base_Shield_V2/)
2. La carte NUCLEO-WB55
3. Un [capteur à ultrasons US100 de Adafruit](https://www.adafruit.com/product/4019)

Pour faire communiquer le capteur avec la carte il faut connecter la broche Echo/Rx du capteur à la broche Rx du Grove Base Shield et la broche Trig/Tx à la broche Dx du Grove Base Shield.<br>
**Attention** : il faut bien veiller à ce que les broches Dx/Tx et Rx soient croisées entre le capteur et la carte car ces lignes sont unidirectionnelles. Dx/Tx correspond à la transmission d'un message tandis que Rx correspond à sa réception. Le Grove Base Shield réalise d'office ce croisement entre la carte et le capteur.

Il faut ensuite alimenter le capteur en reliant la broche VCC à la broche Vcc du Grove Base Shield ou alors tirer un fil supplémentaire pour connecter le deuxième GND à un GND quelconque de la NUCLEO-WB55 ou du Grove Base Shield.<br>
**Important :** Ce module est particulièrement sensible, ne le brachez ou débranchez jamais "à chaud", sur une carte sous tension.

<br>
<div align="left">
<img alt="Schéma de câblage du capteur à ultrasons" src="images/US100_cablage.jpg" width="400px">
</div>
<br>

## Le code MicroPython

> **Vous pouvez télécharger les scripts MicroPython de ce tutoriel (entre autres) en cliquant [ici](../../../assets/Script/MODULES.zip)**.

Il faut ajouter le fichier *US100.py* dans le répertoire du périphérique *PYBFLASH*.

*Etape 1 :* Pour faire fonctionner le programme, nous devons dans un premier temps importer 2 bibliothèques au tout début de notre code de cette façon :

```python
from us100 import US100 # Pilote du sonar
from time import sleep # Pour temporiser
```

*Etape 2 :*  On crée un objet correspondant au capteur qu'on nomme *sonar* :

```python
sonar = US100()
```

*Etape 3 :*  On affiche sur le terminal série la distance en cm que mesure le capteur toutes les secondes dans une boucle infinie :

```python
while True:
    print('Distance : %.1f cm ' % (sonar.distance_mm()/10))
    sleep(1)
```

**Résultat :**

En approchant la main ou un objet du capteur, on peut constater des variations de distance :

<br>
<div align="left">
<img alt="Affichage des données du capteur ultrason" src="images/US100_PuTTY.jpg" width="500px">
</div>
<br>

Les valeurs en -0,1 cm correspondent à une erreur de mesure. Soit le capteur mesure une trop grande distance, soit l'objet est orienté de telle façon que le signal du capteur rebondit dessus mais est dirigé dans une autre direction et ne revient donc pas au capteur, qui ne peut pas mesurer la distance.

## Pour aller plus loin

Nous avons développé un code que vous pouvez trouver en cliquant sur ce [lien](https://gitlab.com/stm32python/fr/-/blob/master/assets/Script/Ultrason/main_radar.py) qui simule le fonctionnement d'un capteur de recul automobile. Il faut ajouter un buzzer sur le port A3 du Grove Base Shield comme montré ci-dessous. Le buzzer produit un son court qui se répète à une vitesse variant en fonction de la distance que détecte le capteur. Le buzzer commence à émettre des sons pour une distance inférieure à 75 cm et produit un son continu lorsque la distance est inférieure à 5 cm. Le détecteur de recul est activé/désactivé en appuyant sur le bouton SW1 de la carte qui est géré par le mécanisme des interruptions.
La LED rouge LED1 s'allume lorsque le détecteur est activé.

<b>
<div align="left">
<img alt="Schéma de câblage du détecteur de recul" src="images/Radar_recul_cablage.jpg" width="500px">
</div>
<b>

*Le listing complet :*

Editez maintenant le script *main.py* du périphérique *PYBFLASH* :

```python
from us100 import US100
from pyb import Pin, Timer
from time import sleep_ms

sonar=US100()

BUZZER = Pin('A3') # Pin A3 (shield Grove et connecteur Arduino) cablée sur Pin PA0 (microcontrôleur)
tim = Timer(2, freq=440)	# TIM2 channel 1 cablé sur A3
ch = tim.channel(1, Timer.PWM, pin=BUZZER)	#Mode PWM sur sur A3

# Initialisation du flag d'interruption
i = 0

# Initialisation du bouton poussoir
sw=pyb.Switch()

# Fonction d'interruption du bouton poussoir
def appuie():
	global i
	pyb.LED(1).toggle()
	if i==0:
		i = 1
	else:
		i = 0

sw.callback(appuie)

# Fonction qui fait sonner le buzzer
def buzz(temps):
	ch.pulse_width_percent(20) # Rapport cyclique de 20%
	sleep_ms(50)
	ch.pulse_width_percent(0)
	sleep_ms(temps)
	
while True: # Boucle infinie qui enclenche le radar s'il y'a eu appui du bouton, l'éteint si appui à nouveau
	if i == 1:
		d = sonar.distance_mm()/10
		print(d)
		
		# pas de son
		if d > 75:
			ch.pulse_width_percent(0)
			
		# son continu
		elif d < 5 and d > 0:
			buzz(0)
			
		# son évoluant en fonction de la distance
		else:
			t = int(d*5)
			buzz(t)
	# eteint le buzzer
	ch.pulse_width_percent(0)
	sleep_ms(5)
```
