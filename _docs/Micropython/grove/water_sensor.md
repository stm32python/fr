---
title: Détecteur d'eau
description: Acquisition du niveau d'eau avec un module Grove détecteur d'eau
---

# Capteur d'eau

Ce tutoriel explique comment mettre en œuvre [un module Grove détecteur d'eau](https://wiki.seeedstudio.com/Grove-Water_Sensor/).
Ce capteur est constitué de pistes conductrices qui seront mises en court-circuit en présence d'eau. Lorsque cela se produit, la résistance de l'ensemble diminue. Une chute de tension aux bornes de la broche SIG permet de détecter la présence et, si les pistes sont immergées, d'estimer le niveau d'eau.<br>
Ce capteur est identique [au module détecteur de pluie](https://eu.robotshop.com/fr/products/raindrop-sensor-module), abordé dans [cette fiche](raindrop).

**Le module Grove détecteur d'eau :**

<br>
<div align="left">
<img alt="Grove - Water sensor" src="images/Water_Sensor.png" width="300px">
</div>
<br>

> Crédit image : [Seeed Studio](http://wiki.seeedstudio.com/Grove_System/)

## Matériel requis

1. Une [carte d'extension de base Grove](https://wiki.seeedstudio.com/Base_Shield_V2/)
2. La carte NUCLEO-WB55
3. [Un module Grove détecteur d'eau](https://wiki.seeedstudio.com/Grove-Water_Sensor/)

Le module est connecté sur la fiche A0 de la carte d'extension de base Grove.

## Le code MicroPython

> **Vous pouvez télécharger les scripts MicroPython de ce tutoriel (entre autres) en cliquant [ici](../../../assets/Script/MODULES.zip)**.

```python
# Lecture et numérisation du signal d'un capteur de niveau d'eau (water sensor)
# Attention : le capteur doit être alimenté en 5V pour donner une réponse entre 0 et 4095.

from pyb import ADC, Pin 		# Convertisseur analogique-numérique et GPIO
from time import sleep 			# Pour les temporisations

# Instanciation et démarrage du convertisseur analogique-numérique
adc = ADC(Pin('A0'))

# Seuil pour la détection d'eau (sans unités)
THRESHOLD = const(700)

while True:

	# Numérise la valeur lue, produit un résultat variable dans le temps dans l'intervalle [0 ; 4095]
	measure = adc.read()
	
	# Si le signal dépasse un seuil donné
	if measure > THRESHOLD:
		print("Eau détectée : %d " %(measure))
	else:
		print("Sec : %d " %(measure))
	sleep(1) # Temporisation d'une seconde
```

<h2>Résultat</h2>

Lancez le script avec *[CTRL]-[D]* dans votre terminal série (PuTTY par exemple). Vous pouvez à présent observer, dans la valeur acquise en faisant tomber de l'eau sur le module ou en l'immergeant partiellement dans l'eau, **en faisant bien attention de ne mouiller que ses pistes métalliques et surtout pas son connecteur Grove ou le câble qui le relie à la carte d'extension de base Grove !**

```console
Sec : 612
Eau détectée : 749
Eau détectée : 829
Sec : 357
Sec : 98
Sec : 352
Sec : 517
Eau détectée : 718
Eau détectée : 868
Sec : 581
Sec : 90
Sec : 326
Sec : 451
Sec : 666
Eau détectée : 867
Sec : 684
Sec : 100
```