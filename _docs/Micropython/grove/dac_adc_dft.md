---
title: Générateur et analyseur de signaux, transformation de Fourier discrète
description: Création d'un générateur de signaux modulés en tension et d'un analyseur de ces signaux en MicroPython, utilisation de la transformation de Fourier discrète (DFT) pour analyse spectrale.
---

# Générateur et analyseur de signaux, transformation de Fourier discrète

Ce tutoriel montre comment construire un analyseur de signaux et un générateur de signaux périodiques en MicroPython, avec des cartes NUCLEO. **Il comporte quatre parties** :

- **Partie 1 :  Séries de Fourier, DFT et FFT**<br>
Nous y expliquerons dans les grandes lignes les principes et les outils de l'analyse de Fourier utilisés dans ce tutoriel.

- **Partie 2 : Conception de l'analyseur de signaux**<br>
**L'analyseur de signaux** va capturer un échantillon d'un signal modulé en tension à l'aide de son [**convertisseur analogique-numérique (ADC)**](../startwb55/adc), puis calculera sa [**DFT**](https://fr.wikipedia.org/wiki/Transformation_de_Fourier_discr%C3%A8te) pour en déduire [son spectre](https://fr.wikipedia.org/wiki/Analyse_spectrale). Pour construire l'analyseur, nous utiliserons une [carte NUCLEO-WB55](../install_win_wb55/index).
Nous le testerons avec une [**photodiode**](../grove/luminosite.md) pour mesurer la fréquence d'une [**lampe fluo-compacte**](https://fr.wikipedia.org/wiki/Lampe_fluorescente).

- **Partie 3 : Conception du générateur de signaux carrés**<br>
**Le générateur de signaux** va produire [**un signal carré**](https://fr.wikipedia.org/wiki/Signal_carr%C3%A9) à l'aide de son [**convertisseur numérique-analogique (DAC)**](../startl476/dac), en calculant sa [**série de Fourier**](https://fr.wikipedia.org/wiki/S%C3%A9rie_de_Fourier) jusqu'à un terme donné. Pour construire ce générateur, nous utiliserons une [carte NUCLEO-L476RG](../install_win_l476/index).
 Nous le testerons avec [**un analyseur logique Picoscope 2205A**](https://www.picotech.com/oscilloscope/2000/Picoscope-2000-overview).

- **Partie 4 : Connexion de l'analyseur de signaux au générateur de signaux**<br>
On vérifiera que l'analyseur détecte bien les différentes fréquences caractéristiques, [**fondamentale** et **harmoniques**](https://fr.wikipedia.org/wiki/Harmonique_(musique)), du signal délivré par le générateur. 

## Partie 1 : Analyse de Fourier d'un signal périodique

L'analyse de Fourier est un outil mathématique fondamental en [traitement du signal](https://fr.wikipedia.org/wiki/Traitement_du_signal). Elle intéressera en priorité les élèves du supérieur compte-tenu du bagage mathématique nécessaire pour l'aborder. Nous n'allons évidemment pas écrire un cours sur ce sujet, nous ne ferions pas mieux que les abondantes ressources déjà disponibles sur Internet (voir la section **Liens et références** en fin de tutoriel). Nous donnons cependant quelques explications indispensables pour comprendre ce que nous allons réaliser dans ce tutoriel.

>**NB :** La présentation qui suit est inspirée de [cet article](https://fais-tes-effets-guitare.com/spectre-et-serie-de-fourier/) sur le site **"Fais tes effets guitare"**.

### Le générateur de signaux : Séries de Fourier et spectre

Considérons tout d'abord un signal "pur", représenté par une sinusoïde de fréquence *f* dont l'amplitude varie de *-a* à *+a*. Son équation est :

<div align="left">
<img alt="Fondamentale" src="images/fourier_sinus.jpg" width="150px">
</div>
<br>

Cette fonction **périodique** (qui redevient égale à elle même toutes les *T* secondes), peut être représentée de deux façons, soit en traçant *son amplitude en fonction du temps*, soit en représentant *son amplitude en fonction de la fréquence*, **son spectre**. Celui-ci est  constitué d'un unique pic d'amplitude *a* pour la fréquence *f*.<br>

Les deux représentations sont dites *duales* :

|Graphique de *F(t)*|Spectre de *F(t)*|
|:-:|:-:|
|<img alt="Sinusoïde" src="images/sinusoide.jpg" width="330px">|<img alt="Spectre d'une sinusoïde" src="images/spectre_sinusoide.jpg" width="230 px">|

[Le mathématicien Jean Baptiste Joseph Fourier](https://fr.wikipedia.org/wiki/Joseph_Fourier) a démontré que, plus généralement, tout signal périodique peut être décrit comme une superposition (une somme infinie) de **signaux sinusoïdaux**. Cette somme s'appelle une [**série de Fourier**](https://fr.wikipedia.org/wiki/S%C3%A9rie_de_Fourier). Le premier terme de la série de Fourier est **sa sinusoïde fondamentale** de fréquence *f* et les termes successifs sont ses **harmoniques**, des sinusoïdes dont les fréquences sont des multiples entiers de *f*.

Il est donc **en théorie** possible de reconstruire parfaitement n'importe quel signal périodique, **en particulier un [signal carré](https://fr.wikipedia.org/wiki/Signal_carr%C3%A9)**, en calculant puis en ajoutant à sa sinusoïde fondamentale toutes les sinusoïdes harmoniques qui le composent. 
Pour un signal carré de fréquence *f* dont l'amplitude varie de *-a* à *+a* [on démontre](https://www.unige.ch/sciences/physique/tp/tpi/Liens/Protocoles/Complements/Decomp-serie-fourier-signal-periodique.pdf) que la série de Fourier est :

<div align="left">
<img alt="Série de Fourier d'un signal carré" src="images/fourier_carre.jpg" width="550px">
</div>
<br>

Les "..." à la fin rappellent que cette série comporte une **infinité de termes**. La représentation graphique de cette somme **infinie** est notre signal carré dans l'espace amplitude-temps. La figure suivante illustre le principe de cette décomposition en série de Fourier :

<div align="left">
<img alt="Signal carré et somme de sinusoïdes" src="images/carre_somme_sinus.jpg" width="850px">
</div>
<br>

Mais on peut aussi représenter le **spectre** de la série de Fourier, c'est à dire les différentes amplitudes des ses termes (fondamental et harmoniques) en fonction de leurs fréquences, exactement comme on l'avait fait pour la fonction sinus seule. Le signal carré et (une partie de) son spectre sont représentés ci-après :

|Signal carré *F(t)*|Spectre de *F(t)*|
|:-:|:-:|
|<img alt="Signal carré" src="images/signal_carre.jpg" width="350px">|<img alt="Spectre d'un signal carré" src="images/spectre_signal carre.jpg" width="360 px">|

<br>

Notre générateur de signaux va reconstruire la série de Fourier d'un signal carré de fréquence *f* et la générer en tension modulée, dans l'intervalle [0 : 3.3V] à l'aide de son DAC. Cependant, le passage de la théorie à la pratique **impose des approximations** au modèle mathématique :

- **Il n'est bien sûr pas possible de calculer une somme infinie**. Notre générateur devra donc "tronquer" la série à un certain ordre, ne calculer que la somme de sa fondamentale et d'un nombre fini (nous avons choisi 49) de ses harmoniques, on parle de "somme partielle".

- **Les calculs effectués par un microprocesseur ne sont pas d'une précision infinie**. Par exemple, pour des nombres à virgule flottante codés sur 32 bits (*floats 32*), la norme IEEE 754 approxime *π* (qui a un nombre infini de décimales) par *3.141592*. Par ailleurs, les amplitudes des harmoniques diminuent rapidement et, au-delà d'un certain ordre, elles sont tellement faibles que l'accumulation des erreurs dues aux arrondis sur les *floats* rendent leur calcul impossible.

Finalement, le signal généré sur le DAC **ne sera jamais exactement carré** ; il comportera **des oscillations de haute fréquence** dans les parties abruptes de ses transitions. C'est ce que l'on appelle le [**phénomène de Gibbs**](https://fr.wikipedia.org/wiki/Approximation_sigma). NB : Il existe une "astuce mathématique" pour atténuer le phénomène de Gibbs, [**l'approximation sigma**](https://fr.wikipedia.org/wiki/Approximation_sigma), mais nous ne l'utiliserons pas pour ne pas complexifier inutilement le script du générateur de signaux. La figure ci-dessous illustre le phénomène de Gibbs pour une série de Fourier tronquée à son quatrième terme : 

|Signal carré "incomplet" *F(t)*|Spectre "tronqué" de *F(t)*|
|:-:|:-:|
|<img alt="Signal carré incomplet" src="images/signal_carre_incomplet.jpg" width="375px">|<img alt="Spectre d'un signal carré incomplet" src="images/spectre_signal carre_incomplet.jpg" width="310 px">|

### L'analyseur de signaux : Echantillonnage, DFT et FFT

L'analyseur de signaux réalise **l'opération réciproque** du générateur de signaux :

1. Il reçoit sur une entrée analogique un signal modulé en tension au cours du temps, de forme à priori arbitraire ;
2. Il commence par **[l'échantillonner](https://fr.wikipedia.org/wiki/%C3%89chantillonnage_(signal)) avec son ADC** ;
3. Il calcule ensuite la [**DFT**](https://fr.wikipedia.org/wiki/) de l'échantillon (à l'aide d'un [algorithme de **FFT**]((https://fr.wikipedia.org/wiki/Transformation_de_Fourier_rapide) )), de laquelle on déduit **son spectre** ;
4. Il affiche enfin **les composantes de plus grande amplitude** du spectre et leurs fréquences. On vérifiera que ce spectre est bien celui du signal carré approximatif provenant du générateur.

**Echantillonnage et théorème de Shannon - Nyquist**

**L'échantillonnage est l'étape la plus sensible**. En effet, pour que l'analyse de Fourier soit possible, il faut que le nombre de mesures par période soit suffisant pour "capturer" les caractéristiques du signal d'entrée. C'est toute la problématique du [**théorème d'échantillonnage (ou de Shannon - Nyquist)**](https://fr.wikipedia.org/wiki/Th%C3%A9or%C3%A8me_d%27%C3%A9chantillonnage), intuitivement facile à comprendre à l'aide de la figure ci-dessous :

<div align="left">
<img alt="Echantillonnage d'une sinusoïde" src="images/echantillonnages_sinus.png" width="700px">
</div>
<br>

> Source : [Wikipedia](https://fr.wikipedia.org/wiki/%C3%89chantillonnage_(signal)#/media/Fichier:Echantillonnages_sinus.png)

Dans le premier cas, on à suffisamment de mesures pour reconstruire la sinusoïde. Dans le second cas, on a moins qu'un échantillon par période du signal et on ne peut pas le reconstituer. **Dit autrement :**

>**La fréquence d'échantillonnage doit être au moins égale au double de la plus grande fréquence (dite [de Nyquist](https://fr.wikipedia.org/wiki/Fr%C3%A9quence_de_Nyquist)) que l'on souhaite résoudre dans le signal d'entrée**.

Ceci pose une contrainte sur les performances de l'ADC du STM32WB55 qui ne sera pas capable, selon la fréquence *f* du signal carré qu'on lui enverra, d'échantillonner assez vite pour résoudre les harmoniques successives du signal (de fréquences *3f*, *5f*, *7f* ...) au-delà d'un ordre donné (voire même de résoudre sa fondamentale si *f* est trop élevée). 
D'après [la datasheet du STM32WB55](https://www.st.com/resource/en/datasheet/stm32wb55cc.pdf), la fréquence maximum d'acquisition de l'ADC est de 4.26 MHz. Nous avons donc assez de ressources pour notre exemple, qui utilise  *f = 100 Hz*.

**Discrete Fourier Transform (DFT) et Fast Fourier Transform (FFT)**

La [**DFT**](https://fr.wikipedia.org/wiki/Transformation_de_Fourier_discr%C3%A8te), acronyme anglais de **Discrete Fourier Transform** (fr : **Transformation de Fourier Discrète**) suppose que le signal d'où provient l'échantillon est **périodique**. Dans le cas contraire, elle va renvoyer un spectre sans fondamentale ou harmoniques remarquables, impossible à interpréter. La DFT est une opération à priori difficile à réaliser sur un microcontrôleur car elle nécessite un grand nombre de calculs. 

[**L'algorithme FFT**](https://fr.wikipedia.org/wiki/Transformation_de_Fourier_rapide), acronyme anglais de **Fast Fourier Transform** (fr : **Transformation de Fourier Rapide**) rend la DFT possible sur des systèmes embarqués. Un certain nombre de microcontrôleurs ARM, dont le STM32WB55RG de notre carte NUCLEO-WB55, disposent de périphériques [**DSP (digital signal processor)**](https://fr.wikipedia.org/wiki/Processeur_de_signal_num%C3%A9rique) intégrés permettant d'accélérer le calcul de la FFT. Dans ce tutoriel, nous ne tirons pas profit du DSP, nous utilisons [**une implémentation MicroPython/assembleur ARM développée par Peter Hinch**](https://github.com/peterhinch/micropython-fourier).

Signalons que la DFT peut être généralisée à des signaux non périodiques par la [**transformation de Fourier**](https://fr.wikipedia.org/wiki/Transformation_de_Fourier), l'un des piliers des mathématiques pour la physique.

## Partie 2 : Conception de l'analyseur de signaux

Vous trouverez ici les indications pour construire et tester l'analyseur de signaux.

### Le matériel

1. Une [carte d'extension de base Grove](https://wiki.seeedstudio.com/Base_Shield_V2/) **avec son commutateur d'alimentation positionné sur 3V3**.
2. La carte NUCLEO-WB55
3. Un [module capteur de luminosité Grove](https://wiki.seeedstudio.com/Grove-Light_Sensor/).
4. Une source de lumière fluorescente (lampe ou tube). 

Connectez le capteur sur **la fiche A0**.

### Le script MicroPython de l'analyseur de spectre

> **Le script présenté ci-après est disponible [dans cette archive ZIP](../../../assets/Script/MODULES.zip)** à l'intérieur du dossier *Générateur et analyseur de signaux\Partie 2\\.*

Copiez les fichiers *dft.py*, *dftclass.py*, *polar.py* et *window.py* dans *PYBFLASH*. Les deux derniers fichiers ne seront pas utilisés par notre programme, mais ils font partie intégrante de [la bibliothèque de FFT développée par Peter Hinch](https://github.com/peterhinch/micropython-fourier), aussi n'avons nous pas pris la liberté d'enlever les référence à ces bibliothèques dans *dftclass.py*. C'est une petite optimisation que vous pourrez réaliser si vous le souhaitez.

Créez un script *main.py* sur votre ordinateur et copiez-y le code suivant, puis glissez-déplacez le dans *PYBFLASH* sur votre NUCLEO-WB55 :

```python
# Objet du script :
# Capturer un signal analogique avec un ADC et extraire ses fréquences à l'aide
# d'une transformation de Fourieur discrète (DFT).
# Reçoit en entrée un signal analogique modulé en tension, généré par une
# photodiode éclairée avec une lampe fluo-compacte.
# Au lancement du script, notre analyseur recalcule spectre de Fourier
# du signal lumineux, qui devrait avoir une fondamentale à f = 100 Hz.
# Le code pour la DFT est celui de Peter Hinch :
#    https://github.com/peterhinch/micropython-fourier

# Bibliothèques
from math import sqrt
from pyb import Pin
from time import sleep_ms
 
# Obtient le deuxième élément d'un couple
@micropython.native 
def takeSecond(elem):
	return elem[1]

# Extrait les fréquences du signal connecté à l'ADC sur 'A0'
@micropython.native
def find_frequency():

	# Nombre maximum de fréquences à afficher
	MAX_PEAKS = const(5)

	# Classe DFT par Peter Hinch 
	from dftclass import DFTADC, FORWARD
	
	# Nombre de mesures réalisées par l'ADC
	# Attention : pour les besoins de la DFT, ce doit être
	# une puissance de 2 !
	NBSAMPLES = const(128) 
	acq_time = 0.1 # Durée de l'acquisition en secondes
	
	freq_samp = NBSAMPLES / acq_time
	print("Fréquence d'échantillonage (Hz) : ", freq_samp)
	
	# Pour la conversion des index en fréquences dans la DFT
	index_to_freq = 1 / acq_time

	# Acquisition d'un échantillon du signal par l'ADC
	mydft = DFTADC(NBSAMPLES,'A0',timer = 1)

	# Calcul de la DFT, les valeurs sont dans 
	# mydft.re (partie réelle) et mydft.im (partie imaginaire).
	dft_time = mydft.run(FORWARD,acq_time)
	
	print('Temps de calcul de la DFT (µs) : ', dft_time)

	half_NBSAMPLES = NBSAMPLES//2
	
	# Liste pour collecter les couples (fréquence, amplitude) 
	freq_amp = []
	
	for i in range(1,half_NBSAMPLES):
		# Calcul des amplitudes de la DFT
		x = (mydft.re)[i]
		y = (mydft.im)[i]
		fft_norm = sqrt(x*x + y*y)

		# Crée une liste de couples (fréquence, amplitude) 
		freq_amp.append((i*index_to_freq, fft_norm))

	# Trie la liste des couples (fréquence, amplitude) dans l'odre des
	# amplitudes décroissantes.
	freq_amp.sort(reverse = True, key = takeSecond)

	# Affiche les MAX_PEAKS premières composantes par amplitudes décroissantes.
	for i in range(0, MAX_PEAKS):
		p = freq_amp[i]
		print(i+1, 'Freq (Hz) : ', round(p[0],1) , 'Ampl (AU) : ', round(p[1],1))
	
	# Ligne de séparation (composée de 50 caractères '-')
	print('-' * 50)

# Boucle principale
while True:

	# Extrait les fréquences du signal connecté à l'ADC sur 'A0'
	find_frequency()

	# Temporisation de 5 secondes
	sleep_ms(5000)
```

### Application à la mesure de la fréquence d'une lampe fluo-compacte

L'éclairage par fluorescence est une technologie désuète et polluante, complètement supplantée par l'éclairage LED, mais bien pratique pour notre exercice car l'intensité de sa lumière oscille à 100 Hz.

Connectez un terminal PuTTY au firmware MicroPython de la carte NUCLEO-WB55, comme indiqué [ici](../install_win_wb55/index) (systèmes Windows), et lancez le script avec *[CTRL]-[D]*. 

Dans la pénombre, à l'intérieur, vous devriez observer les logs suivants :

```console
>>>
MPY: sync filesystems
MPY: soft reboot
Fréquence d'échantillonage (Hz) :  1280.0
Temps de calcul de la DFT (µs) :  2515
1 Freq (Hz) :  10.0 Ampl (AU) :  0.6
2 Freq (Hz) :  50.0 Ampl (AU) :  0.4
3 Freq (Hz) :  20.0 Ampl (AU) :  0.3
4 Freq (Hz) :  100.0 Ampl (AU) :  0.2
5 Freq (Hz) :  30.0 Ampl (AU) :  0.2
```

**Aucune fréquence** ne se détache clairement.

Allumez à présent la lampe fluo-compacte, mais n'éclairez pas la photodiode directement avec pour qu'elle ne sature pas, et relancez le script :

```console
>>>
MPY: sync filesystems
MPY: soft reboot
Fréquence d'échantillonage (Hz) :  1280.0
Temps de calcul de la DFT (µs) :  2515
1 Freq (Hz) :  100.0 Ampl (AU) :  4.6
2 Freq (Hz) :  200.0 Ampl (AU) :  1.4
3 Freq (Hz) :  300.0 Ampl (AU) :  0.5
4 Freq (Hz) :  10.0 Ampl (AU) :  0.4
5 Freq (Hz) :  400.0 Ampl (AU) :  0.2 
```

**La fréquence de 100 Hz est bien mesurée avec une amplitude dominante** et, peut-être (?), quelques uns de ses harmoniques. 

## Partie 3 : Conception du générateur de signaux carrés

Vous trouverez ici les indications pour construire et tester le générateur de signaux carrés.

### Le matériel

1. Une carte NUCLEO-L476RG
2. Deux câbles Dupont mâles
3. Un analyseur logique [**Picoscope 2205A**](https://www.picotech.com/oscilloscope/2000/Picoscope-2000-overview).

Connectez ...
- Un câble Dupont sur la broche *'A2'* (signal) de la NUCLEO-L476 ;
- Un câble Dupont sur l'une des broches *'GND'* (masse) de la NUCLEO-L476 ;
- L'autre extrémité de ces deux câbles Dupont (signal et masse) à l'une des sondes du Picoscope.

**NB : L'analyseur logique n'est pas indispensable**. On pourrait, par exemple, envoyer une copie du signal qui pilote le DAC sur un port série de la carte NUCLEO-L476 et le visualiser avec l'outil [**traceur série de l'IDE Arduino**](../../Stm32duino/installation) ou tout autre logiciel capable de réaliser une représentation graphique d'un flux de données sur un port série.

Mais cela impliquerait de complexifier le script MicroPython de l'analyseur en y rajoutant la gestion d'un port série pour exporter les données, ce que nous ne souhaitions pas faire. C'est un bon exercice complémentaire pour aborder l'UART, en réception par interruptions (voir [cet exemple](../startwb55/uart)) ou encore **en programmation asynchrone** (voir [cet exemple](https://github.com/peterhinch/micropython-async/blob/master/v3/as_demos/auart.py)), une approche très puissante propre à MicroPython.

### Le script MicroPython du générateur de signal carré

**1. Précisions préalables**

Le script de l'analyseur devra calculer les *N+1* premiers termes de la somme partielle de Fourier d'un signal carré de fréquence *f* et d'amplitude *a*, que l'on peut exprimer comme ceci :

<br>
<div align="left">
<img alt="Série de Fourier tronquée d'un signal carré (1)" src="images/fourier_carre_truncated_0.jpg" width="350px">
</div>
<br>

Après avoir posé : 
- *a = π / 4* (Pour simplifier)
- *A<sub>i</sub> = 1/(2i+1)* (Coefficients de la série)
- *B<sub>i</sub> = 2i+1* (Coefficients de la série)
- *ω = 2πf* (La pulsation "oméga").  

Nous pouvons réécrire cette somme partielle sous la forme suivante :

<br>
<div align="left">
<img alt="Série de Fourier tronquée d'un signal carré (2)" src="images/fourier_carre_truncated_1.jpg" width="240px"> 
</div>
<br>

Nous utiliserons ces notations et conventions dans le script MicroPython de l'analyseur.<br>

Puisque notre somme partielle est une somme de fonctions sinus *périodiques*, elle est **nécessairement périodique aussi** (et de même période que son terme fondamental d'ordre `N = 0`). Nous pourrons donc nous contenter de calculer la somme partielle sur une seule période, discrétisée dans un tableau contenant `NB_SAMPLE` valeurs. Nous programmerons ensuite le DAC pour générer le "pseudo" signal carré en "lisant en boucle" ce tableau, à une fréquence que nous lui préciserons.

Nous prenons le parti de **désigner le produit *ωt*, où *t* est un instant donné**, comme étant la **phase** à l'instant *t*. On constate que la phase, telle que définie, est identique quel que soit le terme d'ordre *i* considéré ; elle ne varie que du fait de la croissance du temps écoulé *t* au sein d'une période. Elle peut donc également être pré-calculée pour chaque pas de temps au début du programme et enregistrée dans un tableau  contenant `NB_SAMPLE` éléments afin d'accéléer l'algorithme lors de l'ajout de nouveaux termes.

**2. Le script commenté**

> **Le script présenté ci-après est disponible [dans cette archive ZIP](../../../assets/Script/MODULES.zip)** à l'intérieur du dossier *Générateur et analyseur de signaux\Partie 3\\.* 

Créez un script *main.py* sur votre ordinateur et copiez-y le code suivant, puis transférez le dans le système de fichiers MicroPython sur votre NUCLEO-L476RG à l'aide de l'utilitaire en ligne de commande [**Ampy**](https://github.com/scientifichackers/ampy)  comme expliqué sur [cette page](../../tools/rshell_ampy/index) :

```python
# Objet du script :
# Générer un signal rectangulaire à l'aide de sa série de Fourier avec le DAC d'une
# carte NUCLEO-L476RG, en lui ajoutant progressivement des termes (fondamentale, harmoniques).
# On commence avec le terme du premier ordre de (une fonction sinus) et, à chaque appui 
# sur le bouton utilisateur (bleu), on lui ajoute un terme d'ordre supérieur.
# On peut ainsi voir évoluer, avec un oscilloscope ou bien grâce à un échantillonnage en
# continu sur l'ADC d'une autre carte à microcontrôleur, la forme du signal depuis une sinusoïde
# jusqu'à un oscillogramme proche d'un signal carré (50 termes max dans notre exemple).

# Matériel utilisé :
# - Une carte NUCLEO-L476RG. La broche de sortie de son unique DAC est 'A2'.
#   Cette information est disponible ici : https://os.mbed.com/platforms/ST-Nucleo-L476RG/.
# - Deux câbles Dupont mâles

# Importation des bibliothèques (classes et méthodes)
from math import sin, pow, pi
from array import array
from pyb import DAC, Pin
import gc

# Déclaration du bouton utilisateur de la NUCLEO-L476RG.
# L'alias 'SW' pour sa broche 'PC13' peut être trouvé ici :
# https://github.com/Micropython/micropython/blob/master/ports/stm32/boards/NUCLEO_L476RG/pins.csv

USR_BTN = Pin('SW', Pin.IN, Pin.PULL_UP)

# Est-ce qu'une nouvelle série doit être recalculée ?
COMPUTE = 1

# Fonction de gestion de l'interruption du bouton
def USR_BTN_ISR(pin):

	# Déclarations "global" indispensables dans les fonctions
	# pour éviter des comportements aléatoires du programme !
	global COMPUTE
	
	# Si aucun recalcul de la série n'est en cours ...
	if COMPUTE == 0:
		# Lance l'ordre de recalculer la série
		COMPUTE = 1

# Activation de l'interruption du bouton utilisateur
USR_BTN.irq(trigger = USR_BTN.IRQ_FALLING, handler = USR_BTN_ISR)

# Fréquence du signal (Hz)
FEQUENCY_HZ = const(100)

# Nombre de pas de temps, et donc d'échantillons, constituant une 
# période du signal. 
NB_SAMPLE = const(128)

# Pulsation du signal (en rad/s)
OMEGA = 2 * pi / NB_SAMPLE

# Nombre maximum de termes dans la série de Fourier du signal carré
# que nous allons générer.
NB_TERM_MAX = const(50)

# Nombre actuel de termes dans la série de Fourier du signal.
# On commence avec aucun terme, donc, au démarrage le DAC émettra
# une fonction sinus de fréquence FEQUENCY_HZ et d'amplitude variant 
# entre 0V et 3,3 V.
NB_TERM = 1

# Résolution du DAC : 2^12
DAC_RES = const(12)

# Valeur maximum de commande du DAC
DAC_MAX = pow(2,DAC_RES)-1

# Instanciation du DAC
dac = DAC(1, bits=DAC_RES)

# Tableau de floats qui contiendra les coefficients de Fourier Ai du signal carré
# précalculés jusqu'au terme NB_TERM_MAX.
A = array('f', [0] * NB_TERM_MAX)

# Tableau de floats qui contiendra les coefficients de Fourier Bi du signal carré
# précalculés jusqu'au terme NB_TERM_MAX.
B = array('f', [0] * NB_TERM_MAX)

#  Tableau de floats qui contiendra les phases de la fonction sinus sur une période
phasis = array('f', [0] * NB_SAMPLE)

# Pré-calcule les coefficients Ai et Bi de la série de Fourier d'un signal carré 
# jusqu'à l'ordre N :
#	A[i] Suite des inverses des entiers impairs positifs : 1, 3, 5, 7, 9 ...
#	B[i] Suite des entiers impairs positifs : 1, 3, 5, 7, 9 ...

@micropython.native # Optimisation du bytecode pour STM32
def compute_coefs():

	# Déclarations "global" indispensables dans les fonctions
	# pour éviter des comportements aléatoires du programme !
	global A
	global B
	
	# Précalcul des NB_TERM_MAX premiers coefficients de la série de 
	# Fourier d'un signal carré et de leurs inverses
	B[0] = 1
	A[0] = 1
	for i in range(1,NB_TERM_MAX):
		B[i] = B[i - 1] + 2
		A[i] = 1/B[i]

# Précalcul des phases de la fonction sinus sur une période
@micropython.native 
def compute_phasis():

	global phasis
	
	for t in range(NB_SAMPLE):
		phasis[t] = OMEGA * t

# Tableau de floats qui contiendra une période du signal carré.
# On réalise tous les calculs avec des floats pour ne pas perdre en précision.
# Ils ne seront convertis en entiers codés sur 16 bits (pour programmer le DAC) 
# qu'en toute dernière opération, dans la fonctions update_DAC_buffer (ci-après).
period_data = array('f',[0]*NB_SAMPLE)

# Minimum et maximum de la série de Fourier sur une période
max_val = 0
min_val = 0

# Précalcule une période du signal carré
@micropython.native
def compute_square_period():

	# Déclarations "global" indispensables dans les fonctions
	# pour éviter des comportements aléatoires du programme !
	global period_data
	global A
	global B
	global min_val, max_val
	global NB_TERM
	global OMEGA
	
    # Valeurs initiales des amplitudes max et min de la série sur la période,
    # volontairement irréalistes (voir normalisation plus bas)
	max_val = - NB_TERM
	min_val = NB_TERM
	
	# Calcul de la série de Fourier sur tous les NB_SAMPLE instants d'un période
	for t in range(NB_SAMPLE):

		# valeur de la série de fourier à l'instant t de la période calculée dernièrement
		val = period_data[t]
		
		# Calcule le terme suivant de la série de Fourier et ajoute le
		val += A[NB_TERM - 1] * sin(B[NB_TERM - 1] * phasis[t])

		# Enregistre la nouvelle valeur de la série de fourier à l'instant t de la période
		period_data[t] = val
		
		# Normalisation :
		# Calcule les valeurs minimum et maximum de la série de Fourier sur la période
		# Elles seront utiles pour renormaliser l'amplitude de celle-ci entre 0 et 2^12 - 1
		# (valeur de commande maximum à envoyer au DAC pour une sortie en 3.3V sur 'A2').
		if val > max_val:
			max_val = val
		elif val < min_val:
			min_val = val

# Génère le signal carré avec le DAC
@micropython.native
def update_DAC_buffer():

	# Déclarations "global" indispensables dans les fonctions
	# pour éviter des comportements aléatoires du programme !
	global period_data
	global min_val, max_val

	# Renormalise la série de Fourier calculée dans le tableau de floats 'period_data' et  
	# écrit le résultat dans le tableau d'octets (entiers non signés) 'dac_data' qui servira 
	# d'argument au DAC.
	inv_range = DAC_MAX / (max_val - min_val)
	dac_data = array('H', int((period_data[t] - min_val)*inv_range) for t in range(NB_SAMPLE))
	
	# Programme le DAC pour générer en boucle la période du signal contenu dans dac_data,
	# à la fréquence FEQUENCY_HZ x NB_SAMPLE. Démarre la génération du signal, qui sortira 
	# en modulation de tension sur la broche du DAC,'A2'.
	dac.write_timed(dac_data, FEQUENCY_HZ * len(dac_data), mode=DAC.CIRCULAR)

 # Pré-calcule (une seule fois) les NB_TERM_MAX premiers coefficients de 
 # la série de Fourier d'un signal carré.
compute_coefs()

 # Précalcule (une seule fois) les NB_SAMPLE valeurs de la phase sur une période
compute_phasis()

# Boucle principale, à l'écoute des interruptions du bouton 
# utilisateur via la variable globale COMPUTE.
while True:
	
	# Si une demande de calcul est reçue ...
	if COMPUTE:
		
		# Incrémente le nombre de coefficients de Fourier
		NB_TERM += 1
		# Si on excède NB_TERM_MAX termes, on recommence avec un seul terme
		if NB_TERM > NB_TERM_MAX:
			NB_TERM = 1 # On recommence avec un seul terme
			period_data = [0]*NB_SAMPLE # On réinitialise la période

		# Appel du ramasse-miettes (ne peut pas faire de mal !)
		gc.collect()
		
		if NB_TERM == 1:
			print('Calcul du premier terme de la série de Fourier')
		else:
			print('Ajout du %dème terme de la série de Fourier' %NB_TERM)

		# Recalcule une période de la nouvelle série
		compute_square_period()
		
		# Charge dans le DAC la nouvelle série
		update_DAC_buffer()
		
		# Indique que le calcul est terminé
		COMPUTE = 0
		
		print('Calcul terminé !')

```

### Visualisation avec un Picoscope

Connectez un terminal PuTTY au firmware MicroPython de la carte NUCLEO-L476, comme indiqué [ici](../install_win_l476/index) (systèmes Windows), et lancez le script avec *[CTRL]-[D]*. Appuyez quatre fois sur le bouton bleu de la carte et laissez le temps au script de réaliser les calculs :

```console
>>>
MPY: sync filesystems
MPY: soft reboot
Calcul du premier terme de la série de Fourier
Calcul terminé !
Ajout du 2ème terme de la série de Fourier
Calcul terminé !
Ajout du 3ème terme de la série de Fourier
Calcul terminé !
Ajout du 4ème terme de la série de Fourier
Calcul terminé !
```

En parallèle, sur le Picoscope, vous devriez observer le signal carré approximatif avec le phénomène de Gibbs :

<div align="left">
<img alt="Echantillonnage d'une sinusoïde" src="images/picoscope_spectre ordre 4.jpg" width="800px">
</div>
<br>

Vous pouvez continuer d'appuyer sur le bouton USER et observer l'évolution du signal dont la forme se rapproche d'un carré lorsque le nombre de coefficients de la série de Fourier partielle augmente.

## Partie 4 : Générateur de signaux & analyseur de signaux

Dans cette dernière partie, nous connections le générateur de signaux à l'analyseur de signaux. Nous vérifions que l'analyseur identifie correctement les fréquences du signal envoyé par le générateur.

### Le matériel

1. Une carte NUCLEO-L476 (analyseur)
2. Une carte NUCLEO-WB55 (générateur)
3. Trois câbles Dupont mâles

Connectez, avec les câbles Dupont ...
- La broche *'A2'* de la NUCLEO-L476 sur la broche *'A0'* de la NUCLEO-WB55 (signal) ;
- Une broche *'GND'* de la NUCLEO-L476 sur une broche *'GND'* de la NUCLEO-WB55 (masse commune) ;
- La broche *'D2'* de la NUCLEO-L476 sur la broche *'D2'* de la NUCLEO-WB55 (commande de  prise de mesure du générateur à l'analyseur).

Le système est représenté par la figure suivante :

<br>
<div align="left">
<img alt="Générateur et analyseur de signaux" src="images/generateur_analyseur.jpg" width="600px">
</div>
<br>

### Les scripts MicroPython

> **Les scripts de l'analyseur et du générateur sont disponibles [dans cette archive ZIP](../../../assets/Script/MODULES.zip)** à l'intérieur du dossier *Générateur et analyseur de signaux\Partie 4\\.* 

Pour ne pas rallonger ce tutoriel plus que de raison, nous ne les reproduisons pas ici. Sachez que :

- **Le script de l'analyseur** est pratiquement identique à celui de la Partie 3, et nous vous renvoyons à celle-ci pour les modalités pour le charger dans la NUCLEO-WB55. Une seule différence notable : nous lui avons ajouté la gestion d'une interruption sur la broche *'D2'*, qui surveille les fronts montants et force une mesure lorsqu'il s'en produit.

- **Le script du générateur** est pratiquement identique à celui de la Partie 3, et nous vous renvoyons à celle-ci pour les modalités pour le charger dans la NUCLEO-L476. Une seule différence notable : nous générons une impulsion sur la broche *'D2'* à chaque génération d'un nouveau signal.

### Mise en œuvre

Connectez un terminal PuTTY à l'analyseur et au générateur, puis générez un signal avec 7 harmoniques sur le générateur, en appuyant 8 fois sur le bouton USER (BLEU) de la NUCLEO-L476.

Si votre montage est correct, vous devriez lire **sur le terminal PuTTY de l'analyseur** la fondamentale (100 Hz) et les 7 premiers harmoniques d'un signal carré (300 Hz, 500 Hz, 700 Hz, etc.) comme ceci :

```console
Signal reçu, extraction des fréquences
Fréquence d'échantillonage (Hz) :  10240.0
Temps de calcul de la DFT (µs) :  25725
1 Freq (Hz) :  100.0 Ampl (AU) :  1137.4
2 Freq (Hz) :  300.0 Ampl (AU) :  375.2
3 Freq (Hz) :  500.0 Ampl (AU) :  221.0
4 Freq (Hz) :  700.0 Ampl (AU) :  153.7
5 Freq (Hz) :  900.0 Ampl (AU) :  115.7
6 Freq (Hz) :  1100.0 Ampl (AU) :  90.2
7 Freq (Hz) :  1300.0 Ampl (AU) :  72.9
8 Freq (Hz) :  1500.0 Ampl (AU) :  60.3
9 Freq (Hz) :  1090.0 Ampl (AU) :  8.5
...
```
Vous pouvez pousser plus loin la génération d'harmoniques, le script de l'analyseur devrait être capable d'en résoudre au moins 24.


## Pour aller plus loin

Une amélioration évidente de cet exemple consisterait à modifier le code du générateur afin qu'il puisse générer d'autres types de signaux (triangulaires par exemple).

## Liens et références 

Voici la liste des références sur **l'analyse de Fourier** que nous avons utilisées au fil de ce tutoriel :

- [**Signal carré** (Wikipedia)](https://fr.wikipedia.org/wiki/Signal_carr%C3%A9)
- [**Séries de Fourier** (Wikipedia)](https://fr.wikipedia.org/wiki/S%C3%A9rie_de_Fourier)
- [**Harmoniques** (Wikipedia)](https://fr.wikipedia.org/wiki/Harmonique_(musique))
- [**Série et spectre de Fourier d'un signal carré** (Fais tes effets guitare)](https://fais-tes-effets-guitare.com/spectre-et-serie-de-fourier/)
- [**Décomposition en séries de Fourier d'un signal périodique** (Université de Genève)](https://www.unige.ch/sciences/physique/tp/tpi/Liens/Protocoles/Complements/Decomp-serie-fourier-signal-periodique.pdf)
- [**Phénomène de Gibbs et approximation sigma** (Wikipedia)](https://fr.wikipedia.org/wiki/Approximation_sigma)
- [**Transformation de Fourier** (Wikipedia)](https://fr.wikipedia.org/wiki/Transformation_de_Fourier) 
- [**Transformation de Fourier discrète (DFT)** (Wikipedia)](https://fr.wikipedia.org/wiki/Transformation_de_Fourier_discr%C3%A8te)
- [**Transformation de Fourier rapide (FFT)** (Wikipedia)](https://fr.wikipedia.org/wiki/Transformation_de_Fourier_rapide)
- [**Echantillonnage** (Wikipedia)](https://fr.wikipedia.org/wiki/%C3%89chantillonnage_(signal))
- [**Fréquence de Nyquist** (Wikipedia)](https://fr.wikipedia.org/wiki/Fr%C3%A9quence_de_Nyquist)
- [**Théorème d'échantillonnage** (Wikipedia)](https://fr.wikipedia.org/wiki/Th%C3%A9or%C3%A8me_d%27%C3%A9chantillonnage)

La **bibliothèque FFT réalisant la DFT en MicroPython**, utilisée dans l'analyseur de signaux, se trouve ici :
- [**Single precision FFT written in ARM assembler** (Peter Hinch)](https://github.com/peterhinch/micropython-fourier)