---
title: Ruban de LED RGB Grove Neopixel
description: Mise en œuvre d'un ruban de LED Grove WS2813 avec MicroPython
---

# Rubans de LED RGB Grove Neopixel

Ce tutoriel explique comment mettre en œuvre un ruban de LED Grove WS2813 avec MicroPython.<br>
Ce ruban de LED est constitué d'une succession de LED RGB adressables dites  **Neopixel** . On peut définir l'intensité et la couleur de chaque LED via le contrôleur WS28xxx. Les afficheurs Neopixel peuvent être gérés par la NUCLEO-WB55 selon deux protocoles les connectant à une broche numérique exploitant un pilote spécifique disponible (au moins) depuis la révision 1.17 du firmware MicroPython.

**Le module Grove ruban de 60 LED RGB WS2813 Mini :**

<br>
<div align="left">
<img alt="Grove - 60 RGB LED ribbon" src="images/grove-rgb-60-led-strip-ws2813.jpg" width="350px">
</div>
<br>

> Crédit image : [Seeed Studio](http://wiki.seeedstudio.com/Grove-4-Digit_Display/)


## Matériel requis

1. Une [carte d'extension de base Grove](https://wiki.seeedstudio.com/Base_Shield_V2/) ;
2. La carte NUCLEO-WB55 ;
3. Un [ruban de LED RGB WS2813 Grove](https://www.seeedstudio.com/Grove-WS2813-RGB-LED-Strip-Waterproof-60-LED-m-1m.html).

On utilise un ruban Neopixel étanche équipé d'un connecteur Grove, avec 60 LED RGB en série. Connectez le ruban à l'une des broches numériques du Grove Base Shield (dans notre cas, on a choisi la broche `D2`), lui même installé sur la NUCLEO-WB55.

## Le code MicroPython pour afficher et faire défiler des drapeaux italiens et français

> **Vous pouvez télécharger les scripts MicroPython de ce tutoriel (entre autres) en cliquant [ici](../../../assets/Script/MODULES.zip)**.

Il faut récupérer et ajouter le fichier *neopixel.py* dans le répertoire du périphérique *PYBFLASH*. Editez maintenant le script *main.py* et collez-y le code qui suit :

```python
# Objet du script : Jeux de lumière avec un ruban Neopixel étanche de Grove (60 LED RGB)
# Nécessite également une NUCLEO-WB55 et un Grove Base Shield.
# Source : Christophe PRIOUZEAU, STMicroelectronics

import machine
import time

# Pour gérer le ruban de LED
import neopixel

class StripLED:
	def __init__(self, pin_number, ledNumber, intensity=0.5):
		self._pin = pin_number
		self._ledNumber = ledNumber
		self._intensity = intensity
		self._strip = neopixel.NeoPixel(machine.Pin(self._pin), self._ledNumber)

		self.french_flag = [
					(0,0,250), (0,0,250), (0,0,250),
					(250,250,250), (250,250,250), (250,250,250),
					(250,0,0), (250,0,0), (250,0,0)]
		self.italian_flag = [
					(0,250,0), (0,250,0), (0,250,0),
					(250,250,250), (250,250,250), (250,250,250),
					(250,0,0), (250,0,0), (250,0,0)]

	def _set_to_zero(self, addr):
		if addr < self._ledNumber:
			self._strip[addr] = (0,0,0)
	def _color_intensity(self, color):
		#print("[debug] [%d, %.2f] %d" % (color,  self._intensity, int(color * self._intensity) ))
		return int(color * self._intensity)
	def set_led(self, addr, red, green, blue):
		if addr < self._ledNumber:
			self._strip[addr] = (self._color_intensity(red), self._color_intensity(green), self._color_intensity(blue))

	def _flag_move(self, data, num_led, t):
		i = 0
		while i < (num_led+1):
			if (i + len(data)) < (num_led+1):
				if i != 0:
					self._set_to_zero(i-1)
				j = i
				for d in data:
					self.set_led(j, d[0], d[1], d[2])
					j+=1
				self._strip.write()
				time.sleep_ms(t)
				i+=1
			else:
				break

	def clear(self):
		for i in range(0, self._ledNumber):
			self._set_to_zero(i)
		self._strip.write()

	def get_max_led(self):
		return self._ledNumber

	def french_flag_move(self, t):
		self._flag_move(self.french_flag, self._ledNumber, t)

	def italian_flag_move(self, t):
		self._flag_move(self.italian_flag, self._ledNumber, t)

	def move_dual(self, t):
		self._dual_flag(self.french_flag, self.italian_flag, t)

	def _dual_flag(self, data0, data1, t):
		i = 0
		while i < (self._ledNumber+1):
			if (i + len(data0)) < (self._ledNumber+1):
				for d in data0:
					self.set_led(i, d[0], d[1], d[2])
					self._strip.write()
					time.sleep_ms(t)
					i+=1
				i+=1
			else:
				break
			if (i + len(data1)) < (self._ledNumber+1):
				for d in data1:
					self.set_led(i, d[0], d[1], d[2])
					self._strip.write()
					time.sleep_ms(t)
					i+=1
				i+=1
			else:
				break

# -------------------------------------
#				 MAIN
# -------------------------------------

_NB_LED_RGB = const(60) # Nombre de LED RGB sur le ruban
_INTENSITE_LED = 0.1 # Luminosité des LED (entre 0 et 1)

if __name__ == '__main__':

	# Ruban de LED connecté à D2 sur le Grove Base Shield
	strip_led = StripLED(pin_number='D2', ledNumber = _NB_LED_RGB, intensity= _INTENSITE_LED)

	# Fait défiler un drapeau français au long du ruban
	strip_led.clear()
	strip_led.french_flag_move(500)

	# Fait défiler un drapeau italien au long du ruban
	strip_led.clear()
	strip_led.italian_flag_move(500)
	
	# "Mappe" selon un cycle le ruban en alternant un drapeau français et un drapeau italien
	while True:
		strip_led.clear()
		strip_led.move_dual(100)
		time.sleep(4)
```

## Manipulation

Dans votre émulateur de terminal série, appuyez sur *[CTRL]-[D]* pour lancer le script.<br>
Si tout s'est déroulé correctement, vous verrez défiler les différents jeux de lumière sur le bandeau.

## Pour aller plus loin

Une approche efficace pour apprendre à programmer des effets de lumière sur un ruban Neopixel Grove consiste à utiliser [l'IDE Vittascience pour la NUCLEO-WB55](https://fr.vittascience.com/wb55/?mode=mixed&console=bottom&toolbox=vittascience). Dans la barre d'outils, le menu *Affichage* comporte plusieurs blocs Neopixel.
Construisez d'abord votre programme par blocs en mode hybride (programmation par blocs et affichage simultané du code MicroPython généré). Lorsque celui-ci est terminé, copiez le code MicroPython généré dans *main.py* et modifiez le comme bon vous semble.
