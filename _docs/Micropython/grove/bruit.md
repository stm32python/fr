---
title: Capteur de sons
description: Mise en œuvre du module capteur de sons Grove avec MicroPython
---

# Capteur de sons

Ce tutoriel explique comment mettre en œuvre un module capteur de sons Grove analogique avec MicroPython.
Plus précisément, nous allons construire un détecteur de bruits. On commence par calibrer le capteur : on mesure pendant un certain temps le volume sonore du lieu qui sera surveillé, en faisant le moins de bruits possible.
Une fois ce volume connu, le micro écoute et signale tout bruit plus intense.<br>

**Le module capteur de son Grove :**

<br>
<div align="left">
<img alt="Grove sound sensor" src="images/capteur_sonore.png" width="200px">
</div>
<br>

> Crédit image : [Seeed Studio](http://wiki.seeedstudio.com/Grove_System/)

## Matériel requis et montage

1. [Une carte d'extension de base Grove](https://wiki.seeedstudio.com/Base_Shield_V2/) **avec son commutateur d'alimentation positionné sur 5V**.
2. La carte NUCLEO-WB55
3. [Un module capteur de son Grove](https://wiki.seeedstudio.com/Grove-Sound_Sensor/)

D'après le script qui suit, le module doit être connecté sur une prise analogique du Grove Base Shield, nous choisissons le **connecteur A1**. **Attention**, il est prévu pour fonctionner avec une tension de 5V, vous devez donc placer le commutateur d'alimentation de la carte d'extension de base Grove sur la position 5V !

## Le code MicroPython

> **Vous pouvez télécharger les scripts MicroPython de ce tutoriel (entre autres) en cliquant [ici](../../../assets/Script/MODULES.zip)**.

Editez maintenant le script *main.py* du du périphérique *PYBFLASH* et copiez-y le code qui suit :

```python
# Objet du script : 
# Mise en œuvre du module capteur de son Grove
# Attention, ce module fonctionne en 5V!

from time import sleep_ms	# Pour temporiser
from pyb import Pin, ADC	# Gestion des broches et du coonvertisseur 
							# analogique-numérique (ADC)

adc = ADC(Pin('A1')) # On "connecte" l'ADC à la broche A1

# Première étape : calibrage
# On mesure le voulume sonore ambiant pendant quelques temps pour être en mesure
# de discriminer par la suite les bruits anormaux
# Il s'agit de compenser l'offset naturel du capteur.

ambiant_noise_level = 0

print("Démarrage du calibrage")
print("Ne faites pas de bruit !")
sleep_ms(10000)

for x in range(1500):

	# Délai de 50 millisecondes pour laisser au capteur le temps de se stabiliser
	sleep_ms(50)
	
	# Lecture du bruit ambiant
	noise = adc.read()
	
	# Si la valeur du bruit est la plus grande détectée jusqu'ici
	# mémorise là
	if noise > ambiant_noise_level:
		ambiant_noise_level  = noise

# Auhmente de 50 le seuil de son ambiant
ambiant_noise_level += 50
		
print("Fin du calibrage")
print("Bruit ambiant : %d" %ambiant_noise_level)
sleep_ms(5000)

print("Micro à l'écoute ...")

while True : # Boucle sans clause de sortie

	sleep_ms(50)
	
	noise = adc.read()
	
	# Si le bruit détecté est plus fort que le seuil de bruit ambiant
	# calculé pendant le calibrage ...
	if noise > ambiant_noise_level:
		# Affiche une alerte
		print("Bruit atypique !")
		print("Valeur de l'ADC (prop. volume sonore) : %d" %noise)
		sleep_ms(1000)
```

## Résultat

Lancez le script avec *[CTRL]-[D]* dans le terminal PuTTY. Restez silencieux pendant la phase de calibrage (environs deux minutes). Par la suite, un message s'affichera chaque fois que le module détectera un son plus fort que sa valeur de référence :

```console
MicroPython v1.19.1 on 2022-06-18; NUCLEO-WB55 with STM32WB55RGV6
Type "help()" for more information.
>>>
MPY: sync filesystems
MPY: soft reboot
Démarrage du calibrage
Ne faites pas de bruit !
Fin du calibrage
Bruit ambiant : 892
Micro à l'écoute ...
Bruit atypique !
Valeur de l'ADC (prop. volume sonore) : 1035
Bruit atypique !
Valeur de l'ADC (prop. volume sonore) : 1681
Bruit atypique !
Valeur de l'ADC (prop. volume sonore) : 4095
Bruit atypique !
Valeur de l'ADC (prop. volume sonore) : 4095
Bruit atypique !
Valeur de l'ADC (prop. volume sonore) : 1117
Bruit atypique !
Valeur de l'ADC (prop. volume sonore) : 2615
Bruit atypique !
Valeur de l'ADC (prop. volume sonore) : 1383
Bruit atypique !
Valeur de l'ADC (prop. volume sonore) : 923
```

## Pour aller plus loin : Mesure de l'intensité d'un son en décibels

 Le micro qui équipe le module ne coûte pratiquement rien mais n'est pas d'excellente qualité ; il est incapable d'entendre une plage de fréquences aussi étendue que celle accessible à l'oreille humaine et surtout il ne sait pas convertir les intensités sonores en décibels. Si vous souhaitez obtenir une mesure physique fiable de l'intensité des sons ambiants (i.e. réaliser un décibelmètre), nous vous renvoyons vers [ce tutoriel](sound_level_meter).

 