---
title: Capteur de gestes PAJ7620U2
description: Mise en œuvre d'un module Grove capteur de geste PAJ7620U2 avec MicroPython
---

# Capteur de gestes PAJ7620U2

Ce tutoriel montre la mise en œuvre d'un [**module Grove capteur de gestes**](https://wiki.seeedstudio.com/Grove-Gesture_v1.0/) avec MicroPython. Vous trouverez [ici](https://files.seeedstudio.com/wiki/Grove_Gesture_V_1.0/res/PAJ7620U2_Datasheet_V0.8_20140611.pdf) la fiche technique du [capteur PAJ7620U2](https://www.pixart.com/products-detail/37/PAJ7620U2) de PixArt Imaging Inc qui équipe ce module.

**Le module Grove capteur de gestes PAJ7620U2 :**
<div align="left">
<img alt="Grove -  capteur de gestes PAJ7620U2" src="images/paj7620u2.png" width="200px">
</div>

Ce capteur est capable de reconnaître jusqu'à 9 gestes et peut aussi être utilisé comme un détecteur de proximité. Les gestes possibles, qui doivent être réalisés à quelques centimètres du capteur, sont : 
 
 1. Balayage du bas vers le haut
 2. Balayage du haut vers le bas
 3. Balayage de gauche à droite
 4. Balayage de droite à gauche
 5. Mouvement circulaire de sens horaire autour du capteur
 6. Mouvement circulaire de sens anti-horaire autour du capteur
 7. On s'approche du capteur
 8. On s'éloigne du capteur
 9. Mouvement "de vague" (agitez la main de gauche à droite devant le capteur comme pour lui dire *"au-revoir"*)

 Vous trouverez une vidéo illustrant ces mouvements [ici](https://youtu.be/e3nf-b4W6TY).

Nous allons dans un premier temps présenter un script mettant en œuvre le capteur de gestes seul, puis, dans un deuxième temps, l'utiliser pour piloter un [module Grove anneau de LED RGB](https://wiki.seeedstudio.com/Grove-LED_ring/).

## Pour commencer : Test du module capteur de gestes

> **Vous pouvez télécharger les scripts MicroPython de ce tutoriel (entre autres) en cliquant [ici](../../../assets/Script/MODULES.zip)**.

Ce premier tutoriel montre comment utiliser le capteur de gestes pour envoyer des informations sur le port série de l'USB User. Le capteur communique simplement quel geste il vient de reconnaître. 
 
### Le matériel

1. Une [carte d'extension de base Grove](https://wiki.seeedstudio.com/Base_Shield_V2/)
2. La carte NUCLEO-WB55
3. Un [module Grove capteur de gestes PAJ7620U2](https://wiki.seeedstudio.com/Grove-Gesture_v1.0/)

Connectez le module capteur de gestes sur une fiche I2C de la carte d'extension de base Grove.

### Le script MicroPython

```python
# Objet du script :
# Test d'un module Grove capteur de gestes
# Code adapté de :
# https://github.com/itechnofrance/Micropython/blob/master/librairies/paj7620/use_paj7620.py

import paj7620 # Pilotes pour l'anneau de LED RGB et le capteur de gestes
from time import sleep_ms # Pour temporiser et mesurer le temps écoulé
from machine import I2C # Pour gérer le bus I2C

# On utilise l'I2C n°1 de la carte NUCLEO-WB55 pour communiquer avec le capteur de gestes
i2c = I2C(1) 

# Pause d'une seconde pour laisser à l'I2C le temps de s'initialiser
sleep_ms(1000)

# Liste des adresses I2C des périphériques présents
print("Adresses I2C utilisées : " + str(i2c.scan()))

# Adresse du capteur sur le bus I2C : 0x73 
PAJ7620U2_ADR = const(0x73)

# Instanciation du capteur de gestes
g = paj7620.PAJ7620(i2c = i2c)

# Liste des gestes
gestures = [ "Aucun geste", "Avant", "Arrière", "Droite", "Gauche", "Haut", "Bas", "Sens horaire", "Sens anti-horaire", "Vague"]

while True:

	# Lecture du geste
	index = g.gesture()

	# Extrait le geste de la liste
	if index > 0 and index < 10:
		print(gestures[index])
	
	# Temporisation de 10 millisecondes
	sleep_ms(10)
```

### Usage

Lancez le script avec *[CTRL]-[D]*. Agitez simplement la main devant le capteur et observez la confirmation (ou pas) de votre geste sur le terminal série de PuTTY :

```console
MPY: sync filesystems
MPY: soft reboot
Adresses I2C utilisées : [115]
Avant
Haut
Droite
Droite
Gauche
Droite
Gauche
Droite
Sens anti-horaire
Sens horaire
```

## Pour aller plus loin : Utilisation du module capteur de gestes pour piloter un anneau de LED RGB

> **Vous pouvez télécharger les scripts MicroPython de ce tutoriel (entre autres) en cliquant [ici](../../../assets/Script/MODULES.zip)**.

Ce deuxième exemple montre comment modifier l'animation d'un module Grove anneau de LED RGB (adressables, de type Neopixel) à l'aide du module Grove capteur de gestes. Nous souhaitons que l'animation sur l'anneau reste fluide, qu'elle ne soit pas perturbée par la scrutation du capteur de gestes.Cette contrainte nous impose naturellement l'utilisation **des fonctions asynchrones** de la bibliothèque  [`uasyncio`](https://docs.MicroPython.org/en/latest/library/uasyncio.html) de MicroPython. Cet exemple peut être avantageusement transposé à de nombreuses applications qui nécessitent une acquisition de données et leur affichage simultané sans dégrader l'expérience utilisateur.

Le sujet de la programmation asynchrone est vaste, et les possibilités offertes par MicroPython sur ce sujet sont nombreuses et raisonnablement faciles à mettre en œuvre. Vous trouverez un tutoriel à ce propos [ici](https://GitHub.com/peterhinch/MicroPython-async/blob/master/v3/docs/TUTORIAL.md). 

### Le matériel

1. Une [carte d'extension de base Grove](https://wiki.seeedstudio.com/Base_Shield_V2/)
2. La carte NUCLEO-WB55
3. Un [module Grove capteur de gestes PAJ7620U2](https://wiki.seeedstudio.com/Grove-Gesture_v1.0/)
4. Un [module Grove anneau de LED RGB (24 - WS2813 Mini)](https://wiki.seeedstudio.com/Grove-LED_ring/)

Connectez le module capteur de gestes sur l'une des prises I2C de la carte d'extension de base Grove.
Connectez le module anneau de LED RGB (24 - WS2813 Mini) sur la prise *D2* de la carte d'extension de base Grove.

### Le script MicroPython

```python
# Objet du script :
# Pilotage d'un module Grove anneau de LED RGB avec un module Grove capteur de gestes

import neopixel, paj7620 # Pilotes pour l'anneau de LED RGB et le capteur de gestes
from time import sleep_ms # Pour temporiser et mesurer le temps écoulé
from machine import Pin, I2C # Pour gérer les broches et le bus I2C
import uasyncio # Pour la gestion asynchrone 

# On initialise l'anneau de LED sur la broche D2
_NB_LED = const(24) # 24 LED sur l'anneau
ring = neopixel.NeoPixel(Pin('D2'), _NB_LED)

# On utilise l'I2C n°1 de la carte NUCLEO-WB55 pour communiquer avec le capteur de gestes
i2c = I2C(1) 

# Pause d'une seconde pour laisser à l'I2C le temps de s'initialiser
sleep_ms(1000)

# Liste des adresses I2C des périphériques présents
print("Adresses I2C utilisées : " + str(i2c.scan()))

# Adresse du capteur sur le bus I2C : 0x73 
PAJ7620U2_ADR = const(0x73)

# Instanciation du capteur de gestes
g = paj7620.PAJ7620(i2c = i2c)

ROTATION = 1 # Sens de rotation (horaire au démarrage)
_NB_LED_M1 = const(_NB_LED - 1)

# Coroutine asynchrone de gestion de l'anneau de LED RGB
@micropython.native # Demande au compilateur bytecode de produire un code pour le STM32WB55
async def wheel():

	global ROTATION

	led = 0

	while True:
		
		# Index de la LED acourante
		if ROTATION: # Rotation horaire
			led += 1
			if led > _NB_LED_M1:
				led = 0
		else: # Rotation anti-horaire
			led -= 1
			if led < 0:
				led = _NB_LED_M1
		
		# Eteint toutes les LED
		for i in range(_NB_LED):
			ring[i] = (0, 0, 0)

		# Allume la LED courante
		ring[led] = (128, 128, 128)
		ring.write()
		
		# Temporisation non blocante
		await uasyncio.sleep_ms(10)

# Coroutine asynchrone de gestion du capteur de gestes
@micropython.native # Demande au compilateur bytecode de produire un code pour le STM32WB55
async def control():
	
	global ROTATION

	while True:

		gestures = g.gesture()

		if gestures == 7:
			print("Sens horaire")
			ROTATION = 1
		elif gestures == 8:
			print("Sens anti-horaire")
			ROTATION = 0

		# Temporisation non blocante
		await uasyncio.sleep_ms(100)

# Coroutine asynchrone pour lancer les deux autres coroutines
@micropython.native 
async def main():
	# Crée une tache par coroutine
	task1 = uasyncio.create_task(wheel())   # Tache pour la coroutine de gestion de l'anneau
	task2 = uasyncio.create_task(control()) # Tache pour la coroutine de gestion du capteur
	await task1, task2 # Reste en pause aussi longtemps que les deux taches ne sont pas terminées

# Démarre le planificateur (et donc, les deux taches)
uasyncio.run(main())
```

### Usage

Lancez le script avec *[CTRL]-[D]*. Réalisez le geste "sens-horaire" (respectivement "sens anti-horaire") pour animer l'anneau dans le sens-horaire (respectivement sens anti-horaire). L'affichage sur le terminal confirme votre geste :

```console
MPY: sync filesystems
MPY: soft reboot
Adresses I2C utilisées : [115]
Sens anti-horaire
Sens horaire
Sens horaire
Sens horaire
Sens anti-horaire
Sens anti-horaire
Sens anti-horaire
Sens horaire
Sens anti-horaire
```