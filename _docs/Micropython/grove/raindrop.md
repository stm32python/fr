---
title: Détection de pluie
description: Détection d'averse avec un capteur de gouttes de pluie
---

# Capteur de gouttes de pluie

Ce tutoriel explique comment mettre en œuvre un capteur de gouttes de pluie.

## Description

Un capteur de gouttes de pluie est une carte disposant de pistes conductrices en nickel. Son principe de fonctionnement est facile à comprendre : lorsque des gouttes d'eau (conductrices du fait des faibles concentrations de sels minéraux qu'elles contiennent) tombent sur la face métallisée du capteur elles font chuter sa résistance en créant des courts-circuits entre ses pistes.
Cette variation de résistance entraine une variation de tension qui est ensuite mesurée par un [amplificateur opérationnel](https://fr.wikipedia.org/wiki/Amplificateur_op%C3%A9rationnel).<br>
Ce capteur est identique [au module Grove détecteur d'eau](https://wiki.seeedstudio.com/Grove-Water_Sensor/), abordé dans [cette fiche](water_sensor).

Ce module peut fonctionner de deux façons :
 - Par mesure analogique avec le taux d'humidité
 - Par mesure numérique avec un seuil de dépassement d'humidité


## Matériel requis

1. Une [carte d'extension de base Grove](https://wiki.seeedstudio.com/Base_Shield_V2/)
2. [La carte NUCLEO-WB55](../../Kit/nucleo_wb55rg)
3. Un [module capteur de gouttes de pluie](https://eu.robotshop.com/fr/products/raindrop-sensor-module)

**Le capteur de gouttes de pluie :**

<div align="left">
<img alt="Raindrops sensor" src="images/raindrop.jpg" width="300px">
</div>

Le capteur est connecté sur le connecteur A0 du shield Grove.


<h2>Le code MicroPython</h2>

> **Vous pouvez télécharger les scripts MicroPython de ce tutoriel (entre autres) en cliquant [ici](../../../assets/Script/MODULES.zip)**.

```python
# Attention : le capteur doit être alimenté en 5V pour donner une réponse entre 0 et 4095.

from pyb import ADC, Pin # Convertisseur analogique-numérique et GPIO
from time import sleep # Pour les temporisations

# Instanciation et démarrage du convertisseur analogique-numérique
adc = ADC(Pin('A0'))

while True:

	# Numérise la valeur lue, produit un résultat variable dans le temps dans l'intervalle [0 ; 4095]
	measure = adc.read()

	# Si une goutte tombe sur le capteur alors on averti l'utilisateur. 
	# Pour changer le niveau de detection il faut changer la valeur de la condition if
	if(measure < 3500):
		print("Alerte : détection de pluie")

	# Temporisation d'une seconde
	sleep(1) 
```

<h2>Résultat</h2>

Et voilà ! Nous pouvons à présent observer un signal dans un terminal série chaque fois qu'une goutte d'eau (ou de toute autre matière conductrice) tombe sur le module et met deux de ses pistes en court-circuit :

```console
MPY: sync filesystems
MPY: soft reboot
Alerte : détection de pluie
Alerte : détection de pluie
Alerte : détection de pluie
Alerte : détection de pluie
```
