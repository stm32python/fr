---
title: Contrôle moteur avec un pont en H
description: Mise en œuvre d'un moteur avec un pont en H
---

# Contrôle moteur avec un pont en H

Ce tutoriel explique comment faire tourner un moteur dans deux sens de rotation avec un driver moteur et MicroPython.

<h2>Description</h2>

Un moteur, aussi simple soit-il, ne peut tourner que dans un seul sens s'il est connecté directement à la carte NUCLEO-WB55. A travers ce tutoriel nous allons voir comment faire tourner un moteur dans les deux sens.<br>
Pour ce faire nous allons utiliser *un driver moteur* ; plus précisément *un pont en H*. C'est une structure constituée de 4 interrupteurs (transistors) couplés 2 par 2 permettant de faire circuler sur commande le courant dans un sens ou dans l'autre. En inversant le sens du courant on change tout simplement le sens de rotation du moteur. Si on décompose la structure du pont en H on obtient la figure suivante :

<br>
<div align="left">
<img alt="Decomposition du pont en H" src="images/moteur-pontenH-1.png" width="20%">
</div>
<br>

La charge représente le moteur. Si on reprend l'explication, on obtient alors le sens de courant suivant dans les deux cas :

<br>
<div align="left">
<img alt="Décomposition du fonctionnement du pont en H" src="images/moteur-pontenH-2.png" width="60%">
</div>
<br>

> Crédit images : [Wikimedia](https://upload.wikimedia.org/wikipedia/commons/c/c7/Pont_En_H.png)

Pour le montage nous utiliserons un driver moteur de référence **L293D** ou de référence **L298N** de chez STMicroelectronics. Ces drivers sont capables de contrôler deux moteurs en même temps mais, dans notre cas, nous n'utiliserons qu'un seul moteur. 
Ces deux composants sont très similaires dans leur fonctionnement, seules quelques caractéristiques techniques les différencient. La documentation technique du **L293D** fournit le tableau de correspondance suivant : 

<br>
<div align="left">
<img alt="Figure datasheet du L293D" src="images/moteur-L293D.png" width="20%">
</div>
<br>

| Patte driver moteur | Description                                        |
| :---------------:   | :------------------------------------------------: |
|      Enable 1       | Met en arrêt (GND) ou en marche (VSS) le moteur    |
|      Input 1        | Contrôle le sens du courant via la carte NUCLEO    |
|      Output 1       | Connecté à une patte du moteur                     |
|      GND            | Connecté à la masse (une seule suffit)             |
|      GND            | Commune au composant                               |
|      Output 2       | Connecté à l'autre patte du moteur                 |
|      Input 2        | Contrôle le sens du courant via la carte NUCLEO    |
|      VS             | Alimentation de la commande puissance (moteur)     |
|      VSS            | Alimentation de la commande logique (carte NUCLEO) |


Ce driver trouve utilité par exemple pour faire avancer, reculer ou tourner un petit robot sur roues.

Dans notre cas nous souhaitons utiliser les trois boutons et les trois LED de la carte NUCLEO :
 - un bouton pour faire tourner le moteur dans le sens horaire (LED en bleu).
 - un bouton pour faire tourner le moteur dans le sens anti-horaire (LED en vert).
 - un bouton pour arrêter le moteur (LED en rouge).

<h2>Montage</h2>

En fonction du composant choisi, on utilise les schémas de câblage suivants. A noter que le principe reste le même, peu importe le driver moteur sélectionné.

**Pour le L293D :** nous câblons le montage de cette façon : 

<br>
<div align="left">
<img alt="Schéma de montage moteur avec L293D" src="images/moteur-schema1.png" width="70%">
</div>
<br>

**Pour le L298N :** nous câblons le montage de cette façon :

<br>
<div align="left">
<img alt="Schéma de montage moteur avec L298N" src="images/moteur-schema2.png" width="70%">
</div>
<br>

Quel que soit le driver moteur utilisé, la correspondance entre les composants sera la suivante :

| Driver moteur     | NUCLEO-WB55       | Moteur            | Alim externe      |
| :-------------:   | :---------------: | :---------------: | :---------------: |
|     Enable 1      |         D3        |                   |                   |
|     Input 1       |         D4        |                   |                   |
|     Output 1      |                   |        Oui        |                   |
|     GND           |         GND       |                   |        GND        |
|     GND           |                   |                   |                   |
|     Output 2      |                   |        Oui        |                   |
|     Input 2       |         D5        |                   |                   |
|     VS            |                   |                   |        5V         |
|     VSS           |         3.3V      |                   |                   |

Pour cet exercice nous utilisons une source d'alimentation externe pour le moteur (telle que des piles ou une batterie de 5V minimum). En effet la carte NUCLEO-WB55 n'est pas faite pour contrôler des éléments de puissance tel qu'un moteur, qui nécessite un courant plus important que celle qu'elle est capable de fournir, surtout à son démarrage.

**Remarque 1 :** Pensez bien à connecter les masses (GND) ensemble sous peine de dysfonctionnements.

**Remarque 2 :** La tension d'alimentation de puissance **Vs** peut être différente. Tout dépend de votre source d'alimentation et des caractéristiques du moteur que vous utilisez.


<h2>Le code MicroPython</h2>

> **Vous pouvez télécharger les scripts MicroPython de ce tutoriel (entre autres) en cliquant [ici](../../../assets/Script/MODULES.zip)**.

Editez maintenant le script *main.py* :

**Etape 1 :** On importe les bibliothèques dans notre code.

```python
from pyb import Pin, Timer
import time
import gc # Ramasse miettes, pour éviter de saturer la mémoire
```

**Etape 2 :** On définit des variables globales qui nous serviront à récupérer l'état des boutons poussoirs.

```python
#Variables globales
BP1 = 0
BP2 = 0
BP3 = 1
```

**Etape 3 :** On fait l'initialisation des composants. On utilise une PWM sur la patte D3 (patte servant pour l'**enable 1**) afin de pouvoir contrôler la vitesse du moteur.

```python
# BP (en entrée + pull up)
sw1 = pyb.Pin('SW1')
sw1.init(pyb.Pin.IN, pyb.Pin.PULL_UP, af=-1)
sw2 = pyb.Pin('SW2')
sw2.init(pyb.Pin.IN, pyb.Pin.PULL_UP, af=-1)
sw3 = pyb.Pin('SW3')
sw3.init(pyb.Pin.IN, pyb.Pin.PULL_UP, af=-1)

# LED (LED de la carte NUCLEO)
red_led = pyb.LED(1)
blue_led = pyb.LED(3)
green_led = pyb.LED(2)

# Driver moteur ("enable" + "input 1" et "input 2" en sorties)
motor_enable = pyb.Pin('D3')
motor_pin1 = pyb.Pin('D4', Pin.OUT_PP)
motor_pin2 = pyb.Pin('D5', Pin.OUT_PP)

# PWM pour le moteur (Timer 1 channel 3, fréquence = 1kHz et sur "enable")
tim1 = Timer(1, freq=1000)
ch3 = tim1.channel(3, Timer.PWM, pin=motor_enable)
```

**Etape 4 :** Afin de contrôler le moteur à n'importe quel instant on gère les boutons poussoirs avec des interruptions qui mettront à jour les variables globales définies précédemment.

```python
# Interruption de SW1 (le moteur tourne dans le sens "avant")
def ITbutton1(line):

	# Variables globales
	global BP1
	global BP2
	global BP3
	
	# Incrémente BP1, le reste à 0
	BP1 = BP1 + 1
	BP2 = 0
	BP3 = 0

# Interruption de SW2 (le moteur tourne dans le sens "arrière")
def ITbutton2(line):

	# Variable globales
	global BP1
	global BP2
	global BP3
	
	# Incrémente BP2, le reste à 0
	BP1 = 0
	BP2 = BP2 + 1
	BP3 = 0

# Interruption de SW3 (arrête le moteur)
def ITbutton3(line):

	# Variables globales
	global BP1
	global BP2
	global BP3
	
	# Incrémente BP3, le reste à 0
	BP1 = 0
	BP2 = 0
	BP3 = BP3 + 1

# Initialisation des vecteurs d'interruption
irq_1 = pyb.ExtInt(sw1, pyb.ExtInt.IRQ_FALLING, pyb.Pin.PULL_UP, ITbutton1)
irq_2 = pyb.ExtInt(sw2, pyb.ExtInt.IRQ_FALLING, pyb.Pin.PULL_UP, ITbutton2)
irq_3 = pyb.ExtInt(sw3, pyb.ExtInt.IRQ_FALLING, pyb.Pin.PULL_UP, ITbutton3)
```

**Etape 5 :** On crée une fonction pour gérer le moteur. Celle-ci reçoit en paramètre sa vitesse. Dans cette fonction on teste l'état de boutons et, en fonction du résultat, on active/désactive les LED, les pattes **input 1** et **input 2** et le signal PWM.

```python
# Fonction qui gère le moteur
def motor_state(speed):

	# Variables globales
	global BP1
	global BP2
	global BP3

	# Si BP1 appuyé
	if((BP1 != 0) and (BP2 == 0) and (BP3 == 0)):
		# Allume la LED bleu
		blue_led.on()
		# Allume l'enable 1 / Eteint l'enable 2
		motor_pin1.high()
		motor_pin2.low()
		# PWM pour régler la tension du moteur
		ch3.pulse_width_percent(speed)
	 	
	# Si BP2 appuyé
	if((BP1 == 0) and (BP2 != 0) and (BP3 == 0)):
		# Allume la LED verte
		green_led.on()
		# Allume l'enable 2 / Eteint l'enable 1
		motor_pin1.low()
		motor_pin2.high()
		# PWM pour régler la tension du moteur
		ch3.pulse_width_percent(speed)
		
	# Si BP3 appuyé
	if((BP1 == 0) and (BP2 == 0) and (BP3 != 0)):
		# Allume la LED rouge
		red_led.on()
		# Eteint l'enable 1 et 2
		motor_pin1.low()
		motor_pin2.low()
		# PWM pour régler la tension du moteur
		ch3.pulse_width_percent(0)
```

**Etape 6 :** Finalement on définit la variable vitesse par un rapport cyclique de 100%.

```python
# Variable pour gérer la vitesse de rotation du moteur
speed = 100

#Boucle infinie
while True:

	# Eteint toutes les LED par défaut
	blue_led.off()
	green_led.off()
	red_led.off()
	
	# Passe en parametre la vitesse souhaitée
	motor_state(speed)
	
	# Appel du ramasse-miettes
	gc.collect()
```


<h2>Résultat</h2>

Il ne vous reste plus qu'à enregistrer le code et redémarrer la carte NUCLEO-WB55.
La LED rouge de la carte s'allume indiquant que le moteur est à l'arrêt. Appuyez sur un des boutons poussoirs **SW1** ou **SW2** pour faire tourner le moteur. 
Vous pouvez également vous amuser à changer la valeur (entre 0 et 100) de la variable **vitesse** et observer le comportement du moteur.

Pour d'autres exemples d'utilisation du moteur avec son driver vous pouvez vous reporter au tutoriel du [Nunchuk](nunchuk) dans la partie "pour aller plus loin".

**Remarque :** il arrive parfois que le moteur bloque au démarrage et émette un léger sifflement. Cela se produit lorsqu'il est bloqué. Le cas échéant, vous devrez le faire tourner légèrement à la main (et dans le bon sens) pour le lancer.
