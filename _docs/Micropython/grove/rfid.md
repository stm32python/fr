---
title: Module RFID RC522 13.56 MHz
description: Mise en œuvre de la lecture et de l'écriture de badges RFID en MicroPython avec un module RC522 13.56 MHz
---

# Module RFID RC522

Ce tutoriel explique comment mettre en œuvre la lecture et de l'écriture de badges RFID en MicroPython avec un module RC522 13.56 MHz

<h2>Description</h2>

La radio-identification, le plus souvent désignée par le sigle *RFID*, est une technologie qui code des données numériques dans des étiquettes électroniques (opportunément désignées par *Tags RFID*) et qui permet également de les lire. Le tout se fait sans contact entre le transmetteur et l'émetteur.
Ces badges sont constitués d'une puce électronique et d'une antenne qui lui permet de répondre à une certaine fréquence radio. Pour venir lire ces données il faut donc disposer d'un lecteur exploitant la même fréquence.

Il existe donc différents types de badge RFID, déclinés selon la fréquence d'utilisation. On retrouve ainsi les badges :
- basse fréquence (ou RFID) avec une fréquence de 30 à 300kHz.
- haute fréquence (ou HF) avec une fréquence de 3 à 30Mhz et dont fait partie le NFC. Par exemple votre téléphone utilise le NFC pour les paiements mobiles, partager des données et lire ou écrire des badges RFID.
- très haute fréquence (ou UHF) avec une fréquence de 300MHz à 3GHz.

Les badges RFID sont utilisés dans différents type d'applications telles que :
- les passeports, cartes de transport, cartes de fidélité, badges d'appartement, cartes bancaires et tout type de carte permettant d'identifier une personne.
- la gestion d'inventaires.
- le contrôle de l'accès à des zones sécurisées.
- l'identification d'animaux. Le badge RFID est alors implanté sous sa peau et permet de retrouver des informations sur le propriétaire ou encore l'antécédant médical de l'animal.

De façon générale les badges RFID contiennent un identifiant censé être unique (UID pour Unique IDentifier en anglais).

Dans un premier temps nous verrons comment retrouver le type de badge, son identifiant ainsi que les données stockées à l'intérieur.
Dans un second temps nous verrons comment écrire des données dans le badge.
Enfin dans la partie *pour aller plus loin*, nous mettrons en place un mini-projet de simulation d'une porte sécurisée à l'aide d'un badge RFID.

<h2>Montage</h2>

Pour le montage nous utilisons un lecteur de badge RFID de référence *RFID-RC522* qui fonctionne à une fréquence de 13,56MHz.

<br>
<div align="left">
<img alt="Module MFRC-522 et badge RFID" src="images/RFID-RC522.jpg" width="400px">
</div>
<br>

> Crédit images : [Conrad](https://www.conrad.fr/p/puce-rfid-joy-it-rfid-modul-mfrc-522-sbc-rfid-rc522-1-pcs-1503746)


Ce lecteur dispose d'une portée de lecture d'environ 3 cm et communique en protocole [SPI](../../Kit/glossaire). On connecte le lecteur de badge en suivant ce schéma :

<br>
<div align="left">
<img alt="Schéma de montage lecteur RFID" src="images/rfid-schema1.png" width="800px">
</div>
<br>

La correspondance entre le lecteur de badge et la carte NUCLEO-WB55 est la suivante :

| Lecteur RFID      | ST Nucleo         |
| :-------------:   | :---------------: |
|       SDA         |         D10       |
|       SCK         |         D13       |
|       MOSI        |         D11       |
|       MISO        |         D12       |
|       GND         |         GND       |
|       RST         |         D9        |
|       3.3V        |         3.3V      |

Pour cet exercice il est intéressant d'utiliser au moins 2 badges RFID.

<h2>Le code MicroPython pour la lecture</h2>

> **Vous pouvez télécharger les scripts MicroPython de ce tutoriel (entre autres) en cliquant [ici](../../../assets/Script/MODULES.zip)**.

Ce programme a pour but de lire les données contenues dans un badge. Nous allons afficher le type de badge, son UID ainsi que les valeurs stockées dans sa mémoire.


**Etape 1 :** On importe les bibliothèques dans notre code.

```python
import pyb
import time
import mfrc522
```

**Etape 2 :** On définit les ports utilisés pour établir la connexion avec le lecteur de badge RFID. Enfin on affiche un message pour inviter l'utilisateur à présenter un badge sur le lecteur.

```python
# Initialisation du lecteur de badge
rdr = mfrc522.MFRC522('D13', 'D11', 'D12', 'D9', 'D10') #SCK, MOSI, MISO, RST, SDA

print("\n--> Veuillez présenter un badge sur le lecteur")
```

**Etape 3 :** Dans un premier temps nous allons rechercher la présence d'un badge. Si un badge est effectivement détecté alors on vient récupérer son type et son UID pour les afficher. Dans un second temps nous allons nous authentifier auprès du badge à l'aide du tableau *key*. Une fois l'authentification réussie on lit les données stockées sur le badge pour ensuite les afficher dans le moniteur série. Enfin, si un problème survient on n'oublie pas d'en informer l'utilisateur dans le moniteur série.

```python
while True:
	# Détecte la présence d'un badge
	(stat, tag_type) = rdr.request(rdr.REQIDL)				
	if stat == rdr.OK:
		(stat, raw_uid) = rdr.anticoll()
		if stat == rdr.OK:
			# Affichage du type de badge et de l'UID
			print("\nBadge détecté !")
			print(" - type : %03d" % tag_type)
			print(" - uid : %03d.%03d.%03d.%03d" % (raw_uid[0], raw_uid[1], raw_uid[2], raw_uid[3]))
			# Affichage des données en mémoire
			if rdr.select_tag(raw_uid) == rdr.OK:
				key = [0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF]
				if rdr.auth(rdr.AUTHENT1A, 8, key, raw_uid) == rdr.OK:
					print(" - données : %s" % rdr.read(8))
					rdr.stop_crypto1()
				# Affichage en cas de problème
				else:
					print("Erreur de lecture")
			# Affichage en cas de problème
			else:
				print("Erreur badge")
```


<h2>Le code MicroPython pour l'écriture</h2>

> **Vous pouvez télécharger les scripts MicroPython de ce tutoriel (entre autres) en cliquant [ici](../../../assets/Script/MODULES.zip)**.

Ce programme a pour but d'écrire des données dans le badge. Nous allons ici écrire les valeurs de 0 à 15 dans les 16 emplacements mémoires qu'il met à disposition.

**Etape 1 :** On importe les bibliothèques dans notre code.

```python
import pyb
import time
import mfrc522
```

**Etape 2 :** On définit les ports utilisés pour établir la connexion avec le lecteur de badge RFID. Enfin on affiche un message pour inviter l'utilisateur à présenter un badge sur le lecteur.

```python
# Initialisation du lecteur de badge
rdr = mfrc522.MFRC522('D13', 'D11', 'D12', 'D9', 'D10') #SCK, MOSI, MISO, RST, SDA

print("\n--> Veuillez présenter un badge sur le lecteur")
```

**Etape 3 :** Dans cette étape on reprend la même base de boucle infinie que la partie *programme lecture*. Dans un premier temps détecte la présence d'un badge. En cas de succès, on s'authentifie auprès du badge à l'aide du tableau *key*. Une fois l'authentification réussie on y écrit les valeurs de 0 à 15 (soit 0x0 à 0xF en hexa). On affiche alors un message pour connaitre l'état de l'écriture. Enfin on affiche les nouvelles valeurs contenues en mémoire.

```python
while True:
	# Détecte la présence d'un badge
	(stat, tag_type) = rdr.request(rdr.REQIDL)				
	if stat == rdr.OK:
		(stat, raw_uid) = rdr.anticoll()
		if stat == rdr.OK:
			print("\nBadge détecté !")
			# Authentification
			if rdr.select_tag(raw_uid) == rdr.OK:
				key = [0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF]
				# Ecriture
				if rdr.auth(rdr.AUTHENT1A, 8, key, raw_uid) == rdr.OK:
					print("--> Ecriture en cours...")
					stat = rdr.write(8, b"\x00\x01\x02\x03\x04\x05\x06\x07\x08\x09\x0a\x0b\x0c\x0d\x0e\x0f") # Valeur hexa
					if stat == rdr.OK:
						print("--> Ecriture terminée !")
					else:
						print("--> Impossible d'écrire sur la carte !")
				# Affichage
				print("Données en mémoire : %s" % rdr.read(8))
				# Arrêt
				rdr.stop_crypto1()
		else:
			print("Erreur badge")
	time.sleep_ms(500)
```

<h2>Résultat</h2>

Il ne vous reste plus qu'à sauvegarder l'ensemble puis à redémarrer votre carte NUCLEO-WB55.
Pour la partie *programme lecture* présentez un badge sur le lecteur. Vous devriez alors obtenir un résultat similaire à celui-ci :

<div align="left">
<img alt="Affichage des données de badge RFID" src="images/rfid0-python.png" width="60%">
</div>

Pour la partie *programme écriture* présentez un badge sur le lecteur. Vous devriez alors obtenir un résultat similaire à celui-ci :

<div align="left">
<img alt="Affichage des données de badge RFID" src="images/rfid1-python.png" width="60%">
</div>

On peut observer que les données contenues en mémoire ont bien été remplacées.

**Remarque 1 :** Si vous ne disposez pas de badge RFID vous pouvez utiliser votre téléphone (seulement pour la partie *programme lecture*). Il suffit d'avoir un téléphone disposant du NFC et d'activer l'option. A partir de ce moment vous pouvez au moins lire le type de badge et son UID.

**Remarque 2:** Parfois vous n'arriverez pas à lire les données (ex : avec les Amiibo de chez Nintendo). C'est normal, certains badges sont sécurisés afin d'éviter que leurs données puissent être facilement copiées sur un autre support.


<h1>Pour aller plus loin</h1>

Dans cette partie nous allons assembler plusieurs codes abordés dans les parties précédentes afin de réaliser une porte sécurisée par un badge RFID. Pour cela nous allons utiliser :

 - un lecteur RFID pour récupérer l'information.
 - les 3 LED de la carte NUCLEO-WB55 pour faire un retour d'état à l'utilisateur.
 - un servomoteur permettant de faire pivoter la porte.

Voici le principe de fonctionnement de ce mini projet :

1. Dans un premier temps la LED bleue de la carte NUCLEO-WB55 s'allume indiquant à l'utilisateur que le système est prêt à enregistrer une carte. L'utilisateur présente le badge RFID sur le lecteur de badge. L'enregistrement se termine quand la LED bleue s'éteint.
2. Dans un second temps, si l'utilisateur présente la carte qu'il a enregistrée alors la LED verte s'allume et le système actionne le servomoteur pour le faire pivoter à 90°. Au bout de 3 secondes le servomoteur revient à sa position initiale et la LED verte s'éteint.
3. Si l'utilisateur présente un badge non enregistré dans le système alors la LED rouge s'allume puis s'éteint au bout d'une seconde.
4. Enfin nous voulons voir comment se comporte le système et voir les informations qu'il traite. Par conséquent on  affiche ces informations dans le moniteur série.


<h2>Montage</h2>

On reprend le même montage que précédemment auquel nous venons ajouter en plus un servomoteur. Nous réutilisons les LED de la carte NUCLEO-WB55 donc nous n'avons pas besoin de venir les câbler.

<div align="left">
<img alt="Schéma de montage lecteur RFID" src="images/rfid-schema2.png" width="800px">
</div>

La correspondance entre le servomoteur et la carte NUCLEO-WB55 est la suivante :

| Servomoteur       | ST Nucleo         |
| :-------------:   | :---------------: |
|       Signal      |         D6        |
|       GND         |         GND       |
|       5V          |         5V        |

Et pour rappel :

| Lecteur RFID      | ST Nucleo         |
| :-------------:   | :---------------: |
|       SDA         |         D10       |
|       SCK         |         D13       |
|       MOSI        |         D11       |
|       MISO        |         D12       |
|       GND         |         GND       |
|       RST         |         D9        |
|       3.3V        |         3.3V      |

<h2>Le code MicroPython</h2>

> **Vous pouvez télécharger les scripts MicroPython de ce tutoriel (entre autres) en cliquant [ici](../../../assets/Script/MODULES.zip)**.

**Etape 1 :** On importe les bibliothèques dans notre code.

```python
import pyb
import time
import mfrc522
```

**Etape 2 :** On définit les ports utilisés pour établir la connexion avec le lecteur de badge RFID. On procède également à l'initialisation des LED ainsi qu'a celle du servomoteur. Enfin on déclare 2 variables (globales) :
 - compteur = variable de type entier permettant de savoir si un badge a été enregistré.
 - MASTERKEY = tableau contenant l'UID du badge sur 32 bits.

```python
# Initialisation du lecteur de badge
rdr = mfrc522.MFRC522('D13', 'D11', 'D12', 'D9', 'D10') #SCK, MOSI, MISO, RST, SDA

# Initialisation des LEDs (LED_1, LED_2, LED_3)
blue_led = pyb.LED(1)
green_led = pyb.LED(2)
red_led = pyb.LED(3)

blue_led.on()
red_led.off()
green_led.off()

# Initialisation du servomoteur
servo = pyb.Pin('D6')

# Variables
counter = 0
MASTERKEY = [None]*4

print("\nEnregistrement d'un premier badge")
```

**Etape 3 :** Dans cette étape nous allons configurer notre système pour que celui-ci enregistre un seul et unique badge de référence qui permettra d'ouvrir la porte.<br>
Dans un premier temps on recherche la présence d'un badge. Si un badge est détecté alors on vérifie que d'autres badges ne sont pas déjà enregistrés grâce à la variable *compteur*. Si c'est le cas nous venons lire l'UID puis on le mémorise dans *MASTERKEY*. Enfin on affiche l'UID sur l'ordinateur et on éteint la LED bleue pour signaler à l'utilisateur que l'enregistrement est terminé.<br>
Après avoir configuré un badge nous ne revenons plus dans cette fonction car nous avons incrémenté la variable *compteur* qui indique désormais qu'un badge est déjà enregistré.

```python
while True:
	# Lecture d'un badge
	(stat, tag_type) = rdr.request(rdr.REQIDL)
	# Enregistrement de l'UID d'un badge
	if stat == rdr.OK and counter == 0:
		(stat, raw_uid) = rdr.anticoll()
		if stat == rdr.OK:
			MASTERKEY = raw_uid[0:4] # Récupère les valeurs de l'UID
			print("-"*36)
			print("| UID enregistré : %03d.%03d.%03d.%03d |" %(MASTERKEY[0], MASTERKEY[1], MASTERKEY[2], MASTERKEY[3]))
			print("-"*36)
			counter = counter + 1 # Incrémente le compteur pour ne plus revenir dans cette boucle
			blue_led.off()
```

**Etape 4 :** Après avoir fait l'enregistrement du badge à l'étape précédente nous affichons l'UID dans le terminal chaque fois qu'un badge est présenté au le lecteur. Pour réaliser cela copiez cette partie de code dans la boucle *while True* :

```python
	# Affichage de l'UID dans le moniteur série
	elif stat == rdr.OK and counter != 0:
		(stat, raw_uid) = rdr.anticoll()
		if stat == rdr.OK:
			print("\nUID lu : %03d.%03d.%03d.%03d" %(raw_uid[0], raw_uid[1], raw_uid[2], raw_uid[3]))
```

**Etape 5 :** Cette dernière partie de code va comparer l'UID récupéré avec l'UID du badge enregistré. S'ils sont identiques alors on allume la LED verte et on fait pivoter le moteur de 90°. On attend 3 secondes et on revient à l'état initial. Un retour d'information dans le moniteur série est également fait. Si les deux UID sont différents on allume la LED rouge pendant 1 seconde.

```python
		# Vérification du badge
		if (raw_uid[0:4] == MASTERKEY[0:4]):
			print("--> Badge : valide")
			green_led.on() # Allume la LED verte
			tim_servo = pyb.Timer(1, freq=50)
			tim_servo.channel(1, pyb.Timer.PWM, pin=servo, pulse_width_percent=12.5) # Fait tourner le servomoteur de 90 degrés
		    time.sleep(3) # Temporisation de 3 secondes
			tim_servo.channel(1, pyb.Timer.PWM, pin=servo, pulse_width_percent=7.5)
			time.sleep_ms(500) # Délais pour que le servomoteur se remette en place
			tim_servo.deinit() # Arrêt du timer pour le servomoteur
			green_led.off()
		else:
			print("--> Badge : non valide")
			red_led.on() # Allume la LED rouge
			time.sleep(1) # Temporisation d'une seconde
			red_led.off() # Eteint la LED rouge
```


<h2>Résultat</h2>
Il ne vous reste plus qu'à sauvegarder et redémarrer votre carte (Ctrl + D) pour exécuter le programme !

Vous devriez voir apparaître ce genre de code :

<div align="left">
<img alt="Affichage des données de badge RFID" src="images/rfid2-python.png" width="300px">
</div>

Vous pouvez également regarder les 3 LED de la carte NUCLEO-WB55. Elles devraient réagir en fonction du badge que vous présentez. Le servomoteur devrait également réagir si vous lui présentez le bon badge.

Et voilà, vous avez réussi à faire le premier mini-projet portant sur la porte sécurisée !
La partie suivante reprend la même base de code mais avec des capteurs supplémentaires afin de simuler un système complet de contrôle d'accès. Serez-vous prêt à relever le défi ?

<h1>Pour aller (encore) plus loin</h1>

Dans cette partie nous allons réutiliser la base de code de la partie abordée précédemment afin de réaliser une porte sécurisée par un badge RFID à un niveau plus avancé. Les composants du projet sont :

 - un lecteur RFID pour récupérer l'information.
 - les 3 LED de la carte NUCLEO-WB55 pour faire un retour d'état à l'utilisateur.
 - un servo moteur permettant de faire pivoter la porte.
Auxquels nous ajoutons :
 - un buzzer pour alerter l'utilisateur.
 - les 3 boutons poussoirs de la carte NUCLEO-WB55 pour modifier le système.

Le principe de fonctionnement de ce mini projet est le suivant :

1. Dans un premier temps la LED bleue de la carte NUCLEO-WB55 s'allume indiquant à l'utilisateur que le système est prêt à enregistrer un tag. L'utilisateur présente donc un badge RFID sur le lecteur. L'enregistrement se termine lorsque la LED bleue s'éteint.
2. Dans un second temps, si l'utilisateur présente le badge qu'il a enregistré alors la LED verte s'allume et le système actionne le servomoteur pour le faire pivoter à 90°. Au bout de 3 secondes le servomoteur revient à sa position initiale et la LED verte s'éteint.
3. Si l'utilisateur présente un badge non enregistré dans le système alors la LED rouge s'allume, un son est produit par le buzzer, puis on éteint la LED et le buzzer au bout de 5 périodes.
4. Nous voulons voir comment se comporte le système et les informations qu'il traite. Par conséquent on affiche ces informations dans le moniteur série.
5. Si on appuie sur le bouton SW1 de la carte NUCLEO-WB55 alors on enregistre un nouveau badge dans le système (dans la limite de 3 badges maximum). La LED bleue s'allume pour indiquer que le système est prêt à enregistrer un nouveau badge et s'éteint à la fin.
6. Si on appuie sur le bouton SW2 de la carte NUCLEO-WB55 alors on affiche dans le moniteur série tous les UID des badges enregistrés.
7. Enfin, si on appuie sur le bouton SW3 de la carte NUCLEO-WB55 alors on déréférence un badge en le présentant sur le lecteur. La LED rouge s'allume pour indiquer que le système est en attente de suppression d'un identifiant de badge puis elle s'éteint une fois cette tâche terminée.


<h2>Montage :</h2>

On reprend le même montage que la partie précédente auquel on ajoute un buzzer. Nous utilisons les boutons poussoirs de la carte UCLEO-WB55 donc nous n'avons pas besoin de les câbler.

<div align="left">
<img alt="Schéma de montage lecteur RFID" src="images/rfid-schema3.png" width="800px">
</div>

La correspondance entre le buzzer et la carte NUCLEO-WB55 est la suivante :

| Buzzer      		| ST Nucleo         |
| :-------------:   | :---------------: |
|        +          |         D3        |
|        -          |         GND       |

Et pour rappel :

| Servo-moteur      | ST Nucleo         |
| :-------------:   | :---------------: |
|       Signal      |         D6        |
|       GND         |         GND       |
|       5V          |         5V        |

| Lecteur RFID      | ST Nucleo         |
| :-------------:   | :---------------: |
|       SDA         |         D10       |
|       SCK         |         D13       |
|       MOSI        |         D11       |
|       MISO        |         D12       |
|       GND         |         GND       |
|       RST         |         D9        |
|       3.3V        |         3.3V      |


<h2>Programme :</h2>

> **Vous pouvez télécharger les scripts MicroPython de ce tutoriel (entre autres) en cliquant [ici](../../../assets/Script/MODULES.zip)**.

Le code de cette partie reprend la même base que le code fournit précédemment. Par conséquent seulement les nouvelles fonctions seront expliquées plus en détails.

**Etape 1 :** On importe les bibliothèques dans notre code.

```python
import pyb
import time
import mfrc522
```

**Etape 2 :** On définit les ports utilisés pour établir la connexion avec le lecteur de badge RFID. On initialise des LED, le servomoteur, le buzzer et les 3 boutons poussoirs. On déclare 4 variables :

 - compteur = variable de type entier permettant de savoir si un badge a été enregistré.
 - MASTERKEY = tableau contenant 3 UID de badges, codés sur 32 bits.
 - e = variable de type entier servant de pointeur dans le tableau MASTERKEY sur le badge voulu.
 - nb_max_badge = variable de type entier permettant d'indiquer le nombre de badges utilisés.

```python
rdr = mfrc522.MFRC522('D13', 'D11', 'D12', 'D9', 'D10') #SCK, MOSI, MISO, RST, SDA

# Initialisation des LEDs (LED_1, LED_2, LED_3)
blue_led = pyb.LED(1)
green_led = pyb.LED(2)
red_led = pyb.LED(3)

blue_led.on()
red_led.off()
green_led.off()

# Initialisation du servomoteur
servo = pyb.Pin('D6')

# Initialisation du buzzer
buzzer = pyb.Pin('D3')

# Initialisation des boutons
sw1 = pyb.Pin('SW1', pyb.Pin.IN)
sw1.init(pyb.Pin.IN, pyb.Pin.PULL_UP, af=-1)
sw2 = pyb.Pin('SW2', pyb.Pin.IN)
sw2.init(pyb.Pin.IN, pyb.Pin.PULL_UP, af=-1)
sw3 = pyb.Pin('SW3', pyb.Pin.IN)
sw3.init(pyb.Pin.IN, pyb.Pin.PULL_UP, af=-1)

# Variables
counter = 0
MASTERKEY = [None]*12
e = 0
nb_max_tag = const(8)

print("\nEnregistrement d'un premier badge")
```

**Etape 3 :** Dans cette étape nous allons configurer notre système pour que celui-ci enregistre au moins un badge qui servira à ouvrir la porte.<br>
Dans un premier temps on recherche la présence d'un badge. En cas de succès on vérifie que d'autres badges ne sont pas déjà enregistrés grâce à la variable *compteur*. Si c'est le cas nous lisons l'UID puis nous le mémorisons dans *MASTERKEY*. Enfin, on affiche l'UID sur l'ordinateur et on éteint la LED bleue pour signaler à l'utilisateur que l'enregistrement est terminé.<br>
Après avoir configuré un badge nous ne revenons plus dans cette fonction car nous avons incrémenté la variable *compteur* afin d'indiquer qu'un badge a été enregistré.<br>

**Remarque :** à partir de cette étape toutes les autres parties de codes sont contenues dans la boucle infinie *while*.

```python
while True:
	# Lecture d'un badge
	(stat, tag_type) = rdr.request(rdr.REQIDL)
	# Enregistrement de l'UID d'un badge
	if stat == rdr.OK and counter == 0:
		(stat, raw_uid) = rdr.anticoll()
		if stat == rdr.OK:
			MASTERKEY = raw_uid[0:4] # Récupère les valeurs de l'UID
			print("-"*36)
			print("| UID enregistré : %03d.%03d.%03d.%03d |" %(MASTERKEY[0], MASTERKEY[1], MASTERKEY[2], MASTERKEY[3]))
			print("-"*36)
			counter = counter + 1 # Incrémente le compteur pour ne plus revenir dans cette boucle
			blue_led.off()
```

**Etape 4 :** Ensuite nous créons la partie concernant l'enregistrement d'un nouveau badge quand on presse sur le bouton SW1 de la carte NUCLEO-WB55.<br>
Dans un premier temps on vérifie le nombre de badges enregistrés grâce à la valeur de la variable *e*.
S'il reste de la place alors on allume la LED bleue pour informer l'utilisateur qu'il peut enregistrer un badge en le plaçant sur le lecteur.<br>
Les données du nouveau badge sont mises à la suite des données des autres badges dans le tableau *MASTERKEY*.<br>
Enfin un message s'affiche dans le moniteur série pour indiquer le statut de l'enregistrement des badges.

```python
	elif sw1.value() == 0:
		if e < nb_max_tag:
			blue_led.on()
			print("\nEnregistrement d'un autre badge")
			(stat, tag_type) = rdr.request(rdr.REQIDL)
			(stat, raw_uid) = rdr.anticoll()
			while(stat == rdr.ERR): # En attente tant qu'il n'y a pas de badge détecté
				(stat, tag_type) = rdr.request(rdr.REQIDL)
				(stat, raw_uid) = rdr.anticoll()
			if stat == rdr.OK: # Si un badge est détecté alors on l'enregistre
				e = e + 4 # Pointer dans un emplacement vide de MASTERKEY
				MASTERKEY = MASTERKEY + raw_uid[0:4] # MASTERKEY stocke les anciens UID + le nouveau UID
				print("-"*36)
				print("| UID enregistré : %03d.%03d.%03d.%03d |" %(MASTERKEY[e], MASTERKEY[e+1], MASTERKEY[e+2], MASTERKEY[e+3]))
				print("-"*36)
				blue_led.off()
		else:
			print("\n|!| Erreur : déjà 3 badges enregistrés |!|")
```

**Etape 5 :** Cette partie de code affichera tous les UID stockés par le système dans le moniteur série. Ces données sont mémorisées dans le tableau *MASTERKEY*, il nous suffira donc de faire une boucle sur ses éléments.

**Remarque :** Dans notre cas la boucle **for** s'incrémente de 4 afin d'outrepasser la fonction *print* de MicroPython qui renvoi automatiquement à la ligne. Il a fallu adopter cette solution afin de pouvoir afficher 4 valeurs d'UID sur une même ligne.

```python
	elif sw2.value() == 0:
		print("\nListe des UID enregistrés")
		print("-"*23)
		for i in range(0, e+1, 4):
			print("| --> %03d.%03d.%03d.%03d |"%(MASTERKEY[i+0], MASTERKEY[i+1], MASTERKEY[i+2], MASTERKEY[i+3]))
		print("-"*23)
```

**Etape 6 :** Enfin nous abordons la partie de code concernant la suppression d'un badge en appuyant sur le bouton SW3.<br>
C'est certainement la plus complexe à comprendre. En effet un utilisateur peut vouloir supprimer un badge dont les données se situent n'importe où dans le tableau *MASTERKEY* (par exemple au milieu de celui-ci). Il faut donc repérer où se situent les données du badge, prendre l'ensemble de son UID (les 4 valeurs) pour les supprimer et enfin recoller le tableau *MASTERKEY* pour que les nouvelles cases vides se situent à la fin.<br>
Pour en revenir à notre code, lors de l'appui sur SW3 on allume la LED rouge. Ensuite la boucle *while* nous permet d'attendre jusqu'à ce qu'un badge soit présenté.<br>
Si un badge est présenté alors on initialise une variable *j* qui nous servira à pointer la case de la première valeur de l'UID du badge présenté dans le tableau *MASTERKEY*. La seconde boucle *while* nous permet justement de faire cette recherche.<br>
Enfin *del* nous permet de supprimer les 4 valeurs de l'UID et de recoller le tableau, pratique !<br>
Pour finir on affiche un message dans le moniteur série et on éteint la LED rouge.<br>

```python
	elif sw3.value() == 0:
		print("\nSupression d'un badge")
		red_led.on()
		while(stat == rdr.ERR): # En attente tant qu'il n'y a pas de badge détecté
			(stat, tag_type) = rdr.request(rdr.REQIDL)
			(stat, raw_uid) = rdr.anticoll()
		if stat == rdr.OK:
			j = 0
			while(MASTERKEY[j:j+4] != raw_uid[0:4]): # Recherche de l'UID du badge présenté dans ceux stockés
				j = j + 4
			del MASTERKEY[j:j+4] # Suppression du badge
			e = e - 4
			print("Badge n°%d supprimé !" %(j/4+1))
			red_led.off()
```

**Etape 7 :** Après avoir fait l'enregistrement du badge et avoir traité les boutons poussoirs nous affichons l'UID dans le terminal chaque fois qu'un badge est présenté sur le lecteur.

```python
	elif stat == rdr.OK and counter != 0:
		(stat, raw_uid) = rdr.anticoll()
		if stat == rdr.OK:
			print("\nUID lu : %03d.%03d.%03d.%03d" %(raw_uid[0], raw_uid[1], raw_uid[2], raw_uid[3]))
```

**Etape 8 :** Cette dernière partie de code va comparer l'UID récupéré avec l'UID du badge enregistré.
Si les deux UID sont les mêmes alors on allume la LED verte et on fait pivoter le moteur de 90°. On attend 3 secondes et on revient à l'état initial. Un retour d'information dans le moniteur série est également fait.
Si les deux UID sont différents on allume la LED rouge, on joue 5 fois 2 sons au buzzer puis on arrête et on éteint la LED.

```python
		if (raw_uid[0:4] == MASTERKEY[0:4]) or (raw_uid[0:4] == MASTERKEY[4:8]):
			print("--> Badge : valide")
			green_led.on() # Allume la LED verte
			tim_servo = pyb.Timer(1, freq=50)
			tim_servo.channel(1, pyb.Timer.PWM, pin=servo, pulse_width_percent=12.5) # Fait tourner le servomoteur de 90 degré
			time.sleep(3) # Temporisation de 3 secondes
			tim_servo.channel(1, pyb.Timer.PWM, pin=servo, pulse_width_percent=7.5)
			time.sleep_ms(500) # Délais pour que le servomoteur se remette en place
			tim_servo.deinit() # Arrêt du timer pour le servomoteur
			green_led.off()
		else:
			print("--> Badge : non valide")
			red_led.on() # Allume la LED rouge
			for j in range(0, 5): # Séquence du buzzer
				tim_buzzer = pyb.Timer(1, freq=1000)
				tim_buzzer.channel(3, pyb.Timer.PWM, pin=buzzer, pulse_width_percent=5)
				time.sleep_ms(200)
				tim_buzzer = pyb.Timer(1, freq=3000)
				tim_buzzer.channel(3, pyb.Timer.PWM, pin=buzzer, pulse_width_percent=5)
				time.sleep_ms(200)
			tim_buzzer.deinit() # Arrêt du timer pour le buzzer
			red_led.off()		
```


<h2>Résultat :</h2>
Il ne vous reste plus qu'à sauvegarder et redémarrer votre carte (Ctrl + D) pour exécuter le programme !

Vous devriez voir apparaître ce genre de code :

<br>
<div align="left">
<img alt="Affichage des données de badge RFID" src="images/rfid3-python.png" width="300px">
</div>
<br>

Vous pouvez également regarder les 3 LED de la carte NUCLEO-WB55. Elles devraient réagir en fonction du badge que vous présentez. Le servomoteur devrait également réagir si vous lui présentez le bon badge et, à contrario, le buzzer devrait réagir si vous lui présentez le mauvais badge.

Amusez-vous avec les boutons poussoirs, pour cela il suffit de les maintenir appuyés au moins 1 seconde (le temps que le programme reboucle et prenne en compte les changements).

**Félicitations, vous avez réussi à faire le deuxième mini-projet portant sur la porte sécurisée !**<br>
Au total vous avez réussi à utiliser 5 composants électroniques sur une seule et même carte. Imaginez un peu toutes les possibilités qui s'offrent à vous grâce au STM32 et à MicroPython !
