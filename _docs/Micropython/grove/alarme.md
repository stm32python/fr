---
title: Alarme de mouvement
description: Création d'une alarme de mouvement avec un buzzer et un capteur de mouvements PIR sous MicroPython
---

# Alarme de mouvement

Dans ce tutoriel nous utiliserons un [module buzzer Grove ](buzzer) ainsi qu'un [module détecteur de mouvement PIR Grove](mouvement).
Lorsqu'un mouvement sera détecté, le buzzer produira un son (alarme) pendant une seconde.<br>
Pour générer le signal d'alarme, on enverra au buzzer un signal modulé en largeur d'impulsion (PWM). Vous trouverez une explication de ce qu'est la PWM [ici](buzzer).

**Le buzzer Grove (à gauche) et le capteur de mouvement PIR Grove (à droite) :**

|Grove - Buzzer|Grove - PIR Motion Sensor|
|:-:|:-:|
|<img alt="Grove - Buzzer" src="images/buzzer.png" width="180px">|<img alt="Grove - PIR Motion Sensor" src="images/Grove_-_PIR_Motion_Sensor.jpg" width="250px">|

> Crédit image : [Seeed Studio](http://wiki.seeedstudio.com/Grove_System/)

## Matériel requis et montage

1. Une [carte d'extension de base Grove](https://wiki.seeedstudio.com/Base_Shield_V2/)
2. La carte NUCLEO-WB55
3. Un [buzzer Grove](https://wiki.seeedstudio.com/Grove-Buzzer/)
4. Un [détecteur de mouvement PIR Grove](https://wiki.seeedstudio.com/Grove-PIR_Motion_Sensor/)

Branchez le buzzer sur le connecteur *D3* et le détecteur de mouvements sur le connecteur *D2*.
Si un mouvement est détecté, l'interruption se déclenche ce qui provoque l'activation du buzzer.

## Le code MicroPython

> **Vous pouvez télécharger les scripts MicroPython de ce tutoriel (entre autres) en cliquant [ici](../../../assets/Script/MODULES.zip)**.

Editez le fichier *main.py* et collez y le code qui suit, avant de l'enregistrer dans le répertoire du périphérique PYBFLASH :<br>

```python
# Objet du script :
# Conception d'un système d'alarme basé sur un détecteur de mouvements.
# Un capteur de mouvement PIR est configuré en interruption sur la broche D2.
# Un buzzer est connecté à la broche D3 et piloté par une PWM.
# Matériel requis :
#   - Un capteur PIR (de préférence fonctionnant en 3.3V)
#   - Un buzzer (de préférence fonctionnant en 3.3V)
# On utilise une interruption pour capturer le signal du détecteur de mouvements.


from time import ticks_ms, ticks_diff # Pour gérer les temporisations
from pyb import Pin, Timer

# Configuration du buzzer
frequency = 440
buz = Pin('D3')

# D3 génère une PWM avec TIM1, CH3
timer1 = Timer(1, freq=frequency)
channel3 = timer1.channel(3, Timer.PWM, pin=buz)

# Configuration du capteur PIR
PIR_Pin = Pin('D2', Pin.IN)

# Durée de l'alarme en millisecondes
_ALARM_DURATION_MS = const(999)

# Variables globales
motion = False # Est-ce que j'ai détecté un mouvement ?
tick_alarm_start = 0 # Nombre de millisecondes système au moment de l'alarme

# Fonction de gestion de l'interruption du capteur PIR
def interrupt_handler(pin):
	
	global motion, tick_alarm_start
	global interrupt_pin
	
	# Signale qu'un mouvement a été détecté
	motion = True
	
	# Date de l'évènement
	tick_alarm_start = ticks_ms()

	# Broche générant l'interruption
	interrupt_pin = PIR_Pin

# Activation de l'interruption du capteur PIR
PIR_Pin.irq(trigger=PIR_Pin.IRQ_RISING, handler=interrupt_handler)

# Boucle principale
while True:

	if motion:
		
		print('Mouvement détecté')
		
		# Désactive l'interruption du capteur PIR
		PIR_Pin.irq(trigger=PIR_Pin.IRQ_RISING, handler=None)

		# Rapport cyclique paramétré à 5% (le buzzer est alimenté 5% du temps d'une période)
		channel3.pulse_width_percent(5)

		# Nombre de millisecondes écoulées depuis la détection du mouvement

		if ticks_diff(ticks_ms(), tick_alarm_start) > _ALARM_DURATION_MS:

			# Rapport cyclique paramétré à 0% (le buzzer n'est plus alimenté)
			channel3.pulse_width_percent(0)
		
			# Interruption du capteur réactivée
			PIR_Pin.irq(trigger=PIR_Pin.IRQ_RISING, handler=interrupt_handler)
			print('Détecteur de mouvement desactivé')
			
			# Signale que le traitement de la détection de mouvement est terminé
			motion = False

	# Place le microcontrôleur en sommeil en attendant la prochaine interruption
	pyb.wfi() 
```

## Affichage sur le terminal série de l'USB User

Appuyez sur *[CTRL]-[D]* dans le terminal PuTTY et subissez les alarmes stridentes du buzzer à chaque mouvement !
