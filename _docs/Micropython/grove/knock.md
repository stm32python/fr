---
title: Détection de choc
description: Détection de choc avec un capteur de vibration
---

# Capteur de vibration

Ce tutoriel explique comment mettre en œuvre un capteur de vibration.

<h2>Description</h2>
Ce capteur dispose d'un ressort qui vibre quand celui-ci reçoit un choc.
Il peut être utilisé dans des systèmes de sécurité ou de surveillance.


## Matériel requis

1. Une [carte d'extension de base Grove](https://wiki.seeedstudio.com/Base_Shield_V2/)
2. La carte NUCLEO-WB55
3. Un capteur de vibration 

**Le capteur de gouttes de pluie :**

<div align="left">
<img alt="Grove - Knock sensor" src="images/knock.jpg" width="300px">
</div>

Le capteur est connecté sur le connecteur D3 du shield Grove.


<h2>Le code MicroPython</h2>

> **Vous pouvez télécharger les scripts MicroPython de ce tutoriel (entre autres) en cliquant [ici](../../../assets/Script/MODULES.zip)**.

```python
# Detection d'un choc/vibration
from pyb import Pin				# Pour les accès aux périphériques (GPIO, LED, etc.)
from time import sleep 			# Pour les temporisations

# Instanciation
vibration = Pin('D3', Pin.IN)	# Pour le capteur de vibration
led = pyb.LED(3)				# Pour le retour d'information par LED

while True:
	val = vibration.value()		# Acquisition de la valeur
	if(val == 1):				# Si il ne sait rien passé on laisse la LED éteinte
		led.off()
	else:						# Sinon on allume la LED pendant 3 secondes
		led.on()
		sleep(3)
```

<h2>Résultat</h2>

Essayez de donner un choc au capteur, celui-ci devrait alors le détecter et allumer la LED sur la carte Nucleo.
