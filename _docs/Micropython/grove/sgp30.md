---
title: Capteur de qualité de l'air SGP30
description: Mise en œuvre un module capteur de qualité de l'air SGP30 sur bus I2C en MicroPython
---

# Capteur de qualité de l'air SGP30

Ce tutoriel explique comment mettre en œuvre un module Grove capteur de qualité de l'air SGP30 sur bus I2C en MicroPython. 

**Le module Grove capteur de qualité de l'air SGP30 :**

<br>
<div align="left">
<img alt="Grove sgp30" src="images/sgp30.jpg" width="300px">
</div>
<br>

> Crédit image : [Seeed Studio](http://wiki.seeedstudio.com/Grove_System/)

La fiche technique du capteur SGP30 de Sensirion [est disponible ici](https://www.sensirion.com/fileadmin/user_upload/customers/sensirion/Dokumente/9_Gas_Sensors/Datasheets/Sensirion_Gas_Sensors_Datasheet_SGP30.pdf). L'interprétation des informations renvoyées par un capteur de qualité d'air tel que le SGP30 **nécessite un peu de recul** ; d'après cette [source](https://projetsdiy.fr/test-du-ccs811-iaq-capteur-qualite-air-tcov-eco2-i2c-arduino-esp8266/) :

> *(Le SGP30) retourne donc deux valeurs. Le TVOC qui est la somme de tous les composés organiques volatils (COV) trouvés dans l’atmosphère et l’eCO<sub>2</sub>. Le eCO<sub>2</sub> est un taux de CO<sub>2</sub> calculé mathématiquement par le capteur... Ce n’est donc pas une mesure réelle ... c’est un indicateur théorique qu’il faut lire avec précaution. En présence de polluant(s), l’indicateur eCO<sub>2</sub> risque de s’envoler sans pour autant représenter la teneur réelle en CO<sub>2</sub> dans l’atmosphère. Pour connaître tous les seuils, vous pouvez lire [ce document](http://www.inrs.fr/dms/inrs/CataloguePapier/DMT/TI-TC-74/tc74.pdf) de l’INRS.*


## Matériel requis et montage

1. Une [carte d'extension de base Grove](https://wiki.seeedstudio.com/Base_Shield_V2/)
2. La carte NUCLEO-WB55
3. Un [module Grove - VOC and eCO<sub>2</sub> Gas Sensor (SGP30)](https://wiki.seeedstudio.com/Grove-VOC_and_eCO2_Gas_Sensor-SGP30/)

Branchez le module sur l'un des connecteurs I2C du Grove base shield.

## Le code MicroPython

> **Vous pouvez télécharger les scripts MicroPython de ce tutoriel (entre autres) en cliquant [ici](../../../assets/Script/MODULES.zip)**.

Il faut ajouter le fichier *sgp30.py* dans le répertoire du périphérique *PYBFLASH*.<br>
Editez maintenant le script *main.py* et collez-y le code qui suit :

```python
# Exemple adapté de https://GitHub.com/safuya/MicroPython-sgp30/blob/master/
# Objet du script : Mise en œuvre du module grove I2C capteur de gas COV et CO2
# basé sur le capteur SGP30

from time import sleep_ms
from machine import I2C, Pin
from sgp30 import SGP30

# On utilise l'I2C n°1 de la carte NUCLEO-WB55 pour communiquer avec le capteur
i2c = I2C(1)

# Pause d'une seconde pour laisser à l'I2C le temps de s'initialiser
sleep_ms(1000)

# Liste des adresses I2C des périphériques présents
print("Adresses I2C utilisées : " + str(i2c.scan()))

# Instanciation du capteur
sgp = SGP30(i2c)

while True:

	# Temporisation d'une seconde
	sleep_ms(1000)

	# Lecture des valeurs mesurées
	co2eq, tvoc = sgp.indoor_air_quality

	# Affichage formatté des valeurs mesurées
	print("CO2eq = %d ppm \t TVOC = %d ppb" % (co2eq, tvoc))
```

## Sortie sur le port série de l'USB USER

Appuyez sur *[CTRL]-[D]* dans le terminal PuTTY et observez les valeurs qui défilent :

```console
MPY: sync filesystems
MPY: soft reboot
Adresses I2C utilisées : [88]
CO2eq = 400 ppm          TVOC = 4 ppb
CO2eq = 400 ppm          TVOC = 0 ppb
CO2eq = 400 ppm          TVOC = 0 ppb
CO2eq = 400 ppm          TVOC = 0 ppb
CO2eq = 4634 ppm         TVOC = 253 ppb
CO2eq = 3323 ppm         TVOC = 331 ppb
CO2eq = 25192 ppm        TVOC = 2177 ppb
CO2eq = 4310 ppm         TVOC = 886 ppb
CO2eq = 7456 ppm         TVOC = 792 ppb
CO2eq = 1113 ppm         TVOC = 365 ppb
CO2eq = 508 ppm          TVOC = 182 ppb
CO2eq = 400 ppm          TVOC = 113 ppb
CO2eq = 400 ppm          TVOC = 81 ppb
CO2eq = 400 ppm          TVOC = 52 ppb
CO2eq = 400 ppm          TVOC = 32 ppb
CO2eq = 400 ppm          TVOC = 17 ppb
```


L'augmentation de CO2eq et TVOC s'obtient en soumettant le capteur à des vapeurs d'alcool médicinal à 70%. On constate que le capteur réagit très vite à l'éthanol et que la concentration estimée de CO<sub>2</sub> n'est pas pertinente en l'occurrence ; il n'y a bien évidemment pas de dioxyde de carbone en quantité significative dans les vapeurs de C<sub>2</sub>H<sub>6</sub>O !

Les unités utilisées sont :
- **ppm** : **Parties par million**. L’acronyme « ppm » signifie « partie par million ». Il s’agit d’une unité de mesure communément utilisée par les scientifiques, notamment pour calculer le taux de pollution dans l’air et plus globalement dans l’environnement. Comme son nom l’indique, le ppm permet de savoir combien de molécules de "polluant" on trouve parmi un million de molécules d’air.
- **ppb** : **Parties par milliard** (le "b" correspond à "billion" en anglais, soit "milliard" en français). Indique donc combien de molécules de "polluant" on trouve parmi un milliard de molécules d’air.
