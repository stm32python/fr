---
title: Sonomètre - mesure de la puissance sonore en décibels
description: Mise en œuvre du module sonomètre analogique Gravity SEN0232 de DFROBOT avec MicroPython
---

# Sonomètre - mesure de la puissance sonore en décibels

Ce tutoriel explique comment mettre en œuvre un [module sonomètre analogique Gravity SEN0232 de DFROBOT](https://wiki.dfrobot.com/Gravity__Analog_Sound_Level_Meter_SKU_SEN0232) avec MicroPython.
Plus précisément, nous allons construire un détecteur de bruits. On commence par calibrer le capteur : on mesure pendant un certain temps le volume sonore du lieu qui sera surveillé, en faisant le moins de bruits possible.
Une fois ce volume connu, le micro écoute et signale tout bruit plus intense.<br>

**Le module sonomètre analogique Gravity SEN0232 de DFROBOT :**

<br>
<div align="left">
<img alt="DFROBOT SEN0232 sound level meter" src="images/SoundLevelMeterBoardConnector.png" width="300px">
</div>
<br>

Légende :

|**Num**|**Label**|**Description**|
|:-|:-:|:-:|
|1|A|Sortie signal analogique(0.6~2.6V)|
|2|+|Alimentation VCC (3.3~5.0V)|
|3|-|Alimentation GND (0V)|

> Crédit image et légende : [DFROBOT](https://www.dfrobot.com/)

Ce tutoriel est une version améliorée du [détecteur de bruits construit avec un module capteur de sons Grove](bruit). Le module de DFROBOT est significativement plus sensible (et plus cher !) que celui de Grove et **permet d'obtenir une mesure physique de l'intensité sonore, en décibels (dB)**.

En acoustique environnementale, on indique couramment le niveau du bruit en dB. Cette valeur exprime le rapport de puissance entre la pression acoustique et une valeur de référence qui correspond à un son imperceptible. La figure qui suit rappelle les niveaux de décibels pour certains sons familiers  :

<br>
<div align="left">
<img alt="Echelle des décibels" src="images/echelle_des_decibels.png" width="400px">
</div>
<br>

> Crédit image : image de [b44022101](https://www.istockphoto.com/fr/portfolio/b44022101?mediatype=illustration) via [iStockphoto](https://www.istockphoto.com/fr/vectoriel/l%C3%A9chelle-des-d%C3%A9cibels-gm849774892-139598889).

Pour en savoir plus sur les sujets du bruit et de sa mesure en décibel, nous vous renvoyons à [l'article correspondant de Wikipédia](https://fr.wikipedia.org/wiki/D%C3%A9cibel_(bruit)). 

## Matériel requis et montage

1. [Une carte d'extension de base Grove](https://wiki.seeedstudio.com/Base_Shield_V2/) 
2. La carte NUCLEO-WB55
3. [Un module DFROBOT Gravity SEN0232 sound level meter](https://wiki.dfrobot.com/Gravity__Analog_Sound_Level_Meter_SKU_SEN0232)
4. [Un câble Grove-Dupont mâle](https://fr.vittascience.com/shop/229/5-Grove-cables---Dupont-male)

D'après le script qui suit, le module doit être connecté sur une prise analogique du Grove Base Shield, nous choisissons le **connecteur A1**. Le schémas de connexion du module est indiqué dans le tableau ci-dessus.
Notez bien que ce module ne disposant pas de connecteurs Grove donc vous aurez besoin de câbles dupont et d'adaptateurs Dupont-Grove pour sa connexion à la carte d'extension de base Grove.<br>
**Attention**, le revêtement noir du micro du sonomètre est fragile, ne le touchez pas !

## Le code MicroPython

> **Vous pouvez télécharger les scripts MicroPython de ce tutoriel (entre autres) en cliquant [ici](../../../assets/Script/MODULES.zip)**.

Editez maintenant le script *main.py* du du périphérique *PYBFLASH* et copiez-y le code qui suit :

```python
# Objet du script : 
# Mise en œuvre du module sonomètre analogique SEN0232 de DFROBOT
# Ce module est très sensible et capable de renvoyer une intensité sonore exprimée en décibels.

from time import sleep_ms	# Pour temporiser
from pyb import Pin, ADC	# Gestion des broches et du coonvertisseur 
							# analogique-numérique (ADC)

VREF = 3.3 # Tension de référence de l'ADC
CONV = VREF / 4096 # Facteur de conversion lecture ADC -> Tension
VOLTAGE_TO_DB = VREF * 50 / 4096 # Facteur de conversion lecture ADC -> décibels

adc = ADC(Pin('A1')) # On "connecte" l'ADC à la broche A1

# Première étape : calibrage
# On mesure le voulume sonore ambiant pendant quelques temps pour être en mesure
# de discriminer par la suite les bruits anormaux.

ambiant_noise_level = 0

print("Etape de calibrage")
print("Ne faites pas de bruit !")
sleep_ms(10000)
print("Démarrage du calibrage")

for x in range(500):

	# Délai de 125 millisecondes pour laisser au capteur le temps de se stabiliser
	sleep_ms(125)
	
	# Lecture du bruit ambiant en décibels
	noise = adc.read() * VOLTAGE_TO_DB
	
	# Si la valeur du bruit est la plus grande détectée jusqu'ici
	# mémorise là
	if noise > ambiant_noise_level:
		ambiant_noise_level  = noise
		
print("Fin du calibrage")
print("Volume sonore ambiant : %.1f dB" %ambiant_noise_level)
sleep_ms(5000)

print("Sonomètre à l'écoute ...")

while True : # Boucle sans clause de sortie

	sleep_ms(125)
	
	noise = adc.read() * VOLTAGE_TO_DB
	
	# Si le bruit détecté est plus fort que le seuil de bruit ambiant
	# calculé pendant le calibrage ...
	if noise > ambiant_noise_level:
		# Affiche une alerte
		print("Bruit atypique !")
		print("Intensité : %.1f dB" %noise)
		sleep_ms(1000)
```

## Résultat

Lancez le script avec *[CTRL]-[D]* dans le terminal PuTTY. Restez silencieux pendant la phase de calibrage (environs deux minutes). Par la suite, un message s'affichera chaque fois que le module détectera un son plus fort que sa valeur de référence :

```console
MicroPython v1.19.1 on 2022-06-18; NUCLEO-WB55 with STM32WB55RGV6
Type "help()" for more information.
>>>
>>>
MPY: sync filesystems
MPY: soft reboot
Etape de calibrage
Ne faites pas de bruit !
Démarrage du calibrage
Fin du calibrage
Volume sonore ambiant : 35.2 dB
Sonomètre à l'écoute ...
Bruit atypique !
Intensité : 35.3 dB
Bruit atypique !
Intensité : 38.9 dB
Bruit atypique !
Intensité : 40.8 dB
Bruit atypique !
Intensité : 43.0 dB
Bruit atypique !
Intensité : 47.0 dB
Bruit atypique !
```