---
title: Capteur de température et d'humidité DHT20
description: Mise en œuvre du capteur de température et d'humidité DHT20 (Grove et autres) en MicroPython
---

# Capteur de température et d'humidité DHT20

Ce tutoriel explique comment mettre en œuvre [le module Grove capteur de température et d'humidité DHT20](https://www.seeedstudio.com/Grove-Temperature-Humidity-Sensor-V2-0-DHT20-p-4967.html) en MicroPython. Ce capteur est une évolution du DHT11, voici ses caractéristiques selon Seeed Studio :

- Plage de mesure de températures : -40°C à +80°C ± 0.5 ℃
- Plage de mesure d'humidité : 0 à 100% RH ± 3 % RH (à 25 ℃ )

Surtout, ce module apporte une évolution particulièrement intéressante par comparaison avec [ses versions antérieures](DHT) : **il est désormais géré par un bus I2C au lieu d'imposer un protocole spécifique**.

**Le module Grove Temperature & Humidity Sensor V2.0 :**

<br>
<div align="left">
<img alt="Grove - Temperature & Humidity Sensor V2.0 (DHT20) " src="images/DHT20.jpg" width="300px">
</div>
<br>

> Crédit images : [Seeed Studio](http://wiki.seeedstudio.com/Grove_System/)


## Matériel requis et montage

1. Une [carte d'extension de base Grove](https://wiki.seeedstudio.com/Base_Shield_V2/)
2. La carte NUCLEO-WB55
3. Un [module Grove  Temperature & Humidity Sensor V2.0 (DHT20)](https://www.seeedstudio.com/Grove-Temperature-Humidity-Sensor-V2-0-DHT20-p-4967.html)

Branchez le module sur l'un des connecteurs *I2C* de la carte d'extension de base Grove.

## Le code MicroPython

> **Vous pouvez télécharger les scripts MicroPython de ce tutoriel (entre autres) en cliquant [ici](../../../assets/Script/MODULES.zip)**.

Il faut ajouter le fichier *dht20.py* dans le répertoire du périphérique *PYBFLASH*. Editez maintenant le script *main.py* et copiez-y le code qui suit :

```python
# Objet du script : Mise en œuvre du module Grove I2C capteur de pression,
# température et humidité basé sur le DHT22
# Source : https://github.com/flrrth/pico-dht20/tree/main

from time import sleep_ms
from machine import I2C
import dht20

# On utilise l'I2C n°1 de la carte NUCLEO-WB55 pour communiquer avec le capteur
i2c1 = I2C(1)

# Pause d'une seconde pour laisser à l'I2C le temps de s'initialiser
sleep_ms(1000)

# Liste des adresses I2C des périphériques présents
print("Adresses I2C utilisées : " + str(i2c1.scan()))

# Instanciation du capteur
dht = dht20.DHT20(i2c=i2c1)

while True:

	measurements = dht.measurements
	print(f"Température : {measurements['t']} °C, humidité : {measurements['rh']} %RH")
	sleep_ms(1000)
```

## Affichage sur le terminal série de l'USB User

Une fois le script lancé avec *[CTRL]-[D]*, vous pourrez observer les valeurs d'humidité et de température qui défilent :

```console
MPY: sync filesystems
MPY: soft reboot
Adresses I2C utilisées : [56]
Température : 23.90156 °C, humidité : 46.5374 %RH
Température : 23.8924 °C, humidité : 46.45481 %RH
Température : 23.88744 °C, humidité : 46.36736 %RH
Température : 23.90842 °C, humidité : 46.23365 %RH
Température : 23.9296 °C, humidité : 46.14582 %RH
Température : 23.92044 °C, humidité : 46.14668 %RH
Température : 23.90671 °C, humidité : 46.14716 %RH
Température : 23.90442 °C, humidité : 46.11273 %RH
Température : 23.93169 °C, humidité : 46.08727 %RH
```

