---
title: Anneau de LED RGB Grove Neopixel
description: Mise en œuvre d'un anneau de LED RGB WS2813 Mini Grove avec MicroPython
---

# Anneau de LED RGB Grove Neopixel

Ce tutoriel explique comment mettre en œuvre un module Grove anneau de LED RGB WS2813 Mini avec MicroPython.
Ce module est décliné en plusieurs versions selon le nombre de LED : 16, 24 ou 48. 
On peut le programmer exactement comme un [ruban de LED RGB Grove Neopixel](anneau_neopixel), en utilisant un protocole spécifique géré par la classe Neopixel intégrée au firmware MicroPython. Notre exemple explique comment allumer ou éteindre les LED à l'aide d'un potentiomètre rotatif.

**Le module Grove anneau de 24 LED RGB WS2813 Mini :**

<br>
<div align="left">
<img alt="Grove - 24  RGB LED ring" src="images/grove-rgb-24-led-ring-ws2813-mini.jpg" width="300px">
</div>
<br>

> Crédit image : [Seeed Studio](http://wiki.seeedstudio.com/Grove-4-Digit_Display/)

## Matériel requis et montage

1. Une [carte d'extension de base Grove](https://wiki.seeedstudio.com/Base_Shield_V2/).
2. La carte NUCLEO-WB55.
3. Un [anneau de LED RGB Grove](https://www.seeedstudio.com/Grove-RGB-LED-Ring-16-WS2813-Mini-p-4201.html).
4. Un [module potentiomètre rotatif Grove](https://wiki.seeedstudio.com/Grove-Rotary_Angle_Sensor/).

Branchez :
- La carte d'extension de base Grove sur les connecteurs Arduino de la NUCLEO-WB55 ;
- L'anneau de LED sur le connecteur D2 de la carte d'extension de base Grove ;
- Le potentiomètre sur  le connecteur A0 de la carte d'extension de base Grove.

## Le code MicroPython

> **Vous pouvez télécharger les scripts MicroPython de ce tutoriel (entre autres) en cliquant [ici](../../../assets/Script/MODULES.zip)**.

Il faut récupérer et ajouter le fichier *neopixel.py* dans le répertoire du périphérique *PYBFLASH*.<br>
Editez maintenant le script *main.py* et collez-y le code qui suit :

```python
# Objet du script :
# Pilotage d'un module Grove anneau de LED RGB avec un module Grove potentiomètre
# Lorsqu'on tourne le potentiomètre, les LED s'allument progressivement.

import neopixel # Pilote pour l'anneau de LED RGB
from machine import Pin
from time import sleep_ms # Pour temporiser

# Potentiomètre sur A0
a0 = pyb.ADC('A0')

_NB_LED = const(24) # 24 LED sur l'anneau

# On initialise l'anneau de LED sur la broche D2
ring = neopixel.NeoPixel(Pin('D2'), _NB_LED)

# Fonction pour allumer les LED de l'anneau jusqu'à une LED donnée
# d'indice max_led.
#@micropython.native # Décorateur pour que le code soit optimisé pour STM32
def light_ring(max_led):

	# De 0 à _NB_LED - 1
	for i in range(_NB_LED):
		
		if i <= max_led: # Pour toutes les LED d'indice inférieur à max_led
			ring[i] = (10, 10, 10) # Les allumer en blanc avec une faible intensité
		else: # Pour les autres LED
			ring[i] = (0, 0, 0) # Les éteindre 

	# Evnoyer les instruction à l'anneau
	ring.write()

# Fonction pour remapper un intervalle de valeurs dans un autre
#@micropython.native 
def map (value, from_min, from_max, to_min, to_max):
  return (value-from_min) * (to_max-to_min) / (from_max-from_min) + to_min


while True:
	
	# Convertit la lecture analogique du potentiomètre en un indice entre 0 et 23
	max_led = int(map(a0.read(), 0, 4045, 0, _NB_LED-1))
	
	# Allume les LED jusqu'à l'indice déterminé ci-avant
	light_ring(max_led)
	
	# Temporise 20 millisecondes
	sleep_ms(20)
```

## Manipulation

Dans votre émulateur de terminal série, appuyez sur *[CTRL]-[D]* pour lancer le script.<br>
Si tout s'est déroulé correctement, vous pourrez allumer ou éteindre les LED par une rotation de la molette du potentiomètre.

## Pour aller plus loin

Vous pouvez améliorer cet exemple en utilisant une **programmation asynchrone**, en gérant le potentiomètre et l'anneau de LED dans des coroutines exécutées en temps partagé, comme sur [cet autre tutoriel](paj760u2). 