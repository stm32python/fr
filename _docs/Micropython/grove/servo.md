---
title: Servomoteur
description: Mettre en œuvre un servomoteur avec MicroPython
---

# Servomoteur
Ce tutoriel explique comment mettre en œuvre un servomoteur avec MicroPython.

## Description
Le servomoteur (ou *servo*) est un type de moteur électrique. C'est un boitier contenant :

 - Une partie mécanique avec un moteur (souvent très petit) et des engrenages pour avoir une vitesse plus faible mais un couple plus important.
 - Une partie électronique avec un capteur agissant comme un potentiomètre dont la résistance varie en fonction de l'angle.

On le retrouve dans différents domaines, notamment industriel, pour ouvrir ou fermer des vannes mais aussi dans le modélisme, pour des voitures télécommandées par exemple.

**Servomoteur Grove :**

<br>
<div align="left">
<img alt="Grove - servomotor" src="images/grove-servo.jpg" width="350px">
</div>
<br>

> Crédit image : [Seeed Studio](http://wiki.seeedstudio.com/Grove_System/)

Un servomoteur est donc un moteur dont on contrôle l'angle de rotation, qui peut varier de 0 à 180°, en fonction du signal envoyé par la carte NUCLEO-WB55. Ce signal est de type [PWM](../../Kit/glossaire).


**Qu'est-ce que la PWM ?**

La _modulation en largeur d'impulsion (PWM pour "Pulse Width Modulation")_ est une méthode qui permet de simuler un signal analogique en utilisant une source numérique.
On génère un signal carré, de fréquence donnée, qui est à +3.3V (haut) pendant une proportion paramétrable de la période et à 0V (bas) le reste du temps. C’est un moyen de moduler l’énergie envoyée à un actuateur. Usages : commander des moteurs, faire varier l’intensité d’une LED, faire varier la fréquence d’un buzzer... La proportion de la durée d'une période pendant laquelle la tension est dans l'état "haut" est appelée _rapport cyclique_.

Dans notre cas la PWM est un signal périodique de 50Hz (soit 20ms) dont on fait varier le rapport cyclique entre 5% (1 ms / 20 ms) et 10% (2 ms / 20 ms) pour faire tourner le bras du servo de 0° à 180°, conformément aux schémas qui suivent :

<br>
<div align="left">
<img alt="Description PWM servomoteur" src="images/servomoteur-pwm.svg" width="400px">
</div>
<br>

> Crédit image : [Wikimedia](https://upload.wikimedia.org/wikipedia/commons/f/f6/TiemposServo.svg)

**Programmer des PWM sur la carte NUCLEO-WB55**

En pratique, la génération de signaux PWM est l’une des fonctions assurées par les _timers_, des composants intégrés dans le microcontrôleur qui se comportent comme des compteurs programmables. Le microcontrôleur de notre carte contient plusieurs timers, et chacun d'entre eux pilote plusieurs _canaux_ (_channels_ en anglais).<br>
Certains canaux de certains timers sont finalement connectés à des broches de sorties du microcontrôleur (GPIO). Ce sont ces broches là qui vont pouvoir être utilisées pour générer des signaux de commande PWM. Ainsi, dans notre exemple nous avons choisi la broche *D6* pour commander le servomoteur car elle est reliée au canal 1 du timer 1 (figure ci-après) qui fonctionne en mode génération de signal PWM.

Même si MicroPython permet de programmer facilement des broches en mode sortie PWM, vous aurez donc besoin de connaître :
 1. Quelles broches de la NUCLEO-WB55 sont des sorties PWM ;
 2. A quels timers et canaux sont connectées ces broches dans le STM32WB55.

La figure ci-dessous répond à ces deux questions :

<br>
<div align="left">
<img alt="PWM WB55" src="images/pwm_wb55.jpg" width="400px">
</div>
<br>

## Câblage d'un servomoteur

### **Cas 1 : Vous n'utilisez pas le module servomoteur du kit de base Grove**

Le servomoteur dispose d'une connectique avec des couleurs qui lui sont propres (mais qui sont standardisées). Il faut cependant bien vérifier la correspondance des couleurs en fonction des connecteurs. Dans notre cas nous les connectons de cette façon sur la carte :

<br>
<div align="left">
<img alt="Schéma de montage servomoteur" src="images/servomoteur-schema.png" width="700px">
</div>
<br>

La correspondance entre le servomoteur et la carte NUCLEO-WB55 est la suivante :

| Servomoteur       | Couleur du fil    | ST NUCLEO         |
| :-------------:   | :---------------: | :---------------: |
|       Signal      |      Orange       |         D6        |
|       GND         |      Marron       |         GND       |
|       5V          |      Rouge        |         5V        |

### **Cas 2 : Vous utilisez le module servomoteur du kit de base Grove**

Dans ce cas, branchez la carte d'extension de base Grove sur les connecteurs Arduino de votre NUCLEO-WB55 et le module servomoteur sur la fiche D6. Le câblage sera alors le même que celui indiqué au cas 1 ci-dessus, sauf pour l'alimentation qui passera de 3,3V à 5V selon la position de l'interrupteur de la carte d'extension de base Grove. Placez l'interrupteur sur la position 3,3V.

## Premier exemple : Programmer "en dur" les mouvements du servomoteur

Dans ce premier exemple, nous allons détailler un script qui permet de faire tourner le servomoteur selon trois angles (-90°, à° et +90°) déterminés par des valeurs de PWM décidées à l'avance.


### **Matériel requis**

- Une [carte NUCLEO-WB55](../../Kit/nucleo_wb55rg)
- Une [carte d'extension de base Grove](https://wiki.seeedstudio.com/Base_Shield_V2/)
- Un [module servomoteur analogique Grove](https://wiki.seeedstudio.com/Grove-Servo/)


### **Le script MicroPython**

> **Vous pouvez télécharger les scripts MicroPython de ce tutoriel (entre autres) en cliquant [ici](../../../assets/Script/MODULES.zip)**.


**Etape 1 :** Pour faire fonctionner le programme nous devons dans un premier temps importer la méthode de temporisation `sleep` ainsi que la classe `Pyb` pour accéder aux broches et aux compteurs (timers), de cette façon :

```python
from time import sleep # Pour temporiser
import pyb # Pour accéder  aux broches et aux compteurs (timers)
```

**Etape 2 :** Ensuite il faut vernier initialiser la patte sur laquelle est connecté le servomoteur. On configure également la PWM avec une fréquence de 50Hz.

```python
servo = pyb.Pin('D6')
tim_servo = pyb.Timer(1, freq=50)
```

**Etape 3 :** Il ne nous reste plus qu'à faire tourner le servo.<br>
 Pour cela on crée une boucle infinie qui fait pivoter le servo de 90° toutes les cinq secondes.

```python
# Objet du script :
# Piloter un servomoteur
# Exemple de valeurs de PWM pour le servomoteur Grove livré avec les kits 
# de base pour Arduino.

from time import sleep # Pour temporiser
import pyb # Pour accéder  aux broches et aux compteurs (timers)

servo = pyb.Pin('D6')

Timer_Num = 1 # Numéro du timer
Timer_Cha = 1 # Canal du timer
Timer_Freq = 50 # Fréquence du timer (en Hz)

while True:

	print("Servomoteur à 0 degrés")
	tim_servo = pyb.Timer(Timer_Num, freq=Timer_Freq) # Démarrage du timer
	tim_servo.channel(Timer_Cha, pyb.Timer.PWM, pin=servo, pulse_width_percent=5)	# Servomoteur à 0 degrés
	sleep(5) # temporisation de 5 secondes

	print("Servomoteur à 90 degrés")
	tim_servo = pyb.Timer(Timer_Num, freq=Timer_Freq) # Démarrage du timer
	tim_servo.channel(Timer_Cha, pyb.Timer.PWM, pin=servo, pulse_width_percent=2)	# Servomoteur à 90 degrés
	sleep(5) # temporisation de 5 secondes
	
	print("Servomoteur à -90 degrés")
	tim_servo = pyb.Timer(Timer_Num, freq=Timer_Freq) # Démarrage du timer
	tim_servo.channel(Timer_Cha, pyb.Timer.PWM, pin=servo, pulse_width_percent=9.5)	# Servomoteur à -90 degrés
	sleep(5) # temporisation de 5 secondes

tim_servo.deinit() # Arrêt du timer
```

Sans doute à cause d'une implémentation limitée des PWM du STM32WB55 en MicroPython, il est nécessaire de démarrer et d'arrêter le timer après chaque changement de rapport cyclique (*pulse_width_percent*). En principe, la ligne  `tim_servo = pyb.Timer(Timer_Num, freq=Timer_Freq)` ne devrait donc figurer qu'une seule fois dans le code, avant `while True:`.

Notez également que les valeurs de largeur d'impulsions qui fixent le rapport cyclique et donc, l'angle de rotation du moteur, peuvent être différentes selon les moteurs ou les situations. Par exemple, avec le servo Grove on peut aussi utiliser :

 - `pulse_width_percent=12.5 # Servomoteur à 90 degrés`
 - `pulse_width_percent=7.5 # Servomoteur à 0 degré`
 - `pulse_width_percent=3 # Servomoteur à -90 degrés`

Le zéro degrés ne sera pas placé dans la même direction, mais les angles décrits seront respectés.
Il est aussi possible de balayer tous les angles intermédiaires en faisant varier *pulse_width_percent* entre 3 et 12,5.

**Remarque** : Le programme n'exécutera jamais (en l'absence de bogue tout du moins...) la dernière ligne ```tim_servo.deinit() # Arrêt du timer du servomoteur```.
Mais elle figure ici afin que vous ayez connaissance de la méthode ```deinit()``` spécifique aux timers, destinée à les stopper.

### **Résultat**

Après avoir sauvegardé et redémarré le script sur votre carte NUCLEO-WB55 vous pourrez voir le servomoteur tourner de 90° toutes les deux secondes.
Pour aller plus loin vous pouvez également utiliser le joystick d'une manette pour contrôler le servomoteur comme expliqué [dans le tutoriel sur le module Grove adaptateur de la manette Nintendo NunChuk](nunchuk).


## Deuxième exemple : Commander le servomoteur avec un potentiomètre

Dans ce deuxième exemple, il s'agit de contrôler l'angle de rotation d'un servomoteur à l'aide d'un [potentiomètre](potentiometre), ce qui permet de mettre en pratique une [conversion analogique-numérique (ADC)](https://fr.wikipedia.org/wiki/Convertisseur_analogique-num%C3%A9rique) et une [sortie modulée en largeur d'amplitude (PWM)](https://fr.wikipedia.org/wiki/Modulation_de_largeur_d%27impulsion).<br>
Pour créer ce code, [nous avons utilisé l'IDE en ligne de Vittascience pour la NUCLEO-WB55](https://fr.vittascience.com/wb55/?mode=mixed&console=bottom&toolbox=vittascience), puis nous lui avons apporté quelques modifications (traductions des commentaires essentiellement).


### **Matériel requis**

- Une [carte NUCLEO-WB55](../../Kit/nucleo_wb55rg)
- Une [carte d'extension de base Grove](https://wiki.seeedstudio.com/Base_Shield_V2/)
- Un [module servomoteur analogique Grove](https://wiki.seeedstudio.com/Grove-Servo/)
- Un [module potentiomètre Grove](https://wiki.seeedstudio.com/Grove-Rotary_Angle_Sensor/)


### **Le script MicroPython**

> **Vous pouvez télécharger les scripts MicroPython de ce tutoriel (entre autres) en cliquant [ici](../../../assets/Script/MODULES.zip)**.

```python
# Objet du script :
# Piloter un servomoteur avec un potentiomètre.
# Exemple de valeurs de PWM pour le servomoteur Grove livré avec les kits 
# de base pour Arduino.

import pyb
import machine

# Servomoteur sur D6
d6 = pyb.Pin('D6', pyb.Pin.OUT_PP)
tim_d6 = pyb.Timer(1, freq=50)
pwm_d6 = tim_d6.channel(1, pyb.Timer.PWM, pin=d6)

# Potentiomètre sur A0
a0 = pyb.ADC('A0')

# Convertit l'ange de rotation (en entrée) en une valeur de PWM puis l'envoie au servo.
def setServoAngle(timer, angle):
	if (angle >= 0 and angle <= 180):
		# pw_percent = 3 + angle * (12.5 - 3) / 180
		pw_percent = 3 + angle * 0.052777778
		timer.pulse_width_percent(pw_percent)
	else:
		raise ValueError("La commande d'angle du servomoteur doit être comprise entre 0° et 180°")

# Fonction pour remapper un intervalle de valeurs dans un autre
def map (value, from_min, from_max, to_min, to_max):
	return (value-from_min) * (to_max-to_min) / (from_max-from_min) + to_min

# Initialise le servo sur un angle de 90° 
setServoAngle(pwm_d6, 90)

while True:
	
	# Convertit la lecture analogique du potentiomètre en un angle entre 0° et 180°
	angle = int(map(a0.read(), 0, 4095, 0, 180))
	
	# Applique cet angle au servomoteur
	setServoAngle(pwm_d6, angle)
```

### **Résultat**

Après avoir sauvegardé et redémarré le script sur votre carte NUCLEO-WB55 vous pourrez faire tourner le servomoteur d'un angle arbitraire entre 0° et 180° à l'aide du potentiomètre.
