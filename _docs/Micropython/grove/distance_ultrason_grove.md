---
title: Sonar à ultrasons  HC-SR04 (Grove)
description: Mise en œuvre du module capteur de distance par ultrasons HC-04 Grove V2.0 en MicroPython
---

# Sonar à ultrasons Grove HC-SR04

Ce tutoriel explique comment mettre en œuvre le capteur de distance par ultrasons Grove V2.0 en MicroPython.
Ce module, basé sur le sonar HC-SR04 et désigné par *Ultrasonic Distance Sensor V2.0* ou *Ultrasonic Ranger V2.0* sur son PCB.

**Le capteur de distance à ultrasons Grove v2.0 :**

<br>
<div align="left">
<img alt="Grove - Ultrasonic" src="images/Grove-Ultrasonic-Distance-Sensor-pinout.jpg" width="500px">
</div>
<br>

> Crédit image : [Seeed Studio](http://wiki.seeedstudio.com/Grove_System/)

**NB** : Dans l'implémentation Grove du HC-SR04 la broche SIG (la broche qui renvoie le signal encodant la distance) et la broche TRIG (la broche de commande, qui lance un train d'impulsions sonores pour une mesure) ne font qu'une, pour des raisons de compatibilité avec les connecteurs Grove. Si vous achetez un module HC-SR04 générique, qui dispose d'un broche TRIG et d'une broche SIG distinctes, vous devrez modifier le script MicroPython présenté plus bas pour intégrer cette différence.

Le module émet des ultrasons et calcule le temps qu'ils mettent à revenir vers lui, ce qui lui permet de mesurer la distance le séparant d'un objet qui les a réfléchis, comprise entre 2 cm et 4 m (théoriquement). Il communique avec la carte NUCLEO-WB55 par une broche numérique SIG qui est également utilisée pour le commander. Plus précisément, son fonctionnement est résumé par la figure qui suit :

<br>
<div align="left">
<img alt="Grove - Ultrasonic" src="images/hc-sr04_principle.jpg" width="800px">
</div>
<br>

## Matériel requis et montage

1. Une [carte d'extension de base Grove](https://wiki.seeedstudio.com/Base_Shield_V2/)
2. La carte NUCLEO-WB55
3. Un [module Grove capteur à ultrasons V2.0](https://wiki.seeedstudio.com/Grove-Ultrasonic_Ranger/)

Connectez tout simplement le module à une broche numérique, *D8* dans notre cas.<br>
**Important :** Ce module est particulièrement sensible, ne le branchez ou débranchez jamais "à chaud", sur une carte sous tension.

## Le code MicroPython

> **Vous pouvez télécharger les scripts MicroPython de ce tutoriel (entre autres) en cliquant [ici](../../../assets/Script/MODULES.zip)**.

Pour créer ce code, [nous avons utilisé l'IDE en ligne de Vittascience pour la NUCLEO-WB55](https://fr.vittascience.com/wb55/?mode=mixed&console=bottom&toolbox=vittascience), puis nous lui avons apporté quelques modifications (traductions des commentaires essentiellement).

```python
# Objet du script :
# Mesurer une distance avec le capteur de distance par ultrasons Grove
# Affiche cette distance sur le port série USB_USER.
# Code généré à partir de l'interface de programmation par blocs de 
# Vittascience et retouché.

import machine
import utime

# Module sonar branché sur la broche d8
d8 = machine.Pin('D8', machine.Pin.OUT)

@micropython.native # Décorateur pour forcer une génération de code optimisé STM32
def grove_getUltrasonicData(pin, timeout_us=30000):

	# On éteint la broche de commande (SIG)
	pin.init(machine.Pin.OUT)
	pin.off()
	utime.sleep_us(2)
	# On allume la broche de commande pendant 10 µs
	pin.on()
	utime.sleep_us(10)
	# ... puis on l'éteint, ce qui fait démarrer l'émission 
	# d'ultrasons à 40 kHz par le module
	pin.off()
	pin.init(machine.Pin.IN)
	
	# Temps écoulé entre émission des ultrasons et réception de leur écho
	# la méthode time_pulse_us() compte la durée d'une impulsion en microsecondes sur 
	# la broche pin.
	duration = machine.time_pulse_us(pin, 1, timeout_us)/1e6
	if duration > 0:
		# Calcule la distance en centimètres à partir de la durée de l'impulsion 
		# renvoyée par le sonar :
		# distance de l'obstacle = 34000 cm/s (vitesse du son) x duration divisée par 2
		return round(17150 * duration)
	else:
		return -1

while True:
	
	# Mesure de la distance 
	distance = grove_getUltrasonicData(d8)

	if distance > 0:
		print("Distance : %d cm" %distance)
	else:
		print("Erreur de mesure")
	
	# Temporisation d'une seconde
	utime.sleep(1)
```

## Résultat

Après avoir sauvegardé et redémarré le script sur votre carte NUCLEO-WB55 vous pourrez voir les mesures de distance. En déplaçant la main ou un objet devant le capteur, on fait varier la mesure de distance :

```Console
Distance : 11 cm
Distance : 3 cm
Distance : 7 cm
Distance : 8 cm
Distance : 11 cm
Distance : 13 cm
Distance : 14 cm
Distance : 19 cm
Distance : 26 cm
Distance : 134 cm
Distance : 134 cm
```

Les éventuelles valeurs -1 correspondent à une erreur de mesure. Soit le capteur mesure une trop grande distance, soit l'objet est orienté de telle façon que le signal du capteur rebondit dessus mais est dirigé dans une autre direction et ne revient donc pas jusqu'au capteur, qui ne peut pas mesurer la distance.

## Pour aller plus loin

Pour aller plus loin,  vous pouvez adapter l'exemple *radar de recul* du [tutoriel sur le sonar Adafruit US100](distance_ultrason_us100) à notre module Grove.
