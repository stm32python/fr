---
title: Capteur d'inclinaison
description: Mise en œuvre d'un module capteur d'inclinaison Grove avec MicroPython
---

# Capteur d'inclinaison

Ce tutoriel explique comment mettre en œuvre un module capteur d'inclinaison Grove avec MicroPython. Le *capteur d’inclinaison* (*tilt-sensor* en anglais) change d’état lorsque son inclinaison par rapport à l'horizontale dépasse une valeur limite donnée. Il est constitué d'un tube cylindrique contenant une bille métallique. Lorsqu'il est incliné, la bille roule sous l'effet de la gravité et vient faire contact à l'une des extrémités du tube.

**Le capteur d'inclinaison Grove :**

<br>
<div align="left">
<img alt="Grove tilt sensor" src="images/tiltsensorim.png" width="200px">
</div>
<br>

> Crédit image : [Seeed Studio](http://wiki.seeedstudio.com/Grove_System/)

Ce capteur peut être sous deux états (nommés "Incliné" et "Horizontal" dans le code qui suit).

## Matériel requis et montage

1. Une [carte d'extension de base Grove](https://wiki.seeedstudio.com/Base_Shield_V2/)
2. La carte NUCLEO-WB55
3. Un [capteur d'inclinaison Grove](https://wiki.seeedstudio.com/Grove-Tilt_Switch/)

Branchez le sur le connecteur *D4* du Grove Base Shield.

## Le code MicroPython

> **Vous pouvez télécharger les scripts MicroPython de ce tutoriel (entre autres) en cliquant [ici](../../../assets/Script/MODULES.zip)**.

Editez à présent le fichier *main.py* sur le disque *PYBFLASH* et copiez-y le code qui suit : 

```python
# Objet : mise en œuvre d'un interrupteur à bille / capteur d'inclinaison

from time import sleep_ms # Pour la temporisation
from pyb import Pin

p_in = Pin('D4', Pin.IN, Pin.PULL_UP)

while True :

	sleep_ms(500) # Temporisation de 500 millisecondes

	etat = p_in.value() # Lecture du capteur, 0 si horizontal et 1 si incliné

	if etat:
		print("Incliné")
	else:
		print("Horizontal")
```

## Manipulation

En changeant l'inclinaison du capteur, vous devriez voir tour à tour les messages *"Incliné"* et *"Horizontal"* s'afficher sur le terminal série (celui de PuTTY ou un autre) que vous aurez connecté à l'USB User :

```console
MPY: sync filesystems
MPY: soft reboot
Incliné
Incliné
Incliné
Horizontal
Horizontal
Incliné
Incliné
Incliné
```
