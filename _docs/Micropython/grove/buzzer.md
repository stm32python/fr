---
title: Buzzer, Speaker et PWM
description: Mise en œuvre d'un module buzzer ou speaker Grove avec MicroPython et explication de la PWM
---

# Buzzer, speaker et PWM

Ce tutoriel explique comment mettre en œuvre un module buzzer Grove ou un module speaker Grove à l'aide d'un signal **modulé en largeur d'impulsion (PWM)**, dans un premier exercice puis en modulant rapidement l'état (haut/bast) d'une broches numérique.

**Le buzzer Grove et le speaker Grove :**

|Module buzzer Grove|Module speaker Grove|
|:-:|:-:|
|<img alt="Grove buzzer" src="images/buzzer.png" width="180px">| <img alt="Grove buzzer" src="images/speaker.jpg" width="250px">|

> Crédit image : [Seeed Studio](http://wiki.seeedstudio.com/Grove_System/)

## Prérequis

1. Une [carte d'extension de base Grove](https://wiki.seeedstudio.com/Base_Shield_V2/)
2. La carte NUCLEO-WB55
3. Un [module buzzer Grove](https://wiki.seeedstudio.com/Grove-Buzzer/) ou un [module speaker Grove](https://wiki.seeedstudio.com/Grove-Buzzer). Le deuxième est amplifié et restitue mieux les fréquences, mais nécessite en principe **une alimentation en 5V**. En pratique, il fonctionne correctement aussi en 3.3V.

Branchez le module choisi sur le connecteur *D6* du Grove base shield.

## Premier exemple : jouer un jingle avec un signal PWM

Le module vibre et produit un son lorsqu'on lui transmet une tension, il s'agit donc de moduler celle-ci. Il est possible de modifier la fréquence du son _en générant un signal de type PWM_ sur sa broche de commande.

### **Qu'est-ce que la PWM ?**

La _modulation en largeur d'impulsion (PWM pour "Pulse Width Modulation")_ permet de simuler un signal analogique en utilisant une source numérique.
On génère un signal carré, de fréquence donnée, qui est à +3.3V (haut) pendant une durée paramétrable de la période et à 0V (bas) le reste du temps. C’est un moyen de moduler l’énergie envoyée à un périphérique. Usages : commander des moteurs, faire varier l’intensité d’une LED, faire varier la fréquence d’un buzzer... La proportion de la période pendant laquelle la tension est dans l'état "haut" est appelée _rapport cyclique_. La figure ci-après montre trois signaux PWM avec des rapports cycliques de 25%, 50% et 75% (de bas en haut).

<br>
<div align="left">
<img alt="Grove buzzer" src="images/pwm.png" width="700px">
</div>
<br>

**Programmer des PWM sur la carte NUCLEO-WB55**

En pratique, la génération de signaux PWM est l’une des fonctions assurées par les _timers_, des composants intégrés dans le microcontrôleur qui se comportent comme des compteurs programmables. Le STM32WB55 contient plusieurs timers, et chacun d'entre eux pilote plusieurs _canaux_ (ou _channels_ en anglais). Certains canaux de certains timers sont finalement connectés à des broches de sortie du microcontrôleur (GPIO). Ces broches là pourront être utilisées pour générer des signaux de commande PWM.

 MicroPython permet de programmer facilement des broches en mode sortie PWM, mais vous aurez cependant besoin de connaitre :
 1. Quelles broches de la NUCLEO-WB55 sont des sorties PWM possibles ;
 2. A quels timers et canaux sont connectées ces broches dans le STM32WB55.
 
 La figure et la table ci-dessous fournissent ces deux informations :

<br>
<div align="left">
<img alt="PWM WB55" src="images/pwm_wb55.jpg" width="420px">
<img alt="PWM WB55" src="images/pwm_wb55_table.jpg" width="234px">
</div>
<br>

### **Le code MicroPython**

> **Vous pouvez télécharger les scripts MicroPython de ce tutoriel (entre autres) en cliquant [ici](../../../assets/Script/MODULES.zip)**.

Le code consiste à faire lire "en boucle" au buzzer un tableau *frequency* encodant une gamme de notes musicales. Le canal et le timer de la broche PWM *D6* utilisée sont identifiés à partir du tableau ci-dessus.

```python
# Objet du script : 
# Jouer un jingle sur un buzzer/speaker (Grove ou autre).
# Cet exemple fait la démonstration de l'usage de la PWM pour la broche D6 sur laquelle est 
# branché le buzzer/speaker.

import pyb

# Liste des fréquences des notes qui seront jouées par le buzzer/speaker
frequency = [262, 294, 330, 349, 370, 392, 440, 494]

# Liste des rapports cycliques (énergie injectée) pour moduler la
# puissance du buzzer/speaker (pourcentages de la période).
percent = [5, 7, 9, 11, 13, 15, 17, 19]

# D6 génère une PWM avec TIM1, CH1
d6 = pyb.Pin('D6', pyb.Pin.OUT_PP) 

# Gestion d'erreurs, pour s'assurer que le buzzer/speaker sera éteint correctement si l'utilisateur
# interromp le script avec *[CTRL]-[C]*.
try:
	while True :
		# Itération entre 0 et 7
		for i in range (0,7) :
			# On ajuste la fréquence pendant l'itération
			tim1 = pyb.Timer(1, freq=frequency[i])
			pwm = tim1.channel(1, pyb.Timer.PWM, pin=d6)
			# Rapport cyclique 
			pwm.pulse_width_percent(percent[i])
			
# En cas d'interruption clavier avec *[CTRL]-[C]*
except KeyboardInterrupt:
	pwm.pulse_width_percent(0) # Arrêt de la génération PWM
	tim1.deinit() # Arrêt du timer
```

### **Résultat**

Lancez le script avec *[CTRL]-[D]* dans la console du terminal série connectée à l'invite REPL MicroPython, via l'USB_USER. Le module devrait émettre, en boucle, un son modulé (pas très agréable ...).

## Deuxième exemple : jouer un jingle avec une GPIO numérique 

Ce deuxième exemple montre comment jouer une partie de la musique du film *Pirates des Caraïbes* en modulant rapidement l'état (haut/bas) de la broche sur laquelle est connecté le buzzer/speaker. **Le script a d'abord été généré automatiquement à partir [de l'IDE de Vittascience pour la NUCLEO-WB55](https://fr.vittascience.com/wb55/?mode=blocks&console=bottom&toolbox=vittascience)** puis modifié et commenté.

### **Le code MicroPython**

> **Vous pouvez télécharger les scripts MicroPython de ce tutoriel (entre autres) en cliquant [ici](../../../assets/Script/MODULES.zip)**.

```python
# Objet du script : 
# Jouer un jingle sur un buzzer/speaker (Grove ou autre) d'après l'exemple
# "Jouer la musique Pirates des Caraïbles" de l'IDE Vittascience.
# Cet exemple fait la démonstration de l'usage de la GPIO D6, sur laquelle est 
# branché le buzzer/speaker, 

import machine
import utime

# Buzzer connecté sur la broche D6
d6 = machine.Pin('D6', machine.Pin.OUT)

# Fréquences et durées des différentes notes musicales

FREQUENCIES_1 = [330, 392, 440, 440, 0, 440, 494, 523, 523, 0, 523, 587, 494, 494, 0, 440, 392, 440, 0]

DURATIONS_1 = [125, 125, 250, 125, 125, 125, 125, 250, 125, 125, 125, 125, 250, 125, 125, 125, 125, 375, 125]

FREQUENCIES_2 = [330, 392, 440, 440, 0, 440, 523, 587, 587, 0, 587, 659, 698, 698, 0, 659, 587, 659, 440, 0, 440, 494, 523, 523, 0, 587, 659, 440, 0, 440, 523, 494, 494, 0, 523, 440, 494, 0]

DURATIONS_2 = [125, 125, 250, 125, 125, 125, 125, 250, 125, 125, 125, 125, 250, 125, 125, 125, 125, 125, 250, 125, 125, 125, 250, 125, 125, 250, 125, 250, 125, 125, 125, 250, 125, 125, 125, 125, 375, 375]

# Simule la fréquence de la note en inversant très rapidement l'état de D6 pendant sa durée
@micropython.native # Décorateur pour forcer la génération d'un code assembleur pour STM32 (plus rapide)
def pitch (pin, noteFrequency, noteDuration, silence_ms = 10):
	if noteFrequency is not 0:
	
		microsecondsPerWave = 1e6 / noteFrequency
		millisecondsPerCycle = 1000 / (microsecondsPerWave * 2)
		loopTime = noteDuration * millisecondsPerCycle

		for x in range(loopTime):
			pin.high()
			utime.sleep_us(int(microsecondsPerWave))
			pin.low()
			utime.sleep_us(int(microsecondsPerWave))

	else:
		utime.sleep_ms(noteDuration)
	utime.sleep_ms(silence_ms)

# Joue en boucle la mélodie
while True:

	# Répète trois fois ...
	for j in range(2):
		# Joue une fois les notes de la liste NOTES_1
		for i in range(len(FREQUENCIES_1)):
			pitch(d6, FREQUENCIES_1[i], DURATIONS_1[i])

	# Joue une fois les notes de la liste NOTES_2
	for k in range(len(FREQUENCIES_2)):
		pitch(d6, FREQUENCIES_1[k], DURATIONS_2[k])
```

### **Résultat**

Lancez le script avec *[CTRL]-[D]* dans la console du terminal série connectée à l'invite REPL MicroPython, via l'USB_USER. Le module devrait émettre, en boucle, le début de la mélodie bien connue du film "Pirates des Caraïbes". 

### **Pour Aller plus loin**

Si vous souhaitez pousser plus loin votre étude de cette solution pour encoder de la musique avec MicroPython, sachez que d'autres mélodies sont disponibles via le générateur de code de l'IDE Vittascience ("Star Wars", "Gamme" et "R2D2").

