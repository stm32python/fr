---
title: Horloge temps-réel DS1307
description: Mise en œuvre du module Grove Horloge temps-réel DS1307 en MicroPython
---

# L'horloge temps-réel DS1307

Ce tutoriel explique comment mettre en œuvre un [module I2C Grove horloge temps-réel DS1307](https://wiki.seeedstudio.com/Grove-RTC/) en MicroPython. La fiche technique du composant qui assure la fonction RTC, le DS1307 de Maxim Integrated, [est disponible ici](https://datasheets.maximintegrated.com/en/ds/DS1307.pdf).

**Le module Grove RTC :**

<div align="left">
<img alt="Grove - RTC (DS1307)" src="images/DS1307.jpg" width="400px">
</div>

> Crédit image : [Seeed Studio](http://wiki.seeedstudio.com/Grove_System/)

Ce module RTC (pour "Real-Time Clock" en anglais) remplis la fonction d’une horloge calendrier pour des problématiques d’horodatage. Son pilote se concentre sur l'essentiel ; il offre moins de fonctions que celui de la RTC intégrée au STM32WB55, [présentée ici](https://stm32python.gitlab.io/fr/docs/Micropython/startwb55/rtc) mais il est plus facile d'usage pour des projets simples. Il est aussi équipé de sa propre pile bouton ce qui permet, une fois qu'on l'a mis à l'heure et à la bonne date, de conserver le suivi du temps lorsque la NUCLEO-WB55 est déconnectée de son alimentation. 


## Matériel requis

1. Une [carte d'extension de base Grove](https://wiki.seeedstudio.com/Base_Shield_V2/) avec son commutateur d'alimentation **positionné sur 5V**.
2. La carte NUCLEO-WB55
3. Un [module Grove horloge temps-réel DS1307](https://wiki.seeedstudio.com/Grove-RTC/)
4. Une pile bouton 5V Lithium CR 1225 (pour le module RTC)


## Le code MicroPython

> **Vous pouvez télécharger les scripts MicroPython de ce tutoriel (entre autres) en cliquant [ici](../../../assets/Script/MODULES.zip)**.

Il faut ajouter le fichier *ds1307.py* dans le répertoire du périphérique *PYBFLASH*.<br>
Editez maintenant le script *main.py* :

```python
# Objet du script :
# Démonstration de la mise en œuvre d'un module RTC utilisant un composant
# DS1307 de Maxim Integrated (fiche technique : https://datasheets.maximintegrated.com/en/ds/DS1307.pdf)
# Pilote adapté à partir de la source à l'adresse suivante :
# https://GitHub.com/mcauser/MicroPython-tinyrtc-i2c/blob/master/ds1307.py
# ATTENTION, pour fonctionner correctement ce module doit être alimenté en 5V

from ds1307 import DS1307 # Pilote du ds1307
from machine import I2C # Pilote du bus I2C
from time import sleep # Pour temporiser

# On utilise l'I2C n°1 de la carte NUCLEO-WB55 pour communiquer avec le capteur
i2c = I2C(1) 

# Pause d'une seconde pour laisser à l'I2C le temps de s'initialiser
sleep(1)

# On crée une instance de la RTC
ds = DS1307(i2c)

# On fixe la date au 26 aout 2021 (4eme jour de la semaine)
# On fixe l'heure à 23heures 59min 0sec
ds.setDateTime([2021, 8, 26, 4, 23, 59, 58, 0])

# On affiche pendant une minute, toutes les secondes, l'heure et la date
for i in range(60):
	print(ds.readDateTime())
	sleep(1)

# On arrète l'horloge 
ds.halt(True)
```

## Sortie sur le port série de l'USB USER

**N'oubliez pas de positionner le commutateur d'alimentation du Grove base shield sur +5V.**
Appuyez sur *[CTRL]-[D]* dans le terminal PuTTY et observez les valeurs qui défilent :

<br>
<div align="left">
<img alt="Grove - RTC sortie" src="images/grove_rtc_output.png" width="400px">
</div>
<br>

## Paramétrage en mode REPL

Pour le paramétrage de cette RTC, le mode REPL de l'interpréteur MicroPython est particulièrement adapté. Il vous sera plus facile de régler l'horloge avec une précision d'une seconde en mode REPL plutôt qu'en exécutant le script ci-dessus.
La copie d'écran ci-dessous illustre la mise à l'heure de la RTC en mode REPL via le terminal PuTTY et son interrogation par la suite pour s'assurer qu'elle fonctionne correctement :

<br>
<div align="left">
<img alt="Grove - RTC REPL" src="images/grove_rtc_repl.png" width="450px">
</div>
<br>

**Note :** En mode REPL, lorsque vous tapez "rtc." puis TAB, le terminal affiche toutes les méthodes disponibles pour la classe correspondante.
