---
title: Capteurs de température et d'humidité DHT11 et DH22
description: Mise en œuvre des capteurs de température et d'humidité DHT11 et DHT22 (Grove et autres) en MicroPython
---

# Capteurs de température et d'humidité DHT11 et DHT22

Ce tutoriel explique comment mettre en œuvre des capteurs de température et d'humidité relative DHT11 et DHT22 (Grove et autres) en MicroPython. Ces capteurs sont très peu onéreux. Le DHT22, tout du moins dans l'implémentation en module Grove que nous avons testée, donne des mesures très précises et fiables d'humidité et de température. **Notez bien que le DHT11 n'est pas supposé être capable de mesurer des températures inférieures à 0°C**.<br>
Les figures ci-dessous montrent [un module Grove intégrant un DHT11](https://wiki.seeedstudio.com/Grove-TemperatureAndHumidity_Sensor/) (à gauche) et [un module Grove intégrant un DHT22](https://wiki.seeedstudio.com/Grove-Temperature_and_Humidity_Sensor_Pro/) (à droite).

**Les modules Grove capteurs de température et humidité DHT :**

|Module Grove DHT11|Module Grove DHT22|
|:-:|:-:|
|<img alt="Grove - Temperature&Humidity Sensor(DHT11)" src="images/Grove-DHT11.jpg" width="200px">| <img alt="Grove - Temperature&Humidity Sensor Pro(DHT22)" src="images/Grove-DHT22.jpg" width="200px">|

> Crédit images : [Seeed Studio](http://wiki.seeedstudio.com/Grove_System/)

Ces capteurs utilisent **un protocole de communication qui leur est propre** et qui ne nécessite que trois fils : une masse, une alimentation et **une seule** ligne de communication. Ce protocole implique des mesures de fronts de signaux avec une précision de quelques millisecondes, difficiles à respecter avec du code MicroPython sur des microcontrôleurs cadencés à de "petites" fréquences, c'est pourquoi une bibliothèque pré-compilée est intégrée dans le firmware. c'est pourquoi, **à partir de la version 1.20 du firmware MicroPython**, vous trouverez dans le module *machine* la méthode *dht_readinto* spécialement dédié aux capteurs DHT.

## Matériel requis et montage

1. Une [carte d'extension de base Grove](https://wiki.seeedstudio.com/Base_Shield_V2/)
2. La carte NUCLEO-WB55
3. Un [module Grove  Temperature & Humidity Sensor Pro (DHT22)](https://fr.vittascience.com/shop/218/capteur-de-t%C2%B0-et-d)

Branchez le module sur le connecteur *D2* de la carte d'extension de base Grove.

## Le code MicroPython

> **Vous pouvez télécharger les scripts MicroPython de ce tutoriel (entre autres) en cliquant [ici](../../../assets/Script/MODULES.zip)**.

**Attention !**<br>
Si vous utilisez la version 1.20 du firmware MicroPython (ou une version ultérieure), les scripts distribués dans notre archive fonctionneront tels quels. Pour des versions antérieures, vous devrez remplacer, dans le fichier *dht.py*, la ligne *from machine import dht_readinto* doit être remplacée par *import dht_readinto*.

Il faut ajouter le fichier *dht.py* dans le répertoire du périphérique *PYBFLASH*. Editez maintenant le script *main.py* et copiez-y le code qui suit :

```python
# Objet du script : utiliser un capteur d'humidité et de température
# de la famille DHT (DHT11 ou DHT22).

import dht # Pour gérer les DHT11 et DHT22
from time import sleep_ms # Pour temporiser

# Instanciation du DHT22
sensor = dht.DHT22('D2')

# Pour un DHT11, utilisez simplement cette syntaxe :
# sensor = dht.DHT11('D2')

while True:

	# Structure de gestion des exceptions
	try:
		
		# On mesure et on lit les résultats
		sensor.measure()
		temp = sensor.temperature()
		humi = sensor.humidity()
		
		# Si les deux mesures renvoient 0 simultanément
		if humi == 0 and temp == 0:
			raise ValueError("Erreur capteur")

		# Formattage (arrondis) des mesures
		temperature = round(temp,1)
		humidity = int(humi)

		# Affichage des mesures
		print('=' * 40) # Imprime une ligne de séparation
		print("Température : " + str(temperature) + " °C")
		print("Humidité relative : " + str(humidity) + " %")

	# Si une exception est capturée
	except Exception as e:
		print(str(e) + '\n')

	# Temporisation de deux secondes
	sleep_ms(2000)
```

## Affichage sur le terminal série de l'USB User

Une fois le script lancé avec *[CTRL]-[D]*, vous pourrez observer les valeurs d'humidité et de température qui défilent :

<br>
<div align="left">
<img alt="One Wire output" src="images/DHT22_output.png" width="650px">
</div>
<br>

# Pour aller plus loin

> **Vous pouvez télécharger les scripts MicroPython de ce tutoriel (entre autres) en cliquant [ici](../../../assets/Script/MODULES.zip)**.

Si le protocole de communication des capteurs DHTxx vous intéresse, vous trouverez [une explication (pour Arduino) à ce sujet ici](https://www.carnetdumaker.net/articles/utiliser-un-capteur-de-temperature-et-dhumidite-dht11-dht22-avec-une-carte-arduino-genuino/) et [une implémentation explicite en MicroPython à base de timers pour le DHT22 ici](https://GitHub.com/kurik/uPython-DHT22).<br>

Ce deuxième exemple fonctionne correctement depuis la révision 1.17 du firmware MicroPython pour la NUCLEO-WB55 mais pas avec certaines révisions antérieures.

- Tout d'abord **la bibliothèque *dht22.py*** :

 ```python
# Exemple adapté de https://github.com/kurik/uPython-DHT22/blob/master/main.py
# Construction d'un pilote pour le DHT22 en utilisant un timer pour compter les
# fronts (descendants) des signaux.

from time import sleep_ms # Pour temporiser
from pyb import ExtInt, Pin, Timer # Pour gérer les broches, les interruptions des broches et les Timers.

# Nous devons utiliser ici des variables globales car toute allocation de variable locale
# entrainerait un délai qui planterait la communication.

data = None
timer = None
micros = None

FALL_EDGES = const(42) # La réception comporte 42 fronts descendants

times = list(range(FALL_EDGES))
index = 0

# Le gestionnaire d'interruptions : capture la réponse du capteur après lee START
@micropython.native
def edge(line):

	global index
	global times
	global micros

	times[index] = micros.counter()
	if index < (FALL_EDGES - 1): # Pour éviter un dépassement de buffer s'il y a du bruit sur la ligne
		index += 1

# Initialisation du capteur
@micropython.native
def init(timer_id = 2, data_pin = 'D2'):

	global data
	global micros
	
	# Broche de la ligne de communication
	data = Pin(data_pin)
	
	# Identifiant du timer sélectionné
	timer = timer_id
	
	# Paramètres pour un timer de fréquence 1 microseconde
	micros = Timer(timer, prescaler=65, period=0x3fffffff)

	# Gestionnaire de l'interruption du timer
	ExtInt(data, ExtInt.IRQ_FALLING, Pin.PULL_UP, edge)

@micropython.native
def do_measurement():

	global data
	global micros
	global index

	# Envoie la commande START
	data.init(Pin.OUT_PP)
	data.low()
	micros.counter(0)
	while micros.counter() < 25000:
		pass
		
	# Passe la broche en IN
	data.high()
	micros.counter(0)
	index = 0
	data.init(Pin.IN, Pin.PULL_UP)

	# Après 5 millisecondes, la mesure doit être terminée
	sleep_ms(5)
	
# Lis les données renvoyées par le capteur
@micropython.native
def process_data():
	global times
	i = 2 # On ignore les deux premiers fronts descendants qui sont la réponse à la commande START
	result_i = 0
	result = list([0, 0, 0, 0, 0])
	
	while i < FALL_EDGES:
		result[result_i] <<= 1
		if times[i] - times[i - 1] > 100:
			result[result_i] += 1
		if (i % 8) == 1:
			result_i += 1
		i += 1
		
	[int_rh, dec_rh, int_t, dec_t, csum] = result
	humidity = ((int_rh * 256) + dec_rh)/10
	temperature = (((int_t & 0x7F) * 256) + dec_t)/10
	if (int_t & 0x80) > 0:
		temperature *= -1
	comp_sum = int_rh + dec_rh + int_t + dec_t
	if (comp_sum & 0xFF) != csum:
		raise ValueError('La somme de contrôle est incorrecte')
	return (humidity, temperature)

@micropython.native
def measure():
	do_measurement()
	if index != (FALL_EDGES -1):
		raise ValueError('Echec du transfert de données : %s fronts descendants seulement' % str(index))
	return process_data()
 ```

 - Ensuite **le fichier *main.py*** :

 ```python
# Objet du script : utiliser un capteur d'humidité et de température
# de la famille DHT (DHT11 ou DHT22).

import dht # Pour gérer les DHT11 et DHT22
from time import sleep_ms # Pour temporiser

# Instanciation du DHT22
sensor = dht.DHT22('D2')

# Pour un DHT11, utilisez simplement cette syntaxe :
# sensor = dht.DHT11('D2')

while True:

	# Structure de gestion des exceptions
	try:
		
		# On mesure et on lit les résultats
		sensor.measure()
		temp = sensor.temperature()
		humi = sensor.humidity()
		
		# Si les deux mesures renvoient 0 simultanément
		if humi == 0 and temp == 0:
			raise ValueError("Erreur capteur")

		# Formattage (arrondis) des mesures
		temperature = round(temp,1)
		humidity = int(humi)

		# Affichage des mesures
		print('=' * 40) # Imprime une ligne de séparation
		print("Température : " + str(temperature) + " °C")
		print("Humidité relative : " + str(humidity) + " %")

	# Si une exception est capturée
	except Exception as e:
		print(str(e) + '\n')

	# Temporisation de deux secondes
	sleep_ms(2000)
```
