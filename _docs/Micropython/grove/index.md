---
title: Tutoriels avec des modules externes (Grove et autres) et projets
description: Tutoriels avec des modules (Grove et autres) et projets
---

# Tutoriels avec des modules(Grove et autres) et projets

## Démarrage

Pour la plupart de ces tutoriels, vous devrez disposer de la [*carte d’extension Grove pour Arduino*](https://wiki.seeedstudio.com/Base_Shield_V2/) (ou *Grove Base Shield for Arduino* en anglais).
C'est une carte au format Arduino qui vient se connecter sur la NUCLEO-WB55 et permet de brancher aisément des capteurs et des actionneurs digitaux, analogiques, UART et I2C avec la connectique propriétaire [Grove](http://wiki.seeedstudio.com/Grove_System/).

<br>
<div align="left">
<img alt="Grove base shield pour Arduino" src="images/grove_base_shield.jpg" width="670px">
</div>
<br>

> Crédit images : [Seeedstudio](http://wiki.seeedstudio.com/Grove_System/)

Elle offre :
* Six connecteurs D2, D3... pour des périphériques numériques c'est à dire pilotés par un niveau logique 0 (0V) ou 1 (+3.3V) ;
* Quatre connecteurs A0, A1... pour des périphériques qui envoient un signal analogique en entrée (entre 0 et +3.3V ou 0 et +5V selon la position du petit interrupteur qui l'équipe) ;
* Quatre connecteurs pour des périphériques dialoguant avec le protocole I2C ;
* Un connecteur pour un port série (UART) ;
* Un commutateur 3,3V / 5V qui permet de sélectionner la tension d'alimentation des modules que vous connecterez sur ses fiches.

## La NUCLEO-WB55 n'aime pas les modules 5V !

**Prenez garde aux tensions d'alimentation des modules externes que vous connectez sur la NUCLEO-WB55 !** Certains nécessitent du 5V, d'autres fonctionnent aussi bien avec du 5V et du 3.3V, d'autres enfin fonctionnent exclusivement en 3,3V et seront peut-être endommagés par du 5V. **Lisez bien leurs fiches techniques avant de les brancher !** <br>
Nous vous recommandons également de prendre connaissance des [**précautions d'utilisations des cartes NUCLEO exposées dans ce tutoriel**](../../Kit/nucleo).

D'une façon générale, **la plus grande attention** est requise si vous souhaitez utiliser des périphériques fonctionnant exclusivement en 5V avec la NUCLEO-WB55. 
- S'il s'agit de capteurs qui injectent du courant dans le microcontrôleur, **ils pourraient purement et simplement détruire celui-ci**. Donc, si vous ne maîtrisez pas le sujet, nous vous conseillons vivement de **ne pas en utiliser du tout**.
- S'il s'agit de périphériques en logique 5V **pilotés** par le STM32, ils devraient fonctionner correctement.

Il est à noter que la plupart des modules actuels sont vendus pour être alimentés en 5V ou en 3,3V (grâce à un régulateur de tension). Ils ne posent bien sûr aucun problème.

>> **NB :** Parmi les périphériques qui nécessitent d'être alimentés **exclusivement** en 5V sur la NUCLEO-WB55, ci-dessous, figure en particulier [le module LCD RGB Grove](lcd_RGB_16x2) qui apporte **une autre difficulté** : il ne dispose pas des résistances de tirage (pull-up) requises par le bus I2C auquel il est connecté et ne fonctionnera pas si ces résistances ne sont pas ajoutées par vos soins. Ceci est expliqué dans sa fiche tutoriel.

## Précision importante concernant l'adressage sur le bus I2C

Vous constaterez que la plus grande partie des modules présentés ci-après utilisent **le protocole I2C** et doivent donc être connectés au bus du même nom sur la carte NUCLEO-WB55. I2C est l'acronyme de "Inter-Integrated Circuit" (en français : bus de communication intégré inter-circuits). Il s'agit d'un bus série fonctionnant selon un protocole inventé par Philips. Pour dialoguer sur un bus I2C, chaque module "esclave" qui s'y trouve branché **est identifié par une adresse codée sur 7 bits** afin de dialoguer avec le contrôleur maître intégré au STM32WB55.

Avec la prolifération des modules I2C, **il pourrait arriver que deux ou plusieurs modules que vous auriez connectés sur un bus I2C aient la même adresse**, ce qui conduirait inévitablement à des plantages. Pour éviter ce type de conflits, vous devrez consulter les fiches techniques de vos modules et, pour ceux qui le permettent, prendre soin de modifier (si nécessaire) leur adresse I2C, généralement codée dans leur firmware.

Les tableaux ci-dessous rappellent, entre parenthèses, dans la colonne *Protocole*, l'adresse I2C par défaut - celle qui est écrite dans la classe pilote - des différents modules que nous avons utilisés. Si vous choisissez d'autres modules, il est possible que leur adresse change et que vous soyez obligé de la passer en paramètre lors de l'instanciation de leur pilote. Ce sera parfois le cas, par exemple, selon que vous utiliserez un même capteur implémenté dans un module Grove de Seeed Studio ou dans un module Adafruit.

## Liste des tutoriels 

Vous trouverez ici quelques tutoriels essentiellement [pour des modules au format Grove de la société Seeed Studio](http://wiki.seeedstudio.com/Grove_System/). 
Ils se transposeront facilement à des modules d'autres fabricants pour peu qu'ils utilisent les mêmes protocoles et bus. 

Nous aborderons et expliquerons au gré des tutoriels des notions de programmation embarquée et d'architecture des microcontrôleurs qui vous seront indispensables pour progresser. Lorsque vous rencontrerez un acronyme que vous ne connaissez pas, n'hésitez pas à consulter le [glossaire](../../Kit/glossaire).

**1. Afficheurs & LED**

|**Tutoriel**|**Protocole**|
|:-|:-:|
|[Afficheur 4x7 segments TM1637](tm1637)|spécifique|
|[Afficheur 8x7 segments TM1638](tm1638)|spécifique|
|[Afficheur Grove LCD 16 caractères x 2 lignes](lcd_16x2)|I2C (0x3E)|
|[Afficheur Grove LCD RGB 16 caractères x 2 lignes](lcd_RGB_16x2)|I2C (rétroéclairage : 0x62, afficheur : 0x3E)|
|[Afficheurs Grove OLED 0,96" et 1.12"](oled_1308)|I2C (0x3C)|
|[Anneau de LED RGB Neopixel](anneau_neopixel)|spécifique|
|[Barre de LED MY9221](ledbar_MY9221)|numérique|
|[LED infrarouge](infared_led)|numérique|
|[LED RGB chainable](led_rgb_chainable)|spécifique|
|[LED RGB adressable (Neopixel)](led_rgb_neopixel)|spécifique|
|[Matrice de LED 8x8 MAX7219](max7219)|SPI|
|[Ruban de LED RGB adressable (Neopixel)](ruban_neopixel)|spécifique|

**2. Capteurs spatiaux et de mouvement**

|**Tutoriel**|**Protocole**|
|:-|:-:|
|[Accéléromètre 3 axes MMA7660FC](accel_mma7660fc)|I2C (0x4C)|
|[Capteur de choc](knock)|numérique|
|[Capteur de gestes PAJ7620U2](paj760u2)|I2C (0x73)|
|[Capteur d'inclinaison](inclinaison)|numérique|
|[Centrale inertielle LSM303D](LSM303D)|I2C (0x1E)|
|[Détecteur de mouvement PIR](mouvement)|numérique|
|[Module suiveur de ligne](line_finder)|numérique|
|[Module Ultimate GPS Breakout (Adafruit)](gps_adafruit)|UART|
|[Modules GNSS Grove SIM28 & Air530](gps_grove)|UART|
|[Sonar à ultrasons HC-SR04 (Grove)](distance_ultrason_grove)|numérique|
|[Sonar à ultrasons US100 (Adafruit)](distance_ultrason_us100)|UART|
|[Télémètres infrarouge VL53L0X et VL53L1X](vl53l0x)|I2C (0x29)|

**3. Capteurs environnementaux**

|**Tutoriel**|**Protocole**|
|:-|:-:|
|[Capteur de dioxyde de carbone MH-Z16](mhz16)|UART|
|[Capteur de dioxyde de carbone SCD30](scd30)|I2C (0x61)|
|[Capteur de gaz multicanaux MiCS-6814](MICS6814)|I2C (0x04)|
|[Capteur de gouttes de pluie](raindrop)|analogique|
|[Capteurs d'humidité du sol](soil_moisture)|analogique|
|[Capteur de lumière numérique TSL2561 (module Grove)](tsl2561)|I2C (0x29)|
|[Capteur de lumière numérique TSL2591 (module Adafruit)](tsl2591)|I2C (0x29)|
|[Capteur de luminosité, photodiode](luminosite)|analogique|
|[Capteur de lumière solaire SI1145](SI1145)|I2C (0x60)|
|[Capteur de pression, température et humidité BME280](bme280)|I2C (0x76)|
|[Capteur de pression, température, humidité et qualité d'air BME680](bme680)|I2C (0x76)|
|[Capteur de pression et température BMP280](bmp280)|I2C (0x77)|
|[Capteur de pression intégré MPX5700AP](MPX5700AP)|analogique|
|[Capteur de qualité de l'air SGP30](sgp30)|I2C (0x58)|
|[Capteur de sons](bruit)|analogique|
|[Capteur de température et d'humidité SHT31](sht31)|I2C (0x44)
|[Capteur de température et d'humidité TH02](th02)|I2C (0x40)
|[Capteur de température de précision MCP9808](mcp9808)|I2C (0x18)|
|[Capteurs de température et d'humidité DHT11 et DHT22](DHT)|spécifique|
|[Capteur de température et d'humidité DHT20](DHT20)|I2C (0x38)|
|[Capteur de température, thermistance générique](thermistance)|analogique|
|[Capteur de température, module Grove thermistance](NCP18WF104F03RB)|analogique|
|[Compteur de particules HM3301](HM3301)|I2C (0x40)|
|[Détecteur d'eau](water_sensor)|analogique|
|[Sonomètre - Mesure de la puissance sonore en décibels](sound_level_meter)|analogique|
|[Sonde étanche de température DS18X20](ds1820)|spécifique|

**4. Enregistrement et saisie de données**

|**Tutoriel**|**Protocole**|
|:-|:-:|
|[Adaptateur Nintendo NunChuk](nunchuk)|I2C (0x52)|
|[Bouton et anti-rebond](bouton)|numérique|
|[Clavier matriciel](keypad)|numérique|
|[Interrupteur tactile](toucher)|numérique|
|[Joystick](joystick)|analogique|
|[Manette Nintendo SNES](snes)|SPI|
|[Matrice d'interrupteurs tactiles TTP226](TTP226)|numérique|
|[Module carte SD](sd_card_module)|SPI|
|[Potentiomètre (rotatif ou linéaire)](potentiometre)|analogique|

**5. Actuateurs**

|**Tutoriel**|**Protocole**|
|:-|:-:|
|[Buzzer](buzzer)|PWM|
|[Contrôle moteur avec un pont en H](moteur)|PWM|
|[Contrôle moteur avec un relais ou un transistor](relais_transistor)|numérique|
|[Contrôle moteur pas-à-pas](moteur_pas_a_pas)|numérique|
|[Servomoteur](servo)|PWM|
|[Speaker](buzzer)|PWM|

**6. Communication radiofréquence**

|**Tutoriel**|**Protocole**|
|:-|:-:|
|[Exercices avec le BLE sur la NUCLEO-WB55](../BLE/index)|NA|
|[Liaison maître - esclave avec des modules Bluetooth HC-05](master_slave_hc05)|UART|
|[Communication directe entre deux modules LoRa-E5](lora-e5-p2p)|UART|
|Communication directe entre deux modules NRF24L01 (à venir)|SPI|
|[Liaison RFID 125 kHz Grove](RFID125kHz)|UART|
|[Module RFID RC522 13.56 MHz](rfid)|SPI|
|[Publication LoRaWAN avec un module LoRa-E5](lora-e5)|UART|
|[REPL via un module Bluetooth Grove Serial V3.1](repl_bc417)|UART|

**7. Autres modules et petits projets**

|**Tutoriel**|**Protocole**|
|:-|:-:|
|[Alarme de mouvement](alarme)|PWM|
|[Emetteur - récepteur infrarouge de code Morse](morse)|numérique & analogique|
|[Générateur et analyseur de signaux, transformation de Fourier](dac_adc_dft)|analogique|
|[Horloge temps-réel DS1307](rtc_DS1307)|I2C (0x68)|
|[Horloge temps-réel PCF85063TP](rtc_PCF85063TP)|I2C (0x51)|
|[Station météo inspirée du produit Ikea Klockis](../../../assets/projects/station_meteo_klockis.zip)|I2C, SPI|
|[Variateur de lumière](variateur_lumiere)|PWM|