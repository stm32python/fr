---
title: Module suiveur de ligne
description: Mise en œuvre d'un module Grove suiveur de ligne avec MicroPython
---

# Module suiveur de ligne

Ce tutoriel explique comment mettre en œuvre un module Grove suiveur de ligne avec MicroPython. Le suiveur de ligne est un dispositif largement utilisé pour permettre à de petits robots roulants de se déplacer de façon autonome en suivant une ligne noire tracée sur un sol blanc. Le principe du module est la détection optique du contraste ; il est constitué d'une LED infrarouge et d'un phototransistor. Il envoie un signal digital haut lorsqu'il détecte une ligne noire sur un fond blanc.

**Le module Grove suiveur de ligne :**

<div align="left">
<img alt="PGrove - Line finder" src="images/Grovelinefinder1.jpg" width="300px">
</div>

> Crédit image : [Seeed Studio](http://wiki.seeedstudio.com/Grove_System/)

## Matériel requis

1. Une [carte d'extension de base Grove](https://wiki.seeedstudio.com/Base_Shield_V2/)
2. La carte NUCLEO-WB55
3. Un [module Grove suiveur de ligne](https://wiki.seeedstudio.com/Grove-Line_Finder/)

Le module suiveur de ligne fonctionne avec un port numérique ; nous l'avons connecté sur *D7* dans nos exemples.

## Le code MicroPython en "polling", réglage de la sensibilité du détecteur

Le script qui suit montre comment mettre en œuvre le module suiveur de ligne avec une boucle "infinie", en mode "scrutation" (ou "polling en anglais").

> **Vous pouvez télécharger les scripts MicroPython de ce tutoriel (entre autres) en cliquant [ici](../../../assets/Script/MODULES.zip)**.

Editez maintenant le script *main.py* du périphérique *PYBFLASH* :

```python
# Objet du script : Mettre en œuvre un module suiveur de ligne
# Source : https://GitHub.com/DexterInd/GrovePi/blob/master/Software/Python/grove_line_finder.py

from time import sleep_ms # Pour temporiser
from machine import Pin # Gestion de la broche du suiveur de ligne 

line_finder = Pin('D7', Pin.IN) # Broche du détecteur de ligne

while True:
	# Renvoie 1 lorsque la ligne noire est détectée, 1 si du blanc se trouve sous les diodes
	if line_finder.value(): # Equivalent à  line_finder.value() != 0:, on supprime un test
		print ("Ligne noire détectée !")
	else:
		print ("Surface blanche détectée")

	sleep_ms(100) # On temporise un dixième de seconde
```

**Attention** la sensibilité du détecteur de ligne doit être ajustée à la surface que vous allez surveiller. **Pour ce faire le module dispose d'une résistance réglable** (sur le Line Finder V1.1 de Grove, elle est au dos des diodes de détection, et notée R6. Vous aurez besoin d'un petit tournevis pour la manipuler). Voici la procédure :
- Commencez par charger et exécutez le script qui précède
- Dessinez une ligne noire au marqueur sur une feuille blanche et faites passer les diodes du module au dessus de la ligne. Si la sensibilité du capteur est correcte, vous devriez obtenir un message "ligne noire franchie" chaque fois que vous traversez la ligne. Autrement, ajustez la valeur de la résistance jusqu'à obtenir ce comportement.

## Le code MicroPython avec une interruption

> **Vous pouvez télécharger les scripts MicroPython de ce tutoriel (entre autres) en cliquant [ici](../../../assets/Script/MODULES.zip)**.

Voici l'adaptation du code précédent en utilisant une interruption attachée à la broche du suiveur de ligne :

```python
# Objet du script : Mettre en œuvre un module suiveur de ligne
# en utilisant l'interruption de la broche numérique sur laquelle il est
# connecté.

from machine import Pin # Gestion de la broche du suiveur de ligne 

# Variable globale qui sera modifiée par la routine de service de l'interruption
line_detected = False

# Routine de service de l'interruption
# Cette fonction donne la valeur "True" à la variable globale "line_detected" si l'état de
# la broche "pin" change.
@MicroPython.viper # Optimise agressivement le pseudo-code MicroPython
def handle_interrupt(pin):
  global line_detected
  line_detected = True
  global interrupt_pin
  interrupt_pin = pin

line_finder = Pin('D7', Pin.IN) # Broche du détecteur de ligne

# On "attache" l'interruption à la broche du détecteur de ligne
# Cela signifie que le gestionnaire d'interruptions contenu dans le STM32WB55 va "surveiller" 
# la tension sur la broche D7. Si cette tension augmente et passe de 0 à 3.3V (IRQ_RISING)
# alors le gestionnaire d'interruption forcera le STM32WB55 à exécuter la fontion "handle_interrupt".

line_finder.irq(trigger=Pin.IRQ_RISING, handler=handle_interrupt)

while True:
	if line_detected: # si line_detected = True alors, cela signifie que l'interruption a eue lieu.
		print("Ligne noire franchie !")
		line_detected = False
	# Place le microcontrôleur en sommeil en attendant la prochaine interruption
	pyb.wfi() 
```
