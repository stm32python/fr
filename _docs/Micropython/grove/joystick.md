---
title: Joystick
description: Mise en œuvre du "thumb-joystick" Grove avec MicroPython
---

# Joystick

Ce tutoriel explique comment mettre en œuvre un "thumb-joystick" Grove avec MicroPython.

## Matériel requis

1. Une [carte d'extension de base Grove](https://wiki.seeedstudio.com/Base_Shield_V2/)
2. La carte NUCLEO-WB55
3. Un ["thumb-joystick" Grove](https://wiki.seeedstudio.com/Grove-Thumb_Joystick/)

**Le thumb-joystick de Grove :**

<div align="left">
<img alt="Grove thumb-joystick" src="images/joystick.png" width="200px">
</div>

> Crédit image : [Seeed Studio](http://wiki.seeedstudio.com/Grove_System/)

Ce joystick est similaire à ceux que l'on peut retrouver sur une manette de PS2. Chacun des axes est relié à un potentiomètre  qui va fournir une tension proportionnelle au mouvement. Il possède de plus un bouton poussoir.
Le code permet de vérifier tous les états que le joystick peut prendre. On pourra vérifier nos inputs dans l'interpréteur MicroPython.
Le joystick doit être branché sur un connecteur analogique *A0*, *A1*, *A2* ou *A3* (voir commentaires dans le code ci-dessous).

## Le code MicroPython

> **Vous pouvez télécharger les scripts MicroPython de ce tutoriel (entre autres) en cliquant [ici](../../../assets/Script/MODULES.zip)**.

Editez maintenant le script *main.py* sur le disque *PYBFLASH* :
 
```python
# Objet du script : Mise en œuvre du Joystick Grove.

from time import sleep_ms # Pour temporiser
from pyb import Pin

vertical = ADC(PIN('A0')) #en branchant le joystick sur A0,l'axe Y sera lu par A0 et l'axe X par A1
horizontal= ADC(PIN('A1')) #sur A1, Y sera lu par A1 et X par A2; sur A2, Y sera lu par A2 et X par A3..

while True :

	sleep_ms(500) # Temporisation de 500 millisecondes
	
	x = vertical.read()
	y = horizontal.read()

	if x <= 780 and x >= 750 :
		print("Haut")
	if x <= 280 and x >= 240 :
		print("Bas")
	if y <= 780 and y >= 750 :
		print("Gauche")
	if y <= 280 and y >= 240 :
		print("Droite")
	if x >= 1000: # En appuyant sur le joystick, la sortie de l'axe X se met à 1024, le maximum.
```

## Manipulation

Démarrez le script avec *[CTRL]-[D]* sur votre terminal série (PuTTY par exemple) et vérifiez que l'affichage est bien 
en cohérence avec la position du joystick :

```console
>>>
MPY: sync filesystems
MPY: soft reboot

Haut
Haut
Haut
Haut
Appuyé
Appuyé
Gauche
Gauche
Gauche
Bas
Bas
Bas
```
