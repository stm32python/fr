---
title: Capteur de température et d'humidité SHT31
description: Mise en œuvre un module Grove capteur de température et d'humidité SHT31 sur bus I2C en MicroPython
---

# Capteur de température et d'humidité SHT31

Ce tutoriel explique comment mettre en œuvre un module Grove capteur de température et humidité SHT31 sur bus I2C en MicroPython. La fiche technique du capteur SHT31 de Sensirion [est disponible ici](https://www.sensirion.com/fileadmin/user_upload/customers/sensirion/Dokumente/2_Humidity_Sensors/Datasheets/Sensirion_Humidity_Sensors_SHT3x_Datasheet_digital.pdf).

**Le module Grove capteur de température et d'humidité SHT31 :**

<br>
<div align="left">
<img alt="Grove sht31" src="images/sht31.jpg" width="350px">
</div>
<br>

> Crédit image : [Botland](https://botland.store/grove-temperature-sensors/15329-grove-temperature-and-humidity-sensor-sht31-i2c.html)

## Matériel requis et montage

1. Une [carte d'extension de base Grove](https://wiki.seeedstudio.com/Base_Shield_V2/)
2. La carte NUCLEO-WB55
3. Un [module Grove Temp and Humi Sensor (SHT31)](https://wiki.seeedstudio.com/Grove-TempAndHumi_Sensor-SHT31/)

Branchez le module sur l'un des connecteurs I2C du Grove base shield.


## Le code MicroPython

> **Vous pouvez télécharger les scripts MicroPython de ce tutoriel (entre autres) en cliquant [ici](../../../assets/Script/MODULES.zip)**.

Il faut ajouter le fichier *sht31.py* dans le répertoire du périphérique *PYBFLASH*.<br>
Editez maintenant le script *main.py* et collez-y le code qui suit :

```python
# Exemple adapté de https://GitHub.com/kfricke/MicroPython-sht31/blob/master/
# Objet du script : Mise en œuvre du module grove I2C capteur de température 
# et humidité basé sur le capteur SHT31

from time import sleep_ms
from machine import I2C, Pin
from sht31 import SHT31

# On utilise l'I2C n°1 de la carte NUCLEO-WB55 pour communiquer avec le capteur
i2c = I2C(1)

# Pause d'une seconde pour laisser à l'I2C le temps de s'initialiser
sleep_ms(1000)

# Liste des adresses I2C des périphériques présents
print("Adresses I2C utilisées : " + str(i2c.scan()))

# Instanciation du capteur
sht = SHT31(i2c=i2c)

while True:

	# Temporisation d'une seconde
	sleep_ms(1000)
	
	# Lecture des valeurs mesurées
	shtdata = sht.get_temp_humi()

	# Affichage formatté des mesures
	print('=' * 40) # Imprime une ligne de séparation
	
	# Affiche la température en degrés Celsius.
	print("Température : %.1f °C" %shtdata[0])
	
	# Affiche l'humidité en pourcents. 
	# Attention, le caractère '%' à la fin est dédoublé pour ne pas être interprété
	# comme une instruction de formattage !
	print("Humidité relative : %.1f %%" %shtdata[1])
```

## Sortie sur le port série de l'USB USER

Appuyez sur *[CTRL]-[D]* dans le terminal PuTTY et observez les valeurs qui défilent :

<br>
<div align="left">
<img alt="Grove - SHT31 sortie" src="images/grove_sht31_output.png" width="450px">
</div>
<br>
