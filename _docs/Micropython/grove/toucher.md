---
title: Interrupteur tactile
description: Mise en œuvre d'un interrupteur tactile avec MicroPython
---

# Interrupteur tactile

Ce tutoriel explique comment mettre en œuvre un interrupteur tactile capacitif de type TTP223-B avec MicroPython. Il peut détecter un changement de capacité lorsqu'un doigt l'approche. Il sera dans l’état *ON* s’il y a contact et *OFF* dans le cas contraire.

## Matériel requis

1. Une [carte d'extension de base Grove](https://wiki.seeedstudio.com/Base_Shield_V2/)
2. La carte NUCLEO-WB55
3. Un [module capteur tactile Grove](https://wiki.seeedstudio.com/Grove-Touch_Sensor/)

**L'interrupteur tactile Grove (Touch sensor) :**

<br>
<div align="left">
<img alt="Grove touch sensor" src="images/grove_touch_sensor.jpg" width="350px">
</div>
<br>

> Crédit image : [Seeed Studio](http://wiki.seeedstudio.com/Grove_System/)

On trouve bien sûr d'autres implémentations matérielle du capteur tactile que celle de SeeedStudio. Voici par exemple une version qui doit être câblée "à la main". Le capteur ne possède que 3 broches qu'il faut connecter à *GND*, *VCC* (alimentation, câbles rouge et noir) et *D4* (signal, câble jaune).

<br>
<div align="left">
<img alt="Câblage interrupteur tactile" src="images/capteur_tactile.png" width="400px">
</div>
<br>

## Le code MicroPython

> **Vous pouvez télécharger les scripts MicroPython de ce tutoriel (entre autres) en cliquant [ici](../../../assets/Script/MODULES.zip)**.

Editez maintenant le script *main.py* sur le disque *PYBFLASH* et collez-y le code qui suit :

```python
# Objet du script : Mise en œuvre d'un capteur/interrupteur tactile

from pyb import Pin
from time import  sleep_ms # Pour temporiser

# Configuration de la broche du capteur en entrée, pull-up
p_in = Pin('D4', Pin.IN, Pin.PULL_UP)

while True :
	
	sleep_ms(500) # Temporisation de 500 millisecondes

	if p_in.value() == 1: # Si on touche le capteur
		print("ON")
	else: # Autrement
		print("OFF")
```

## Manipulation

Démarrez le script avec *[CTRL]-[D]* sur votre terminal série (PuTTY par exemple) et vérifiez que l'affichage est bien 
```ON``` lorsque vous posez le doigt sur l'interrupteur, et ```OFF``` dans le cas contraire : 

```console
>>>
MPY: sync filesystems
MPY: soft reboot

OFF
OFF
OFF
OFF
OFF
ON
ON
ON
ON
OFF
OFF
OFF
```