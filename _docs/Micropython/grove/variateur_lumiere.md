---
title: Variateur de lumière
description: Faire varier l'intensité lumineuse d'une LED avec MicroPython
---

# Variateur de lumière

Ce tutoriel explique comment faire varier l'intensité lumineuse d'une LED avec MicroPython en utilisant **un signal de commande modulé en largeur d'impulsion (PWM)**. Il s'agit de faire varier continûment l'intensité d'une LED. Pour cela, il est nécessaire de connecter la LED à *l'une des broches de la carte NUCLEO-WB55 qui supporte la fonction PWM*. Vous trouverez une explication de ce qu'est la PWM et la liste des broches/paramètres pour la mettre en œuvre [ici](buzzer).

## Matériel requis

1. Une [carte d'extension de base Grove](https://wiki.seeedstudio.com/Base_Shield_V2/)
2. La carte NUCLEO-WB55
3. Un [module LED Grove](https://wiki.seeedstudio.com/Grove-LED_Socket_Kit/) (image ci-dessous)

<div align="left">
<img alt="Grove - LED Socket Kit" src="images/Grove-White-LED-p-2016.jpeg" width="400px">
</div>

> Crédit image : [Seeed Studio](http://wiki.seeedstudio.com/Grove_System/)

**Attention**, nous vous rappelons que **les LED sont polarisées** ; si vous les branchez incorrectement, vous les détruirez probablement. La patte la plus longue de la LED que vous utiliserez devra être insérée dans la borne "+" du module Grove et la plus courte dans sa borne "-".

## Premier code MicroPython : gestion du bouton en scrutation

> **Vous pouvez télécharger les scripts MicroPython de ce tutoriel (entre autres) en cliquant [ici](../../../assets/Script/MODULES.zip)**.

Dans ce premier exemple, l'intensité de la LED cesse de varier lorsque le bouton *sw1* est relâché. La gestion du bouton est faite par *scrutation* ("polling" en anglais).

```python
# Objet du Script : Mise en œuvre d'une PWM pour réaliser un variateur de lumière.
# L'intensité de la LED cesse de varier lorsque le bouton sw1 est relâché.
# Gestion du bouton par "scrutation" ("polling" en anglais).
# Matériel (en plus de la carte NUCLEO-WB55) : une LED connectée sur D6 et GND.

from pyb import Pin, Timer
from time import sleep_ms
import gc # Ramasse miettes, pour éviter de saturer la mémoire

# Initialisation du bouton SW1
sw1 = pyb.Pin( 'SW1' , pyb.Pin.IN)
sw1.init(pyb.Pin.IN, pyb.Pin.PULL_UP, af=-1)

button_pressed = False

# initialisation de la PWM 
led_pin = Pin('D6') 
ti = 1 # Timer 
ch = 1 # Canal
tim = Timer(ti, freq=1000) # Fréquence de la PWM fixée à 1kHz
ch = tim.channel(ch, Timer.PWM, pin=led_pin)
i=0

while True:

	if not sw1.value(): # Si le bouton est appuyé

		while i < 101: # Augmente l'intensité de la LED par pas de 1%
			ch.pulse_width_percent(i)
			i=i+1
			sleep_ms(10) # Pause de 10 ms

		while i > 0: # Réduit l'intensité de la LED par pas de 1%
			ch.pulse_width_percent(i)
			i=i-1
			sleep_ms(10) # Pause de 10 ms

	else:
		ch.pulse_width_percent(0) # Si le bouton n'est pas appuyé

	# Appel du ramasse-miettes, indispensable pour que le programme ne se bloque pas
	# très rapidement en fragmentant complètement la RAM.
	gc.collect()
```

## Deuxième code MicroPython : gestion du bouton avec une interruption externe

> **Vous pouvez télécharger les scripts MicroPython de ce tutoriel (entre autres) en cliquant [ici](../../../assets/Script/MODULES.zip)**.

Dans ce deuxième exemple l'intensité de la LED cesse de varier après un premier appui sur le bouton *sw1*.
Elle recommence à varier après un second appui sur *sw1*. Le bouton est géré avec *une interruption externe*.

```python
# Objet du Script : Mise en œuvre d'une PWM pour réaliser un variateur de lumière.
# L'intensité de la LED cesse de varier après un premier appui sur le bouton sw1.
# Elle recommence à varier après un second appui sur sw1.
# Utilisation d'une interruption externe pour gérer le bouton.
# Matériel (en plus de la carte NUCLEO-WB55) : une LED connectée sur D6 et GND.

from pyb import Pin, Timer, ExtInt
from time import sleep_ms
import gc # Ramasse miettes, pour éviter de saturer la mémoire

# Initialisation du bouton SW1
sw1 = pyb.Pin( 'SW1' , pyb.Pin.IN)
sw1.init(pyb.Pin.IN, pyb.Pin.PULL_UP, af=-1)

# Est-ce que le bouton a été pressé ?
button_pressed = False

# Routine de gestion de l'interruption du bouton
def ISR_bouton(line):
#	print("line =", line)
	global button_pressed # mot clef "global" très important ici
	button_pressed = not button_pressed

ext = ExtInt(Pin('SW1'), ExtInt.IRQ_RISING, Pin.PULL_UP, ISR_bouton)

# initialisation de la PWM 
led_pin = Pin('D6') 
ti = 1 # Timer 
ch = 1 # Canal
tim = Timer(ti, freq=1000) # Fréquence de la PWM fixée à 1kHz
ch = tim.channel(ch, Timer.PWM, pin=led_pin)
i=0

while True:

	if button_pressed :

		while i < 101: # augmente l'intensité de la LED par pas de 1%
			ch.pulse_width_percent(i)
			i=i+1
			sleep_ms(10) # pause de 10 ms

		while i > 0: # réduit l'intensité de la LED par pas de 1%
			ch.pulse_width_percent(i)
			i=i-1
			sleep_ms(10) # pause de 10 ms

	# Appel du ramasse-miettes, indispensable pour que le programme ne se bloque pas
	# très rapidement en fragmentant complètement la RAM.
	gc.collect()
```
