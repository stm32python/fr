---
title: LED infrarouge
description: Mise en œuvre d'un module Grove LED infrarouge avec MicroPython
---

# LED infrarouge

Ce tutoriel explique comment faire clignoter une LED à une fréquence donnée, sur une broche numérique. Il s'applique à tout type de LED mais nous prenons ici l'exemple d'une LED infrarouge (IR) dont l'émission est imperceptible par nos yeux. Vous pourrez vérifier que la LED clignote effectivement avec la caméra de votre smartphone qui, elle, est capable de "voir" dans l'IR !

**La LED infrarouge de Grove :**

<br>
<div align="left">
<img alt="Grove - IR emitter" src="images/gove_IR_LED.jpg" width="300px">
</div>
<br>

> Crédit image : [Seeed Studio](http://wiki.seeedstudio.com/Grove_System/)

## Matériel requis et montage

1. Une [carte d'extension de base Grove](https://wiki.seeedstudio.com/Base_Shield_V2/)
2. La carte NUCLEO-WB55
3. Un [module LED (infrarouge) Grove](https://wiki.seeedstudio.com/Grove-Infrared_Emitter/)

Branchez le module sur le connecteur *D4* de la carte d'extension de base Grove.

## Le code MicroPython

> **Vous pouvez télécharger les scripts MicroPython de ce tutoriel (entre autres) en cliquant [ici](../../../assets/Script/MODULES.zip)**.

Editez le script *main.py* du périphérique *PYBFLASH* et collez-y le code qui suit :

```python
# Objet du code : 
# Version MicroPython du programme "Blink".
# Fait clignoter une LED (éventuellement infrarouge) à une fréquence programmable.
# Nécessite une LED externe à la carte (module Grove par exemple).

from pyb import Pin # Classe pour gérer les broches GPIO
from time import sleep_ms # Classe pour temporiser

# La LED est assignéeà la broche D4
led = Pin('D4', Pin.OUT_PP)

while True :
	sleep_ms(500) # Pose 0.5 seconde
	led.off()
	print("LED éteinte")
	sleep_ms(1000) # Pause 1 seconde
	led.on()
	print("LED allumée")
```

## Affichage sur le terminal série de l'USB User

Appuyez sur *[CTRL]-[D]* et observez les messages qui défilent dans le terminal PuTTY :

<br>
<div align="left">
<img alt="Blink LED output" src="images/blink_output.png" width="450px">
</div>

## Pour aller plus loin : le code Morse

On peut se servir de la lumière infrarouge pour transférer des messages à courte distance, entre une diode émettrice et une diode réceptrice. La plupart des télécommandes d'équipements domestiques utilisent cette technologie qui nécessite de placer l'émetteur et le récepteur l'un en face de l'autre, à quelques mètres de distance au plus.

[**Ce tutoriel**](morse) montre comment réaliser une connexion half-duplex (un émetteur et un récepteur) entre deux NUCLEO-WB55, l'une équipée d'un module Grove LED IR et l'autre d'un [module Grove capteur de luminosité](https://wiki.seeedstudio.com/Grove-Light_Sensor/). La première carte envoie un message en [code Morse](https://fr.wikipedia.org/wiki/Code_Morse_international) et la deuxième capte ce message et le décode.
