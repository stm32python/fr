---
title: Module carte SD
description: Mettre en œuvre un module carte SD sur bus SPI avec MicroPython
---

# Module carte SD sur bus SPI

Ce tutoriel explique comment mettre en œuvre un module carte SD sur bus SPI avec MicroPython.

Une grande partie des projets que vous aurez envie de développer nécessiteront un accès en lecture et écriture à une carte SD, pour :
 - Enregistrer / relire des mots de passe ou des paramètres utilisateurs
 - Enregistrer / relire des séries de mesures en provenance de capteurs
 - Enregistrer / relire des messages d'erreur
 - Etc.

Ce tutoriel partage un code exemple des différentes fonctions indispensables à la mise en œuvre d'un module de lecture / écriture sur une carte SD. Ce code et la bibliothèque *sdcard.py* sont directement copiés depuis [ce site](https://GitHub.com/Micropython/MicroPython).

**Deux modules carte SD SPI :**

|MH-SD Card Module (alim : 3.3V ou 5V) | Catalex micro SD Card Module (alim : 5V)|
|:-:|:-:|
|<img alt="MH-SD Card Module" src="images/sd-card-module.jpg" width="260px">| <img alt="Catalex µSD Card Module" src="images/Catalex_SD_card_module.jpg" width="280px">|

> Crédit image : [Aranacorp](https://www.aranacorp.com/fr/lire-et-ecrire-sur-une-carte-sd-avec-arduino/)

## Matériel requis

1. La carte NUCLEO-WB55
2. Un  module carte SD SPI
	Module SD utilisé : MH-SD Card Module, qui peut être alimenté en 3.3V ou 5V
	*Attention : les modules microSD (ou µSD) Catalex doivent être alimenté en 5V !*
3. Des câbles dupont
4. Un [module Grove BME280](https://wiki.seeedstudio.com/Grove-Barometer_Sensor-BME280/) pour la deuxième partie du tutoriel

## Première partie : le code MicroPython pour tester la bibliothèque sdcard.py

> **Vous pouvez télécharger les scripts MicroPython de ce tutoriel (entre autres) en cliquant [ici](../../../assets/Script/MODULES.zip)**..

Branchez le module carte SD sur sur le bus SPI1 de la carte NUCLEO-WB55. Les broches utilisées sont les suivantes, sur les connecteurs Arduino :

- MOSI : D11
- MISO : D12
- SCK : D13
- CS : D9
- GND : Celle que vous voulez
- Alimentation : 3V ou 5V selon votre module

Et voici le code :

```python
# Objet du script : Présenter les fonctions de lecture et écriture offertes par sdcard.py.
# Source : https://github.com/Micropython/micropython/tree/master/drivers
# Module SD utilisé : MH-SD Card Module alimentable en 3.3V 
# Attention : les modules µSD Catalex ne semblent pas fonctionner avec sdcard.py !
 
import sdcard, os
from pyb import SPI

spi = SPI(1, SPI.MASTER, baudrate=100000, polarity=1, phase=0) # Instance du bus SPI
sd = sdcard.SDCard(spi, machine.Pin('D9')) # Broche de sélection du module carte SD

vfs = os.VfsFat(sd) # Déclaration d'un système de fichier FAT
os.mount(vfs, "/fc") # Montage du volume logique associé au module carte SD

print("Liste des fichiers présents sur la carte SD (test du système de fichiers)")
print(os.listdir("/fc"))

letters = "abcdefghijklmnopqrstuvwxyz\n"
more_letters = letters * 200 # 5400 caractères
numbers = "1234567890\n"

fn = "/fc/fichier1.txt"
print()

print("Lecture / écriture de plusieurs blocs")
with open(fn, "w") as f:
	n = f.write(more_letters)
	print(n, "octets écrits")
	n = f.write(numbers)
	print(n, "octets écrits")
	n = f.write(more_letters)
	print(n, "octets écrits")

with open(fn, "r") as f:
	result1 = f.read()
	print(len(result1), "octets lus")

fn = "/fc/fichier2.txt"
print()
print("Lecture/écriture d'un seul block")
with open(fn, "w") as f:
	n = f.write(numbers)  # un seul block
	print(n, "octets écrits")

with open(fn, "r") as f:
	result2 = f.read()
	print(len(result2), "octets lus")

os.umount("/fc")

print()
print("Contrôle des données écrites")
success = True

if result1 == "".join((more_letters, numbers, more_letters)):
	print("Grand fichier : OK")
else:
	print("Grand fichier : Echec")
	success = False
	
if result2 == numbers:
	print("Petit fichier : OK")
else:
	print("Petit fichier : Echec")
	success = False
print()
print("Tests", "réussis" if success else "échoués")

```

Si vous avez correctement câblé le module et que celui-ci est compatible avec la bibliothèque *sdcard.py*, vous devriez obtenir ce retour sur le terminal série du l'USB user :

<br>
<div align="left">
<img alt="Module carte SD SPI, retour du test fonctionnel" src="images/sdcard_feedback.png" width="700px">
</div>
<br>

Vous devriez trouver sur la carte SD les fichiers "fichier1.txt" et "fichier2.txt" avec les contenus écrits par le code ci-avant.

## Deuxième partie : le code MicroPython pour enregistrer les mesures d'un module Grove BME280

> **Vous pouvez télécharger les scripts MicroPython de ce tutoriel (entre autres) en cliquant [ici](../../../assets/Script/MODULES.zip)**.

Nous allons à présent donner un exemple de script pour enregistrer sur la carte SD les mesures effectuées par un module I2C Grove BME280. Nous sommes repartis du script donné pour [cet exemple](bme280).

Laissez le module carte SD connecté là où il est, et branchez le module BME280 sur sur le bus I2C de la carte NUCLEO-WB55. Les broches utilisées sont les suivantes, sur les connecteurs Arduino :

- SCL : D15
- SDA : D14
- GND : Celle que vous voulez
- Alimentation : 3V3

Et voici le code :

```python
# Objet du script : Mise en œuvre du module grove I2C capteur de pression,
# température et humidité basé sur le capteur BME280
# Enregistre les mesures dans un fichier sur une carte SD.

import os,pyb,time,sdcard,bme280	# Pilotes pour lire-écrire des fichiers, pour temporiser, 
									# du module carte SD et du capteur bme280.
from machine import I2C, Pin # Pilotes du contrôleur de bus I2C et des entrées-sorties
from pyb import SPI # Pilote du contrôleur de bus SPI

# On utilise l'I2C n°1 de la carte NUCLEO-W55 pour communiquer avec le capteur
i2c = I2C(1)

# Pause d'une seconde pour laisser à l'I2C le temps de s'initialiser
time.sleep_ms(1000)

# Liste des adresses I2C des périphériques présents
print("Adresses I2C utilisées : " + str(i2c.scan()))

# Instanciation du capteur
bme = bme280.BME280(i2c=i2c)

spi = SPI(1, SPI.MASTER, baudrate=100000, polarity=1, phase=0) # Instance du bus SPI
sd = sdcard.SDCard(spi, machine.Pin('D9')) # Broche de sélection du module carte SD

vfs = os.VfsFat(sd) # Déclaration d'un système de fichier FAT
os.mount(vfs, "/fc") # Montage du volume logique associé au module carte SD

fname = "/fc/log.csv"

with open(fname, "w") as log: # Ouverture du fichier "log.csv" en écriture
	
	n = log.write("Temps,Temp,Pres,Humi" + '\n')
	
	while True:

		# Temporisation d'une seconde
		time.sleep_ms(1000)
	
		# Lecture des valeurs mesurées
		bme280data = bme.values
	
		# Séparation et formattage (arrondis) des mesures
		temp = round(bme280data[0],1)
		press = int(bme280data[1])
		humi = int(bme280data[2])

		# Affichage des mesures
		print('=' * 40)  # Imprime une ligne de séparation
		print("Température : " + str(temp) + " °C")
		print("Pression : " + str(press) + " hPa")
		print("Humidité relative : " + str(humi) + " %")
	
		# Ecriture dans un fichier "log.csv" de la carte SD
		t = time.ticks_ms() # Etiquette de temps
		# écriture dans le fichier
		#n = log.write(str(t) + "," + str(temp) + "," + str(press) + "," + str(humi) + '\n')
		n = log.write("{},{},{},{}\n".format(t, temp, press, humi))
		print(n, "octets écrits")
```

 Vous devriez obtenir ce retour sur le terminal série du l'USB user :

<br>
<div align="left">
<img alt="Module carte SD SPI, retour du test fonctionnel" src="images/sdcard_bme280_feedback.png" width="700px">
</div>
<br>

Et, si vous accédez au contenu de la carte SD, vous devriez y trouver un fichier  "log.csv" avec un contenu de cette forme :

<br>
<div align="left">
<img alt="Module carte SD SPI, retour du test fonctionnel" src="images/sdcard_bme280_logs.png" width="300px">
</div>
<br>
