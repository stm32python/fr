---
title: Modules GNSS Grove SIM28 & Air530
description: Mise en œuvre des modules Grove GNSS UART avec MicroPython
---

# Modules GNSS Grove SIM28 & Air530 

Ce tutoriel explique comment mettre en œuvre les Modules GNSS [Grove SIM28](https://wiki.seeedstudio.com/Grove-GPS/) et [Air530](https://wiki.seeedstudio.com/Grove-GPS-Air530/) de Seeed Studio avec MicroPython. 

|Module GPS UART Grove SIM28|Module GPS UART Grove Air530|
|:-:|:-:|
|<img alt="Grove - GPS SMI28" src="images/grove_sim28.jpg" width="275px">|<img alt="Grove - GPS Air530" src="images/grove_air530.png" width="330px">|
|Crédit image : [RS](https://fr.rs-online.com/)|Crédit image : [Seeed Studio](http://wiki.seeedstudio.com/Grove_System/)|

## Qu'est-ce donc que le GNSS ?

Pour une explication rapide de ce que sont les différents systèmes globaux de localisation par satellites, autrement désignés par l'acronyme anglais **GNSS**, nous vous renvoyons au tutoriel sur le [module Ultimate GPS Breakout d'Adafruit](gps_adafruit).

## Matériel requis et montage

1. Une [carte d'extension de base Grove](https://wiki.seeedstudio.com/Base_Shield_V2/)
2. Une carte NUCLEO-WB55
3. Un [module Grove SIM28](https://wiki.seeedstudio.com/Grove-GPS/) ou un [module Grove Air530](https://wiki.seeedstudio.com/Grove-GPS-Air530/).

Le module GNSS choisi doit être branché *sur le connecteur UART du Grove base shield*, qui peut être alimenté en 3.3V ou 5V.

## Le script MicroPython

> **Vous pouvez télécharger les scripts MicroPython de ce tutoriel (entre autres) en cliquant [ici](../../../assets/Script/MODULES.zip)**. Une fois l'archive ZIP décompressée vous trouverez le fichier *main.py* et le fichier *adafruit_gps.py* dans le dossier *\\Module GNSS Grove\\*. 

Il s'agit d'une adaptation du travail de **Peter Hinch**, disponible [sur ce dépôt GitHub](https://github.com/peterhinch/micropython-async/blob/master/v3/docs/GPS.md).

**Attention !**<br>Ce script fonctionnera avec n'importe quel module GNSS UART, **mais vous devez veiller à renseigner la bonne valeur du baudrate pour votre module !** En général, les valeurs par défaut sont 9600 bauds ou 115200 bauds.

**Attention ! (bis)**<br>les commandes de la partie configuration du script qui suit sont propres à chaque puce GNSS :
- Pour le module SIM28, la documentation est [ici](SIM28_SIM68R_SIM68V_Manual_V1.01.pdf).
- Pour le module Air530, la documentation est [ici](Air530_GNSS_Manual.pdf).

**Attention ! (ter)**<br>Les antennes fournies avec ces modules n'étant généralement pas actives (amplifiées), vous serez très certainement obligé de tester les modules à l'extérieur, sans un étage au-dessus de votre tête, pour obtenir le fix des satellites.

Copiez les fichiers **as_GPS.py** dans le répertoire du périphérique *PYBFLASH*. Editez maintenant le script *main.py* et copiez-y le code qui suit :

```python
# Example de mise en oeuvre d'un module GPS UART à l'aide de l'éxécution asynchrone de MicroPython.
# Cet exemple est une application immédiate du travail de Peter Hinch, ici :
# https://github.com/peterhinch/micropython-async/blob/master/v3/docs/GPS.md

import uasyncio as asyncio # Pour exécution asynchrone
from machine import UART # Pour piloter l'UART
import as_GPS # Routines GPS asynchrones
from time import sleep # Pour temporises
import gc # Ramasse miettes

GNSS_BAUDRATE = const(9600) # Débit du port série du module GNSS
RUN_DURATION_S = const(600) # Durée de collecte de mesures après fix
UART_NUMBER = const(2) # Identifiant de l'UART qui sera utilisé

# Décodage des trames NMEA : extraction et affichage des informations
# Exécuté pour chaque fix valide
def callback(gnss, *_): 

    # Temps universel coordonné (heure sans les ajustements été/hivers)
    utc = gnss.utc
    hh = '{:0>2}'.format(utc[0])
    mi = '{:0>2}'.format(utc[1])
    ss = '{:0>2}'.format(utc[2])

    # Date
    date = gnss.date
    dd = '{:0>2}'.format(date[0])
    mm = '{:0>2}'.format(date[1])
    yy = '{:0>2}'.format(date[2])

    # Coordonnées géographiques (altitude, latitude, longitude)
    alt = gnss.altitude
    lat0 = gnss.latitude()[0]
    lat1 = gnss.latitude()[1]
    lon0 = gnss.longitude()[0]
    lon1 = gnss.longitude()[1]
    
    # Impression sur le port série
    print("Date %s/%s/%s Time (UTC) %s:%s:%s" %(dd, mm, yy, hh, mi, ss))
    print("Lat = %.8f %s, Lon = %.8f %s, Alt = %d m\r\n" %(lat0, lat1, lon0, lon1 , alt))
    gc.collect() # Appel du ramasse miettes

"""Envoie une commande au module GNSS
Ajoute le $ au début et le *<Checksum>\r\n à la fin.
"""
def send_command(command):
	uart.write('$')
	uart.write(command)
	checksum = 0
	for char in command:
		checksum ^= ord(char)
	uart.write('*')
	uart.write('{:02x}'.format(checksum).upper())
	uart.write('\r\n')

# Configuration du module
def configure():
    print("Configuration du module en cours")
    # Spécifique aux modules Grove SIM28
    # Demande au module d'utiliser conjointement les constellations GPS et GLONASS
    # d'après https://files.seeedstudio.com/wiki/Grove-GPS/res/SIM28_DATA_File.zip
    # send_command("PMTK353,1,1")
    # sleep(1)
    
    # Spécifique aux modules Grove Air30
    # Demande au module d'utiliser conjointement les constellations GPS et GLONASS
    # d'après https://github.com/sivaelid/Heltec_AB02S_Mods/blob/master/README.md
    send_command("PGKC115,1,1,0,0")
    sleep(1)
    print("Configuration terminée.")

# Tâche de réception GNSS
async def recv_task():
    print("Attente du fix GNSS ...\r\n")
    # Réception asynchrone de la position, de l'altitude et de la date
    await gnss.data_received(position=True, altitude=True, date=True)
    # Lecture pendant RUN_DURATION_S secondes après fix des satellites
    await asyncio.sleep(RUN_DURATION_S)

# Débit, en bauds, de la communication série du GNSS
BAUDRATE = const(GNSS_BAUDRATE) 

# Initialisation de l'UART
uart = UART(UART_NUMBER, BAUDRATE)

# Flux de réception de l'UART en mode asynchrone
sreader = asyncio.StreamReader(uart)

# Configuration du récepteur GNSS
configure()

# Instance GNSS, appelle la fonction "callback" au fix
gnss = as_GPS.AS_GPS(sreader, fix_cb=callback)

# Lance l'ordonnanceur pour l'exécution asynchrone
asyncio.run(recv_task())
```

## Sortie sur le port série de l'USB USER

Lancez le script avec *[CTRL]-[D]* dans le terminal PuTTY et observez les relevés de position :

```console
Date 01/04/24 Time (UTC) 07:25:55
Lat = 48.84611007 N, Lon = 2.34621218 E, Alt = 330 m

Date 01/04/24 Time (UTC) 07:25:56
Lat = 48.84611007 N, Lon = 2.34621218 E, Alt = 330 m

Date 01/04/24 Time (UTC) 07:25:57
Lat = 48.84611007 N, Lon = 2.34621265 E, Alt = 330 m

Date 01/04/24 Time (UTC) 07:25:58
Lat = 48.84611770 N, Lon = 2.34621122 E, Alt = 330 m
```

Soyez attentif à la représentation choisie pour la latitude et la longitude, qui sont **exprimées ici en degrés décimaux (DD)**. Plusieurs écritures équivalentes peuvent être sont utilisées, notamment les degrés-minutes-secondes (DMS), ou [**système sexagésimal**](https://fr.wikipedia.org/wiki/Coordonn%C3%A9es_g%C3%A9ographiques), expression traditionnelle des coordonnées géographiques. Vous pouvez utiliser Google Maps pour passer des DD au DMS et vérifier que le relevé de position est pertinent. Par exemple, si vous tapez ```48.84611, 2.34621``` dans le champ de recherche de Google Maps vous obtiendrez :

<br>
<div align="left">
<img alt="Coordonnées Panthéon" src="images/gps_pantheon.jpg" width="400px">
</div>
<br> 

> Crédit image : [Google Maps](https://www.google.com/maps/)

L'expression des coordonnées en DMS est donnée par la première ligne sous la photo du lieu : ```48°50'46.0"N 2°20'46.4"E```. 

## Liens et références

Les liens suivants pourraient vous être utiles :
- [La page de Seeed Studio sur le module SIM28](https://wiki.seeedstudio.com/Grove-GPS/)
- [La page de Seeed Studio sur le module Air530](https://wiki.seeedstudio.com/Grove-GPS-Air530/)
- [Le GitHub de Si Vaelid](https://github.com/sivaelid/Heltec_AB02S_Mods/blob/master/README.md) sur le Air530 (en langage C) et, notamment, sa traduction de la notice du module du chinois vers l'anglais.
- [Un calculateur en ligne de sommes de contrôle NMEA](https://nmeachecksum.eqth.net/)
- [Une explication du calcul des sommes de contrôle NMEA (Arduino)](https://forum.arduino.cc/t/nmea-checksums-explained/1046083
)
- [Une fiche d'activités pédagogiques pour les STI2D](https://lewebpedagogique.com/isneiffel/files/2017/06/Module-GPS.pdf) du [Lycée Gustave Eiffel](https://lewebpedagogique.com/isneiffel/) (Arduino et Raspberry)
