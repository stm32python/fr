---
title: REPL via un module Grove Serial Bluetooth v3.01
description: Interaction avec la Read Evaluate Print Loop de MicroPython sur une carte NUCLEO-WB55 par une connexion Bluetooth à l'aide d'un module Grove Serial Bluetooth v3.01
---

#  REPL via un module Grove Serial Bluetooth v3.01

Ce tutoriel explique comment mettre en œuvre en MicroPython un [module Grove Serial Bluetooth v3.01](https://files.seeedstudio.com/wiki/Grove-Serial_Bluetooth_v3.0/res/Bluetooth_module.pdf), basé sur un composant [Cambridge Silicon Radio CSR BlueCore (BC417)](CSR-BC417-datasheet.pdf) afin de pouvoir envoyer à une carte NUCLEO-WB55 des commandes REPL (Read Evaluate Print Loop) "over the air" depuis un ordinateur personnel ou un smartphone.

**Un module Grove Serial Bluetooth v3.01 :**

<br>
<div align="left">
<img alt="Grove - Serial Bluetooth v3.0" src="images/Grove-Serial_Bluetooth_v3.0.jpg" width="310px">
</div>
<br>

> Crédit image : [Seeed Studio](http://wiki.seeedstudio.com/Grove_System/)

**NB :** Le composant BC417 est le même que celui qui équipe les très populaires modules Bluetooth [HC-05](HC-05_Datasheet.pdf) et [HC-06](HC-06_Datasheet.pdf), mais les commandes "AT" (voir plus loin) de ces derniers ne sont pas identiques à celles du module  Grove Serial Bluetooth v3.01. Vous ne devriez donc pas rencontrer de grosses difficultés à transposer ce tutoriel sur HC-05 ou sur HC-06.

Le cas d'usage qui nous intéresse est précisé par la figure suivante :

<br>
<div align="left">
<img alt="REPL via Bluetooth" src="images/repl_via_bluetooth.jpg" width="900px">
</div>
<br>

* **Dans un premier temps, on établit une connexion série entre le module Bluetooth et l'hôte** (un ordinateur personnel ou un smartphone). Ceci nécessite d'abord la bonne connexion du module à la carte NUCLEO-WB55 (selon le schéma ci-dessus), la re-configuration (éventuelle) du module avec des commandes AT et son appairage avec l'hôte.
* **Dans un deuxième temps, le REPL est dupliqué sur cette connexion série Bluetooth** à l'aide des commandes MicroPython appropriées.

## Bluetooth versus Bluetooth Low Energy ?

Il ne faut pas confondre [le Bluetooth](https://fr.wikipedia.org/wiki/Bluetooth) avec [le Bluetooth basse consommation (Bluetooth Low Energy ou Bluetooth Smart), désigné par l'acronyme BLE](https://fr.wikipedia.org/wiki/Bluetooth_%C3%A0_basse_consommation). Selon Wikipédia, les modes BLE (bande passante plus limitée et très faible consommation) et Bluetooth standard (niveau d’émission plus élevé et portée plus grande) sont des technologies **complémentaires**. Vous trouverez plus d'informations concernant le BLE [sur cette page](../BLE/index.).

La carte [NUCLEO-WB55](../../Kit/nucleo_wb55rg) intégrant la norme BLE via son SoC STM32WB55RG, il peut paraître futile de s'y connecter avec un module externe en Bluetooth. D'ailleurs, un tutoriel "REPL via BLE" est disponible pour celle-ci [sur cette page](../BLE/BLEREPL).
Mais notre objectif est avant tout de vous montrer comment on peut réaliser, avec MicroPython, une connexion Bluetooth entre une carte à microcontrôleur et un ordinateur ou un smartphone.

Il existe une aussi une bonne raison de préférer le Bluetooth au BLE : sa simplicité d'utilisation. Une fois le module alimenté et appairé avec un ordinateur ou un smartphone, il se comporte **comme un simple câble virtuel** par lequel transitent les messages de l'UART concerné. Cette simplicité se heurte malheureusement à une **intolérance des smartphones et ordinateurs récents vis à vis du Bluetooth** qui ont souvent des problèmes de compatibilité logicielle et matérielle avec les modules BC417 et qui refusent de s'y connecter.

## Matériel requis et connexion du module Grove Serial Bluetooth v3.01 sur la NUCLEO-WB55

Pour ce tutoriel, nous aurons besoin du matériel suivant :

1. Une [carte d'extension de base Grove](https://wiki.seeedstudio.com/Base_Shield_V2/) **avec son commutateur d'alimentation positionné sur 5V**.
2. La carte NUCLEO-WB55
3. Un module Grove [Serial Bluetooth v3.01](https://wiki.seeedstudio.com/Grove-Serial_Bluetooth_v3.0/)  **connecté au port UART** de la carte d'extension de base Grove.

<br>

**Branchement du module Grove Serial Bluetooth v3.01 sur la NUCLEO-WB55**

Vous pouvez connecter un module Grove Serial Bluetooth de deux façons sur votre NUCLEO-WB55 :

1. Par la carte d'extension de base Grove, sur son connecteur UART. Dans ce cas le câblage sera forcément correct, mais **n'oubliez pas de placer le commutateur d'alimentaton sur 5V**.

2. **Ou bien** par les connecteurs ARDUINO avec un câble Grove-Dupont. Dans ce cas **il faudra croiser RX et TX**. La broche TX du module devra être câblée sur la broche RX/D0 de la NUCLEO WB55 et la broche RX du module devra être câblée sur la broche TX/D1 de la NUCLEO WB55. Bien sûr il faudra aussi que vous connectiez le VCC du module sur la broche 5V de la NUCLEO-WB55 et le GND du module sur l'une des broches GND de la NUCLEO-WB55. C'est ce qui est représenté sur la figure en début de cet article.

> **Attention !**<br>
Comme tous les modules électroniques, les modules Bluetooth **sont très sensibles aux sauts de tension**. Nous avons constaté **une très forte propension des modules basés sur le BC417 à "griller" lorsqu'on les branche et débranche "à chaud", sur une carte à microcontôleur alimentée**. Nous insistons donc sur la nécessité de déconnecter votre carte NUCLEO-WB55 de son alimentation lorsque vous manipulez ces modules.

Pour finir, précisons que, même si ces modules sont alimentés en 5V, leurs broches acceptent les niveaux logiques de commande 3.3V. Donc ils peuvent être pilotés sans difficultés par des systèmes à microcontrôleurs STM32.

## Re-configuration des modules Grove Serial Bluetooth v3.01 avec des commandes AT

Les modules Bluetooth devront parfois être re-paramétrés pour fonctionner correctement dans votre application. Par exemple, le module Grove Serial Bluetooth v3.01 a pour **code d'appairage** par défaut "1234" en sortie d'usine. Vous aurez peut être envie de remplacer celui-ci par un code de votre choix pour le rendre confidentiel. De même, vous pourriez vouloir changer le **nom par défaut du module** ou encore son **nombre de symboles transmissibles par seconde ([exprimé en bauds](../startwb55/uart))**.

Le paramétrage des modules se fait **à l'aide de commandes AT** (pour "**AT**tention"), des messages-textes qu'on leur adresse depuis la carte NUCLEO-WB55 via son UART. Il faudra donc exécuter sur la NUCLEO-WB55 un script qui transmettra au BC417 les commandes AT que vous saisirez dans un terminal série (affiché sur votre PC/MAC).

**Nous expliquons à présent comment procéder pour envoyer des commandes AT à un module Grove Serial Bluetooth v3.01** à l'aide d'un script MicroPython. La liste (**complète et correcte ?**) de ses commandes AT est disponible [dans ce document](CSR-BC417-datasheet.pdf), extrait [du site du fabricant du module](http://www.jnhuamao.cn/index_en.asp?ID=1).

Afin de ne pas alourdir ce tutoriel, nous ne recopions pas le script *main.py* qui relaye les commandes AT, **vous le trouverez [dans cette archive téléchargeable](../../../assets/Script/MODULES.zip)**, dans le chemin *"\REPL via un module Bluetooth BC417\Script AT\main.py*.

Copiez ce *main.py* dans le dossier *PYBFLASH*, connectez un terminal PuTTY à la console REPL de l'interpréteur MicroPython et démarrez le script avec *[CTRL]+[D]*.<br>
La console de PuTTY devrait ressembler à ceci :

```console
MicroPython v1.20.0 on 2023-04-26; NUCLEO-WB55 with STM32WB55RGV6
Type "help()" for more information.
>>>
>>>
MPY: sync filesystems
MPY: soft reboot
Entrez votre commande :
```

Dans la console PuTTY, tapez "AT" puis *[Entrée]*.<br>
Si les paramètres de la connexion avec le module (à savoir les valeurs des constantes *BAUDRATE*, *UART_NUMBER* et *EOL*) sont corrects, le module devrait vous répondre simplement *OK*, comme ci-dessous : 

```console
MicroPython v1.20.0 on 2023-04-26; NUCLEO-WB55 with STM32WB55RGV6
Type "help()" for more information.
>>>
>>>
MPY: sync filesystems
MPY: soft reboot
Entrez votre commande : AT
Envoyé : AT
Message reçu : OK
Entrez votre commande :
```

Nous avons confirmation que la communication est établie, nous pouvons donc envoyer des commandes plus complexes au module.<br>

Demandons au module le nom par défaut qui lui est attribué, avec la commande *"AT+NAME?"* :

```console
Entrez votre commande : AT+NAME?
Envoyé : AT+NAME?
Message reçu : OK+NAME:HMSoft
```
Le module nous a répondu que son nom est actuellement *"HMSoft"*.<br>
Si on souhaite changer le nom du module en *"grove_serial"*, il faut envoyer la commande *"AT+NAMEgrove_serial"*, comme ceci :

```console
Entrez votre commande : AT+NAMEgrove_serial
Envoyé : AT+NAMEgrove_serial
Message reçu : OK+Set:grove_serial
```
Le module confirme le changement par un message retour *"OK+Set:grove_serial"*. **Attention**, le nom du module ne doit pas comporter plus que 12 caractères**, à priori choisis parmi les alphanumériques et l'underscore "_".

Pour finir, nous allons réinitialiser le module avec ses paramètres d'usine en lui envoyant la commande *"AT+DEFAULT"* :

```console
Entrez votre commande : AT+DEFAULT
Envoyé : AT+DEFAULT
Message reçu : OK+DEFAULT
```
On peut vérifier que le nom du module est redevenu  *"HMSoft"* de nouveau avec la commande *"AT+NAME?"* :

```console
Entrez votre commande : AT+NAME?
Envoyé : AT+NAME?
Message reçu : OK+NAME:HMSoft
```

**Remarque importante : Paramètres UART, BAUDRATE et EOL**

Ces trois constantes définies dans *main.py* sont déterminantes pour établir la communication série avec un module Bluetooth BC417 (Grove ou autre). Pour notre module Grove connecté à une carte NUCLEO-WB55, elles doivent avoir les valeurs suivantes :
  - *UART_NUMBER = const(2)* : c'est le numéro de l'UART de la NUCLEO-WB55 pris en charge par MicroPython (indépendant du module)
  - *BAUDRATE = const(9600)* : c'est le débit préconfiguré d'usine du module Grove
  - *EOL = ""* (rien du tout !) : ce sont les caractères de terminaison de la commande AT, qui doivent lui être systématiquement ajoutés pour que le module traite celle-ci lorsqu'on appuie sur *[Entrée]* dans la console de PuTTY.

Ces paramètres *EOL* et *BAUDRATE* peuvent être différents d'un module à l'autre, selon le fabricant, la révision de leur firmware et leur programmation en usine. Par exemple, parfois le *BAUDRATE* par défaut d'un module sera *38400* ou *115200* et *EOL* pourra être *"\n\r"* ou encore *"\n"*. En principe, ces informations sont données dans la documentation du module, avec d'autres paramètres plus subtils comme le contrôle de parité, le nombre de bits de données et le nombre de bits de stop, le contrôle de flux ... qui peuvent aussi compromettre la communication et nécessiter une modification du script *main.py*. Mais ces documentations sont souvent erronées ou incomplètes !

## Appairage d'un module Grove Serial Bluetooth v3.01 avec un ordinateur personnel sous Windows

Pour établir une liaison série virtuelle entre un PC/un smartphone (l'hôte) et un module Bluetooth (connecté à notre carte NUCLEO-WB55), il faut d'abord **les appairer**. **Cette étape ne se passe pas toujours bien** à cause de problèmes potentiels de compatibilité entre l'hôte et le module. Tout dépend du matériel et du logiciel que vous utilisez et il n'existe malheureusement pas de procédure pour déterminer si votre PC sous Windows ou sous Linux va effectivement détecter votre module et accepter de s'appairer avec ...

Du fait qu'il n'est pas possible de couvrir tous les cas possibles, **nous allons nous concentrer sur l'appairage d'un module Grove Serial Bluetooth v3.01 avec un PC exécutant Windows 10**. Pour les autres environnements (PC Linux, MAC OS, iOS ... ) nous vous invitons à rechercher sur Internet. Sachez cependant que [les IPhones sont à priori incompatibles avec les modules BC417](https://forum.arduino.cc/t/iphone-cannot-detect-bluetooth-hc06/375000).

Pour appairer votre module avec Windows ...

* Commencez par le connecter correctement (comme expliqué) à la NUCLEO-WB55 et alimentez cette dernière avec l'USB. Une fois le module actif, sa LED D1 clignote une fois par seconde.

* Sur le PC Windows 10, allez dans « *Settings* » (Paramètres) et choisir l’entrée « *Bluetooth & other devices* ».

* Ensuite, cliquez sur « *+ Add Bluetooth or other device* » : 

<div align="left">
<img alt="Appairage Bluetooth 1" src="images/pairing_1.png" width="400px">
</div>
<br>

* Ensuite, dans la fenêtre « *Add a device* », sélectionner « *Bluetooth - Mice, keyboards ...* » : 

<div align="left">
<img alt="Appairage Bluetooth 2" src="images/pairing_2.png" width="400px">
</div>
<br>

* La recherche démarre et, **s’il n’est pas déjà appairé à votre PC**, le nom du module Bluetooth,  « *HMSoft* », devrait apparaître :  

<div align="left">
<img alt="Appairage Bluetooth 3" src="images/pairing_3.png" width="400px">
</div>
<br>

* Cliquez sur sur « *HMsoft* ». Après quelques instants de recherche, on vous demande de saisir le code PIN du module. Dans notre cas, il s’agit du code PIN par défaut programmé par Seeed Studio, soit « *1234* ». Renseignez-le et validez avec le bouton « *Connect* » :

<div align="left">
<img alt="Appairage Bluetooth 4" src="images/pairing_4.png" width="400px">
</div>
<br>

Après quelques instants, la fenêtre vous confirme « *Your device is ready to go !* ». Cliquez sur le bouton « *Done* » et retournez dans la fenêtre « *Settings* ».<br>
**Vous pourrez vérifier que le module y figure et qu’il est à présent signalé comme appairé.** On notera que le code PIN fait partie des données préprogrammées d'usine dans les modules BC417 et, qu'à moins d'avoir été changé avec une commande AT, il vaudra en général *"0000"* ou *"1234"*.

## Duplication de la console REPL sur le port série du module Grove Serial Bluetooth v3.01

Tout est prêt à présent pour réaliser l'application de REPL via un Bluetooth. L'étape suivante consiste à dupliquer la console RPL de MicroPython sur le port série de notre module Bluetooth.

### Le script MicroPython

> **Vous pouvez télécharger les scripts MicroPython de ce tutoriel (entre autres) en cliquant [ici](../../../assets/Script/MODULES.zip)**.

Créez un script *main.py* sur votre ordinateur et copiez-y le code suivant, puis glissez-déplacez le dans *PYBFLASH* :

```python
# Objet du script : Duplication de la console REPL du firmware MicroPython
# sur un UART associé à un module Grove Serial Bluetooth V3.01.
# Il faut préalablement avoir configuré correctement le module sur un hôte
# (un PC Windows par exemple) qui exécutera le terminal série.

from machine import UART # Pour piloter l'UART
from os import dupterm  # Fonction pour cloner le terminal REPL

# Constantes relatives au paramétrage de l'UART
DELAY_TIMEOUT = const(1000) # Durée (en millisecondes) pendant laquelle l'UART attend de reçevoir un message
BAUDRATE = const(9600)	# Débit, en bauds, de la communication série
UART_NUMBER = const(2)	# Identifiant de l'UART de la carte NUCLEO-WB55 qui sera utilisé
RX_BUFF = const(512)	# Taille du buffer de réception (les messages reçus seront tronqués 
						# à ce nombre de caractères)

# Initialisation de l'UART du module Bluetooth
bt_uart = UART(UART_NUMBER, BAUDRATE, timeout = DELAY_TIMEOUT, rxbuf = RX_BUFF)

# Duplication de la console REPL sur le port série du module Bluetooth
dupterm( bt_uart )

# Message de confirmation
print("Console REPL accessible par le module Bluetooth !")
```

Se script duplique la console REPL sur le port série du module Bluetooth en utilisant la fonction *dupterm*. 

### Mise en œuvre du script : REPL via Bluetooth

Appuyez sur *[CTRL]*+*[D]* dans le terminal PuTTY connecté (par un câble) au port USB user pour lancer le script. 
Vous devriez obtenir ceci :

```Console
MPY: sync filesystems
MPY: soft reboot
Console REPL accessible par le module Bluetooth !
MicroPython v1.20.0 on 2023-04-26; NUCLEO-WB55 with STM32WB55RGV6
Type "help()" for more information.
```
Rendez vous à présent sur l'ordinateur Windows sur lequel vous avez appairé le module Bluetooth.<br>
Lancez son *gestionnaire de périphériques* (*devices manager*) et identifiez dans la liste des ports série actifs celui du module Bluetooth :

<div align="left">
<img alt="Pour série du module Bluetooth" src="images/repl_over_bt_1.png" width="550px">
</div>
<br>

Deux ports possibles sont énumérés : le *COM47* et le *COM48*. **Nous ne savons pas pourquoi deux ports sont proposés et pas un seul !**<br>
En l'occurrence, "le bon" port est le *COM47*, identifié par essai et erreur. Nous nous y connectons avec PuTTY, **en prenant soin de préciser une vitesse de 9600 bauds, celle du module** :

<div align="left">
<img alt="Connexion au module Bluetooth avec PuTTY" src="images/repl_over_bt_2.png" width="550px">
</div>
<br>

Une fois sur la console PuTTY, nous retrouvons notre terminal REPL exactement comme si la connexion avait été réalisée avec un câble USB, on a tapé ici la commande *help()* :

<div align="left">
<img alt="Console REPL via UART Bluetooth" src="images/repl_over_bt_3.png" width="750px">
</div>
<br>

**Et voilà, ce long tutoriel est terminé !**

## Ressources

Ce tutoriel est essentiellement inspiré de l'article "REPL via Bluetooth" de l'ouvrage de Dominique MEURISSE, [*MicroPython et Pyboard, Python sur microcontrôleur : de la prise en main à l'utilisation avancée (Editions ENI)*](https://www.editions-eni.fr/livre/MicroPython-et-pyboard-python-sur-microcontroleur-de-la-prise-en-main-a-l-utilisation-avancee-9782409022906).
