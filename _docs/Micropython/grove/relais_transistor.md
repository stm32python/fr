---
title: Contrôle moteur avec un relais ou transistor
description: Mise en œuvre d'un moteur avec un relais ou un transistor
---

# Contrôle moteur avec un relais ou un transistor

Ce tutoriel explique comment contrôler un moteur avec un relais ou un transistor sous MicroPython.

<h2>Description</h2>

Un moteur est un élément de puissance qui ne peut pas être directement contrôlé par la carte NUCLEO-WB55. 
La carte ne peut pas fournir la puissance qui est nécessaire au moteur - surtout lors de son démarrage qui génère des pics de courant et de tension. Pour remédier à ce problème on peut mettre en place un relais ou un transistor entre les deux éléments. Ces composants ont pour but de séparer la partie commande (coté NUCLEO-WB55) de la partie puissance (coté moteur).

Un relais et un transistor servent donc d'interrupteurs contrôlés par la NUCLEO-WB55. Pour ce faire revenons rapidement sur les principes de fonctionnement de ces deux composants.

**Un relais :**
 - est contenu dans un petit boitier rectangulaire (souvent de couleur jaune, noire ou bleue)
 - est un interrupteur mécanique contrôlé électriquement
 - contient une bobine qui, lorsqu'elle est alimentée, ferme l'interrupteur grâce au champ électromagnétique engendré par une bobine.
 - est identifiable grâce à un bruit de claquement lorsque l'on ferme ou on ouvre son interrupteur.

<div align="left">
<img alt="Relais" src="images/relais-image-relais.png" width="20%">
</div>


**Un transistor :**
 - Est identifiable par son boitier souvent rond et biseauté sur un coté. Il existe également d'autres types de boitiers.
 - Est disponible en différentes "versions" (bipolaire, à effet de champ, Darlington)
 - Doit être connecté en avec attention car ses broches (Base, Collecteur et Emetteur) sont polarisées selon son type (NPN ou PNP pour les transistors bipolaires).
 - Peut être utilisé comme un interrupteur commandé par sa tension de seuil ; le courant circule entre le collecteur et l'émetteur seulement lorsque la tension appliquée à sa base dépasse sa tension de seuil. On peut aussi utiliser les transistors NPN / PNP comme amplificateurs linéaires de courant, mais pas pour les besoins d'un montage de contrôle moteur.

<div align="left">
<img alt="Transistor" src="images/relais-image-transitor.png" width="20%">
</div>

<h2>Montage</h2>

**Prérequis :**

1. La carte NUCLEO-WB55
2. Une breadboard
3. Un moteur à courant continu
4. Une alimentation externe (batterie ou pile)


**Et en fonction du montage souhaité :**
- Un relais (dans cet exercice nous utilisons la référence suivante : **HK4100F-DC5V-SHG**)

**Ou :**
- Un transistor bipolaire NPN (dans cet exercice nous utilisons la référence suivante : **2N2222A**) ainsi qu'une résistance de 220 Ω pour protéger la base du transistor

<div align="left">
<img alt="Schéma de montage moteur avec relais et transistor" src="images/relais-schema.png" width="80%">
</div>

**Remarque :** Pensez bien à connecter les masses (GND) ensemble sous peine de dysfonctionnements.


<h2>Le code MicroPython</h2>

> **Vous pouvez télécharger les scripts MicroPython de ce tutoriel (entre autres) en cliquant [ici](../../../assets/Script/MODULES.zip)**.

Pour le code nous allons tout simplement contrôler le moteur en appuyant sur le bouton SW1 qui sera géré par une interruption. Un retour d'état sera fait via les LED rouge et verte.

**Etape 1 :** On importe les bibliothèques dans notre code.

```python
from pyb import Pin, Timer
import gc # Ramasse miettes, pour éviter de saturer la mémoire
```

**Etape 2 :** On définit une variable globale qui nous servira à récupérer l'état du bouton poussoir.

```python
# Variables globales
motor_state = 0
```

**Etape 3 :** Initialisation des boutons, des LED (verte et rouge) et du moteur (sur D4).

```python
# Bouton poussoir (en entrée + pull up)
sw1 = pyb.Pin('SW1')
sw1.init(pyb.Pin.IN, pyb.Pin.PULL_UP, af=-1)

# LED (LED de la carte NUCLEO)
green_led = pyb.LED(2)
red_led = pyb.LED(1)

# GPIO qui contrôle le relais/transistor
motor_pin = pyb.Pin('D4', Pin.OUT_PP)
```

**Etape 4 :** Afin de contrôler le moteur à n'importe quel instant on gère le bouton poussoir avec une interruption qui mettra à jour la variable globale définie précédemment.

```python
def ITbutton1(line):
	# Variables globales
	global motor_state
	# Etat moteur à 0 ou 1
	if(motor_state == 1):
		motor_state = 0
	else:
		motor_state = 1

# Initialisation des vecteurs d'interruption
irq_1 = pyb.ExtInt(sw1, pyb.ExtInt.IRQ_FALLING, pyb.Pin.PULL_UP, ITbutton1)
```

**Etape 5 :** Enfin dans la boucle infinie on gère l'état des LED et du moteur.
```python
# Boucle infinie
while True:

	if motor_state == 0:
		green_led.off()
		red_led.on()
		motor_pin.low()
		
	if motor_state == 1:
		green_led.on()
		red_led.off()
		motor_pin.high()
		
	# Appel du ramasse-miettes
	gc.collect()
```

<h2>Résultat</h2>

Voici le script complet :

```python
# Objet du script : Mise en œuvre d'un contrôle moteur avec un relais ou avec un transistor
# Matériel requis :
#  - La carte NUCLEO-WB55
#  - Une breadboard
#  - Un moteur à courant continu
#  - Une alimentation externe (batterie ou pile)
#  - Un relais HK4100F-DC5V-SHG ou un transistor NPN 2N2222A + une résistance de 220 Ohm, selon le montage choisi

from pyb import Pin, Timer
import gc # Ramasse miettes, pour éviter de saturer la mémoire

# Variables globales
motor_state = 0

# Bouton poussoir (en entrée + pull up)
sw1 = pyb.Pin('SW1')
sw1.init(pyb.Pin.IN, pyb.Pin.PULL_UP, af=-1)

# LED (LED de la carte NUCLEO)
green_led = pyb.LED(2)
red_led = pyb.LED(1)

# GPIO qui contrôle le relais/transistor
motor_pin = pyb.Pin('D4', Pin.OUT_PP)

# Interruption de SW1
def ITbutton1(line):
	# Variables globales
	global motor_state
	# Etat moteur à 0 ou 1
	if(motor_state == 1):
		motor_state = 0
	else:
		motor_state = 1

# Initialisation des vecteurs d'interruption
irq_1 = pyb.ExtInt(sw1, pyb.ExtInt.IRQ_FALLING, pyb.Pin.PULL_UP, ITbutton1)

# Boucle infinie
while True:

	if motor_state == 0:
		green_led.off()
		red_led.on()
		motor_pin.low()
		
	if motor_state == 1:
		green_led.on()
		red_led.off()
		motor_pin.high()
		
	# Appel du ramasse-miettes
	gc.collect()
```

Il ne vous reste plus qu'à l'enregistrer sur le disque *PYBFLASH* et à le lancer avec CTRL + D sur PuTTY.
La LED rouge devrait s'allumer et le moteur devrait être éteint. Appuyez sur le bouton poussoir, la LED verte devrait alors s'allumer et le moteur se mettre en mouvement.
