---
title: Potentiomètre (rotatif ou linéaire)
description: Mise en œuvre d'un module Grove potentiomètre avec MicroPython
---

# Potentiomètre (rotatif ou linéaire)

Ce tutoriel explique comment mettre en œuvre un potentiomètre analogique Grove avec MicroPython. Le potentiomètre permet de moduler manuellement une tension d’entrée comprise entre 0V et +3.3V (pour des microcontrôleurs STM32). Après conversion de cette tension (par l’ADC de la broche analogique sur laquelle est connecté le potentiomètre) le microcontrôleur reçoit une valeur entière comprise entre 0 et 4095.
Cette valeur peut ensuite servir à piloter d’autres actuateurs, par exemple à faire varier l’intensité sonore d’un buzzer.<br>
Les modules potentiomètres Grove sont déclinés selon deux architectures : **potentiomètres rotatifs** (modules *rotary angle sensor* en anglais) et **potentiomètres linéaires** ( modules *slide potentiometer*).

**Les modules potentiomètres Grove :**

|Potentiomètre rotatif|Potentiomètre linéaire|
|:-:|:-:|
|<img alt="Rotary angle sensor" src="images/potentiometer_rotary.jpg" width="190px">| <img alt="Slide potentiometer" src="images/potentiometer_slide.jpg" width="290px">|

> Crédit images : [Seeed Studio](http://wiki.seeedstudio.com/Grove_System/)

## Matériel requis

1. Une [carte d'extension de base Grove](https://wiki.seeedstudio.com/Base_Shield_V2/)
2. La carte NUCLEO-WB55
3. Un potentiomètre Grove [rotatif](https://wiki.seeedstudio.com/Grove-Rotary_Angle_Sensor/) ou [linéaire](https://wiki.seeedstudio.com/Grove-Slide_Potentiometer/) connecté sur la **broche / le connecteur A0**.

## Le code MicroPython

> **Vous pouvez télécharger les scripts MicroPython de ce tutoriel (entre autres) en cliquant [ici](../../../assets/Script/MODULES.zip)**.

Editez maintenant le script *main.py* sur le disque *PYBFLASH* et collez-y le code qui suit :

```python
# Objet du script :
# Exemple de configuration d'un ADC pour numériser une tension d'entrée variable grâce à un potentiomètre.
# Pour augmenter la précision, on calcule la moyenne de la tension d'entrée sur 10 mesures 
# (une mesure toutes les 5 millisecondes).
# Matériel requis : potentiomètre (Grove ou autre) connecté sur A0 avec une alimentation du Grove base shield
# positionnée sur 3.3V

import pyb # pour gérer les GPIO
from time import sleep_ms # Pour temporiser

print( "L'ADC avec MicroPython c'est facile" )

# Tension de référence / étendue de mesure de l'ADC : +3.3V
VAREF = const(3.3)

# Résolution de l'ADC 12 bits = 2^12 = 4096 (mini = 0, maxi = 4095)
RESOLUTION = const(4096)

# Quantum de l'ADC
quantum = VAREF / RESOLUTION

# Initialisation de l'ADC sur la broche A0
adc_A0 = pyb.ADC(pyb.Pin( 'A0' ))

# Initialisations pour calcul de la moyenne
nb_val = 100
inv_nb_val = 1 / nb_val

while True: # Boucle "infinie" (sans clause sortie)
	
	sum_voltage = 0
	average_voltage = 0
	
	# Calcul de la moyenne de la tension aux bornes du potentiomètre

	for i in range(nb_val): # On fait Nb_Mesures conversions de la tension d'entrée
		
		# Lit la conversion de l'ADC (un nombre entre 0 et 4095 proportionnel à la tension d'entrée)
		value_sampled = adc_A0.read()
		
		# On calcule à présent la tension (valeur analogique) 
		voltage = value_sampled * quantum

		# On l'ajoute à la valeur calculée à l'itération précédente
		sum_voltage += voltage

		# Temporisation pendant 5 ms
		sleep_ms(5)
	
	# On divise par Nb_Mesures pour calculer la moyenne de la tension du potentiomètre
	average_voltage = sum_voltage * inv_nb_val 
	
	# Affichage de la tension moyenne sur le port série de l'USB USER
	print( "La valeur moyenne de la tension est : %.2f V" %average_voltage)
````
## Affichage sur le terminal série de l'USB User

Lancez le script sous PuTTY avec CTR+D et faite tourner la molette ou glisser la commande du potentiomètre. Les valeurs de sa tension de sortie changent en conséquence :

```console
MPY: sync filesystems
MPY: soft reboot
L'ADC avec MicroPython c'est facile
La valeur moyenne de la tension est : 2.28 V
La valeur moyenne de la tension est : 2.28 V
La valeur moyenne de la tension est : 1.84 V
La valeur moyenne de la tension est : 1.80 V
La valeur moyenne de la tension est : 1.56 V
La valeur moyenne de la tension est : 0.61 V
La valeur moyenne de la tension est : 1.02 V
La valeur moyenne de la tension est : 2.01 V
La valeur moyenne de la tension est : 0.90 V
La valeur moyenne de la tension est : 0.89 V
```
