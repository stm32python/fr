---
title: Lecteur RFID 125 kHz Grove
description: Mise en œuvre du Lecteur RFID 125 kHz Grove en MicroPython
---

# Lecteur RFID 125kHz Grove

Ce tutoriel explique comment mettre en œuvre le [module lecteur de badge RFID 125 KHz de Grove](https://wiki.seeedstudio.com/Grove-125KHz_RFID_Reader/) en MicroPython. Celui-ci permet de lire des informations inscrites sur **une étiquette RFID** (tag) de type uem400. Ce module est capable de détecter sans fil jusqu'à une distance de 7 cm. Il se raccorde sur le port série de la NUCLEO-WB55 et **doit être alimenté en 5 volts**.

|Grove - Lecteur RFID 125kHzGrove - Lecteur RFID 125kHz|Badges et cartes RFID de type uem400|
|:-:|:-:|
|<img alt="Grove - Lecteur RFID 125kHz" src="images/Grove_125KHz_RFID_Reader.jpg" width="450px">| <img alt="Badges et cartes RFID de type uem400" src="images/uem400.jpg" width="320px">|

> Crédit image : [Seeed Studio](http://wiki.seeedstudio.com/Grove_System/)

## Matériel requis

1. Une [carte d'extension de base Grove](https://wiki.seeedstudio.com/Base_Shield_V2/) **avec le commutateur d'alimentation placé sur 5V**.
2. La carte NUCLEO-WB55
3. Un [Module lecteur RFID 125 kHz Grove](https://fr.vittascience.com/shop/43/lecteur-rfid-125khz-grove)
4. Un ensemble de [badges et cartes RFID de type uem400](https://fr.vittascience.com/shop/128/tag-rfid-(3-badges-et-2-cartes)).

Connectez le module sur *la prise UART* de la carte d'extension de base Grove. 

## Le code MicroPython

> **Vous pouvez télécharger les scripts MicroPython de ce tutoriel (entre autres) en cliquant [ici](../../../assets/Script/MODULES.zip)**.

Ce code a été adapté à partir [de ce site](https://gcworks.fr/tutoriel/esp/LecteurRFID125kHz.html).
Le module lecteur RFID doit être branché sur le connecteur *UART* du Grove Base Shield, en prenant bien soin de positionner le commutateur d'alimentation de celui-ci sur la position 5V.

Lors du passage du badge devant le lecteur une trame de 14 octets est transmise, structurée comme suit : 
- 1 octet pour signaler le début de la trame
- 10 octets contenant l'identifiant du tag (le message "utile")
- 2 octets de *somme de contrôle* (*checksum* en anglais), destinés à vérifier que les 10 octets du message ont été lus correctement.<br>

	Le principe d'une somme de contrôle repose sur l'utilisation d'une *fonction de hachage*. Supposez que vous disposiez d'un message *M* composé d'un grand nombre de caractère (1500 par exemple). Lorsqu'on applique une fonction de hachage à *M*, celle-ci produit une somme de contrôle *S* qui a les propriétés suivantes : (1) *S* est bien plus courte que *M* (par exemple 32 octets) et (2) à chaque *M* correspond une *S* unique. Si un seul des octets de *M* est changé, alors la fonction de hachage donnera une nouvelle somme de contrôle **complètement différente de la première**.<br>

	Par conséquent, *la somme de contrôle peut être utilisée pour vérifier qu'un message a été correctement transmis*. On commence par émettre sur le canal de communication le message *M* et sa somme de contrôle *S*. Le récepteur recalcule une somme de contrôle à partir des octets de *M* qu'il a reçus, en utilisant la même fonction de hachage que celle de l'émetteur. Si le résultat est différent de *S* reçue, alors cela signifie qu'un ou plusieurs des octets de *M* ont été modifiés pendant la transmission.<br>

	Dans cet exemple, nous n'utiliserons pas ce processus de vérification, mais il est fondamental et utilisé pour toutes les transmissions sur tous les réseaux modernes. D'autres codes sont utilisés dans les communications, qui permettent de corriger partiellement les erreurs de transmission en introduisant de subtiles redondances au niveau de la structure binaire des messages, on les appelle des [*codes correcteurs*](https://fr.wikipedia.org/wiki/Code_correcteur) ; ce sont des outils issus de la théorie de l'information.
- 1 octet pour signaler la fin de la trame

Notre programme lit les 10 octets de l'identifiant du tag et les affiche dans le terminal PuTTY (celui de l'USB User).

Editez maintenant le script *main.py* et copiez-y le code qui suit :

```python
# Objet du script : mise en œuvre d'un lecteur RFID 125 kHz de Grove
# Ce script est adapté du site https://gcworks.fr/tutoriel/esp/LecteurRFID125kHz.html

from pyb import UART # Classe pour gérer l'UART

uart = UART(2) # Instance de l'UART numéro 2

# Initialisation à 9600 bauds, trames de 8 bits, sans contrôle de parité avec un stop bit à 1
uart.init(9600, bits=8, parity = None, stop = 1)

# Nombre d'octets dans l'identifiant du TAG
_RFID_TAG_SIZE = const(10)

while True:
	
	# Lecture d'un tag RFID. 
	# Les données arrivent sur le port série et sont stockées dans un tableau d'octets

	# Lorsque tous les octets de l'identifiant du tag / badge RFID sont dans la file de réception
	if uart.any() > _RFID_TAG_SIZE: 
		
		# Charge les octets dans un tableau
		tag_data = uart.read()
		print("Données tag RFID :            ", tag_data.decode("ascii"))

		# On isole et on décode les 5 octets de poids faible du champ "10 ASCII Data Characters"
		rfid_hexa = ""
		for i in range(5, 11):
			rfid_hexa += chr(tag_data[i])
		print("Identifiant RFID en hexa :    ", rfid_hexa)

		# L'identifiant RFID indiqué sur le tag est obtenu par conversion hexadecimal -> décimal
		rfid = str(int(rfid_hexa, 16))
		while len(rfid)< _RFID_TAG_SIZE:
			rfid = "0" + rfid
		print("Identifiant RFID en décimal : %s \n" %rfid)
```

## Sortie sur le port série du terminal PuTTY

Appuyez sur *[CTRL]-[D]* dans le terminal PuTTY pour lancer le script *main.py*. Ce résultat est obtenu en passant différents badges RFID devant l'antenne du lecteur :


```console
MicroPython v1.20.0 on 2023-04-26; NUCLEO-WB55 with STM32WB55RGV6
Type "help()" for more information.
>>>
MPY: sync filesystems
MPY: soft reboot
Données tag RFID :             05007EA672AF
Identifiant RFID en hexa :     7EA672
Identifiant RFID en décimal : 0008300146

Données tag RFID :             08001BDF61AD
Identifiant RFID en hexa :     1BDF61
Identifiant RFID en décimal : 0001826657

Données tag RFID :             080030400179
Identifiant RFID en hexa :     304001
Identifiant RFID en décimal : 0003162113
```