---
title: Les microcontrôleurs STM32
description: Présentation de la famille de microcontrôleurs STM32 de STMicroelectronics
---

# Les microcontrôleurs STM32

La société STMicroelectronics (STM) est à l'origine de la gamme de [microcontrôleurs (MCU) STM32](https://www.st.com/en/microcontrollers-microprocessors/stm32-32-bit-arm-cortex-mcus.html) qui est à cette date (2023) la plus utilisée au monde. Tout au long de la journée vous utilisez probablement, sans le savoir, et de façon répétitive, des MCU STM32. On les trouve en effet dans un très grand nombre de produits tels que : les véhicules, les objets connectés, les équipements réseaux et informatiques, les  téléphones portables ...

Et, pour confirmer son leadership, STM ne cesse de diversifier son offre ; des centaines de références de MCU STM32 sont disponibles. La figure ci-dessous rappelle les quatre familles de MCU STM32 à la date de la rédaction de cette page (août 2023, cette liste évolue sans cesse) :

<br>
<div align="left">
<img alt="Portefeuille MCU STM32" src="images/new-portfolio-mcu.jpg" width="450px">
</div>
<br>

> Source : [STMicroelectronics](https://www.st.com/content/st_com/en.html)


Chacune de ces familles est conçue autour de [microprocesseurs ARM Cortex M](https://fr.wikipedia.org/wiki/ARM_Cortex-M) spécifiquement optimisés pour les applications embarquées. Cette offre très large se justifie par la nécessité de disposer de puces aussi peu chères que possible qui répondent au mieux aux besoins de chaque application.

Par exemple, si vous souhaitez piloter l'afficheur à cristaux liquides d'une machine à café, vous opterez sûrement pour un STM32L0 qui ne calcule pas très vite mais qui est très économe en énergie. Et si vous souhaitez piloter un drone ou exécuter un algorithme de reconnaissance d'images, vous choisirez probablement pour un STM32H7, 40 fois plus véloce que le STM32L0 mais plus énergivore en contrepartie.

Pour en apprendre un peu plus sur les MCU et les MCU STM32 en particulier, nous vous conseillons de lire [cet article](../Microcontrollers/microcontroleur).

## Les cartes de prototypage de STMicroelectronics

Afin de faciliter l'apprentissage de la programmation des STM32, STM propose aussi des cartes de prototypage de différents types. Vous en apprendrez plus à ce sujet par [cet article](nucleo). Dans les différents tutoriels de ce site, nous utilisons notamment :

- Des cartes [NUCLEO-L476RG](nucleo_l476rg), équipées d'un [MCU STM32L476RG](https://www.st.com/en/microcontrollers-microprocessors/stm32l476rg.html) très bon marché et particulièrement riche en fonctionnalités.
- Des cartes [NUCLEO-WB55RG](nucleo_wb55rg), équipées d'un [MCU STM32WB55RG](https://www.st.com/en/microcontrollers-microprocessors/stm32wb55rg.html) intégrant une radio [Bluetooth basse consommation](../Embedded/ble).
- Des cartes [NUCLEO-WL55JC1](nucleo_wl55jc1), équipées d'un [MCU STM32WL55JC](https://www.st.com/en/microcontrollers-microprocessors/stm32wl55jc.html) intégrant une radio [LoRa](../Embedded/lora).


## Les environnements de développement pour les MCU STM32 et leurs cartes de prototypage

Cette thématique est expliquée un peu plus en détails à la fin de [cet article](../Microcontrollers/programmer).
Sur ce site, l'essentiel des ressources utilisent [l'environnement MicroPython](../Micropython/index) ou [l'environnement Arduino](../Stm32duino/index) pour programmer les MCU STM32.