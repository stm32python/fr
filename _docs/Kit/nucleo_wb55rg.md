---
title: La carte NUCLEO-WB55RG
description: La carte NUCLEO-WB55RG
---

# La carte NUCLEO-WB55RG

Cette fiche présente la **carte NUCLEO-WB55RG de STMicroelectronics** que nous utiliserons pour des tutoriels sur [**le Bluetooth Low Energy (BLE)**](../Embedded/index).

## Le microcontrôleur STM32WB55RG

>> **En préalable à cette section, nous vous conseillons de lire cette qui traite des microcontrôleurs STM32 en général, [ici](stm32)**.

La carte NUCLEO-WB55RG est un support de test et démonstration [du microcontrôleur (MCU) STM32WB55RG](https://www.st.com/en/microcontrollers-microprocessors/stm32wb55rg.html), un SoC (“System on Chip”, en français “Système sur puce”) réunissant trois systèmes aux fonctions différentes sur un même substrat de silicium / sur une même puce :

1. Un cœur ARM Cortex-M4 “utilisateur” cadencé à 64 MHz avec 192 ko de SRAM et un grand nombre de périphériques intégrés et d’entrées-sorties ;
2. Un cœur ARM Cortex-M0 sécurisé cadencé à 32 MHz qui gère …
3. Un modem radio 2.4 GHz.

Les figures qui suivent illustrent l’architecture du SoC STM32WB55RG :

|Diagramme blocs du SoC STM32WB55RG|Architecture du STM32WB55RG|
|:-:|:-:|
|<img alt="Diagramme blocs du STM32WB55RG" src="images/block_diagram_STM32WB55RG.jpg" width="450px">| <img alt="Architecture du STM32WB55RG" src="images/architecture du STM32WB55RG.jpg" width="450">|

> Crédit images : [STMicroelectronics](https://www.st.com/content/st_com/en.html)

Celle-ci a été entièrement pensée pour **contenir la consommation énergétique et le coût** (architecture SoC, fréquence de fonctionnement "raisonnable", mémoires embarquées de tailles optimales ...) mais aussi pour renforcer la **cybersécurité embarquée** (pile radio gérée par un composant sécurisé et bien d'autres mesures).

Les deux cœurs Cortex partagent diverses ressources : mémoire (1 MB de Flash, 32kB de SRAM2), "hub" d'interruptions externes EXTI, générateur de nombres aléatoires, unité de chiffrement AES-256, unité de gestion des communications inter-processeurs IPCC et HSEM, etc.

Le SoC peut réaliser plusieurs modulations et supporte les protocoles Bluetooth LE 5.4, 802.15.4, Zigbee, Thread et Matter.

## Anatomie de la NUCLEO-WB55

>> **En préalable à cette section, nous vous conseillons de lire celle qui traite des cartes NUCLEO en général, [ici](nucleo)**.

La carte NUCLEO-WB55 permet d'évaluer un microcontrôleur STM32WB55 qui rassemble sur la même puce en silicium deux microprocesseurs ARM Cortex ultra - basse consommation et offre **une connectivité radio Bluetooth à basse consommation (BLE pour "Bluetooth Low Energy")**.

Le protocole BLE permet l’échange bidirectionnel de données à courte distance (une dizaine de mètres) en utilisant des ondes radio sur la bande de fréquence 2,4 GHz, optimisé logiciellement et matériellement pour consommer très peu d'énergie.<br>
- Une présentation détaillée du protocole BLE est disponible [**sur cette page**](../Embedded/ble).
- Vous trouverez des applications du BLE avec la NUCLEO-WB55 sur [**cette page**](../MicroPython/BLE/index).

Les figures ci-dessous décrivent la NUCLEO-WB55 : 

<br>
<div align="left">
<img alt="NUCLEO-WB55" src="images/board.png" width="1000px">
</div>
<br>

- **Antenne PCB** : C’est l’antenne Bluetooth pour le microcontrôleur STM32WB55. Elle est dite "PCB" (pour "Printed Circuit Board") car elle est intégrée directement dans la plaque d'époxy de la carte. Elle est conçue de sorte à optimiser la réception et l’émission des ondes radio pour la fréquence du Bluetooth soit 2,4 GHz. 

- **Connecteurs Arduino et connecteurs MORPHO** : Des kits d’extensions peuvent être ajoutés simplement grâce à ces connecteurs normalisés. On pourra, par exemple, brancher le kit MEMS inertiel et environnemental X-NUCLEO-IKS01A3 sur la NUCLEO-WB55 :

<br>
<div align="left">
<img alt="Carte Nucleo WB55 et shield X-NUCLEO-IKS01A3" src="images/WB55_IKS01A3.png" width="300px">
</div>
<br>

 - **STM32WB55RG** : C'est le Microcontrôleur qui anime la carte. Comme déjà évoqué, c'est un système sur puce (SoC) avec deux cœurs ARM Cortex, un dédié à l'application de l'utilisateur (un Cortex M4) et l'autre (un Cortex M0+) à la gestion de la communication selon le protocole BLE.

- **Bouton reset** : Lorsqu'on appuie dessus, le microcontrôleur interrompt son travail en cours et lance sa séquence d'initialisation.

- **Level Shifter** : Ce circuit est spécialisé dans la conversion entre deux niveaux de tensions par exemple pour offrir des alimentations en 5V sur certaines broches et en 3.3V sur d'autres.

- **USB User** : C’est le port USB qui nous servira à:

  1. Communiquer avec la machine virtuelle MicroPython
  2. Programmer le système
  3. Alimenter le kit de développement

- **Compartiment CR2032** : Une fois le système programmé, il sera possible d’alimenter le kit par une pile CR2032 afin de rendre l'ensemble portable. Notez bien qu'une "power bank" rechargeable branchée sur le port USB User fera tout aussi bien (en fait, mieux) l'affaire si l'encombrement de votre montage n'est pas contraint.

- **ST-LINK** : C’est l’outil qui permet de programmer le microcontrôleur, en particulier d'y installer le [**firmware MicroPython**](../MicroPython/firmware/index) que vous pouvez [télécharger ici](../Micropython/Telechargement).
   - [**La procédure d'installation du firmware sous Linux est ici**](../Micropython/install_linux/index).
   - [**La procédure d'installation du firmware sous Windows est ici**](../Micropython/install_win_wb55/index).

  Pour assurer sa mission de "programmeur de firmware", le ST-LINK est animé par un MCU STM32L0 qui a son propre firmware, qu'il faudra parfois mettre à jour selon [cette procédure](../tools/cubeprog/cube_prog_firmware_stlink).

  Le ST-LINK permet aussi le débogage interactif du firmware exécuté par le MCU STM32WB55.

- **LED utilisateur (User LED)** : Ces trois LED, une rouge, une bleue et une verte, sont accessibles au développeur et pourront être commutées grâce à MicroPython, Arduino ou tout autre framework que vous choisiriez ; nous les utiliserons tout au long des exercices par la suite. Précisons que :

   - La LED **rouge** est sérigraphiée *LED3* sur le PCB
   - La LED **verte** est sérigraphiée *LED2* sur le PCB
   - La LED **bleue** est sérigraphiée *LED1* sur le PCB

- **Boutons utilisateurs** : Ces trois boutons peuvent également être utilisés dans le firmware, tout comme les LED utilisateur.

## Les connecteurs Arduino de la carte NUCLEO-WB55RG et leurs fonctions secondaires / alternatives

Les schémas qui suivent font la correspondance entre les broches de la NUCLEO-WB55 et les fonctions / connecteurs de l'Arduino UNO. Les cartographies ci-dessous situent les fonctions secondaires qui nous seront utiles sur ces derniers. Le brochage de notre NUCLEO-WB55 est presque identique à celui des Arduino UNO **à l'exception notable des sorties [PWM](../Kit/glossaire)**. Attention donc aux potentielles incompatibilités avec les shields conçus pour les Arduino UNO qui ont besoin de signaux PWM sur des broches bien précises !

<br>
<div align="left">
<img alt="Arduino mapping" src="images/wb55_arduino_connectors_mapping.png" width="900px">
</div>
<br>

## La cartographie des broches MORPHO de la carte NUCLEO-WB55RG

La figure qui suit rappelle la correspondance entre les broches du SoC STM32WB55 et les broches des [connecteurs MORPHO](nucleo) (en bleu) de la carte NUCLEO-WB55.

<br>
<div align="left">
<img alt="MORPHO mapping" src="images/wb55_morpho_mapping.png" width="600px">
</div>
<br>

La nomenclature des broches (PA2, PC5, PC13, etc.) vous sera très utile si vous décidez de développer des applications avec l'environnement [STM32duino (Arduino pour STM32)](../Stm32duino/index) ou encore **STM32Cube**, qui permettent d'utiliser les broches MORPHO.

En fait, cette nomenclature est celle des entrées/sorties à usage général (les "GPIO") du MCU STM32WL55JC. Les fonctions qui leurs sont attribuées par défaut dépendent de l'environnement dans lequel on travaille (MicroPython, STM32Cube, STM32duino ...).

## IMPORTANT : Mise à jour du microprogramme pour le BLE sur le Cortex M0+

Le **STM32WB55** qui anime notre carte est système sur puce (SoC) avec deux cœurs ARM Cortex, un dédié à l'application de l'utilisateur (un Cortex M4) et l'autre (un Cortex M0+) à la gestion de la communication selon le protocole radiofréquence du BLE.

Tous les exercices sur ce site vont consister à programmer le Cortex M4, qui est facilement accessible avec les outils fournis par MicroPython, par Arduino, par STMicroelectronics, etc. En revanche, le firmware (ou microprogramme) BLE  du Cortex M0+ n'est pas supposé être modifié aisément ou régulièrement par l'utilisateur compte tenu de sa fonction critique pour le bon fonctionnement du SoC.

Vous pourrez cependant être amené à charger un nouveau firmware BLE (appelé aussi "pile BLE") dans le Cortex M0+ pour diverses raisons, notamment pour que la fonction BLE continue de fonctionner correctement avec vos applications suite aux évolutions apportées par STMicroelectronics sur la NUCLEO-WB55. Le cas échéant, la procédure pour reprogrammer la pile BLE est expliquée [par ce tutoriel](../tools/cubeprog/cube_prog_firmware_ble_hci).

