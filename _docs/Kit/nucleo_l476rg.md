---
title: La carte NUCLEO-L476RG
description: Revue de la carte NUCLEO-L476RG
---

# La carte NUCLEO-L476RG

Cette fiche présente la **carte NUCLEO-L476RG de STMicroelectronics** que nous utiliserons essentiellement pour des tutoriels sur [**Arduino pour STM32**](../Stm32duino/index).

## Le microcontrôleur STM32L476RG

>> **En préalable à cette section, nous vous conseillons de lire cette qui traite des microcontrôleurs STM32 en général, [ici](stm32)**.

La carte NUCLEO-WL476RG est un support de test et démonstration [du microcontrôleur (MCU) STM32L476RG](https://www.st.com/en/evaluation-tools/nucleo-l476rg.htm) qui intègre sur un même substrat de silicium / sur une même puce un cœur ARM Cortex-M4 cadencé à 80 MHz, 128 ko de SRAM, 1 Mb de mémoire Flash et un grand nombre de périphériques et d’entrées-sorties. **Il est optimisé pour un contrôle fin de sa consommation d'énergie** et est capable de gérer pas moins de 9 modes de veille et 3 modes "run" optimisés selon l'application.

La figure qui suit donne le **diagramme blocs du STM32L476RG** qui rappelle toutes les fonctions qu'il intègre :

<br>
<div align="left">
<img alt="Diagramme blocs du STM32L476RG" src="images/block_diagram_STM32L476RG.jpg" width="450px">
</div>
<br>

> Crédit images : [STMicroelectronics](https://www.st.com/content/st_com/en.html)

 Pour vous faire une idée de la complexité de microcontrôleur, vous pouvez télécharger **[ici](stm32l476xx.pdf) sa fiche technique**. Vous trouverez plus d'information sur le STM32L476RG dans [cet article](https://stm32python.gitlab.io/fr/docs/Stm32duino/projets/stm32l476rg).

## Anatomie de la NUCLEO-L476RG

>> **En préalable à cette section, nous vous recommandons de lire celle qui traite des cartes NUCLEO en général, [ici](nucleo)**.

Une partie de nos tutoriels utilisent la **carte NUCLEO-L476RG de STMicroelectronics**. Vous trouverez sa description détaillée et sa fiche technique [ici](https://www.st.com/en/evaluation-tools/nucleo-l476rg.htm). Elle est équipée d’un [**microcontrôleur (MCU) STM32L476RG**](https://www.st.com/en/microcontrollers-microprocessors/stm32l476rg.html) cadencé à 80 MHz offrant **1 Mb de mémoire Flash**, 128 kb de SRAM et **un grand nombre d’entrées-sorties**. La figure ci-dessous illustre cette carte et sa légende précise un certain nombre de termes techniques que vous rencontrerez souvent par la suite :


**La carte NUCLEO-L476R (face avant)**
<br>
<div align="left">
<img alt="La carte NUCLEO-L476R" src="images/nucleo_l476rg.jpg" width="700px">
</div>
<br>

Comme sur toutes les cartes NUCLEO, le port USB **ST-LINK** sert à la programmer et offre également un canal de communication série avec l'ordinateur auquel elle est connectée. Le ST-LINK permet aussi le débogage interactif du firmware exécuté par le MCU STM32L476RG. On remarquera que, pour assurer sa mission de "programmeur de firmware", le ST-LINK est animé par un MCU STM32L0 qui a son propre firmware, qu'il faudra parfois mettre à jour selon [cette procédure](../tools/cubeprog/cube_prog_firmware_stlink).

## Les connecteurs de la carte NUCLEO-L476RG

Le MCU STM32L476RG propose un grand nombre de fonctions et 64 broches sur son boitier, qui ne sont donc pas toutes accessibles par les 32 broches des connecteurs Arduino. Les broches des connecteurs Morpho corrigent partiellement ce problème en offrant 32 connexions supplémentaires : 

<br>
<div align="left">
<img alt="Connecteurs cartes NUCLEO 64" src="images/connecteurs_l476rg.jpg" width="700px">
</div>
<br>

La nomenclature des broches MORPHO (PA2, PC5, PC13, etc.) vous sera très utile si vous décidez de développer des applications avec l'environnement [STM32duino (Arduino pour STM32)](../Stm32duino/index) ou encore **STM32Cube**, qui permettent de les utiliser.

## Cartographie des fonctions alternatives - broches pour la NUCLEO-L476RG

La présente section donne une représentation graphique du câblage "par défaut" des fonctions alternatives (ou pas) pour la carte NUCLEO-L476RG. Veuillez noter que cette correspondance n’est pas imposée « en dur » : il est possible de rediriger les périphériques internes du MCU vers d’autres broches de son boitier – et donc de la carte NUCLEO. Cette reconfiguration se fait avec [les bibliothèques LL et HAL](https://community.st.com/s/question/0D50X00009XkhQSSAZ/hal-vs-ll) et est facilitée par l’utilisation de l’outil de configuration graphique [STM32Cube MX](https://www.st.com/en/development-tools/stm32cubemx.html). Tous les périphériques internes ne peuvent cependant pas être redirigés sur n'importe quelles broches (le multiplexage des fonctions a ses limites !), mais les configurations possibles sont très nombreuses.
 
 
### **Broches pour le bouton et la LED utilisateur**

<br>
<div align="left">
<img alt="Broches pour le bouton et la LED utilisateur" src="images/fig_a1_1.jpg" width="650px">
</div>
<br>

### **Broches pour les six contrôleurs de ports séries**

<br>
<div align="left">
<img alt="Broches pour les six contrôleurs de ports séries" src="images/fig_a1_2.jpg" width="650px">
</div>
<br>

### **Broches pour les trois contrôleurs I<sup>2</sup>C**

<br>
<div align="left">
<img alt="Broches pour les trois contrôleurs I2C" src="images/fig_a1_3.jpg" width="650px">
</div>
<br>

### **Broches pour les trois contrôleurs SPI**

<br>
<div align="left">
<img alt="Broches pour les trois contrôleurs SPI" src="images/fig_a1_4.jpg" width="650px">
</div>
<br>

### **Broches et paramètres pour les vingt-sept canaux PWM**

<br>
<div align="left">
<img alt="Broches pour les vingt-sept canaux PWM" src="images/fig_a1_5.jpg" width="650px">
</div>
<br>

### **Broches et paramètres pour les douze canaux ADC**

<br>
<div align="left">
<img alt="Broches pour les douze canaux ADC" src="images/fig_a1_6.jpg" width="650px">
</div>
<br>
