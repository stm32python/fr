---
title: Présentation des cartes NUCLEO de STMicroelectronics
description: Présentation des cartes NUCLEO de STMicroelectronics
---

# Les cartes NUCLEO de STMicroelectronics

Comme expliqué [sur cette page](stm32) la société STMicroelectronics (STM) conçoit et fabrique, entre autres composants, un grand nombre de modèles différents de microcontrôleurs (MCU) STM32. Afin de faciliter l'apprentissage de leur programmation STM propose des cartes de prototypage de types [EVAL](https://www.st.com/en/evaluation-tools/stm32-eval-boards.html), [DISCOVERY](https://www.st.com/en/evaluation-tools/stm32-discovery-kits.html) et [NUCLEO](https://www.st.com/en/evaluation-tools/stm32-nucleo-boards.html). Vous trouverez un document plus complet à ce sujet [ici](https://www.st.com/resource/en/product_presentation/stm32_eval-tools_portfolio.pdf).

Les cartes NUCLEO sont elles-mêmes déclinées en trois familles, NUCLEO 32, NUCLEO 64 et NUCLEO 144, le chiffre indiquant le nombre de broches qu'elles exposent des MCU qui les animent. Dans nos tutoriels, nous nous focaliserons sur les cartes NUCLEO 64 qui présentent selon nous le meilleur compromis entre fonctionnalités, prix, performances et encombrement pour les besoins des enseignants et des élèves.<br>

La plupart des cartes de la famille NUCLEO 64 ont cet aspect :
<br>
<div align="left">
<img alt="Anatomie d'une carte NUCLEO-L476RG" src="images/anatomie_L476RG.png" width="700px">
</div>
<br>

L'exemple ci-dessus correspond à une [NUCLEO-L476RG](nucleo_l476rg) que nous utiliserons pour certains tutoriels. D'autres cartes, telles que la [NUCLEO-WB55RG](nucleo_wb55rg) ou encore la [NUCLEO-WL55JC1](nucleo_wl55jc1) ont un aspect sensiblement différent, mais on y retrouve cependant l'essentiel des "organes" mentionnés ci-dessus.


## Le ST-LINK 

Toute carte NUCLEO est équipée [**d'un module ST-LINK**](../Kit/glossaire) qui sert à la programmer et offre également un canal de communication série avec l'ordinateur auquel elle est connectée. Pour assurer ses missions de "programmeur de firmware" et de "liaison pour le débogage", le ST-LINK est animé par un microcontrôleur STM32L0 programmé avec son propre firmware.

Le cristal de quartz a ici la même utilité que dans les montres ; il s’agit d’un oscillateur piézoélectrique très stable qui fournit une « source d’horloge » de qualité pour le microcontrôleur. En l’absence de quartz, on peut utiliser comme références de fréquence des oscillateurs (i.e. des circuits RLC) intégrés dans le MCU STM32.

## Connecteurs Arduino, connecteurs Morpho, compatibilité Arduino UNO

Les [microcontrôleurs STM32 proposent un grand nombre de fonctions](stm32) et les cartes de la famille NUCLEO 64 permettent d'accéder à 64 broches de ceux-ci, alors que les connecteurs Arduino comptent seulement 32 broches. Les broches des connecteurs Morpho corrigent partiellement ce problème en offrant 32 connexions supplémentaires vers le MCU.<br>

Exemple de la cartographie des broches pour une carte NUCLEO-L476RG :

<br>
<div align="left">
<img alt="Connecteurs cartes NUCLEO 64" src="images/connecteurs_l476rg.jpg" width="700px">
</div>
<br>

Précisons que :

 1. **Les broches Arduino dupliquent certaines des broches Morpho**. Par exemple, la broche Arduino D1 (broche numéro 2 du connecteur CN9) est connectée à la broche Morpho PA2 (broche 35 du connecteur CN10). De même, la broche Arduino D3, quatrième du connecteur CN9, offre une fonction PWM et est connectée à la broche Morpho PB3 qui expose donc la même PWM.

 2.	**Toutes les broches Morpho ne sont** (évidemment) **pas connectées à des broches Arduino**. C’est notamment le cas pour celles de la colonne de droite pour CN10 et de la colonne de gauche pour CN7. Par exemple, la broche Morpho AGND (broche 32 du connecteur CN10) n’est reliée à aucune broche Arduino. De même pour les broches PD1, PD0, VDD … de CN7. 

 3. **Attention, la compatibilité matérielle avec l'Arduino UNO n'est pas systématique !** Bien que les cartes NUCLEO disposent de connecteurs Arduino, il n'est pas garanti que toutes les cartes d'extension ("les shields") conçus pour les cartes Arduino UNO fonctionnent correctement avec celles-ci. La NUCLEO-L476RG présentée ici est un bon exemple de NUCLEO 100% compatible sur ses connecteurs de l'Arduino UNO, mais ce n'est pas toujours le cas. Par exemple, les sorties PWM des autres cartes NUCLEO 64 ne sont pas forcément câblées comme sur les cartes Arduino UNO, ce qui interdira d'utiliser certains shields de contrôle moteur conçus pour l'écosystème Arduino. 


## Les principales fonctions secondaires ou alternatives exposées par les cartes NUCLEO

Cette section est évidemment redondante avec [les explications données à propos des MCU STM32](stm32), puisque les broches d'une carte NUCLEO sont aussi celle du MCU qui se trouve dessus. Par exemple, la liste exhaustive des fonctions du microcontrôleur STM32L476RG qui sont câblées sur les broches Morpho de la carte NUCLEO-L476RG prise comme exemple est conséquente. Pour ne citer que quelques-une de ces fonctions :
 
- **Entrée analogique (ADC)**. Une entrée analogique, connectée sur un convertisseur analogique-numérique (soit un ADC pour *Analog to Digital Converter*) du microcontrôleur, permet de convertir une tension provenant de l'extérieur (sur une broche) en un nombre entier qui lui est proportionnel et qui pourra être traitée par le MCU. L'ADC 12 bits des MCU STM32 accepte en entrée des tensions dont les valeurs sont comprises entre 0 et 3.3V et les convertit en nombres entiers proportionnels, compris entre 0 et 2<sup>12</sup>-1 (soit 4095). L'application la plus courante de l'ADC reste la lecture d'un signal fourni par un capteur analogique, par exemple [**une thermistance**](https://stm32python.gitlab.io/fr/docs/Micropython/grove/thermistance).

- **Sortie analogique (DAC)**. Une sortie analogique, connectée à un convertiseur numérique-analogique (soit un DAC pour *Digital to Analog Converter*) du microcontrôleur, traduit à la volée une valeur numérique donnée par le programmeur en une tension qui lui est proportionnelle et qu'il transmet en sortie sur une broche. Le DAC 12 bits des MCU STM32 accepte en entrée des nombres entiers compris entre 0 et 2<sup>12</sup>-1 et les convertit en tensions proportionelles dont les valeurs sont comprises entre 0 et 3.3V. Le DAC est donc un **générateur de signal** piloté par le MCU (exemples [ici](../Micropython/grove/dac_adc_dft) et [ici](../Stm32duino/exercices/dac_adc_fft)).

- **Bus I2C**. Le bus I2C (pour *Inter Integrated Circuits*) est un bus série qui permet de connecter une multitude de périphériques (capteurs, actuateurs, afficheurs ...) en utilisant deux fils, une ligne de données (SDA) et un signal d'horloge (SCL) d'une longueur maximum de 1 m. Il n'alimente pas les périphériques en courant / tension, une ligne Vcc (source de tension) et une ligne GND (masse) sont donc également nécessaires pour chacun d'eux.<br>
Le bus I2C n'est pas approprié pour la manipulation de données en haut débit, par exemple l'écriture dans des mémoires ou la gestion d'afficheurs au-delà d'une certaine résolution. Pour ces applications, on lui préfèrera le bus SPI.<br>
De nombreux périphériques I2C sont disponibles, voir par exemple [**cette centrale à inertie**](https://stm32python.gitlab.io/fr/docs/Micropython/grove/LSM303D).<br>
**Attention, pour qu'une communication I2C fonctionne avec un périphérique, il faut que ses broches SCL et SDA aient un potentiel porté à +3.3V**. Or, les cartes NUCLEO n'intègrent pas les résistances de tirage nécessaires, ce qui peut poser un problème avec certains modules qui ne les contiennent pas non plus, comme par exemple le [module Grove LCD RGB](../Micropython/grove/lcd_16x2.md).


- **Bus SPI**. Le bus SPI (pour *Serial Peripheral Interface*) est un bus série comme le bus I2C, mais d'une architecture plus complexe permettant les transferts rapides de gros volumes de données. Chaque périphérique est connecté au bus par 4 lignes : SS (pour *Slave Select*, permettant de l'activer sur le bus), SCLK (signal d’horloge pour cadencer les échanges), MOSI (pour *Master Out, Slave In*, communication du microcontrôleur vers le périphérique) et MISO (pour *Master In, Slave Out*,communication du périphérique vers le microcontrôleur). Les lignes d'alimentations (Vcc et GND) restent bien sûr requises pour les périphériques connectés dessus.<br>
Compte tenu du câblage plus complexe (une ligne SS nécessaire par périphérique) et pour ne pas diviser son débit, on ne branche en général qu'un seul périphérique sur par bus / contrôleur SPI, par exemple [**un module carte SD**](https://stm32python.gitlab.io/fr/docs/Micropython/grove/sd_card_module). 

- **Sortie PWM**. Une sortie PWM (pour *Pulse Width Modulation*, soit *modulation de largeur d’impulsion*) génère un signal en tension carré et périodique dans le temps. La génération de signaux PWM est essentiellement utile pour piloter des moteurs électriques ou des [**servomoteurs**](https://stm32python.gitlab.io/fr/docs/Micropython/grove/servo).<br>
La PWM n'est qu'une fonction offerte par le microcontrôleur, pas un périphérique intégré dans celui-ci. Les broches qui l'exposent sont en fait pilotées par un périphérique complexe capable de fournir bien d'autres fonctions : *un timer* (voir ci-après). 

- **UART ("Port série")**. L’UART, signifiant *Universal Asynchronous Receiver-Transmitter*, permet l’échange bidirectionnel d’informations  entre le microcontrôleur et un unique module/périphérique externe (liaison point à point). L’échange de données se fait à l’aide de deux fils (RX et TX), après une phase de synchronisation.<br>
L'UART est un protocole robuste et facile d'utilisation ; il permet d'échanger des commandes et/ou des données en mode texte, lisible par tout un chacun. Il est donc très utilisé pour piloter ou exploiter des périphériques qui renvoient des informations complexes tels que les [**modules GPS**](https://stm32python.gitlab.io/fr/docs/Micropython/grove/gps) ou encore les [**modules Wi-Fi**]().

- **Les timers et leur canaux**. Les timers figurent parmi les périphériques intégrés les plus complexes des MCU STM32. A la base, un timer n'est rien d'autre qu'un compteur qui revient à zéro après un certain nombre de "tics" à l'image d'un chronomètre. Les timers sont utilisés pour un grand nombre d'application telles que : générer des signaux PWM (voir ci-dessus), capturer des signaux qui proviennent de l'extérieur, déclencher des actions périodiques, etc.<br>
Les timers sont également connectés à des broches via des canaux d’entrée ou de sortie sur lesquels ils peuvent lire ou générer des signaux modulés en tension (tels que les PWM). Une revue exhaustive des timers du STM32 est hors de propos ici, mais nous les utiliserons dans quelques tutoriels, par exemple [**celui-ci**](https://stm32python.gitlab.io/fr/docs/Micropython/startwb55/blink_many).

Ces périphériques internes / intégrés se retrouvent dans la plupart des microcontrôleurs "modernes". [Le glossaire](../Kit/glossaire) en cite bien d'autres, et chaque nouvelle génération de MCU en intègre de nouveaux (par exemple, depuis peu, les NPU pour accélérer les firmwares qui réalisent des fonctions d'intelligence artificielle embarquée ou encore des contrôleurs de bus I3C, une évolution réçente de l'I2C).<br>

Apprendre à programmer un microcontrôleur c'est donc essentiellement apprendre à programmer ses périphériques intégrés afin que son microprocesseur puisse communiquer avec les LED, les moteurs, les modules GPS... qui constituent l'application (par exemple, un drone) et les piloter. C'est ce que nous allons faire dans tous les exercices, projets et tutoriels proposés sur ce site.


## Tensions et niveaux logiques des microcontrôleurs STM32

La plupart des broches des microcontrôleurs STM32, et de ce fait des cartes NUCLEO, peuvent être utilisées comme des entrées/sorties numériques :

- **Configurée en sortie numérique**, une broche est au niveau logique `HAUT` (1, ou encore `True`) lorsque la tension qu'elle délivre est 3.3 V (le maximum possible). Elle est au niveau logique `BAS` (0, ou encore `False`) lorsque la tension qu'elle délivre est 0 V (le minimum possible).<br>
En pratique, vous pourrez piloter un périphérique ayant une tension logique de 5V à l'aide d'un microcontrôleur STM32 de tension logique 3.3V.

- **Configurée en entrée numérique**, une broche est forcée au niveau HAUT lorsque la tension qui lui est appliquée est comprise entre 2V et 3.3V. Toute tension inférieure à 0.8 V produira un niveau BAS.<br>

##  Recommandations pour ne pas détruire votre matériel

Cette section est un résumé des conseils avisés disponibles dans l'excellent ouvrage de [*MicroPython et Pyboard - Python sur microcontrôleur : de la prise en main à l'utilisation avancée*](https://www.editions-eni.fr/livre/MicroPython-et-pyboard-python-sur-microcontroleur-de-la-prise-en-main-a-l-utilisation-avancee-9782409022906) de Dominique MEURISSE, qui restent valables quel que soit l'environnement de programmation utilisé.

- **Il ne faut jamais travailler alors que la carte NUCLEO est sous tension**.<br>

Chaque fois que vous branchez ou débranchez quelque chose sur une broche, assurez vous que la carte n'est pas alimentée. La plupart des modules conçus par STMicroelectronics, Seeed studio, DF Robot, Adafruit … résistent remarquablement bien aux branchements / débranchements brutaux sous tension, mais d’autres (notamment les modules Bluetooth HC-05 et HC-06, très utilisés mais fabriqués on ne sait où par on ne sait qui) n’attendent que ça pour « griller ».

- **Vérifiez toujours deux fois vos branchements avant de remettre la carte NUCLEO sous tension**.<br>

Avec les connecteurs normalisés de type Grove, Qwiic stemma, micro:bit ... le risque de vous tromper sur le câblage au point de détruire un module est proche de zéro, c’est pour cela que nous les utilisons dans la plupart des tutoriels de ce site. Leur autre avantage (pour les débutants seulement) c’est qu’ils permettent de réaliser des systèmes complexes même lorsqu'on n'a aucune connaissance (ou presque) sur le fonctionnement d’un circuit électrique / électronique. Dit autrement : ils permettent de se concentrer sur la programmation embarquée plutôt que sur la conception électronique.<br>

En revanche, si vous utilisez des câbles dupont, des résistances et des platines d’essai pour câbler votre système, vous entrez sur le territoire des électroniciens avisés ; vous êtes supposé avoir une bonne connaissance des courants, des tensions, des composants. Soyez extrêmement vigilant à ne pas mettre en court-circuit deux broches du microcontrôleur, à ne pas envoyer une surtension sur une broche d’entrée, à ne pas brancher dans le mauvais sens un composant polarisé, etc. Pour ne pas se tromper sur les polarités, un respect des codes-couleurs des fils est essentiel (rouge pour l'alimentation positive, noir pour la masse).

- **Concernant les broches du microcontrôleur, une petite liste de points de vigilance sur les fausses manipulations potentiellement fatales** : 

1. Ne pas appliquer des tensions supérieures à 3.3V sur des broches configurées en "entrée". La plupart des broches d'un microcontrôleur STM32, **mais pas toutes**, tolèrent une tension externe allant jusqu'à 5V. **Nous vous déconseillons donc d'appliquer des tensions supérieures à 3.3V sur les broches à moins d'avoir bien étudié la fiche technique du microcontrôleur !** En cas d'erreur, vous risqueriez purement et simplement de le détruire.
2. Ne pas appliquer une tension supérieure à 3.3V sur la broche +3V3 (Ne pas lui appliquer sciemment de tension du tout d'ailleurs, puisque c'est une alimentation !).
3. Ne pas appliquer une tension supérieure à 5V sur la broche +5V (Idem ci-dessus).
4. Ne pas brancher une broche configurée en sortie directement à la masse. Placée dans l'état `HAUT` elle y injecterait un courant d'intensité égale au maximum de ce qu'elle peut délivrer  (25 mA) et elle "fondrait" immédiatement.
5. Ne pas solliciter une broche configurée en sortie numérique pour délivrer un courant d'une intensité dépassant les 25 mA qu'elle peut délivrer. Par exemple, une broche peut piloter un relais qui commande un moteur, lui même alimenté par un circuit qui lui est dédié. Mais une broche **ne peut pas** fournir le courant nécessaire au moteur et **ne doit pas** être connectée "en direct" dessus.<br>
Ce point de vigilance reste pertinent aussi lorsqu'on utilise **plusieurs broches du microcontrôleur simultanément comme sources de courant**. Le courant d’alimentation maximum du STM32WB55 est de 150 mA, il ne peut donc pas délivrer plus à un instant donné sur l'ensemble de ses broches sans être irrémédiablement détruit.<br>
En pratique, que vous utilisiez une ou plusieurs broches comme alimentations, tenez vous loin de la limite des 25 mA ; **1 à 2 mA par broche** est le maximum raisonnable. Si votre montage nécessite plus de courant il faut prévoir un circuit d'alimentation indépendant.
6. Ne pas brancher des broches ensembles ; en court-circuit.
7. Ne pas se tromper sur le signe (et la valeur !) de la tension d'alimentation de la carte NUCLEO si on décide de passer par la broche V<sub>IN</sub>. Elle doit être positive, bien sûr ; un microcontrôleur est un composant polarisé qui fonctionne en courant continu.

Tout ceci peut paraître évident, mais on a vite fait de se retrouver dans l'une de ces situations lorsqu'on est débutant et que l'on conçoit un circuit complexe avec une platine d'essai.
