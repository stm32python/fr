---
title: Glossaire
description: Glossaire
---

# Glossaire

**3GPP** : Third Generation Partnership Project. Désigne une coopération internationale entre organismes de normalisation en télécommunications. Il produit et publie les spécifications techniques pour les réseaux mobiles de 3e (3G), 4e (4G) et 5e (5G) générations.

**ADC** : Analog to Digital Converter (fr : Convertisseur Analogique vers Numérique). Circuit électronique spécialisé dans la conversion de signaux d’entrée analogiques (des tensions éventuellement variables dans le temps, comprises entre 0 et +3,3V) en signaux numériques (des nombres entiers proportionnels à la tension d’entrée à chaque instant et compris entre 0 et 4095, soit 2<sup>12</sup>-1, pour un ADC avec une résolution de 12 bits).

**AES-256** : Advanced Encryption Standard - 256 (fr : Norme de Chiffrement Avancé - 256) désigne en toute généralité un algorithme de *chiffrement symétrique*, ce qui signifie que la même clé est utilisée à la fois pour le chiffrement et le déchiffrement. Le chiffre "256" est le nombre de bits constituant la clef. Cette "grande" taille rend l’AES-256 extrêmement résistant aux attaques par force brute consistant à tester toutes les combinaisons possibles de clés pour déchiffrer les données. Dans le contexte des *MCU*, AES-256 fait plutôt référence à des unités matérielles intégrées qui accélèrent les opérations de l'algorithme homonyme. On trouve aussi des unités de calcul AES-128 intégrées dans les MCU.

**ALU** : Arithmetic and Logic Unit (fr : Unité d’opérations arithmétiques et logiques). L’unité arithmétique et logique est un circuit électronique intégré au CPU et chargé d'effectuer les calculs sur des nombres entiers et/ou binaires.

**Arduino** : Écosystème initialement éducatif développé par la société du même nom. Arduino désigne à la fois les cartes programmables (la carte Arduino UNO étant la plus connue), la *bibliothèque* en langage C++ qui simplifie (mais restreint par là même) leur programmation et une IDE épurée. Arduino développe également depuis peu des outils de programmation et des cartes (pour certaines équipées de microcontrôleurs STM32) à l'attention des professionnels. 

**Assembler** (fr : assembleur). Un *programme assembleur* convertit le listing d'un autre programme écrit en *langage d'assemblage* en un fichier binaire, en vue de créer, par exemple, un fichier objet ou un *firmware*.

**ARM Cortex** : Microprocesseur offrant un excellent compromis performances / consommation conçu par la société ARM, pour des applications embarquées. Le microcontrôleur STM32WB55, par exemple, est équipé de deux microprocesseurs de cette famille, un Cortex M0 pour la gestion du BLE et un Cortex M4 qui traite les programmes utilisateur.

**ART** : Adaptive Real Time memory accelerator. Mémoire cache intégrée dans de nombreux *MCU STM32* ayant pour fonction de masquer les latences de la mémoire Flash afin que cette dernière puisse être utilisée pour l'exécution de *firmwares*.

**Baud** ou **baudrate** : Unitée exprimant le débit par seconde en symboles élémentaires d'une liaison série asynchrone. Le plus souvent (c’est le cas pour les UART / USART des MCU STM32) 1 Baud  = 1 bit/s. **Mais ce n’est pas systématique !** Certaines modulations permettent de transférer plus que 1 bit par symbole. De ce fait, on peut écrire, en toute généralité : *Bits / s = Baudrate × Bits par symbole*.

**BLE** : Bluetooth Low Energy (fr : Bluetooth faible consommation énergétique). C'est une technique de transmission sans fil créée par Nokia en 2006 sous la forme d’un standard ouvert basé sur le Bluetooth, qu'il complète mais sans le remplacer. Comparé au Bluetooth, le BLE permet un débit du même ordre de grandeur (1 Mbit/s) pour une consommation d'énergie 10 fois moindre.

**Bluetooth** : C'est une norme de communication permettant l'échange bidirectionnel de données à courte distance en utilisant des ondes radio UHF sur la bande de fréquence de 2,4 GHz. Son but est de simplifier les connexions entre les appareils électroniques à proximité en supprimant des liaisons filaires.

**Bootloader** : Système logiciel de démarrage d'un microcontrôleur, permettant la mise à jour de son firmware.

**CAN** : Controller Area Network (fr : pas de version française usuelle). Contrôleur de bus série multi-maître et multiplexé conçu par Bosch pour augmenter la fiabilité et abaisser le coût des réseaux embarqués dans les automobiles. Le protocole CAN gère matériellement les erreurs, les priorités des messages et l’ajout ou la suppression dynamique et « à chaud » de nœuds / stations sur son réseau.

**CISC** : Complex Instruction Set Computer (fr : calculateur à jeu d'instructions complexe). Paradigme d'implémentation d'une microarchitecture de CPU. Les CPU CISC offrent de nombreuses instructions de formes et de temps d'exécution variables, certaines réalisant des tâches très complexes et nécessitant un grand nombre de cycle d'horloges (unités de temps élémentaires). Au début de la microélectronique, les architectures CISC dominaient car elles étaient les plus pertinentes compte tenu du nombre réduit de *MOSFET* dont on disposait pour les designs. Par exemple, jusque dans les années 90, les microprocesseurs d'Intel étaient essentiellement des machines CISC. Depuis, architecture *RISC*, plus efficace, s'est imposée. 

**Compiler** (fr : compilateur). Programme qui transforme un code source en un code objet. Généralement, le code source est écrit dans un langage de programmation (le langage source), il est de haut niveau d'abstraction, et facilement compréhensible par l'humain.
Le code objet est généré dans un langage de plus bas niveau (appelé langage cible), par exemple un langage d'assemblage ou langage machine, afin de créer un programme exécutable par une machine, comme, par exemple un *firmware*. 
Un langage qui peut être in fine transformé en code binaire après compilation, comme C ou C++ est qualifié de *langage compilé*. MicroPython *n'est pas* un langage compilé, c'est un langage *interprété* (voir *Interpreter*).

**Cortex M** : Famille de cœurs pour microcontrôleurs développée par la société ARM. Les Cortex M sont des microprocesseurs offrant un excellent compromis entre performances et consommation d'énergie pour des applications embarquées et/ou gérant des systèmes électroniques qui doivent rester réactifs (dits "temps-réel") en certaines circonstances.

**CPU** : Central Processing Unit (fr : Unité Centrale de Traitement). Unité centrale de traitement du microcontrôleur, en l’occurrence, pour la NUCLEO-WB55, un microprocesseur Cortex M4 conçu par la société ARM et optimisé pour être intégré dans les microcontrôleurs et autres SoC.

**CSS** : Chirp-Spread Spectrum. Modulation par étalement de fréquence spécifique aux technologies de communication radiofréquence *LoRa* et *LoRaWAN*.

**DAC** : Digital to Analog Converter (fr : Convertisseur Numérique vers Analogique). Un convertisseur numérique-analogique est un circuit électronique dont la fonction est de traduire à la volée une valeur numérique qu'il reçoit en une valeur analogique qui lui est proportionnelle et qu'il transmet aux circuits électroniques suivants, capables uniquement de traiter un signal analogique encodé sous forme d'une tension variable.

**dBm** : Décibel-milliwatt. Unité **relative** et **logarithmique** utilisée pour caractériser les signaux en communication, qui exprime les puissances par rapport à 1 mW selon la formule *Puissance (dBm) = 10 log<sub>10</sub> ( Puissance exprimée en Watts) / 0.001 )*.

**D-BPSK** : Differential Binary phase-shift keying. Technique de modulation utilisée par le réseau *LPWAN* *Sigfox*.

**Debugger** (fr : débugueur ou débogueur). Un **débogueur** est un logiciel qui aide un développeur à analyser les erreurs de son programme. Pour cela, il permet notamment d'exécuter le programme pas-à-pas (c'est-à-dire, le plus souvent, ligne par ligne), d'afficher la valeur des variables à tout moment et de mettre en place des **points d'arrêt**. Un point d'arrêt est un emplacement d'un programme (ligne, instruction) où son exécution est mise en pause (par le débogueur). Un point d'arrêt peut aussi être **conditionnel** et suspendre l'exécution lorsque certaines conditions se réalisent (par exemple, si une variable donnée du programme prend la valeur zéro).

**DFU** : Device Firmware Update (fr : Mise à jour firmware du système). Voir également *Bootloader*.

**DMA** : Direct Memory Access (fr : Contrôleur d'accès direct à la mémoire). Circuit électronique connecté sur le même bus interne que le CPU et spécialisé dans les transferts d’informations d’une zone mémoire vers une autre. Le contrôleur DMA fonctionne en parallèle avec le CPU et est nettement plus rapide que celui-ci pour les opérations de copie qu’il réalise.

**DRAM** : Dynamic Random Access Memory (fr : mémoire à accès aléatoire dynamique ou mémoire vive dynamique). Chaque point mémoire d'une DRAM est un minuscule condensateur, ce qui la rend compacte et peu dispendieuse. Sans alimentation, la DRAM perd ses données, ce qui la range dans la famille des mémoires volatiles. Sa consommation énergétique élevée en proscrit l'intégration au sein d'un microcontrôleur ; elle est essentiellement présente dans les "barrettes de mémoire" des ordinateurs personnels.

**Eclipse**. Selon Wikipédia, Eclipse est un projet, décliné et organisé en un ensemble de sous-projets de développements logiciels, de la fondation Eclipse, visant à développer un environnement de production de logiciels libres qui soit extensible, universel et polyvalent, en s'appuyant principalement sur Java.

**EEPROM** : Electrically-Erasable Programmable Read-Only Memory (fr : mémoire morte effaçable électriquement et programmable, aussi appelée E<sup>2</sup>PROM). L'EEPROM est un type de mémoire non-volatile, c'est à dire utilisée pour enregistrer des informations qui ne doivent pas être perdues lorsque l'appareil qui les contient n'est plus alimenté en électricité. Elle est plus rapide que la *mémoire flash* mais aussi plus chère (car adressable par octets) et elle supporte nettement moins de cycles de lecture-écriture avant de se dégrader. Dans les microcontrôleurs elle est utilisée pour enregistrer des petites quantités d'informations qui ne nécessitent pas d'être modifiées souvent : mots de passe, configuration système, offsets de capteurs après calibration, etc. Elle tend cependant à être remplacée par de la mémoire *flash*.

**Embedded system** (fr : système embarqué). Un système embarqué désigne un système électronique (hardware) et informatique (software) autonome dédié à une tâche bien précise. Par exemple : capteurs environnementaux connectés (station météo, montre connectée), systèmes de guidage (de robots, de drones, de missiles ...), applications domotiques, les cartes à puces, de nombreux sous-systèmes dans les automobiles, etc. La plupart des objets connectés de l'*IoT* sont des systèmes embarqués.

**EXTI** : Extended Interrupts and Events Controller (fr : gestionnaire d'interruption et d'évènements externes). Circuit dédié à la gestion des interruptions et évènements provenant des GPIO et de circuits externes au microcontrôleur.

**Firmware** : Nom donné au fichier binaire qui contient un programme compilé destiné à un microcontrôleur.

**Flash (mémoire)** : Type de mémoire non volatile dans laquelle est placé le code compilé d'un microcontrôleur, pour lequel elle fait office de mémoire de masse. Cette mémoire ne s’efface pas lorsque son alimentation électrique est éteinte. Elle est approximativement quatre fois plus dense, càd qu’elle peut stocker quatre fois plus de bits par unité de surface, que la *SRAM* ou l'*EEPROM*. En contrepartie, la mémoire flash est plus lente à lire et à écrire que la SRAM ou l'EEPROM. La flash peut être effacée par secteurs et dispose d'une endurance aux lectures et écritures plus importante que l'*EEPROM*.

**GAP** : Generic Access Profile (fr : Profil d'accès générique). Partie du protocole de communication Bluetooth Low Energy (BLE) chargée du contrôle des connections et du mode "advertising" ("publicité" en traduction littérale). GAP rend votre objet BLE visible au reste du monde et détermine comment il pourra (ou ne pourra pas) interagir avec un autre objet BLE.

**GATT** : Generic Attribute Profile (fr : Profil d'attribut générique). Partie du protocole de communication Bluetooth Low Energy (BLE) chargée des transferts de données entre deux objets connectés, sur la base de concepts appelés *services* et *caractéristiques*.

**GPIO** : General Purpose Input Output (fr : Port D’entrée Sortie à usage Général). Les Ports d’entrée-sortie à usage général désignent l’ensemble des fonctions (programmables) que peuvent remplir les broches du microcontrôleur. Ils permettent de sélectionner les circuits internes au microcontrôleur que l’on souhaite utiliser afin d’interagir avec des composants externes et connectés à celui-ci tels que mémoires, moteurs, capteurs …

**GSM** : Global System for Mobile Communications est un standard numérique de seconde génération (2G) pour la téléphonie mobile. Le GSM est un prédécesseur des normes de communication 3G (voir *3GPP*).

**HAL (Library)** : Hardware Abstraction Layer (fr : couche d'abstraction matérielle). *Bibliothèque* développée par STMicroelectronics pour programmer ses microcontrôleurs de la famille *STM32*.

**HCI** : Host Controller Interface (fr : Interface contrôleur hôte). C'est une couche logicielle qui transporte les commandes et les événements entre le Cortex M4 et le Cortex M0+ contrôleur de la pile de protocoles *BLE* d'un MCU de la famille STM32WB. 

**I2C ou IIC** : Inter-Integrated Circuit (fr : bus de communication inter-circuits intégrés). Contrôleur de bus série fonctionnant selon un protocole inventé par Philips. Tout comme le bus SPI, le bus I2C a été créé pour fournir un moyen simple de transférer des informations numériques entre des capteurs, des actuateurs, des afficheurs et un microcontrôleur. Il nécessite deux lignes (c'est pourquoi on le trouve aussi parfois désigné par "Two-Wire"), SDA ("Serial Data", ligne de données) et SCL ("Serial Clock", ligne de synchronisation / horloge partagée). Les périphériques que l'on y connecte doivent tous avoir une adresse identifiante unique et statique (non modifiable, jusqu'à 2<sup>7</sup> = 128 adresses possibles sur un même bus). Son débit maximum théorique est de 3 Mbps sur quelques dizaines de centimètres.

**I3C** :  Improved Inter-Integrated Circuit (fr : bus de communication inter-circuits intégré amélioré). La norme I3C succède à la norme *I2C*, les premiers *microcontrôleurs* intégrant des contrôleurs de bus série I3C datent de 2023. Entre autres améliorations, un bus I3C permet des débits théoriques de 30 Mbps (dix fois plus que l'I2C, mais avec portée inchangée de quelques dizaines de centimètres) et est capable d'attribuer dynamiquement des adresses aux périphériques que l'on y connecte. Il  gère aussi les interruptions en provenance de ses périphériques esclaves sans monopoliser d'autres broches du microcontrôleur maître. Les constructeurs s'arrangent bien évidemment pour assurer une compatibilité ascendante entre I2C et I3C (un périphérique I2C pourra être connecté à un bus I3C et y fonctionner).

**IDE** : Integrated Development Environment (fr : environnement de développement intégré). Un environnement de développement intégré est un ensemble d'outils logiciels qui permet d'augmenter la productivité des programmeurs qui développent de nouveaux logiciels.

**IWDG** : Independant Watchdog (fr : chien de garde indépendant). Le chien de garde indépendant est un circuit électronique qui fonctionne avec une horloge dédiée. Il réalise un compte à rebours depuis une valeur maximum programmable jusqu’à zéro. L’IWDG génère un redémarrage (reset) du microcontrôleur lorsque le compte à rebours atteint zéro, sauf si celui-ci est relancé avant cette échéance par une instruction du programme utilisateur.

**Internal RC oscillator** (fr : oscillateur RC interne). Oscillateur à résistance - capacité intégré au microcontrôleur. Ce circuit RC sera utilisé comme référence pour les horloges internes du microcontrôleur en l'absence d'un cristal de quartz plus performant mais aussi plus onéreux (voir *XTAL oscillator*). La fréquence de sortie du circuit RC est généralement multipliée par un circuit *PLL* également intégré au microcontrôleur.

**Interpreter** (fr : interpréteur).  Programme informatique ayant pour tâche d'analyser, de traduire et d'exécuter d'autres programmes informatiques. Les langages dont les programmes sont exécutés par un interpréteur sont généralement qualifiés de "langages interprétés". MicroPython est un langage interprété. Son interpréteur est un firmware installé dans le microcontrôleur hôte, chargé de compiler à la volée les scripts MicroPython rédigés en mode texte afin de les traduire en instructions binaires exécutables. 

**Interrupt** (fr : Interruption). Mécanisme d'exécution intégré à un microcontrôleur qui lui permet de réagir presque instantanément à des signaux provenant de périphériques (des capteurs par exemple) en exécutant de façon prioritaire des programmes répondant à ces signaux. Plus précisément, le programme exécuté par le microcontrôleur juste avant l'interruption est mis en pause,  puis le microcontrôleur exécute un programme spécialement dédié à celle-ci, appelé "routine de service de l'interruption" (ou ISR pour "Interrupt Service Routine", en anglais). Une fois que l'ISR a terminé, l'exécution du programme principal reprend son cours.

**IoT** : Internet of Things (fr : Internet des Objets). L'IoT est l'interconnexion entre l'Internet et des objets, des lieux et des environnements physiques. L'appellation désigne un nombre croissant d'objets connectés à Internet permettant ainsi une communication entre nos biens dits physiques et leurs existences numériques.

**I/O** : Inputs / Outputs (fr : entrées / sorties). Voir *GPIO*.

**ISR** : Interrupt Service Routine (fr : routine de service d'une interruption). Programme lancé par le microcontrôleur lorsqu'une *interruption* survient, renvoyée par un périphérique (par exemple : le signal d'un capteur, la fin d'une communication avec un UART, etc.). Le code de l'ISR est écrit par le programmeur pour traiter un évènement particulier et "asynchrone" au sens "non planifié" (par exemple : allumer les phares d'un robot lorsque la luminosité ambiante devient plus faible qu'un seuil donné, déployer un airbag en cas de choc, etc.). Ce code doit être aussi simple que possible pour ne pas retarder l'exécution d'autres ISR qui pourraient survenir ou paralyser durablement le programme principal, qui est mis en pause en attendant que toutes les ISR soient traitées.

**JTAG/SWD** : Joint Test Action Group / Serial Wire Debug (fr : pas de version française usuelle). Interfaces (bus) de communication avec les fonctions de débogage en temps réel intégrées dans l’architecture ARM Cortex.

**LCD** : Liquid Crystals Display (fr : afficheur à cristaux liquides).

**LED** : Luminescent Electronic Diode (fr : Diode Électroluminescente). **Attention**, les LED sont polarisées ; si vous les branchez incorrectement, vous les détruirez probablement. La patte la plus longue d'une LED doit être connectée au "+" et la plus courte au "-".

**Library (bibliothèque)**. Une bibliothèque est un ensemble de fonctions et définitions (pour les langages C/C++ ou autres) qui contiennent des fonctions utilisées de façon récurrente par les firmwares : pilotes de périphériques, manipulation de texte, fonctions mathématiques, manipulation de fichiers, etc.

**Linker** (fr : éditeur de lien). L’édition des liens permet de créer un *firmware* à partir de fichiers objets (binaires) produits par la *compilation* ou *l'assemblage* et des routines provenant de bibliothèques statiques.

**LL (Library)** : Low Layer (fr : couche d'abstraction de bas niveau). *Bibliothèque* développée par STMicroelectronics pour programmer ses microcontrôleurs de la famille *STM32*.

**Load-Store architecture** (fr : Architecture chargement et enregistrement). Les *CPU Cortex-M*  sont des machines de type "chargement et enregistrement". Lors du traitement des instructions sur les données, les opérandes sont chargés dans les **registres** centraux, l’opération est ensuite effectuée sur ces registres et les résultats sont finalement enregistrés en retour dans *la mémoire SRAM*.

**LoRa** : Long Range (fr : longue portée). LoRa est le nom donné à la technologie de modulation des ondes radios sur laquelle sont basés les réseaux longue portée et bas débit *LoRaWAN*. *LoRaWAN* est un protocole tandis que LoRa fait référence à la couche physique du réseau. LoRa (et LoRaWAN) ont été créées et brevetées par la start-up française Cycleo en 2009 et rachetées en 2012 par [*Semtech*](https://www.semtech.fr/).

**LoRaWAN** :  Long-range wide area network (fr : réseau étendu et longue portée à basse consommation). Protocole de communication radiofréquence grande portée et faible consommation développé pour les objets connectés fondé sur la technologie de modulation à très large bande (ULW) LoRa. LoRaWAN appartient à la famille plus large des technologies *Low Power Wide Area Networks* (LPWAN) ou *réseau étendu à basse consommation* en français. LoRaWAN est l'un des deux LPWAN les plus populaires, avec *Sigfox*.

**LPUART** : *Low Power* Universal Asynchronous Receiver Transmitter (fr : Récepteur-Transmetteur Asynchrone Universel *à basse consommation*). Voir *UART*.

**LPWAN** : Low Power Wide Area Networks (fr : réseau étendu à basse consommation). Un réseau LPWAN est un type de réseau employé dans l'*Internet des objets* (Internet of Things ou IoT) et dans la communication intermachines (Machine to Machine ou M2M). Les LPWAN sont caractérisés par leur très faible consommation d'énergie, leur très grande portées et leur très faible débit. Les deux LPWAN les plus populaires sont *LoRaWAN* et *Sigfox*.

**LTE** : Long Term Evolution, terme désignant une évolution en 4G, est une norme de service cellulaire (sans fil) de quatrième génération pour toutes les communications mobiles. Le LTE est composé d’une architecture, de fréquences radio, de performances et d’usages, qui permet le très haut débit, une faible latence, des téléchargements rapides, etc. (Source : [Orange](https://iotjourney.orange.com/fr-FR/support/faq/quelles-differences-entre-le-lte-et-le-lte-m)).

**LTE-M** : Long-Term Evolution Machine Type Communication est un réseau cellulaire dédié à l’IoT dont la technologie a été normalisée par le *3GPP*. Les réseaux LTE-M sont entièrement compatibles avec les réseaux LTE (2G) et sont une extension de la 4G.  

**MCU** : Microcontroller Unit (fr : Unité Microcontrôleur). Un microcontrôleur est un « système sur puce » (SoC) car il rassemble sur une même puce de silicium un microprocesseur et d’autres composants et périphériques : des bus, des horloges, de la mémoire, des ports entrées-sorties généralistes, etc.

**MBED/MBED OS** : *IDE* et *bibliothèques* reposant sur le langage C/C++ développées ARM ayant comme objectif, tout comme Arduino, de simplifier la tâche des programmeurs de systèmes embarqués mais aussi des *objets connectés*. MBED permet de tirer mieux parti des ressources des microcontrôleurs que ne le fait Arduino, au prix d'une programmation un peu plus complexe. 

**MEMS** : Micro Electro Mechanical System (fr : système micro-électro-mécanique). Capteurs physiques miniaturisés. Les capteurs MEMS ont révolutionné les applications électroniques et sont à la base – avec des capteurs d’image et de distance sans cesse plus performants – de toutes les fonctions ludiques et avancées des smartphones et des objets connectés en général. Ils sont également les composants clés des centrales inertielles des drones et des casques et manettes de réalité virtuelle. Les MEMS les plus communs sont :

- L'accéléromètre tridimensionnel
- Le gyroscope tridimensionnel
- Le magnétomètre tridimensionnel
- Le capteur de pression
- Le capteur de température
- Le capteur d'humidité relative

**Microcontroller** : Voir *MCU*.

**MicroPython** : Implémentation légère et efficace du langage de programmation Python 3 incluant un petit sous-ensemble de la bibliothèque standard Python et optimisée pour fonctionner sur des microcontrôleurs disposant de peu de mémoire.

**MORPHO** : Type de connecteur propriétaire de STMicroelectronics sur les cartes NUCLEO, donnant accès à presque toutes les fonctions du microcontrôleur considéré.

**MOSFET** : Metal-Oxide-Semiconductor Field Effect Transistor (fr : Transistor Métal-Oxyde-Semiconducteur à effet de champ). Famille de composants miniaturisés encore majoritairement utilisés dans l'architecture des puces électroniques. Les MOSFET se comportent comme des interrupteurs minuscules commandés par une tension électrique. Les MOSFET sont peu à peu remplacés par des transistors d'autres types pour les besoins d'une miniaturisation encore plus poussée.

**NB-IoT** : Narrowband Internet of things est un protocole de communication radio dédié aux réseaux étendus à faible consommation et à l'Internet des objets.
NB-IoT privilégie couverture, capacité, faible coût en composants et basse consommation électrique au prix d'un débit de transmission réduit. Contrairement à d'autres protocoles similaires comme LoRaWAN ou Sigfox, il utilise le réseau cellulaire.

**NFC** : Near-Field Communication (fr : communication en champ proche). Le NFC est une technologie de communication sans fil à courte portée et à haute fréquence, permettant l'échange d'informations entre des périphériques jusqu'à une distance d'environ 10 cm dans le cas général. Son application sans doute la plus populaire est le paiement sans contact (voir aussi *RFID*).

**NUCLEO** : Nom commercial donné aux cartes de développement conçues par STMicroelectronics, qui permettent d'évaluer divers composants développés par cette société (microcontrôleurs, capteurs MEMS ...).

**NVIC** : Nested Vectored Interrupts Controller (fr : Contrôleur d'Interruptions Vectorisées et Imbriquées). Circuit intégré au CPU ARM Cortex chargé de la gestion des interruptions. Il optimise en particulier la réactivité du système aux interruptions en leur assignant des priorités et en gérant les situations où plusieurs interruptions surviennent dans un intervalle de temps très court.

**NVM** : Non Volatile Memories (fr : mémoires non volatiles). Une mémoire non volatile est une mémoire qui conserve ses données en l'absence d'alimentation électrique. Parmi les NVM, on distingue les mémoires mortes (*ROM*), certaines mémoires de type RAM non volatiles (NVRAM pour non-volatile RAM) ou encore les *mémoires flash* et les *EEPROM*.

**OLED** : Organic Luminescent Electronic Diode (fr : Diode Électroluminescente Organique).

**One-Wire** (fr : "Un-câble"). Aussi connu sous le nom de bus Dallas ou 1-Wire, c’est un bus conçu par Dallas Semiconductor qui permet de connecter (en série, parallèle ou en étoile) des composants avec seulement deux fils (un fil de données et un fil de masse). Il ne consiste en fait qu’en un protocole, il n’y a pas besoin d’intégrer un circuit spécifique pour le supporter à l’intérieur du *microcontrôleur*. Pour l’exécuter le *MCU* envoie des impulsions rapides (et structurées) sur l’une de ses broches.  

**PLL** : Phase-Locked Loop (fr : boucle à phase asservie, ou boucle à verrouillage de phase). Une PLL est un montage électronique permettant d'asservir la phase ou la fréquence de sortie d'un système sur la phase ou la fréquence d'un signal d'entrée. Elle peut aussi asservir une fréquence de sortie sur un multiple de la fréquence d'entrée. Les PLL intégrées dans les microcontrôleurs servent de *multiplicateurs de fréquence d'horloge*, fréquence fournie par un circuit intégré oscillant RC (voir *Int. RC oscillator*) ou par un cristal oscillateur piézoélectrique de quartz (voir *XTAL oscillator*).

**PWM** : Pulse Width Modulation (fr : modulation en largeur d’impulsion). La modulation en largeur d’impulsion est une technique permettant de faire varier le potentiel électrique d’une broche entre 0 et +3,3V (pour le microcontrôleur concerné) selon un signal rectangulaire de fréquence et de rapport cyclique (ratio entre le temps où le signal est à 0V et celui où il est à +3,3V au sein d’une période) dynamiquement ajustables. La génération de signaux PWM est l’une des fonctions assurées par les timers, essentiellement pour piloter des moteurs électriques.

**Power Supply ou PWR** (fr : Alimentation). Désigne toute la partie de l’architecture d'un MCU dédiée à la gestion de l'alimentation et de ses modes basse consommation. Les circuits PWR offrent les fonctions pour préserver certains registres ainsi que la RTC lorsque le CPU est placé dans des modes avancés d’économie d’énergie.

**RAM** : Random Access Memory (fr : mémoire à accès aléatoire ou mémoire vive). La mémoire vive intégrée aux microcontrôleurs est nettement plus performante que leur mémoire flash mais est disponible en quantité plus réduite du fait de son architecture plus complexe et moins dense. Toutes les informations enregistrées dans une mémoire RAM sont perdues lorsque son alimentation électrique est interrompue ; c'est une mémoire *volatile*. Elle se décline essentiellement en deux architectures, *SRAM* - plus adaptée à l'intégration dans les microcontrôleurs - et *DRAM* - moins chère, plus dense, plus lente et plus énergivore.<br>
Il existe également des *NVRAM*, pour *non-volatile RAM*, qui ne s'effacent pas hors alimentation électrique.

**RCC** : Reset and Clock Control (fr : contrôle des reset et horloges). Désigne toute la logique et la circuiterie dédiées à la génération et à la distribution de signaux d’horloges et d’alimentations dans le CPU et pour les périphériques du microcontrôleur. 

**Registers** (fr : registres). Mémoires très rapides constituées de bascules de transistors (architecture de type *SRAM*) et de tailles limitées (8, 16 ou 32 bits en général) intégrées dans un CPU. Les registres sont utilisés par le microprocesseur pour calculer et manipuler instructions et données des programmes pendant leur exécution, mais aussi pour interagir avec les périphériques du microcontrôleur.

**REPL** : Read Evaluate Print Loop (fr : Boucle de lecture, évaluation et impression). Spécifique à l'environnement MicroPython, le mode REPL permet d'envoyer des instructions ou des lignes d'instructions à l'interpréteur MicroPython en les tapant dans un terminal série (type PuTTY) connecté à un UART/USART du microcontrôleur hôte. L'interpréteur renvoie des messages d'acquittement, d'information, d'erreur ... dans le terminal.

**RFID** : Radio Frequency Identification (fr : radio-identification). La radio-identification est une méthode pour mémoriser et récupérer des données à distance en utilisant des marqueurs appelés « radio-étiquettes » (« RFID tag » ou « RFID transponder » en anglais).

**ROM** : Read Only Memory (fr : mémoire en lecture seule). Mémoire qui peut être extrêmement dense car potentiellement encodée dans la structure même du microcontrôleur lors de sa fabrication. Les instructions exécutées par le CPU sont par exemple répertoriées dans une ROM. Une ROM est donc une *mémoire non-volatile (NVM)* ; elle ne s'efface pas lorsque le système n'est plus alimenté électriquement.

**RISC** : Reduced Instruction Set Computer (fr : calculateur à jeu d'instructions réduit). Paradigme d'implémentation d'une microarchitecture de *CPU*. Les **Cortex M** (et de nombreux autres CPU) ont une architecture de type RISC, pourvue d'instructions courtes de forme aussi simple que possible, pouvant s'exécuter en un minimum d'unités de temps (on parle de "cycles d'horloge"), et qui réalisent des tâches peu complexes. Par comparaison avec les architectures *CISC*, un CPU RISC nécessite généralement moins de MOSFET et consomme moins d'énergie pour réaliser une tâche donnée. Les *compilateurs* RISC sont plus efficaces pour produire un code assembleur optimisé. 

**RSSI** : Received Signal Strength Indication (fr : indication de puissance du signal reçu). En communications radiofréquence (RF) telle que *NFC*, *BLE*, *LoRaWAN*, *Wi-Fi*, *Sigfox* ... le RRSI est la puissance du signal reçu mesurée en *dBm* (décibels-milliwat). Plus elle sera élevée, plus le signal sera "audible" par le récepteur. Le RSSI a toujours une valeur négative, et ne peut pas être meilleur que 0 dBm. 

**RTC** : Real-Time Clock (fr : Horloge Temps-Réel). Circuit électronique remplissant les fonctions d’une horloge très précise pour des problématiques d’horodatage. Suivant sa complexité, la RTC pourra aussi gérer des fonctions plus complexes comme des alarmes déclenchées suivant un calendrier, voire mettre le microcontrôleur en sommeil et le réveiller si elle est intégrée dans celui-ci.

**Sigfox** :  (D'après Wikipédia) Opérateur de télécommunications français créé en 2009 par Christophe Fourtet et Ludovic Le Moan et implanté à Labège, commune de la banlieue toulousaine. Sigfox est spécialisé dans *l'IoT* grâce à un réseau bas débit dit "0G". Il contribue à l'IoT en permettant l'interconnexion via une passerelle. Sa technologie radio UNB (en) (« Ultra narrow band ») lui permet de bâtir un réseau cellulaire bas-débit, économe en énergie. Ce type de réseau est déployé dans certaines bandes de fréquences ISM, disponibles mondialement sans licence. Sigfox est l'un des deux LPWAN les plus populaires, avec *LoRaWAN*.

**SNR ou S/R** : Signal-to-Noise Ratio (fr : rapport signal sur bruit). Le SNR est le rapport des puissances entre la partie d'un signal (radiofréquence ou autre) qui représente une information et tout ce qui est reçu en plus, qui constitue le "bruit de fond". Ce bruit indésirable a différentes origines, il peut provenir des émetteurs récepteurs tout comme de perturbations électromagnétiques extérieures.

**SoC** : System on Chip (fr : Système sur Puce). Désigne un système complet embarqué sur une seule puce (« circuit intégré ») et pouvant comprendre de la mémoire, un ou plusieurs microprocesseurs, des périphériques d'interface ou tout autre composant nécessaire à la réalisation d’une fonction requise. Les microcontrôleurs sont des SoC.

**SPI** : Serial Peripheral Interface (fr : Interface Série pour Périphériques). Contrôleur de bus série inventé par Motorola.  Un bus SPI permet la connexion en série entre un microcontrôleur maître et plusieurs circuits périphériques esclaves disposant d’interfaces compatibles avec trois fils de liaison. Une ligne supplémentaire est requise pour chaque esclave connecté au bus, afin de l'adresser (un seul esclave est activé à chaque transaction sur le bus).<br>
Par comparaison avec l'I2C, le bus SPI offre des débits plus élevés (jusqu'à 60 Mbps) en contrepartie de sa connectique plus complexe, mais sa portée reste de quelques dizaines de centimètres. Des variantes du SPI, le **Dual SPI** et le **Quad SPI** ont également été développées ; elles consistent à doubler ou quadrupler certaines lignes de données pour augmenter d'autant le débit binaire. Elles servent à la gestion de périphériques tels que des puces de mémoire flash ou des afficheurs de haute résolution.  

**SRAM** : Static Random Access Memory (fr : mémoire à accès aléatoire statique ou mémoire vive statique). La SRAM est un type de mémoire vive utilisant des bascules (de transistors) pour mémoriser les données. Mais contrairement à la mémoire dynamique *DRAM*, elle n'a pas besoin de rafraîchir périodiquement son contenu. Comme la mémoire dynamique, elle est *volatile* : elle ne peut se passer d'alimentation sous peine de voir les informations effacées irrémédiablement. La SRAM est plus onéreuse et moins dense, mais beaucoup moins énergivore et plus rapide que la DRAM. Elle est donc réservée aux applications qui requièrent soit des temps d'accès courts, soit une faible consommation comme les microcontrôleurs.

**STM32** : Famille de microcontrôleurs produits par la société franco-italienne STMicroelectronics, construits autour des processeurs 32 bits ARM Cortex M.

**STM32WB** :  Famille de microcontrôleurs produits par la société franco-italienne STMicroelectronics, construits autour des processeurs 32 bits ARM Cortex M intégrant un émetteur-récepteur radiofréquence pour, notamment, le protocole *BLE*.

**STM32WL** :  Famille de microcontrôleurs produits par la société franco-italienne STMicroelectronics, construits autour des processeurs 32 bits ARM Cortex M intégrant un émetteur-récepteur radiofréquence pour, notamment, le protocole *LoRa*.

**STM32Cube** : Ensemble de bibliothèques et de logiciels conçus pour programmer les microcontrôleurs de la famille *STM32* de la société franco-italienne STMicroelectronics.

**STM32CubeIDE** : *IDE* conçue sur la base du projet open source *Eclipse* pour programmer les microcontrôleurs de la famille *STM32* de la société franco-italienne STMicroelectronics.

**STM32duino** : Extension des bibliothèques *Arduino* aux cartes de prototypage produites par la société franco-italienne STMicroelectronics ainsi qu'à de nombreuses cartes produites par d'autres sociétés utilisant des microcontrôleurs de la famille *STM32*. 

**STM8** : Famille de microcontrôleurs produits par la société franco-italienne STMicroelectronics, construits autour de microprocesseurs 8 bits propriétaires.

**ST-LINK** : Programmeur fabriqué par STMicroelectronics pour transférer un firmware dans la mémoire non-volatile (Flash en général) d’un microcontrôleur.

**SysTick timer** (fr : Timer système). Timer précis et intégré au ARM Cortex, capable de générer une base de temps précise et régulière, dont les performances sont normalisées. Il est essentiellement conçu pour cadencer les éventuels systèmes d’exploitation en temps réel qui pourraient être installés sur le microcontrôleur.

**Sigfox** : Opérateur de télécommunications français créé en 2009 par Christophe Fourtet et Ludovic Le Moan et implanté à Labège, commune de la banlieue toulousaine. C'est un LPWAN qui utilise technologie radio UNB (« Ultra narrow band »). 

**Timer** ou TIM (fr : Compteur). Circuit électronique basé sur un compteur qui coordonne des fonctions variées (selon son architecture) telles que la lecture de signaux extérieurs variables dans le temps, la génération de signaux modulés en largeur d’impulsion (PWM & contrôle de moteurs électriques), la génération d’évènements périodiques, etc.

**Toolchain** (fr : Chaîne de compilation). Une chaîne de compilation désigne l'ensemble des logiciels utilisés dans le processus de compilation d'un programme, pour un processeur donné. Le compilateur n'est qu'un élément de cette chaîne, laquelle varie selon l'architecture matérielle cible. On y trouve généralement un *assembleur*, un *compilateur*, un *éditeur de liens*, un *débogueur*, un *simulateur*, un *programmeur de firmware* (voir *ST-LINK*) ...

**UART** : Universal Asynchronous Receiver Transmitter (fr : Récepteur-Transmetteur Asynchrone Universel). En langage courant, l'UART un composant utilisé pour faire la liaison entre le microcontrôleur et l’un de ses ports série pour échanger des messages (souvent du texte) avec certains modules au fonctionnement complexe tels que les récepteurs GPS ou les modules radiofréquence Wi-Fi. Un UART n'est pas un protocole de communication comme *SPI* ou *I2C*, mais un circuit physique pour la communication série asynchrone.

**USART** : Universal Synchronous/Asynchronous Receiver Transmitter (fr : Récepteur-Transmetteur Synchrone/Asynchrone Universel). Voir *UART*.

**UNB** : Ultra-Narrow Band (fr : bande ultra-étroite). Une technologie de communication radiofréquence est dite UNB si ses messages sont modulés sur des bandes de fréquences très étroites (ceci étant relatif à l'application considérée). L'UNB est utilisé pour transmettre des messages loin avec une faible consommation d'énergie et un très bas débit. L'UNB est utilisé notamment par *Sigfox*.

**USB** : Universal Serial Bus (fr : contrôleur de bus série universel). Contrôleur intégré au microcontrôleur pour gérer le célèbre protocole série éponyme que l'on trouve désormais sur quasiment tous les objets connectés, les ordinateurs, etc.

**UWB** : Ultra-Wide Band (fr : ultra-large bande). Une technologie de communication radiofréquence est dite UWB si ses messages sont modulés sur des bandes de fréquences très larges (ceci étant relatif à l'application considérée). L'UWB est utilisé notamment par *LoRaWAN* mais aussi pour localiser précisément des objets en intérieur.

**Wi-Fi** : Wireless Fidelity (fr : fidélité sans fil). Le Wi-Fi, ou Wifi, est un réseau local lancé en 1999 qui utilise des ondes radio pour relier entre eux, sans fil, plusieurs appareils informatiques dans le but de faciliter la transmission de données. Régi par les normes IEEE 802.11, le Wifi est principalement utilisé pour relier des appareils (ordinateurs portables, smartphones, etc.) à des liaisons haut débit (d'après [cette source](https://www.journaldunet.fr/web-tech/dictionnaire-de-l-iot/1203421-wifi-definition-signification-et-role-avec-la-5g-20200716))

**WWDG** : Window Watchdog (fr : Chien de garde sur Intervalle). Circuit intégré au microcontrôleur chargé de veiller au respect du temps d'exécution d'un programme ou d'une portion de programme. Si le temps d'exécution du code surveillé est plus court qu'une valeur inférieure précisée, ou plus long qu'une valeur supérieure précisée, le WWDG génère un redémarrage (reset) du microcontrôleur.

**XTAL oscillator** (fr : oscillateur à cristal de quartz piézoélectrique). Composant intégrant un cristal de quartz externe au microcontrôleur. On utilise la propriété piézoélectrique du quartz cristallisé pour générer une source de fréquence de qualité qui cadencera les horloges internes du microcontrôleur. Elle est généralement multipliée par un circuit *PLL* intégré dans celui-ci.

**ZigBee** est un protocole de haut niveau permettant la communication d'équipements personnels ou domestiques équipés de petits émetteurs radios à faible consommation. Cette technologie a pour but la communication à courtes distances, telle que le propose déjà la technologie Bluetooth, tout en étant moins chère et plus simple (Source : [Wikipédia](https://fr.wikipedia.org/wiki/ZigBee)). ZigBee permet plusieurs topologies de réseaux : étoile, maillage, arborescente (voir [cette source](https://connect.ed-diamond.com/MISC/misc-086/tout-tout-tout-vous-saurez-tout-sur-le-zigbee)).
