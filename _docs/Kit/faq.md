---
title: Foire aux Questions
description: Foire aux Questions pour résoudre vos problèmes

---
# Foire aux Questions STM32python et STM32duino

* **Q1 - [STM32python]<br>Quelle est la bonne façon d'effacer des fichiers dans le dossier *PYBFLASH* de la carte NUCLEO-WB55 ?**

  Si vous souhaitez effacer des fichiers dans le dossier *PYBFLASH* de votre carte NUCLEO-WB55, utilisez le raccourci clavier *[Shift][Suppr]* pour éviter que le système d'exploitation de votre PC/MAC ne vienne créer un dossier "Corbeille" indésirable dans la mémoire flash du STM32WB55RG. 

<br>

* **Q2 - [STM32python]<br>Que faire quand une carte NUCLEO (NUCLEO-WB55 ou autre) n'est subitement plus détectée par Windows ?**

    1. Vérifier que le câble USB est correctement connecté des deux côtés.
    2. Selon la carte, vérifier que des cavaliers n'ont pas été enlevés ou déplacés.
    3. Essayer un autre port USB sur votre PC / MAC.
    4. Changer de câble USB.
    5. Mettez à jour le frimware du ST Link (si nécessaire) puis effacez la mémoire flash du MCU STM32 avec [STM32CubeProgrammer](https://www.st.com/en/development-tools/stm32cubeprog.html) selon [ce tutoriel](../tools/cubeprog/cube_prog_erase_flash). 
    5. Une fois la flash effacée, reprogrammez (si pertinent) le firmware MicroPython selon les instructions données [ici pour Linux](install_linux/index) ou [ici pour Windows](install_win/index).

<br>

* **Q3 -  [STM32python]<br>Tous les scripts MicroPython qui exploitent le BLE plantent sur ma nouvelle NUCLEO-WB55. Que faire ?**

  Si ces scripts fonctionnaient avant sur d'autres cartes NUCLEO-WB55, il est très probable que la "pile" BLE programmée en usine dans votre nouvelle carte doive être remplacée. La procédure nécessite l'installation de  [STM32CubeProgrammer](https://www.st.com/en/development-tools/stm32cubeprog.html) et est expliquée dans [ce tutoriel](../tools/cubeprog/cube_prog_firmware_ble_hci). 

<br>

* **Q4 - [STM32python]<br>Quelles sont les bonnes pratiques pour ne pas "planter" le système de fichiers MicroPython *PYBFLASH* installé sur la NUCLEO-WB55 ?**

  Nous donnons ici une méthode éprouvée (des milliers de fois) sous Windows, mais ces conseils valent tout autant pour Linux. Lorsque la carte NUCLEO-WB55 est configurée pour exécuter MicroPython, et lorsqu'elle est connectée à un ordinateur Windows par l'USB_USER, elle "expose" sous l'explorateur un disque virtuel *PYBFLASH* dans lequel doivent être déposés les scripts MicroPython, **notamment main.py** qui contient le programme utilisateur.

  Il est donc tentant d'ouvrir directement *main.py* depuis *PYBFLASH*, de le modifier, de le sauvegarder puis de lancer son exécution. **Mais c'est une très mauvaise idée !** Physiquement, la mémoire flash  qui constitue *PYBFLASH* est justement contenue dans le microcontrôleur. Chaque fois que vous sauvegardez un fichier dessus avec un éditeur de code, il faut que les données soient transférées à travers la connexion USB. De nombreux problèmes peuvent survenir pendant cette copie (instabilité de la liaison USB, Windows peut écrire des données cachées, la gestion de l'écriture différée par Windows peut échouer, etc.) qui peuvent corrompre le système de fichiers de *PYBFLASH* et vous contraindre à tout réinstaller (voir question suivante).
   
  C'est pourquoi nous vous conseillons de **ne jamais travailler directement sur les scripts Python contenus dans _PYBFLASH_**. Nous vous recommandons la procédure suivante :
  - Modifier et sauvegarder vos scripts dans un dossier contenu sur le disque dur/le SSD de votre ordinateur ;
  - Arrêter tout script en cours d'exécution sur la carte (*[CTRL]* + *[C]* sous PuTTY) ;
  - Puis glisser-déplacer vos nouveaux scripts dans *PYBFLASH* ;
  - Enfin déconnecter la carte de votre ordinateur en passant par l'icône prévue à cet effet dans la barre des tâches de Windows. Patientez jusqu'à ce que Windows vous confirme que vous pouvez déconnecter physiquement la carte !

<br>

* **Q5 - [STM32python]<br>Que faire quand Windows déclare les fichiers comme « corrupt file » et qu'il n’est plus possible non plus de placer un fichier sur la carte, d’en réécrire un ou même d’en éditer ?**

    Ceci arrive dans de rares cas lorsque la carte n’a pas été utilisée « proprement » (voir ci-avant). Dans cette situation, il est conseillé **d'effacer complètement la mémoire flash du microcontrôleur avec le programme STM32CubeProgrammer** que vous pouvez télécharger sur le site de STMicroelectronics <a href="https://www.st.com/content/st_com/en/products/development-tools/software-development-tools/stm32-software-development-tools/stm32-programmers/stm32cubeprog.html#get-software" target="_blank">ici</a> (vous devrez vous créer un compte *myST*) et re-flasher ensuite selon les instructions données [ici pour Linux](install_linux/index) ou [ici pour Windows](../tools/cubeprog/cube_prog_firmware_stm32). Un tutoriel sur l'utilisation de STM32CubeProgrammer est disponible [ici](../tools/cubeprog/index).
    **Sous linux**, quand le système de fichiers MicroPython est en lecture seule, il peut suffit de faire un *fsck* dessus.

<br>
 
* **Q6 - Que faire quand les capteurs de la carte d’extension X-NUCLEO-IKS01A3 ne renvoient que des « 0 » ?**

    - Tester les branchements
    - Eviter de trop enfoncer les connecteurs

<br>

* **Q7 - Que faire quand "rien ne se passe" avec le shield Grove ?**

    - Vérifier les branchements. Assurez vous notamment que toutes les broches sont bien insérées dans les connecteurs Arduino et que le shield Grove n'est pas "décalé" d'une broche par rapport aux connecteurs.

    - S’assurer que l’interrupteur en bas à gauche du shield (en dessous du connecteur A0) est bien sur 3.3V ou bien sur 5V si le module branché requiert cette tension d'alimentation.

<br>

* **Q8 - J'ai branché le module Grove LCD RGB 16x2 sur le shield Grove et il ne fonctionne pas. Que faire ?**

    Cette problématique est traitée notamment par [ce tutoriel](grove/lcd_RGB_16x2). Pour résumer :
    - Vérifier que le commutateur d'alimentation du shield Grove est bien positionnée sur 5V.
    - Assurez-vous que des résistances de tirage (pull-up) sont bien présente pour le bus I2C. Ceci peut être notamment réalisé en positionnant le shield X-NUCLEO-IKS01A3 au-dessus du shield Grove (voir question suivante).

<br>

* **Q9 - Comment ajouter, sans avoir à faire des soudures, des résistances de tirage (pull-up) sur les connecteurs I2C du shield Grove ?**

  La solution la plus simple consiste à empiler sur le shield Grove un autre shield, tel que le X-NUCLEO KS01A3, qui intègre ces résistances de tirage (voir question suivante).

<br>

* **Q10 - Comment utiliser simultanément le shield Grove et la carte d'extension X-NUCLEO-IKS01A3 avec la NUCLEO-WB55 ?**

    - Brancher d’abord le Shield Grove sur la NUCLEO-WB55 puis l’extension X-NUCLEO KS01A3 sur le Shield Grove. On perd en conséquence l’accès à 3 connecteurs I2C, au connecteur UART et à 5 connecteurs digitaux. Resteront donc accessibles sur le Shield Grove : un connecteur I2C, les connecteurs numériques D4 et D8 et les 4 connecteurs analogiques A0, A1, A2, A3.

    - Un empilement NUCLEO-WB55 puis X-NUCLEO-IKS01A3 puis Shield Grove (dans cet ordre) n’est pas compatible mécaniquement à cause du connecteur ICSP de ce dernier. Vous devrez donc soit enlever le connecteur ICSP du Shield Grove (il n’est pas utilisé par les cartes NUCLEO de ST), soit retirer les 2 cavaliers centraux du X-NUCLEO-IKS01A3 et les remplacer par des ponts de soudure.

<br>

* **Q11 - Avec le module Grove LED, quelle patte de la LED dois-je insérer dans le connecteur marqué « + », la courte ou bien la longue ?**

  La longue ! Attention ; nous vous rappelons que Les LED sont polarisées. Si vous les branchez incorrectement, vous les détruirez probablement.

<br>

* **Q12 - Je souhaite contribuer à la rédaction d'articles sur ce site ; je dispose des autorisations pour le faire. Quelles sont les bonnes pratiques et les outils que vous me conseillez ?**

  Dans un premier temps, consultez [ce tutoriel](../../COMMENT_AJOUTER_UN_TUTORIEL).<br>
  **Si vous travaillez sur PC Windows**, nous vous conseillons d'utiliser [**Github Desktop**](https://desktop.github.com/) pour synchroniser le site en local sur votre ordinateur et "pousser" plus tard vos modifications sur Gitlab ainsi que [**Visual Studio Code**](https://code.visualstudio.com/) pour éditer et prévisualiser les fichiers markdown (.md).

<br>


* **Q13 - [STM32python]<br>Je souhaite installer un firmware MicroPython au format ".hex" sur ma carte NUCLEO, par exemple tel que celui que l'on peut télécharger [ici pour la NUCLEO-L476RG](https://micropython.org/download/NUCLEO_L476RG/) sur le site officiel de MicroPython. Quelle est la procédure à suivre ?**
 
  La méthode la plus sûre pour installer le dernier firmware MicroPython à jour sur une carte NUCLEO **depuis un PC Windows** consiste à :

  - Télécharger le firmware **au format .hex** le plus récent pour votre carte directement depuis [micropython.org](https://micropython.org/download/).
  - Installer [STM32CubeProgrammer](../../tools/cubeprog/index).
  - Programmer le firmware dans le MCU STM32 selon la procédure indiquée [ici](../../tools/cubeprog/cube_prog_firmware_stm32).

  <br>Il existe une alternative nettement plus simple en ligne de commande avec l'utilitaire Windows **robocopy**, présentée  [ici](../../tools/robocopy/index).

  **NB :** Tout ceci suppose que vous utilisez un ordinateur personnel avec un système MS Windows. Si votre système est Linux, vous trouverez un tutoriel sur [cette page](../install_linux/index). Si vous possédez un MAC ... la procédure reste à votre discrétion !

<br>
  
* **Q14 - [STM32python]<br>J'ai installé un firmware MicroPython sur une autre carte NUCLEO que la NUCLEO-WB55, munie d'un seul port USB (via son ST-Link). Je souhaite accéder à la mémoire flash de mon microcontrôleur afin de consulter ou modifier les scripts MicroPython qui s'y trouvent. Quelle est la procédure à suivre ?**

  La plupart des cartes NUCLEO, qui ne disposent que d'un seul connecteur USB ST-LINK, (par exemple la [NUCLEO-L476RG](../../Kit/nucleo_l476)) **n'exposent pas** le système de fichiers *PYBFLASH*. Un outil supplémentaire sera donc nécessaire pour déposer vos scripts Python dans *PYBFLASH*. Nous avons identifié et testé deux solutions :

   - Sous Linux **Rshell**, que vous trouverez [ici](https://github.com/dhylands/rshell)
   - Sous Windows et Linux, l'application [Ampy](https://github.com/scientifichackers/ampy) de Adafruit
   - Une page sur notre site est consacrée à Rshell et Ampy, [ici](../tools/rshell_ampy/index).
   
<br>
 
 * **Q15 - [STM32python]<br>Je souhaite convertir un firmware MicroPython distribué en format ".dfu" en un firmware au format ".bin". Comment puis-je procéder ?**

   Nous vous conseillons de télécharger [à cette adresse](https://www.st.com/en/development-tools/stsw-stm32080.html) les utilitaires **DfuSe USB device firmware upgrade (UM0412)** et de les installer. Cet ensemble de logiciels contient une application **Dfu file manager** qui permet de réaliser cette opération.
    - Lancez *Dfu file manager*.
    - Choisissez l'option *I want to EXTRACT S19, HEX or BIN files from a DFU one*, validez avec le bouton *OK*.
    - Sélectionnez l'emplacement du fichier ".hex" dans *Images" et cochez "Multiple Bin", validez avec le bouton *Extract* :
    
    <br>
    <div align="left">
    <img alt="Dfu file manager" src="images/dfu_1.jpg" width="500px">
    </div>
    <br>

      - Le fichier ".bin" devrait se trouver dans le même dossier que le fichier ".hex". 
