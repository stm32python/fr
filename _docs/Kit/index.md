---
title: Enseigner la programmation embarquée avec les solutions de STMicroelectronics
description: Présentation du matériel utilisé pour les tutoriels de ce site 
---

# Enseigner la programmation embarquée avec les solutions de STMicroelectronics

Comme expliqué sur [la page d'accueil](../../pages/index) ce site propose un ensemble de tutoriels, articles et ressources pour l'enseignement de la programmation embarquée avec [des cartes de prototypage de la société partenaire STMicroelectronics (STM)](nucleo).

## Le matériel

La liste des cartes de prototypage et d'extension de STM est très fournie  [(voir cette liste par exemple)](kits) et ne cesse de s'étoffer et au fil des années, nous allons essentiellement utiliser :

* [La NUCLEO-WB55RG](https://www.st.com/en/evaluation-tools/p-nucleo-wb55.html) - Carte contenant un modem [Bluetooth™ 5](../Embedded/ble) via son SoC[^1] [STM32WB55RG](https://www.st.com/en/microcontrollers-microprocessors/stm32wb55rg.html).

* [La NUCLEO-WL55JC](https://www.st.com/en/evaluation-tools/nucleo-wl55jc.html) - Carte contenant modem [LoRA](../Embedded/lora) via son SoC[^1] [STM32WL55JC](https://www.st.com/en/microcontrollers-microprocessors/stm32wl55jc.html).

* [La NUCLEO-L476RG](https://www.st.com/en/evaluation-tools/nucleo-l476rg.html) - Carte équipée d'un MCU[^2] [STM32L476RG](https://www.st.com/en/microcontrollers-microprocessors/stm32l476rg.html).

* [La X-NUCLEO-IKS01A3 - Carte d'extension avec capteurs inertiels, capteurs environnementaux et connecteurs Arduino UNO](https://www.st.com/en/ecosystems/x-nucleo-iks01a3.html).

Le matériel de STM ne sera cependant pas suffisant, aussi nous utiliserons un grand nombre de modules ou cartes d'extension comme par exemple ...

* [Le Grove Base Shield V2 - Carte fille de connexion](https://wiki.seeedstudio.com/Base_Shield_V2/) et les différents modules fournis dans le [Grove starter kit](http://wiki.seeedstudio.com/Grove_System/#grove-starter-kit). 

  Le Grove Base Shield V2 est recommandé car *il fiabilise et simplifie grandement les connexions via la connectique propriétaire Grove de Seeed Studio*. Si vous ne souhaitez pas l'utiliser, il sera bien sûr tout à fait possible de connecter les différents modules et capteurs directement aux connecteurs Arduino de la NUCLEO-WB55 en utilisant des *câbles dupont* et des *platines d'essais*.

<br>

|P-NUCLEO-WB55 kit|X-NUCLEO-IKS01A3|Grove starter kit (avec Grove Base Shield V2)|
|:-:|:-:|:-:|
|<img alt="p-nucleo-wb55" src="images/p-nucleo-wb55.jpg" width="350px">|<img alt="x-nucleo-iks01a3" src="images/x-nucleo-iks01a3.jpg" width="350px">|<img alt="grove_starter_kit" src="images/grove_starter_kit.jpg" width="400px">|

<br>

> Crédits des images : [STMicroelectronics](https://www.st.com) et [Seeed Studio](http://wiki.seeedstudio.com/Grove_System)


## Le logiciel

Les cartes de prototypage de STMicroelectronics peuvent être programmées depuis
* L'environnement [STM32duino (Arduino IDE)]()
* L'environnement [STM32Cube](https://www.st.com/stm32cube)
* L'environnement [MBed OS](https://os.mbed.com/mbed-os/)
* L'environnement [RIOT OS](https://github.com/RIOT-OS/RIOT/tree/master/boards)
 * L'environnement [MicroPython](https://MicroPython.org/stm32/).

Tous ces environnements sont **gratuits**. L'essentiel des ressources que nous proposons à ce jour utilisent l'environnement MicroPython ou l'environnement Arduino qui sont bien adaptés à notre objectif de partage avec les enseignants et les élèves / étudiants.

## L'environnement et les kits STM32 de Vittascience

La société [**Vittascience**](https://fr.vittascience.com/) propose sur son catalogue **quatre kits pédagogiques** à l’attention des collégiens et lycéens, développés en partenariat avec la société STMicroelectronics et basés sur MicroPython :

-	Le [**Starter kit - version NUCLEO-L476**](https://fr.vittascience.com/shop/product.php?id=310)
-	La [**Station de mesures connectée - version NUCLEO-L476**]( https://fr.vittascience.com/shop/product.php?id=311)
-	La [**Plante connectée - version NUCLEO-WB55RG**]( https://fr.vittascience.com/shop/product.php?id=274)
-	Le [**Robot martien - version NUCLEO-WB55RG**]( https://fr.vittascience.com/shop/product.php?id=275)

Ces kits sont supportés par [**la plateforme de développement de Vittascience**]( https://fr.vittascience.com/code).

Exécutée dans un navigateur, en ligne comme hors ligne, l’IDE de Vittascience permet de « construire » avec des blocs (« façon scratch ») le firmware du microcontrôleur STM32 qui anime le kit.
Plus précisément, le schéma de blocs réalisé dans l’IDE est d’abord traduit en langage Python 3 avant d’être exécuté par l’interpréteur MicroPython installé préalablement sur le microcontrôleur STM32 (ou autre).
Entre autres fonctions très utiles, cette IDE offre aussi *un simulateur*, *un débogueur pas à pas* et *un éditeur de code source MicroPython*.

Un certain nombre de tutoriels sur ce site exploitent des modules, des pilotes logiciels ou plus généralement du matériel provenant des kits STM32 de Vittascience.

## Où acheter les cartes, modules et composants utilisés sur ce site ?

- Les cartes de prototypage de STM peuvent être achetées auprès de fournisseurs de composants électroniques tels que [Vittascience](https://fr.vittascience.com/), [Farnell](https://fr.farnell.com), [RS Online](https://fr.rs-online.com/web/), [Conrad](https://www.conrad.fr) ou bien [Mouser](https://www.mouser.fr/).

 - La plupart des modules du système [Grove de Seeed Studio](http://wiki.seeedstudio.com/Grove_System) et les platines I2C [Qwiic de Sparkfun](https://www.sparkfun.com/qwiic) peuvent également être achetés auprès de ces mêmes fournisseurs.

## Notes

[^1]: Abréviation de "System on Chip", littéralement "Système sur Puce". Désigne un système complet embarqué sur une seule puce (« circuit intégré ») et pouvant comprendre de la mémoire, un ou plusieurs microprocesseurs, des périphériques d’interface ou tout autre composant nécessaire à la réalisation d’une fonction requise. Les microcontrôleurs sont des SoC.

[^2]: Abréviation de "Microcontroler Unit", littéralement "Unité Microcontrôleur". Un microcontrôleur est un « système sur puce » (SoC) car il rassemble sur une même puce de silicium un microprocesseur et d’autres composants et périphériques : des bus, des horloges, de la mémoire, des ports entrées-sorties généralistes, etc.