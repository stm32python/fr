---
title: La carte NUCLEO-WL55JC1
description: Revue de la carte NUCLEO-WL55JC1
---

# La carte NUCLEO-WL55JC1

Cette fiche présente la **carte NUCLEO-WL55JC1 de STMicroelectronics** que nous utiliserons pour des tutoriels sur [**LoRa et LoRaWAN**](../Embedded/index).

## Le microcontrôleur STM32WL55JC1

>> **En préalable à cette section, nous vous conseillons de lire cette qui traite des microcontrôleurs STM32 en général, [ici](stm32)**.

La carte NUCLEO-WL55JC1 est un support de test et démonstration du **microcontrôleur (MCU) [STM32WL55JC](https://www.st.com/en/microcontrollers-microprocessors/stm32wl55jc.html)**, un **SoC** ("System on Chip", en français "Système sur puce") réunissant trois systèmes aux fonctions différentes sur un même substrat de silicium / sur une même puce :

- Un cœur ARM Cortex-M4 "utilisateur" cadencé à 48 MHz offrant **256 kb de mémoire Flash**, **64 kb de SRAM** et **un grand nombre de périphériques intégrés et d’entrées-sorties** ;
- Un cœur ARM Cortex-M0 **sécurisé** cadencé à 48 MHz qui gère ...
- Un [**modem radio LPWAN lui aussi intégré**](../IoT/protocole_rf) et sa pile protocolaire.

Les figures qui suivent illustrent l'architecture du SoC STM32WL55JC :

|Le SoC STM32WL55JC|Diagramme blocs du STM32WL55JC|
|:-:|:-:|
|<img alt="Le SoC STM32WL55JC" src="images/STM32WL55JC_SoC.jpg" width="330px">| <img alt="Diagramme blocs du STM32WL55JC" src="images/block_diagram_STM32WL55JC.jpg" width="350px">|

> Crédit images : [STMicroelectronics](https://www.st.com/content/st_com/en.html)

Celle-ci a été entièrement pensée pour **contenir la consommation énergétique et le coût** (architecture SoC, fréquence de fonctionnement "raisonnable", mémoires embarquées de tailles optimales ...) mais aussi pour renforcer la **cybersécurité embarquée** (pile radio gérée par un composant sécurisé et bien d'autres mesures).

Le SoC peut réaliser plusieurs modulations et supporte des protocoles sub-GHz différents, STMicroelectronics fournit en particulier des piles protocolaires certifiées pour LoRaWAN et Sigfox.

Vous trouverez une présentation très complète du STM32WL55JC [ici](../Kit/microcontrollers_stm32wl_series_product_overview.pdf).

## Anatomie de la carte NUCLEO-WL55JC1

>> **En préalable à cette section, nous vous recommandons de lire celle qui traite des cartes NUCLEO en général, [ici](nucleo)**.

La figure ci-dessous décrit les principales parties d'une NUCLEO-WL55JC (1 ou 2) vue de dessus :

<br>
<div align="left">
<img alt="La carte NUCLEO-WL55JC" src="images/nucleo_wl55jc.jpg" width="600px">
</div>
<br>

**ATTENTION**, tous nos tutoriels utiliseront exclusivement la NUCLEO-WL55**JC1** conçue dans le respect des réglementations de fréquences et des puissances d'émission imposées par la communauté européenne (entre autres). Il existe une autre version de la carte, la NUCLEO-WL55**JC2** adaptée aux réglementations US (entre autres). Ne vous trompez pas de modèle ! 

L'électronique RF passive (filtres et composants de raccordement à l'antenne radio) n'est pas intégrée dans le SoC STMS3WL55JC mais disposée autour de celui-ci et l'ensemble est protégé par un blindage métallique pour une meilleure isolation électromagnétique.

Comme sur toutes les cartes NUCLEO, le port USB **ST-LINK** sert à la programmer et offre également un canal de communication série avec l'ordinateur auquel elle est connectée. Le ST-LINK permet aussi le débogage interactif du firmware exécuté par le MCU STM32WL55JC1. On remarquera que, pour assurer sa mission de "programmeur de firmware", le ST-LINK est animé par un autre MCU STM32 qui a son propre firmware, qu'il faudra parfois mettre à jour selon [cette procédure](../tools/cubeprog/cube_prog_firmware_stlink).

Vous trouverez la page web de présentation de la carte NUCLEO-WL55JC1 [ici](https://www.st.com/en/evaluation-tools/nucleo-wl55jc.html) et sa fiche technique [ici](https://www.st.com/resource/en/user_manual/um2592-stm32wl-nucleo64-board-mb1389-stmicroelectronics.pdf).

## Les connecteurs de la carte NUCLEO-WL55JC1

Le MCU STM32WL55JC1 propose un grand nombre de fonctions et 64 broches sur son boitier, qui ne sont donc pas toutes accessibles par les 32 broches des **connecteurs Arduino** de la  NUCLEO-WL55JC1. Les broches des **connecteurs Morpho**  de la  NUCLEO-WL55JC1 corrigent partiellement ce problème en offrant 32 connexions supplémentaires.

La présente section donne une représentation graphique du câblage "par défaut" des fonctions pour la carte NUCLEO-WL55JC1 sur ses connecteurs Morpho et Arduino. Veuillez noter que cette correspondance n’est pas imposée « en dur » : il est possible de rediriger les périphériques internes du MCU vers d’autres broches de son boitier – et donc de la carte NUCLEO. Cette reconfiguration se fait avec [les bibliothèques LL et HAL](https://community.st.com/s/question/0D50X00009XkhQSSAZ/hal-vs-ll) et est facilitée par l’utilisation de l’outil de configuration graphique [STM32Cube MX](https://www.st.com/en/development-tools/stm32cubemx.html). Tous les périphériques internes ne peuvent cependant pas être redirigés sur n'importe quelles broches (le multiplexage des fonctions a ses limites !), mais les configurations possibles sont très nombreuses.

## Cartographie des broches Arduino de la NUCLEO-WL55JC1

**Attention**, nous vous rappelons que, bien que la nomenclature des broches Arduino pour notre carte soit identique à celle sur les cartes [Arduino UNO](https://store.arduino.cc/products/arduino-uno-rev3), les fonctions du MCU câblées sur ces broches peuvent être différentes (en particulier les PWM). Ceci implique que vous pouvez acheter et brancher un shield conçu pour la carte Arduino UNO sur la carte NUCLEO-WL55JC1, mais qu'il ne fonctionnera pas nécessairement du fait de cette différence de routage des fonctions du MCU !

|Broches compatibles Arduino Uno|Broches PWM disponibles|
|:-:|:-:|
|<img alt="Broches compatibles Arduino Uno" src="images/nucleo_wl55_arduino_pins.png" width="330px">| <img alt="Broches PWM" src="images/nucleo_wl55_arduino_pins_pwm.png" width="330">|

## Cartographie des broches Morpho de la NUCLEO-WL55JC1

La nomenclature des broches Morpho (PA2, PC5, PC13, etc.) vous sera très utile si vous décidez de développer des applications avec l'environnement [STM32duino (Arduino pour STM32)](../Stm32duino/index) ou encore **STM32Cube**, qui permettent de les utiliser.

<br>
<div align="left">
<img alt="Connecteurs Morpho de la NUCLEO-WL55JC1" src="images/connecteurs_morpho_wl55jc1.jpg" width="480">
</div>
<br>

En fait, cette nomenclature est celle des entrées/sorties à usage général (les "GPIO") du MCU STM32WL55JC. Les fonctions qui leurs sont attribuées par défaut dépendent de l'environnement dans lequel on travaille (MicroPython, STM32Cube, STM32duino ...).


