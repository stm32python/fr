---
title: Quelques outils incontournables
description: Quelques outils incontournables
---

# Quelques outils incontournables

Vous trouverez dans cette section des tutoriels sur des logiciels et sujets incontournables pour l'apprentissage de la programmation d'objets connectés avec notre site.

**Outils pour Arduino et (Micro)Python :**

- Pour effacer et/ou reprogrammer la mémoire flash d'un microcontrôleur STM32 : [STM32CubeProgrammer](../tools/cubeprog/index)
- Pour créer facilement des applications pour smartphones : [MIT App Inventor](../tools/AppInventor/index)
- Pour configurer une passerelle LoRaWAN : [The Things Indoor Gateway](../Embedded/ttid)

**Outils pour MicroPython :**

- Pour programmer un firmware au format *.hex* ou *.bin* dans une carte NUCLEO (tous OS) : [STM32CubeProgrammer](../tools/cubeprog/cube_prog_firmware_stm32)
- Pour programmer un firmware au format *.hex* ou *.bin* dans une carte NUCLEO (MS Windows seulement) : [robocopy](../tools/robocopy/index)
- Pour manipuler en ligne de commande le système de fichiers de MicroPython : [Rshell ou Ampy](../tools/rshell_ampy/index)