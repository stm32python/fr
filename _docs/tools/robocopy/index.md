---
title: Installer un firmware MicroPython sous Windows avec robocopy
description: Comment installer un firmware MicroPython sous Windows à l'aide de l'outil en ligne de commande robocopy
---

# Installer un firmware MicroPython sous Windows avec robocopy

Les [firmwares MicroPython](../../Micropython/firmware/index) pour une liste conséquente de cartes de prototypage de STMicroelectronics sont distribués sur [le site officiel de MicroPython](https://micropython.org/download/).

Nous partageons ici une procédure assez simple pour programmer l'un de ces firmwares **au format *".hex" ou ".bin"*** dans une carte NUCLEO (peut importe laquelle), **avec l'utilitaire en ligne de commande *robocopy* intégré à MS Windows**.


## **Etape 1 : Configuration de la carte NUCLEO**

Pour la plupart des cartes NUCLEO (par exemple la [NUCLEO-L476RG](nucleo_l476rg)) qui ne disposent que d'un seul connecteur USB ST-LINK, branchez celui-ci à votre PC.
Si la configuration des cavaliers sur la carte est correcte (normalement il s'agit de la configuration par défaut), la carte et son module ST-Link devraient se mettre sous tension.

<br>
<div align="left">
<img alt="Nucleo_L476RG_Config_STL" src="images/Nucleo_L476_Config_STL.png" width="300px">
</div>
<br>

D'autres cartes NUCLEO sont un peu plus complexes, comme par exemple la [NUCLEO-WB55RG](nucleo_wb55rg) qui dispose de deux connecteurs USB. Dans son cas particulier, il faut configurer la carte comme ceci :

<br>
<div align="left">
<img alt="Nucleo_WB55_Config_STL" src="images/Nucleo_WB55_Config_STL.png" width="300px">
</div>
<br>

En cas de doute, reportez-vous à la fiche technique de la carte pour vous assurer que la carte est configurée en mode ST-LINK.

Si tout se passe correctement, vous devriez observer dans l'Explorateur de Windows un disque virtuel intitulé *NOD_XXXXXX* (où *XXXXXX* dépend de votre carte NUCLEO) exposé par le composant ST-LINK de la carte NUCLEO. Par exemple, pour une NUCLEO-L476RG, on a :

<br>
<div align="left">
<img alt="Nucleo L476RG, ST-LINK" src="images/nod_l476rg.jpg" width="750px">
</div>
<br>

Le ST-LINK expose ici un lecteur virtuel *D:* intitulé *NOD_L476RG*. **Notez la lettre du lecteur, elle nous servira par la suite**.

## Etape 2 : Télécharger le firmware au format *".hex"* pour votre carte

Supposons que nous souhaitions installer un firmware MicroPython dans [une carte NUCLEO-L47-RG](../../Kit/nucleo_l476rg).

Cherchez votre carte sur le site officiel de MicroPython, [ici](https://micropython.org/download/), puis téléchargez le firmware qui vous intéresse au format *".hex"* sur [le site officiel de MicroPython](https://micropython.org/download/). Par exemple, pour une NUCLEO-L476RG, il se trouve [ici](https://micropython.org/resources/firmware/NUCLEO_L476RG-20230426-v1.20.0.hex).

A la date de la rédaction de ce tutoriel, la version la plus récente du firmware est la *1.20.0*, c'est celle que nous avons sélectionnée.

## Etape 3 : Installer le firmware dans le MCU STM32 avec robocopy

Votre carte étant connectée comme indiqué  à l'étape 1, utilisez l'explorateur de fichiers de votre PC Windows et allez dans le dossier où vous avez téléchargé le firmware. Pour continuer avec l'exemple de la NUCLEO-L476RG :

<br>
<div align="left">
<img alt="Firmware téléchargé" src="images/robocopy_1.jpg" width="750px">
</div>
<br>

Cliquez avec le bouton droit de la souris à l'intérieur de ce dossier, sur la ligne du fichier *".hex"* par exemple. Dans le menu contextuel qui s'affiche, choisissez l'option *Ouvrir dans le Terminal*. Une invite en ligne de commande Windows PowerShell devrait apparaître :

<br>
<div align="left">
<img alt="Windows PowerShell" src="images/robocopy_2.jpg" width="750px">
</div>
<br>

A présent, tapez la commande suivante dans la console Windows PowerShell :

```Console
  robocopy . D:\ NUCLEO_L476RG-20230426-v1.20.0.hex /z
 ```

Cette ligne de commande est structurée comme suit :
 - Le caractère "." après "robocopy" indique que nous sommes positionnés dans le dossier où se trouve de firmware que nous venons de télécharger (le fichier *NUCLEO_L476RG-20230426-v1.20.0.hex*).
 - Le lecteur "D:\\" est celui du disque virtuel attribué par Windows au ST-LINK de la carte NUCLEO.
 - Le commutateur "\z" précise à Windows que le firmware doit être copié dans le lecteur *"D:\\"* en mode *"bootable"*.

 Une fois validée avec *[Entrée]* cette ligne de commande va directement copier le firmware dans la mémoire flash du STM32  :

<br>
<div align="left">
<img alt="Windows Robocopy" src="images/robocopy_3.jpg" width="750px">
</div>
<br>

**Et voilà, c'est terminé !**