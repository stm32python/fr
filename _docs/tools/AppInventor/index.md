---
title: Utiliser MIT App Inventor
description: Comment créer des applications clientes pour la NUCLEO-WB55 avec MIT App Inventor
---

# Utiliser MIT App Inventor

<br>

<img src="images/Mit_app_inventor.png" width="200" alt="MIT App Inventor">

<br>


[MIT App Inventor](http://appinventor.mit.edu/) est un environnement de programmation visuel intuitif qui permet à tout le monde - même aux enfants - de créer des applications entièrement fonctionnelles pour smartphones et tablettes. Le langage de programmation utilisé est [Scratch](https://scratch.mit.edu/).

## Sommaire

Vous trouverez dans cette partie les exercices vous permettant de créer des applications MIT App Inventor permettant de connecter votre carte NUCLEO-WB55 à votre smartphone ou votre tablette Android.

 - [Création pas à pas d'une application client BLE pour serveur MicroPython avec MIT App Inventor](../Micropython/BLE/MITAppInventor_1)
 - [Création pas à pas d'une application client BLE pour serveur Arduino avec MIT App Inventor](../Stm32duino/exercices/MITAppInventor_1) (bientôt)

## Références

- Une bonne revue de AppInventor avec un exemple "Objet connecté" sur [turgotlimoges.scenari-community.org](https://turgotlimoges.scenari-community.org/SI-CIT/App_inventor/co/module_App_inventor.html)