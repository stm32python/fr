---
title: Installer et utiliser les application Rshell et Ampy pour MicroPython 
description: Comment installer et utiliser les application Rshell et Ampy pour MicroPython 
---

# Installer et utiliser les application Rshell et Ampy pour MicroPython 

Le [firmware MicroPython pour les microcontrôleurs (MCU) STM32](../../Micropython/firmware/index) génère un système de fichiers *PYBFLASH* dans la mémoire flash du MCU hôte  au moment de son installation.<br>
Pour lancer l'exécution d'un programme par la machine virtuelle du firmware, il faut **commencer par déposer dans *PYBFLASH* un fichier script *main.py*** qui contient les instructions MicroPython dudit programme.<br>
Cette étape pose une difficulté sur [la plupart des cartes NUCLEO supportées par MicroPython](https://micropython.org/stm32/) qui ne disposent que d'une seule interface USB ([le ST-LINK](../../Kit/nucleo)) : ***PYBFLASH* n'est pas directement accessible**. La [NUCLEO-WB55](../../Kit/nucleo_wb55rg) fait figure d'exception car elle dispose de deux ports USB dont un qui expose directement *PYBFLASH* (comme une clef USB) mais, par exemple, vous aurez ce problème avec la [NUCLEO-L476](../../Kit/nucleo_l476rg).<br>
Pour copier *main.py* à l'intérieur de *PYBFLASH* sur une NUCLEO-L476, **il faut utiliser une application spécifique qui réalise cette opération en s'appuyant sur l'interface REPL du firmware MicroPython**. Quelques applications pour PC permettent de réaliser cette opération. Nous allons présenter brièvement les deux que nous avons testées :
- Sur PC Windows, [Ampy de Adafruit](https://github.com/scientifichackers/ampy).
- Sur PC Linux, [Rshell de Dave Hylands](https://github.com/dhylands/rshell).

# Ampy

Ampy est un utilitaire permettant d'interagir avec une carte **CircuitPython** (*i.e. le portage MicroPython de Adafruit, que nous n'utilisons pas*) ou MicroPython via une connexion série.

Ampy est un outil en ligne de commande simple permettant de manipuler des fichiers et d'exécuter du code sur une carte CircuitPython ou MicroPython via sa connexion série. Avec Ampy, vous pouvez envoyer des fichiers de votre ordinateur vers le système de fichiers de la carte, télécharger des fichiers d'une carte vers votre ordinateur, et même envoyer un script MicroPython à une carte pour qu'il soit exécuté.

## Installation

Ampy est une application écrite en Python, ce qui permet de l'exécuter sur PC Linux, Mac OS ou Windows. **Nous allons nous focaliser sur une installation pour Windows**.

### Etape 1 : Installer Python sur votre PC
Il faut naturellement commencer par télécharger et installer **le portage Python pour votre PC depuis le site [python.org](https://www.python.org/)**.

### Etape 2 : Installer PIP sur votre PC
Il faut ensuite installer **PIP**, un outil pour installer et gérer des paquets de logiciels/bibliothèques écrits en Python.

Vous pouvez suivre la procédure qui suit, **traduite depuis le site [geeksforgeeks.org](https://www.geeksforgeeks.org/how-to-install-pip-on-windows/)** qui apporte quelques informations supplémentaires.

**2.1** - Téléchargez le fichier *get-pip.py* [depuis ce site](https://bootstrap.pypa.io/get-pip.py) et déposez-le dans le dossier où Python est installé sur votre PC :

<br>
<div align="left">
<img alt="Où copier get-pip.py ?" src="images/pip-install-1.jpg" width="800px">
</div>
<br>

**2.2** - Ouvrez une invite de commandes (ou terminal Windows *cmd*) dans le dossier en question et tapez puis validez la commande suivante pour démarrer l'installation de PIP :

```console
python get-pip.py
```

Les messages dans la console *cmd* devraient vous confirmer le bon déroulement du processus d'installation. Patientez jusqu'à ce qu'il soit achevé.  

**2.3** - Vous aurez **peut-être** besoin d'ajouter une référence à PIP dans les **variables d'environnement (PATH) de Windows**. Ceci ne sera nécessaire que si Windows vous adresse des messages d'erreur à l'utilisation de PIP. Voici comment procéder sous Windows 11 :

* Allez dans le menu démarrer de Windows et tapez *PATH* dans la barre de recherche, puis validez avec *[Entrée]*.

* L'outil de recherche devrait vous proposer un lien vers le *Panneau de configuration* intitulé *"Modifier les variables d'environnement système"*. Cliquez dessus pour faire apparaître une fenêtre intitulée *"Propriétés système"*.

* Dans *"Propriétés système"*, cliquez sur le bouton *"Variables d'environnement..."*.

* Dans la boite *"Variables utilisateur pour USER" * (où *USER* est votre nom de profil Windows) sélectionnez la ligne *"Path"*. Cliquez sur *"Nouvelle"*, et ajoutez le chemin du fichier *pip.exe*. Par exemple :

```console
C:\Users\USER\AppData\Local\Programs\Python\Python311\Scripts\\ 
```
Puis validez avec *"OK"*.

### Etape 3 : Installer Ampy sur votre PC

Ouvrez un terminal Windows, tapez puis validez avec *[Entrée]* la commande suivante :

```console
pip install adafruit-ampy
```

Les messages dans la console *cmd* devraient vous confirmer le bon déroulement du processus d'installation. Patientez jusqu'à ce qu'il soit achevé. 

## Utilisation

Pour lancer Ampy, il suffit de taper *ampy* dans une invite de commande Windows et valider avec *[Entrée]*. Vous devriez obtenir ceci :

<br>
<div align="left">
<img alt="Ampy, ligne de commande 1" src="images/terminal_ampy.jpg" width="800px">
</div>
<br>

**Pour finir**, quelques exemples de lignes de commande pour les opérations les plus courantes :

**(1) Copier des fichiers ".py" dans *PYBFLASH***

Dans quelques tutoriels MicroPython pour la carte NUCLEO-L476, **nous l'utiliserons pour transférer un fichier *main.py* et d'éventuelles bibliothèques dans la mémoire flash du MCU STM32L476RG**.

**Ouvrez un terminal dans le dossier sur votre ordinateur contenant les fichiers *".py"* à transférer** et, par exemple, validez la ligne de commande suivante :

```console
ampy -p COM8 put main.py
```

Cette commande va transférer le fichier *main.py* dans la mémoire flash du MCU de la carte NUCLEO connectée sur le port série **COM8** de votre PC. Répétez cette opération autant de fois que nécessaire pour transférer d'autres fichiers *".py"* éventuellement requis par votre application.

**(2) Obtenir la liste des fichiers dans *PYBFLASH***

La ligne de commande pour cette opération, pour une NUCLEO sur COM8, est la suivante :

```console
 ampy -p COM8 ls /flash
```
Dans notre cas elle renvoie :

```console
/flash/README.txt
/flash/boot.py
/flash/main.py
/flash/pybcdc.inf
```

**(3) Effacer des fichiers dans *PYBFLASH***

**Pensez de temps en temps à "faire le ménage"** dans la mémoire flash en effaçant les anciens fichiers *.py* avec la commande *rm*.

La ligne de commande pour cette opération, pour supprimer *main.py* dans une NUCLEO sur COM8 est la suivante :

```console
 ampy -p COM8 rm /flash/main.py
```

**Etc. Nous vous laissons explorer plus avant les possibilités de cet outil !**

# Rshell

[**RShell**](https://github.com/dhylands/rshell) est un utilitaire ayant la même vocation que Ampy, mais plus complet et, à priori, ne fonctionnant que sur les systèmes Linux (à confirmer).

## Installation de Rshell

**Premièrement**, utiliser une machine Linux Ubuntu (avec WSL sur Windows, no hope !).
  
**Ensuite**, installer le package rshell comme indiqué sur [ce site](https://github.com/dhylands/rshell).

**Après** entrez dans un terminal :

  ```console
    sudo rshell -p /dev/ttyACM0 -e nano
  ```

Vous devriez obtenir cette réponse :

  ```console
    Using buffer-size of 32
    Connecting to /dev/ttyACM0 (buffer-size 32)...
    Trying to connect to REPL  connected
    Retrieving sysname ... pyboard
    Testing if ubinascii.unhexlify exists ... Y
    Retrieving root directories ... /flash/
    Setting time ... Feb 24, 2023 08:21:09
    Evaluating board_name ... pyboard
    Retrieving time epoch ... Jan 01, 2000
    Welcome to rshell. Use Control-D (or the exit command) to exit rshell
  ```

**Rshell est à présent installé sur votre PC**.

## Utilisation de Rshell

Dans l'invite de commande de RShell, tapez :
  
  ```console
    edit /flash/main.py
  ```

**Et voilà**, le contenu de *main.py* (ou de tout autre fichier *.py*) s'affiche dans nano !<br>

Rshell permet de faire bien plus qu'éditer un fichier *".py"* dans la mémoire flash d'un MCU. Vous trouverez un tutoriel assez complet à son sujet sur [ce site](https://wiki.mchobby.be/index.php?title=MicroPython-Hack-RShell#cp).