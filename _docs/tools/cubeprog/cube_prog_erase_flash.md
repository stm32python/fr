---
title: Effacer la mémoire flash du MCU STM32 sur une carte NUCLEO avec STM32CubeProgrammer
description: Ce tutoriel explique comment effacer la mémoire flash du MCU STM32 sur une carte NUCLEO avec STM32CubeProgrammer
---

# Effacer la mémoire flash d'un microcontrôleur STM32 (sur une carte NUCLEO)

Ce tutoriel explique comment effacer complètement la mémoire flash d'un microcontrôleur (MCU) STM32 d'une carte NUCLEO par le ST-Link à l'aide de l'application STM32CubeProgrammer (version 2.13 ou plus récente).

## **Configuration de la carte NUCLEO**

Pour la plupart des cartes NUCLEO (par exemple la [NUCLEO-L476RG](nucleo_l476rg)) qui ne disposent que d'un seul connecteur USB ST-Link, branchez celui-ci au PC/MAC sur lequel est installé STM32CubeProgrammer.
Si la configuration des cavaliers sur la carte est correcte (normalement il s'agit de la configuration par défaut), la carte et son module ST-Link devraient se mettre sous tension.

<br>
<div align="left">
<img alt="Nucleo_L476RG_Config_STL" src="images/Nucleo_L476_Config_STL.png" width="300px">
</div>
<br>

D'autres cartes NUCLEO sont un peu plus complexes, comme par exemple la [NUCLEO-WB55RG](nucleo_wb55rg) qui dispose de deux connecteurs USB. Dans son cas particulier, il faut configurer la carte comme ceci :

<br>
<div align="left">
<img alt="Nucleo_WB55_Config_STL" src="images/Nucleo_WB55_Config_STL.png" width="300px">
</div>
<br>

Dans tous les cas, en cas de doute, reportez-vous à la fiche technique de la carte pour vous assurer que la carte est configurée en mode ST-Link.

## **Installation de STM32CubeProgrammer**

Nous ne détaillerons pas l'installation de [**STM32CubeProgrammer**](https://www.st.com/en/development-tools/stm32cubeprog.html), qui peut être téléchargé gratuitement [ici](https://www.st.com/en/development-tools/stm32cubeprog.html#get-software) ; la procédure est expliquée sur le site de STMicroelectronics pour les environnements Linux, Windows et Macintosh.

## **Manipulation de STM32CubeProgrammer**

**(1) Lancez STM32CubeProgrammer**, l'interface devrait avoir cet apparence :

<br>
<div align="left">
<img alt="Effacer la flash 1" src="images/erase_flash_1.jpg" width="900px">
</div>
<br>

Vous remarquerez que la fenêtre active, *Memory & File editing*, peut être sélectionnée dans la barre d'icônes verticale complètement à gauche, en cliquant sur l'icône en forme de crayon.

**(2) Cliquez  sur le bouton *Connect* en haut à droite**. La fenêtre *Log* (en bas) confirme que la lecture de la mémoire flash du microcontrôleur de la carte NUCLEO s'est déroulée correctement
La fenêtre centrale affiche le contenu de la mémoire flash. L'interface représente la mémoire par groupes de 4 octets codés en hexadécimal. Les adresses sont incrémentées par pas de 4 (chaque octet est adressé dans les architectures ARM).

<br>
<div align="left">
<img alt="Effacer la flash 1" src="images/erase_flash_2.jpg" width="900px">
</div>
<br>

**(3) Cliquez sur l'icône en forme de gomme**, dans la barre d'icônes verticale complètement à gauche, en bas. Une fenêtre popup s'affiche pour vous demander de confirmer votre souhait d'effacer la totalité de la mémoire flash (*"Are you sure you want to erase full chip flash memory ?"*) :

<br>
<div align="left">
<img alt="Effacer la flash 1" src="images/erase_flash_3.jpg" width="900px">
</div>
<br>

**(4) Validez en appuyant sur le bouton *OK*** de la fenêtre popup* et attendez quelques secondes. Une nouvelle fenêtre popup s'affiche pour vous confirmer que la mémoire flash est complètement effacée (*"Mass erase successfully achieved"*) :

<br>
<div align="left">
<img alt="Effacer la flash 1" src="images/erase_flash_4.jpg" width="900px">
</div>
<br>

**(5) Validez en appuyant sur le bouton *OK* de la fenêtre popup** et déconnectez la carte à l'aide du bouton *Disconnect* en haut à droite. **C'est terminé**, la mémoire flash du microcontrôleur est entièrement effacée. Vous pouvez fermer STM32CubeProgrammer. Nous vous conseillons de déconnecter votre carte du câble USB puis de la reconnecter pour la forcer à faire un reset matériel avant de tenter de la reprogrammer.
