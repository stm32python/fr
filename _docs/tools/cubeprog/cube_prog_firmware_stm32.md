---
title: Reprogrammer le firmware du microcontrôleur d'une carte NUCLEO avec STM32CubeProgrammer
description: Ce tutoriel explique comment reprogrammer le firmware du microcontrôleur d'une carte NUCLEO avec STM32CubeProgrammer
---

# Reprogrammer le firmware du microcontrôleur d'une carte NUCLEO avec STM32CubeProgrammer

Ce tutoriel explique pas à pas comment reprogrammer le firmware du microcontrôleur **au format .hex ou au format .bin** d'une carte NUCLEO avec STM32CubeProgrammer. 

Si vous êtes à l'aise avec les outils en ligne de commande, une procédure **exclusive aux PC Windows** et nettement plus simple (qui ne nécessite l'installation d'aucun logiciel) est donnée sur [cette page](../robocopy/index).

Il peut être nécessaire d'effacer un firmware déjà installé sur le MCU, dans ce cas vous pourrez dans un premier temps suivre les instructions de [ce tutoriel](cube_prog_erase_flash).

## **Configuration de la carte NUCLEO**

Pour la plupart des cartes NUCLEO (par exemple la [NUCLEO-L476RG](nucleo_l476rg)) qui ne disposent que d'un seul connecteur USB ST-LINK, branchez celui-ci au PC/MAC sur lequel est installé STM32CubeProgrammer.
Si la configuration des cavaliers sur la carte est correcte (normalement il s'agit de la configuration par défaut), la carte et son module ST-Link devraient se mettre sous tension.

<br>
<div align="left">
<img alt="Nucleo_L476RG_Config_STL" src="images/Nucleo_L476_Config_STL.png" width="300px">
</div>
<br>

D'autres cartes NUCLEO sont un peu plus complexes, comme par exemple la [NUCLEO-WB55RG](nucleo_wb55rg) qui dispose de deux connecteurs USB. Dans son cas particulier, il faut configurer la carte comme ceci :

<br>
<div align="left">
<img alt="Nucleo_WB55_Config_STL" src="images/Nucleo_WB55_Config_STL.png" width="300px">
</div>
<br>

Dans tous les cas, en cas de doute, reportez-vous à la fiche technique de la carte pour vous assurer que la carte est configurée en mode ST-LINK.

## **Installation de STM32CubeProgrammer**

Nous ne détaillerons pas l'installation de [**STM32CubeProgrammer**](https://www.st.com/en/development-tools/stm32cubeprog.html), qui peut être téléchargé gratuitement [ici](https://www.st.com/en/development-tools/stm32cubeprog.html#get-software) ; la procédure est expliquée sur le site de STMicroelectronics pour les environnements Linux, Windows et Macintosh.

## **Manipulation de STM32CubeProgrammer**

**(1) Lancez STM32CubeProgrammer**, l'interface devrait avoir cet apparence :

<br>
<div align="left">
<img alt="Programmer la flash 1" src="images/program_flash_0.jpg" width="900px">
</div>
<br>

Vous remarquerez que la fenêtre active, *Memory & File editing*, peut être sélectionnée dans la barre d'icônes verticale complètement à gauche, en cliquant sur l'icône en forme de crayon.

**(2) Cliquez  sur le bouton *Connect* en haut à droite**. La fenêtre *Log* (en bas) confirme que la lecture de la mémoire flash du microcontrôleur de la carte NUCLEO s'est déroulée correctement
La fenêtre centrale affiche le contenu de la mémoire flash. L'interface représente la mémoire par groupes de 4 octets codés en hexadécimal. Les adresses sont incrémentées par pas de 4 (chaque octet est adressé dans les architectures ARM).

<br>
<div align="left">
<img alt="Programmer la flash 2" src="images/program_flash_1.jpg" width="900px">
</div>
<br>

**(3) Sélectionnez *Erasing & programming***, dans la barre d'icônes verticale complètement à gauche :

<br>
<div align="left">
<img alt="Programmer la flash 3" src="images/program_flash_2.jpg" width="900px">
</div>
<br>

**(4) Avec le bouton *Browse* renseignez l'emplacement du firmware (.hex ou .bin) pour le MCU STM32 dans *File path***. Il est également important de préciser dans *Start address* l'adresse du premier mot dans  la flash à partir duquel le fichier binaire doit être écrit. Par défaut, il s'agit comme sur notre exemple de *0x08000000*, mais pas toujours ! Il est important de vérifier ce point avant de procéder à la programmation d'un firmware.

<br>
<div align="left">
<img alt="Programmer la flash 4" src="images/program_flash_3.jpg" width="900px">
</div>
<br>

**(5) Cliquez sur le bouton *Start Programming*** et attendez que la barre de progression en bas atteigne 100%. Vous devrez valider trois fenêtres popup (compte tenu des options par défaut que nous avons choisies) : *"Download verified successfully", *"Start operation achieved successfully"* et *"File download complete"*.

<br>
<div align="left">
<img alt="Programmer la flash 5" src="images/program_flash_4.jpg" width="900px">
</div>
<br>

**(6) Validez en appuyant sur le bouton *OK* de la fenêtre popup** et déconnectez la carte à l'aide du bouton *Disconnect* en haut à droite. **C'est terminé**, votre firmware est installé sur le microcontrôleur. Vous pouvez fermer STM32CubeProgrammer. Nous vous conseillons de déconnecter votre carte du câble USB puis de la reconnecter pour la forcer à faire un reset matériel avant de tenter de la reprogrammer.
