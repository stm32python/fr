---
title: Reprogrammer le firmware du ST-Link d'une carte NUCLEO avec STM32CubeProgrammer
Description: Ce tutoriel explique comment reprogrammer le firmware du ST-Link d'une carte NUCLEO avec STM32CubeProgrammer
---

# Reprogrammer le firmware du ST-Link d'une carte NUCLEO avec STM32CubeProgrammer

Ce tutoriel explique comment reprogrammer le firmware du ST-Link d'une carte NUCLEO avec STM32CubeProgrammer (version 2.13 ou plus récente).<br>
 Nous rappelons que le ST-Link est une interface logicielle et matérielle qui permet de déboguer et programmer le  microcontrôleur (MCU) STM32 évalué sur la carte NUCLEO considérée. Un firmware est donc nécessaire pour le ST-Link, il est installé et exécuté par un autre MCU STM32 présent sur la carte NUCLEO ; celui qui gère le connecteur USB ST-Link (justement).<br>
 Le firmware du ST-Link est régulièrement mis à jour par les développeurs de STMicroelectronics et vous aurez parfois besoin de le mettre à jour, selon la procédure détaillée ici.
 
 ## **Configuration de la carte NUCLEO**

Pour la plupart des cartes NUCLEO (par exemple la [NUCLEO-L476RG](nucleo_l476rg)) qui ne disposent que d'un seul connecteur USB ST-Link, branchez celui-ci au PC/MAC sur lequel est installé STM32CubeProgrammer.
Si la configuration des cavaliers sur la carte est correcte (normalement il s'agit de la configuration par défaut), la carte et son module ST-Link devraient se mettre sous tension.

<br>
<div align="left">
<img alt="Nucleo_L476RG_Config_STL" src="images/Nucleo_L476_Config_STL.png" width="300px">
</div>
<br>

D'autres cartes NUCLEO sont un peu plus complexes, comme par exemple la [NUCLEO-WB55RG](nucleo_wb55rg) qui dispose de deux connecteurs USB. Dans son cas particulier, il faut configurer la carte comme ceci :

<br>
<div align="left">
<img alt="Nucleo_WB55_Config_STL" src="images/Nucleo_WB55_Config_STL.png" width="300px">
</div>
<br>

Dans tous les cas, en cas de doute, reportez-vous à la fiche technique de la carte pour vous assurer que la carte est configurée en mode ST-Link.

## **Installation de STM32CubeProgrammer**

Nous ne détaillerons pas l'installation de [**STM32CubeProgrammer**](https://www.st.com/en/development-tools/stm32cubeprog.html), qui peut être téléchargé gratuitement [ici](https://www.st.com/en/development-tools/stm32cubeprog.html#get-software) ; la procédure est expliquée sur le site de STMicroelectronics pour les environnements Linux, Windows et Macintosh.

## **Manipulation de STM32CubeProgrammer**

**(1) Lancez STM32CubeProgrammer**, l'interface devrait avoir cet apparence :

<br>
<div align="left">
<img alt="Mettre à jour le firmware du ST-Link, étape 1" src="images/update_stlink_1.jpg" width="900px">
</div>
<br>

**(2) Cliquez sur le bouton *Firmware upgrade* dans la fenêtre *ST-LINK configuration*  à droite**, une fenêtre popup intitulée *STlink Upgrade x.x.x* (ou x.x.x est un numéro de version, 3.4.0 dans notre copie d'écran ci-dessous) apparaît :

<br>
<div align="left">
<img alt="Mettre à jour le firmware du ST-Link, étape 2" src="images/update_stlink_2.jpg" width="900px">
</div>
<br>

**(3) Cliquez sur le bouton *Open in update mode* de la fenêtre popup, puis sur le bouton *Upgrade***, l'interface devrait avoir cet apparence :

<br>
<div align="left">
<img alt="Mettre à jour le firmware du ST-Link, étape 3" src="images/update_stlink_3.jpg" width="900px">
</div>
<br>

**(4) Patientez jusqu'à ce que la barre de progression en face du bouton *Upgrade* atteigne 100%** : 

<br>
<div align="left">
<img alt="Mettre à jour le firmware du ST-Link, étape 4" src="images/update_stlink_4.jpg" width="900px">
</div>
<br>

**C'est terminé**, le firmware du ST-Link est à jour. Vous pouvez fermer STM32CubeProgrammer. Nous vous conseillons de déconnecter votre carte du câble USB puis de la reconnecter pour la forcer à faire un reset matériel avant de tenter de la reprogrammer.
