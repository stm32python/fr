---
title: Utilisation du logiciel STM32CubeProgrammer
description: Utilisation du logiciel STM32CubeProgrammer

---
# Utilisation du logiciel STM32CubeProgrammer

## Présentation de STM32CubeProgrammer

Dans plusieurs tutoriels sur ce site se pose la question de reprogrammer divers composants des cartes NUCLEO lorsque les outils fournis par MicroPython ou Arduino ne suffisent plus. Dans ces situations, le logiciel [**STM32CubeProgrammer**](https://www.st.com/en/development-tools/stm32cubeprog.html), gratuit et disponible en téléchargement [ici](https://www.st.com/en/development-tools/stm32cubeprog.html#get-software), apparaîtra comme la meilleure solution.

<br>
<div align="left">
<img alt="STM32CubeProgrammer" src="images/ST10279_PR_ST32CubeProgrammer.jpg" width="600px">
</div>
<br>

STM32CubeProgrammer est un outil logiciel tout-en-un pour la programmation des mémoires des MCU STM32 (telles que Flash et RAM). Il fournit un environnement facile à utiliser et efficace pour lire, écrire et vérifier la mémoire des MCU STM32 à travers leur interface de débogage (JTAG et SWD) ou celle d'un bootloader (accessible par UART, USB DFU, I2C, SPI ou CAN). Il est disponible en version GUI (interface utilisateur graphique) et CLI (interface en ligne de commande).
Vous trouverez la documentation complète de STM32CubeProgrammer, y compris ses commandes en ligne, [ici](https://www.st.com/resource/en/user_manual/um2237-stm32cubeprogrammer-software-description-stmicroelectronics.pdf).

Nous ne détaillerons pas l'installation de STM32CubeProgrammer, la procédure est expliquée sur le site de STMicroelectronics pour les environnements Linux, Windows et Macintosh.

## Quand utiliser STM32CubeProgrammer ?

Les liens qui suivent détaillent comment utiliser STM32CubeProgrammer pour ...

* [**Effacer la mémoire flash d'un microcontrôleur STM32**](cube_prog_erase_flash)
* [**Reprogrammer le microcode d'un microcontrôleur STM32**](cube_prog_firmware_stm32)
* [**Reprogrammer le microcode du ST-Link d'une carte NUCLEO**](cube_prog_firmware_stlink)
* [**Reprogrammer la pile BLE du SoC d'une carte NUCLEO-WB55**](cube_prog_firmware_ble_hci)