---
title: Reprogrammer la pile BLE d'un STM32WB sur une carte NUCLEO avec STM32CubeProgrammer
description: Ce tutoriel explique comment reprogrammer la pile BLE d'un STM32WB sur une carte NUCLEO avec STM32CubeProgrammer
---

# Reprogrammer la pile BLE d'un SoC STM32WB sur une carte NUCLEO-WB55 avec STM32CubeProgrammer

Ce tutoriel explique pas à pas comment reprogrammer le firmware qui gère le Bluetooth Low Energy (BLE) dans [le SoC STM32WB55RG](https://www.st.com/en/microcontrollers-microprocessors/stm32wb-series.html) d'une carte [NUCLEO-WB55RG](https://www.st.com/en/evaluation-tools/nucleo-wb55rg.html). Pour simplifier par la suite on dira que l'on explique comment programmer la "pile BLE HCI".

Nous traitons le cas particulier de la mise à jour vers **le firmware le plus récent qui implémente les commandes Host Controller Interface (HCI)**  indispensables à la mise en œuvre de la communication BLE avec la carte NUCLEO-WB55RG dans les environnements de programmation MicroPython et Arduino qui nous intéressent tout particulièrement.


## **Configuration de la carte NUCLEO-WB55**

La figure ci-dessous rappelle comment configurer la carte NUCLEO-WB55 avant de lancer STM32CubeProgrammer :

<br>
<div align="left">
<img alt="Nucleo_WB55_Config_STL" src="images/Nucleo_WB55_Config_STL.png" width="300px">
</div>
<br>

Cette carte possède deux ports USB, celui que nous utiliserons est l'USB ST-LINK, à droite lorsque la carte est vue du dessus. Prenez soin également de positionner le cavalier d'alimentation (celui signalé en rouge) sur la dernière position à droite, comme indiqué sur la figure.

## **Présentation et téléchargements des derniers firmwares pour les FUS et la pile BLE HCI**

Dans un premier temps, nous examinons le dépôt GitHub qui explique la procédure de mise à jour que nous allons suivre et qui met à disposition les différents firmwares ainsi que les adresses auxquelles ils devront être programmés dans le SoC STM32WB55RG. 
Pour atteindre directement le site qui propose **les tout derniers firmwares**, [**rendez-vous à cette adresse**](https://github.com/STMicroelectronics/STM32CubeWB/blob/master/Projects/STM32WB_Copro_Wireless_Binaries/STM32WB5x/). Votre écran devrait ressembler à ceci, mais, bien sûr, le numéro de release sera sans doute plus "grand" que 1.16.0, lorsque vous y serez connecté :

<br>
<div align="left">
<img alt="GitHub WB5x 1" src="images/github_wb55_1.jpg" width="900px">
</div>
<br>

**Remarque :** Pour diverses raisons, vous pourriez avoir besoin de télécharger des versions antérieures des firmwares, et pas nécessairement leur dernière mise à jour. Dans ce cas, vous pouvez cliquer sur le bouton *master* (en haut à droite), sélectionner l'onglet *Tags* qui apparaîtra alors, puis choisir une version antérieure des fichiers dans le menu déroulant de celui-ci. 

Quelle que soit la *release* que vous aurez choisie, quatre fichiers vous seront utiles dans la liste exposée par cette page :
1. *Release_Notes.html* qui explique la procédure que nous allons suivre, nous y reviendrons.
2. *stm32wb5x_BLE_HCILayer_fw.bin* qui contient le microcode de la pile BLE HCI. **Téléchargez le sur votre PC / MAC**.
3. *stm32wb5x_FUS_fw_for_fus_0_5_3.bin* qui contient une version du microcode des FUS.  **Téléchargez le sur votre PC / MAC**.
4. *stm32wb5x_FUS_fw.bin*  qui contient une autre version du microcode des FUS.  **Téléchargez le sur votre PC / MAC**.

Pour visualiser correctement le fichier *Release_Notes.html*, tapez cette URL dans la barre d'adresse de votre navigateur :

>> https://htmlpreview.github.io/?https://github.com/STMicroelectronics/STM32CubeWB/blob/master/Projects/STM32WB_Copro_Wireless_Binaries/STM32WB5x/Release_Notes.html

(On a simplement rajouté *https://htmlpreview.github.io/?* devant l'URL du fichier *Release_Notes.html*).

Le contenu de *Release_Notes.html* devrait s'afficher, comme ceci (aux variations d'aspect près, selon votre navigateur Internet) :

<br>
<div align="left">
<img alt="GitHub WB5x 2" src="images/github_wb55_2.jpg" width="900px">
</div>
<br>

Dans cette page, vous trouverez la version complète de la procédure que nous expliquons ici ainsi que ce tableau qui contient des informations qui seront requises par STM32CubeProgrammer :

<br>
<div align="left">
<img alt="GitHub WB5x 3" src="images/github_wb55_3.jpg" width="900px">
</div>
<br>

Cette copie d'écran montre (entre autres) les deux premières colonnes qui nous intéressent : 
1. *Wireless Coprocessor Binary* qui donne le nom du fichier contenant le firmware.
2. *STM32WB5xxG(1M)* qui contient l'adresse dans la mémoire flash du STM32WB55RG à laquelle le firmware en question doit commencer à être écrit. Les colonnes suivantes sur la droite donnent la même informations pour les autres SoC STM32 de la famille STM32WB (elles ne nous seront pas utiles).

Les informations qui nous seront utiles, **pour la release 1.16.0 que nous avons sélectionnée ici**, extraites de ce tableau, sont les suivantes :

|**Désignation**|**Fichier du firmware**|**Adresse de début en flash**|
|:-|:-:|:-:|
|FUS (cas 1)|stm32wb5x_FUS_fw_for_fus_0_5_3.bin|0x080**EC**000|
|FUS (cas 2)|stm32wb5x_FUS_fw.bin|0x080**EC**000|
|Pile BLE HCI|stm32wb5x_BLE_HCILayer_fw.bin|0x080**E0**000|

Bien évidemment, les adresses vont changer au fil des releases, celles que nous donnons ici correspondent à la release *1.16.0*, mais elles seront probablement différentes pour les releases ultérieures. Soyez attentifs à ce point ; **ne recopiez pas les adresses de notre tutoriel sans vérifier qu'elles sont toujours valables pour les firmwares que vous avez téléchargés !**

**Nous pouvons à présent procéder à la mise à la reprogrammation des FUS et de la pile BLE HCI**.

<br>

## **Préalables importants**

1. **Installation de STM32CubeProgrammer**<br>
Nous ne détaillerons pas l'installation de [**STM32CubeProgrammer**](https://www.st.com/en/development-tools/stm32cubeprog.html), qui peut être téléchargé gratuitement [ici](https://www.st.com/en/development-tools/stm32cubeprog.html#get-software) ; la procédure est expliquée sur le site de STMicroelectronics pour les environnements Linux, Windows et Macintosh.

2. **La séquence des opérations**<br>
Pour installer le firmware de la pile BLE HCI, vous devrez procéder en deux étapes :

    - **Etape 1** : Mettre à jour le microcode des [firmware upgrade services (FUS)](https://www.st.com/resource/en/application_note/dm00513965-st-firmware-upgrade-services-for-stm32wb-series-stmicroelectronics.pdf).
    - **Etape 2** : Mettre à jour le firmware de la pile BLE HCI elle même.
     
Nous allons à présent expliquer ces deux étapes.

<br>

## **Etape 1 : Programmation du firmware des FUS**

**(1)** Une fois la carte connectée par son ST-Link, lancez STM32CubeProgrammer, cliquez sur le bouton "Connect" en haut à droite. **Sélectionnez ensuite l'onglet *Firmware Upgrade Services* dans le menu vertical à gauche**. Vous devriez obtenir ceci :

<br>
<div align="left">
<img alt="FUS upgrade 1" src="images/fus_1.jpg" width="900px">
</div>
<br>

**(2)** Dans la zone *WB Commands* au milieu, en bas, **cliquez sur le bouton *Start FUS***. Une fenêtre popup confirme alors que le FUS est activé par le message *StartFus activated successfully* (copie d'écran ci-dessous). Cliquez sur *OK* pour fermer la  fenêtre popup.<br>
Il est possible que l'application indique que cette opération a échoué via une autre fenêtre popup. Le cas échéant, cliquez sur le bouton *Start FUS* jusqu'à ce que cette étape soit validée.

<br>
<div align="left">
<img alt="FUS upgrade 2" src="images/fus_2.jpg" width="900px">
</div>
<br>

**(3)** Dans la zone *FUS information* au milieu, en haut, **cliquez sur le bouton *Read FUS infos***. La zone de texte *FUS Version* indique à présent **la version des FUS actuellement installée sur votre carte** (dans notre exemple, la v0.5.3.0). 
- Si cette version est la 0.5.3, alors, le firmware pour le point (4) devra être *tm32wb5x_FUS_fw_for_fus_0_5_3.bin*.
- S'il s'agit d'une autre version, à l'exception de la plus récente, alors le firmware pour le point (4) devra être *stm32wb5x_FUS_fw.bin*.
- Si la version la plus à jour des FUS est déjà installée sur votre carte, vous pouvez directement passer à l'étape 2. A cette date la version la plus à jour des FUS est la v1.2.0.0.

<br>
<div align="left">
<img alt="FUS upgrade 3" src="images/fus_3.jpg" width="900px">
</div>
<br>

**(4)** Dans la zone *Firmware Upgrade* en haut à droite, renseignez les informations que vous avez déterminées au point (3) concernant le firmware FUS que vous allez charger sur la carte :
- Dans la zone *File path*, le chemin du firmware sélectionné sur votre ordinateur.
- Dans la zone *Start address*, l'adresse donnée pour le firmware sélectionné suivant ce tableau :

  |**Fichier du firmware**|**Adresse de début en flash**|
  |:-|:-:|
  |stm32wb5x_FUS_fw_for_fus_0_5_3.bin|0x080**EC**000|
  |stm32wb5x_FUS_fw.bin|0x080**EC**000|
  
Pour notre cas, cela donne :

<br>
<div align="left">
<img alt="FUS upgrade 4" src="images/fus_4.jpg" width="900px">
</div>
<br>

**(5)** Cliquez ensuite sur le bouton *Firmware Upgrade*, puis validez par *OK* les deux fenêtres popup qui s'affichent consécutivement, une vous indiquant que le précédant firmware BLE vient d'être effacé et l'autre que la mise à jour des FUS est terminée.

Vous pouvez alors cliquer **sur le bouton *Read FUS infos***. La zone de texte *FUS Version* indique à présent la version des FUS que vous venez d'installer (dans notre cas, v1.2.0.0) mais aussi confirme que la pile BLE a été effacée (*STACK Version v0.0.0.0*) : 

<br>
<div align="left">
<img alt="FUS upgrade 5" src="images/fus_5.jpg" width="900px">
</div>
<br>

Tout est prêt à présent pour mettre à jour la pile BLE HCI. **Ne déconnectez pas votre carte NUCLEO-WB55 de STM32CubeProgrammer et passez à l'étape 2**.


<br>

## **Etape 2 : Programmation du firmware de la pile BLE HCI**

**(1)** Dans la zone *Firmware Upgrade* en haut à droite, renseignez les informations concernant le firmware de la pile BLE HCI que vous allez charger sur la carte :
- Dans la zone *File path*, le chemin du firmware sélectionné sur votre ordinateur.
- Dans la zone *Start address*, l'adresse donnée pour le firmware sélectionné suivant ce tableau :

  |**Fichier du firmware**|**Adresse de début en flash**|
  |:-|:-:|
  |stm32wb5x_BLE_HCILayer_fw.bin|0x080**E0**000|

<br>
<div align="left">
<img alt="HCI BLE stack upgrade 1" src="images/hci_1.jpg" width="900px">
</div>
<br>

**(2)** Cliquez ensuite sur le bouton *Firmware Upgrade*, puis validez par *OK* les deux fenêtres popup qui s'affichent consécutivement, une vous indiquant que le précédant firmware BLE vient d'être effacé et l'autre que la mise à jour de la pile BLE HCI est terminée.<br>

**ATTENTION !**<br>
A ce stade nous vous conseillons de **ne pas** cliquer sur *Read FUS infos*. Pour des raisons qui nous échappent, cette manipulation compromet parfois le bon fonctionnement de la pile HCI, en particulier avec MicroPython. **En revanche, si vous souhaitez utiliser le BLE avec STM32duino, il est indispensable de cliquer sur *Start Wireless Stack*** sinon les appels à la pile BLE ne fonctionneront pas !

**C'est terminé, la pile BLE HCI de votre carte est à jour**. Vous pouvez à présent cliquer sur le bouton *Disconnect* de STM32CubeProgrammer puis déconnecter physiquement votre NUCLEO-WB55 du câble USB pour forcer un "reset matériel".

<br>

## **Mettre à jour la pile BLE HCI pour STM32duino et pour MicroPython**

Depuis quelques temps, les cartes NUCLEO-WB55RG **ne sont plus programmées en usine avec la pile BLE HCI**. Cette modification pose **UN PROBLEME MAJEUR** car toutes les activités BLE proposées sur ce site avec la carte NUCLEO-WB55 nécessitent :

- Pour MicroPython une pile BLE HCI (firmware *stm32wb5x_BLE_HCILayer_fw.bin*) ;
- Pour Arduino une pile BLE HCI (firmware *stm32wb5x_BLE_HCILayer_fw.bin*) en version 1.6.0 ou ultérieure.

**Pour MicroPython en particulier**, si vous essayez de lancer un script parmi ceux de la [section BLE](../../Micropython/BLE/index) et que vous obtenez dans le terminal PuTTY le message d'erreur suivant par le REPL du firmware MicroPython ...

  ```console
  tl_ble_wait_resp: timeout.
  ```

... alors cela signifie que votre carte n'a pas la bonne pile BLE installée et **il est INDISPENSABLE que vous suiviez le présent tutoriel depuis le début pour corriger le problème**.

Vous devrez ensuite procéder à une reprogrammation du firmware MicroPython avec STM32CubeProgrammer, comme indiqué [ici](cube_prog_firmware_stm32). Testez la fonction BLE pour vous assurer que les timeouts ont disparu.

Vous pouvez également vérifier la bonne installation des composants de la pile RF ) l'aide du module [```stm```](https://docs.micropython.org/en/latest/library/stm.html), en utilisant les *Functions specific to STM32WBxx MCUs*.

Pour tester la version du FUS, entrez les commandes REPL suivantes :

```console
>>> import stm
>>> stm.rfcore_fw_version(0)
(1, 2, 0, 0, 0)
>>>
```

Ce qui signifie que le FUS installé sur cette carte a pour version 1.2.0.0.

La procédure est sensiblement la même pour tester la version de la pile BLE installée :

```console
>>> import stm
>>> stm.rfcore_fw_version(1)
(1, 18, 0, 0, 3)
>>>
```

Ce qui signifie que la révision de la pile BLE installée sur cette carte est 1.18.0.0.3. Mais on n'a aucune information sur le type de pile BLE choisi, HCI ou autre ...

**IMPORTANT**<br>
 **Pour une raison qui nous écha ppe, et pour certaines cartes**, il est parfois nécessaire de programmer **deux fois consécutivement** le firmware BLE HCI avant de procéder à l'installation du firmware MicroPython si on souhaite que le BLE fonctionne à la fin ...

<br>

## **Complément : La solution en lignes de commandes**

Si vous avez lu les tutoriels qui précèdent, vous disposez des connaissances suffisantes pour mettre à jour le firmware des FUS, le firmware de la pile BLE SCI et le firmware MicroPython des NUCLEO-WB55 **en utilisant STM32CubeProgrammer en mode console, avec des lignes de commandes**.<br>
Cette approche présente deux avantages par rapport aux manipulations avec l'interface graphique (GUI) de STM32Cube programmer :

1. Elle est bien plus simple et rapide à partir du moment où on comprends comment sont construites les lignes de commandes ;
2. Elle permet d'automatiser les opérations avec un fichier .bat (sous Windows) ou un script shell (sous Linux).

Vous trouverez la documentation complète de STM32CubeProgrammer, y compris ses commandes en ligne, [ici](https://www.st.com/resource/en/user_manual/um2237-stm32cubeprogrammer-software-description-stmicroelectronics.pdf).

Voici comment procéder pas à pas :

**(1) Préparer la mise à jour**

- Installez la toute dernière version de STM32CubeProgrammer ;
- Connectez votre carte au ST-LINK comme indiqué en début de tutoriel ;
- Mettez à jour le ST-LINK de votre carte comme indiqué [ici](cube_prog_firmware_stlink) ;
- Déterminez quelle est la version des FUS installée dessus. Il n'existe pas d'autre solution à ce jour qu'utiliser la GUI de STM32CubeProgrammer en faisant un *Read FUS infos* comme expliqué ci-dessus. Si votre NUCLEO-WB55 est distribuée en boite cartonnée, cette étape est inutile (ce sera expliqué plus loin).

**(2) Mémorisation du chemin de la CLI**

 Sous Windows, lancez une invite de commande MS-DOS et validez la commande :

  ```Console
  SET PROG="C:\Program Files\STMicroelectronics\STM32Cube\STM32CubeProgrammer\bin\STM32_Programmer_CLI.exe"
  ```

Le chemin indiqué est celui de l'exécutable *STM32_Programmer_CLI.exe*, qui peut être différent sur votre ordinateur.


**(3) Démarrage des Firmware Update Services**

Avant tout, démarrez les FUS présents sur la carte :

  ```Console
  %PROG% -c port=swd -startfus
  ```
Cette ligne de commandes (tout comme les suivantes) renvoie un grand nombre d'informations que nous ne copions pas ici afin de ne pas surcharger le tutoriel. Selon les cartes, **vous serez parfois obligé de la lancer deux fois pour que les FUS démarrent avec succès**. En cas d'échec, vous aurez ce message dans la console :
 
   ```Console
    Error: Fus is not yet running, try again
   ```
Lorsque les FUS démarrent correctement, vous aurez ce message dans la console :

 ```Console
    StartFus activated successfully
   ```


**(4) Effacement complet des firmwares BLE**

Tapez ensuite cette commande pour effacer les firmwares FUS et BLE HCI :

  ```Console
  %PROG% -c port=swd -fwdelete
  ```

Attendez que l'opération d'effacement soit validée et terminée (par les messages *"Firmware delete Success"*  puis *"fwdelete command execution finished"*).

**(5) Mise à jour du firmware des FUS**

Selon les résultats de l'opération *Read FUS infos* validez l'une des lignes de commandes suivantes pour mettre à jour le firmware des FUS.

- Si FUS version = v0.5.3 :
  ```Console
  %PROG% -c port=swd -fwupgrade "C:\stm32wb5x_FUS_fw_for_fus_0_5_3.bin" 0x080EC000 firstinstall=0
  ```
- Si FUS version différente de v0.5.3 et antérieure à v1.2.0 :
  ```Console
  %PROG% -c port=swd -fwupgrade "C:\stm32wb5x_FUS_fw.bin" 0x080EC000 firstinstall=0
  ```
- Si FUS version = v1.2.0 (pour les cartes NUCLEO distribuées en boites cartonnées) :
  ```Console
  Pas de ligne de commandes (sautez cette étape, passez directement à (6)).
  ```
  
**Attention**, les firmwares et les adresses où ils doivent être écrits vont changer au cours du temps, lisez bien la section *Présentation et téléchargements des derniers firmwares pour les FUS et la pile BLE HCI* au début de ce tutoriel pour ne pas faire une bêtise !

Notez également que notre ligne de commande suppose que le firmware est déposé dans le dossier *```C:\```* de l'ordinateur, il peut bien évidemment être différent dans votre cas.

Si la console confirme que l'opération s'est bien déroulée par le message *Firmware Upgrade Success* vous pouvez passer au point suivant.

**(6) Mise à jour du firmware de la pile BLE HCI**

Tapez ensuite cette commande afin de mettre à jour le firmware de la pile BLE HCI :

  ```Console
   %PROG% -c port=swd -fwupgrade "C:\stm32wb5x_BLE_HCILayer_fw.bin" 0x080E0000 firstinstall=1
  ```

Comme expliqué au point 4, assurez vous que vous utilisez bien la dernière version du firmware BLE HCI et que vous l'écrivez à la bonne adresse.

Si la console confirme que l'opération s'est bien déroulée par le message *Firmware Upgrade Success* vous pouvez ensuite *démarrer la pile BLE*, comme ceci :

  ```Console
   %PROG% -c port=swd -startwirelessstack
  ```

Cette opération est **indispensable** si vous désirez utiliser le BLE de la NUCLEO-WB55 avec le framework STM32duino.

Passez au point (7) ou (8).

**(7) Pour MicroPython, programmez le firmware**

Vous pouvez à présent charger le firmware MicroPython. Cela se traduit par ces deux lignes de commandes, une première pour effacer complètement la flash et une deuxième  pour reprogrammer le firmware à l'adresse 0x08000000 :

  ```Console
   %PROG% -c port=swd -e all  

   %PROG% -c port=swd mode=UR -d "c:\fw_upy_nucl_wb55_1.19.1.hex" 0x08000000 -v
  ```

**Testez à présent le BLE, par exemple en mode advertising, [comme expliqué ici](../../Micropython/BLE/BLEGAP)**. Si le script advertiser continue de renvoyer des timeouts, répétez (6) et (7) sur votre carte, cela devrait corriger le problème.

 **(8) Et voilà, c'est terminé !**
  Si on rassemble le tout pour, par exemple, le copier ensuite dans un fichier *.bat*, on obtient, dans le cas où la carte est initialement sur la version  v0.5.3 des FUS et où on souhaite installer le firmware MicroPython 1.19.1 :

  ```Console
  SET PROG="C:\Program Files\STMicroelectronics\STM32Cube\STM32CubeProgrammer\bin\STM32_Programmer_CLI.exe"

  %PROG% -c port=swd -startfus

  %PROG% -c port=swd -fwdelete

  %PROG% -c port=swd -fwupgrade "C:\stm32wb5x_FUS_fw_for_fus_0_5_3.bin" 0x080EC000 firstinstall=0

  %PROG% -c port=swd -fwupgrade "C:\stm32wb5x_BLE_HCILayer_fw.bin" 0x080E0000 firstinstall=1

  %PROG% -c port=swd -startwirelessstack

  %PROG% -c port=swd -e all  
  
  %PROG% -c port=swd mode=UR -d "c:\fw_upy_nucl_wb55_1.19.1.hex" 0x08000000 -v
  ```
