---
title: Interfaçage avec Jupyter
description: Interfaçage avec Jupyter
---
<div align="left"> <img  width="130" src="images/logo_jupyter.png" alt="Jupyter" /></div>

# Interfaçage avec Jupyter

Pour pouvoir visualiser plus efficacement les résultats des différents programmes et pour réaliser des comptes rendus de TP plus facilement nous pouvons utiliser le notebook Jupyter.

Pour pouvoir utiliser ce notebook nous devons simplement ajouter MicroPython à Jupyter et définir le port correspondant à notre carte.

Sous Windows, ces commandes sont à taper dans l'invite de commande.

![cmd windows](images/cmd.png)

## Installation du noyau MicroPython

### Installation de Python3

Aller sur le site [https://www.python.org/downloads/](https://www.python.org/downloads/)

Télécharger et Installer Python3

Pour vérifier que l'installation s'est faite correctement `python --version` et `pip --version`

![pip et python versions](images/python_pip_version.PNG)

### Installation de Jupyter

```bash
pip install notebook`
```

Pour vérifier que tout fonctionne vous pouvez lancer un notebook Jupyter avec la commande `jupyter notebook`. Vous devriez arriver sur une page comme celle ci dans votre navigateur :

![Accueil Jupyter](images/python_pip_version.PNG)

### Récupération du projet MicroPython

Aller sur le site [https://GitHub.com/goatchurchprime/jupyter_MicroPython_kernel.git](https://GitHub.com/goatchurchprime/jupyter_MicroPython_kernel.git)

![projet git](images/git_clone.PNG)

Télécharger le projet git sous format zip et dézipper le

Mettre le dossier dans Utilisateur

Installation de la librairie :
```bash
pip install -e jupyter_MicroPython_kernel-master
```

Installation du noyau dans Jupyter
```bash
python -m jupyter_MicroPython_kernel.install
```

Affichage de la liste des noyaux disponibles :
```bash
jupyter kernelspec list
```

![verification kernelgit](images/kernelspec.PNG)

Vous pouvez maintenant [utiliser un notebook Jupyter](./notebook)