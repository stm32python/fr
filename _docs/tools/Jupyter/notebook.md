---
title: Utilisation d'un notebook Jupyter avec la carte MicroPython
description: Utilisation d'un notebook Jupyter avec la carte MicroPython
---
<div align="left"> <img  width="130" src="images/logo_jupyter.png" alt="Jupyter" /></div>

# Utilisation d'un notebook Jupyter avec la carte MicroPython

 Démarrer Jupyter en entrant la commande
```bash
jupyter notebook
```

En haut à droite cliquer sur `New` et sélectionner `MicroPython-USB`

![creation notebook jupyter](images/jupyter_new.PNG)

 Pour utiliser la carte nous devons préciser le port à utiliser. Cette ligne est à ajouté comme première ligne du notebook :
```bash
serialconnect to –port=COM14 –baud=115200
```

> Attention, le port `COM` pourrait changer suivant l'ordinateur. Vous pouvez maintenant exécuter dans ce notebook tous les programmes décrit dans les exercices.
