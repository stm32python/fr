---
layout: page
title: STM32 pour l'éducation
permalink: /
---

<!-- Google tag (gtag.js) -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=G-P57S1562RW"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());
    gtag('config', 'G-P57S1562RW');
  </script>

# Bienvenue sur le site STM32 pour l'éducation

## Contexte

La réforme des lycées introduit un nouvel enseignement suivi par tous les élèves de seconde générale et technologique :
[SNT (Sciences Numériques et Technologie)](https://eduscol.education.fr/cid143713/snt-bac-2021.html).
Un des thèmes abordés par cet enseignement est l’[Internet des Objets (IdO)](https://fr.wikipedia.org/wiki/Internet_des_objets) (en anglais on traduit "Internet des Objets" par "Internet of Things", abrégé par IoT), couvert par le chapitre *"Informatique embarquée et objets connectés"*, qui représente l’extension d’Internet à des choses et à des lieux du monde physique.

## Objectif

L'objectif prioritaire de **STM32 pour l'éducation** est de fournir aux enseignants du lycée et aux lycéens des tutoriels open-source pour la conception de supports pédagogiques et pour l'initiation à l’Internet des Objets pour l’enseignement de [SNT (Sciences Numériques et Technologie)](https://eduscol.education.fr/cid143713/snt-bac-2021.html).

Les tutoriels  sont également adaptés à d'autres enseignements de première et terminale générales, notamment en spécialité NSI (Numérique et Sciences Informatiques), en spécialité SI (Sciences de l’ingénieur), ou en série technologique STI2D (Sciences et Technologies de l’Industrie et du Développement Durable).

Les filières d'enseignement professionnel concernées par les compétences du numérique sont aussi bénéficiaires de cette initiative notamment, dans le secondaire, les sections BAC pros CIEL (pour « Cybersécurité, Informatique et réseaux, Électronique ») et leur prolongement dans le supérieur, les BTS CIEL.

Les [associations Ingénieurs Pour L'Ecole (IPE)](https://www.linkedin.com/company/ing%C3%A9nieur-pour-l-%C3%A9cole/?originalSubdomain=fr) et [Campus d'Excellence des Métiers et des Qualifications Industrie du Futur Sud](https://campus-industriefutur-sud.com/) participent à l'adaptation de ces ressources pour les pratiques et besoins pédagogiques des enseignants et des élèves (en collaboration avec les inspecteurs de l'Education Nationale). Ainsi, les ressources mises en ligne sur se site ont-elles été révisées et testées à l'école afin qu'elles soient adaptées tout autant au e-learning des enseignants qui préparent leurs cours qu'à l'animation "en direct" d'ateliers sur la programmation embarquée en salle de classe, face aux élèves.

Pour finir, nous rappelons que ces tutoriels s'appuient essentiellement sur des [cartes de prototypage NUCLEO](https://www.st.com/en/evaluation-tools/stm32-nucleo-boards.html) de [l'entreprise partenaire STMicroelectronics](https://www.st.com), mais aussi sur les kits pédagogiques proposés par [l'entreprise partenaire Vittascience](https://fr.vittascience.com/shop/). Ils permettent de réaliser des montages électroniques et des programmes pour les microcontrôleurs STM32 avec les langages C/C++ et MicroPython.

## Partenaires et contributeurs

Ce projet est soutenu par un ensemble de **partenaires** académiques, universitaires et du monde de l'entreprise et a bénéficié de nombreuses contributions individuelles.

### Les partenaires sont :

* Les rectorats des académies de [Grenoble](http://www.ac-grenoble.fr) et d’[Aix-Marseille](http://www.ac-aix-marseille.fr)
* [L'association Ingénieurs Pour l'Ecole (IPE)](https://eduscol.education.fr/855/le-dispositif-ingenieurs-pour-l-ecole).
* [L'association Campus d'Excellence Industrie du Futur Sud](https://campus-industriefutur-sud.com/)
* [STMicroelectronics (entreprise partenaire)](https://www.st.com)
* [Inventhys (entreprise partenaire)](http://www.inventhys.com)
* [Vittascience (entreprise partenaire)](https://fr.vittascience.com)
* [Polytech Grenoble](https://www.polytech-grenoble.fr)
* [Grenoble INP Institut d'ingénierie et de management](https://www.grenoble-inp.fr/)
* [Université Grenoble Alpes](https://www.univ-grenoble-alpes.fr)


### Contributrices et contributeurs (liste non exhaustive) : 

* ALBERT Antoine
* BENAMMAR Manel
* CHAIX Manon
* CHATEIGNIER Guy
* DONSEZ Didier
* ESCODA Michael
* FARGES Robin
* FEVRIER Arnaud
* FOUILLEUL Alex
* GAH-EL-HILAL Ahmed-Baba
* GHAZALI Waïl
* GIROUD Nicolas
* JOLAINE Baptiste
* LE SAINT Erwan
* LEMIERE Gaël
* LOPES Pedro
* MARIETTI Yannick
* MAUDUIT Claire
* MICHELARD Leïla
* NGUENA Gloria
* NOLLOT Romaric
* PHAN Richard
* PRIOUZEAU Christophe
* RAYANE Bouzid
* REYNAUD Aurélien
* SLAMA Jeremy
* TERRIER Julien
* VIOLET Florian


### Comment contribuer ?

Enseignant, étudiant, lycéen, ingénieur, hobbyiste, vous avez réalisé un complément à ces tutoriels ?<br>
N'hésitez pas nous contacter pour contribuer au projet sur cette adresse :

>> stm32python-contact@imag.fr