# Objet du script : Créer un "chenillard" avec interruptions
# Exemple de configuration des GPIO pour une gestion des LED intégrées de la carte NUCLEO-WB55

import pyb # Bibliothèque de MicroPython permettant les accès aux périphériques (GPIO, LED, etc.)
import time # Bibliothèque de MicroPython permettant de faire des pauses systèmes

print( "Les interruptions avec MicroPython c'est facile" )

# Initialisation des LEDs (LED_1, LED_2, LED_3)
led_bleu = pyb.LED(1)
led_vert = pyb.LED(2)
led_rouge = pyb.LED(3)

# Initialisation des variables globales
compteur_de_led = 0
#flags des interruptions
pause = 0
inv = 0

# Initialisation des boutons (SW1 et SW2)
sw1 = pyb.Pin('SW1')
sw1.init(pyb.Pin.IN, pyb.Pin.PULL_UP, af=-1)
sw2 = pyb.Pin('SW2')
sw2.init(pyb.Pin.IN, pyb.Pin.PULL_UP, af=-1)


#Fonction d'interruption de SW1 (met en pause le chenillard)
def Pause(line):
	global pause
	if(pause == 0):
		pause = 1
		print("Pause")
	else:
		pause = 0

#Fonction d'interruption de SW2 (Inverse le sens du chenillard)
def Inversion(line):
	global inv
	if(inv == 0):
		inv = 1
	else:
		inv = 0
	
# Initialisation des vecteurs d'interruption
irq_1 = pyb.ExtInt(sw1, pyb.ExtInt.IRQ_FALLING, pyb.Pin.PULL_UP, Pause)
irq_2 = pyb.ExtInt(sw2, pyb.ExtInt.IRQ_FALLING, pyb.Pin.PULL_UP, Inversion)


while True: # Création d'une boucle "infinie" avec des actions uniquement si le système n'est pas en pause
	if (pause == 0):
		if compteur_de_led == 0:
			led_bleu.on()
			led_rouge.off()
			led_vert.off()
		elif compteur_de_led == 1:
			led_bleu.off()
			led_vert.on()
			led_rouge.off()
		else :
			led_bleu.off()
			led_vert.off()
			led_rouge.on()
		
		# On veut allumer la prochaine LED à la prochaine itération de la boucle avec gestion du sens
		if (inv==0):
			compteur_de_led = compteur_de_led + 1
			if compteur_de_led > 2:
				compteur_de_led = 0
		else:
			compteur_de_led = compteur_de_led - 1
			if compteur_de_led < 0:
				compteur_de_led = 2
		time.sleep_ms(500) # Temporisation de 500ms
