import pyb
from pyb import Pin, Timer
import time

#Variables globales
BP1 = 0
BP2 = 0
BP3 = 1

#BP (en entrée + pull up)
sw1 = pyb.Pin('SW1')
sw1.init(pyb.Pin.IN, pyb.Pin.PULL_UP, af=-1)
sw2 = pyb.Pin('SW2')
sw2.init(pyb.Pin.IN, pyb.Pin.PULL_UP, af=-1)
sw3 = pyb.Pin('SW3')
sw3.init(pyb.Pin.IN, pyb.Pin.PULL_UP, af=-1)

#LED (LED de la carte NUCLEO)
led_bleu = pyb.LED(3)
led_vert = pyb.LED(2)
led_rouge = pyb.LED(1)

#Driver moteur ("enable" + "input 1" et "input 2" en sorties)
moteur_enable = pyb.Pin('D3')
moteur_pin1 = pyb.Pin('D4', Pin.OUT_PP)
moteur_pin2 = pyb.Pin('D5', Pin.OUT_PP)

#PWM pour le moteur (Timer 1 channel 3, fréquence = 1kHz et sur "enable")
tim1 = Timer(1, freq=1000)
ch3 = tim1.channel(3, Timer.PWM, pin=moteur_enable)

#Interruption de SW1 (avancer le moteur)
def ITbutton1(line):
    #Variables globales
    global BP1
    global BP2
    global BP3
    #Incremente BP1, le reste à 0
    BP1 = BP1 + 1
    BP2 = 0
    BP3 = 0

#Interruption de SW2 (reculer le moteur)
def ITbutton2(line):
    #Variable globales
    global BP1
    global BP2
    global BP3
    #Incremente BP2, le reste à 0
    BP1 = 0
    BP2 = BP2 + 1
    BP3 = 0

#Interruption de SW3 (arreter le moteur)
def ITbutton3(line):
    #Variables globales
    global BP1
    global BP2
    global BP3
    #Incremente BP3, le reste à 0
    BP1 = 0
    BP2 = 0
    BP3 = BP3 + 1

#Initialisation des vecteurs d'interruption
irq_1 = pyb.ExtInt(sw1, pyb.ExtInt.IRQ_FALLING, pyb.Pin.PULL_UP, ITbutton1)
irq_2 = pyb.ExtInt(sw2, pyb.ExtInt.IRQ_FALLING, pyb.Pin.PULL_UP, ITbutton2)
irq_3 = pyb.ExtInt(sw3, pyb.ExtInt.IRQ_FALLING, pyb.Pin.PULL_UP, ITbutton3)

#Fonction qui gère le moteur
def etat_moteur(vitesse):
    #Variables globales
    global BP1
    global BP2
    global BP3

    #Si BP1 appuyé
    if((BP1 != 0) and (BP2 == 0) and (BP3 == 0)):
        #Allume la LED bleu
        led_bleu.on()
        #Allume l'enable 1 / Eteint l'enable 2
        moteur_pin1.high()
        moteur_pin2.low()
        #PWM pour régler la tension du moteur
        ch3.pulse_width_percent(vitesse)
    #Si BP2 appuyé
    if((BP1 == 0) and (BP2 != 0) and (BP3 == 0)):
        #Allume la LED verte
        led_vert.on()
        #Allume l'enable 2 / Eteint l'enable 1
        moteur_pin1.low()
        moteur_pin2.high()
        #PWM pour régler la tension du moteur
        ch3.pulse_width_percent(vitesse)
    #Si BP3 appuyé
    if((BP1 == 0) and (BP2 == 0) and (BP3 != 0)):
        #Allume la LED rouge
        led_rouge.on()
        #Eteint l'enable 1 et 2
        moteur_pin1.low()
        moteur_pin2.low()
        #PWM pour régler la tension du moteur
        ch3.pulse_width_percent(0)

#Variable pour gérer la vitesse de rotation du moteur
vitesse = 100

#Boucle infinie
while True:
    #Eteind toutes les LED par défaut
    led_bleu.off()
    led_vert.off()
    led_rouge.off()
    #Passe en parametre la vitesse souhaité
    etat_moteur(vitesse)
