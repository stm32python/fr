import pyb
from machine import I2C, Pin
from time import sleep_ms
from wiichuck import WiiChuck

#Initilisation servomoteur
servo = pyb.Pin('D6')
tim_servo = pyb.Timer(1, freq=50)

#Initialisation Nunchuk
i2c = I2C(1)
sleep_ms(1000)
wii = WiiChuck(i2c)

while True:
	joystick_x = (wii.joy_x + 200)/26
	tim_servo.channel(1, pyb.Timer.PWM, pin=servo, pulse_width_percent=joystick_x)
	wii.update()

#periode = 1/f (en general periode entre 10 et 20ms, ici 20ms)
#pulse_width_percent = (temps/periode)*100
#temps entre 0,5 et 2,5ms