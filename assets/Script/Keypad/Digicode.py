from pyb import Pin, ExtInt
import time
print("start")

#*** Définition des vecteurs d'entrée, de sortie et d'interruptions ***
IN = [0]*4
OUT = [0]*4
IRQ = [0]*4

#*** Définition des connexions physiques ***
ROW = ['A15', 'C10', 'A10', 'C6']
COLUMN = ['A9','C12', 'C13', 'A8']

#*** Définition de la matrice représentative du clavier ***
MATRIX = [[1,2,3,'A'],
     [4,5,6,'B'],
     [7,8,9,'C'],
     ['*',0,'#','D']]	

#*** Attribution des sorties ***
for a in range(4):
	OUT[a] = Pin(ROW[a], Pin.OUT)
	OUT[a].value(1)

led_bleue = pyb.LED(3)
led_verte = pyb.LED(2)
led_rouge = pyb.LED(1)

#*** Flags et fonctions d'interruption ***
flag = 0
flag_BP = 0

def inter(line):	#Appuie keypad
	global flag
	time.sleep(0.2)
	flag = 1
	
def BP(line):		#Appuie SW1
	global flag_BP
	global nb_saisie
	print("Saisir nouveau code : ")
	led_bleue.on()
	flag_BP = 1
	nb_saisie = 0

#*** Attribution des entrées en interruption ***
for a in range(4):
	IN[a] = pyb.Pin( COLUMN[a] , pyb.Pin.IN)
	IN[a].init(pyb.Pin.IN, pyb.Pin.PULL_DOWN, af=-1)
	IRQ[a] = ExtInt(IN[a], ExtInt.IRQ_RISING, Pin.PULL_DOWN, inter)
	
sw1 = pyb.Pin('SW1')
sw1.init(pyb.Pin.IN, pyb.Pin.PULL_UP, af=-1)
irq_BP = pyb.ExtInt(sw1, pyb.ExtInt.IRQ_FALLING, pyb.Pin.PULL_UP, BP)

#*** Définition des variables permettant la gestion du digicode ***	
old_carac = -1
nb_saisie = 0
code = [1,2,3,'A']
code_saisi = [0]*4

#*** Fonction qui gère l'appuie sur une touche du keypad ***
def appuie():
	global flag
	global old_carac
	global nb_saisie
	global code_saisi
	global code
	
	flag = 0
		
	for i in range(4):
		if(IN[i].value()==1):	#Test quelle colonne est concernée par l'appuie 
			for j in range(4):	#Eteint les différentes lignes
				OUT[j].value(0);
				if(IN[i].value()==0):
					if (MATRIX[i][j]!=old_carac):	#Trouve le bouton appuyé et l'affiche en gérant les rebonds et supprimant les appuies longs
						print(MATRIX[i][j])
						old_carac = MATRIX[i][j]
						code_saisi[nb_saisie] = MATRIX[i][j]
						
						if(flag_BP == 1):
							code[nb_saisie] = MATRIX[i][j]	#Change le code
							 
						nb_saisie = nb_saisie + 1
						
					for k in range(4):
						OUT[k].value(1)		#Rallume toutes les lignes
					break;	

#*** Fonction qui teste si le code entré est le bon ***			
def test_code():
	global nb_saisie
	
	for i in range(4):
		if(code[i] != code_saisi[i]):
			print("Code faux")
			led_rouge.on()
			nb_saisie = 0
			time.sleep(2)
			led_rouge.off()
			print("\nEntrez le code à 4 chiffres : ")
			return -1
	print("Code juste")
	led_verte.on()
	nb_saisie = 0
	time.sleep(2)
	led_verte.off()
	print("\nEntrez le code à 4 chiffres : ")

			
#*** Début du main ***
print("Entrez le code à 4 chiffres : ")	
	
while (1):
	if((flag == 1) and (nb_saisie < 4)):	#Si une touche a été appuyée
		appuie()
		
	if (nb_saisie >= 4):	#Si le code est saisie en entier
		
		if(flag_BP==1):		#Gestion du nouveau code
			flag_BP=0
			print("Le nouveau code est : ", code[0], code[1], code[2], code[3])
			nb_saisie = 0
			time.sleep(2)
			led_bleue.off()
			print("\nEntrez le code à 4 chiffres : ")	
			
		else:
			test_code()
